import { TICKET_FILTER_ALL } from '../constants/ticket-filters';
import { CONTROLLED_FILTER_ALL } from '../constants/controlled-filters';

import moment from 'moment';

export const initialState = {
  settings: {
    domain: 'CORP',
    ticketFilter: TICKET_FILTER_ALL,
    controlledFilter: CONTROLLED_FILTER_ALL,
    notificationsToggled: false,
    notificationsFxToggled: true
  },
  session: {
    page: 1,
    dateBegin: moment(new Date()).startOf('day').format(),
    dateEnd: moment(new Date()).endOf('day').format(),
    historyDateBegin: moment(new Date()).startOf('day').format(),
    historyDateEnd: moment(new Date()).endOf('day').format(),
    dictionaries: {
      "pump": { name: 'Насосы', data: [] },
      "repair-type-e": { name: 'Типы ремонтов', data: [] },
      "repair-type-s": { name: 'Типы ремонтов', data: [] },
      "puller-type": { name: 'Типы съемников', data: [] }
    },
    incomingTable: {
      data: [],
      index: 1,
      size: 10,
      sizeTotal: 0,
      isFetching: false
    },
    outcomingTable: {
      data: [],
      index: 1,
      size: 10,
      sizeTotal: 0,
      isFetching: false
    },
    historyTable: {
      data: [],
      index: 1,
      size: 10,
      sizeTotal: 0,
      isFetching: false
    },
    routes: [],
    membershipID: null,
    token: null,
    notifications: [],
    wizardRequest: {
      isVisible: false
    },
    wizardResponse: {
      isVisible: false
    },
    wizardControlled: {
      isVisible: false
    },
    wizardWell: {
      isVisible: false
    },
    pumps:[],
    isPumpsLoading: false,
    pullerTypes: [],
    isPullerTypesLoading: false,
    repairTypes: [],
    isRepairTypesLoading: false,
    departments: [],
    wells: [],
    shops: [],
    fields: [],
    isFieldsLoading: false,
    clusters: [],
    isClustersLoading: false,
    measureFacilityPositions: [],
    isMeasureFacilityPositionsLoading: false,
    requestCount: 0
  },
  cache: {
    information: {
      wells: {},
      departments: {},
      ticketDetails: {},
      pumps: {},
    }
  },
  routing: {
    location: null
  }
};