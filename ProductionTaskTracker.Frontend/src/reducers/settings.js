import Immutable from 'immutable';
import { initialState } from './initial-state';

import {
  SETTINGS_SET,
  SETTINGS_RESET
} from '../actions/settings';

export function settingsReducer(state = Immutable.fromJS(initialState.settings), action) {
  switch (action.type) {
    case SETTINGS_SET:
      return state.set(action.payload.field.toString(), action.payload.value);
    case SETTINGS_RESET:
      return state.set(action.payload.field.toString(), initialState.settings[action.payload.field.toString()]);
    default:
      return state;
  }
}