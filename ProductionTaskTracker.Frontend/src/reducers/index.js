import Immutable from 'immutable';
import { combineReducers } from 'redux-immutable';
import { reducer as form } from 'redux-form/immutable';
import { routerReducer } from 'react-router-redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import { reducer as toastr } from 'react-redux-toastr';
import reduceReducers from 'reduce-reducers';

import { settingsReducer } from './settings';
import { sessionReducer } from './session';
import { cacheReducer } from './cache';

const rootReducer = reduceReducers(
  combineReducers({
    settings: settingsReducer,
    session: sessionReducer,
    cache: cacheReducer,
    routing: routerReducer,
    form,
    loadingBar: loadingBarReducer,
    toastr
  }),
  (state) => {
    const formFields = state.getIn(['form', 'wizard-request', 'fields', 'wells']);
    const formValues = state.getIn(['form', 'wizard-request', 'values', 'wells']);
    const formParametersValues = state.getIn(['form', 'wizard-request', 'values', 'parameters']);
    const stateFields = state.setIn(['form', 'wizard-request', 'fields', 'parameters'], formFields);
    const condition = formParametersValues && formValues && formParametersValues.count() != formValues.length || (formValues && typeof formParametersValues === 'undefined');
    return condition ? stateFields.setIn(['form', 'wizard-request', 'values', 'parameters'], Immutable.List(Array.from(Array(formValues.length)))) : stateFields;
  }
);

export default rootReducer;
