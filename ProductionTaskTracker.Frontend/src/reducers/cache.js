import Immutable from 'immutable';
import { initialState } from './initial-state';

import {
  DICTIONARY_PUMP_FILTER_SUCCESS
} from '../actions/dictionary';

import {
  INFORMATION_WELL_SUCCESS,
  INFORMATION_TICKET_DETAIL_SUCCESS,
  INFORMATION_TICKET_DETAIL_GROUP_SUCCESS
} from '../actions/information';

export function cacheReducer(state = Immutable.fromJS(initialState.cache), action) {
  switch (action.type) {
    case INFORMATION_WELL_SUCCESS:
      return state.setIn(['information', 'wells', action.payload.id.toString()], Immutable.fromJS(action.payload));
    case INFORMATION_TICKET_DETAIL_SUCCESS:
      return state.setIn(['information', 'ticketDetails', action.payload.ticket_id.toString()], Immutable.fromJS(action.payload));
    case INFORMATION_TICKET_DETAIL_GROUP_SUCCESS: {
      let output = state;
      action.payload.parameters.forEach(x => output = output.setIn(['information', 'ticketDetails', x.id.toString()], Immutable.fromJS(x)));
      return output;
    }
    case DICTIONARY_PUMP_FILTER_SUCCESS:
      return state.setIn(['information', 'pumps', action.payload.id.toString()], Immutable.fromJS(action.payload));
    default:
      return state;
  }
}