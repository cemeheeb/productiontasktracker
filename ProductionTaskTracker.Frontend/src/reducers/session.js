import Immutable from 'immutable';

import { initialState } from './initial-state';
import { trimDetails } from '../constants/configuration';

import { mapRouteToDictionary } from '../utilities/classifiers';

import {
  LOGIN_SUCCESS,
  LOGOUT
} from '../actions/authorization';

import {
  INCOMING_UNREADED_FILTER_SUCCESS
} from '../actions/incoming';

import {
  INFORMATION_TICKET_DETAIL_SUCCESS,
  INFORMATION_TICKET_DETAIL_GROUP_SUCCESS,
  INFORMATION_TICKET_DETAIL_VISIT_PUSH_SUCCESS
} from '../actions/information';

import {
  WIZARD_REQUEST_SHOW,
  WIZARD_RESPONSE_SHOW,
  WIZARD_CONTROLLED_SHOW,
  WIZARD_WELL_SHOW,
  WIZARD_RESET,
  WIZARD_SET_HISTORY
} from '../actions/wizard';

import {
  DICTIONARY_PUMP_SEARCH_FILTER_REQUEST,
  DICTIONARY_PUMP_SEARCH_FILTER_SUCCESS,
  DICTIONARY_WELL_FILTER_REQUEST,
  DICTIONARY_WELL_FILTER_SUCCESS,
  DICTIONARY_FIELD_FILTER_REQUEST,
  DICTIONARY_FIELD_FILTER_SUCCESS,
  DICTIONARY_CLUSTER_FILTER_REQUEST,
  DICTIONARY_CLUSTER_FILTER_SUCCESS,
  DICTIONARY_MEASURE_FACILITY_POSITIONS_REQUEST,
  DICTIONARY_MEASURE_FACILITY_POSITIONS_SUCCESS,
  DICTIONARY_CLASSIFIER_SUCCESS
} from '../actions/dictionary';

import {
  SESSION_DATE_BEGIN_CHANGE,
  SESSION_DATE_END_CHANGE,
  SESSION_PAGE_CHANGE,
  SESSION_HISTORY_DATE_BEGIN_CHANGE,
  SESSION_HISTORY_DATE_END_CHANGE
} from '../actions/session';

import extractDepartmentsBranch from '../utilities/extract-departments-branch';

export function sessionReducer(state = Immutable.fromJS(initialState.session), action) {
  // SUCCESS
  // FAILURE
  const actionTypePostfix = action.type.substr(-7);
  switch (actionTypePostfix) {
    case "REQUEST":
      state = state.merge(Immutable.fromJS({
        requestCount: state.get("requestCount") + 1
      }));
      break;
    case "SUCCESS":
    case "FAILURE":
      state = state.merge(Immutable.fromJS({
        requestCount: state.get("requestCount") - 1
      }));
      break;
  }

  switch (action.type) {
    case LOGIN_SUCCESS: {
      return state.merge(
        Immutable.fromJS({
          token: action.payload.access_token,
          membershipID: action.payload.membership_id,
          departmentIDs: extractDepartmentsBranch(action.payload.department_id, JSON.parse(action.payload.departments)),
          positionName: action.payload.position_name,
          positionRolename: action.payload.position_rolename,
          memberships: JSON.parse(action.payload.memberships),
          membershipRolenames: JSON.parse(action.payload.membership_rolenames),
          departments: JSON.parse(action.payload.departments),
          routes: JSON.parse(action.payload.routes)
        })
      );
    }
    case LOGOUT:
      return Immutable.fromJS(initialState.session);
    case SESSION_DATE_BEGIN_CHANGE:
      return state.set('dateBegin', action.payload);
    case SESSION_DATE_END_CHANGE:
      return state.set('dateEnd', action.payload);
    case SESSION_PAGE_CHANGE:
      return state.set('page', action.payload);
    case SESSION_HISTORY_DATE_BEGIN_CHANGE:
      return state.set('historyDateBegin', action.payload);
    case SESSION_HISTORY_DATE_END_CHANGE:
      return state.set('historyDateEnd', action.payload);
    case INFORMATION_TICKET_DETAIL_SUCCESS:
      return state.set('notifications',
        Immutable.List(state.get('notifications').filter(x => x.id !== action.payload.id))
      );
    case INFORMATION_TICKET_DETAIL_GROUP_SUCCESS:
      return state.set('notifications',
        Immutable.List(state.get('notifications').filter(x => {
          let founded = false;
          action.payload.parameters.forEach(parameter => founded = founded || parameter.id == x.id);
          return founded;
        }))
      );
    case INFORMATION_TICKET_DETAIL_VISIT_PUSH_SUCCESS: {
      const filtered = state.get('notifications').filter(x => action.payload.indexOf(x.ticket_movement_id) < 0);
      return state.set('notifications', Immutable.List(filtered));
    }
    case INCOMING_UNREADED_FILTER_SUCCESS:
      return state.set('notifications', Immutable.List(action.payload));
    case WIZARD_REQUEST_SHOW:
      return state
        .set('wizardHistory', Immutable.fromJS([0]))
        .setIn(['wizardRequest', 'isVisible'], true);
    case WIZARD_RESPONSE_SHOW:
      if (typeof action.ticketDetail.parameters !== 'undefined') {
        action.ticketDetail.parameters.forEach((x) => trimDetails(x));
      }
      return state
        .set('wizardHistory', Immutable.fromJS([0]))
        .setIn(['wizardResponse', 'ticketDetail'],
          Immutable.fromJS(
            Object.assign({},
              action.ticketDetail,
              { behaviour: typeof action.ticketDetail.parameters !== 'undefined' && action.ticketDetail.parameters.length > 1 ? 'Group' : 'Single' }
            )
          )
        )
        .setIn(['wizardResponse', 'isVisible'], true);
    case WIZARD_CONTROLLED_SHOW:
      return state
        .set('wizardHistory', Immutable.fromJS([0]))
        .setIn(['wizardControlled', 'isVisible'], true);
    case WIZARD_WELL_SHOW:
      return state
        .set('wizardHistory', Immutable.fromJS([0]))
        .setIn(['wizardWell', 'isVisible'], true);
    case WIZARD_RESET:
      return state
        .set('wizardResponse', Immutable.fromJS({ isVisible: false }))
        .set('wizardRequest', Immutable.fromJS({ isVisible: false }))
        .set('wizardControlled', Immutable.fromJS({ isVisible: false }))
        .set('wizardWell', Immutable.fromJS({ isVisible: false }));
    case WIZARD_SET_HISTORY:
      return state
        .set('wizardHistory', Immutable.fromJS(action.payload));
    case DICTIONARY_PUMP_SEARCH_FILTER_REQUEST:
      return state
        .set('isPumpsLoading', true);
    case DICTIONARY_PUMP_SEARCH_FILTER_SUCCESS:
      return state
        .set('pumps', action.payload)
        .set('isPumpsLoading', false);
    case DICTIONARY_WELL_FILTER_REQUEST:
      return state
        .set('isWellsLoading', true);
    case DICTIONARY_WELL_FILTER_SUCCESS:
      return state
        .set('wells', action.payload.data)
        .set('isWellsLoading', false);
    case DICTIONARY_FIELD_FILTER_REQUEST:
      return state
        .set('isFieldsLoading', true);
    case DICTIONARY_FIELD_FILTER_SUCCESS:
      return state
        .set('fields', action.payload)
        .set('isFieldsLoading', false);
    case DICTIONARY_CLUSTER_FILTER_REQUEST:
      return state
        .set('isClustersLoading', true);
    case DICTIONARY_CLUSTER_FILTER_SUCCESS:
      return state
        .set('clusters', action.payload)
        .set('isClustersLoading', false);
    case DICTIONARY_MEASURE_FACILITY_POSITIONS_REQUEST:
      return state
        .set('isMeasureFacilityPositionsLoading', true);
    case DICTIONARY_MEASURE_FACILITY_POSITIONS_SUCCESS:
      return state
        .set('measureFacilityPositions', action.payload)
        .set('isMeasureFacilityPositionsLoading', false);
    case DICTIONARY_CLASSIFIER_SUCCESS:
      return state
        .setIn(['dictionaries', mapRouteToDictionary(action.payload.dictionary), "data"], action.payload.data);
    default:
      return state;
  }
}