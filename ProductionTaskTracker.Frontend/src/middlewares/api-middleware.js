import Raven from 'raven-js';

import { API_ROOT } from '../constants/configuration';
import { selectSessionToken } from '../selectors/session';
import { LOGOUT } from '../actions/authorization';

export const API_REQUEST = 'API_REQUEST';

function request(endpoint, options, parameters) {
  let url = `${API_ROOT}/${endpoint}`;
  if (parameters) {
    let searchParameters = new URLSearchParams();
    for (let parameter in parameters) {
      if (typeof parameters[parameter] !== 'undefined' && parameters[parameter] !== null) {
        if (parameters[parameter].constructor.name == "Array") {
          parameters[parameter].forEach((value) => {
            searchParameters.append(parameter, value);
          });
        } else {
          searchParameters.append(parameter, parameters[parameter]);
        }
      }
    }

    url = `${url}?${searchParameters}`;
  }

  return fetch(url, options)
    .then(response =>
      response.json()
        .then(data => {
          if (response.ok) {
            return data;
          }

          if (response.status === 401) {
            // TODO: Try to refresh jwt token here or reject with
          }

          if (data.error === "invalid_client") {
            return Promise.reject({message: "Неверное имя пользователя или пароль", status: 401});  
          }

          return Promise.reject({message: data.Message, status: response.status});
        })
    , 
    error => {
      if (error.message === "NetworkError when attempting to fetch resource.") {
        return Promise.reject({message: "Удаленный сервер - недоступен", status: 404});
      }

      return Promise.reject({message: error.message, status: 401});
    });
}

const pendingRequestMap = new Set();

// Прослойка
export const apiMiddleware = store => next => action => {
  const context = action[API_REQUEST];
  if (typeof context === 'undefined') {
    return next(action);
  }

  const { endpoint, payload: { options, parameters }, types } = context;

  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('Expected an array of three action types.');
  }

  if (!types.every(type => typeof type === 'string')) {
    throw new Error('Expected action types to be strings.');
  }

  const [requestType, successType, failureType] = types;

  const actionWith = data => {
    const finalAction = Object.assign({}, action, data);
    delete finalAction[API_REQUEST];
    return finalAction;
  };

  next(actionWith({ type: requestType }));

  if (pendingRequestMap.has(types[0])) {
    const actionNext = actionWith({
      type: failureType,
      error: 'Повторный вызов операции до его выполнения. Дождитесь завершения текущей операции или перезагрузите страницу',
      status
    });
    return new Promise(() => {
      return next(
        actionNext
      );
    });
  } else {
    pendingRequestMap.add(types[0]);
  }

  let finalOptions = options;
  const token = selectSessionToken(store.getState());
  if (token) {
    finalOptions = {... options, 
      headers: { ... options.headers,
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${token}`
      }
    };
  }

  return request(endpoint, finalOptions, parameters).then(
    response => {
      pendingRequestMap.delete(types[0]);
      return next(
        actionWith({
          type: successType,
          payload: response
        })
      );
    },
    error => {
      Raven.captureException(error, {
        extra: {
          action,
          state: store.getState()
        }
      });

      next({
        type: '@ReduxToastr/toastr/ADD',
        payload: {
          type: 'error',
          position: 'bottom-center',
          options: {
            removeOnHover: true,
            showCloseButton: true
          },
          title: 'Ошибка',
          message: `${error.message}`
        }
      });

      const status = error.status;
      if (status === 401) {
        next({type: LOGOUT });
      }

      pendingRequestMap.delete(types[0]);
      return next(
        actionWith({
          type: failureType,
          error: error.message || 'Не удалось выполнить HTTP запрос',
          status
        })
      );
    }
  );
};