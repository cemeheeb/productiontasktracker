export const loggerMiddleware = store => next => action => {
  console.log('Dispatching', action); // eslint-disable-line no-console
  let result = next(action);
  console.log('Next state', store.getState()); // eslint-disable-line no-console
  return result;
};