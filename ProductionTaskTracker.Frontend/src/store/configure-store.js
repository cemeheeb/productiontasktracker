import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import { autoRehydrate } from 'redux-persist-immutable';
import thunk from 'redux-thunk';
import Raven from "raven-js";
import createRavenMiddleware from "raven-for-redux";

import rootReducer from '../reducers';
import { apiMiddleware } from '../middlewares/api-middleware';
import { loggerMiddleware } from '../middlewares/logger-middleware';

Raven.config('https://07b964a8f916427fb7af4cf19c86b265@sentry.io/281291').install();

function configureStoreProduction(initialState, history) {
  const middlewares = [
    createRavenMiddleware(Raven),
    thunk,
    apiMiddleware,
    loadingBarMiddleware({ promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAILURE'], }),
    routerMiddleware(history)
  ];

  return createStore(rootReducer, initialState, 
    compose(
      autoRehydrate(),
      applyMiddleware(...middlewares)
    )
  );
}

function configureStoreDevelopment(initialState, history) {
  const middlewares = [
    createRavenMiddleware(Raven),
    thunk,
    apiMiddleware,
    loadingBarMiddleware({ promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAILURE'], }),
    routerMiddleware(history),
    loggerMiddleware
  ];

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(rootReducer, initialState, 
    composeEnhancers(
      autoRehydrate(),
      applyMiddleware(...middlewares)
    )
  );

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

export const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProduction : configureStoreDevelopment;