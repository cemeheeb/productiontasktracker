import PropTypes from 'prop-types';
import React from 'react';

import { bindActionCreators } from 'redux';
import { connect, Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import { getFormValues, formValueSelector } from 'redux-form/immutable';
import Immutable from "immutable";
import moment from 'moment';

// Actions
import {
  actionLogout
} from '../actions/authorization';

import {
  actionSettingsSet,
  actionSettingsReset
} from '../actions/settings';

import {
  actionResetForm,
  actionChangeForm,
  actionChangeFormFieldArray,
  actionInitializeForm,
  actionArrayPush
} from '../actions/form';

import {
  actionDictionaryWellFilter,
  actionDictionaryDepartmentsWellFilter,
  actionDictionaryFieldFilter,
  actionDictionaryFieldMeasureFacilityFilter,
  actionDictionaryClusterFilter,
  actionDictionaryPumpFilter,
  actionDictionaryPumpSearchFilter,
  actionDictionaryContractorFilter,
  actionDictionaryContractorAdd,
  actionDictionaryContractorSearchFilter,
  actionDictionaryMeasureFacilityPositionsFilter,
  actionDictionaryClassifierFilter,
  actionDictionaryClassifierAdd,
  actionDictionaryClassifierUpdate
} from '../actions/dictionary';

import {
  actionIncomingFilter,
  actionIncomingUnreadedFilter,
  actionIncomingTicketMovement,
  actionIncomingTicketGroupMovement
} from '../actions/incoming';

import {
  actionControlledFilter,
  actionControlledCreate,
  actionControlledUpdate,
  actionControlledDelete
} from '../actions/controlled';

import {
  actionWellCreate
} from '../actions/wells';

import {
  actionInformationWell,
  actionInformationField,
  actionInformationTicketDetail,
  actionInformationTicketDetailVisits,
  actionInformationTicketDetailVisitPush,
  actionInformationTicketHistory,
  actionInformationTicketHistoryGroup
} from '../actions/information';

import {
  actionOutcomingFilter,
  actionTicketRegistration,
  actionTicketRegistrationGroup
} from '../actions/outcoming';

import {
  actionExportDocument,
  actionImportUpload
} from '../actions/import';

import {
  actionHistoryFilter
} from '../actions/history';

import {
  actionReportMakeA,
  actionReportMakeB
} from '../actions/reports';

import {
  actionWizardRequest,
  actionWizardResponse,
  actionWizardControlled,
  actionWizardWell,
  actionWizardReset,
  actionWizardSetHistory
} from '../actions/wizard';

import {
  actionSessionDateBeginChange,
  actionSessionDateEndChange,
  actionSessionHistoryDateBeginChange,
  actionSessionHistoryDateEndChange,
  actionSessionPageChange
} from '../actions/session';

import {
  actionAttachmentsGet,
  actionAttachmentsUpload,
  actionAttachmentsTicketDetailUpload
} from '../actions/attachments';

import {
  actionPumpImport
} from '../actions/pumps';

import {
  actionMembershipFilter,
  actionMembershipAdd,
  actionMembershipUpdate,
  actionDepartmentFilter,
  actionDepartmentUpdate,
  actionDepartmentRemove,
  actionPositionFilter,
  actionPositionUpdate,
  actionPositionRemove,
  actionWorkPeriodFilter,
  actionWorkPeriodCreate,
  actionWorkPeriodRemove
} from '../actions/administrative';

import {
  actionToastrPush
} from '../actions/toastr';

import {
  actionAutocompleteOptionFilter,
  actionAutocompleteOptionRemove
} from '../actions/autocomplete';

// Props
import {
  settingsSelector
} from '../selectors/settings';

import {
  sessionSelector,
  selectSessionWizardRequest,
  selectSessionWizardResponse,
  selectSessionWizardControlled,
  selectSessionWizardWell
} from '../selectors/session';

// Components
import { Layout } from '../components/layout';

import { B10 } from '../constants/ticket-types';

class RootInternal extends React.Component {
  render() {
    const { store, history, settings, session, actions } = this.props;
    const props = {
      settings,
      session,
      actions
    };

    return (
      <div style={{ height: '100%' }}>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <Layout {...props} />
          </ConnectedRouter>
        </Provider>
      </div>
    );
  }
}

RootInternal.propTypes = {
  settings: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
};

const wizardControlledFormSelector = formValueSelector('wizard-controlled');
const wizardWellFormSelector = formValueSelector('wizard-well');

function mapStateToProps(state) { // eslint-disable-line no-unused-vars
  const wizardRequest = selectSessionWizardRequest(state).toJS();
  const wizardRequestFormValuesImmutable = getFormValues('wizard-request')(state);
  const wizardRequestInputData = typeof wizardRequestFormValuesImmutable !== 'undefined' ? wizardRequestFormValuesImmutable.toJS() : {};

  const wizardResponse = selectSessionWizardResponse(state).toJS();
  const wizardResponseFormValuesImmutable = getFormValues('wizard-response')(state);
  const wizardResponseInputData = typeof wizardResponseFormValuesImmutable !== 'undefined' ? wizardResponseFormValuesImmutable.toJS() : {};

  const wizardControlled = selectSessionWizardControlled(state).toJS();
  const wizardWell = selectSessionWizardWell(state).toJS();

  const administrateDepartmentFormValuesImmutable = getFormValues('administrate-department')(state);
  const administrateDepartmentInputData = typeof administrateDepartmentFormValuesImmutable !== 'undefined' ? administrateDepartmentFormValuesImmutable.toJS() : {};
  
  const administratePositionFormValuesImmutable = getFormValues('administrate-position')(state);
  const administratePositionInputData = typeof administratePositionFormValuesImmutable !== 'undefined' ? administratePositionFormValuesImmutable.toJS() : {};

  const administratePositionAssignedFormValuesImmutable = getFormValues('administrate-position-assigned')(state);
  const administratePositionAssignedInputData = typeof administratePositionAssignedFormValuesImmutable !== 'undefined' ? administratePositionAssignedFormValuesImmutable.toJS() : {};

  const administrateMembershipFormValuesImmutable = getFormValues('administrate-membership')(state);
  const administrateMembershipInputData = typeof administrateMembershipFormValuesImmutable !== 'undefined' ? administrateMembershipFormValuesImmutable.toJS() : {};

  const administrateDictionaryFormValuesImmutable = getFormValues('administrate-dictionary')(state);
  const administrateDictionaryInputData = typeof administrateDictionaryFormValuesImmutable !== 'undefined' ? administrateDictionaryFormValuesImmutable.toJS() : {};

  const session = Object.assign(
    sessionSelector(state)
      .set('dateBegin', moment(sessionSelector(state).get('dateBegin')))
      .set('dateEnd', moment(sessionSelector(state).get('dateEnd')))
      .set('historyDateBegin', moment(sessionSelector(state).get('historyDateBegin')))
      .set('historyDateEnd', moment(sessionSelector(state).get('historyDateEnd')))
      .toJS(), {
      wizardRequest: {
        ticketDetail: wizardRequest.ticketDetail,
        isMeasureFacility: typeof wizardRequestInputData !== 'undefined' && wizardRequestInputData.ticket_type === B10,
        inputData: wizardRequestInputData,
        isVisible: wizardRequest.isVisible
      },
      wizardResponse: {
        ticketDetail: wizardResponse.ticketDetail,
        isMeasureFacility: typeof wizardResponseInputData !== 'undefined' && wizardResponseInputData.ticket_type === B10,
        inputData: wizardResponseInputData,
        isVisible: wizardResponse.isVisible
      },
      wizardControlled: {
        inputData: Immutable.Map({
          id: wizardControlledFormSelector(state, 'id'),
          well_id: wizardControlledFormSelector(state, 'well_id'),
          ticket_type: wizardControlledFormSelector(state, 'ticket_type'),
          department_id: wizardControlledFormSelector(state, 'department_id'),
          repair_type: wizardControlledFormSelector(state, 'repair_type'),
          event_type: wizardControlledFormSelector(state, 'event_type'),
          pump_id: wizardControlledFormSelector(state, 'pump_id'),
          pump_depth: wizardControlledFormSelector(state, 'pump_depth')
        }).toJS(),
        isVisible: wizardControlled.isVisible
      },
      wizardWell: {
        inputData: Immutable.Map({
          id: wizardWellFormSelector(state, 'id'),
          shop: wizardWellFormSelector(state, 'shop'),
          field: wizardWellFormSelector(state, 'field'),
          cluster: wizardWellFormSelector(state, 'cluster'),
          code: wizardWellFormSelector(state, 'code')
        }).toJS(),
        isVisible: wizardWell.isVisible
      },
      administrateDictionaryInputData,
      administratePositionInputData,
      administratePositionAssignedInputData,
      administrateMembershipInputData,
      administrateDepartmentInputData
    });

  return {
    settings: settingsSelector(state).toJS(),
    session
  };
}

function mapDispatchToProps(dispatch) { // eslint-disable-line no-unused-vars
  return ({
    actions: {
      // Внутренние механизмы (Инфраструктура)
      actionLogout: bindActionCreators(actionLogout, dispatch),
      actionResetForm: bindActionCreators(actionResetForm, dispatch),
      actionChangeForm: bindActionCreators(actionChangeForm, dispatch),
      actionChangeFormFieldArray: bindActionCreators(actionChangeFormFieldArray, dispatch),
      actionInitializeForm: bindActionCreators(actionInitializeForm, dispatch),
      actionArrayPush: bindActionCreators(actionArrayPush, dispatch),
      actionSettingsSet: bindActionCreators(actionSettingsSet, dispatch),
      actionSettingsReset: bindActionCreators(actionSettingsReset, dispatch),
      // Справочники
      actionDictionaryWellFilter: bindActionCreators(actionDictionaryWellFilter, dispatch),
      actionDictionaryDepartmentsWellFilter: bindActionCreators(actionDictionaryDepartmentsWellFilter, dispatch),
      actionDictionaryFieldFilter: bindActionCreators(actionDictionaryFieldFilter, dispatch),
      actionDictionaryFieldMeasureFacilityFilter: bindActionCreators(actionDictionaryFieldMeasureFacilityFilter, dispatch),
      actionDictionaryClusterFilter: bindActionCreators(actionDictionaryClusterFilter, dispatch),
      actionDictionaryPumpFilter: bindActionCreators(actionDictionaryPumpFilter, dispatch),
      actionDictionaryPumpSearchFilter: bindActionCreators(actionDictionaryPumpSearchFilter, dispatch),
      actionDictionaryContractorFilter: bindActionCreators(actionDictionaryContractorFilter, dispatch),
      actionDictionaryContractorSearchFilter: bindActionCreators(actionDictionaryContractorSearchFilter, dispatch),
      actionDictionaryContractorAdd: bindActionCreators(actionDictionaryContractorAdd, dispatch),
      actionDictionaryMeasureFacilityPositionsFilter: bindActionCreators(actionDictionaryMeasureFacilityPositionsFilter, dispatch),
      actionDictionaryClassifierFilter: bindActionCreators(actionDictionaryClassifierFilter, dispatch),
      actionDictionaryClassifierAdd: bindActionCreators(actionDictionaryClassifierAdd, dispatch),
      actionDictionaryClassifierUpdate: bindActionCreators(actionDictionaryClassifierUpdate, dispatch),
      // Подразделения
      actionDepartmentFilter: bindActionCreators(actionDepartmentFilter, dispatch),
      actionDepartmentUpdate: bindActionCreators(actionDepartmentUpdate, dispatch),
      actionDepartmentRemove: bindActionCreators(actionDepartmentRemove, dispatch),
      // Должности
      actionPositionFilter: bindActionCreators(actionPositionFilter, dispatch),
      actionPositionUpdate: bindActionCreators(actionPositionUpdate, dispatch),
      actionPositionRemove: bindActionCreators(actionPositionRemove, dispatch),
      // Автокомплит
      actionAutocompleteOptionFilter: bindActionCreators(actionAutocompleteOptionFilter, dispatch),
      actionAutocompleteOptionRemove: bindActionCreators(actionAutocompleteOptionRemove, dispatch),
      // Информация
      actionInformationWell: bindActionCreators(actionInformationWell, dispatch),
      actionInformationField: bindActionCreators(actionInformationField, dispatch),
      actionInformationTicketDetail: bindActionCreators(actionInformationTicketDetail, dispatch),
      actionInformationTicketDetailVisits: bindActionCreators(actionInformationTicketDetailVisits, dispatch),
      actionInformationTicketDetailVisitPush: bindActionCreators(actionInformationTicketDetailVisitPush, dispatch),
      actionInformationTicketHistory: bindActionCreators(actionInformationTicketHistory, dispatch),
      actionInformationTicketHistoryGroup: bindActionCreators(actionInformationTicketHistoryGroup, dispatch),
      // Входящие
      actionIncomingFilter: bindActionCreators(actionIncomingFilter, dispatch),
      actionIncomingUnreadedFilter: bindActionCreators(actionIncomingUnreadedFilter, dispatch),
      actionIncomingTicketMovement: bindActionCreators(actionIncomingTicketMovement, dispatch),
      actionIncomingTicketGroupMovement: bindActionCreators(actionIncomingTicketGroupMovement, dispatch),
      // Исходящие
      actionExportDocument: bindActionCreators(actionExportDocument, dispatch),
      actionImportUpload: bindActionCreators(actionImportUpload, dispatch),
      actionOutcomingFilter: bindActionCreators(actionOutcomingFilter, dispatch),
      actionTicketRegistration: bindActionCreators(actionTicketRegistration, dispatch),
      actionTicketRegistrationGroup: bindActionCreators(actionTicketRegistrationGroup, dispatch),
      // Подконтрольные
      actionControlledFilter: bindActionCreators(actionControlledFilter, dispatch),
      actionControlledCreate: bindActionCreators(actionControlledCreate, dispatch),
      actionControlledUpdate: bindActionCreators(actionControlledUpdate, dispatch),
      actionControlledDelete: bindActionCreators(actionControlledDelete, dispatch),
      // Скважины
      actionWellCreate: bindActionCreators(actionWellCreate, dispatch),
      // История
      actionHistoryFilter: bindActionCreators(actionHistoryFilter, dispatch),
      // Отчеты
      actionReportMakeA: bindActionCreators(actionReportMakeA, dispatch),
      actionReportMakeB: bindActionCreators(actionReportMakeB, dispatch),
      // Вложения
      actionAttachmentsUpload: bindActionCreators(actionAttachmentsUpload, dispatch),
      actionAttachmentsTicketDetailUpload: bindActionCreators(actionAttachmentsTicketDetailUpload, dispatch),
      actionAttachmentsGet: bindActionCreators(actionAttachmentsGet, dispatch),
      // Механизм отображения мастера заявки
      actionWizardRequest: bindActionCreators(actionWizardRequest, dispatch),
      actionWizardResponse: bindActionCreators(actionWizardResponse, dispatch),
      actionWizardControlled: bindActionCreators(actionWizardControlled, dispatch),
      actionWizardWell: bindActionCreators(actionWizardWell, dispatch),
      actionWizardReset: bindActionCreators(actionWizardReset, dispatch),
      actionWizardSetHistory: bindActionCreators(actionWizardSetHistory, dispatch),
      //Администрирование
      actionMembershipFilter: bindActionCreators(actionMembershipFilter, dispatch),
      actionMembershipAdd: bindActionCreators(actionMembershipAdd, dispatch),
      actionMembershipUpdate: bindActionCreators(actionMembershipUpdate, dispatch),
      actionWorkPeriodFilter: bindActionCreators(actionWorkPeriodFilter, dispatch),
      actionWorkPeriodCreate: bindActionCreators(actionWorkPeriodCreate, dispatch),
      actionWorkPeriodRemove: bindActionCreators(actionWorkPeriodRemove, dispatch),
      // Прочие
      actionSessionDateBeginChange: bindActionCreators(actionSessionDateBeginChange, dispatch),
      actionSessionDateEndChange: bindActionCreators(actionSessionDateEndChange, dispatch),
      actionSessionHistoryDateBeginChange: bindActionCreators(actionSessionHistoryDateBeginChange, dispatch),
      actionSessionHistoryDateEndChange: bindActionCreators(actionSessionHistoryDateEndChange, dispatch),
      actionSessionPageChange: bindActionCreators(actionSessionPageChange, dispatch),
      
      actionPumpImport: bindActionCreators(actionPumpImport, dispatch),
      actionToastrPush: bindActionCreators(actionToastrPush, dispatch)
    }
  });
}

export const Root = connect(mapStateToProps, mapDispatchToProps)(RootInternal);