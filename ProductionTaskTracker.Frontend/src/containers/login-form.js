import PropTypes from 'prop-types';
import React from 'react';

import { Field, reduxForm } from 'redux-form/immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { FormField } from '../components/form-field';
import { SelectRedux } from '../components/select-redux';

import { actionLogin, LOGIN_SUCCESS } from '../actions/authorization';
import { actionSettingsSet } from '../actions/settings';
import { actionInitializeForm } from '../actions/form';

import { settingsSelector } from '../selectors/settings';

const validate = values => {
  const errors = {};

  if (!values.get('password')) {
    errors.password = 'Пароль не может быть пустым';
  }

  return errors;
};

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeDomain = this.onChangeDomain.bind(this);

    this.state = {
      domain: null
    };
  }

  componentDidMount() {
    actionInitializeForm('login-form', {... {domain: this.state.domain}});
  }

  handleSubmit(formDataImmutable) {
    const { router: { history } } = this.context;
    const { actionLogin, actionSettingsSet } = this.props;
    const formData = formDataImmutable.toJS();

    const domain = typeof formData.domain !== 'undefined' && formData.domain !== null ? (typeof formData.domain.value !== 'undefined' ? formData.domain.value : formData.domain) : null;
    
    actionLogin(formData.username, formData.password, domain).then(
      (action) => {
        if (action.type === LOGIN_SUCCESS) {
          actionSettingsSet('domain', domain);
          history.push('/tickets/incoming');
        }

        return Promise.resolve(action);
      }
    );
  }

  onChangeDomain(value) {
    this.setState({ domain: value });
  }

  render() {
    const { handleSubmit, submitting } = this.props;
    let iconPreloaderComponent = submitting ? <i className="preloader fa fa-spinner fa-spin" /> : null;

    return (
      <form onSubmit={handleSubmit(this.handleSubmit)} className="login-form m-t">
        <Field name="username" type="text" component={FormField} label="Имя пользователя" placeholder="Введите имя пользователя" icon="membership" />
        <Field name="password" type="password" component={FormField} label="Пароль" placeholder="Введите пароль" icon="lock" />
        <div className="form-group">
          <label>Домен</label>
          <div className="input-group">
            <Field name="domain"
              component={SelectRedux}
              value={this.state.domain}
              onChange={this.onChangeDomain}
              options={[
                { label:'CORP', value:'CORP' }
              ]}
              isClearable
              placeholder="Выберите домен из списка"
            />
          </div>
        </div>
        <div className="form-group text-center">
          <div className="input-group">
            <button type="submit" disabled={submitting} className="btn btn-primary">{iconPreloaderComponent}Войти</button>
          </div>
        </div>
      </form>
    );
  }
}

Form.propTypes = {
  domain: PropTypes.string,
  actionLogin: PropTypes.func.isRequired,
  actionSettingsSet: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired
};

Form.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired
  })
};

const ReduxForm = reduxForm({ form: 'login-form', validate })(Form);

function mapStateToProps(state) {
  return { initialValues: { domain: settingsSelector(state).toJS().domain } };
}

function mapDispatchToProps(dispatch) {
  return { 
    actionLogin: bindActionCreators(actionLogin, dispatch),
    actionSettingsSet: bindActionCreators(actionSettingsSet, dispatch),
    actionInitializeForm: bindActionCreators(actionInitializeForm, dispatch),
  };
}

export const LoginForm = connect(
  mapStateToProps, 
  mapDispatchToProps
)(ReduxForm);