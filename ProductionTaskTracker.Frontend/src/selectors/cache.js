export function cacheInformationWellSelector(state, id) {
  return state.getIn(['cache', 'information', 'wells', id.toString()]);
}

export function cacheInformationContractorSelector(state, id) {
  return state.getIn(['cache', 'information', 'contractors', id.toString()]);
}

export function cacheInformationPumpSelector(state, id) {
  return state.getIn(['cache', 'information', 'pumps', id.toString()]);
}

export function cacheInformationShopSelector(state, code) {
  return state.getIn(['cache', 'information', 'shops', code]);
}

export function cacheInformationFieldSelector(state, code) {
  return state.getIn(['cache', 'information', 'fields', code]);
}

export function cacheInformationDepartmentSelector(state, id) {
  return state.getIn(['cache', 'information', 'departments', id.toString()]);
}

export function cacheInformationTicketDetailSelector(state, id) {
  return state.getIn(['cache', 'information', 'ticketDetails', id.toString()]);
}