export function sessionSelector(state) {
  return state.get('session');
}

export function selectSessionToken(state) {
  return sessionSelector(state).get('token');
}

export function selectSessionWizardRequest(state) {
  return state.getIn(['session', 'wizardRequest']);
}

export function selectSessionWizardResponse(state) {
  return state.getIn(['session', 'wizardResponse']);
}

export function selectSessionWizardControlled(state) {
  return state.getIn(['session', 'wizardControlled']);
}

export function selectSessionWizardWell(state) {
  return state.getIn(['session', 'wizardWell']);
}