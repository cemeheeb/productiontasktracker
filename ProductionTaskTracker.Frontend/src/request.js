import qs from 'querystring';
import { API_ROOT } from './constants/configuration';

export const authorize = function(username, password) {
  return fetch(`${API_ROOT}/token`, {  
    method: 'post', 
    headers: {
      "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
    },
    body: qs.stringify({ grant_type:'password', username, password })
  })
  .then(
    (response) => {
      if(response.ok) {
        return response.json();
      }
    
      throw new Error(response.json());
    }
  );
};

export const request = function(resource, options, body) {
  let uri = `${API_ROOT}${resource}`;
  if (body) {
    uri = `${uri}?${new URLSearchParams(Object.entries(body))}`;
  }

  return fetch(uri, {
    method: (options && typeof options.method !== 'undefined') ? options.method : 'post',
    headers: {
      "Authorization": `Bearer ${options.token}`
    }
  }).then(
    (response) => {
      if(response.ok) {
        return response.json();
      }

      throw Error(response.json());
    }
  ).catch(
    (error) => {
      return Promise.reject(error.response || error);
    }
  );
};
