export default (promise) => {
  let hasAborted = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then(
      value => hasAborted ? reject({ isAborted : true }) : resolve(value),
      error => hasAborted ? reject({ isAborted: true }) : reject(error)
    );
  });

  return {
    promise: wrappedPromise,
    cancel() {
      hasAborted = true;
    },
  };
};