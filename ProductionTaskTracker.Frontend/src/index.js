/* eslint-disable import/default */
// Поддержка IE9+
require('isomorphic-fetch');
require('es6-promise').polyfill();
import 'babel-polyfill';
import 'url-search-params-polyfill';

import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import createBrowserHistory from 'history/createBrowserHistory';
import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { persistStore as createPersistor } from 'redux-persist-immutable';
import 'react-dates/lib/css/_datepicker.css';

import { configureStore } from './store/configure-store';
import { Root } from './containers/root';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import 'font-awesome/scss/font-awesome.scss';
import './styles/styles.scss';

require('./favicon.ico');

moment.locale('ru-RU');

const history = typeof process.env.HOST !== 'undefined' ? createBrowserHistory({basename: `${process.env.HOST}`}) : createBrowserHistory();
const store = configureStore(undefined, history);

const persistor = createPersistor(store, { blacklist: ['form', 'loadingBar', 'toastr'] }, () => {
  ReactDOM.render(
    <Root store={store} persistor={persistor} history={history} />,
    document.getElementById('application')
  );
});

chai.use(chaiImmutable);
