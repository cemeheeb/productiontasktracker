import { 
  API_REQUEST
} from '../middlewares/api-middleware';

import {
  cacheInformationWellSelector,
  cacheInformationShopSelector,
  cacheInformationFieldSelector
} from '../selectors/cache';

export const INFORMATION_WELL_REQUEST = 'INFORMATION_WELL_REQUEST';
export const INFORMATION_WELL_SUCCESS = 'INFORMATION_WELL_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INFORMATION_WELL_FAILURE = 'INFORMATION_WELL_FAILURE'; /* payload: { code, message } */

export const actionInformationWell = (wellID) => (dispatch, getState) => {
  const informationWell = cacheInformationWellSelector(getState(), wellID);
  
  if (typeof(informationWell) !== 'undefined' && informationWell !== null) {
    return Promise.resolve({ payload: informationWell.toJS() });
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_WELL_REQUEST, INFORMATION_WELL_SUCCESS, INFORMATION_WELL_FAILURE ],
      endpoint: `wells/${wellID}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_FIELD_REQUEST = 'INFORMATION_FIELD_REQUEST';
export const INFORMATION_FIELD_SUCCESS = 'INFORMATION_FIELD_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INFORMATION_FIELD_FAILURE = 'INFORMATION_FIELD_FAILURE'; /* payload: { code, message } */

export const actionInformationField = (fieldCode) => (dispatch, getState) => {
  const informationField = cacheInformationFieldSelector(getState(), fieldCode);
  
  if (typeof(informationField) !== 'undefined' && informationField !== null) {
    return Promise.resolve({ payload: informationField.toJS() });
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_FIELD_REQUEST, INFORMATION_FIELD_SUCCESS, INFORMATION_FIELD_FAILURE ],
      endpoint: `fields/${fieldCode}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_SHOP_REQUEST = 'INFORMATION_SHOP_REQUEST';
export const INFORMATION_SHOP_SUCCESS = 'INFORMATION_SHOP_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INFORMATION_SHOP_FAILURE = 'INFORMATION_SHOP_FAILURE'; /* payload: { code, message } */

export const actionInformationShop = (shopCode) => (dispatch, getState) => {
  const informationShop = cacheInformationShopSelector(getState(), shopCode);
  
  if (typeof(informationShop) !== 'undefined' && informationShop !== null) {
    return Promise.resolve({ payload: informationShop.toJS() });
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_SHOP_REQUEST, INFORMATION_SHOP_SUCCESS, INFORMATION_SHOP_FAILURE ],
      endpoint: `shops/${shopCode}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_TICKET_DETAIL_REQUEST = 'INFORMATION_TICKET_DETAIL_REQUEST';
export const INFORMATION_TICKET_DETAIL_SUCCESS = 'INFORMATION_TICKET_DETAIL_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INFORMATION_TICKET_DETAIL_FAILURE = 'INFORMATION_TICKET_DETAIL_FAILURE'; /* payload: { code, message } */
export const INFORMATION_TICKET_DETAIL_IGNORE = 'INFORMATION_TICKET_DETAIL_IGNORE';

export const actionInformationTicketDetail = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_TICKET_DETAIL_REQUEST, INFORMATION_TICKET_DETAIL_SUCCESS, INFORMATION_TICKET_DETAIL_FAILURE ],
      endpoint: `ticket-details/${id}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_TICKET_DETAIL_GROUP_REQUEST = 'INFORMATION_TICKET_DETAIL_GROUP_REQUEST';
export const INFORMATION_TICKET_DETAIL_GROUP_SUCCESS = 'INFORMATION_TICKET_DETAIL_GROUP_SUCCESS'; 
export const INFORMATION_TICKET_DETAIL_GROUP_FAILURE = 'INFORMATION_TICKET_DETAIL_GROUP_FAILURE'; 
export const actionInformationTicketDetailGroup = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [INFORMATION_TICKET_DETAIL_GROUP_REQUEST, INFORMATION_TICKET_DETAIL_GROUP_SUCCESS, INFORMATION_TICKET_DETAIL_GROUP_FAILURE],
      endpoint: `ticket-details/groups/${id}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_TICKET_DETAIL_VISITS_REQUEST = 'INFORMATION_TICKET_DETAIL_VISITS_REQUEST';
export const INFORMATION_TICKET_DETAIL_VISITS_SUCCESS = 'INFORMATION_TICKET_DETAIL_VISITS_SUCCESS'; 
export const INFORMATION_TICKET_DETAIL_VISITS_FAILURE = 'INFORMATION_TICKET_DETAIL_VISITS_FAILURE'; /* payload: { code, message } */

export const actionInformationTicketDetailVisits = (ticketDetailID) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_TICKET_DETAIL_VISITS_REQUEST, INFORMATION_TICKET_DETAIL_VISITS_SUCCESS, INFORMATION_TICKET_DETAIL_VISITS_FAILURE ],
      endpoint: `ticket-details/${ticketDetailID}/visits`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_TICKET_DETAIL_VISIT_PUSH_REQUEST = 'INFORMATION_TICKET_DETAIL_VISIT_PUSH_REQUEST';
export const INFORMATION_TICKET_DETAIL_VISIT_PUSH_SUCCESS = 'INFORMATION_TICKET_DETAIL_VISIT_PUSH_SUCCESS';
export const INFORMATION_TICKET_DETAIL_VISIT_PUSH_FAILURE = 'INFORMATION_TICKET_DETAIL_VISIT_PUSH_FAILURE'; /* payload: { code, message } */

export const actionInformationTicketDetailVisitPush = (ticketDetailIDList) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_TICKET_DETAIL_VISIT_PUSH_REQUEST, INFORMATION_TICKET_DETAIL_VISIT_PUSH_SUCCESS, INFORMATION_TICKET_DETAIL_VISIT_PUSH_FAILURE ],
      endpoint: `ticket-movements/visits`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(ticketDetailIDList)
        }
      }
    }
  });
};

export const INFORMATION_TICKET_HISTORY_REQUEST = 'INFORMATION_TICKET_HISTORY_REQUEST';
export const INFORMATION_TICKET_HISTORY_SUCCESS = 'INFORMATION_TICKET_HISTORY_SUCCESS';
export const INFORMATION_TICKET_HISTORY_FAILURE = 'INFORMATION_TICKET_HISTORY_FAILURE'; /* payload: { code, message } */

export const actionInformationTicketHistory = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ INFORMATION_TICKET_HISTORY_REQUEST, INFORMATION_TICKET_HISTORY_SUCCESS, INFORMATION_TICKET_HISTORY_FAILURE ],
      endpoint: `tickets/${id}/history`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INFORMATION_TICKET_HISTORY_GROUP_REQUEST = 'INFORMATION_TICKET_HISTORY_GROUP_REQUEST';
export const INFORMATION_TICKET_HISTORY_GROUP_SUCCESS = 'INFORMATION_TICKET_HISTORY_GROUP_SUCCESS';
export const INFORMATION_TICKET_HISTORY_GROUP_FAILURE = 'INFORMATION_TICKET_HISTORY_GROUP_FAILURE'; /* payload: { code, message } */

export const actionInformationTicketHistoryGroup = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [INFORMATION_TICKET_HISTORY_REQUEST, INFORMATION_TICKET_HISTORY_SUCCESS, INFORMATION_TICKET_HISTORY_FAILURE],
      endpoint: `ticket-groups/${id}/history`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};
