import moment from 'moment';
import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const OUTCOMING_FILTER_REQUEST = 'OUTCOMING_FILTER_REQUEST';
export const OUTCOMING_FILTER_SUCCESS = 'OUTCOMING_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const OUTCOMING_FILTER_FAILURE = 'OUTCOMING_FILTER_FAILURE'; /* payload: { code, message } */

export const actionOutcomingFilter = (filter, dateBegin, dateEnd, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ OUTCOMING_FILTER_REQUEST, OUTCOMING_FILTER_SUCCESS, OUTCOMING_FILTER_FAILURE ],
      endpoint: "tickets",
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          kind: "outcoming",
          filter,
          date_begin: moment(dateBegin).format(),
          date_end: moment(dateEnd).format(),
          index: index - 1,
          size
        }
      }
    }
  });
};

export const OUTCOMING_TICKET_REGISTRATION_REQUEST = 'OUTCOMING_TICKET_REGISTRATION_REQUEST';
export const OUTCOMING_TICKET_REGISTRATION_SUCCESS = 'OUTCOMING_TICKET_REGISTRATION_SUCCESS';
export const OUTCOMING_TICKET_REGISTRATION_FAILURE = 'OUTCOMING_TICKET_REGISTRATION_FAILURE';

export const actionTicketRegistration = (inputData) => (dispatch) => {
  [
    'start_time',
    'start_time_plan',
    'time_completion'
  ].forEach(x => {
    if (typeof inputData[x] !== 'undefined' && inputData[x] !== null) { 
      inputData[x] = moment(inputData[x], 'DD.MM.YYYY HH:mm').format();
    }
    });
  
  // eslint-disable-next-line no-unused-vars
  const { wells, parameters, ...inputDataPrepared } = inputData; 
  if (typeof wells !== 'undefined' && wells.length > 0) {
    inputDataPrepared.well_id = wells[0].id;
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ OUTCOMING_TICKET_REGISTRATION_REQUEST, OUTCOMING_TICKET_REGISTRATION_SUCCESS, OUTCOMING_TICKET_REGISTRATION_FAILURE ],
      endpoint: "tickets",
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(inputDataPrepared)
        }
      }
    }
  });
};

export const OUTCOMING_TICKET_REGISTRATION_GROUP_REQUEST = 'OUTCOMING_TICKET_REGISTRATION_GROUP_REQUEST';
export const OUTCOMING_TICKET_REGISTRATION_GROUP_SUCCESS = 'OUTCOMING_TICKET_REGISTRATION_GROUP_SUCCESS';
export const OUTCOMING_TICKET_REGISTRATION_GROUP_FAILURE = 'OUTCOMING_TICKET_REGISTRATION_GROUP_FAILURE';

export const actionTicketRegistrationGroup = (inputData) => (dispatch) => {
  inputData.parameters.forEach(parameter => {
    [
      'start_time',
      'start_time_plan',
      'time_completion'
    ].forEach(x => {
      if (typeof parameter[x] !== 'undefined' && parameter[x] !== null) {
        parameter[x] = moment(parameter[x], 'DD.MM.YYYY HH:mm').format();
      }
    });
  });

  return dispatch({
    [API_REQUEST]: {
      types: [OUTCOMING_TICKET_REGISTRATION_GROUP_REQUEST, OUTCOMING_TICKET_REGISTRATION_GROUP_SUCCESS, OUTCOMING_TICKET_REGISTRATION_GROUP_FAILURE],
      endpoint: "tickets/group",
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(inputData)
        }
      }
    }
  });
};