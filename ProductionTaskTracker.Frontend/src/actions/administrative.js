import {
  API_REQUEST
} from '../middlewares/api-middleware';

export const ADMINISTRATIVE_MEMBERSHIP_FILTER_REQUEST = 'ADMINISTRATIVE_MEMBERSHIP_FILTER_REQUEST';
export const ADMINISTRATIVE_MEMBERSHIP_FILTER_SUCCESS = 'ADMINISTRATIVE_MEMBERSHIP_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const ADMINISTRATIVE_MEMBERSHIP_FILTER_FAILURE = 'ADMINISTRATIVE_MEMBERSHIP_FILTER_FAILURE'; /* payload: { code, message } */

export const actionMembershipFilter = (index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_MEMBERSHIP_FILTER_REQUEST, ADMINISTRATIVE_MEMBERSHIP_FILTER_SUCCESS, ADMINISTRATIVE_MEMBERSHIP_FILTER_FAILURE],
      endpoint: 'administrative/memberships',
      payload: {
        options: {
          method: 'get',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        },
        parameters: {
          index,
          size
        }
      }
    }
  });
};

export const ADMINISTRATIVE_MEMBERSHIP_ADD_REQUEST = 'ADMINISTRATIVE_MEMBERSHIP_ADD_REQUEST';
export const ADMINISTRATIVE_MEMBERSHIP_ADD_SUCCESS = 'ADMINISTRATIVE_MEMBERSHIP_ADD_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_MEMBERSHIP_ADD_FAILURE = 'ADMINISTRATIVE_MEMBERSHIP_ADD_FAILURE'; /* payload: { code, message } */

export const actionMembershipAdd = (data) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_MEMBERSHIP_ADD_REQUEST, ADMINISTRATIVE_MEMBERSHIP_ADD_SUCCESS, ADMINISTRATIVE_MEMBERSHIP_ADD_FAILURE],
      endpoint: `administrative/memberships`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(data)
        }
      }
    }
  });
};

export const ADMINISTRATIVE_MEMBERSHIP_UPDATE_REQUEST = 'ADMINISTRATIVE_MEMBERSHIP_UPDATE_REQUEST';
export const ADMINISTRATIVE_MEMBERSHIP_UPDATE_SUCCESS = 'ADMINISTRATIVE_MEMBERSHIP_UPDATE_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_MEMBERSHIP_UPDATE_FAILURE = 'ADMINISTRATIVE_MEMBERSHIP_UPDATE_FAILURE'; /* payload: { code, message } */

export const actionMembershipUpdate = (data) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_MEMBERSHIP_UPDATE_REQUEST, ADMINISTRATIVE_MEMBERSHIP_UPDATE_SUCCESS, ADMINISTRATIVE_MEMBERSHIP_UPDATE_FAILURE],
      endpoint: `administrative/memberships`,
      payload: {
        options: {
          method: 'put',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(data)
        }
      }
    }
  });
};

export const ADMINISTRATIVE_DEPARTMENT_FILTER_REQUEST = 'ADMINISTRATIVE_DEPARTMENT_FILTER_REQUEST';
export const ADMINISTRATIVE_DEPARTMENT_FILTER_SUCCESS = 'ADMINISTRATIVE_DEPARTMENT_FILTER_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_DEPARTMENT_FILTER_FAILURE = 'ADMINISTRATIVE_DEPARTMENT_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDepartmentFilter = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_DEPARTMENT_FILTER_REQUEST, ADMINISTRATIVE_DEPARTMENT_FILTER_SUCCESS, ADMINISTRATIVE_DEPARTMENT_FILTER_FAILURE],
      endpoint: `administrative/departments`,
      payload: {
        options: {
          method: 'get',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        }
      }
    }
  });
};

export const ADMINISTRATIVE_DEPARTMENT_UPDATE_REQUEST = 'ADMINISTRATIVE_DEPARTMENT_UPDATE_REQUEST';
export const ADMINISTRATIVE_DEPARTMENT_UPDATE_SUCCESS = 'ADMINISTRATIVE_DEPARTMENT_UPDATE_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_DEPARTMENT_UPDATE_FAILURE = 'ADMINISTRATIVE_DEPARTMENT_UPDATE_FAILURE'; /* payload: { code, message } */

export const actionDepartmentUpdate = ({ id, full_name, display_name, shop_code }) => (dispatch) => {
  //const idNumber = id.substring(1)(0, 1);
  console.error('>>>>>>>', { id, full_name, display_name, shop_code });
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_DEPARTMENT_UPDATE_REQUEST, ADMINISTRATIVE_DEPARTMENT_UPDATE_SUCCESS, ADMINISTRATIVE_DEPARTMENT_UPDATE_FAILURE],
      endpoint: `administrative/departments/${id.substring(1)}`,
      payload: {
        options: {
          method: 'put',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify({
            id: id.substring(1),
            full_name,
            display_name,
            shop_code
          })
        }
      }
    }
  });
};

export const ADMINISTRATIVE_DEPARTMENT_MOVE_REQUEST = 'ADMINISTRATIVE_DEPARTMENT_MOVE_REQUEST';
export const ADMINISTRATIVE_DEPARTMENT_MOVE_SUCCESS = 'ADMINISTRATIVE_DEPARTMENT_MOVE_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_DEPARTMENT_MOVE_FAILURE = 'ADMINISTRATIVE_DEPARTMENT_MOVE_FAILURE'; /* payload: { code, message } */

export const actionDepartmentMove = (id, parent_id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_DEPARTMENT_MOVE_REQUEST, ADMINISTRATIVE_DEPARTMENT_MOVE_SUCCESS, ADMINISTRATIVE_DEPARTMENT_MOVE_FAILURE],
      endpoint: `administrative/departments/${id}`,
      payload: {
        options: {
          method: 'put',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify({
            id,
            parent_id
          })
        }
      }
    }
  });
};

export const ADMINISTRATIVE_DEPARTMENT_REMOVE_REQUEST = 'ADMINISTRATIVE_DEPARTMENT_REMOVE_REQUEST';
export const ADMINISTRATIVE_DEPARTMENT_REMOVE_SUCCESS = 'ADMINISTRATIVE_DEPARTMENT_REMOVE_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_DEPARTMENT_REMOVE_FAILURE = 'ADMINISTRATIVE_DEPARTMENT_REMOVE_FAILURE'; /* payload: { code, message } */

export const actionDepartmentRemove = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_DEPARTMENT_REMOVE_REQUEST, ADMINISTRATIVE_DEPARTMENT_REMOVE_SUCCESS, ADMINISTRATIVE_DEPARTMENT_REMOVE_FAILURE],
      endpoint: `administrative/departments/${id}`,
      payload: {
        options: {
          method: 'DELETE'
        }
      }
    }
  });
};

export const ADMINISTRATIVE_POSITION_FILTER_REQUEST = 'ADMINISTRATIVE_POSITION_FILTER_REQUEST';
export const ADMINISTRATIVE_POSITION_FILTER_SUCCESS = 'ADMINISTRATIVE_POSITION_FILTER_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_POSITION_FILTER_FAILURE = 'ADMINISTRATIVE_POSITION_FILTER_FAILURE'; /* payload: { code, message } */

export const actionPositionFilter = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_POSITION_FILTER_REQUEST, ADMINISTRATIVE_POSITION_FILTER_SUCCESS, ADMINISTRATIVE_POSITION_FILTER_FAILURE],
      endpoint: `administrative/positions`,
      payload: {
        options: {
          method: 'get',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        }
      }
    }
  });
};

export const ADMINISTRATIVE_POSITION_UPDATE_REQUEST = 'ADMINISTRATIVE_POSITION_UPDATE_REQUEST';
export const ADMINISTRATIVE_POSITION_UPDATE_SUCCESS = 'ADMINISTRATIVE_POSITION_UPDATE_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_POSITION_UPDATE_FAILURE = 'ADMINISTRATIVE_POSITION_UPDATE_FAILURE'; /* payload: { code, message } */

export const actionPositionUpdate = ({ id, name, display_name, position_role_name}) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_POSITION_UPDATE_REQUEST, ADMINISTRATIVE_POSITION_UPDATE_SUCCESS, ADMINISTRATIVE_POSITION_UPDATE_FAILURE],
      endpoint: `administrative/positions/${id}`,
      payload: {
        options: {
          method: 'put',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify({
            id,
            name,
            display_name,
            position_role_name
          })
        }
      }
    }
  });
};

export const ADMINISTRATIVE_POSITION_REMOVE_REQUEST = 'ADMINISTRATIVE_POSITION_REMOVE_REQUEST';
export const ADMINISTRATIVE_POSITION_REMOVE_SUCCESS = 'ADMINISTRATIVE_POSITION_REMOVE_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_POSITION_REMOVE_FAILURE = 'ADMINISTRATIVE_POSITION_REMOVE_FAILURE'; /* payload: { code, message } */

export const actionPositionRemove = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_POSITION_REMOVE_REQUEST, ADMINISTRATIVE_POSITION_REMOVE_SUCCESS, ADMINISTRATIVE_POSITION_REMOVE_FAILURE],
      endpoint: `administrative/positions/${id}`,
      payload: {
        options: {
          method: 'DELETE'
        }
      }
    }
  });
};

export const ADMINISTRATIVE_WORK_PERIOD_FILTER_REQUEST = 'ADMINISTRATIVE_WORK_PERIOD_FILTER_REQUEST';
export const ADMINISTRATIVE_WORK_PERIOD_FILTER_SUCCESS = 'ADMINISTRATIVE_WORK_PERIOD_FILTER_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_WORK_PERIOD_FILTER_FAILURE = 'ADMINISTRATIVE_WORK_PERIOD_FILTER_FAILURE'; /* payload: { code, message } */

export const actionWorkPeriodFilter = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_WORK_PERIOD_FILTER_REQUEST, ADMINISTRATIVE_WORK_PERIOD_FILTER_SUCCESS, ADMINISTRATIVE_WORK_PERIOD_FILTER_FAILURE],
      endpoint: `administrative/work-periods`,
      payload: {
        options: {
          method: 'get',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        }
      }
    }
  });
};

export const ADMINISTRATIVE_BEGIN_WORK_PERIOD_REQUEST = 'ADMINISTRATIVE_BEGIN_WORK_PERIOD_REQUEST';
export const ADMINISTRATIVE_BEGIN_WORK_PERIOD_SUCCESS = 'ADMINISTRATIVE_BEGIN_WORK_PERIOD_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_BEGIN_WORK_PERIOD_FAILURE = 'ADMINISTRATIVE_BEGIN_WORK_PERIOD_FAILURE'; /* payload: { code, message } */

export const actionWorkPeriodCreate = ({ position_id, membership_id }) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_BEGIN_WORK_PERIOD_REQUEST, ADMINISTRATIVE_BEGIN_WORK_PERIOD_SUCCESS, ADMINISTRATIVE_BEGIN_WORK_PERIOD_FAILURE],
      endpoint: `administrative/work-periods`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify({
            position_id,
            membership_id
          })
        }
      }
    }
  });
};

export const ADMINISTRATIVE_END_WORK_PERIOD_REQUEST = 'ADMINISTRATIVE_END_WORK_PERIOD_REQUEST';
export const ADMINISTRATIVE_END_WORK_PERIOD_SUCCESS = 'ADMINISTRATIVE_END_WORK_PERIOD_SUCCESS'; /* payload: { data } */
export const ADMINISTRATIVE_END_WORK_PERIOD_FAILURE = 'ADMINISTRATIVE_END_WORK_PERIOD_FAILURE'; /* payload: { code, message } */

export const actionWorkPeriodRemove = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ADMINISTRATIVE_END_WORK_PERIOD_REQUEST, ADMINISTRATIVE_END_WORK_PERIOD_SUCCESS, ADMINISTRATIVE_END_WORK_PERIOD_FAILURE],
      endpoint: `administrative/work-periods/${id}`,
      payload: {
        options: {
          method: 'delete',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        }
      }
    }
  });
};

