export const SETTINGS_SET = 'SETTINGS_SET';
export function actionSettingsSet(field, value) {
  return (dispatch) => dispatch({
    type: SETTINGS_SET,
    payload: {
      field,
      value
    }
  });
}

export const SETTINGS_RESET = 'SETTINGS_RESET';
export function actionSettingsReset(field) {
  return (dispatch) => dispatch({
    type: SETTINGS_RESET,
    payload: {
      field
    }
  });
}

