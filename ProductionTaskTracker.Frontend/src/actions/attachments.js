import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const ATTACHMENTS_UPLOAD_REQUEST = 'ATTACHMENTS_UPLOAD_REQUEST';
export const ATTACHMENTS_UPLOAD_SUCCESS = 'ATTACHMENTS_UPLOAD_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const ATTACHMENTS_UPLOAD_FAILURE = 'ATTACHMENTS_UPLOAD_FAILURE'; /* payload: { code, message } */

export const actionAttachmentsUpload = (attachments) => (dispatch) => {
  const formData = new FormData();
  for(let x in attachments) {
    formData.append(x, attachments[x]);
  }
  return dispatch({
    [API_REQUEST]: {
      types: [ ATTACHMENTS_UPLOAD_REQUEST, ATTACHMENTS_UPLOAD_SUCCESS, ATTACHMENTS_UPLOAD_FAILURE ],
      endpoint: 'attachments',
      payload: {
        options: {
          method: 'post',
          body: formData
        }
      }
    }
  });
};

export const actionAttachmentsTicketDetailUpload = (attachments, ticketDetailID) => (dispatch) => {
  const formData = new FormData();
  for(let x in attachments) {
    formData.append(x, attachments[x]);
  }
  
  return dispatch({
    [API_REQUEST]: {
      types: [ ATTACHMENTS_UPLOAD_REQUEST, ATTACHMENTS_UPLOAD_SUCCESS, ATTACHMENTS_UPLOAD_FAILURE ],
      endpoint: `ticket-details/${ticketDetailID}/attachments`,
      payload: {
        options: {
          method: 'post',
          body: formData
        }
      }
    }
  });
};

export const ATTACHMENTS_GET_REQUEST = 'ATTACHMENTS_GET_REQUEST';
export const ATTACHMENTS_GET_SUCCESS = 'ATTACHMENTS_GET_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const ATTACHMENTS_GET_FAILURE = 'ATTACHMENTS_GET_FAILURE'; /* payload: { code, message } */

export const actionAttachmentsGet = (attachmentID) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ ATTACHMENTS_GET_REQUEST, ATTACHMENTS_GET_SUCCESS, ATTACHMENTS_GET_FAILURE ],
      endpoint: `attachments/${attachmentID}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};