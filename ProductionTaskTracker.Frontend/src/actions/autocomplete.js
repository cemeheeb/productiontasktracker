import {
  API_REQUEST
} from '../middlewares/api-middleware';

export const AUTOCOMPLETE_FILTER_REQUEST = 'AUTOCOMPLETE_FILTER_REQUEST';
export const AUTOCOMPLETE_FILTER_SUCCESS = 'AUTOCOMPLETE_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const AUTOCOMPLETE_FILTER_FAILURE = 'AUTOCOMPLETE_FILTER_FAILURE'; /* payload: { code, message } */

export const actionAutocompleteOptionFilter = (membershipID, parameter) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [AUTOCOMPLETE_FILTER_REQUEST, AUTOCOMPLETE_FILTER_SUCCESS, AUTOCOMPLETE_FILTER_FAILURE],
      endpoint: `autocompletes`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          membership_id: membershipID,
          parameter
        }
      }
    }
  });
};

export const AUTOCOMPLETE_REMOVE_REQUEST = 'AUTOCOMPLETE_REMOVE_REQUEST';
export const AUTOCOMPLETE_REMOVE_SUCCESS = 'AUTOCOMPLETE_REMOVE_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const AUTOCOMPLETE_REMOVE_FAILURE = 'AUTOCOMPLETE_REMOVE_FAILURE'; /* payload: { code, message } */

export const actionAutocompleteOptionRemove = (membershipID, parameter, text) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [AUTOCOMPLETE_REMOVE_REQUEST, AUTOCOMPLETE_REMOVE_SUCCESS, AUTOCOMPLETE_REMOVE_FAILURE],
      endpoint: `autocompletes`,
      payload: {
        options: {
          method: 'delete'
        },
        parameters: {
          membership_id: membershipID,
          parameter,
          text
        }
      }
    }
  });
};