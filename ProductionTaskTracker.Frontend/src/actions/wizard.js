import moment from 'moment';

import {
  actionInformationTicketDetail,
  actionInformationTicketDetailGroup
  //actionInformationControlled
} from '../actions/information';

import {
  actionInitializeForm
} from '../actions/form';

import {
  A1, A3, A4,
  B2, B3, B4, B5, B6, B8,
  C
} from '../constants/ticket-types';

export const WIZARD_REQUEST_SHOW = 'WIZARD_REQUEST_SHOW';
export const actionWizardRequest = () => (dispatch) => {
  actionInitializeForm('wizard-request', { status_from: null });
  return dispatch({ type: WIZARD_REQUEST_SHOW });
};

const setInitialFrom = (data) => {
  const booleanFields = [];

  switch (data.ticket_type) {
    case A1:
      booleanFields.push('crimping');
      booleanFields.push('revision_svu');
      booleanFields.push('replace_svu');
      break;
    case A3:
      booleanFields.push('watering');
      booleanFields.push('kvch');
      booleanFields.push('multi_component');
      booleanFields.push('carbonate');
      booleanFields.push('inhibitor');
      booleanFields.push('carbonate');
      booleanFields.push('oil_component');
      booleanFields.push('complex');
      break;
    case A4:
      booleanFields.push('dmg');
      booleanFields.push('gathering');
      break;
    case B2:
      booleanFields.push('pump_crimping');
      booleanFields.push('pump_washing');
      break;
    case B3:
      booleanFields.push('pump_crimping');
      booleanFields.push('pump_washing');
      break;
    case B4:
      booleanFields.push('dmg_before');
      break;
    case B5:
      booleanFields.push('pressure_after_damping');
      booleanFields.push('liquid_probe');
      break;
    case B6:
      booleanFields.push('preparing');
      booleanFields.push('mixture');
      break;
    case B8:
      booleanFields.push('purge');
      booleanFields.push('requested_epu');
      booleanFields.push('requested_cnipr');
      break;
    case C:
      booleanFields.push('lock_available');
      booleanFields.push('service_area');
      booleanFields.push('balance_delta');
      break;
  }

  booleanFields.forEach(field => {
    if (typeof data[field] !== 'undefined' || (typeof data.requested !== 'undefined' && data.requested.indexOf(field) >= 0)) {
      data[field] = false;
    }
  }); 
};

const convertDateTimes = (formData) => {
  ['ticket_created', 'probe_time', 'start_time', 'start_time_plan', 'time_completion', 'treatment_datebegin', 'treatment_dateend'].forEach(x => {
    if (Object.keys(formData).indexOf(x) < 0) {
      return;
    }

    formData[x] = moment(formData[x]);
  });
};

export const WIZARD_RESPONSE_SHOW = 'WIZARD_RESPONSE_SHOW';
export const actionWizardResponse = (id, behaviour) => (dispatch) => {
  if (behaviour === 'Group') {
    dispatch(actionInformationTicketDetailGroup(id)).then((x) => {
      const formData = Object.assign({}, x.payload);

      formData.parameters = [];
      x.payload.parameters.forEach((parameter) => {
        const internalFormData = Object.assign({}, parameter);
        setInitialFrom(internalFormData);
        convertDateTimes(internalFormData);
        formData.parameters.push(internalFormData);
      });

      dispatch(actionInitializeForm('wizard-response', formData));
      return dispatch({ type: WIZARD_RESPONSE_SHOW, ticketDetail: x.payload });
    });
  } else {
    dispatch(actionInformationTicketDetail(id)).then((x) => {
      const internalFormData = Object.assign({}, x.payload);
      internalFormData.id = internalFormData.ticket_id;
      setInitialFrom(internalFormData);
      convertDateTimes(internalFormData);

      dispatch(actionInitializeForm('wizard-response', internalFormData));
      return dispatch({ type: WIZARD_RESPONSE_SHOW, ticketDetail: x.payload });
    });
  }
};

export const WIZARD_CONTROLLED_SHOW = 'WIZARD_CONTROLLED_SHOW';
export const actionWizardControlled = () => (dispatch) => {
  return dispatch({ type: WIZARD_CONTROLLED_SHOW });
};

export const WIZARD_WELL_SHOW = 'WIZARD_WELL_SHOW';
export const actionWizardWell = () => (dispatch) => {
  return dispatch({ type: WIZARD_WELL_SHOW });
};

export const WIZARD_RESET = 'WIZARD_RESET';
export const actionWizardReset = () => (dispatch) => {
  return dispatch({ type: WIZARD_RESET });
};

export const WIZARD_SET_HISTORY = 'WIZARD_SET_HISTORY';
export const actionWizardSetHistory = (value) => (dispatch) => {
  return dispatch({ type: WIZARD_SET_HISTORY, payload: value });
};