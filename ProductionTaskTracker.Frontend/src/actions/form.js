import { reset, change, initialize } from 'redux-form';
import { arrayPush } from 'redux-form/immutable';

export const actionResetForm = (form) => (dispatch) => {
  return dispatch(reset(form));
};

export const actionChangeForm = (form, field, value) => (dispatch) => {
  return dispatch(change(form, field, value));
};

export const actionChangeFormFieldArray = (form, array, index, field, value) => (dispatch) => {
  return dispatch(change(form, `${array}[${index}].${field}`, value));
};

export const actionInitializeForm = (form, data) => (dispatch) => {
  return dispatch(initialize(form, data));
};

export const actionArrayPush = (formName, arrayName, value) => (dispatch) => {
  return dispatch(arrayPush(formName, arrayName, value));
};