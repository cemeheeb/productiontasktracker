import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const CONTROLLED_FILTER_REQUEST = 'CONTROLLED_FILTER_REQUEST';
export const CONTROLLED_FILTER_SUCCESS = 'CONTROLLED_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const CONTROLLED_FILTER_FAILURE = 'CONTROLLED_FILTER_FAILURE'; /* payload: { code, message } */

export const actionControlledFilter = (filter, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ CONTROLLED_FILTER_REQUEST, CONTROLLED_FILTER_SUCCESS, CONTROLLED_FILTER_FAILURE ],
      endpoint: `controlled-wells`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          filter,
          index: index - 1,
          size
        }
      }
    }
  });
};

export const CONTROLLED_CREATE_REQUEST = 'CONTROLLED_CREATE_REQUEST';
export const CONTROLLED_CREATE_SUCCESS = 'CONTROLLED_CREATE_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const CONTROLLED_CREATE_FAILURE = 'CONTROLLED_CREATE_FAILURE'; /* payload: { code, message } */

export const actionControlledCreate = (inputData) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ CONTROLLED_CREATE_REQUEST, CONTROLLED_CREATE_SUCCESS, CONTROLLED_CREATE_FAILURE ],
      endpoint: `controlled-wells`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(inputData)
        },
      }
    }
  });
};

export const CONTROLLED_UPDATE_REQUEST = 'CONTROLLED_UPDATE_REQUEST';
export const CONTROLLED_UPDATE_SUCCESS = 'CONTROLLED_UPDATE_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const CONTROLLED_UPDATE_FAILURE = 'CONTROLLED_UPDATE_FAILURE'; /* payload: { code, message } */

export const actionControlledUpdate = (inputData) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ CONTROLLED_UPDATE_REQUEST, CONTROLLED_UPDATE_SUCCESS, CONTROLLED_UPDATE_FAILURE ],
      endpoint: `controlled-wells/${inputData.id}`,
      payload: {
        options: {
          method: 'put',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(inputData)
        },
      }
    }
  });
};

export const CONTROLLED_DELETE_REQUEST = 'CONTROLLED_DELETE_REQUEST';
export const CONTROLLED_DELETE_SUCCESS = 'CONTROLLED_DELETE_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const CONTROLLED_DELETE_FAILURE = 'CONTROLLED_DELETE_FAILURE'; /* payload: { code, message } */

export const actionControlledDelete = (id) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ CONTROLLED_DELETE_REQUEST, CONTROLLED_DELETE_SUCCESS, CONTROLLED_DELETE_FAILURE ],
      endpoint: `controlled-wells/${id}`,
      payload: {
        options: {
          method: 'delete'
        },
      }
    }
  });
};