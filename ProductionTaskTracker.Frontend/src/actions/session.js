export const SESSION_DATE_BEGIN_CHANGE = 'SESSION_DATE_BEGIN_CHANGE';
export function actionSessionDateBeginChange(value) {
  return (dispatch) => dispatch({
    type: SESSION_DATE_BEGIN_CHANGE,
    payload: value.startOf('day').format()
  });
}

export const SESSION_DATE_END_CHANGE = 'SESSION_DATE_END_CHANGE';
export function actionSessionDateEndChange(value) {
  return (dispatch) => dispatch({
    type: SESSION_DATE_END_CHANGE,
    payload: value.endOf('day').format()
  });
}

export const SESSION_HISTORY_DATE_BEGIN_CHANGE = 'SESSION_HISTORY_DATE_BEGIN_CHANGE';
export function actionSessionHistoryDateBeginChange(value) {
  return (dispatch) => dispatch({
    type: SESSION_HISTORY_DATE_BEGIN_CHANGE,
    payload: value.startOf('day').format()
  });
}

export const SESSION_HISTORY_DATE_END_CHANGE = 'SESSION_HISTORY_DATE_END_CHANGE';
export function actionSessionHistoryDateEndChange(value) {
  return (dispatch) => dispatch({
    type: SESSION_HISTORY_DATE_END_CHANGE,
    payload: value.endOf('day').format()
  });
}

export const SESSION_PAGE_CHANGE = 'SESSION_PAGE_CHANGE';
export function actionSessionPageChange(value) {
  return (dispatch) => dispatch({
    type: SESSION_PAGE_CHANGE,
    payload: value
  });
}
