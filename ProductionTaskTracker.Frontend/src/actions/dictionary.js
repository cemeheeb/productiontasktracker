import { 
  API_REQUEST
} from '../middlewares/api-middleware';

import {
  cacheInformationContractorSelector,
  cacheInformationPumpSelector
} from '../selectors/cache';

export const DICTIONARY_WELL_FILTER_REQUEST = 'DICTIONARY_WELL_FILTER_REQUEST';
export const DICTIONARY_WELL_FILTER_SUCCESS = 'DICTIONARY_WELL_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_WELL_FILTER_FAILURE = 'DICTIONARY_WELL_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryDepartmentsWellFilter = (search, departments, index, size) => (dispatch) => {
  const parameters = {
    search,
    departments,
    index,
    size
  };

  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_WELL_FILTER_REQUEST, DICTIONARY_WELL_FILTER_SUCCESS, DICTIONARY_WELL_FILTER_FAILURE ],
      endpoint: `wells`,
      payload: {
        options: {
          method: 'get',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        },
        parameters
      }
    }
  });
};

export const actionDictionaryWellFilter = (search, index, size) => (dispatch) => {
  const parameters = {
    search,
    index,
    size
  };

  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_WELL_FILTER_REQUEST, DICTIONARY_WELL_FILTER_SUCCESS, DICTIONARY_WELL_FILTER_FAILURE ],
      endpoint: `wells`,
      payload: {
        options: {
          method: 'get',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          }
        },
        parameters
      }
    }
  });
};

export const DICTIONARY_FIELD_FILTER_REQUEST = 'DICTIONARY_FIELD_FILTER_REQUEST';
export const DICTIONARY_FIELD_FILTER_SUCCESS = 'DICTIONARY_FIELD_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_FIELD_FILTER_FAILURE = 'DICTIONARY_FIELD_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryFieldFilter = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [DICTIONARY_FIELD_FILTER_REQUEST, DICTIONARY_FIELD_FILTER_SUCCESS, DICTIONARY_FIELD_FILTER_FAILURE],
      endpoint: `fields`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const actionDictionaryFieldMeasureFacilityFilter = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_FIELD_FILTER_REQUEST, DICTIONARY_FIELD_FILTER_SUCCESS, DICTIONARY_FIELD_FILTER_FAILURE ],
      endpoint: `fields`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          measure_facility_filter: true
        }
      }
    }
  });
};

export const DICTIONARY_CLUSTER_FILTER_REQUEST = 'DICTIONARY_CLUSTER_FILTER_REQUEST';
export const DICTIONARY_CLUSTER_FILTER_SUCCESS = 'DICTIONARY_CLUSTER_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CLUSTER_FILTER_FAILURE = 'DICTIONARY_CLUSTER_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryClusterFilter = (fieldCode) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_CLUSTER_FILTER_REQUEST, DICTIONARY_CLUSTER_FILTER_SUCCESS, DICTIONARY_CLUSTER_FILTER_FAILURE ],
      endpoint: `clusters`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          field_code: fieldCode
        }
      }
    }
  });
};

export const DICTIONARY_PUMP_SEARCH_FILTER_REQUEST = 'DICTIONARY_PUMP_SEARCH_FILTER_REQUEST';
export const DICTIONARY_PUMP_SEARCH_FILTER_SUCCESS = 'DICTIONARY_PUMP_SEARCH_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_PUMP_SEARCH_FILTER_FAILURE = 'DICTIONARY_PUMP_SEARCH_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryPumpSearchFilter = (search, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_PUMP_SEARCH_FILTER_REQUEST, DICTIONARY_PUMP_SEARCH_FILTER_SUCCESS, DICTIONARY_PUMP_SEARCH_FILTER_FAILURE ],
      endpoint: `pumps`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          search,
          index,
          size
        }
      }
    }
  });
};

export const DICTIONARY_PUMP_FILTER_REQUEST = 'DICTIONARY_PUMP_FILTER_REQUEST';
export const DICTIONARY_PUMP_FILTER_SUCCESS = 'DICTIONARY_PUMP_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_PUMP_FILTER_FAILURE = 'DICTIONARY_PUMP_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryPumpFilter = (pumpID) => (dispatch, getState) => {
  const pump = cacheInformationPumpSelector(getState(), pumpID);
  if (typeof pump !== 'undefined' && pump !== null) {
    return Promise.resolve({ payload: pump.toJS() });
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_PUMP_FILTER_REQUEST, DICTIONARY_PUMP_FILTER_SUCCESS, DICTIONARY_PUMP_FILTER_FAILURE ],
      endpoint: `pumps/${pumpID}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const DICTIONARY_CONTRACTOR_SEARCH_FILTER_REQUEST = 'DICTIONARY_CONTRACTOR_SEARCH_FILTER_REQUEST';
export const DICTIONARY_CONTRACTOR_SEARCH_FILTER_SUCCESS = 'DICTIONARY_CONTRACTOR_SEARCH_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CONTRACTOR_SEARCH_FILTER_FAILURE = 'DICTIONARY_CONTRACTOR_SEARCH_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryContractorSearchFilter = (search, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [DICTIONARY_CONTRACTOR_SEARCH_FILTER_REQUEST, DICTIONARY_CONTRACTOR_SEARCH_FILTER_SUCCESS, DICTIONARY_CONTRACTOR_SEARCH_FILTER_FAILURE],
      endpoint: `contractors`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          search,
          index,
          size
        }
      }
    }
  });
};

export const DICTIONARY_CONTRACTOR_ADD_REQUEST = 'DICTIONARY_CONTRACTOR_ADD_REQUEST';
export const DICTIONARY_CONTRACTOR_ADD_SUCCESS = 'DICTIONARY_CONTRACTOR_ADD_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CONTRACTOR_ADD_FAILURE = 'DICTIONARY_CONTRACTOR_ADD_FAILURE'; /* payload: { code, message } */

export const actionDictionaryContractorAdd = (parameters) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_CONTRACTOR_SEARCH_FILTER_REQUEST, DICTIONARY_CONTRACTOR_SEARCH_FILTER_SUCCESS, DICTIONARY_CONTRACTOR_SEARCH_FILTER_FAILURE ],
      endpoint: `contractors`,
      payload: {
        options: {
          method: 'post'
        },
        parameters
      }
    }
  });
};

export const DICTIONARY_CONTRACTOR_FILTER_REQUEST = 'DICTIONARY_CONTRACTOR_FILTER_REQUEST';
export const DICTIONARY_CONTRACTOR_FILTER_SUCCESS = 'DICTIONARY_CONTRACTOR_FILTER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CONTRACTOR_FILTER_FAILURE = 'DICTIONARY_CONTRACTOR_FILTER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryContractorFilter = (contractorID) => (dispatch, getState) => {
  const contractor = cacheInformationContractorSelector(getState(), contractorID);
  if (typeof contractor !== 'undefined' && contractor !== null) {
    return Promise.resolve({ payload: contractor.toJS() });
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_CONTRACTOR_FILTER_REQUEST, DICTIONARY_CONTRACTOR_FILTER_SUCCESS, DICTIONARY_CONTRACTOR_FILTER_FAILURE ],
      endpoint: `contractors/${contractorID}`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const DICTIONARY_MEASURE_FACILITY_POSITIONS_REQUEST = 'DICTIONARY_MEASURE_FACILITY_POSITIONS_REQUEST';
export const DICTIONARY_MEASURE_FACILITY_POSITIONS_SUCCESS = 'DICTIONARY_MEASURE_FACILITY_POSITIONS_SUCCESS'; /* payload: { data } */
export const DICTIONARY_MEASURE_FACILITY_POSITIONS_FAILURE = 'DICTIONARY_MEASURE_FACILITY_POSITIONS_FAILURE'; /* payload: { code, message } */

export const actionDictionaryMeasureFacilityPositionsFilter = (field, cluster) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ DICTIONARY_MEASURE_FACILITY_POSITIONS_REQUEST, DICTIONARY_MEASURE_FACILITY_POSITIONS_SUCCESS, DICTIONARY_MEASURE_FACILITY_POSITIONS_FAILURE ],
      endpoint: `fields/${field}/clusters/${cluster}/measure-facility-positions`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const DICTIONARY_CLASSIFIER_REQUEST = 'DICTIONARY_CLASSIFIER_REQUEST';
export const DICTIONARY_CLASSIFIER_SUCCESS = 'DICTIONARY_CLASSIFIER_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CLASSIFIER_FAILURE = 'DICTIONARY_CLASSIFIER_FAILURE'; /* payload: { code, message } */

export const actionDictionaryClassifierFilter = (dictionary, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [DICTIONARY_CLASSIFIER_REQUEST, DICTIONARY_CLASSIFIER_SUCCESS, DICTIONARY_CLASSIFIER_FAILURE],
      endpoint: `classifiers/${dictionary}`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          index,
          size
        }
      }
    }
  });
};

export const DICTIONARY_CLASSIFIER_ADD_REQUEST = 'DICTIONARY_CLASSIFIER_ADD_REQUEST';
export const DICTIONARY_CLASSIFIER_ADD_SUCCESS = 'DICTIONARY_CLASSIFIER_ADD_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CLASSIFIER_ADD_FAILURE = 'DICTIONARY_CLASSIFIER_ADD_FAILURE'; /* payload: { code, message } */

export const actionDictionaryClassifierAdd = (data) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [DICTIONARY_CLASSIFIER_ADD_REQUEST, DICTIONARY_CLASSIFIER_ADD_SUCCESS, DICTIONARY_CLASSIFIER_ADD_FAILURE],
      endpoint: `classifiers`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(data)
        }
      }
    }
  });
};

export const DICTIONARY_CLASSIFIER_UPDATE_REQUEST = 'DICTIONARY_CLASSIFIER_UPDATE_REQUEST';
export const DICTIONARY_CLASSIFIER_UPDATE_SUCCESS = 'DICTIONARY_CLASSIFIER_UPDATE_SUCCESS'; /* payload: { data } */
export const DICTIONARY_CLASSIFIER_UPDATE_FAILURE = 'DICTIONARY_CLASSIFIER_UPDATE_FAILURE'; /* payload: { code, message } */

export const actionDictionaryClassifierUpdate = (data) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [DICTIONARY_CLASSIFIER_UPDATE_REQUEST, DICTIONARY_CLASSIFIER_UPDATE_SUCCESS, DICTIONARY_CLASSIFIER_UPDATE_FAILURE],
      endpoint: `classifiers`,
      payload: {
        options: {
          method: 'put',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(data)
        }
      }
    }
  });
};