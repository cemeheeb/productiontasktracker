export const actionToastrPush = (message) => (dispatch) => dispatch({
  type: '@ReduxToastr/toastr/ADD',
  payload: {
    type: 'error',
    position: 'bottom-center',
    options: {
      removeOnHover: true,
      showCloseButton: true
    },
    title: 'Ошибка:',
    message
  }
});
