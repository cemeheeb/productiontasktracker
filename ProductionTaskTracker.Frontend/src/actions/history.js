import moment from 'moment';
import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const HISTORY_FILTER_REQUEST = 'HISTORY_FILTER_REQUEST';
export const HISTORY_FILTER_SUCCESS = 'HISTORY_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const HISTORY_FILTER_FAILURE = 'HISTORY_FILTER_FAILURE'; /* payload: { code, message } */

export const actionHistoryFilter = (wellID, dateBegin, dateEnd, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ HISTORY_FILTER_REQUEST, HISTORY_FILTER_SUCCESS, HISTORY_FILTER_FAILURE ],
      endpoint: `wells/${wellID}/history`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          date_begin: moment(dateBegin).format(), 
          date_end: moment(dateEnd).format(),
          index,
          size
        }
      }
    }
  });
};