import {
  API_REQUEST
} from '../middlewares/api-middleware';

export const IMPORT_DOWNLOAD_REQUEST = 'IMPORT_DOWNLOAD_REQUEST';
export const IMPORT_DOWNLOAD_SUCCESS = 'IMPORT_DOWNLOAD_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const IMPORT_DOWNLOAD_FAILURE = 'IMPORT_DOWNLOAD_FAILURE'; /* payload: { code, message } */

export const actionExportDocument = (ticket_type, department_id, sl, is_night, ticket_detail_ids) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [IMPORT_DOWNLOAD_REQUEST, IMPORT_DOWNLOAD_SUCCESS, IMPORT_DOWNLOAD_FAILURE],
      endpoint: 'tickets/import',
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          ticket_type,
          department_id,
          sl,
          is_night,
          ticket_detail_ids
        }
      }
    }
  }).then(
    x => {
      window.open(x.payload, '_blank');
      return Promise.resolve(x);
    }
  );
};


export const IMPORT_UPLOAD_REQUEST = 'IMPORT_UPLOAD_REQUEST';
export const IMPORT_UPLOAD_SUCCESS = 'IMPORT_UPLOAD_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const IMPORT_UPLOAD_FAILURE = 'IMPORT_UPLOAD_FAILURE'; /* payload: { code, message } */

export const actionImportUpload = (attachments) => (dispatch) => {
  const formData = new FormData();
  for (let x in attachments) {
    formData.append(x, attachments[x]);
  }
  
  return dispatch({
    [API_REQUEST]: {
      types: [IMPORT_UPLOAD_REQUEST, IMPORT_UPLOAD_SUCCESS, IMPORT_UPLOAD_FAILURE],
      endpoint: 'tickets/import',
      payload: {
        options: {
          method: 'post',
          body: formData
        }
      }
    }
  });
};