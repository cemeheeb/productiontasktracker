import moment from 'moment';

import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const REPORT_TYPE_A_REQUEST = 'REPORT_TYPE_A_REQUEST';
export const REPORT_TYPE_A_SUCCESS = 'REPORT_TYPE_A_SUCCESS';
export const REPORT_TYPE_A_FAILURE = 'REPORT_TYPE_A_FAILURE';

export const actionReportMakeA = (dateBegin, dateEnd, departments, wells, ticketTypes, SL, showExpired) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ REPORT_TYPE_A_REQUEST, REPORT_TYPE_A_SUCCESS, REPORT_TYPE_A_FAILURE ],
      endpoint: "reports/reportA",
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          date_begin: moment(dateBegin).format(),
          date_end: moment(dateEnd).format(),
          departments: departments,
          wells,
          ticket_types: ticketTypes,
          sl: SL,
          show_expired: showExpired
        }
      }
    }
  }).then(
    x => {
      window.open(x.payload, '_blank');
      return Promise.resolve(x);
    }
  );
};

export const REPORT_TYPE_B_REQUEST = 'REPORT_TYPE_B_REQUEST';
export const REPORT_TYPE_B_SUCCESS = 'REPORT_TYPE_B_SUCCESS';
export const REPORT_TYPE_B_FAILURE = 'REPORT_TYPE_B_FAILURE';

export const actionReportMakeB = (dateBegin, dateEnd) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ REPORT_TYPE_B_REQUEST, REPORT_TYPE_B_SUCCESS, REPORT_TYPE_B_FAILURE ],
      endpoint: "reports/reportB",
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          date_begin: moment(dateBegin).format(),
          date_end: moment(dateEnd).format(),
        }
      }
    }
  }).then(
    x => {
      window.location.href = x.payload;
      return Promise.resolve(x);
    }
  );
};