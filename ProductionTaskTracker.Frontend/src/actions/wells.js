import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const WELL_CREATE_REQUEST = 'WELL_CREATE_REQUEST';
export const WELL_CREATE_SUCCESS = 'WELL_CREATE_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const WELL_CREATE_FAILURE = 'WELL_CREATE_FAILURE'; /* payload: { code, message } */

export const actionWellCreate = (inputData) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ WELL_CREATE_REQUEST, WELL_CREATE_SUCCESS, WELL_CREATE_FAILURE ],
      endpoint: `wells`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(inputData)
        },
      }
    }
  });
};