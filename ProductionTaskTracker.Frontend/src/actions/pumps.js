import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const PUMP_IMPORT_REQUEST = 'PUMP_IMPORT_REQUEST';
export const PUMP_IMPORT_SUCCESS = 'PUMP_IMPORT_SUCCESS'; /* payload: { data } */
export const PUMP_IMPORT_FAILURE = 'PUMP_IMPORT_FAILURE'; /* payload: { code, message } */

export const actionPumpImport = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ PUMP_IMPORT_REQUEST, PUMP_IMPORT_SUCCESS, PUMP_IMPORT_FAILURE ],
      endpoint: `pumps/import`,
      payload: {
        options: {
          method: 'post'
        }
      }
    }
  });
};