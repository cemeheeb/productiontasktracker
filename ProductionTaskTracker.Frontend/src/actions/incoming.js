import moment from 'moment';
import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const INCOMING_FILTER_REQUEST = 'INCOMING_FILTER_REQUEST';
export const INCOMING_FILTER_SUCCESS = 'INCOMING_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INCOMING_FILTER_FAILURE = 'INCOMING_FILTER_FAILURE'; /* payload: { code, message } */

export const actionIncomingFilter = (filter, dateBegin, dateEnd, index, size) => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ INCOMING_FILTER_REQUEST, INCOMING_FILTER_SUCCESS, INCOMING_FILTER_FAILURE ],
      endpoint: `tickets`,
      payload: {
        options: {
          method: 'get'
        },
        parameters: {
          kind: "incoming",
          filter,
          date_begin: moment(dateBegin).format(),
          date_end: moment(dateEnd).format(),
          index: index - 1,
          size
        }
      }
    }
  });
};

export const INCOMING_UNREADED_FILTER_REQUEST = 'INCOMING_UNREADED_FILTER_REQUEST';
export const INCOMING_UNREADED_FILTER_SUCCESS = 'INCOMING_UNREADED_FILTER_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INCOMING_UNREADED_FILTER_FAILURE = 'INCOMING_UNREADED_FILTER_FAILURE'; /* payload: { code, message } */

export const actionIncomingUnreadedFilter = () => (dispatch) => {
  return dispatch({
    [API_REQUEST]: {
      types: [ INCOMING_UNREADED_FILTER_REQUEST, INCOMING_UNREADED_FILTER_SUCCESS, INCOMING_UNREADED_FILTER_FAILURE ],
      endpoint: `ticket-movements/notifications`,
      payload: {
        options: {
          method: 'get'
        }
      }
    }
  });
};

export const INCOMING_TICKET_MOVEMENT_REQUEST = 'INCOMING_TICKET_MOVEMENT_REQUEST';
export const INCOMING_TICKET_MOVEMENT_SUCCESS = 'INCOMING_TICKET_MOVEMENT_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INCOMING_TICKET_MOVEMENT_FAILURE = 'INCOMING_TICKET_MOVEMENT_FAILURE'; /* payload: { code, message } */

export const actionIncomingTicketMovement = (data) => (dispatch) => {
  [
    'start_time',
    'probe_time',
    'treatment_time',
    'obtain_time',
    'padding_time',
    'start_time_plan',
    'time_completion'
  ].forEach(x => {
    if (typeof data[x] !== 'undefined' && data[x] !== null) { 
      data[x] = moment(data[x], 'DD.MM.YYYY HH:mm').format();
    }
  });
  
  return dispatch({
    [API_REQUEST]: {
      types: [ INCOMING_TICKET_MOVEMENT_REQUEST, INCOMING_TICKET_MOVEMENT_SUCCESS, INCOMING_TICKET_MOVEMENT_FAILURE ],
      endpoint: `ticket-movements`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(data)
        }
      }
    }
  });
};

export const INCOMING_TICKET_GROUP_MOVEMENT_REQUEST = 'INCOMING_TICKET_GROUP_MOVEMENT_REQUEST';
export const INCOMING_TICKET_GROUP_MOVEMENT_SUCCESS = 'INCOMING_TICKET_GROUP_MOVEMENT_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const INCOMING_TICKET_GROUP_MOVEMENT_FAILURE = 'INCOMING_TICKET_GROUP_MOVEMENT_FAILURE'; /* payload: { code, message } */

export const actionIncomingTicketGroupMovement = (data) => (dispatch) => {
  [
    'start_time',
    'probe_time',
    'treatment_time',
    'obtain_time',
    'padding_time',
    'start_time_plan',
    'time_completion'
  ].forEach(x => {
    if (typeof data.parameters !== 'undefined') {
      data.parameters.forEach(parameter => {
        if (typeof parameter[x] !== 'undefined' && parameter[x] !== null) {
          parameter[x] = moment(parameter[x], 'DD.MM.YYYY HH:mm').format();
        }
      });
    }
  });

  return dispatch({
    [API_REQUEST]: {
      types: [INCOMING_TICKET_GROUP_MOVEMENT_REQUEST, INCOMING_TICKET_GROUP_MOVEMENT_SUCCESS, INCOMING_TICKET_GROUP_MOVEMENT_FAILURE],
      endpoint: `ticket-movements/group`,
      payload: {
        options: {
          method: 'post',
          headers: {
            "Content-Type": 'application/json; charset=utf-8'
          },
          body: JSON.stringify(data)
        }
      }
    }
  });
};