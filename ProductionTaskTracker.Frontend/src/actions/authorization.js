import qs from 'querystring';

import { REHYDRATE } from 'redux-persist/lib/constants';

import { 
  API_REQUEST
} from '../middlewares/api-middleware';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'; /* payload: { membership, token, creadentials: { username, password } } */
export const LOGIN_FAILURE = 'LOGIN_FAILURE'; /* payload: { code, message } */

import { initialState } from '../reducers/initial-state';

export const actionLogin = (username, password, domain) => (dispatch) => {
  const body = {
    grant_type:'password',
    username,
    password,
    domain
  };

  // В случае если домен не задан удаляем из объекта поле domain
  if (!domain) {
    delete body['domain'];
  }

  return dispatch({
    [API_REQUEST]: {
      types: [ LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE ],
      endpoint: `token`,
      payload: {
        options: {
          method: 'post',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
          },
          body: qs.stringify(body)
        }
      }
    }
  });
};

export const LOGOUT = 'LOGOUT';
export const actionLogout = () => (dispatch) => {
  dispatch({ type: REHYDRATE, payload: initialState });
  dispatch({ type: LOGOUT });
};