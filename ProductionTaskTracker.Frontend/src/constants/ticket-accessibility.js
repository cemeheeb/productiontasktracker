export const TicketAccessibility = {
  well: [
    'A1', 'A2', 'A3', 'A4',
    'B1', 'B2', 'B2A', 'B2B', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B11', 'B12', 'B13',
    'C',
    'E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8'
  ],
  cluster: [
    'B10'
  ]
};