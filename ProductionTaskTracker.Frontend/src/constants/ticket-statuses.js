// Новая
export const CREATED = 0;

// Поданная
export const FILED = 1;

// Принятая
export const ACCEPTED = 2;

// Отклоненная
export const REJECTED = 3;

// Согласованная
export const AGREED = 4;

// В работе
export const PROGRESS = 5;

// Отработано
export const PROGRESS_READY = 6;

// Подтвержденная - подтверждено линейным руководителем
export const PROGRESS_CONFIRMED = 7;

// На доработке
export const PROGRESS_REJECTED = 8;

// Выполненная - подтверждено руководителем
export const COMPLETED = 9;

// Ознакомленная
export const VISITED = 12;

// Снятая
export const CANCELLED = 13;