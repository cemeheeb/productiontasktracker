import propertyConverter from '../utilities/property-converter';

export const PROPERTY_TYPE_LIST = {
  requested: {
    component:'SelectRedux',
    matchPos:"start",
    multi: true, 
    simpleValue: true
  },
  volume: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  damping_volume: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  treatment_datebegin: {
    component: 'FormDateTimePicker'
  },
  treatment_dateend: {
    component: 'FormDateTimePicker'
  },
  qn_volume_before_loss: {
    component: 'FormFieldNumeric',
    min: 0
  },
  qn_volume_before_collector: {
    component: 'FormFieldNumeric',
    units: 'м³'
  },
  temperature_interval: {
    component: 'FormFieldNumeric',
    units: 'C°'
  },
  pressure_adpm_begin: {
    component: 'FormFieldNumeric',
    units: 'МПа'
  },
  pressure_adpm_end: {
    component: 'FormFieldNumeric',
    units: 'МПа'
  },
  wash_volume: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  pressure_ecn_minimal: {
    component: 'FormFieldNumeric',
    units: 'Мпа (м)',
    min: 0
  },
  pump_depth: {
    component: 'FormFieldNumeric',
    units: 'м',
    min: 0
  },
  h_dynamic_minimal: {
    component: 'FormFieldNumeric',
    units: 'м',
    min: 0
  },
  weight: {
    component: 'FormFieldNumeric',
    units: 'г/см³',
    min: 0
  },
  damping_weight: {
    component: 'FormFieldNumeric',
    units: 'г/см³',
    min: 0
  },
  brigade: {
    component: 'FormFieldNumeric',
    min: 0
  },
  pressure_engine: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  q: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj_locked: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj_redirect: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj_manual: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj_manual_before: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj_manual_after: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  qj_reverse: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    min: 0
  },
  pressure_buffer: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  h_dynamic: {
    component: 'FormFieldNumeric',
    units: 'м',
    min: 0
  },
  puller: {
    component: 'FormFieldNumeric',
    min: 0
  },
  pressure: {
    component: 'FormFieldNumeric',
    units: 'МПа',
    min: 0
  },
  pressure_pipe: {
    component: 'FormFieldNumeric',
    units: 'атм',
    min: 0
  },
  amperage: {
    component: 'FormFieldNumeric',
    units: 'А',
    min: 0
  },
  amperage_reverse: {
    component: 'FormFieldNumeric',
    units: 'А',
    min: 0
  },
  voltage: {
    component: 'FormFieldNumeric',
    units: 'В',
    min: 0
  },
  voltage_after: {
    component: 'FormFieldNumeric',
    units: 'В',
    min: 0
  },
  loading: {
    component: 'FormFieldNumeric',
    units: '%',
    min: 0
  },
  loading_reverse: {
    component: 'FormFieldNumeric',
    units: '%',
    min: 0
  },
  rotation_frequency: {
    component: 'FormFieldNumeric',
    units: 'об/мин',
    min: 0
  },
  rotation_frequency_delta: {
    component: 'FormFieldNumeric',
    units: 'Гц',
    min: 0
  },
  pressure_environment: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  temperature_environment: {
    component: 'FormFieldNumeric',
    units: 'C°',
    min: 0
  },
  temperature_engine: {
    component: 'FormFieldNumeric',
    units: 'C°',
    min: 0
  },
  rock_frequency: {
    component: 'FormFieldNumeric',
    min: 2,
    max: 20
  },
  wheeling: {
    component: 'FormFieldNumeric',
    units: 'об/мин',
    min: 0
  },
  h_static: {
    component: 'FormFieldNumeric',
    units: 'м',
    min: 0
  },
  h_static_pipe: {
    component: 'FormFieldNumeric',
    units: 'м',
    min: 0
  },
  h_static_after_stop: {
    component: 'FormFieldNumeric',
    units: 'м',
    min: 0
  },
  pressure_crimping: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  wash_pressure_begin: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  wash_pressure_end: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  amperage_after: {
    component: 'FormFieldNumeric',
    units: 'А',
    min: 0
  },
  pressure_after: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  h_dynamic_15: {
    component: 'FormFieldNumeric',
    units: 'м',
  },
  union_dynamic: {
    component: 'FormFieldNumeric',
    min: 2,
    max: 20
  },
  pressure_15: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  q_15: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    type: 'number'
  },
  pressure_environment_15: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  pressure_engine_15: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  temperature_environment_15: {
    component: 'FormFieldNumeric',
    units: 'C°',
    type: 'number'
  },
  temperature_engine_15: {
    component: 'FormFieldNumeric',
    units: 'C°',
    type: 'number'
  },
  amperage_15: {
    component: 'FormFieldNumeric',
    units: 'A',
    type: 'number'
  },
  loading_15: {
    component: 'FormFieldNumeric',
    units: '%',
    type: 'number'
  },
  rotation_frequency_15: {
    component: 'FormFieldNumeric',
    units: 'об/мин',
    min: 0
  },
  shank_length: {
    component: 'FormFieldNumeric',
    units: 'м',
    type: 'number'
  },
  h_dynamic_30: {
    component: 'FormFieldNumeric',
    units: 'м',
    type: 'number'
  },
  pressure_30: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  q_30: {
    component: 'FormFieldNumeric',
    units: 'м³/сут',
    type: 'number'
  },
  pressure_environment_30: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  pressure_engine_30: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  temperature_environment_30: {
    component: 'FormFieldNumeric',
    units: 'C°',
    type: 'number'
  },
  temperature_engine_30: {
    component: 'FormFieldNumeric',
    units: 'C°',
    type: 'number'
  },
  amperage_30: {
    component: 'FormFieldNumeric',
    units: 'A',
    type: 'number'
  },
  loading_30: {
    component: 'FormFieldNumeric',
    units: '%',
    type: 'number'
  },
  rotation_frequency_30: {
    component: 'FormFieldNumeric',
    units: 'об/мин',
    min: 0
  },
  amperage_before: {
    component: 'FormFieldNumeric',
    units: 'А',
    min: 0
  },
  loading_before: {
    component: 'FormFieldNumeric',
    units: '%',
    min: 0
  },
  pressure_environment_before: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  pressure_engine_before: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  temperature_environment_before: {
    component: 'FormFieldNumeric',
    units: 'C°',
    min: 0
  },
  temperature_engine_before: {
    component: 'FormFieldNumeric',
    units: 'C°',
    min: 0
  },
  pressure_environment_after: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  pressure_engine_after: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  temperature_environment_after: {
    component: 'FormFieldNumeric',
    units: 'C°',
    min: 0
  },
  temperature_engine_after: {
    component: 'FormFieldNumeric',
    units: 'C°',
    min: 0
  },
  crimping_begin: {
    component: 'FormFieldNumeric',
    units: 'Мпа'
  },
  crimping_end: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    type: 'number'
  },
  crimping_duration: {
    component: 'FormFieldNumeric',
    units: 'мин'
  },
  h_static_after_crimping: {
    component: 'FormFieldNumeric',
    units: 'м'
  },
  h_dynamic_pressure_after_wash: {
    component: 'FormFieldNumeric',
    units: 'м'
  },
  h_dynamic_pressure_duration: {
    component: 'FormFieldNumeric',
    units: '15мин м/Мпа'
  },
  scraped: {
    component: 'FormFieldNumeric',
    units: 'м'
  },
  pressure_redundant: {
    component: 'FormFieldNumeric',
    units: 'Мпа',
    min: 0
  },
  protection_correction: {
    component: 'FormFieldNumeric'
  },
  union_diameter: {
    component: 'FormFieldNumeric'
  },

  damping_volume_cycle_1: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  weight_cycle_1: {
    component: 'FormFieldNumeric',
    units: 'г/см³',
    min: 0
  },
  damping_volume_cycle_2: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  weight_cycle_2: {
    component: 'FormFieldNumeric',
    units: 'г/см³',
    min: 0
  },
  damping_volume_cycle_3: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  weight_cycle_3: {
    component: 'FormFieldNumeric',
    units: 'г/см³',
    min: 0
  },
  damping_volume_cycle_4: {
    component: 'FormFieldNumeric',
    units: 'м³',
    min: 0
  },
  weight_cycle_4: {
    component: 'FormFieldNumeric',
    units: 'г/см³',
    min: 0
  },
  separate_expense: {
    component: 'FormFieldNumeric'
  },
  separate_humidity: {
    component: 'FormFieldNumeric',
    min: 0
  },
  separate_pressure: {
    component: 'FormFieldNumeric'
  },
  separate_temperature: {
    component: 'FormFieldNumeric'
  },
  plunger_length: {
    component: 'FormFieldNumeric',
    units: 'м'
  },
  balance_delta: {
    component: 'FormFieldCheckbox',
  },
  lock_available: {
    component: 'FormFieldCheckbox',
  },
  ua: {
    component: 'FormFieldCheckbox',
  },
  ktpn: {
    component: 'FormFieldCheckbox',
  },
  sk: {
    component: 'FormFieldCheckbox',
  },
  agzu: {
    component: 'FormFieldCheckbox',
  },
  bg: {
    component: 'FormFieldCheckbox',
  },
  watering: {
    component: 'FormFieldCheckbox',
  },
  crimping: {
    component: 'FormFieldCheckbox',
  },
  purge: {
    component: 'FormFieldCheckbox',
  },
  preparing: {
    component: 'FormFieldCheckbox',
  },
  mixture: {
    component: 'FormFieldCheckbox',
  },
  carbonate: {
    component: 'FormFieldCheckbox'
  },
  kvch: {
    component: 'FormFieldCheckbox'
  },
  multi_component: {
    component: 'FormFieldCheckbox'
  },
  inhibitor: {
    component: 'FormFieldCheckbox'
  },
  dmg: {
    component: 'FormFieldCheckbox'
  },
  dmg_before: {
    component: 'FormFieldCheckbox'
  },
  gathering: {
    component: 'FormFieldCheckbox'
  },
  service_area: {
    component: 'FormFieldCheckbox'
  },
  requested_epu: {
    component: 'FormFieldCheckbox'
  },
  requested_cnipr: {
    component: 'FormFieldCheckbox'
  },
  pile_field_demontage: {
    component: 'FormFieldCheckbox'
  },
  power_off: {
    component: 'FormFieldCheckbox'
  },
  worked: {
    component: 'FormFieldCheckbox'
  },
  need_crimping: {
    component: 'FormFieldCheckbox'
  },
  pressure_after_damping: {
    component: 'FormFieldCheckbox'
  },
  liquid_probe: {
    component: 'FormFieldCheckbox'
  },
  revision_svu: {
    component: 'FormFieldCheckbox'
  },
  replace_svu: {
    component: 'FormFieldCheckbox'
  },
  phase_check: {
    component: 'FormFieldCheckbox'
  },
  oil_component: {
    component: 'FormFieldCheckbox'
  },
  complex: {
    component: 'FormFieldCheckbox'
  },
  pump_crimping: {
    component: 'FormFieldCheckbox'
  },
  brigade_type: {
    component: 'SelectRedux',
    options: [ 
      { label:'ТРС', value: 'TRS' },
      { label:'КРС', value: 'KRS' },
      { label:'ГИС', value: 'GIS' },
      { label:'ЦНИПР', value: 'CNIPR' },
      { label:'Хим. звено', value: 'Chemical' }
    ],
    searchable: false
  },
  union_action_type: {
    component: 'SelectRedux',
    options: [ 
      { label:'Установка', value: 'Installed' },
      { label:'Снятие', value: 'Uninstalled' },
      { label:'Замена', value: 'Replaced' }
    ],
    searchable: false
  },
  equipment_verification:{
    component: 'SelectRedux',
    options: [ 
      { label:'Отклонено', value: 'Rejected' },
      { label:'Подтверждено', value: 'Accepted' }
    ],
    searchable: false
  },
  valve_verification:{
    component: 'SelectRedux',
    options: [ 
      { label:'Отклонено', value: 'Rejected' },
      { label:'Подтверждено', value: 'Accepted' }
    ],
    searchable: false
  },
  treatment: {
    component: 'SelectRedux',
    options: [ 
      { label: 'ТиКРС', value: 'WellRepair' },
      { label: 'КВУ', value: 'CurveLevelRepair' },
      { label: 'Хим. Звено', value: 'Chemical' },
      { label: 'Прочее', value: 'Other' },
    ],
    searchable: false
  },
  research: {
    component: 'SelectRedux',
    options: [ 
      { label: 'КВУ', value: 'KVU' },
      { label: 'КВД', value: 'KVD' },
      { label: 'НСТ', value: 'NST' },
      { label: 'ТиКРС', value: 'TKRS' },
      { label: 'Прочее', value: 'Other' }
    ],
    searchable: false
  },
  well_purpose: {
    component: 'SelectRedux',
    options: [
      { label: 'Добывающая', value: 'Production' },
      { label: 'Нагнетательная', value: 'Injection' },
      { label: 'Водозаборная', value: 'Water' }
    ],
    searchable: false
  },
  cycles: {
    component: 'SelectRedux',
    options: [ 
      { label:'1', value: '1' },
      { label:'2', value: '2' },
      { label:'3', value: '3' },
      { label:'4', value: '4' }
    ],
    searchable: false,
    clearable: false
  },
  repair_type: {
    component: 'SelectRedux',
    options: [ 
      { label:'Текущий ремонт', value: 'Support' },
      { label:'Капитальный ремонт', value: 'Full' },
      { label:'Освоение', value: 'Development' }
    ],
    searchable: false
  },
  event_type: { 
    component: 'SelectRedux',
    options: [ 
      { label:'Смена', value: 'Shift' },
      { label:'ОПЗ', value: 'Cleaning' }
    ],
    searchable: false
  },
  called_agents: {
    component: 'SelectRedux',
    options: [ 
      { label:'ТиКРС', value: 'TKRS' },
      { label:'КНПО-С', value: 'KNPO' },
      { label:'ЭПУ', value: 'EPU' }
    ],
    searchable: false,
    multi: true,
    simpleValue: true
  },
  stop_reason: {
    component: 'SelectRedux',
    options: [ 
      { label:'ТиКРС', value: 'TKRS' },
      { label:'Прочие', value: 'Other' }
    ],
    searchable: false
  },
  well_area: {
    component: 'SelectRedux',
    options: [ 
      { label:'Подготовка', value: 'Prepare' },
      { label:'Отсыпка', value: 'Backfill' }
    ],
    searchable: false
  },
  dmg_type: {
    component: 'SelectRedux',
    options: [
      { label: 'Количество циклов', value: 'CyclesCounter' },
      { label: 'Тест клапанов', value: 'ValveType' }
    ],
    searchable: false
  },
  request_type: {
    component: 'SelectRedux',
    options: [
      { label: 'КВУ', value: 'KVU' },
      { label: 'КВД', value: 'KVD' },
      { label: 'КПД', value: 'KPD' }
    ],
    searchable: false
  },
  well_period_mode: {
    component: 'SelectRedux',
    options: [
      { label: 'КПР', value: 'KPR' },
      { label: 'АПВ', value: 'APV' }
    ],
    searchable: false
  },
  contractor_id: {
    component: 'SelectContractor',
  },
  pump_id: {
    component: 'SelectPump'
  },
  pump_typesize: {
    component: 'SelectRedux',
    options: [
      { label: 'ФЛ', value: 'Basic' },
      { label: 'ФЛ + Пакер', value: 'BasicPacker' }
    ],
    searchable: false
  },
  puller_type_id: {
    component: 'SelectPullerType'
  },
  probe_time: {
    component: 'FormDateTimePicker'
  },
  time_completion: {
    component: 'FormDateTimePicker'
  },
  start_time: {
    component: 'FormDateTimePicker'
  },
  start_time_plan: {
    component: 'FormDateTimePicker'
  },
  start_time_fact: {
    component: 'FormDateTimePicker'
  },
  treatment_time: {
    component: 'FormDateTimePicker'
  },
  obtain_time: {
    component: 'FormDateTimePicker'
  },
  padding_time: {
    component: 'FormDateTimePicker'
  },
  probe_volume: {
    component: 'FormFieldMinimal',
    units: 'л',
    min: 0
  },
  damping_volume_cycle_1: {
    component: 'FormFieldMinimal',
    units: 'м³',
    min: 0
  },
  weight_cycle_1: {
    component: 'FormFieldMinimal',
    units: 'г/см³',
    min: 0
  },
  note: {
    component: 'FormFieldMinimal',
    maxLength: 1024,
    height: 200
  },
};

export const PROPERTY_LIST_INCOMING = {
  propertyValues: {
    A1: {
      qj: {},
      equipment_verification: {},
      valve_verification: {},
      qj_locked: {},
      qj_redirect: {},
      crimping: {},
      pressure_buffer: {},
      h_dynamic: {},
      pressure: {},
      amperage: {},
      loading: {},
      rotation_frequency: {},
      pressure_environment: {},
      temperature_environment: {},
      pressure_engine: {},
      temperature_engine: {},
      revision_svu: {},
      replace_svu: {},
      note: {}
    },
    A2: {
      union_action_type: {},
      rock_frequency: {},
      rotation_frequency_delta: {},
      wheeling: {},
      note: {}
    },
    A3: {
      watering: {},
      kvch: {},
      multi_component: {},
      carbonate: {},
      inhibitor: {},
      oil_component: {},
      complex: {},
      note: {}
    },
    A4: {
      voltage: {},
      amperage: {},
      loading: {},
      rotation_frequency: {},
      pressure_environment: {},
      temperature_environment: {},
      pressure_engine: {},
      temperature_engine: {},
      dmg_type: {},
      gathering: {},
      separate_expense: {},
      separate_humidity: {},
      separate_pressure: {},
      separate_temperature: {},
      note: {}
    },
    B1: {
      start_time: {},
      note: {}
    },
    B1A: {
      switch: {
        request_type: {
          KVU: {
            start_time_fact: {},
            h_static: {},
            pressure: {},
            pressure_environment: {},
            pressure_engine: {},
          },
          KVD: {
            start_time_fact: {},
            h_static: {},
            pressure: {},
            pressure_environment: {},
            pressure_engine: {},
          },
          KPD: {
            start_time_fact: {},
            h_static_pipe: {},
            pressure_pipe: {},
          }
        }
      },
      note: {}
    },
    B1B: {
      note: {}
    },
    B2: {
      time_completion: {},
      pump_washing: {},
      pump_crimping: {},
      amperage_before: {},
      loading_before: {},
      rotation_frequency: {},
      pressure_engine_before: {},
      pressure_environment_before: {},
      temperature_environment_before: {},
      temperature_engine_before: {},
      rotation_change: {},
      phase_check: {},
      h_static_after_stop: {},
      crimping_begin: {},
      crimping_end: {},
      crimping_duration: {},
      h_static_after_crimping: {},
      wash_pressure_begin: {},
      wash_pressure_end: {},
      h_dynamic_pressure_after_wash: {},
      h_dynamic_pressure_duration: {},
      qj_manual_after: {},
      amperage_after: {},
      voltage_after: {},
      pressure_engine_after: {},
      pressure_environment_after: {},
      temperature_environment_after: {},
      temperature_engine_after: {},
      note: {}
    },
    B3: {
      time_completion: {},
      pump_washing: {},
      pump_crimping: {},
      qj_manual_before: {},
      amperage_before: {},
      loading_before: {},
      rotation_frequency: {},
      pressure_engine_before: {},
      pressure_environment_before: {},
      temperature_environment_before: {},
      temperature_engine_before: {},
      h_static_after_stop: {},
      crimping_begin: {},
      crimping_end: {},
      crimping_duration: {},
      h_static_after_crimping: {},
      wash_pressure_begin: {},
      wash_pressure_end: {},
      h_dynamic_pressure_after_wash: {},
      h_dynamic_pressure_duration: {},
      qj_manual_after: {},
      amperage_after: {},
      voltage_after: {},
      pressure_engine_after: {},
      pressure_environment_after: {},
      temperature_environment_after: {},
      temperature_engine_after: {},
      note: {}
    },
    B4: {
      time_completion: {},
      dmg_before: {},
      treatment_datebegin: {},
      treatment_dateend: {},
      qn_volume_before_loss: {},
      qn_volume_before_collector: {},
      temperature_interval: {},
      pressure_adpm_begin: {},
      pressure_adpm_end: {},
      qj_manual: {},
      note: {}
    },
    B5: {
      time_completion: {},
      switch: {
        cycles: {
          "1": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {}
          },
          "2": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {},
            damping_volume_cycle_2: {},
            weight_cycle_2: {}
          },
          "3": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {},
            damping_volume_cycle_2: {},
            weight_cycle_2: {},
            damping_volume_cycle_3: {},
            weight_cycle_3: {}
          },
          "4": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {},
            damping_volume_cycle_2: {},
            weight_cycle_2: {},
            damping_volume_cycle_3: {},
            weight_cycle_3: {},
            damping_volume_cycle_4: {},
            weight_cycle_4: {},
          },
        }
      },
      liquid_probe: {},
      pressure_after_damping: {},
      note: {}
    },
    B6: {
      time_completion: {},
      preparing: {},
      mixture: {},
      note: {}
    },
    B7: {
      time_completion: {},
      note: {}
    },
    B8: {
      time_completion: {},
      h_dynamic: {},
      pressure: {},
      h_static: {},
      pressure_after: {},
      pressure_redundant: {},
      power_off: {},
      purge: {},
      requested_epu: {},
      requested_cnipr: {},
      note: {}
    },
    B9: {
      time_completion: {},
      worked: {},
      note: {}
    },
    B10: {
      time_completion: {},
      note: {}
    },
    B11: {
      time_completion: {},
      note: {}
    },
    B12: {
      time_completion: {},
      note: {}
    },
    B13: {
      time_completion: {},
      q: {},
      h_dynamic: {},
      pressure: {},
      amperage: {},
      voltage: {},
      loading: {},
      rotation_frequency: {},
      pressure_environment: {},
      pressure_engine: {},
      temperature_environment: {},
      temperature_engine: {},
      protection_correction: {},
      rotation_change: {},
      qj_reverse: {},
      amperage_reverse: {},
      loading_reverse: {},
      note: {}
    },
    C: {
      h_static: {},
      pressure: {},
      lock_available: {},
      service_area: {},
      balance_delta: {},
      note: {}
    },
    D1: {
      qj: {},
      h_dynamic: {},
      pressure: {},
      amperage: {},
      loading: {},
      rotation_frequency: {},
      pressure_engine: {},
      pressure_environment: {},
      temperature_engine: {},
      temperature_environment: {},
      duration: {},
      operation_mode: {},
      note: {}
    },
    D2: {
      qj: {},
      h_dynamic: {},
      pressure: {},
      amperage: {},
      loading: {},
      rotation_frequency: {},
      pressure_engine: {},
      pressure_environment: {},
      temperature_engine: {},
      temperature_environment: {},
      duration: {},
      operation_mode: {},
      note: {}
    },
    E1: {
      probe_time: {},
      note: {}
    },
    E2: {
      probe_time: {},
      note: {}
    },
    E3: {
      probe_time: {},
      puller_type_id: {},
      voltage: {},
      amperage: {},
      loading: {},
      rotation_frequency: {},
      pressure_environment: {},
      pressure_engine: {},
      temperature_environment: {},
      temperature_engine: {},
      note: {}
    },
    E4: {
      treatment_time: {},
      note: {}
    },
    E5: {
      obtain_time: {},
      note: {}
    },
    E6: {
      padding_time: {},
      note: {}
    },
    E7: {
      time_completion: {},
      h_dynamic: {},
      pressure: {},
      note: {}
    },
    E8: {
      time_completion: {},
      scraped: {},
      scrap_problem: {},
      note: {}
    },
    F: {
      probe_time: {},
      note: {}
    },
    G: {
      probe_time: {},
      note: {}
    }
  }
};

export const PROPERTY_LIST_OUTCOMING = {
  propertyValues: {
    A1: {
      requested: { 
        options:[
          'qj',
          'equipment_verification',
          'valve_verification', 
          'qj_locked',
          'qj_redirect',
          'crimping',
          'pressure_buffer',
          'h_dynamic|pressure',
          'amperage|loading',
          'rotation_frequency', 
          'pressure_environment|pressure_engine',
          'temperature_environment|temperature_engine',
          'revision_svu',
          'replace_svu',
          'ground_equipment_verification' // новый показатель
        ].map(x => ({ label: propertyConverter(x), value: x}))
      },
      note: {}
    },
    A2: {
      well_period_mode: {},
      union_action_type: {},
      union_dynamic: {},
      plunger_length: {},
      rock_frequency: {},
      rotation_frequency_delta: {},
      wheeling: {},
      note: {}
    },
    A3: {
      requested: { 
        options:[
          'watering',
          'kvch',
          'multi_component',
          'carbonate',
          'inhibitor',
          'oil_component',
          'complex'
        ].map(x => ({ label: propertyConverter(x), value: x}))
      },
      probe_volume: {},
      note: {}
    },
    A4: {
      requested: { 
        options:[
          'voltage',
          'amperage|loading',
          'rotation_frequency', 
          'pressure_environment|pressure_engine',
          'temperature_environment|temperature_engine',
          'dmg_type',
          'gathering',
          'separate_expense',
          'separate_humidity',
          'separate_pressure',
          'separate_temperature'
        ].map(x => ({ label: propertyConverter(x), value: x }))
      },
      note: {}
    },
    B1: {
      start_time: {},
      switch: {
        research: {
          KVU: {
            h_static: {},
            pressure: {},
            q: {},
            pressure_environment: {},
            pressure_engine: {},
            temperature_environment: {},
            temperature_engine: {},
            amperage: {},
            loading: {},
            rotation_frequency: {},
            h_dynamic_15: {},
            pressure_15: {},
            q_15: {},
            pressure_environment_15: {},
            pressure_engine_15: {},
            temperature_environment_15: {},
            temperature_engine_15: {},
            amperage_15: {},
            loading_15: {},
            rotation_frequency_15: {},
            h_dynamic_30: {},
            pressure_30: {},
            q_30: {},
            pressure_environment_30: {},
            pressure_engine_30: {},
            temperature_environment_30: {},
            temperature_engine_30: {},
            amperage_30: {},
            loading_30: {},
            rotation_frequency_30: {},
          },
          KVD: {
            h_static: {},
            pressure: {},
            q: {},
            pressure_environment: {},
            pressure_engine: {},
            temperature_environment: {},
            temperature_engine: {},
            amperage: {},
            loading: {},
            rotation_frequency: {},
            h_dynamic_15: {},
            pressure_15: {},
            q_15: {},
            pressure_environment_15: {},
            pressure_engine_15: {},
            temperature_environment_15: {},
            temperature_engine_15: {},
            amperage_15: {},
            loading_15: {},
            rotation_frequency_15: {}
          },
          NST: {
            q: {},
            pressure: {}
          },
          TKRS: {
            repair_type: {},
          },
          Other: {}
        }
      },
      pump_id: {},
      well_purpose: {},
      research: {},
      pump_depth: {},
      damping_volume: {},
      damping_weight: {},
      pressure_ecn_minimal: {},
      shank_length: {},
      note: {}
    },
    B1A: {
      start_time: {},
      request_type: {},
      note: {}
    },
    B1B: {
      start_time: {},
      treatment: {},
      repair_type: {},
      pump_typesize: {},
      union_diameter: {},
      switch: {
        treatment: {
          WellRepair: {
            pump_depth: {},
            damping_volume: {},
            damping_weight: {}
          }
        }
      },
      note: {}
    },  
    B2: {
      requested: {
        options: [
          'pump_washing',
          'pump_crimping',
          'rotation_change'
        ].map(x => ({ label: propertyConverter(x), value: x }))
      },
      time_completion: {},
      wash_volume: {},
      weight: {},
      called_agents: {},
      note: {}
    },
    B3: {
      requested: { 
        options:[
          'pump_washing',
          'pump_crimping'
        ].map(x => ({ label: propertyConverter(x), value: x }))
      },
      time_completion: {},
      wash_volume: {},
      weight: {},
      note: {}
    },
    B4: {
      requested: { 
        options:[
          'dmg_before',
          'treatment_datebegin',
          'treatment_dateend',
          'qn_volume_before_loss',
          'qn_volume_before_collector',
          'temperature_interval',
          'pressure_adpm_begin',
          'pressure_adpm_end',
          'qj_manual'
        ].map(x => ({ label: propertyConverter(x), value: x}))
      },
      time_completion: {},
      wash_volume: {},
      note: {}
    },
    B5: {
      requested: { 
        options:[
          'pressure_after_damping',
          'liquid_probe'
        ].map(x => ({ label: propertyConverter(x), value: x}))
      },
      time_completion: {},
      damping_volume: {},
      weight: {},
      cycles: {},
      switch: {
        cycles: {
          "1": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {}
          },
          "2": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {},
            damping_volume_cycle_2: {},
            weight_cycle_2: {}
          },
          "3": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {},
            damping_volume_cycle_2: {},
            weight_cycle_2: {},
            damping_volume_cycle_3: {},
            weight_cycle_3: {}
          },
          "4": {
            damping_volume_cycle_1: {},
            weight_cycle_1: {},
            damping_volume_cycle_2: {},
            weight_cycle_2: {},
            damping_volume_cycle_3: {},
            weight_cycle_3: {},
            damping_volume_cycle_4: {},
            weight_cycle_4: {},
          },
        }
      },
      note: {}
    },
    B6: {
      time_completion: {},
      requested: { 
        options:[
          'preparing',
          'mixture'
        ].map(x => ({ label: propertyConverter(x), value: x}))
      },
      note: {}
    },
    B7: {
      time_completion: {},
      pressure_crimping: {},
      note: {}
    },
    B8: {
      time_completion: {},
      request_type: {},
      requested: { 
        options:[
          'h_dynamic|pressure',
          'h_static',
          'pressure_after',
          'pressure_redundant',
          'purge',
          'requested_epu',
          'requested_cnipr'
        ].map(x => ({ label: propertyConverter(x), value: x }))
      },
      power_off: {},
      note: {}
    },
    B9: {
      time_completion: {},
      stop_reason: {},
      note: {}
    },
    B10: {
      time_completion: {},
      request_reason: {},
      note: {}
    },
    B11: {
      time_completion: {},
      request_reason: {},
      note: {}
    },
    B12: {
      contractor_id: {},
      brigade: {},
      time_completion: {},
      request_reason: {},
      note: {}
    },
    B13: {
      requested: { 
        options:[
          'q',
          'h_dynamic|pressure',
          'voltage',
          'amperage|loading',
          'rotation_frequency',
          'pressure_environment|pressure_engine',
          'temperature_environment|temperature_engine',
          'protection_correction',
          'rotation_change',
          'qj_reverse',
          'amperage_reverse|loading_reverse',
        ].map(x => ({ label: propertyConverter(x), value: x }))
      },
      time_completion: {},
      note: {}
    },
    C: {
      requested: { 
        options:[
          'h_static',
          'pressure',
          'lock_available',
          'service_area',
          'balance_delta'
        ].map(x => ({ label: propertyConverter(x), value: x }))
      },
      start_time_plan: {},
      brigade_type: {},
      brigade: {},
      well_area: {},
      note: {}
    },
    D1: {
      repair_type: {},
      event_type: {},
      pump_id: {},
      pump_depth: {}
    },
    D2: {
      repair_type: {},
      event_type: {},
      pump_id: {},
      pump_depth: {}
    },
    E1: {
      note: {}
    },
    E2: {
      note: {}
    },
    E3: {
      note: {}
    },
    E4: {
      note: {}
    },
    E5: {
      note: {}
    },
    E6: {
      note: {}
    },
    E7: {
      time_completion: {},
      note: {}
    },
    E8: {
      time_completion: {},
      note: {}
    },
    F: {
      note: {}
    },
    G: {
      note: {}
    }
  }
};

export const PROPERTY_LIST_CONTROLLED = {
  propertyValues: {
    D1: {
      repair_type: {},
      event_type: {},
      pump_id: {},
      pump_depth: {},
      qj:{},
      h_dynamic:{},
      amperage:{},
      loading:{},
      rotation_frequency: {},
      pressure_environment: {},
      pressure_engine: {},
      temperature_environment: {},
      temperature_engine: {},
      duration: {}
    },
    D2: {
      repair_type: {},
      event_type: {},
      pump_id: {},
      pump_depth: {}
    }
  }
};

export const trimDetails = function(ticketDetail) {
  if (Object.keys(PROPERTY_LIST_INCOMING.propertyValues).indexOf(ticketDetail.ticket_type) < 0) {
    return ticketDetail;
  }

  const ignore = ['id', 'well_id', 'membership_id', 'department_id', 'status', 'ticket_id', 'ticket_created', 'ticket_type'];
  const fields = [...Object.keys(PROPERTY_LIST_INCOMING.propertyValues[ticketDetail.ticket_type]), ...Object.keys(PROPERTY_LIST_OUTCOMING.propertyValues[ticketDetail.ticket_type])];
  const output = {};
  Object.keys(ticketDetail).filter(x => fields.indexOf(x) > -1 || ignore.indexOf(x) > -1).forEach(x => output[x] = ticketDetail[x]);

  return output;
};

export const API_ROOT = process.env.NODE_ENV === 'production' ? `http://c-kglupz/${process.env.HOST}/api` : "http://localhost:62105/api";