const closeDatePicker = 'Закрыть';
const focusStartDate = 'Выберите дату';
const clearDate = 'Очитсить дату';
const clearDates = 'Очистить даты';
const jumpToPrevMonth = 'Перемещение назад для перехода к предыдущему месяцу';
const jumpToNextMonth = 'Перемещение вперёд для перехода к следующему месяцу';
const keyboardShortcuts = 'Горячие клавиши';
const showKeyboardShortcutsPanel = 'Открыть панель горячих клавиш';
const hideKeyboardShortcutsPanel = 'Закрыть панель горячих клавиш';
const openThisPanel = 'Открыть панель';
const enterKey = 'Ввести ключ';
const leftArrowRightArrow = 'Стрелки влево и вправо';
const upArrowDownArrow = 'Стрелки вверх и вниз';
const pageUpPageDown = 'Клавиши PageUp и PageDown';
const homeEnd = 'Клавиши Home и End';
const escape = 'Клавиша Escape';
const questionMark = 'Помощь';
const selectFocusedDate = 'Отметить выделенную дату';
const moveFocusByOneDay = 'Переместиться назад (влево) или вперёд (вправо) на один день';
const moveFocusByOneWeek = 'Переместиться назад (влево) или вперёд (вправо) на одну неделю';
const moveFocusByOneMonth = 'Переключиться на месяцы';
const moveFocustoStartAndEndOfWeek = 'Переместиться в начало или в конец недели';
const returnFocusToInput = 'Вернуться к окну редактирования даты';
const keyboardNavigationInstructions = `Нажмите стрелку вниз для взаимодействия с календарём и выделения даты.
Нажмите на знак вопроса для отображения информации о горячих клавишах.`;

// eslint-disable-next-line camelcase
const chooseAvailableStartDate = ({ date }) => `Выберите дату начала ${date}. Она доступна.`;

// eslint-disable-next-line camelcase
const chooseAvailableEndDate = ({ date }) => `Выберите дату окончания ${date} . Она доступна.`;
const chooseAvailableDate = ({ date }) => date;
const dateIsUnavailable = ({ date }) => `Недоступная дата. ${date}`;

export default {
  closeDatePicker,
  focusStartDate,
  clearDate,
  clearDates,
  jumpToPrevMonth,
  jumpToNextMonth,
  keyboardShortcuts,
  showKeyboardShortcutsPanel,
  hideKeyboardShortcutsPanel,
  openThisPanel,
  enterKey,
  leftArrowRightArrow,
  upArrowDownArrow,
  pageUpPageDown,
  homeEnd,
  escape,
  questionMark,
  selectFocusedDate,
  moveFocusByOneDay,
  moveFocusByOneWeek,
  moveFocusByOneMonth,
  moveFocustoStartAndEndOfWeek,
  returnFocusToInput,
  keyboardNavigationInstructions,

  chooseAvailableStartDate,
  chooseAvailableEndDate,
  dateIsUnavailable,
};

export const DateRangePickerPhrases = {
  closeDatePicker,
  clearDates,
  focusStartDate,
  jumpToPrevMonth,
  jumpToNextMonth,
  keyboardShortcuts,
  showKeyboardShortcutsPanel,
  hideKeyboardShortcutsPanel,
  openThisPanel,
  enterKey,
  leftArrowRightArrow,
  upArrowDownArrow,
  pageUpPageDown,
  homeEnd,
  escape,
  questionMark,
  selectFocusedDate,
  moveFocusByOneDay,
  moveFocusByOneWeek,
  moveFocusByOneMonth,
  moveFocustoStartAndEndOfWeek,
  returnFocusToInput,
  keyboardNavigationInstructions,
  chooseAvailableStartDate,
  chooseAvailableEndDate,
  dateIsUnavailable,
};

export const DateRangePickerInputPhrases = {
  focusStartDate,
  clearDates,
  keyboardNavigationInstructions,
};

export const SingleDatePickerPhrases = {
  closeDatePicker,
  clearDate,
  jumpToPrevMonth,
  jumpToNextMonth,
  keyboardShortcuts,
  showKeyboardShortcutsPanel,
  hideKeyboardShortcutsPanel,
  openThisPanel,
  enterKey,
  leftArrowRightArrow,
  upArrowDownArrow,
  pageUpPageDown,
  homeEnd,
  escape,
  questionMark,
  selectFocusedDate,
  moveFocusByOneDay,
  moveFocusByOneWeek,
  moveFocusByOneMonth,
  moveFocustoStartAndEndOfWeek,
  returnFocusToInput,
  keyboardNavigationInstructions,
  chooseAvailableDate,
  dateIsUnavailable,
};

export const SingleDatePickerInputPhrases = {
  clearDate,
  keyboardNavigationInstructions,
};

export const DayPickerPhrases = {
  jumpToPrevMonth,
  jumpToNextMonth,
  keyboardShortcuts,
  showKeyboardShortcutsPanel,
  hideKeyboardShortcutsPanel,
  openThisPanel,
  enterKey,
  leftArrowRightArrow,
  upArrowDownArrow,
  pageUpPageDown,
  homeEnd,
  escape,
  questionMark,
  selectFocusedDate,
  moveFocusByOneDay,
  moveFocusByOneWeek,
  moveFocusByOneMonth,
  moveFocustoStartAndEndOfWeek,
  returnFocusToInput,
  chooseAvailableStartDate,
  chooseAvailableEndDate,
  chooseAvailableDate,
  dateIsUnavailable,
};

export const DayPickerKeyboardShortcutsPhrases = {
  keyboardShortcuts,
  showKeyboardShortcutsPanel,
  hideKeyboardShortcutsPanel,
  openThisPanel,
  enterKey,
  leftArrowRightArrow,
  upArrowDownArrow,
  pageUpPageDown,
  homeEnd,
  escape,
  questionMark,
  selectFocusedDate,
  moveFocusByOneDay,
  moveFocusByOneWeek,
  moveFocusByOneMonth,
  moveFocustoStartAndEndOfWeek,
  returnFocusToInput,
};

export const DayPickerNavigationPhrases = {
  jumpToPrevMonth,
  jumpToNextMonth,
};

export const CalendarDayPhrases = {
  chooseAvailableDate,
  dateIsUnavailable,
};