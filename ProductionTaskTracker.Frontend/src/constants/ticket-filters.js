//Без фильтра
export const TICKET_FILTER_ALL = 'All';

//Текущие
export const TICKET_FILTER_ACTUAL = 'Actual';

//Вызов представителя
export const TICKET_FILTER_EXECUTOR = 'Executor';

//Вывод на режим
export const TICKET_FILTER_VNR = 'Vnr';

//Информационные
export const TICKET_FILTER_INFORMATION = 'Information';

//Истекшие
export const TICKET_FILTER_EXPIRED = 'Expired';