import {
  A1, A2, A3, A4,
  B1, B1A, B1B, B2, B3, B4, B5, B6, B7, B8, B9, B10, B11, B12, B13,
  C,
  D1, D2, E1, E2, E3, E4, E5, E6, E7, E8,
  F, 
  G
} from '../constants/ticket-types';

export default function(ticketType) {
  switch (ticketType) {
    case A1:
        return "1.1 Отклонение от режима работы";
    case A2:
        return "1.2 Изменение режима работы";
    case A3:
        return "1.3 Отбор проб";
    case A4:
        return "1.4 Снятие данных ТМС, ДМГ";
    case B1:
        return "2.1 Запуск скважины";
    case B1A:
        return "2.1.1 Запуск нефтяной скважины";
    case B1B:
        return "2.1.2 Запуск нагнетательной скважины";
    case B2:
        return "2.2 Проведение мероприятий согласно приказам№ 354";
    case B3:
        return "2.3 Проведение промывки, опрессовки ГНО";
    case B4:
        return "2.4 Проведение обработки скважины горячей нефтью";
    case B5:
        return "2.5 Глушение скважины";
    case B6:
        return "2.6 Хим. обработка КЗХ";
    case B7:
        return "2.7 Опрессовка ГНО";
    case B8:
        return "2.8 Остановка скважины на исследование";
    case B9:
        return "2.9 Остановка скважины по технологическим причинам";
    case B10:
        return "2.10 Комиссионный разбор АГЗУ";
    case B11:
        return "2.11 Комиссионный разбор показаний СВУ";
    case B12:
        return "2.12 Вызов в бригаду (ТРС, КРС, Бурения, хим. Звена, партии ГИС)";
    case B13:
        return "2.13 Работа с ЭПУ";
    case C:
        return "3 Подготовка скважины под ТРС, КРС, ГИС, ЦНИПР, хим. Звено";
    case D1:
        return "4.1 Скважины ВНР";
    case D2:
        return "4.2 Скважины подконтрольные";
    case E1:
        return "5.1 Отбор проб на воду по графику";
    case E2:
        return "5.2 Отбор проб на КВЧ по графику";
    case E3:
        return "5.3 Снятие ТМС по графику";
    case E4:
        return "5.4 Проведение обработок скребком по графику";
    case E5:
        return "5.5 Снятие динамограмм по графику";
    case E6:
        return "5.6 Отбивка уровней по графику";
    case E7:
        return "5.7 Отбитие контрольного Нд";
    case E8:
        return "5.8 Прогнать контрольный скребок";
    case F:
        return "6 Информационная заявка от цеха в ЦИО";
    case G:
        return "7 Информационная заявка от ЦИО в цех";
    default:
        return null;
  }
}