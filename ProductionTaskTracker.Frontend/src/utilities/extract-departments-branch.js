export default function extractDepartmentsBranch(departmentID, departments) {
  const output = [];

  let scanline = departments.filter(x => x.id == departmentID)[0];
  if (!scanline) {
    return [];
  }

  output.push(scanline.id);

  while (scanline.parent_id !== null) {
    scanline = departments.filter(x => x.id == scanline.parent_id)[0];
    output.push(scanline.id);
  }

  return output;
}