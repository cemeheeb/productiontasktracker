import moment from 'moment';

export default function (value) {
  return typeof value !== 'undefined' && value !== null ? moment(value).format('HH:mm') : '';
}