import {
  CREATED,
  FILED,
  ACCEPTED,
  REJECTED,
  AGREED,
  PROGRESS,
  PROGRESS_READY,
  PROGRESS_CONFIRMED,
  PROGRESS_REJECTED,
  COMPLETED,
  VISITED,
  CANCELLED
} from '../constants/ticket-statuses';

export default function(ticketStatus) {
  switch (ticketStatus) {
    case CREATED:
      return 'ticket-type-created';
    case FILED:
      return 'ticket-type-filed';
    case ACCEPTED:
      return 'ticket-type-accepted';
    case REJECTED:
      return 'ticket-type-rejected';
    case AGREED:
      return 'ticket-type-agreed';
    case PROGRESS:
      return 'ticket-type-progress';
    case PROGRESS_READY:
      return 'ticket-type-progress-ready';
    case PROGRESS_CONFIRMED:
      return 'ticket-type-progress-confirmed';
    case PROGRESS_REJECTED:
      return 'ticket-type-progress-rejected';
    case COMPLETED:
      return 'ticket-type-completed';
    case VISITED:
      return 'ticket-type-visited';
    case CANCELLED:
      return 'ticket-type-cancelled';
  }
}