export function encodeUri(value) {
  const base64 = btoa(value);
  return escape(base64);
}