import {
  CREATED,
  FILED,
  ACCEPTED,
  REJECTED,
  AGREED,
  PROGRESS,
  PROGRESS_READY,
  PROGRESS_CONFIRMED,
  PROGRESS_REJECTED,
  COMPLETED,
  VISITED,
  CANCELLED
} from '../constants/ticket-statuses';

export default function(ticketStatus) {
  switch (ticketStatus) {
    case CREATED:
      return 'Созданная';
    case FILED:
      return 'Поданная';
    case ACCEPTED:
      return 'Принятая';
    case REJECTED:
      return 'Отклоненная';
    case AGREED:
      return 'Согласованная';
    case PROGRESS:
      return 'В работе';
    case PROGRESS_READY:
      return 'Отработано';
    case PROGRESS_CONFIRMED:
      return 'Подтвержденная';
    case PROGRESS_REJECTED:
      return 'На доработке';
    case COMPLETED:
      return 'Выполненная';
    case VISITED:
      return 'Ознакомленная';
    case CANCELLED:
      return 'Снятая';
  }
}