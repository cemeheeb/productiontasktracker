export const mapDictionaryToRoute = (dictionary) => {
  switch (dictionary) {
    case "repair-type-e":
      return "RTE";
    case "repair-type-s":
      return "RTS";
    case "puller-type":
      return "PT_";
  }
};

export const mapRouteToDictionary = (dictionary) => {
  switch (dictionary) {
    case "RTE":
      return "repair-type-e";
    case "RTS":
      return "repair-type-s";
    case "PT_":
      return "puller-type";
  }
};