const dictionary = {
  'h_dynamic,pressure': 'Нд / Рзатр',
  'amperage,loading': 'Ток / Загрузка',
  'pressure_environment,pressure_engine': 'Рср / Рм',
  'temperature_environment,temperature_engine': 'tср / tм',
  'pressure_engine_before,pressure_environment_before': 'Рср / Рм до',
  'temperature_engine_before,temperature_environment_before': 'tср / tм до',
  'pressure_engine_after,pressure_environment_after': 'Рср / Рм после',
  'temperature_engine_after,temperature_environment_after': 'tср / tм после'
};

export default function (propertyName) {
    return dictionary[propertyName];
}
