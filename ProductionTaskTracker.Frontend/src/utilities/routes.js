import moment from 'moment';

const filterActual = function (routes, timestamp = moment()) {
  return routes.filter(x => timestamp > moment(x.date_begin) && (typeof x.date_end === 'undefined' || x.date_end === null || timestamp < moment(x.date_end)));
};

export { filterActual };