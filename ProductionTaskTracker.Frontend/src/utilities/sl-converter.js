import {
  SL1,
  SL2,
  SL3
} from '../constants/sl';

export default function(level) {
  switch (level) {
    case SL1:
      return 'SL1';
    case SL2:
      return 'SL2';
    case SL3:
      return 'SL3';
  }
}