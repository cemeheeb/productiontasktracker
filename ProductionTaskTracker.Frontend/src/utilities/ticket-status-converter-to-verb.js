import {
  CREATED,
  FILED,
  ACCEPTED,
  REJECTED,
  AGREED,
  PROGRESS,
  PROGRESS_READY,
  PROGRESS_CONFIRMED,
  PROGRESS_REJECTED,
  COMPLETED,
  VISITED,
  CANCELLED
} from '../constants/ticket-statuses';

export default function(ticketStatus) {
  switch (ticketStatus) {
    case CREATED:
      return 'Создать';
    case FILED:
      return 'Подать';
    case ACCEPTED:
      return 'Принять';
    case REJECTED:
      return 'Отклонить';
    case AGREED:
      return 'Согласовать';
    case PROGRESS:
      return 'В работу';
    case PROGRESS_READY:
      return 'Отработано';
    case PROGRESS_CONFIRMED:
      return 'Подтвердить';
    case PROGRESS_REJECTED:
      return 'На доработку';
    case COMPLETED:
      return 'Выполнено';
    case VISITED:
      return 'Ознакомлен';
    case CANCELLED:
      return 'Снять';
  }
}