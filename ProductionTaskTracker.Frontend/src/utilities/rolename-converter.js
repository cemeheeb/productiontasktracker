export default function(propertyName) {
  const dictionary = {
    director_cio:"Руководитель ЦИО",
    subdirector_cio:"Заместитель руководителя ЦИО",
    dispatcher_cio:"Диспетчер ЦИО",
    specialist_cio:"Специалист ЦИО",
    specialist_dispatcher_cio:"Ответственный специалист группы ЦИО",
    superviser_cio:"Начальник смены ЦИО",
    director_cdng:"Руководитель ЦДНГ",
    manager_cdng:"Лин. руководитель ЦДНГ",
    dispatcher_cdng:"Диспетчер ЦДНГ",
    specialist_cdng:"Исполнитель ЦДНГ"
  };

  return dictionary[propertyName];
}