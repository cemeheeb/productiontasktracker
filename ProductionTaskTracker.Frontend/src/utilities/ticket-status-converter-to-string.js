import {
  CREATED,
  FILED,
  ACCEPTED,
  REJECTED,
  AGREED,
  PROGRESS,
  PROGRESS_READY,
  PROGRESS_CONFIRMED,
  PROGRESS_REJECTED,
  COMPLETED,
  VISITED,
  CANCELLED
} from '../constants/ticket-statuses';

export default function(ticketStatus) {
  switch (ticketStatus) {
    case CREATED:
      return 'Created';
    case FILED:
      return 'Filed';
    case ACCEPTED:
      return 'Accepted';
    case REJECTED:
      return 'Rejected';
    case AGREED:
      return 'Agreed';
    case PROGRESS:
      return 'Progress';
    case PROGRESS_READY:
      return 'ProgressReady';
    case PROGRESS_CONFIRMED:
      return 'ProgressConfirmed';
    case PROGRESS_REJECTED:
      return 'ProgressRejected';
    case COMPLETED:
      return 'Completed';
    case VISITED:
      return 'Visited';
    case CANCELLED:
      return 'Cancelled';
  }
}