import PropTypes from 'prop-types';
import React from 'react';

import { Field } from 'redux-form/immutable';

import { SelectRedux } from './select-redux';
import { FormFieldMinimal } from './form-field-minimal';

export class WizardFormWell extends React.Component {
  constructor(props) {
    super(props);

    const shopDepartments = props.session.routes.map(x => x.department_id_to).filter((v, i, a) => a.indexOf(v) === i).sort((a, b) => a > b ? 1 : (a < b ? -1 : 0));
    this.state = {
      shops: shopDepartments.map(x => props.session.departments.filter(department => department.id === x)[0]),
      shop: null,
      field: null,
      cluster: null,
      code: null
    };
  }

  componentDidMount() {
    this.props.actions.actionDictionaryFieldFilter();
  }

  render() {
    return (
      <div className="col-md-12">
        <div className="form-group">
          <label>Цех</label>
          <div className="input-group">
            <Field name="shop"
              component={SelectRedux}
              options={this.state.shops}
              valueKey="shop_code"
              labelKey="display_name"
              value={this.props.inputData.shop}
              placeholder="Выберите цех"
              isSearchable={true}
            />
          </div>
        </div>
        <div className="form-group">
          <label>Месторождение</label>
          <div className="input-group">
            <Field name="field"
                component={SelectRedux}
                options={this.props.session.fields}
                labelKey="name"
                valueKey="code"
                value={this.props.inputData.field}
                placeholder="Выберите месторождение"
                isSearchable={true}
              />
          </div>
        </div>
        <div className="form-group">
          <label>Куст</label>
          <div className="input-group">
            <Field name="cluster"
                component={FormFieldMinimal}
                labelKey="display_name"
                valueKey="id"
                value={this.props.inputData.cluster}
                placeholder="Введите куст"
              />
          </div>
        </div>
        <div className="form-group">
          <label>Код скважины</label>
          <div className="input-group">
            <Field name="id"
                component={FormFieldMinimal}
                value={this.props.inputData.id}
                placeholder="Введите код скважины"
              />
          </div>
        </div>
        <div className="form-group">
          <label>Номер скважины</label>
          <div className="input-group">
            <Field name="code"
                component={FormFieldMinimal}
                value={this.props.inputData.code}
                placeholder="Введите номер скважины"
              />
          </div>
        </div>
      </div>
    );
  }
}

WizardFormWell.propTypes = {
  actions: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
  inputData: PropTypes.shape({
    id: PropTypes.number,
    shop: PropTypes.string,
    field: PropTypes.string,
    cluster: PropTypes.string,
    code: PropTypes.string
  }).isRequired
};