import PropTypes from 'prop-types';
import React from 'react';

import { HistoryPageHeader } from './history-page-header';
import { HistoryTable } from './history-table';

export class HistoryPage extends React.Component {
  constructor(props) {
    super(props);

    this.onModalDetailClose = this.onModalDetailClose.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);
    this.onRefreshData = this.onRefreshData.bind(this);
    this.invalidateData = this.invalidateData.bind(this);

    this.state = {
      dateBegin: props.session.historyDateBegin,
      dateEnd: props.session.historyDateEnd,
      data: [],
      index: 1,
      size: 30,
      sizeTotal: 0,
      isBusy: false
    };
  }

  componentDidMount() {
    this.props.actions.actionDictionaryWellFilter('', 0, 30);
  }

  onPageChange(index, size) {
    this.setState({ index, size, isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onSizePerPageList(size) {
    this.setState({ size }, () => {
      this.invalidateData();
    });
  }

  onRefreshData(wellID, dateBegin, dateEnd) {
    this.setState({wellID, dateBegin, dateEnd, index: 1, isBusy: true}, () => {
      this.invalidateData(wellID);
    });
  }

  invalidateData(wellID) {
    if (typeof wellID === 'undefined' || wellID === null) {
      this.setState({ data: [] });
      return;
    }

    const { actions: { actionHistoryFilter } } = this.props;
    const { dateBegin, dateEnd, index, size } = this.state;

    this.setState({ isBusy: true }, () => {
      actionHistoryFilter(wellID, dateBegin, dateEnd, index, size)
        .then(response => {
          this.setState({ isBusy: false });

          if (response.error) {
            return;
          }

          const { data, size_total } = response.payload;
          this.setState({ data, sizeTotal: size_total });
        });
    });
  }

  onModalDetailClose() {
    this.setState({ isModalDetailOpen: false });
  }

  render() {
    const { session, actions } = this.props;
    const { data, index, size, sizeTotal } = this.state;
    return (
      <div>
        <HistoryPageHeader {...{
          session,
          actions,
          handlers: {
            onRefreshData: this.onRefreshData
          }
        }} />
        <div className="container-fluid">
          <div className="wrapper wrapper-content">
            <HistoryTable {...{
              data,
              handlers: {
                onPageChange: this.onPageChange,
                onSizePerPageList: this.onSizePerPageList
              },
              index,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
              size,
              sizeTotal
            }}
            />
          </div>
        </div>
      </div>
    );
  }
}

HistoryPage.propTypes = {
  session: PropTypes.shape({
    historyDateBegin: PropTypes.object.isRequired,
    historyDateEnd: PropTypes.object.isRequired
  }).isRequired,
  actions: PropTypes.shape({
    actionToastrPush: PropTypes.func.isRequired,
    actionDictionaryWellFilter: PropTypes.func.isRequired,
    actionHistoryFilter: PropTypes.func.isRequired
  }).isRequired
};