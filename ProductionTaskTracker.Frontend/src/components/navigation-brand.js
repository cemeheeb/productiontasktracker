import React from 'react';
import PropTypes from 'prop-types';

export class NavigationBrand extends React.Component {
  render() {
    return (
      <div className="navigation-brand">
        СУПЗ
      </div>
    );
  }
}

NavigationBrand.propTypes = {
  children: PropTypes.node
};
