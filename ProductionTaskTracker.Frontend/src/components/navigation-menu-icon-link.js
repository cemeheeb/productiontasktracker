import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';

export class NavigationMenuIconLink extends React.Component {
  render() {
    const { icon, to, children } = this.props;

    return (
        <li className="nav navbar-nav">
            <NavLink exact to={to}><i className={classnames("fa", icon)} />{children}</NavLink>
        </li>
    );
  }
}

NavigationMenuIconLink.propTypes = {
  icon: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.node
};


