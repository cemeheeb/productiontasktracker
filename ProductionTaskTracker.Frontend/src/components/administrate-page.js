import PropTypes from 'prop-types';
import React from 'react';
import { Switch } from 'react-router-dom';
import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { RouteExtended } from './route-extended';
import { AdministratePageHeader } from './administrate-page-header';
import { AdministrateMembershipsPage } from './administrate-memberships-page';
import { AdministrateDepartmentsPage } from './administrate-departments-page';
import { AdministrateDictionariesPage } from './administrate-dictionaries-page';

export class AdministratePage extends React.Component {
  render() {
    const { session, actions, match } = this.props;
    
    return (
      <div>
        <AdministratePageHeader />
        <div className="loading-bar-wrapper"><LoadingBar className="loading-bar"/></div>
        <div className="container-fluid">
          <div className="wrapper wrapper-content">
            <Switch>
              <RouteExtended path="/administrate/memberships" component={AdministrateMembershipsPage}  {...{ session, actions, match }} />
              <RouteExtended path="/administrate/departments" component={AdministrateDepartmentsPage}  {...{ session, actions, match }} />
              <RouteExtended path="/administrate/dictionaries" component={AdministrateDictionariesPage}  {...{ session, actions, match }} />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

AdministratePage.propTypes = {
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};