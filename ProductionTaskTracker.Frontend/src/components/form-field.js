import PropTypes from 'prop-types';
import React from 'react';

export class FormField extends React.Component {
  render() {
    const { input, label, prefix, postfix, placeholder, type, readOnly, tabIndex, meta: { touched, error, warning } } = this.props;
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          { !prefix || <span className="input-group-addon">{prefix}</span> }
          <input className="form-control" {...input} placeholder={placeholder} type={type} readOnly={readOnly} tabIndex={tabIndex} />
          { !postfix || <span className="input-group-addon">{postfix}</span> }
        </div>
        {touched && ((error && <span className="validation-error">{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    );
  }
}

FormField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  prefix: PropTypes.string,
  postfix: PropTypes.string,
  placeholder: PropTypes.string,
  icon: PropTypes.string,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object,
  tabIndex: PropTypes.string,
  readOnly: PropTypes.bool
};

FormField.defaultProps = {
    type: "text"
};
