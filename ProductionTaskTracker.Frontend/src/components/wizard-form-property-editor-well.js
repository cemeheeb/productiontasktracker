import PropTypes from 'prop-types';
import React from 'react';

import debounce from 'debounce-promise';

import { Field } from 'redux-form/immutable';

import { FormFieldMinimal } from './form-field-minimal';
import { FormDateTimePicker } from './form-date-time-picker';
import { FormFieldNumeric } from './form-field-numeric';
import { FormFieldCheckbox } from './form-field-checkbox';
import { FormFieldCheckboxGroup } from './form-field-checkbox-group';
import { FormFieldDropzone } from './form-field-dropzone';

import { SelectRedux } from './select-redux';
import { SelectReduxArray } from './select-redux-array';
import { SelectReduxAsync } from './select-redux-async';

import propertyConverter from '../utilities/property-converter';
import { filterActual } from '../utilities/routes';
import { PROPERTY_TYPE_LIST } from '../constants/configuration';

export class WizardFormPropertyEditorWell extends React.Component {
  constructor(props) {
    super(props);
    this.actionDictionaryPumpFilter = this.actionDictionaryPumpFilter.bind(this);
    this.actionDictionaryContractorFilter = this.actionDictionaryContractorFilter.bind(this);
    this.actionDictionaryPumpSearchFilter = debounce(this.actionDictionaryPumpSearchFilter.bind(this), 500);
    this.actionDictionaryContractorSearchFilter = debounce(this.actionDictionaryContractorSearchFilter.bind(this), 500);
  }

  componentDidMount() {
    const { pump_id, contractor_id } = this.props.inputData;

    if (typeof pump_id !== 'undefined' && pump_id !== null) {
      this.props.actions.actionDictionaryPumpFilter(pump_id).then(
        x => {
          this.setState({ pump: x.payload.title });
        }
      );
    }
    if (typeof contractor_id !== 'undefined' && contractor_id !== null) {
      this.props.actions.actionDictionaryContractorFilter(contractor_id).then(
        x => {
          this.setState({ contractor: x.payload.name });
        }
      );
    }
  }

  actionDictionaryPumpFilter() {
    if (typeof this.props.inputData.pump_id !== 'undefined' && this.props.inputData.pump_id !== null) {
      return this.props.actions.actionDictionaryPumpFilter(this.props.inputData.pump_id);
    }

    return Promise.resolve({ payload: [] });
  }

  actionDictionaryContractorFilter() {
    if (typeof this.props.inputData.contractor_id !== 'undefined' && this.props.inputData.contractor_id !== null) {
      return this.props.actions.actionDictionaryContractorFilter(this.props.inputData.contractor_id);
    }

    return Promise.resolve({ payload: [] });
  }

  actionDictionaryPumpSearchFilter(search) {
    if ((typeof search === 'undefined' || search === '' || search === null) && typeof this.props.inputData.pump_id !== 'undefined' && this.props.inputData.pump_id !== null) {
      return Promise.resolve({ options: [{ id: this.props.inputData.pump_id, title: this.state.pump }] });
    }

    return this.props.actions.actionDictionaryPumpSearchFilter(search, 0, 20).then(x => {
      return Promise.resolve({ options: x.payload.data });
    });
  }

  actionDictionaryContractorSearchFilter(search) {
    if ((typeof search === 'undefined' || search === '' || search === null) && typeof this.props.inputData.contractor_id !== 'undefined' && this.props.inputData.contractor_id !== null) {
      return Promise.resolve({ options: [{ id: this.props.inputData.contractor_id, name: this.state.contractor }] });
    }

    return this.props.actions.actionDictionaryContractorSearchFilter(search, 0, 20).then(x => {
      return Promise.resolve({ options: x.payload.data });
    });
  }
  
  render() {
    const {
      session,
      propertyValues,
      inputData
    } = this.props;

    let propertyValuesReady = Object.assign({}, { ...propertyValues });
    if (typeof propertyValues !== 'undefined' && typeof propertyValues.switch !== 'undefined' && propertyValues.switch !== null) {
      Object.keys(propertyValues.switch).forEach(x => {
        const switchCaseProperties = propertyValues.switch[x][inputData[x]];
        propertyValuesReady = Object.assign(propertyValuesReady, { ...switchCaseProperties });
      });
    }

    if (typeof inputData.ticket_type === 'undefined' || inputData.ticket_type == null) {
      return null;
    }

    const components = [];
    Object.keys(propertyValuesReady).map(propertyName => {
      if (['well_id', 'mf_position', 'switch'].indexOf(propertyName) > -1) {
        return;
      }

      const routes = filterActual(session.routes).filter(x => x.ticket_class.indexOf(inputData.ticket_type[0]) > -1);
      if (propertyName !== 'note' && session.departmentIDs.indexOf(routes[0].executor_id) < 0  && inputData.status > 1) {
        return;
      }

      const propertyData = Object.assign({}, { ...propertyValuesReady[propertyName] });
      const propertyType = Object.assign({}, { ...PROPERTY_TYPE_LIST[propertyName] }, { ...propertyData });

      const componentName = propertyType.component;
      delete propertyType['component'];

      let component = null;
      let componentProperties = {};
      switch (componentName) {
        case 'FormDateTimePicker': {
          component = FormDateTimePicker;
          break;
        }
        case 'SelectRedux': {
          component = SelectRedux;
          break;
        }
        case 'SelectReduxArray': {
          component = SelectReduxArray;
          break;
        }
        case 'SelectContractor': {
          component = SelectReduxAsync;
          componentProperties = {
            handlers: { initialize: this.actionDictionaryContractorFilter, search: this.actionDictionaryContractorSearchFilter },
            searchable: true,
            valueKey: "id",
            labelKey: "name",
            value: inputData.contractor_id
          };
          break;
        }
        case 'SelectPump': {
          component = SelectReduxAsync;
          componentProperties = {
            handlers: { initialize: this.actionDictionaryPumpFilter, search: this.actionDictionaryPumpSearchFilter },
            searchable: true,
            valueKey: "id",
            labelKey: "title",
            value: inputData.pump_id
          };
          break;
        }
        case 'SelectPullerType': {
          component = SelectRedux;
          componentProperties = {
            options: this.props.pullerTypes,
            isSearchable: false,
            valueKey: "id",
            labelKey: "name",
            value: inputData.puller_type_id
          };
          break;
        }
        case 'FormFieldNumeric': {
          component = FormFieldNumeric;
          break;
        }
        case 'FormFieldCheckbox': {
          component = FormFieldCheckbox;
          break;
        }
        case 'CheckboxGroup': {
          component = FormFieldCheckboxGroup;
          break;
        }
        default: {
          component = FormFieldMinimal;
          componentProperties = {
            membershipID: this.props.session.membershipID,
            actions: this.props.actions
          };
          break;
        }
      }

      components.push(
        <div key={propertyName} className="form-group" tabIndex={components.length + 1}>
          <label className="col-md-6 control-label">{propertyConverter(propertyName)} {propertyType.units ? `[${propertyType.units}]` : ''}</label>
          <div className="col-md-6">
            <Field name={propertyName} component={component} {...propertyType} {...componentProperties}  />
          </div>
        </div>
      );
    });

    components.push(
      <div key="attachments" className="form-group">
        <label className="col-md-6 control-label">Вложения:</label>
        <div className="col-md-6">
          <Field name="attachments" component={FormFieldDropzone} actions={this.props.actions} />
        </div>
      </div>
    );

    return (
      <div className="form-container">
        <div className="col-md-12 ">
          {components}
        </div>
      </div>
    );
  }
}

WizardFormPropertyEditorWell.propTypes = {
  form: PropTypes.string.isRequired,
  propertyValues: PropTypes.object,
  session: PropTypes.object.isRequired,
  actions: PropTypes.shape({
    actionDictionaryPumpSearchFilter: PropTypes.func.isRequired,
    actionDictionaryPumpFilter: PropTypes.func.isRequired,
    actionDictionaryContractorSearchFilter: PropTypes.func.isRequired,
    actionDictionaryContractorFilter: PropTypes.func.isRequired
  }),
  inputData: PropTypes.object.isRequired,
  pullerTypes: PropTypes.array.isRequired,
  attachments: PropTypes.array.isRequired,
  label: PropTypes.string
};