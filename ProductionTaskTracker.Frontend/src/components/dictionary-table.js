import PropTypes from 'prop-types';
import React from 'react';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Gateway } from 'react-gateway';

export class DictionaryTable extends React.Component {
  constructor(props) {
    super(props);
    this.buttonFormatter = this.buttonFormatter.bind(this);
  }

  buttonFormatter(cell, row) {
    return (
      <div>
        <div className="pull-left">
          <button type="button" className="btn btn-warning" title="Изменить" onClick={() => this.props.handlers.onEdit(row)} ><i className="fa fa-bars" /></button>
          <button type="button" className="btn btn-danger" title="Удалить" onClick={() => this.props.handlers.onRemove(row)} ><i className="fa fa-times" /></button>
        </div>
        {cell}
      </div>
    );
  }

  render() {
    const { index, size, sizeTotal, handlers: { onAdd, onRefreshData, onPageChange, onSizePerPageList }, data } = this.props;

    return (
      <div>
        <BootstrapTable hover condensed remote pagination
          data={data}
          fetchInfo={{ dataTotalSize: sizeTotal }}
          trClassName={this.trClassFormat}
          options={{ 
            page: index,
            sizePerPage: size,
            onPageChange,
            onSizePerPageList,
            expandRowBgColor: 'rgb(242, 255, 163)',
            onRowDoubleClick: this.onEdit,
            noDataText: 'Нет данных',
            paginationPosition: 'top'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
        >
          <TableHeaderColumn
            width="150"
            dataField="id"
            dataAlign="right"
            dataFormat={this.buttonFormatter} 
            isKey={true}
          >Код</TableHeaderColumn>

          <TableHeaderColumn
            dataField="name"
          >Название</TableHeaderColumn>
        </BootstrapTable>
        
        <Gateway into="administrative-toolbar">
          <button type="button" title="Добавить" className="btn btn-primary" onClick={onAdd}><i className="fa fa-plus" /></button>
          <button type="button" title="Обновить" className="btn btn-error" onClick={onRefreshData}><i className="fa fa-refresh" /></button>
        </Gateway>
      </div>
    );
  }
}

DictionaryTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
    onAdd: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};
