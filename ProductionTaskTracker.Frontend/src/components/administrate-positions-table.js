import PropTypes from 'prop-types';
import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import dateFormatter from '../utilities/date-formatter';

export class AdministratePositionsTable extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <div className="administrative-positions-table" >
        <BootstrapTable hover condensed remote pagination
          data={data}
          options={{
            expandRowBgColor: 'rgb(242, 255, 163)',
            noDataText: 'Нет данных'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
          selectRow={{
            mode: 'radio',
            clickToSelect: true,
            className: 'table-row-selected',
            hideSelectColumn: true
          }}
        >
          <TableHeaderColumn width="100" dataField="id" isKey={true}>Код</TableHeaderColumn>
          <TableHeaderColumn width="150" dataField="fullname" >ФИО</TableHeaderColumn>
          <TableHeaderColumn width="150" dataField="username" >Логин</TableHeaderColumn>
          <TableHeaderColumn width="150" dataField="created" dataFormat={dateFormatter}>Дата регистрации</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

AdministratePositionsTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};