import React from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';

export const UploadDropzone = (field) => {
  const attachments = field.input.value;
  
  return (
    <div className="dropzone-field">
      <Dropzone
        name={field.name}
        className="dropzone"
        onDrop={(filesToUpload) => field.input.onChange(filesToUpload)}
      >
        <span>Для добавления:<br />Перетащите файл или кликните мышью</span>
      </Dropzone>
      {field.meta.touched && field.meta.error && <span className="error">{field.meta.error}</span>}
      {attachments && Array.isArray(attachments) && <label>Выбранные:</label>}
      <ul>
        {attachments && attachments.map(x => <li key={x.name}>{x.name} - {Math.floor(x.size / 1024)} Кб</li>)}
      </ul>
    </div>
  );
};

UploadDropzone.propTypes = {
  actions: PropTypes.shape({
    actionAttachmentsUpload: PropTypes.func.isRequired
  }).isRequired
};