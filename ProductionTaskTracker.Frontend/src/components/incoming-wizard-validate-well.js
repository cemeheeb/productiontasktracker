import moment from 'moment';
import { PROPERTY_LIST_INCOMING } from '../constants/configuration';

const validateMoment = (values, field) => {
  const value = values.get(field);
  const valueMoment = moment(value, 'DD.MM.YYYY HH:mm');

  return values.has(field) && value !== null && valueMoment !== '01.01.0001 00:00' && valueMoment.isValid();
};

export const validate = values => {
  const errors = {};

  const requiredText = 'Поле обязательно для заполнения';

  if (!values.has('ticket_type') || values.get('ticket_type') == null) {
    errors.ticket_type = requiredText;
  } else {
    const ticketType = values.get('ticket_type');
    const propertyValues = PROPERTY_LIST_INCOMING.propertyValues[ticketType];

    Object.keys(PROPERTY_LIST_INCOMING.propertyValues[ticketType]).forEach(x => {
      if (['note'].indexOf(x) >= 0) {
        return;
      }

      if (!values.has(x) || values.get(x) === null || values.get(x) === "") {
        errors[x] = requiredText;
      }
    });

    // Так же необходимо учесть switch-case параметры
    if (typeof propertyValues.switch !== 'undefined' && propertyValues.switch !== null) {
      Object.keys(propertyValues.switch).forEach((x) => {
        const switchCaseProperties = propertyValues.switch[x][values.get(x)];
        if (typeof switchCaseProperties !== 'undefined') {
          Object.keys(switchCaseProperties).forEach(property => {
            if (!values.has(property) || values.get(property) == null) {
              errors[property] = requiredText;
            }
          });
        }
      });
    }
  }

  if (!validateMoment(values, 'probe_time')) {
    errors.probe_time = requiredText;
  } else if (moment(values.get('probe_time'), 'DD.MM.YYYY HH:mm') <= moment().endOf('month')) {
    errors.probe_time = "Значение должно быть больше текущего месяца";
  }

  if (!validateMoment(values, 'time_completion')) {
    errors.time_completion = requiredText;
  }

  if (!validateMoment(values, 'treatment_datebegin')) {
    errors.treatment_datebegin = requiredText;
  }

  if (!validateMoment(values, 'treatment_dateend')) {
    errors.treatment_dateend = requiredText;
  }

  if (Object.keys(errors).length > 0) {
    console.error({ errors });
  }

  return errors;
};