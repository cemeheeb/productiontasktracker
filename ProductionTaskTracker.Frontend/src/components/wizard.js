import { PropTypes } from 'prop-types';
import React, { Component } from 'react';

export class Wizard extends Component {
  constructor(props) {
    super(props);

    this.onPrevious = this.onPrevious.bind(this);
    this.onNext = this.onNext.bind(this);
  }
  
  onPrevious() {
    const history = this.props.history.slice();
    history.pop();
    this.props.actions.actionWizardSetHistory(history);
  }

  onNext(index) {
    const history = this.props.history.slice();
    history.push(index);
    this.props.actions.actionWizardSetHistory(history);
  }

  render() {
    const { steps, history, component } = this.props;

    let step, 
        stepLast,
        scanline = steps;

    history.forEach(x => {
      if (typeof scanline !== 'undefined' && scanline !== null) {
        stepLast = step;
        step = scanline[x];
        scanline = step.next;
      }
    });

    if (typeof scanline === 'undefined' || scanline === null) {
      scanline = [];
    }

    const Component = component;
    const next = scanline.map(x => {
      return {
        handler: (index) => x.handler(index).then(() => this.onNext(index), () => { } ),
        text: x.text
      };
    });

    return <Component isLocked={this.props.isLocked} previous={stepLast ? { handler: this.onPrevious, text: stepLast.text } : null} next={next} children={step.component} />;
  }
}

Wizard.propTypes = {
  isLocked: PropTypes.bool.isRequired,
  steps: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  history: PropTypes.array,
  component: PropTypes.func.isRequired
};