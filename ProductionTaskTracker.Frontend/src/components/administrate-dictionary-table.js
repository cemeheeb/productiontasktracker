import PropTypes from 'prop-types';
import React from 'react';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export class AdministrateDictionaryTable extends React.Component {
  render() {
    const { handlers: { onSelect, onPageChange, onSizePerPageList }, data, index, size, sizeTotal, selectedRow } = this.props;

    return (
      <div>
        <BootstrapTable hover condensed remote pagination
          selectRow={{
            mode: 'radio',
            clickToSelect: true,
            className: 'table-row-selected',
            hideSelectColumn: true,
            onSelect,
            selected: [selectedRow ? selectedRow.id : null]
          }}
          data={data}
          fetchInfo={{ dataTotalSize: sizeTotal }}
          trClassName={this.trClassFormat}
          options={{ 
            page: index,
            sizePerPage: size,
            onPageChange,
            onSizePerPageList,
            expandRowBgColor: 'rgb(242, 255, 163)',
            onRowDoubleClick: this.onEdit,
            noDataText: 'Нет данных'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
        >
          <TableHeaderColumn
            width="100"
            dataField="id"
            dataAlign="right"
            isKey={true}
          >Код</TableHeaderColumn>

          <TableHeaderColumn
            dataField="name"
          >Название</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

AdministrateDictionaryTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    onRefreshData: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired,
  selectedRow: PropTypes.object
};
