import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';

import { PageHeader } from './page-header';

export class WellPageHeader extends React.Component {
  constructor(props) {
    super(props);

    this.onAddClick = this.onAddClick.bind(this);
    this.onRefreshDataClick = this.onRefreshDataClick.bind(this);
  }
  
  componentDidMount() {
    this.onRefreshDataClick();
  }

  onAddClick() {
    const { handlers: { onAdd }, enabled } = this.props;
    if (enabled) {
      onAdd();
    }
  }

  onRefreshDataClick() {
    const { handlers: { onRefreshData }, enabled } = this.props;
    if (enabled) {
      onRefreshData();
    }
  }

  render() {
    const { session, enabled } = this.props;

    const addGranted = session.positionRolename.substr(-4) === '_cio';

    return (
      <PageHeader title="Скважины">
        <form role="form" className="page-filter-form form-inline">
          {
            addGranted &&
            <div className="form-group">
              <div className="input-group">
                <button type="button" title="Добавить" className="btn btn-primary" onClick={this.onAddClick}><i className="fa fa-plus" /></button>
              </div>
            </div>
          }  
          <div className="form-group">
            <div className="input-group">
              <button type="button" title="Обновить данные" className={classnames("btn", "btn-error", {"btn-disabled" : !enabled})} onClick={this.onRefreshDataClick}><i className="fa fa-refresh" /></button>
            </div>
          </div>
        </form>
      </PageHeader>
    );
  }
}

WellPageHeader.propTypes = {
    handlers: PropTypes.shape({ 
      onAdd: PropTypes.func.isRequired,
      onRefreshData: PropTypes.func.isRequired
    }).isRequired,
    session: PropTypes.object.isRequired,
    enabled: PropTypes.bool.isRequired
};
