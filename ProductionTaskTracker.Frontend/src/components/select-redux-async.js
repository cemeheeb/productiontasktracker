import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select/lib/Async';
import classnames from 'classnames';

export class SelectReduxAsync extends React.Component {
  constructor(props) {
    super(props);

    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onInputChange = this.onInputChange.bind(this);

    this.state = {
      options: []
    };
  }

  componentDidMount() {
    this.props.handlers.initialize().then((x) => {
      this.setState({ options: [x.payload] });
    });
  }

  onBlur() {
    const { input: { onBlur, value } } = this.props;
    return onBlur(value);
  }

  onChange(value) {
    const { input: { onChange }, handlers } = this.props;
    if (value) {
      handlers && handlers.onChange && handlers.onChange(value.id);
      return onChange(value.id);
    }
  }

  onInputChange(search) {
    if (!search) {
      return;
    }

    this.props.handlers.search(search)
      .then(
        state => {
          this.setState(state);
        },
        () => this.setState({ options: [] })
      );
  }

  render() {
    const { className, labelKey, valueKey, input: { value, onFocus }, meta: { touched, error }, tabIndex } = this.props;

    return (
      <div className="select-container">
        <Select
          value={value}
          options={this.state.options}
          onChange={this.onChange}
          onInputChange={this.onInputChange}
          onBlur={this.onBlur}
          onFocus={onFocus}
          cache={false}
          tabIndex={tabIndex}
          labelKey={labelKey}
          valueKey={valueKey}
          className={classnames(className, touched && error ? 'error' : null)}
        />
      </div>
    );
  }
}

SelectReduxAsync.propTypes = {
  className: PropTypes.string,
  handlers: PropTypes.shape({
    initialize: PropTypes.func,
    search: PropTypes.func
  }).isRequired,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.any,
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string
  }).isRequired,
  isMulti: PropTypes.bool,
  isSearchable: PropTypes.bool,
  placeholder: PropTypes.string,
  matchPos: PropTypes.string,
  tabIndex: PropTypes.string
};
