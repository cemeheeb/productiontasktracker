import PropTypes from 'prop-types';
import React from 'react';


export class InformationProfile extends React.PureComponent {
  render() {
    return (
      <div/>
    );
  }
}

InformationProfile.propTypes = {
  membershipFullname: PropTypes.string.isRequired,
  positionRolename: PropTypes.string.isRequired,
  departmentIDs: PropTypes.array.isRequired
};
