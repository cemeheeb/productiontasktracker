import React from 'react';
import PropTypes from 'prop-types';

export class Tabs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: this.props.selected
    };

    this.onTabClick = this.onTabClick.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props !== nextProps || this.state !== nextState;
  }

  onTabClick(event, index) {
    event.preventDefault();
    this.setState({
      selected: index
    });
  }

  onTabClickHandler() {
    return this.onTabClick();
  }
  

  renderTitles() {
    return React.Children.map(this.props.children, (child, index) => {
      const activeClass = (this.state.selected === index ? 'active' : '');
      return (
        <li className={activeClass} key={child.props.label}>
          <a href="#"
            onClick={(event) => this.onTabClick(event, index)}>
            {child.props.label}
          </a>
        </li>
      );
    });
  }

  renderContent() {
    return (
      <div className="tab-content">
        {this.props.children[this.state.selected]}
      </div>
    );
  }

  render() {
      return (
      <div>
        <ul className="nav nav-tabs">{this.renderTitles()}</ul>
        {this.renderContent()}
      </div>
    );
  }
}

Tabs.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.node
  ]).isRequired,
  selected: PropTypes.number.isRequired
};

Tabs.defaultProps = {
  selected: 0
};