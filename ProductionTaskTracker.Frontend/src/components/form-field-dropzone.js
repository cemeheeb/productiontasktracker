import React from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';

export class FormFieldDropzone extends React.Component {
  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(attachments) {
    const { input: { onBlur, onChange }, actions: { actionAttachmentsUpload }, tag } = this.props;
    onBlur(attachments);
    onChange(attachments);
    actionAttachmentsUpload(attachments, tag);
  }

  render() {
    const { input: { value } } = this.props;
    return (
      <div className="dropzone-field">
        <Dropzone className="dropzone" onDrop={this.onDrop} >
          <span>Для добавления:<br />Перетащите файл или кликните мышью</span>
        </Dropzone>
        {value && <label>Выбранные:</label> }
        <ul>
          {value && value.map(x => <li key={x.name}>{x.name} - {Math.floor(x.size / 1024)} Кб</li>)}
        </ul>
      </div>
    );
  }
}

FormFieldDropzone.propTypes = {
  input: PropTypes.shape({
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.string
    ])
  }).isRequired,
  actions: PropTypes.shape({
    actionAttachmentsUpload: PropTypes.func.isRequired
  }).isRequired,
  tag: PropTypes.object
};