import PropTypes from 'prop-types';
import React from 'react';

import { AdministrateDepartmentsTree } from './administrate-departments-tree';
import { AdministrateDepartmentsForm } from './administrate-departments-form';
import { AdministratePositionsForm } from './administrate-positions-form';
import { AdministratePositionsAssignedForm } from './administrate-positions-assigned-form';

export class AdministrateDepartmentsPage extends React.Component {
  constructor(props) {
    super(props);

    this.onRefreshData = this.onRefreshData.bind(this);

    this.onSelect = this.onSelect.bind(this);
    this.onDepartmentAdd = this.onDepartmentAdd.bind(this);
    this.onDepartmentAddSave = this.onDepartmentAddSave.bind(this);
    this.onPositionAdd = this.onPositionAdd.bind(this);
    this.onPositionAddSave = this.onPositionAddSave.bind(this);
    this.onAddCancel = this.onAddCancel.bind(this);
    this.onDepartmentEdit = this.onDepartmentEdit.bind(this);
    this.onDepartmentEditSave = this.onDepartmentEditSave.bind(this);
    this.onPositionEdit = this.onPositionEdit.bind(this);
    this.onPositionEditSave = this.onPositionEditSave.bind(this);
    this.onEditCancel = this.onEditCancel.bind(this);
    this.onMove = this.onMove.bind(this);
    this.onRemove = this.onRemove.bind(this);
    this.onMembershipAssign = this.onMembershipAssign.bind(this);
    this.onMembershipRemove = this.onMembershipRemove.bind(this);
    
    this.state = {
      data: [],
      workPeriodData: [],
      index: 1,
      size: 30,
      sizeTotal: 0,
      isDepartmentNew: false,
      isPositionNew: false,
      isBusy: false
    };
  }

  componentDidMount() {
    this.onRefreshData();
  }

  onRefreshData() {
    this.setState({ isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onDepartmentAdd() {
    this.setState({ selectedNode: null, isDepartmentNew: true, isPositionNew: false }, () =>
      this.props.actions.actionInitializeForm('administrate-department', {
        name: '',
        display_name: ''
      })
    );
  }

  onDepartmentAddSave() {
    this.props.actions.actionDepartmentAdd(this.props.session.administrateDepartmentInputData).then(() => this.onRefreshData());
  }

  onPositionAdd() {
    this.setState({ selectedNode: null, isDepartmentNew: false, isPositionNew: true }, () =>
      this.props.actions.actionInitializeForm('administrate-department', {
        name: '',
        display_name: ''
      })
    );
  }

  onPositionAddSave() {
    this.props.actions.actionPositionAdd(this.props.session.administratePositionInputData).then(() => this.onRefreshData());
  }

  onAddCancel() {
    console.error('onAddCancel');
  }

  onSelect(nodeIDs) {
    console.error('onSelect');
    if (nodeIDs.length > 0) {
      const node = this.state.data.filter(x => x.id == [nodeIDs[0]])[0];
      this.setState({ selectedNode: node, isDepartmentNew: false, isPositionNew: false }, node.id === null || node.id[0] === 'd' ? this.onDepartmentEdit : this.onPositionEdit);
    } else {
      this.setState({ selectedNode: null, isDepartmentNew: false, isPositionNew: false });
    }
  }

  onDepartmentEdit() {
    this.props.actions.actionInitializeForm('administrate-department', this.state.selectedNode);
  }

  onDepartmentEditSave() {
    console.error('onDepartmentEditSave', this.props.session.administrateDepartmentInputData);
    this.props.actions.actionDepartmentUpdate(this.props.session.administrateDepartmentInputData).then(() => this.onRefreshData());
  }

  onPositionEdit() {
    const { actions: { actionInitializeForm } } = this.props;
    actionInitializeForm('administrate-position', this.state.selectedNode);

    const selectedNodeMembership = this.state.workPeriodData.filter(x => x.position_id === this.state.selectedNode.id);
    if (selectedNodeMembership.length > 0) {
      console.error('administrate-position-assigned', selectedNodeMembership[0]);
      actionInitializeForm('administrate-position-assigned', selectedNodeMembership[0]);
    } else {
      actionInitializeForm('administrate-position-assigned', {});
    }
  }

  onPositionEditSave() {
    console.error({ inputData: this.props.session.administratePositionInputData});
    const data = { ...this.props.session.administratePositionInputData, id: this.props.session.administratePositionInputData.id.substring(1) };
    this.props.actions.actionPositionUpdate(data).then(() => this.onRefreshData());
  }

  onEditCancel() {
    console.error('onEditCancel');
  }

  onMove(nodeIDFrom, nodeIDTo) {
    const { actionDepartmentMove } = this.props.actions;
    actionDepartmentMove(nodeIDFrom, nodeIDTo).then(() => {
      this.onRefreshData();
    });
  }

  onRemove(nodeID) {
    const { actionDepartmentRemove } = this.props.actions;
    actionDepartmentRemove(nodeID).then(() => {
      this.onRefreshData();
    });
  }

  onMembershipAssign() {
    const { actionWorkPeriodCreate } = this.props.actions;
    const { selectedNode } = this.state;
    actionWorkPeriodCreate({ position_id: selectedNode.id.substring(1), membership_id: this.props.session.administratePositionAssignedInputData.membership_id }).then(() => {
      this.onRefreshData();
    });
  }

  onMembershipRemove() {
    const { actionInitializeForm, actionWorkPeriodRemove } = this.props.actions;
    const { workPeriodData, selectedNode } = this.state;
    const workPeriodAssigned = workPeriodData.filter(x => x.position_id == selectedNode.id);
    if (workPeriodAssigned.length > 0) {
      actionWorkPeriodRemove(workPeriodAssigned[0].id).then(() => {
        actionInitializeForm('administrate-position-assigned', {});
        this.onRefreshData();
      });
    }
  }

  invalidateData() {
    this.setState({ isBusy: true }, () => this.props.actions.actionDepartmentFilter()
      .then(responseDepartment => {
        if (responseDepartment.error) {
          return;
        }

        const dataDepartments = responseDepartment.payload.map(x => ({ ...x, id: x.id !== null ? 'd' + x.id : null, parent_id: x.parent_id !== null ? 'd' + x.parent_id : null }));

        this.props.actions.actionPositionFilter().then((responsePosition) => {
          const dataPositions = responsePosition.payload.map(x => ({ ...x, id: 'p' + x.id, parent_id: 'd' + x.department_id }));
          this.setState({ data: [...dataDepartments, ...dataPositions], isBusy: false }, () => {
            this.props.actions.actionWorkPeriodFilter().then((responseWorkPeriod) => {
              return this.setState({ workPeriodData: [...responseWorkPeriod.payload.map(x => ({ ...x, position_id: 'p' + x.position_id }))] });
            });
          });
        });
      })
    );
  }

  render() {
    const { data, isDepartmentNew, isPositionNew, selectedNode } = this.state;
    const selectedNodeMembership = typeof this.state.selectedNode !== 'undefined' && this.state.selectedNode !== null ? this.state.workPeriodData.filter(x => x.position_id === this.state.selectedNode.id) : [];
    const isAssigned = selectedNodeMembership.length > 0;

    return (
      <div className="col-md-12">
        <div className="col-md-6">
          <AdministrateDepartmentsTree data={data} handlers={{ onSelect: this.onSelect }} selectedNode={selectedNode} />
        </div>
        <div className="col-md-6">
          <div className="administrative-toolbar">
            <button type="button" title="Обновить" className="btn btn-error" onClick={this.onRefreshData}><i className="fa fa-refresh" /></button>
            {!isDepartmentNew && (!selectedNode || selectedNode.id[0] === 'd') && <button type="button" className="btn btn-primary" onClick={this.onDepartmentAdd}><i className="fa fa-plus" /><span className="text">Подразделение</span></button>}
            {!isPositionNew && (!selectedNode || selectedNode.id[0] === 'd') && <button type="button" className="btn btn-primary" onClick={this.onPositionAdd}><i className="fa fa-plus" /><span className="text">Должность</span></button>}
          </div>
          {isDepartmentNew && <AdministrateDepartmentsForm title="Добавить подразделение" handlers={{ onSave: this.onDepartmentAddSave, onCancel: this.onAddCancel }} />}
          {selectedNode && (!selectedNode.id || selectedNode.id[0] === 'd') && <AdministrateDepartmentsForm title="Изменить" handlers={{ onSave: this.onDepartmentEditSave, onCancel: this.onEditCancel }} />}
          {selectedNode && (selectedNode.id && selectedNode.id[0] !== 'd') &&
            <React.Fragment>
              <AdministratePositionsForm title="Изменить" handlers={{ onSave: this.onPositionEditSave, onCancel: this.onEditCancel }} />
              <AdministratePositionsAssignedForm title="Действующий работник" memberships={this.props.session.memberships} handlers={!isAssigned ? { onAssign: this.onMembershipAssign } : { onRemove: this.onMembershipRemove }} />
            </React.Fragment>
          }
        </div>
      </div>
    );
  }
}

AdministrateDepartmentsPage.propTypes = {
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

