import PropTypes from 'prop-types';
import React from 'react';

export class WizardStepForm extends React.Component {
  handleSubmit() {
  }
  
  render() {
    const { previous, next, handleSubmit, children } = this.props;

    return (
      <form onSubmit={handleSubmit(this.handleSubmit)} className="ticket-form form-horizontal">
        {children}
        <div className="form-group">
          <div className="col-md-12">
            <hr />
            <div className="input-group">
              {next.map((x, i) => <button tabIndex="1000" key={`next_${i}`} type="submit" onClick={handleSubmit(() => x.handler(i))} className="btn btn-primary pull-right">{x.text}</button>) }
              {previous ? <button tabIndex="0" onClick={previous.handler} className="btn btn-primary pull-left">{previous.text}</button> : null }
            </div>
          </div>
        </div>
      </form>
    );
  }
}

WizardStepForm.propTypes = {
  previous: PropTypes.shape({
    text: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired
  }),
  next: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      handler: PropTypes.func.isRequired
    })
  ).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
};