import PropTypes from 'prop-types';
import React from 'react';
import Modal from 'react-overlays/lib/Modal';

import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { WellPageHeader } from './well-page-header';
import { WellTable } from './well-table';

import { WizardWell } from './wizard-well';
import { ModalHeader } from './modal-header';

export class WellPage extends React.Component {
  constructor(props) {
    super(props);

    this.onRefreshData = this.onRefreshData.bind(this);

    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);

    this.onAdd = this.onAdd.bind(this);
    this.onWizardCancel = this.onWizardCancel.bind(this);
    this.onWizardSubmit = this.onWizardSubmit.bind(this);
    this.invalidateData = this.invalidateData.bind(this);

    this.state = {
      data: [],
      index: 1,
      size: 30,
      sizeTotal: 0,
      isBusy: false
    };
  }

  onRefreshData() {
    this.setState({index: 1, isBusy: true}, () => {
      this.invalidateData();
    });
  }

  onPageChange(index, size) {
    this.setState({ index: index, size, isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onSizePerPageList(size) {
    this.setState({ size, isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onAdd() {
    this.props.actions.actionWizardWell();
  }

  onWizardCancel() {
    const { actions: { actionWizardReset, actionResetForm } } = this.props;
 
    actionWizardReset();
    actionResetForm("wizard-well");
  }

  onWizardSubmit() {
    this.onRefreshData();
    this.onWizardCancel();
  }

  invalidateData() {
    this.props.actions.actionDictionaryWellFilter('', this.state.index, this.state.size)
    .then(response => {
      this.setState({ isBusy: false });

      if (response.error) {
        return;
      }

      const { data, size_total } = response.payload;
      this.setState({ data, sizeTotal: size_total });
    });
  }

  render() {
    const { data, index, size, sizeTotal, isBusy } = this.state;
    const { actions, session } = this.props;

    return (
      <div>
        <WellPageHeader
          handlers={{ 
            onRefreshData: this.onRefreshData,
            onAdd: this.onAdd
          }}
          session={session}
          enabled={!isBusy}
        />
        <div className="loading-bar-wrapper"><LoadingBar className="loading-bar"/></div>
        <div className="container-fluid">
          <div className="wrapper wrapper-content">
            <WellTable {...{
              data,
              handlers: {
                onPageChange: this.onPageChange,
                onSizePerPageList: this.onSizePerPageList,
              },
              index,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
              size,
              sizeTotal
            }}/>
          </div>
        </div>
        {
          session.wizardWell.isVisible &&
            (
              <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" backdrop="static" show={session.wizardWell.isVisible} onHide={this.onWizardCancel}>
                <div className="layout-modal-dialog" >
                  <ModalHeader title="Добавление скважины" handlers={{onClose: this.onWizardCancel}} />
                  <hr />
                  <WizardWell
                    session={session}
                    actions={actions}
                    handlers={{
                      onSubmit: this.onWizardSubmit
                    }}
                  />
                </div>
              </Modal>
            )
          }
      </div>
    );
  }
}

WellPage.propTypes = {
  actions: PropTypes.object.isRequired,
  session: PropTypes.shape({
    departmentIDs: PropTypes.array.isRequired,
    positionRolename: PropTypes.string.isRequired,
    wizardControlled: PropTypes.shape({
      isVisible:PropTypes.bool.isRequired
    }).isRequired,
    routes: PropTypes.array.isRequired
  }).isRequired
};