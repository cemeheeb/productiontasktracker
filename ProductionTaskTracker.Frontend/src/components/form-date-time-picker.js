import PropTypes from 'prop-types';
import React from 'react';

import moment from 'moment';

import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

import classnames from 'classnames';

export class FormDateTimePicker extends React.Component {
  constructor() {
    super();

    this.state = {
      focused: false
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    const { input: { onChange }, handlers } = this.props;

    handlers && handlers.onChange && handlers.onChange(value);
    return onChange(moment.isMoment(value) ? value.format("DD.MM.YYYY HH:mm") : value);
  }

  render() {
    const { input, meta: { error } } = this.props;
    return (
      <Datetime
        name={input.name}
        value={input.value}
        className={classnames(error ? 'error' : null)}
        locale="ru"
        dateFormat="DD.MM.YYYY"
        timeFormat="HH:mm"
        onChange={this.onChange} />
    );
  }
}

FormDateTimePicker.propTypes = {
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object 
    ]),
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func
  }),
  placeholder: PropTypes.string,
  meta: PropTypes.object
};
