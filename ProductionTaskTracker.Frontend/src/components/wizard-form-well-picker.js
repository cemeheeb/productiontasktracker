import PropTypes from 'prop-types';
import React from 'react';
import { Field } from 'redux-form/immutable';

import { SelectWellMulti } from './select-well-multi';

export class WizardFormWellPicker extends React.PureComponent {
  render() {
    const { actions, departmentIDs } = this.props;
    
    return (
      <div className="col-md-12">
        <div className="form-group">
          <label>Скважина</label>
          <div className="input-group">
            <Field name="wells"
              component={SelectWellMulti}
              departmentIDs={departmentIDs}
              actions={actions}
              tabIndex="1"
            />
          </div>
        </div>
      </div>
    );
  }
}

WizardFormWellPicker.propTypes = {
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  departmentIDs: PropTypes.array.isRequired
};
