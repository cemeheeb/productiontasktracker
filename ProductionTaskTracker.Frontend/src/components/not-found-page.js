import React from 'react';
import { Link } from 'react-router-dom';

export class NotFoundPage extends React.Component {
  render() {
    return (
      <div className="not-found-page middle-box text-center">
        <h1>404</h1>
        <h3 className="font-bold">Страница не найдена</h3>
        <Link to="/"><i className="fa fa-arrow-left"/> Назад</Link>
      </div>
    );
  }
}