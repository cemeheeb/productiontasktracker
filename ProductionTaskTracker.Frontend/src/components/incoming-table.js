import PropTypes from 'prop-types';
import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import classnames from 'classnames';

import slConverter from '../utilities/sl-converter';
import ticketStatusToClassConverter from '../utilities/ticket-status-to-class-converter';
import dateFormatter from '../utilities/date-formatter';

export class IncomingTable extends React.Component {
  constructor(props) {
    super(props);

    this.onEdit = this.onEdit.bind(this);
    this.onRemove = this.onRemove.bind(this);

    this.createCustomFilter = this.createCustomFilter.bind(this);
    this.buttonFormatter = this.buttonFormatter.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    const { index, size, sizeTotal, data } = this.props;
    return (nextProps.index !== index || nextProps.size !== size || nextProps.sizeTotal !== sizeTotal || nextProps.data !== data);
  }
  
  onEdit(row) {
    this.props.handlers.onEdit(row);
  }

  onRemove(row) {
    this.props.handlers.onRemove(row);
  }




  createCustomFilter(filterHandler, parameters) {
    return (
      <Select
        cache={false}
        placeholder="Не задано"
        noResultsText="Пусто"
        onChange = {(option) => {
          filterHandler({ filterName: parameters.filterKey, value: typeof option !== 'undefined' && option != null ? option.value : null });
        }}
        value = {
          (this.props.filterOptions[parameters.filterName] || []).map(x => ({ label: x.value, value: x.code }))
            .filter(x => x.value === this.props.filter[parameters.filterKey])[0]
        }
        options = {(this.props.filterOptions[parameters.filterName] || []).map(x => ({ label: x.value, value: x.code }))}
        isLoading={this.props.isLoading}
        isSearchable
        isClearable
      />
    );
  }

  slFormatter(cell, row) {
    return <div>{slConverter(cell)} {row.is_night && "[Ночная]"}</div>;
  }

  buttonFormatter(cell, row) {
    return (
      <div>
        <button type="button" className="btn btn-warning" title="Просмотреть" onClick={() => this.onEdit(row)} ><i className="fa fa-bars" /></button>
        { row.allow_unregister && <button type="button" className="btn btn-danger" title="Снять" onClick={() => this.onRemove(row)} ><i className="fa fa-times" /></button> }
      </div>
    );
  }

  expiredFormatter(cell, row) {
    return (
      <div>
        {row.is_expired ? <i className="fa fa-hourglass-end red" /> : null}
        {dateFormatter(cell)}
      </div>
    );
  }

  trClassFormat(rowData) {
    const output = {};
    output[ticketStatusToClassConverter(rowData.status)] = true;
    output["ticket-type-repeated"] = rowData.is_repeated;
    return classnames(output);
  }
  
  render() {
    const {
      index,
      size,
      sizeTotal,
      handlers: {
        onFilterChange,
        onPageChange,
        onSizePerPageList
      },
      data
    } = this.props;
    
    return (
      <BootstrapTable hover condensed remote pagination
        data={data}
        fetchInfo={{ dataTotalSize: sizeTotal }}
        trClassName={this.trClassFormat}
        options={{
          page: index,
          sizePerPage: size,
          onFilterChange,
          onPageChange,
          onSizePerPageList,
          expandRowBgColor: 'rgb(242, 255, 163)',
          onRowDoubleClick: this.onEdit,
          noDataText: 'Нет данных',
          paginationPosition: 'top'
        }}
        tableHeaderClass="table-header"
        tableContainerClass="table-container"
      >
        <TableHeaderColumn
          width="82"
          dataFormat={this.buttonFormatter}
        />

        <TableHeaderColumn isKey
          width="100"
          dataField="id"
          dataAlign="right"
        >Код заявки</TableHeaderColumn>

        <TableHeaderColumn
          width="150"
          dataField="department_from"
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "department_from",
              filterName: "department_name_from",
            }
          }}
        >Заказчик</TableHeaderColumn>

        <TableHeaderColumn
          width="150"
          dataField="department_to"
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "department_to",
              filterName: "department_name_to",
            }
          }}
        >Исполнитель</TableHeaderColumn>

        <TableHeaderColumn
          dataField="ticket_type_description"
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "ticket_type",
              filterName: "ticket_type_name",
            }
          }}
        >Вид работ</TableHeaderColumn>

        <TableHeaderColumn
          width="180"
          dataField="field"
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "field",
              filterName: "field_name",
            }
          }}
        >Месторождение</TableHeaderColumn>

        <TableHeaderColumn
          width="100"
          dataField="cluster"
        >Куст</TableHeaderColumn>

        <TableHeaderColumn
          width="150"
          dataField="well"
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "well",
              filterName: "well_name"
            }
          }}
        >Скважина</TableHeaderColumn>

        <TableHeaderColumn
          width="150"
          dataField="status_description"
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "status",
              filterName: "status_name"
            }
          }}
        >Cтатус</TableHeaderColumn>

        <TableHeaderColumn
          width="150"
          dataField="sl"
          dataFormat={this.slFormatter}
          filter={{
            type: 'CustomFilter',
            getElement: this.createCustomFilter,
            customFilterParameters: {
              filterKey: "sl",
              filterName: "sl",
            }
          }}
        >SL</TableHeaderColumn>

        <TableHeaderColumn 
          width="150"
          dataField="created"
          dataFormat={dateFormatter} 
        >Зарегистрирована</TableHeaderColumn>

        <TableHeaderColumn
          width="150"
          dataField="expired"
          dataFormat={this.expiredFormatter} 
        >Срок выполнения</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}

IncomingTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onFilterChange: PropTypes.func.isRequired,
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired,
  filterOptions: PropTypes.object.isRequired,
  dateBegin: PropTypes.object.isRequired,
  dateEnd: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};
