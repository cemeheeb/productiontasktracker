import moment from 'moment';
import { PROPERTY_LIST_OUTCOMING } from '../constants/configuration';

const validateMoment = (values, field) => {
  const value = values.get(field);
  const valueMoment = moment(value, 'DD.MM.YYYY HH:mm');

  return values.has(field) && value !== null && valueMoment !== '01.01.0001 00:00' && valueMoment.isValid();
};

const initializeParameters = (count) => {
  const parameters = [];
  while (count--) {
    parameters.push({});
  }
  
  return parameters;
};

export const validate = values => {
  const errors = {};
  const requiredText = 'Поле обязательно для заполнения';

  if (!values.has('ticket_type') || values.get('ticket_type') == null) {
    errors.ticket_type = requiredText;
  } else {
    if (!values.has('department_id') || values.get('department_id') === null) {
      errors.department_id = requiredText;
    }

    if (!values.has('wells') || values.get('wells').size == 0) {
      errors.wells = requiredText;
    }

    const ticketType = values.get('ticket_type');
    if (ticketType && values.has('parameters') && values.get('parameters').size > 0) {
      values.get('parameters').forEach((parameter, index) => {
        if (typeof parameter !== 'undefined') {
          Object.keys(PROPERTY_LIST_OUTCOMING.propertyValues[ticketType]).forEach(x => {
            if (ticketType === 'A2' && x !== 'well_period_mode') {
              return;
            }

            if (['switch', 'power_off', 'note', 'probe_volume'].indexOf(x) >= 0) {
              return;
            }
            
            if (parameter.get('well_purpose') !== 'Production' && x === 'pressure_ecn_minimal') {
              return;
            }

            if (parameter.get('research') !== 'TKRS' && ['pump_depth', 'damping_volume', 'damping_weight'].indexOf(x) > -1) {
              return;
            }

            if (!parameter.has(x) || parameter.get(x) == null || parameter.get(x) === "") {
              errors.parameters = (errors.parameters && !errors.parameters.hasOwnProperty('_error')) ? errors.parameters : initializeParameters(values.get('parameters').size);
              errors.parameters[index][x] = `Поле ${x} обязательно для заполнения`;
            } else {
              if (['start_time', 'start_time_plan', 'time_completion'].indexOf(x) >= 0 && !validateMoment(parameter, x)) {
                errors.parameters = (errors.parameters && !errors.parameters.hasOwnProperty('_error')) ? errors.parameters : initializeParameters(values.get('parameters').size);
                errors.parameters[index][x] = `Поле ${x} за границами допустимого диапазона`;
              }
            }
          });

          if (typeof PROPERTY_LIST_OUTCOMING.propertyValues[ticketType].switch !== 'undefined') {
            Object.keys(PROPERTY_LIST_OUTCOMING.propertyValues[ticketType].switch).forEach(x => {
              if (parameter.has(x) && parameter.get(x) != null) {
                  Object.keys(PROPERTY_LIST_OUTCOMING.propertyValues[ticketType].switch[x][parameter.get(x)]).forEach(property => {
                    if (!parameter.has(property) || parameter.get(property) == null) {
                      errors.parameters = (errors.parameters && !errors.parameters.hasOwnProperty('_error')) ? errors.parameters : initializeParameters(values.get('parameters').size);
                      errors.parameters[index][property] = requiredText;
                    }
                  });
                }
            });
          }
        }
      });

      if (!errors.hasOwnProperty('parameters') &&  !values.get('parameters').toJS().every(x => typeof x !== 'undefined')) {
        errors.parameters = { _error: 'Параметры заявок заполнены не по всем скважинам' };
      }
    } else {
      errors.parameters = { _error: 'Параметры заявок заполнены не по всем скважинам' };
    }
  }

  return errors;
};