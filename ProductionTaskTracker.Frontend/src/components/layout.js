import PropTypes from 'prop-types';
import React from 'react';
import { GatewayProvider } from 'react-gateway';

import ReduxToastr from 'react-redux-toastr';
import 'react-redux-toastr/src/styles/index.scss';

import { UnauthorizedLayout } from './unauthorized-layout';
import { AuthorizedLayout } from './authorized-layout';

export class Layout extends React.Component {
  render() {
    // В зависимости от роли пользователя, рисуем соответствующий Layout
    return (
      <GatewayProvider>
        <div style={{height: '100%' }}>
          { this.props.session.token ? <AuthorizedLayout {...this.props} /> : <UnauthorizedLayout {...this.props} /> }
          <ReduxToastr
            timeOut={8000}
            newestOnTop={false}
            preventDuplicates
            transitionIn="fadeIn"
            transitionOut="fadeOut"
          />
        </div>
      </GatewayProvider>
    );
  }
}

Layout.propTypes = {
  session: PropTypes.shape({
    token: PropTypes.string
  }).isRequired
};