import PropTypes from 'prop-types';
import React from 'react';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export class WellTable extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { index, size, sizeTotal, data } = this.props;
    return (nextProps.index !== index || nextProps.size !== size || nextProps.sizeTotal !== sizeTotal || nextProps.data !== data);
  }

  render() {
    const { index, size, sizeTotal, handlers: { onPageChange, onSizePerPageList }, data } = this.props;

    return (
      <div>
        <BootstrapTable hover condensed remote pagination
          data={data}
          fetchInfo={{ dataTotalSize: sizeTotal }}
          trClassName={this.trClassFormat}
          options={{ 
            page: index,
            sizePerPage: size,
            onPageChange,
            onSizePerPageList,
            expandRowBgColor: 'rgb(242, 255, 163)',
            onRowDoubleClick: this.onEdit,
            noDataText: 'Нет данных',
            paginationPosition: 'top'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
        >
          <TableHeaderColumn width="100" dataField="id" dataAlign="right" isKey={true} >Код</TableHeaderColumn>
          <TableHeaderColumn dataField="shop" >Цех</TableHeaderColumn>
          <TableHeaderColumn dataField="field" >Месторождение</TableHeaderColumn>
          <TableHeaderColumn dataField="cluster" >Куст</TableHeaderColumn>
          <TableHeaderColumn dataField="code" >Скважина</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

WellTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};
