import React from 'react';
import PropTypes from 'prop-types';

export class NavigationMenuFakeLink extends React.PureComponent {
  render() {
    const { children } = this.props;

    return (
        <li className="nav navbar-nav">
            <a className="fake">{children}</a>
        </li>
    );
  }
}

NavigationMenuFakeLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node
};


