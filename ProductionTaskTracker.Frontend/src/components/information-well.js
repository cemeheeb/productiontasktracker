import PropTypes from 'prop-types';
import React from 'react';

import { DisplayTable } from './display-table';
import { DisplayTableText } from './display-table-text';

export class InformationWell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:null,
      shop:null,
      field:null,
      cluster:null,
      code:null,
      qj:null,
      qn:null,
      timestamp:null
    };
  }

  componentDidMount() {
    const { actions: { actionInformationWell }, wellID } = this.props;
    actionInformationWell(wellID).then(
      x => this.setState(x.payload)
    );
  }

  render() {
    const {
      shop,
      field,
      cluster,
      code,
      qj,
      qn
    } = this.state;

    return (
      <div>
        <div className="col-md-6">
          <DisplayTable>
            <DisplayTableText label="Цех" value={shop}/>
            <DisplayTableText label="Месторождение" value={field}/>
            <DisplayTableText label="Куст" value={cluster}/>
            <DisplayTableText label="Скважина" value={code}/>
          </DisplayTable>
        </div>
        <div className="col-md-6">
          <DisplayTable>
            <DisplayTableText label="Дебит жидкости" value={qj}/>
            <DisplayTableText label="Дебит нефти" value={qn}/>
          </DisplayTable>
        </div>
      </div>
    );
  }
}

InformationWell.propTypes = {
  wellID: PropTypes.number.isRequired,
  actions: PropTypes.shape({
    actionInformationWell: PropTypes.func.isRequired
  }).isRequired
};