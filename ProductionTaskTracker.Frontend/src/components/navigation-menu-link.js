import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';

export class NavigationMenuLink extends React.Component {
  render() {
    const { children, to } = this.props;

    return (
        <li className={classnames("nav", "navbar-nav", this.props.className)}>
            <NavLink exact to={to} activeClassName="selected">{children}</NavLink>
        </li>
    );
  }
}

NavigationMenuLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.node
};


