import React from 'react';

import { LoginForm } from '../containers/login-form';

export class LoginPage extends React.Component {
  render() {
    return (
      <div className="middle-box loginscreen">
        <LoginForm />
      </div>
    );
  }
}