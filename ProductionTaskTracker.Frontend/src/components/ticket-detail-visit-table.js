import PropTypes from 'prop-types';
import React from 'react';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';

export class TicketDetailVisitTable extends React.Component {
  dateFormatter(cell) {
    return moment(cell).format("DD.MM.YYYY HH:mm");
  }

  render() {
    const { data } = this.props;
    return (
      <BootstrapTable hover condensed
        data={data}
        options={{ 
          expandRowBgColor: 'rgb(242, 255, 163)',
          paginationPosition: 'top'
        }}
        className="table-visits"
        tableHeaderClass="table-header"
        tableContainerClass="table-container"
        selectRow={{
          mode: 'radio',
          clickToSelect: true,
          className: 'table-row-selected',
          hideSelectColumn: true
        }}
      >
        <TableHeaderColumn width="60" dataField="id" isKey={true} hidden={true}>Код</TableHeaderColumn>
        <TableHeaderColumn dataField="membership_display_name">Пользователь</TableHeaderColumn>
        <TableHeaderColumn width="150" dataField="created" dataFormat={this.dateFormatter}>Дата просмотра</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}

TicketDetailVisitTable.propTypes = {
  data: PropTypes.array.isRequired
};
