import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import Select from 'react-select';

export class SelectWellMulti extends React.Component {
  constructor(props) {
    super(props);
    
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onInputChange = this.onInputChange.bind(this);

    this.state = {
      options: []
    };
  }

  onBlur() {
    const { input: { onBlur, value } } = this.props;
    return onBlur([...value]);
  }

  onChange(value) {
    const { input: { onChange }, handlers } = this.props;
    handlers && handlers.onChange && handlers.onChange(value);
    return onChange(value);
  }

  onInputChange(search) {
    if (!search) {
      this.setState({ options: [ this.props.input.value ] });
      return;
    }

    this.props.actions.actionDictionaryDepartmentsWellFilter(search, this.props.departmentIDs, 0, 30)
      .then(
        response => this.setState({ options: this.prepareOptions(response.payload.data) }),
        () => this.setState({ options: [] })
      );
  }

  prepareOptions(options) {
    return options
      .map(x => x.shop).filter((value, index, self) => self.indexOf(value) === index) // По каждому цеху один раз
      .map(x => ({
        label: x, 
        options: options
          .filter((value) => value.shop === x)
          .map(x => ({
            ...x, 
            label: `№ ${x.code}, ${!x.cluster || (`куст ${x.cluster},`)} ${x.field}`,
            value: x.id
          }))
      }));
  }

  render() {
    const { input: { value, onFocus }, meta: { touched, error }, tabIndex } = this.props;
    const { options } = this.state;

    return (
      <div className="select-container">
        <Select
          name="select-well-import"
          value={value}
          options={options}
          onChange={this.onChange}
          onInputChange={this.onInputChange}
          onFocus={onFocus}
          isSearchable
          isMulti
          cache={false}
          tabIndex={tabIndex}
          className={classnames(touched && error ? 'error' : null)}
          placeholder="Введите номер скважины"
        />
      </div>
    );
  }
}

SelectWellMulti.propTypes = {
  actions: PropTypes.shape({
    actionDictionaryDepartmentsWellFilter: PropTypes.func.isRequired
  }),
  departmentIDs: PropTypes.array.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.object), PropTypes.string]),
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func
  }),
  tabIndex: PropTypes.string
};