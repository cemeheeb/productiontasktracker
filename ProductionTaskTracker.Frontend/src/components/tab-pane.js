import React from 'react';
import PropTypes from 'prop-types';

export class TabPane extends React.PureComponent {
  render() {
    return (
      <div>{this.props.children}</div>
    );
  }
}

TabPane.propTypes = {
  label: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};