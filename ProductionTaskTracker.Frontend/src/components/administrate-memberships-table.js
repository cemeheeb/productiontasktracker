import PropTypes from 'prop-types';
import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import boolFormatter from '../utilities/bool-formatter';
import dateFormatter from '../utilities/date-formatter';

export class AdministrateMembershipsTable extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { index, size, sizeTotal, data } = this.props;
    return (nextProps.index !== index || nextProps.size !== size || nextProps.sizeTotal !== sizeTotal || nextProps.data !== data);
  }

  render() {
    const { handlers: { onSelect, onPageChange, onSizePerPageList }, data, index, size, sizeTotal, selectedRow } = this.props;

    return (
      <BootstrapTable hover condensed remote pagination
        selectRow={{
          mode: 'radio',
          clickToSelect: true,
          className: 'table-row-selected',
          hideSelectColumn: true,
          onSelect,
          selected: [selectedRow ? selectedRow.id : null]
        }}
        data={data}
        fetchInfo={{ dataTotalSize: sizeTotal }}
        trClassName={this.trClassFormat}
        options={{
          page: index,
          sizePerPage: size,
          onPageChange,
          onSizePerPageList,
          expandRowBgColor: 'rgb(242, 255, 163)',
          onRowDoubleClick: this.onEdit,
          noDataText: 'Нет данных'
        }}
        tableHeaderClass="table-header"
        tableContainerClass="table-container"
      >
        <TableHeaderColumn width="100" dataField="id" isKey={true}>Код</TableHeaderColumn>
        <TableHeaderColumn width="150" dataField="fullname" >ФИО</TableHeaderColumn>
        <TableHeaderColumn width="150" dataField="username" >Логин</TableHeaderColumn>
        <TableHeaderColumn width="150" dataField="created" dataFormat={dateFormatter}>Дата регистрации</TableHeaderColumn>
        <TableHeaderColumn width="150" dataField="is_local" dataFormat={boolFormatter} >Локальный?</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
AdministrateMembershipsTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    onRefreshData: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired,
  selectedRow: PropTypes.object
};