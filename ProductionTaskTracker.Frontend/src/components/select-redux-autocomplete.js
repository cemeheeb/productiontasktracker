import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select-plus';
import classnames from 'classnames';

export class SelectRedux extends React.Component {
  constructor(props) {
    super(props);

    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onBlur() {
    const { input: { onBlur, value } } = this.props;
    return onBlur([...value]);
  }

  onChange(value) {
    const { input: { onChange }, handlers, valueKey, simpleValue } = this.props;
    
    if (value !== null) {
      if (simpleValue) {
        handlers && handlers.onChange && handlers.onChange(value);
        return onChange(value);
      }
      
      handlers && handlers.onChange && handlers.onChange(value[valueKey]);
      return onChange(value[valueKey]);
    } 

    return onChange(null);
  }

  render() {
    const { className, input: { value, onFocus }, meta: { error } } = this.props;

    return (
      <div className="select-container">
        <Select
          {...this.props}
          className={classnames(className, error ? 'error' : null)}
          onChange={this.onChange}
          onFocus={onFocus}
          value={value}
        />
      </div>
    );
  }
}

SelectRedux.propTypes = {
  handlers: PropTypes.shape({
    onChange: PropTypes.func
  }).isRequired,
  className: PropTypes.string,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string,
    warning: PropTypes.string
  }).isRequired,
  multi: PropTypes.bool,
  isLoading: PropTypes.bool,
  value: PropTypes.object,
  valueKey: PropTypes.string.isRequired,
  labelKey: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  simpleValue: PropTypes.bool,
  matchPos: PropTypes.string,
  options: PropTypes.array.isRequired
};

SelectRedux.defaultProps = {
  handlers: {},
  valueKey: 'value',
  labelKey: 'label'
};