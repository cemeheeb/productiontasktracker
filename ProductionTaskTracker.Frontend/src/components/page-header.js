import PropTypes from 'prop-types';
import React from 'react';

export class PageHeader extends React.Component {
  render() {
    const { title, children } = this.props;
    return (
      <div className="row wrapper page-header" >
        <div className="col-lg-12">
          <h2>{title}</h2>
          {children}
        </div>
      </div>
    );
  }
}

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node
};
