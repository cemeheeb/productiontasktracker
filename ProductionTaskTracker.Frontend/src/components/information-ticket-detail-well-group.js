import PropTypes from "prop-types";
import moment from 'moment';
import React from "react";

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.scss';

import { InformationWell } from "./information-well";
import { InformationRequested } from "./information-requested";
import { InformationAttachments } from "./information-attachments";
import { InformationDepartment } from "./information-department";

import { DisplayTable } from "./display-table";
import { DisplayTableText } from "./display-table-text";

import slConverter from "../utilities/sl-converter";
import ticketTypeConverter from "../utilities/ticket-type-converter";
import propertyConverter from "../utilities/property-converter";
import extractDepartmentsBranch from '../utilities/extract-departments-branch';

import { PROPERTY_TYPE_LIST } from '../constants/configuration';

export class InformationTicketDetailWellGroup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pumps: [],
      contractors: []
    };

    this.convertTicketDetailPropertyValue = this.convertTicketDetailPropertyValue.bind(this);
  }

  componentDidMount() {
    this.props.ticketDetail.parameters.forEach((parameter, index) => {
      if (typeof parameter.pump_id !== 'undefined' && parameter.pump_id !== null) {
        this.props.actions.actionDictionaryPumpFilter(parameter.pump_id).then(
          x => {
            const newPumps = [...this.state.pumps];
            newPumps[index] = x.payload.title;
            this.setState({ pumps: newPumps });
          }
        );
      }
      if (typeof parameter.contractor_id !== 'undefined' && parameter.contractor_id !== null) {
        this.props.actions.actionDictionaryContractorFilter(parameter.contractor_id).then(
          x => {
            const newContractors = [...this.state.contractors];
            newContractors[index] = x.payload.name;
            this.setState({ contractors: newContractors });
          }
        );
      }
    });
  }

  convertTicketDetailPropertyValue(key, value, parameterIndex) {
    if (typeof key !== 'undefined' && key !== null && typeof value !== 'undefined' && value !== null) {
      switch (key) {
        case 'called_agents':
          return value.split(',').map(v => PROPERTY_TYPE_LIST[key].options.filter(x => x.value === v).map(x => x.label)).join(', ');
        case 'equipment_verification':
        case 'valve_verification':
        case 'research':
        case 'well_purpose':
        case 'treatment':
        case 'union_action_type':
        case 'well_period_mode':
        case 'request_type':
        case 'brigade_type':
        case 'repair_type':
        case 'event_type':
        case 'well_area':
        case 'dmg_type':
        case 'pump_typesize':
        case 'stop_reason':
          return PROPERTY_TYPE_LIST[key].options.filter(x => x.value === value).map(x => x.label);
        case 'ua':
        case 'ktpn':
        case 'sk':
        case 'agzu':
        case 'bg':
        case 'watering':
        case 'kvch':
        case 'multi_component':
        case 'carbonate':
        case 'inhibitor':
        case 'requested_epu':
        case 'requested_cnipr':
        case 'balance_delta':
        case 'crimping':
        case 'dmg':
        case 'dmg_before':
        case 'power_off':
        case 'pile_field_demontage':
        case 'preparing':
        case 'mixture':
        case 'service_area':
        case 'lock_available':
        case 'purge':
        case 'worked':
        case 'revision_svu':
        case 'replace_svu':
        case 'oil_component':
        case 'complex':
        case 'phase_check':
        case 'pump_crimping':
        case 'gathering':
          return value ? 'да' : 'нет';
        case 'probe_time':
        case 'start_time_plan':
        case 'start_time':
        case 'time_completion':
        case 'treatment_time':
        case 'obtain_time':
        case 'padding_time':
        case 'treatment_datebegin':
        case 'treatment_dateend': {
          const time = moment(value, 'DD.MM.YYYY HH:mm').isValid() ? moment(value, 'DD.MM.YYYY HH:mm') : moment(value);
          return time.format('DD.MM.YYYY HH:mm');
        }
        case 'contractor_id':
          return this.state.contractors[parameterIndex];
        case 'pump_id':
          return this.state.pumps[parameterIndex];
        case 'puller_type_id':
          return this.props.session.pullerTypes.filter(x => x.id === value).map(x => x.name)[0];
        default:
          if (typeof value === 'boolean') {
            return value ? 'да' : 'нет';
          }
          return value.toString();
      }
    } else return '-';
  }

  renderTicketType(ticketType, isNight) {
    return (
      <div key="information" className="form-group clearfix">
        <label className="control-label">Тип заявки</label>
        <div>{ticketTypeConverter(ticketType)}{isNight && ' [Ночная смена]'}</div>
      </div>
    );
  }

  renderDetailProperties(status, target, propertyValues, parameterIndex) {
    const output = [];

    let requested = null;
    if (typeof target.requested !== 'undefined' && typeof target.requested === "string" && status <= 5) {
      requested = target.requested.split(",");
      output.push(
        <div key="requested" className="form-group clearfix">
          <label className="control-label">{propertyConverter("requested")}</label>
          <InformationRequested parameters={requested.map(x => ({ key: x, label: propertyConverter(x) }))} />
        </div>
      );
    }

    const propertiesFounded = [];
    Object.keys(propertyValues).map(propertyName => {
      if (["requested", "note", "attachment_id", "switch"].indexOf(propertyName) >= 0) {
        return;
      }

      // Не для всех статусов нужно отображать запрашиваемые параметры
      if (requested !== null && requested.indexOf(propertyName) > -1 && [6, 8, 9].indexOf(status) < 0) {
        return;
      }
     
      const value = typeof target[propertyName] !== 'undefined' ? target[propertyName] : null;
      propertiesFounded.push({ key: propertyName, label: propertyConverter(propertyName), value });
    });

    if (propertiesFounded.length > 0) {
      output.push(
        <div className="form-group clearfix" key="properties">
          <label className="control-label">Параметры заявки</label>
          <div>
            <div className="col-md-6">
              <DisplayTable>
                {propertiesFounded.filter((item, index) => index % 2 == 0).map(x => <DisplayTableText key={x.key} label={x.label} value={this.convertTicketDetailPropertyValue(x.key, x.value, parameterIndex)} />)}
              </DisplayTable>
            </div>
            <div className="col-md-6">
              <DisplayTable>
                {propertiesFounded.filter((item, index) => index % 2 != 0).map(x => <DisplayTableText key={x.key} label={x.label} value={this.convertTicketDetailPropertyValue(x.key, x.value, parameterIndex)} />)}
              </DisplayTable>
            </div>
          </div>
        </div>
      );
    }

    return output;
  }

  renderNoteComponent(note) {
    return (
      <div key={"note"} className="form-group clearfix">
        <label className="control-label">{propertyConverter("note")}</label>
        <div>{note}</div>
      </div>
    );
  }

  renderSlComponent(sl) {
    return (
      <div key={"sl"} className="form-group clearfix">
        <label className="control-label">Уровень срочности</label>
        <div>{slConverter(sl)}</div>
      </div>
    );
  }

  renderAttachmentsComponent(attachmentID, actions) {
    return (
      <div key={"attachment_id"} className="form-group clearfix">
        <InformationAttachments attachmentID={attachmentID} actions={actions} />
      </div>
    );
  }

  renderExecutorComponent(departmentID, departments) {
    return (
      <div key={"executor"} className="form-group clearfix" >
        <label className="control-label">Исполнитель</label>
        <InformationDepartment departmentIDs={extractDepartmentsBranch(departmentID, departments)} departments={departments} />
      </div>
    );
  }

  render() {
    const {
      session,
      propertyValues,
      ticketDetail,
      actions: {
        actionInformationWell
      }
    } = this.props;

    if (typeof ticketDetail === 'undefined' || ticketDetail === null) {
      return null;
    }

    const components = [];
    ticketDetail.wells.forEach((well, index) => {
      const tabComponents = [];

      tabComponents.push(this.renderTicketType(ticketDetail.ticket_type, ticketDetail.is_night));

      tabComponents.push(
        <div key="wells" className="form-group clearfix" >
          <label className="control-label">Скважина</label>
          <InformationWell wellID={well.id} actions={{ actionInformationWell }} />
        </div>
      );

      // Основные данные
      tabComponents.push(this.renderDetailProperties(ticketDetail.status, ticketDetail.parameters[index], propertyValues[well.id.toString()], index));

      // Примечение
      if (typeof ticketDetail.parameters[index].note !== 'undefined' && typeof ticketDetail.parameters[index].note === "string") {
        tabComponents.push(this.renderNoteComponent(ticketDetail.parameters[index].note));
      }

      // SL
      if (typeof ticketDetail.sl === "number") {
        tabComponents.push(this.renderSlComponent(ticketDetail.sl));
      }

      // Вложения документов
      if (typeof ticketDetail.parameters[index].attachment_id !== 'undefined' && typeof (ticketDetail.parameters[index].attachment_id) !== 'undefined' && ticketDetail.parameters[index].attachment_id != null) {
        tabComponents.push(this.renderAttachmentsComponent(ticketDetail.parameters[index].attachment_id, this.props.actions));
      }

      // Исполнитель
      tabComponents.push(this.renderExecutorComponent(ticketDetail.department_id, session.departments));

      components.push(<TabPanel key={well.id}>{tabComponents}</TabPanel>);
    });


    return (<Tabs><TabList> {ticketDetail.wells.map((x) => <Tab key={x.id}>{x.code}</Tab>)} </TabList>{components}</Tabs>);
  }
}

InformationTicketDetailWellGroup.propTypes = {
  propertyValues: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
  ticketDetail: PropTypes.object.isRequired,
  actions: PropTypes.shape({
    actionInformationWell: PropTypes.func.isRequired,
    actionInformationField: PropTypes.func.isRequired,
    actionDictionaryPumpFilter: PropTypes.func.isRequired,
    actionDictionaryContractorFilter: PropTypes.func.isRequired
  }).isRequired
};