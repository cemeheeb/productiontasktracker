import PropTypes from 'prop-types';
import React from 'react';

export class InformationDepartment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      department: null
    };
  }

  render() {
    const {
      departmentIDs,
      departments
    } = this.props;

    if (typeof departmentIDs === 'undefined' || departmentIDs === null) {
      return null;
    }

    const targetIDs = [...departmentIDs];
    return (<ol className="breadcrumb">{targetIDs.reverse().map(x => departments.filter(d => d.id === x)[0]).map(x => (<li key={x.id}>{x.full_name}</li>))}</ol>);
  }
}

InformationDepartment.propTypes = {
  departmentIDs: PropTypes.array.isRequired,
  departments: PropTypes.array.isRequired
};