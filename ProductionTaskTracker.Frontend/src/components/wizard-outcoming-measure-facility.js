import PropTypes from 'prop-types';
import React from 'react';
import { reduxForm } from 'redux-form/immutable';

import { Wizard } from './wizard';

import { WizardFormOutcoming } from './wizard-form-outcoming';
import { WizardFormMeasureFacility } from './wizard-form-measure-facility';
import { WizardFormPropertyEditorMeasureFacility } from './wizard-form-property-editor-measure-facility';
import { WizardFormPreview } from './wizard-form-preview';
import { WizardStepForm } from './wizard-step-form';
import { validate } from './outcoming-wizard-validate-measure-facility';

import { PROPERTY_LIST_OUTCOMING } from '../constants/configuration';

import { filterActual } from '../utilities/routes';

const WizardStep = reduxForm({ 
  form: 'wizard-request',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false,
  validate
})(WizardStepForm);

export class WizardOutcomingMeasureFacility extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      configuration: PROPERTY_LIST_OUTCOMING,
      departments: [],
      attachments: []
    };

    this.actionAttachmentsUpload = this.actionAttachmentsUpload.bind(this);
    this.updateDepartments = this.updateDepartments.bind(this);
  }

  updateDepartments(ticketType, statusTo, departmentIDs, routes, departments) {
    if (typeof ticketType !== 'undefined') {
      const filtered = filterActual(routes)
        .filter(x => x.ticket_class.indexOf(ticketType[0].toUpperCase()) >= 0 && x.department_id_from == departmentIDs && x.status_from == null && x.status_to == statusTo)
        .sort((a, b) => a.executor_id > b.executor_id ? 1 : (a.executor_id < b.executor_id ? -1 : 0))
        .map(x => x.executor_id);
  
      this.setState({
        departments: 
          filtered.filter((x, index) => filtered.indexOf(x) == index)
            .map(x => departments.filter(y => y.id == x)[0])
      });
    }
  }

  actionAttachmentsUpload(attachments) {
    const { actions: { actionAttachmentsUpload, actionChangeForm } } = this.props;
    
    return actionAttachmentsUpload(attachments).then(x => {
      this.setState({attachments: x.payload}, () => {
        actionChangeForm('wizard-request', 'attachment_id', x.payload.length > 0 && x.payload[0] !== null ? x.payload[0].attachment_id : null);
      });
      return Promise.resolve(x);
    });
  }

  onPrevious() {
  }

  onNext() {
  }

  render() {
    const {
      configuration,
      attachments
    } = this.state;
    
    const {
      handlers: { 
        onSubmit
      },
      actions,
      session
    } = this.props;

    const step = {
      index: 0,
      text: "Тип заявки",
      component: <WizardFormOutcoming {... {
        actions,
        handlers: {
          updateDepartments: this.updateDepartments
        },
        session,
        departments: this.state.departments,
        inputData: session.wizardRequest.inputData
      }}/>
    };

    if (
      typeof session.wizardRequest.inputData.ticket_type !== 'undefined' &&
      session.wizardRequest.inputData.ticket_type !== null &&
      typeof session.wizardRequest.inputData.department_id !== 'undefined' &&
      session.wizardRequest.inputData.department_id !== null
    ) {
      step.next = [{
        index: 0,
        handler: () => Promise.resolve(),
        text: "Выбор объекта",
        component: <WizardFormMeasureFacility {... {
          actions,
          inputData: session.wizardRequest.inputData,
          fields: session.fields,
          isFieldsLoading: session.isFieldsLoading,
          clusters: session.clusters,
          isClustersLoading: session.isClustersLoading,
          measureFacilityPositions: session.measureFacilityPositions,
          isMeasureFacilityPositions: session.isMeasureFacilityPositionsLoading
        }} />
      }];

      step.next[0].next = [{
        index: 0,
        handler: () => Promise.resolve(),
        text: "Параметры заявки",
        component: <WizardFormPropertyEditorMeasureFacility {... {
          actions: { ...actions, actionAttachmentsUpload: this.actionAttachmentsUpload },
          session,
          inputData: session.wizardRequest.inputData,
          propertyValues: configuration.propertyValues[session.wizardRequest.inputData.ticket_type],
          pullerTypes: session.pullerTypes,
          isPullerTypesLoading: session.isPullerTypesLoading,
          attachments
        }}/>
      }];

      const ticketDetail = Object.assign({}, session.wizardRequest.inputData);

      const previewValues = Object.assign({}, configuration.propertyValues[ticketDetail.ticket_type]);
      if (configuration.propertyValues[ticketDetail.ticket_type].hasOwnProperty("switch")) {
        Object.keys(configuration.propertyValues[ticketDetail.ticket_type].switch).forEach(x => {
          if (typeof ticketDetail[x] !== 'undefined') {
            const switchCaseProperties = configuration.propertyValues[ticketDetail.ticket_type].switch[x][ticketDetail[x]];
            Object.keys(switchCaseProperties).forEach(property => {
              previewValues[property] = {};
            });
          }
        });
      }

      previewValues['note'] = {};

      step.next[0].next[0].next = [{
        index: 0,
        handler: () => Promise.resolve(),
        text: "Предпросмотр",
        component: <WizardFormPreview {... {
          propertyValues: previewValues,
          session,
          actions,
          ticketDetail,
          behaviour: "Single",
          isMeasureFacility: true
        }} />
      }];

      const targetDepartmentID = filterActual(session.routes)
        .filter(x => x.ticket_class.indexOf(session.wizardRequest.inputData.ticket_type[0].toUpperCase()) >= 0)
        .filter(x => x.status_to == session.wizardRequest.inputData.status && x.department_id_from == session.departmentIDs)
        .map(x => x.department_id_to)[0];
      
      step.next[0].next[0].next[0].next = [{
        index: 0,
        handler: () => {
          actions.actionTicketRegistration(session.wizardRequest.inputData, targetDepartmentID).then(() => { onSubmit(); });
          return Promise.reject();
        },
        text: "Подтвердить"
      }];
    }

    return <Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[step]} component={WizardStep} />;
  }
}

WizardOutcomingMeasureFacility.propTypes = {
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    wizardRequest: PropTypes.shape({
      inputData: PropTypes.object.isRequired
    }).isRequired,
    departments: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    isFieldsLoading: PropTypes.bool.isRequired,
    clusters: PropTypes.array.isRequired,
    isClustersLoading: PropTypes.bool.isRequired,
    pullerTypes: PropTypes.array.isRequired,
    wizardHistory: PropTypes.array
  }),
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};
