import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';
import { DateRangePicker } from 'react-dates';

import { PageHeader } from './page-header';

export class HistoryPageHeader extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      well: null,
      wellInputValue: '',
      wellOptions: [],
      dateRangeFocusedInput: null
    };

    this.onWellChange = this.onWellChange.bind(this);
    this.onWellInputChange = this.onWellInputChange.bind(this);
    this.onDateRangeChange = this.onDateRangeChange.bind(this);
    this.onDateRangeFocusChange = this.onDateRangeFocusChange.bind(this);
    this.onRefreshDataClick = this.onRefreshDataClick.bind(this);
  }

  onDateRangeChange({ startDate, endDate }) {
    const { 
      actions: { 
        actionSessionHistoryDateBeginChange,
        actionSessionHistoryDateEndChange
      }
    } = this.props;

    actionSessionHistoryDateBeginChange(startDate);
    actionSessionHistoryDateEndChange(endDate === null ? startDate : endDate);
  }

  onDateRangeFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  onWellChange(value) {
    const { handlers } = this.props;
    handlers && handlers.onChange && handlers.onChange(value);
  }

  onWellInputChange(search) {
    if (!search) {
      this.setState({ options: [this.state.well] });
      return;
    }

    this.props.actions.actionDictionaryDepartmentsWellFilter(search, [this.props.session.departmentID], 0, 30)
      .then(
        response => this.setState({ options: this.prepareOptions(response.payload.data) }),
        () => this.setState({ options: [] })
      );
  }

  onRefreshDataClick() {
    const {
      actions: {
        actionToastrPush
      },
      session : { 
        historyDateBegin,
        historyDateEnd
      }
    } = this.props;
    const { well } = this.state;

    if (typeof well === 'undefined' || well === null) {
      actionToastrPush("Не выбрана скважина!");
      return;
    }
    
    this.props.handlers.onRefreshData(well.id, historyDateBegin, historyDateEnd);
  }

  

  render() {
    const { wellOptions } = this.state;

    return (
      <PageHeader title="История">
        <form role="form" className="page-filter-form form-inline">
          <div className="form-group">
            <div className="history-page-header-select-well">
              <Select
                name="select-well"
                styles={{
                  input: () => ({ width: '200px', lineHeight: '44px', height: '44px'})
                }}
                value={this.state.well}
                options={wellOptions}
                onChange={this.onWellChange}
                onInputChange={this.onWellInputChange}
                onFocus={this.onWellFocus}
                isSearchable
                cache={false}
                placeholder="Введите номер скважины"
              />
            </div>
          </div>
          <div className="form-group">
            <div className="input-daterange input-group">
              <DateRangePicker
                startDate={this.props.session.historyDateBegin}
                endDate={this.props.session.historyDateEnd}
                onDatesChange={this.onDatesChange}
                onFocusChange={this.onDatesFocusChange}
                focusedInput={this.state.datesFocusedInput}
                isOutsideRange={() => false}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <button type="button" className="btn btn-error" onClick={this.onRefreshDataClick}><i className="fa fa-refresh" /></button>
            </div>
          </div>
        </form>
      </PageHeader>
    );
  }
}

HistoryPageHeader.propTypes = {
  actions: PropTypes.object.isRequired,
  session: PropTypes.shape({
    wells: PropTypes.array.isRequired,
    historyDateBegin: PropTypes.object.isRequired,
    historyDateEnd: PropTypes.object.isRequired
  }),
  handlers: PropTypes.shape({
    onRefreshData: PropTypes.func.isRequired
  }).isRequired
};
