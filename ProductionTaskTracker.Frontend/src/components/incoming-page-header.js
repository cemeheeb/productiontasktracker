import PropTypes from 'prop-types';
import React from 'react';
import { DateRangePicker } from 'react-dates';
import classnames from 'classnames';
import moment from 'moment';

import { PageHeader } from './page-header';
import DateRangePhrases from '../constants/date-range-phrases';

export class IncomingPageHeader extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      focusedInput: null
    };

    this.onDatesChange = this.onDatesChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);

    this.onRefreshDataClick = this.onRefreshDataClick.bind(this);
  }

  componentWillMount() {
    this.onRefreshDataClick();
  }

  onDatesChange({ startDate, endDate }) {
    const { actions: { 
      actionSessionDateBeginChange,
      actionSessionDateEndChange
    }} = this.props;

    actionSessionDateBeginChange(startDate);
    actionSessionDateEndChange(endDate === null ? startDate : endDate);
  }

  onFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  onRefreshDataClick() {
    const { handlers: { onRefreshData }, session: { dateBegin, dateEnd }, enabled } = this.props;
    if (enabled) {
      onRefreshData(moment(dateBegin), moment(dateEnd));
    }
  }

  render() {
    const { enabled } = this.props;
    
    return (
      <PageHeader title="Входящие">
        <form role="form" className="page-filter-form form-inline">
          <div className="form-group">
            <div className="input-daterange input-group">
              <DateRangePicker
                displayFormat="DD.MM.YYYY"
                startDate={this.props.session.dateBegin}
                endDate={this.props.session.dateEnd}
                onDatesChange={this.onDatesChange}
                focusedInput={this.state.focusedInput}
                onFocusChange={this.onFocusChange}
                isOutsideRange={() => false}
                phrases={DateRangePhrases}
                minimumNights={0}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <button type="button" className={classnames("btn", "btn-error", {"btn-disabled" : !enabled})} onClick={this.onRefreshDataClick}><i className="fa fa-refresh" /></button>
            </div>
          </div>
        </form>
      </PageHeader>
    );
  }
}

/*
<div className="form-group">
  <div className="input-group">
    <label><input type="checkbox" />{`Только ${this.props.session}`}</label>
  </div>
</div>
*/

IncomingPageHeader.propTypes = {
  actions: PropTypes.shape({
    actionSessionDateBeginChange: PropTypes.func.isRequired,
    actionSessionDateEndChange: PropTypes.func.isRequired
  }),
  session: PropTypes.shape( {
    dateBegin: PropTypes.object.isRequired,
    dateEnd: PropTypes.object.isRequired
  }).isRequired,
  handlers: PropTypes.shape({
    onRefreshData: PropTypes.func.isRequired,
  }).isRequired,
  enabled: PropTypes.bool.isRequired
};