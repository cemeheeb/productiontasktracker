import PropTypes from "prop-types";
import moment from 'moment';
import React from "react";

import { InformationWell } from "./information-well";
import { InformationRequested } from "./information-requested";
import { InformationAttachments } from "./information-attachments";
import { InformationDepartment } from "./information-department";

import { DisplayTable } from "./display-table";
import { DisplayTableText } from "./display-table-text";

import slConverter from "../utilities/sl-converter";
import ticketTypeConverter from "../utilities/ticket-type-converter";
import propertyConverter from "../utilities/property-converter";
import extractDepartmentsBranch from '../utilities/extract-departments-branch';

import { PROPERTY_TYPE_LIST } from '../constants/configuration';

export class InformationTicketDetailWell extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pump: null,
      contractor: null,
      pullerType: null
    };

    this.convertTicketDetailPropertyValue = this.convertTicketDetailPropertyValue.bind(this);
  }

  componentDidMount() {
    const { pump_id, contractor_id } = this.props.ticketDetail;
    
    if (typeof pump_id !== 'undefined' && pump_id !== null) {
      this.props.actions.actionDictionaryPumpFilter(pump_id).then(
        x => {
          this.setState({ pump: x.payload.title });
        }
      );
    }
    if (typeof contractor_id !== 'undefined' && contractor_id !== null) {
      this.props.actions.actionDictionaryContractorFilter(contractor_id).then(
        x => {
          this.setState({ contractor: x.payload.name });
        }
      );
    }
  }

  convertTicketDetailPropertyValue(key, value) {
    if (typeof key !== 'undefined' && key !== null && typeof value !== 'undefined' && value !== null) {
      switch (key) {
        case 'called_agents':
          return value.split(',').map(v => PROPERTY_TYPE_LIST[key].options.filter(x => x.value === v).map(x => x.label)).join(', ');
        case 'equipment_verification':
        case 'valve_verification':
        case 'research':
        case 'well_purpose':
        case 'treatment':
        case 'union_action_type':
        case 'well_period_mode':
        case 'request_type':
        case 'brigade_type':
        case 'repair_type':
        case 'event_type':
        case 'well_area':
        case 'dmg_type':
        case 'pump_typesize':
        case 'stop_reason':
          return PROPERTY_TYPE_LIST[key].options.filter(x => x.value === value).map(x => x.label);
        case 'ua':
        case 'ktpn':
        case 'sk':
        case 'agzu':
        case 'bg':
        case 'watering':
        case 'kvch':
        case 'multi_component':
        case 'carbonate':
        case 'inhibitor':
        case 'requested_epu':
        case 'requested_cnipr':
        case 'balance_delta':
        case 'crimping':
        case 'dmg':
        case 'dmg_before':
        case 'power_off':
        case 'pile_field_demontage':
        case 'preparing':
        case 'mixture':
        case 'service_area':
        case 'lock_available':
        case 'purge':
        case 'worked':
        case 'revision_svu':
        case 'replace_svu':
        case 'oil_component':
        case 'complex':
        case 'phase_check':
        case 'pump_crimping':
        case 'gathering':
          return value ? 'да' : 'нет';
        case 'probe_time':
        case 'start_time_plan':
        case 'start_time':
        case 'time_completion':
        case 'treatment_time':
        case 'obtain_time':
        case 'padding_time':
        case 'treatment_datebegin':
        case 'treatment_dateend': {
          const time = moment(value, 'DD.MM.YYYY HH:mm').isValid() ? moment(value, 'DD.MM.YYYY HH:mm') : moment(value);
          return time.format('DD.MM.YYYY HH:mm');
        }
        case 'contractor_id':
          return this.state.contractor;
        case 'pump_id':
          return this.state.pump;
        case 'puller_type_id':
          return this.props.session.pullerTypes.filter(x => x.id === value).map(x => x.name)[0];
        default:
          if (typeof value === 'boolean') {
            return value ? 'да' : 'нет';
          }
          
          return value.toString();
      }
    } else return '-';
  }

  render() {
    const {
      propertyValues,
      session,
      ticketDetail,
      actions: {
        actionInformationWell
      }
    } = this.props;

    if (typeof ticketDetail === 'undefined' || ticketDetail === null) {
      return null;
    }

    const components = [];
    components.push(
      <div key="information" className="form-group clearfix">
        <label className="control-label">Тип заявки</label>
        <div>{ticketTypeConverter(ticketDetail.ticket_type)}{ticketDetail.is_night && ' [Ночная смена]'}</div>
      </div>
    );

    if (typeof ticketDetail.well_id !== 'undefined') {
      components.push(
        <div key="well_id" className="form-group clearfix" >
          <label className="control-label">Скважина</label>
          <InformationWell wellID={ticketDetail.well_id} actions={{ actionInformationWell }} />
        </div>
      );
    }

    let requested = null;
    if (ticketDetail.hasOwnProperty("requested") && typeof ticketDetail.requested === "string" && [0, 1, 2].indexOf(ticketDetail.status) > -1) {
      requested = ticketDetail.requested.split(",");
      components.push(
        <div key="requested" className="form-group clearfix">
          <label className="control-label">{propertyConverter("requested")}</label>
          <InformationRequested parameters={requested.map(x => ({ key: x, label: propertyConverter(x) }))} />
        </div>
      );
    }

    const propertiesFounded = [];
    Object.keys(propertyValues).map(propertyName => {
      if (["behaviour", "requested", "note", "attachment_id", "switch"].indexOf(propertyName) >= 0) {
        return;
      }

      // Не для всех статусов нужно отображать запрашиваемые параметры
      if (requested !== null && requested.indexOf(propertyName) > -1 && [6, 8, 9].indexOf(ticketDetail.status) < 0) {
        return;
      }

      const value = ticketDetail.hasOwnProperty(propertyName) ? ticketDetail[propertyName] : null;
      propertiesFounded.push({ key: propertyName, label: propertyConverter(propertyName), value });
    });

    if (propertiesFounded.length > 0) {
      components.push(
        <div className="form-group clearfix" key="properties">
          <label className="control-label">Параметры заявки</label>
          <div>
            <div className="col-md-6">
              <DisplayTable>
                {propertiesFounded.filter((item, index) => index % 2 == 0).map(x => <DisplayTableText key={x.key} label={x.label} value={this.convertTicketDetailPropertyValue(x.key, x.value)} />)}
              </DisplayTable>
            </div>
            <div className="col-md-6">
              <DisplayTable>
                {propertiesFounded.filter((item, index) => index % 2 != 0).map(x => <DisplayTableText key={x.key} label={x.label} value={this.convertTicketDetailPropertyValue(x.key, x.value)} />)}
              </DisplayTable>
            </div>
          </div>
        </div>
      );
    }

    if (ticketDetail.hasOwnProperty("note") && typeof ticketDetail.note === "string") {
      components.push(
        <div key={"note"} className="form-group clearfix">
          <label className="control-label">{propertyConverter("note")}</label>
          <div>{ticketDetail["note"]}</div>
        </div>
      );
    }

    if (ticketDetail.hasOwnProperty("sl") && typeof ticketDetail.sl === "number") {
      components.push(
        <div key={"sl"} className="form-group clearfix">
          <label className="control-label">Уровень срочности</label>
          <div>{slConverter(ticketDetail["sl"])}</div>
        </div>
      );
    }

    if (ticketDetail.hasOwnProperty("attachment_id") && typeof (ticketDetail.attachment_id) !== 'undefined' && ticketDetail.attachment_id !== null) {
      components.push(
        <div key={"attachment_id"} className="form-group clearfix">
          <InformationAttachments attachmentID={ticketDetail.attachment_id} actions={this.props.actions} />
        </div>
      );
    }

    components.push(
      <div key={"executor"} className="form-group clearfix">
        <label className="control-label">Исполнитель</label>
        <InformationDepartment departmentIDs={extractDepartmentsBranch(ticketDetail.department_id, session.departments)} departments={session.departments} />
      </div>
    );

    return (<div>{components}</div>);
  }
}

InformationTicketDetailWell.propTypes = {
  propertyValues: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
  ticketDetail: PropTypes.object.isRequired,
  actions: PropTypes.shape({
    actionInformationWell: PropTypes.func.isRequired,
    actionInformationField: PropTypes.func.isRequired,
    actionDictionaryPumpFilter: PropTypes.func.isRequired,
    actionDictionaryContractorFilter: PropTypes.func.isRequired
  }).isRequired
};