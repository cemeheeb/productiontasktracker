import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';

export class SelectReduxArray extends React.Component {
  constructor(props) {
    super(props);

    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onBlur() {
    const { input: { onBlur, value } } = this.props;
    return onBlur(value);
  }

  onChange(option) {
    const { input: { onChange }, handlers } = this.props;

    if (option !== null) {
      handlers && handlers.onChange && handlers.onChange(option.value ? JSON.stringify(option) : null);
      return onChange(typeof option !== "undefined" && option != null ? JSON.stringify(option) : null);
    } 

    return onChange(null);
  }

  render() {
    const { className, input: { value, onFocus }, meta: { error } } = this.props;

    return (
      <div className="select-container">
        <Select
          {...this.props}
          className={className}
          onBlur={this.onBlur}
          onChange={this.onChange}
          onFocus={onFocus}
          value={value ? JSON.parse(value) : []}
        />
        {(error) &&
          <div style={{ 'fontSize': '12px', 'color': 'rgb(244, 67, 54)' }}>Обязательно для заполнения</div>
        }
      </div>
    );
  }
}

SelectReduxArray.propTypes = {
  handlers: PropTypes.shape({
    onChange: PropTypes.func
  }).isRequired,
  className: PropTypes.string,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string,
    warning: PropTypes.string
  }).isRequired,
  isMulti: PropTypes.bool,
  isSearchable: PropTypes.bool,
  isLoading: PropTypes.bool,
  value: PropTypes.object,
  placeholder: PropTypes.string,
  simpleValue: PropTypes.bool,
  matchPos: PropTypes.string,
  options: PropTypes.array.isRequired
};

SelectReduxArray.defaultProps = {
  handlers: {}
};