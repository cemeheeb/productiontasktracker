import PropTypes from 'prop-types';
import React from 'react';

import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { PumpPageHeader } from './pump-page-header';
import { PumpTable } from './pump-table';

export class PumpPage extends React.Component {
  constructor(props) {
    super(props);

    this.onRefreshData = this.onRefreshData.bind(this);

    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);

    this.onImport = this.onImport.bind(this);
    this.invalidateData = this.invalidateData.bind(this);

    this.state = {
      data: [],
      index: 1,
      size: 30,
      sizeTotal: 0,
      isBusy: false
    };
  }

  onRefreshData() {
    this.setState({index: 1, isBusy: true}, () => {
      this.invalidateData();
    });
  }

  onPageChange(index, size) {
    this.setState({ index, size, isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onSizePerPageList(size) {
    this.setState({ size, isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onImport() {
    const { actionPumpImport } = this.props.actions;
    this.setState({ isBusy: true }, () => {
      actionPumpImport()
        .then(() => this.invalidateData())
        .catch(() => this.setState({ isBusy: false }));
    });
  }
  
  invalidateData() {
    this.props.actions.actionDictionaryPumpSearchFilter(null, this.state.index, this.state.size)
    .then(response => {
      this.setState({ isBusy: false });

      if (response.error) {
        return;
      }

      const { data, size_total } = response.payload;
      this.setState({ data, sizeTotal: size_total });
    });
  }

  render() {
    const { data, index, size, sizeTotal, isBusy } = this.state;

    return (
      <div>
        <PumpPageHeader
          handlers={{ 
            onRefreshData: this.onRefreshData,
            onImport: this.onImport,
            onPumpFilterChanged: this.onPumpFilterChanged 
          }}
          enabled={!isBusy}
        />
        <div className="loading-bar-wrapper"><LoadingBar className="loading-bar"/></div>
        <div className="container-fluid">
          <div className="wrapper wrapper-content">
            <PumpTable {...{
              data,
              handlers: {
                onPageChange: this.onPageChange,
                onSizePerPageList: this.onSizePerPageList,
              },
              index,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
              size,
              sizeTotal
            }}/>
          </div>
        </div>
      </div>
    );
  }
}

PumpPage.propTypes = {
  actions: PropTypes.object.isRequired,
  session: PropTypes.shape({
    departmentIDs: PropTypes.array.isRequired,
    positionRolename: PropTypes.string.isRequired,
    wizardControlled: PropTypes.shape({
      isVisible:PropTypes.bool.isRequired
    }).isRequired,
    routes: PropTypes.array.isRequired
  }).isRequired
};