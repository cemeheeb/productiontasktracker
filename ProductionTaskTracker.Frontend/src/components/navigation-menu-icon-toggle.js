import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export class NavigationMenuIconToggle extends React.Component {
  render() {
    const { className, icon, children, quantity } = this.props;

    return (
        <li className={className}>
          <a className="notifications-toggle" onClick={this.props.handleClick}>
            <i className={classnames("fa", icon)} />{ quantity > 0 ? <span className="label label-primary">{quantity}</span> : null }{children}
          </a>
        </li>
    );
  }
}

NavigationMenuIconToggle.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.node,
  quantity: PropTypes.number.isRequired,
  handleClick: PropTypes.func
};

NavigationMenuIconToggle.defaultProps = {
  quantity: 0
};