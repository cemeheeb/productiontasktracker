import PropTypes from 'prop-types';
import React from 'react';

import { reduxForm, Field } from 'redux-form/immutable';

import { FormFieldMinimal } from './form-field-minimal';

class Form extends React.PureComponent {
  render() {
    return (
      <div>
        <div className="form-group">
          <label>Название</label>
          <div className="input-group">
            <Field name="name"
              component={FormFieldMinimal}
              placeholder="Название"
            />
          </div>
          <div className="input-group">
            <div className="pull-right">
              <button className="m-t-sm btn btn-primary btn-outline" onClick={this.props.handlers.onSave} >Сохранить</button>
              <button className="m-t-sm btn btn-danger btn-outline" onClick={this.props.handlers.onCancel}>Отмена</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  handlers: PropTypes.object.isRequired
};

export const AdministrateDictionaryForm = reduxForm({
  form: 'administrate-dictionary'
})(Form);