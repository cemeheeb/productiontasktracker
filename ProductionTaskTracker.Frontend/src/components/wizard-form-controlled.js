import PropTypes from 'prop-types';
import React from 'react';

import { Field } from 'redux-form/immutable';

import { SelectRedux } from './select-redux';

import {
  D1, D2
} from '../constants/ticket-types';

import ticketTypeConverter from '../utilities/ticket-type-converter';
import { filterActual } from '../utilities/routes';

const createOption = (ticketType) => ({ label: ticketTypeConverter(ticketType), value: ticketType });

function getTicketTypesByClass(ticketClass) {
  switch (ticketClass) {
    case 'D':
      return [D1, D2];
  }
}

export class WizardFormControlled extends React.Component {
  constructor(props) {
    super(props);

    this.onTicketTypeChange = this.onTicketTypeChange.bind(this);

    const { session: { routes } } = props;
    let include = '';

    filterActual(routes)
      .filter(x => x.status_from == null).forEach(x => {
        for (let i = 0; i < x.ticket_class.length; i++) {
          const letter = x.ticket_class.charAt(i);
          if (include.indexOf(letter.toUpperCase()) < 0 && letter === 'D') {
            include = include + letter;
          }
        }
      });

    const ticketTypes = [];
    for (let i = 0; i < include.length; i++) {
      getTicketTypesByClass(include[i]).forEach(x => ticketTypes.push(x));
    }

    this.state = {
      ticketTypes: ticketTypes.map(x => createOption(x))
    };
  }

  componentWillMount() {
    const { inputData } = this.props;
    if (typeof inputData !== 'undefined' && typeof inputData.ticketType !== 'undefined') {
      const { session: { departmentIDs, routes, departments }, handlers: { updateDepartments } } = this.props;
      updateDepartments(inputData.ticketType, 0, departmentIDs, routes, departments);
    }
  }
  
  onTicketTypeChange(ticketType) {
    const { session: { departmentIDs, routes, departments }, handlers: { updateDepartments } } = this.props;
    
    if (typeof ticketType === 'undefined') {
      this.setState({ statuses: [] });
    } else {
      this.props.actions.actionInitializeForm('wizard-controlled', { status_from:null });
      updateDepartments(ticketType, 0, departmentIDs, routes, departments);
    }
  }

  render() {
    const { departments } = this.props;
    return (
      <div className="col-md-12">
        <div className="form-group">
          <label>Тип заявки</label>
          <div className="input-group">
            <Field name="ticket_type"
              component={SelectRedux}
              options={this.state.ticketTypes}
              handlers={{ onChange: this.onTicketTypeChange }}
              placeholder="Выберите тип заявки"
              isClearable={false}
              matchPos="start"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Исполнитель</label>
          <div className="input-group">
            <Field name="department_id"
                component={SelectRedux}
                options={departments}
                isSearchable={false}
                value={this.props.inputData.department_id}
              />
          </div>
        </div>
      </div>
    );
  }
}

WizardFormControlled.propTypes = {
  actions: PropTypes.shape({
    actionInitializeForm: PropTypes.func.isRequired,
    actionChangeForm: PropTypes.func.isRequired
  }).isRequired,
  handlers: PropTypes.shape({
    updateDepartments: PropTypes.func.isRequired
  }),
  session: PropTypes.shape({
    routes: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    departmentIDs: PropTypes.array.isRequired
  }),
  departments: PropTypes.array.isRequired,
  inputData: PropTypes.shape({
    ticket_type: PropTypes.string,
    department_id: PropTypes.number,
    status: PropTypes.number
  }).isRequired
};