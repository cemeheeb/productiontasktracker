import PropTypes from 'prop-types';
import React from 'react';

export class ModalHeader extends React.Component {
  render() {
    return <div className="layout-modal-header">{this.props.title}<a className="pull-right" onClick={this.props.handlers.onClose}><i className="close-button fa fa-times" /></a></div>;
  }
}

ModalHeader.propTypes = {
  title: PropTypes.string,
  handlers: PropTypes.shape({
    onClose: PropTypes.func.isRequired
  }).isRequired
};