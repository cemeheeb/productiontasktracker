import moment from 'moment';
import { PROPERTY_LIST_INCOMING } from '../constants/configuration';

const validateMoment = (values, field) => {
  const value = values.get(field);
  const valueMoment = moment(value, 'DD.MM.YYYY HH:mm');

  return values.has(field) && value !== null && valueMoment !== '01.01.0001 00:00' && valueMoment.isValid();
};

const initializeParameters = (count) => {
  const parameters = [];
  while (count--) {
    parameters.push({});
  }

  return parameters;
};

export const validate = values => {
  const errors = {};
  const requiredText = 'Поле обязательно для заполнения';

  if (!values.has('ticket_type') || values.get('ticket_type') == null) {
    errors.ticket_type = requiredText;
  } else {
    const ticketType = values.get('ticket_type');
    const propertyValues = PROPERTY_LIST_INCOMING.propertyValues[ticketType];

    if (ticketType && values.has('parameters') && values.get('parameters').size > 0) {
      values.get('parameters').forEach((parameter, index) => {
        if (typeof parameter !== 'undefined') {
          Object.keys(propertyValues).forEach(x => {
            if (['note'].indexOf(x) >= 0) {
              return;
            }
            
            if (!parameter.has(x) || parameter.get(x) === null || parameter.get(x) === '') {
              errors.parameters = (typeof errors.parameters !== 'undefined' && !errors.parameters.hasOwnProperty('_error') && errors.parameters.length === values.get('parameters').size) ? errors.parameters : initializeParameters(values.get('parameters').size);
              errors.parameters[index][x] = requiredText;
            } else {
              ['start_time', 'start_time_plan', 'time_completion', 'treatment_datebegin', 'treatment_dateend', 'probe_time'].forEach(timeParameter => {
                if (x === timeParameter && !validateMoment(parameter, x)) {
                  errors.parameters = (errors.parameters && !errors.parameters.hasOwnProperty('_error') && errors.parameters.length === values.get('parameters').size) ? errors.parameters : initializeParameters(values.get('parameters').size);
                  errors.parameters[index][x] = `Поле ${x} за границами допустимого диапазона`;
                }
              });

              // Так же необходимо учесть switch-case параметры
              if (typeof propertyValues.switch !== 'undefined' && propertyValues.switch !== null) {
                Object.keys(propertyValues.switch).forEach((x) => {
                  const switchCaseProperties = propertyValues.switch[x][parameter.get(x)];
                  if (typeof switchCaseProperties !== 'undefined') {
                    Object.keys(switchCaseProperties).forEach(property => {
                      if (!parameter.has(property) || parameter.get(property) == null) {
                        errors.parameters = (errors.parameters && !errors.parameters.hasOwnProperty('_error')) ? errors.parameters : initializeParameters(values.get('parameters').size);
                        errors.parameters[index][property] = `Поле ${property} обязательно для заполнения`;
                      }
                    });
                  }
                });
              }
            }
          });
        }
      });

      if (!errors.hasOwnProperty('parameters') && !values.get('parameters').toJS().every(x => typeof x !== 'undefined')) {
        errors.parameters = { _error: 'Параметры заявок заполнены не по всем скважинам' };
      }
    } else {
      errors.parameters = { _error: 'Параметры заявок заполнены не по всем скважинам' };
    }
  }

  if (Object.keys(errors).length > 0) {
    console.error({ errors });
  }

  return errors;
};