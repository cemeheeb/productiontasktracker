import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';

import ticketTypeConverter from '../utilities/ticket-type-converter';
import ticketStatusConverter from '../utilities/ticket-status-converter';

export class Notification extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
    this.onCloseClick = this.onCloseClick.bind(this);
  }

  onClick() {
    const { actionWizardResponse } = this.props.actions;
    actionWizardResponse(this.props.data.id, this.props.data.behaviour);
  }

  onCloseClick() {
    const { data, actions: { actionInformationTicketDetailVisitPush } } = this.props;
    actionInformationTicketDetailVisitPush([data.ticket_movement_id]);
  }

  render() {
    return (
      <li className="notification-ticket">
        <div className="clearfix" >
          <a className="pull-right close-link" onClick={this.onCloseClick}><i className="fa fa-times" /></a>
          <div className="pull-left"><strong>{`№ ${this.props.data.ticket_movement_id}`}</strong></div>
        </div>
        <ul className="notification-ticket-description" onClick={this.onClick}>
          <li>{ticketTypeConverter(this.props.data.ticket_type)}</li>
          <li>Статус: {ticketStatusConverter(this.props.data.status)}</li>
        </ul>
        <div className="notification-ticket-footer clearfix">
          <i className="fa fa-clock-o"/>{moment(this.props.data.created).format('DD.MM.YYYY HH:mm')}
        </div>
      </li>
    );
  }
}

Notification.propTypes = {
  actions: PropTypes.shape({
    actionWizardResponse: PropTypes.func.isRequired,
    actionInformationTicketDetailVisitPush: PropTypes.func.isRequired
  }).isRequired,
  data: PropTypes.object.isRequired
};