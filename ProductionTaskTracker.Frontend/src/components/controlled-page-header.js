import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import { SingleDatePicker } from 'react-dates';
import Select from 'react-select';
import moment from 'moment';

import { PageHeader } from './page-header';

const ALLOWED_TYPES = [
  { label: 'ВНР', value: 'vnr' },
  { label: 'Подконтрольные', value: 'controlled' }
];

export class ControlledPageHeader extends React.Component {
  constructor(props) {
    super(props);

    this.onAddClick = this.onAddClick.bind(this);
    this.onRefreshDataClick = this.onRefreshDataClick.bind(this);
    
    this.onFocusChange = this.onFocusChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onTypeChange = this.onTypeChange.bind(this);

    console.error();

    this.state = {
      date: moment(),
      focusedInput: false,
      type: ALLOWED_TYPES[0]
    };
  }
  
  componentDidMount() {
    this.onRefreshDataClick();
  }

  onFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  onDateChange({ date }) {
    console.error({ date });
  }

  onDepartmentChange({ date }) {
    console.error({ date });
  }

  onTypeChange(option) {
    this.setState({ type: option });
  }
  
  onAddClick() {
    const { handlers: { onAdd }, enabled } = this.props;
    if (enabled) {
      onAdd();
    }
  }

  onRefreshDataClick() {
    const { handlers: { onRefreshData }, enabled } = this.props;
    if (enabled) {
      onRefreshData();
    }
  }

  render() {
    const { enabled } = this.props;

    return (
      <PageHeader title="Скважины ВНР">
        <form role="form" className="page-filter-form form-inline">
          <div className="form-group">
            <div className="input-group">
              <button type="button" className="btn btn-primary" onClick={this.onAddClick}><i className="fa fa-plus" /></button>
            </div>
          </div>
          <div className="form-group">
            <div className="input-daterange input-group">
              <SingleDatePicker
                date={this.state.date}
                onDateChange={this.onDateChange}
                onFocusChange={this.onFocusChange}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <Select
                styles={{ input: () => ({ width: '200px', height: '44px', lineHeight: '44px' }) }}
                value={this.state.type}
                options={ALLOWED_TYPES}
                onChange={this.onTypeChange}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <button type="button" className={classnames("btn", "btn-error", {"btn-disabled" : !enabled})} onClick={this.onRefreshDataClick}><i className="fa fa-refresh" /></button>
            </div>
          </div>
        </form>
      </PageHeader>
    );
  }
}

ControlledPageHeader.propTypes = {
    handlers: PropTypes.shape({ 
      onAdd: PropTypes.func.isRequired,
      onRefreshData: PropTypes.func.isRequired
    }).isRequired,
    enabled: PropTypes.bool.isRequired
};
