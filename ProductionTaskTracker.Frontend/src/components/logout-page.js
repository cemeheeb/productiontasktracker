import PropTypes from 'prop-types';
import React from 'react';

export class LogoutPage extends React.Component {
  componentWillMount() {
    this.props.actions.actionLogout();
  }

  render() {
    return <div />;
  }
}

LogoutPage.propTypes = {
  actions: PropTypes.shape({
    actionLogout: PropTypes.func.isRequired
  }).isRequired
};