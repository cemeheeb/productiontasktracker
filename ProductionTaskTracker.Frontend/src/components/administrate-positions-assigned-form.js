import PropTypes from 'prop-types';
import React from 'react';

import { Field, reduxForm } from 'redux-form/immutable';
import { FormFieldMinimal } from './form-field-minimal';
import { SelectRedux } from './select-redux';

class Form extends React.Component {
  render() {
    const { title, handlers: { onAssign, onRemove } } = this.props;

    return (
      <div className="form-inline">
        <h1>{title}</h1>
        <div className="form-group">
          <div className="input-group">
            <Field name="membership_id"
              className="membership-select"
              component={SelectRedux}
              options={this.props.memberships.map(x => ({ label: x.fullname, value: x.id }))}
              placeholder="Не назначен"
              readOnly={typeof onAssign !== 'undefined' && onAssign !== null}
            />
          </div>
        </div>
        <div className="form-group">
          <div className="input-group">
            { typeof onRemove !== 'undefined' && onRemove !== null &&
              <Field name="date_begin"
                component={FormFieldMinimal}
                readOnly
              />
            }
          </div>
        </div>
        <div className="form-group">
          <div className="input-group">
            <div className="administrative-toolbar">
              {typeof onAssign !== 'undefined' && onAssign !== null && <button className="btn btn-primary btn-outline" onClick={onAssign}>Назначить</button>}
              {typeof onRemove !== 'undefined' && onRemove !== null && <button className="btn btn-danger btn-outline" onClick={onRemove}>Снять</button>}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  title: PropTypes.string,
  memberships: PropTypes.array.isRequired,
  handlers: PropTypes.object.isRequired
};

export const AdministratePositionsAssignedForm = reduxForm({ form: 'administrate-position-assigned' })(Form);