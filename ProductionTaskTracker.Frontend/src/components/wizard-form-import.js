import PropTypes from 'prop-types';
import React from 'react';
import { InstantUpload } from './instant-upload';

export class WizardFormImport extends React.PureComponent {
  render() {
    const { ticketType, departmentIDs, sl, isNight, wellIDs, actions: { actionExportDocument, actionImportUpload } } = this.props;
    return (
      <div className="col-md-12">
        <div className="form-group">
          <div className="input-group">
            <center><button onClick={() => actionExportDocument(ticketType, departmentIDs, sl, isNight, wellIDs)} className="btn btn-primary"><i className="fa fa-upload" /> Получить бланк</button></center>
          </div>
        </div>
        <div className="form-group">
          <div className="input-group">
            <InstantUpload actions={{ actionImportUpload }} />
          </div>
        </div>
      </div>
    );
  }
}

WizardFormImport.propTypes = {
  ticketType: PropTypes.string.isRequired,
  departmentIDs: PropTypes.array.isRequired,
  sl: PropTypes.number.isRequired,
  isNight: PropTypes.bool.isRequired,
  wellIDs: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  attachments: PropTypes.object
};
