import React from 'react';
import { PropTypes } from 'prop-types';
import { Route } from 'react-router-dom';

import { renderMergedProps } from './render-merged-props';

const RouteExtended = ({ component, ...rest }) => {
  return (
    <Route {...rest} render={routeProps => {  
      return renderMergedProps(component, routeProps, rest);
    }} />
  );
};

RouteExtended.propTypes = {
  component: PropTypes.func.isRequired,
};

export { RouteExtended };