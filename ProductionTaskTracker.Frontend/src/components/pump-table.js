import PropTypes from 'prop-types';
import React from 'react';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export class PumpTable extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { index, size, sizeTotal, data } = this.props;
    return (nextProps.index !== index || nextProps.size !== size || nextProps.sizeTotal !== sizeTotal || nextProps.data !== data);
  }

  render() {
    const { index, size, sizeTotal, handlers: { onPageChange, onSizePerPageList }, data } = this.props;

    return (
      <div>
        <BootstrapTable hover condensed remote pagination
          data={data}
          fetchInfo={{ dataTotalSize: sizeTotal }}
          trClassName={this.trClassFormat}
          options={{ 
            page: index,
            sizePerPage: size,
            onPageChange,
            onSizePerPageList,
            expandRowBgColor: 'rgb(242, 255, 163)',
            onRowDoubleClick: this.onEdit,
            noDataText: 'Нет данных',
            paginationPosition: 'top'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
        >
          <TableHeaderColumn width="100" dataField="id" dataAlign="right" isKey={true} >Код</TableHeaderColumn>
          <TableHeaderColumn dataField="title" >Наименование</TableHeaderColumn>
          <TableHeaderColumn dataField="type_ecn" >Тип УЭЦН</TableHeaderColumn>
          <TableHeaderColumn dataField="gb" >Габарит 5 или 5А</TableHeaderColumn>
          <TableHeaderColumn dataField="nominal_flow" >Номинальный напор</TableHeaderColumn>
          <TableHeaderColumn dataField="nominal_feed" >Номинальная подача</TableHeaderColumn>
          <TableHeaderColumn dataField="col1" >Кол1</TableHeaderColumn>
          <TableHeaderColumn dataField="col2" >Кол2</TableHeaderColumn>
          <TableHeaderColumn dataField="col2" >Кол3</TableHeaderColumn>
          <TableHeaderColumn dataField="country" >Страна</TableHeaderColumn>
          <TableHeaderColumn dataField="factory" >Завод</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

PumpTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};
