import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import { reduxForm } from 'redux-form/immutable';

import { Wizard } from './wizard';

import { WizardFormPropertyEditorMeasureFacility } from './wizard-form-property-editor-measure-facility';
import { WizardFormPreview } from './wizard-form-preview';

//import ticketStatusConvert from '../utilities/ticket-status-converter';
import ticketStatusConverterToVerb from '../utilities/ticket-status-converter-to-verb';

import { WizardStepForm } from './wizard-step-form';
import { validate } from './incoming-wizard-validate-measure-facility';
import { PROPERTY_LIST_INCOMING, PROPERTY_LIST_OUTCOMING } from '../constants/configuration';
import { PROGRESS_READY, COMPLETED } from '../constants/ticket-statuses';

import { filterActual } from '../utilities/routes';

const WizardStep = reduxForm({
  form: 'wizard-response',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false,
  validate
})(WizardStepForm);

export class WizardIncomingMeasureFacility extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pumps: [],
      isPumpsLoading: false,
      attachments: []
    };

    this.actionAttachmentsUpload = this.actionAttachmentsUpload.bind(this);
  }

  actionAttachmentsUpload(attachments) {
    const { actions: { actionAttachmentsTicketDetailUpload, actionChangeForm } } = this.props;

    return actionAttachmentsTicketDetailUpload(attachments, this.props.session.wizardResponse.ticketDetail.id).then(x => {
      this.setState({ attachments: x.payload });
      actionChangeForm('wizard-response', 'attachment_id', x.payload.length > 0 && x.payload[0] !== null ? x.payload[0].attachment_id : null);
      return Promise.resolve(x);
    });
  }

  onPrevious() {
  }

  onNext() {
  }

  render() {
    const {
      attachments
    } = this.state;

    const {
      session: {
        departmentIDs,
        wizardResponse: {
          inputData,
          ticketDetail
        },
        routes,
        pullerTypes,
        isPullerTypesLoading
      },
      handlers: {
        onSubmit
      },
      actions
    } = this.props;

    if (ticketDetail == null)
      return null;

    const statuses = filterActual(routes, moment(ticketDetail.ticket_created))
      .filter(x => x.ticket_class.indexOf(ticketDetail.ticket_type[0].toUpperCase()) > -1)//                                                            TODO: Исправить хак, 2 - id ЦИО
      .filter(x => x.status_from == ticketDetail.status && x.department_id_from == departmentIDs && (x.department_id_to == ticketDetail.department_id || x.department_id_to == 2))
      .map(x => x.status_to);

    const previewValues = {};

    let requestable = [];
    const outcomingConfiguration = PROPERTY_LIST_OUTCOMING.propertyValues[ticketDetail.ticket_type];
    if (outcomingConfiguration.hasOwnProperty("requested") && outcomingConfiguration.requested.hasOwnProperty('options')) {
      requestable = outcomingConfiguration.requested.options.map(x => x.value);
    }

    // Исходящие
    Object.keys(ticketDetail).forEach(x => {
      switch (x) {
        case 'id':
        case 'ticket_id':
        case 'ticket_created':
        case 'membership_id':
        case 'department_id':
        case 'well_id':
        case 'mf_position':
        case 'field_code':
        case 'cluster':
        case 'status':
        case 'sl':
        case 'ticket_type':
          break;
        default:
          previewValues[x] = {};
          break;
      }
    });


    if (ticketDetail.hasOwnProperty("requested") && typeof ticketDetail.requested === "string") {
      ticketDetail.requested.split(',').forEach(x => {
        previewValues[x] = {};
      });
    }

    // Входящие тображаем только для заявок в статусе после отработки
    if (ticketDetail.status > 5) {
      const incomingConfiguration = PROPERTY_LIST_INCOMING.propertyValues[ticketDetail.ticket_type];
      if (incomingConfiguration.hasOwnProperty("switch")) {
        Object.keys(incomingConfiguration.switch).forEach(x => {
          const switchCaseProperties = incomingConfiguration.switch[x][ticketDetail[x]];
          Object.keys(switchCaseProperties).forEach(property => {
            previewValues[property] = {};
          });
        });
      }
    }

    if (outcomingConfiguration.hasOwnProperty("switch")) {
      Object.keys(outcomingConfiguration.switch).forEach(x => {
        const switchCaseProperties = outcomingConfiguration.switch[x][ticketDetail[x]];
        Object.keys(switchCaseProperties).forEach(property => {
          previewValues[property] = {};
        });
      });
    }

    delete previewValues['note'];
    previewValues['note'] = {};

    const {
      note, // eslint-disable-line no-unused-vars
      ...other
    } = inputData;

    const ticketDetailPreview = Object.assign({}, other, ticketDetail);
    const step = {
      index: 0,
      text: "Предпросмотр",
      component: <WizardFormPreview {... {
        propertyValues: previewValues,
        session: this.props.session,
        actions,
        behaviour: "Single",
        ticketDetail: ticketDetailPreview,
        isMeasureFacility: true
      }} />
    };


    let propertyValues;
    let nextStep = step;
    if (statuses.length > 0) {
      nextStep.next = statuses.map((x, i) => {
        const text = ticketStatusConverterToVerb(x);
        switch (x) {
          case COMPLETED:
          case PROGRESS_READY: {
            // Исключаем из выборки параметры которые находятся в перечне запрашиваемых 
            // параметров по данному типу заявки, но которые не были запрошены при формировании запроса
            propertyValues = { ...PROPERTY_LIST_INCOMING.propertyValues };
            const propertyValuesTicketType = { ...propertyValues[ticketDetail.ticket_type] };
            const propertyValuesToDelete = [];
            const requested = typeof ticketDetail.requested !== 'undefined' && ticketDetail.requested !== null ? ticketDetail.requested.split(',') : [];

            requestable.forEach(value => {
              if (requested.indexOf(value) < 0) {
                propertyValuesToDelete.push(value);
              }
            });

            propertyValuesToDelete.forEach(value => {
              delete propertyValuesTicketType[value.toString()];
            });

            propertyValues[ticketDetail.ticket_type] = propertyValuesTicketType;
            break;
          }
          default: {
            // Для всех остальных
            propertyValues = { ...PROPERTY_LIST_INCOMING.propertyValues };
            propertyValues[ticketDetail.ticket_type] = { note: {} };
          }
        }

        const output = {
          index: i,
          handler: () => Promise.resolve(),
          text,
          component: <WizardFormPropertyEditorMeasureFacility {... {
            actions: { ...actions, actionAttachmentsUpload: this.actionAttachmentsUpload },
            propertyValues: propertyValues[ticketDetail.ticket_type],
            session: this.props.session,
            inputData,
            pullerTypes,
            isPullerTypesLoading,
            attachments
          }} />
        };

        output.next = [{
          index: 0,
          handler: () => {
            inputData.status = x;
            actions.actionIncomingTicketMovement({ ticket_id: ticketDetail.ticket_id, ...inputData }).then(() => onSubmit());
            return Promise.reject();
          },
          text: 'Подтвердить'
        }];

        return output;
      });
    }

    return <Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[step]} component={WizardStep} />;
  }
}

WizardIncomingMeasureFacility.propTypes = {
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    positionRolename: PropTypes.string.isRequired,
    wizardResponse: PropTypes.shape({
      ticketDetail: PropTypes.object,
      inputData: PropTypes.object.isRequired
    }).isRequired,
    routes: PropTypes.array.isRequired,
    pullerTypes: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    departmentIDs: PropTypes.array.isRequired,
    wizardHistory: PropTypes.array
  }).isRequired,
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};
