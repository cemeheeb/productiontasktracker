import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';

export class SelectRedux extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(option) {
    const { input: { onChange }, handlers } = this.props;
    
    if (option !== null) {
      handlers && handlers.onChange && handlers.onChange(option.value);
      return onChange(option.value);
    } 

    return onChange(null);
  }

  render() {
    const { className, input: { value, onFocus }, meta: { error } } = this.props;

    return (
      <div className="select-container">
        <Select
          {...this.props}
          className={className}
          onChange={this.onChange}
          onFocus={onFocus}
          value={
            typeof value !== "object"
              ? this.props.options.filter(option => option.value === value)[0]
              : value
          }
        />
        {(error) &&
          <div style={{ 'fontSize': '12px', 'color': 'rgb(244, 67, 54)' }}>Обязательно для заполнения</div>
        }
      </div>
    );
  }
}

SelectRedux.propTypes = {
  handlers: PropTypes.shape({
    onChange: PropTypes.func
  }).isRequired,
  className: PropTypes.string,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string,
    warning: PropTypes.string
  }).isRequired,
  isMulti: PropTypes.bool,
  isSearchable: PropTypes.bool,
  isLoading: PropTypes.bool,
  value: PropTypes.object,
  placeholder: PropTypes.string,
  simpleValue: PropTypes.bool,
  matchPos: PropTypes.string,
  options: PropTypes.array.isRequired
};

SelectRedux.defaultProps = {
  handlers: {},
  valueKey: 'value',
  labelKey: 'label'
};