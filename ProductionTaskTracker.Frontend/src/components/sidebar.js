import React from 'react';
import PropTypes from 'prop-types';

export class Sidebar extends React.Component {
  render() {
    const { notificationsFxToggled, handleNotificationsFxToggleClick, handleNotificationsToggleClick } = this.props;

    return (
      <div className="sidebar-panel">
        {this.props.children}
        <div className="sidebar-title"><button className="btn btn-warning btn-block btn-outline" onClick={handleNotificationsFxToggleClick}>{ !notificationsFxToggled ? "Включить звук" : "Выключить звук"}</button></div>
        <div className="sidebar-title"><button className="btn btn-primary btn-block btn-outline" onClick={handleNotificationsToggleClick}>Скрыть</button></div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  notificationsFxToggled: PropTypes.bool.isRequired,
  handleNotificationsToggleClick: PropTypes.func.isRequired,
  handleNotificationsFxToggleClick: PropTypes.func.isRequired,
  children: PropTypes.node
};