import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export class NavigationMenu extends React.Component {
  render() {
    return (
        <ul className={classnames('nav navbar-nav', this.props.className)}>
            {this.props.children}
        </ul>
    );
  }
}

NavigationMenu.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};


