import PropTypes from "prop-types";
import React from "react";

import { InformationWell } from "./information-well";
import { InformationDepartment } from "./information-department";

import { DisplayTable } from "./display-table";
import { DisplayTableText } from "./display-table-text";

import ticketTypeConverter from "../utilities/ticket-type-converter";
import propertyConverter from "../utilities/property-converter";
import extractDepartmentsBranch from '../utilities/extract-departments-branch';

import { PROPERTY_TYPE_LIST } from '../constants/configuration';

export class InformationControlledDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pump: null
    };

    this.convertTicketDetailPropertyValue = this.convertTemplatePropertyValue.bind(this);
  }

  componentDidMount() {
    const { pump_id } = this.props.template;
    if (typeof pump_id !== 'undefined' && pump_id !== null) {
      this.props.actions.actionDictionaryPumpFilter(pump_id).then(
        x => {
          this.setState({pump: x.payload.title});
        }
      );
    }
  }

  convertTemplatePropertyValue(key, value) {
    if (typeof key !== 'undefined' && key !== null && typeof value !== 'undefined' && value !== null) {
      switch (key) {
        case 'repair_type':
        case 'event_type':
          return PROPERTY_TYPE_LIST[key].options.filter(x => x.value === value).map(x => x.label);
        case 'pump_id':
          return this.state.pump;
        default:
          return value.toString();
      }
    } else return '-';
  }

  render() {
    const {
      configuration:{
        propertyValues
      },
      session,
      template,
      actions: {
        actionInformationWell
      }
    } = this.props;

    if (typeof template === 'undefined' || template === null) {
      return null;
    }

    const components = [];
    components.push(
      <div key="information" className="form-group">
        <label className="control-label">Тип заявки</label>
        <div>{ticketTypeConverter(template.ticket_type)}</div>
      </div>
    );

    components.push(
      <div key="well_id" className="form-group" >
        <label className="control-label">Объект</label>
        <InformationWell wellID={template.well_id} actions={{actionInformationWell}} />
      </div>
    );

    const propertiesFounded = [];
    Object.keys(propertyValues[template.ticket_type]).map(propertyName => {
      const value = typeof template[propertyName] !== 'undefined' ? template[propertyName] : null;
      propertiesFounded.push({key:propertyName, label:propertyConverter(propertyName), value });
    });

    if (propertiesFounded.length > 0) {
      components.push(
        <div className="form-group" key="properties">
          <label className="control-label">Параметры</label>
          <div>
            <div className="col-md-6">
              <DisplayTable>
                {propertiesFounded.filter((item, index) => index%2 == 0).map(x => <DisplayTableText key={x.key} label={x.label} value={this.convertTemplatePropertyValue(x.key, x.value)}/>) }
              </DisplayTable>
            </div>
            <div className="col-md-6">
              <DisplayTable>
                {propertiesFounded.filter((item, index) => index%2 != 0).map(x => <DisplayTableText key={x.key} label={x.label} value={this.convertTemplatePropertyValue(x.key, x.value)}/>) }
              </DisplayTable>
            </div>
          </div>
        </div>
      );
    }

    components.push(
      <div key={"department_id"} className="form-group">
        <label className="control-label">Исполнитель</label>
        <InformationDepartment departmentIDs={extractDepartmentsBranch(template.department_id, session.departments)} departments={session.departments}/>
      </div>
    );
    
    return (<div>{components}</div>);
  }
}

InformationControlledDetail.propTypes = {
  configuration: PropTypes.shape({
    propertyValues: PropTypes.object.isRequired
  }).isRequired,
  session: PropTypes.object.isRequired,
  template: PropTypes.object.isRequired,
  actions: PropTypes.shape({
    actionInformationWell: PropTypes.func.isRequired,
    actionInformationField: PropTypes.func.isRequired,
    actionDictionaryPumpFilter: PropTypes.func.isRequired,
    actionDictionaryContractorFilter: PropTypes.func.isRequired
  }).isRequired
};