import React from 'react';
import PropTypes from 'prop-types';

export class TabHeader extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    event.preventDefault();
    this.props.handlers.onClick(this.props.index);
  }

  render() {
    const { index, label, isActive } = this.props;
    
    return (
      <li key={index}>
        <a href="#"
          className={isActive ? 'active' : ''}
          onClick={this.onTabClickHandler()}>
          {label}
        </a>
      </li>
    );
  }
}

TabHeader.propTypes = {
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired
};

TabHeader.defaultProps = {
  isActive: false
};
