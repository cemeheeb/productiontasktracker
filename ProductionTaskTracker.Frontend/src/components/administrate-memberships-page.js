import PropTypes from 'prop-types';
import React from 'react';

import { AdministrateMembershipsTable } from './administrate-memberships-table';
import { AdministrateMembershipsForm } from './administrate-memberships-form';

export class AdministrateMembershipsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      index: 0,
      size: 10,
      sizeTotal: 0,
      isBusy: false,
      isModalDetailOpen: false
    };
    
    this.onRefreshData = this.onRefreshData.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);

    this.onSelect = this.onSelect.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onAddCancel = this.onAddCancel.bind(this);
    this.onAddSave = this.onAddSave.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEditCancel = this.onEditCancel.bind(this);
    this.onEditSave = this.onEditSave.bind(this);

    this.invalidateData = this.invalidateData.bind(this);
  } 

  componentDidMount() {
    this.onRefreshData();
  }

  onRefreshData() {
    this.setState({ isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onSelect(row, selected) {
    const isNew = (row == null || !selected) ? this.state.isNew : false;
    this.setState({ selectedRow: selected ? row : null, isNew }, () => {
      this.props.actions.actionInitializeForm('administrate-membership', row);
    });
  }

  onAdd() {
    this.setState({ selectedRow: null, isNew: true }, () =>
      this.props.actions.actionInitializeForm('administrate-membership', {
        firstname: '',
        lastname: '',
        patronym: '',
        rolename: ''
      })
    );
  }

  onAddSave() {
    const { created, ...inputData } = this.props.session.administrateMembershipInputData; // eslint-disable-line
    console.error({ inputData });
    this.setState({ isNew: false }, () => this.props.actions.actionMembershipAdd(inputData).then(() => this.onRefreshData()));
  }

  onAddCancel() {
    this.setState({ isNew: false });
  }

  onEdit() {
  }

  onEditSave() {
    this.setState({ selectedRow: null }, () => this.props.actions.actionMembershipUpdate(this.props.session.administrateMembershipInputData).then(() => this.onRefreshData()));
  }

  onEditCancel() {
    this.setState({ selectedRow: null });
  }

  onPageChange(index, size) {
    this.setState({ index, size, isBusy: true }, () => {
      this.invalidateData();
    });
  }

  onSizePerPageList(size) {
    this.setState({ size }, () => {
      this.invalidateData();
    });
  }

  invalidateData() {
    const { actions: { actionMembershipFilter } } = this.props;
    const { index, size } = this.state;

    this.setState({ isBusy: true }, () => {
      actionMembershipFilter(index, size)
        .then(response => {
          if (response.error) {
            this.setState({ isBusy: false });
            return;
          }

          const { data, size_total } = response.payload;
          this.setState({ isBusy: false, data, sizeTotal: size_total });
        });
    });
  }

  render() {
    const { data, index, size, sizeTotal, isNew, selectedRow } = this.state;
    const handlers = {
      onPageChange: this.onPageChange,
      onSizePerPageList: this.onSizePerPageList,
      onSelect: this.onSelect,
      onRefreshData: this.onRefreshData,
    };

    return (
      <div className="col-md-12" >
        <div className="col-md-6">
          <AdministrateMembershipsTable {...{ data, handlers, index, size, sizeTotal, selectedRow }} />
        </div>
        <div className="col-md-6">
          <div className="administrative-toolbar">
            <button type="button" title="Обновить" className="btn btn-error" onClick={this.onRefreshData}><i className="fa fa-refresh" /></button>
            {!isNew && <button type="button" title="Добавить" className="btn btn-primary" onClick={this.onAdd}><i className="fa fa-plus" /></button>}
          </div>
          {isNew && <AdministrateMembershipsForm title="Добавить" handlers={{ onSave: this.onAddSave, onCancel: this.onAddCancel }} isNew />}
          {selectedRow && <AdministrateMembershipsForm title="Изменить" handlers={{ onSave: this.onEditSave, onCancel: this.onEditCancel }} isNew={false}/>}
        </div>
      </div>
    );
  }
}

AdministrateMembershipsPage.propTypes = {
  session: PropTypes.shape({
    administrateMembershipInputData: PropTypes.object.isRequired
  }).isRequired,
  actions: PropTypes.shape({
    actionInitializeForm: PropTypes.func.isRequired,
    actionMembershipFilter: PropTypes.func.isRequired,
    actionMembershipAdd: PropTypes.func.isRequired,
    actionMembershipUpdate: PropTypes.func.isRequired
  }).isRequired
};