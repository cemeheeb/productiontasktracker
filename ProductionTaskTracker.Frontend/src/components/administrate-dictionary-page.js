import PropTypes from 'prop-types';
import React from 'react';

import { AdministrateDictionaryTable } from './administrate-dictionary-table';
import { mapDictionaryToRoute } from '../utilities/classifiers';

import { AdministrateDictionaryForm } from './administrate-dictionary-form';

export class AdministrateDictionaryPage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      size: 10,
      sizeTotal: 0,
      selectedRow: null,
      isNew: false
    };

    this.onRefreshData = this.onRefreshData.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);

    this.onAdd = this.onAdd.bind(this);
    this.onAddCancel = this.onAddCancel.bind(this);
    this.onAddSave = this.onAddSave.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEditCancel = this.onEditCancel.bind(this);
    this.onEditSave = this.onEditSave.bind(this);
    this.onRemove = this.onRemove.bind(this);

    this.invalidateData = this.invalidateData.bind(this);
  }

  componentDidMount() {
    this.onRefreshData(this.props.match.params.dictionary);
  }

  componentDidUpdate(prevProps) { 
    if (prevProps.match.params.dictionary !== this.props.match.params.dictionary) {
      this.onRefreshData(this.props.match.params.dictionary);
    }
  }
  
  onRefreshData() {
    this.setState({index: 1, isBusy: true}, () => {
      this.invalidateData(this.props.match.params.dictionary);
    });
  }

  onPageChange(index, size) {
    this.setState({ index, size, isBusy: true }, () => {
      this.invalidateData(this.props.match.params.dictionary);
    });
  }

  onSizePerPageList(size) {
    this.setState({ size }, () => {
      this.invalidateData(this.props.match.params.dictionary);
    });
  }

  onAdd() {
    this.setState({ selectedRow: null, isNew: true }, () => this.props.actions.actionInitializeForm('administrate-dictionary', {
      kind: mapDictionaryToRoute(this.props.match.params.dictionary)
    }));
  }

  onAddSave() {
    this.props.actions.actionDictionaryClassifierAdd(this.props.session.administrateDictionaryInputData).then(() => this.onRefreshData());
  }

  onAddCancel() {
    this.setState({ isNew: false });
  }

  onEdit(row, selected) {
    const isNew = (row == null || !selected) ? this.state.isNew : false;
    this.setState({ selectedRow: selected ? row : null, isNew }, () => {
      this.props.actions.actionInitializeForm('administrate-dictionary', row);
    });
  }

  onEditSave() {
    this.props.actions.actionDictionaryClassifierUpdate(this.props.session.administrateDictionaryInputData).then(() => this.onRefreshData());
  }

  onEditCancel() {
    this.setState({ selectedRow: null });
  }

  onRemove() {

  }

  invalidateData(dictionary) {
    const dictionaryCode = mapDictionaryToRoute(dictionary);
    this.props.actions
      .actionDictionaryClassifierFilter(dictionaryCode, this.state.index, this.state.size)
      .then(response => {
        this.setState({ isBusy: false });

        if (response.error) {
          return;
        }

        const { size_total } = response.payload;
        this.setState({ selectedRow: null, isNew: false, sizeTotal: size_total });
      });
  }

  render() {
    const { session: { dictionaries }, match: { params: { dictionary }} } = this.props;
    const { index, size, sizeTotal, isNew, selectedRow } = this.state;

    const handlers = {
      onPageChange: this.onPageChange,
      onSizePerPageList: this.onSizePerPageList,
      onSelect: this.onEdit,
      onRefreshData: this.onRefreshData
    };

    return (
      <div className="col-md-12">
        <div className="col-md-6">
          {Object.keys(dictionaries).indexOf(dictionary) < 0 ? <span>Не найдено</span> : <AdministrateDictionaryTable {...{ handlers, index, size, sizeTotal, selectedRow }} {...dictionaries[dictionary]} />}
        </div>
        <div className="col-md-6">
          <div className="administrative-toolbar">
            <button type="button" title="Обновить" className="btn btn-error" onClick={this.onRefreshData}><i className="fa fa-refresh" /></button>
            {!isNew && <button type="button" title="Добавить" className="btn btn-primary" onClick={this.onAdd}><i className="fa fa-plus" /></button>}
            {selectedRow && <button type="button" title="Удалить" className="btn btn-danger" onClick={this.onRemove} ><i className="fa fa-times" /></button>}
          </div>
          {isNew && <AdministrateDictionaryForm handlers={{ onSave: this.onAddSave, onCancel: this.onAddCancel }} />}
          {selectedRow && <AdministrateDictionaryForm handlers={{ onSave: this.onEditSave, onCancel: this.onEditCancel }}/>}
        </div>
      </div>
    );
  }
}

AdministrateDictionaryPage.propTypes = {
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};