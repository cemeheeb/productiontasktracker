import PropTypes from "prop-types";
import React from "react";

import { InformationControlledDetail } from "./information-controlled-detail";

export class WizardFormControlledPreview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pump: null
    };
  }

  componentDidMount() {
    const { pump_id } = this.props.template;

    if (typeof pump_id !== 'undefined' && pump_id !== null) {
      this.props.actions.actionDictionaryPumpFilter(pump_id).then(
        x => {
          this.setState({pump: x.payload.title});
        }
      );
    }
  }

  render() {
    const {
      template
    } = this.props;

    if (typeof template === 'undefined' || template === null) {
      return null;
    }

    return (
        <div className="wrapper">
          <InformationControlledDetail {... this.props}/>
        </div>
    );
  }
}

WizardFormControlledPreview.propTypes = {
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  template: PropTypes.object
};