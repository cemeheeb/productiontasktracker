import PropTypes from 'prop-types';
import React from 'react';
import { reduxForm, FieldArray } from 'redux-form/immutable';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import { Wizard } from './wizard';
import { WizardFormOutcoming } from './wizard-form-outcoming';
import { WizardFormWellPicker } from './wizard-form-well-picker';
import { WizardFormPropertyEditorWellGroup } from './wizard-form-property-editor-well-group';
import { WizardFormPreview } from './wizard-form-preview';
import { WizardStepForm } from './wizard-step-form';
import { validate } from './outcoming-wizard-validate-well-group';

import { PROPERTY_LIST_OUTCOMING } from '../constants/configuration';

import { filterActual } from '../utilities/routes';

const WizardStep = reduxForm({
  form: 'wizard-request',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false,
  validate
})(WizardStepForm);

export class WizardOutcomingWellGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      configuration: PROPERTY_LIST_OUTCOMING,
      departments: [],
      attachments: [],
      selectedParameterID: 0
    };

    this.updateDepartments = this.updateDepartments.bind(this);
    this.actionAttachmentsUpload = this.actionAttachmentsUpload.bind(this);
    this.renderPropertyEditor = this.renderPropertyEditor.bind(this);
    this.onTabClick = this.onTabClick.bind(this);
  }

  updateDepartments(ticketType, statusTo, departmentIDs, routes, departments) {
    if (typeof ticketType !== 'undefined') {
      const filtered = filterActual(routes)
        .filter(x => x.ticket_class.indexOf(ticketType[0].toUpperCase()) >= 0 && x.department_id_from == departmentIDs && x.status_from == null && x.status_to == statusTo)
        .sort((a, b) => a.executor_id > b.executor_id ? 1 : (a.executor_id < b.executor_id ? -1 : 0))
        .map(x => x.executor_id);

      this.setState({
        departments:
          filtered.filter((x, index) => filtered.indexOf(x) == index)
            .map(x => departments.filter(y => y.id == x)[0])
      });
    }
  }

  actionAttachmentsUpload(attachments, tag) {
    const { actions: { actionAttachmentsUpload, actionChangeForm } } = this.props;

    return actionAttachmentsUpload(attachments).then(x => {
      this.setState({ attachments: x.payload }, () =>
        actionChangeForm('wizard-request', `${tag}.attachment_id`, x.payload.length > 0 && x.payload[0] !== null ? x.payload[0].attachment_id : null)
      );
      return Promise.resolve(x);
    });
  }
  
  onTabClick(parameterID) {
    this.setState({ selectedParameterID: parameterID });
  }

  renderPropertyEditor({ fields, meta: { error } }) {
    const {
      attachments,
      configuration
    } = this.state;

    const {
      actions,
      session
    } = this.props;

    return (
      <div>
        <Tabs forceRenderTabPanel>
          <TabList> {fields.map((field, index) => <Tab key={field} onClick={() => this.onTabClick(index)}>{session.wizardRequest.inputData.wells[index].code}</Tab>) } </TabList>
          {
            fields.map((field, index) =>
              <TabPanel key={field}>
                <WizardFormPropertyEditorWellGroup {... {
                  id: field,
                  parameter: typeof session.wizardRequest.inputData.parameters !== 'undefined' ? session.wizardRequest.inputData.parameters[index] : undefined,
                  actions: { ...actions, actionAttachmentsUpload: this.actionAttachmentsUpload },
                  session,
                  inputData: session.wizardRequest.inputData,
                  propertyValues: configuration.propertyValues[session.wizardRequest.inputData.ticket_type],
                  pullerTypes: session.pullerTypes,
                  isPullerTypesLoading: session.isPullerTypesLoading,
                  attachments
                }} />
              </TabPanel>
            )
          }
        </Tabs>
        {error && <span className="error-box">{error}</span>}
      </div>
    );
  }
  
  onPrevious() {
  }

  onNext() {
  }

  onTemplateApply() {
    const { actions: { actionChangeFormFieldArray }, session: { wizardRequest: { inputData: { parameters, wells } } } } = this.props;
    const { well_id, ...parameter } = parameters[this.state.selectedParameterID];
    console.info({ ...parameter, well_id });
    
    parameters.forEach((x, index) => {
      const parameterNew = { ...parameter, well_id: wells[index].id };
      Object.keys(parameterNew).forEach((field) =>
        actionChangeFormFieldArray('wizard-request', "parameters", index, field, parameterNew[field])
      );
    });
  }

  render() {
    const {
      configuration
    } = this.state;

    const {
      handlers: {
        onSubmit
      },
      actions,
      session
    } = this.props;

    const targetDepartmentID = typeof session.wizardRequest.inputData.ticket_type === 'undefined' ?
      0 :
      filterActual(session.routes)
        .filter(x => x.ticket_class.indexOf(session.wizardRequest.inputData.ticket_type[0].toUpperCase()) >= 0)
        .filter(x => x.status_to == session.wizardRequest.inputData.status && session.departmentIDs.indexOf(x.department_id_from) > -1)
        .map(x => x.department_id_to)[0];

    const { wells, ...ticketRegistrationInputData } = session.wizardRequest.inputData;
    if (typeof wells !== 'undefined' && wells.length > 0 && typeof ticketRegistrationInputData.parameters !== 'undefined' && ticketRegistrationInputData.parameters.length > 0) {
      ticketRegistrationInputData.parameters.forEach((x, index) => { if (typeof x !== 'undefined') { x.well_id = wells[index] == null ? null : wells[index].id; } });
    }

    const stepCommit = {
      index: 0,
      handler: () => {
        actions.actionTicketRegistrationGroup(ticketRegistrationInputData, targetDepartmentID).then(() => { onSubmit(); });
        return Promise.reject();
      },
      text: "Подтвердить"
    };

    const ticketDetail = Object.assign({}, session.wizardRequest.inputData);
    const previewValues = {};
    
    if (typeof session.wizardRequest.inputData.ticket_type !== 'undefined' && typeof ticketDetail.parameters !== 'undefined') {
      ticketDetail.parameters.forEach(parameter => {
        if (typeof parameter !== 'undefined' && typeof parameter.well_id !== 'undefined') {
          previewValues[parameter.well_id] = Object.assign({}, configuration.propertyValues[ticketDetail.ticket_type]);

          if (typeof configuration.propertyValues[ticketDetail.ticket_type].switch !== 'undefined') {
            Object.keys(configuration.propertyValues[ticketDetail.ticket_type].switch).forEach(x => {
              if (typeof parameter[x] !== 'undefined' && parameter[x] != null) {
                const switchCaseProperties = configuration.propertyValues[ticketDetail.ticket_type].switch[x][parameter[x]];
                Object.keys(switchCaseProperties).forEach(property => {
                  previewValues[parameter.well_id][property] = {};
                });
              }
            });
          }

          previewValues[parameter.well_id]['note'] = {};
        }
      });
    }

    const stepPreview = {
      handler: () => Promise.resolve(),
      text: "Предпросмотр",
      component: <WizardFormPreview {... {
        propertyValues: previewValues,
        session,
        actions,
        ticketDetail,
        behaviour: "Group",
        isMeasureFacility: false
      }} />,
      next: [stepCommit]
    };

    let stepParameters = {};
    stepParameters = {
      handler: () => Promise.resolve(),
      text: "Параметры заявки",
      component: (
        <React.Fragment>
          < button className="btn btn-success pull-right" style={{ marginLeft: '5px' }} onClick={() => this.onTemplateApply()}>Применить ко всем</button>
          <FieldArray name="parameters" component={this.renderPropertyEditor} />
        </React.Fragment>
      ),
      next: [stepPreview]
    };

    const stepTicketType = {
      text: "Тип заявки",
      component: <WizardFormOutcoming {... {
        actions,
        handlers: {
          updateDepartments: this.updateDepartments
        },
        session,
        departments: this.state.departments,
        inputData: session.wizardRequest.inputData
      }} />,
    };

    if (
      typeof session.wizardRequest.inputData.ticket_type !== 'undefined' &&
      session.wizardRequest.inputData.ticket_type !== null &&
      typeof session.wizardRequest.inputData.department_id !== 'undefined' &&
      session.wizardRequest.inputData.department_id !== null
    ) {
      stepTicketType.next = [{
        index: 0,
        handler: () => Promise.resolve(),
        text: "Выбор объекта",
        component: <WizardFormWellPicker {... {
          session,
          actions,
          departmentIDs: session.wizardRequest.inputData.department_id
        }}
        />,
        next: stepParameters == null ? [] : [stepParameters]
      }];
    }

    return (
      <div>
        < Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[stepTicketType]} component={WizardStep} />
      </div>
    );
  }
}

WizardOutcomingWellGroup.propTypes = {
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    wizardRequest: PropTypes.shape({
      inputData: PropTypes.object.isRequired
    }).isRequired,
    departments: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    isFieldsLoading: PropTypes.bool.isRequired,
    clusters: PropTypes.array.isRequired,
    isClustersLoading: PropTypes.bool.isRequired,
    pullerTypes: PropTypes.array.isRequired,
    wizardHistory: PropTypes.array
  }),
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};
