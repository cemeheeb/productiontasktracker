import PropTypes from 'prop-types';
import React from 'react';
import { Field } from 'redux-form/immutable';

import { FormFieldCheckbox } from './form-field-checkbox';

import { SelectRedux } from './select-redux';

import { filterActual } from '../utilities/routes';

import {
  A1, A2, A3, A4,
  B1, B1A, B1B, B2, B3, B4, B5, B6, B8, B9, B10, B11, B12, B13,
  C,
  E1, E2, E3, E4, E5, E6, E7, E8,
  F,
  G
} from '../constants/ticket-types';

import ticketTypeConverter from '../utilities/ticket-type-converter';
import ticketStatusConverter from '../utilities/ticket-status-converter';

const createOption = (ticketType) => ({ label: ticketTypeConverter(ticketType), value: ticketType });

function getSLVisibility(ticketClass) {
  switch (ticketClass) {
    case 'A':
      return true;
    default:
      return false;
  }
}

function getTicketTypesByClass(ticketClass) {
  switch (ticketClass) {
    case 'A':
      return [A1, A2, A3, A4];
    case 'B':
      return [B1, B1A, B1B, B2, B3, B4, B5, B6, B8, B9, B10, B11, B12, B13];
    case 'C':
      return [C];
    case 'E':
      return [E1, E2, E3, E4, E5, E6, E7, E8];
    case 'F':
      return [F];
    case 'G':
      return [G];
  }
}

export class WizardFormOutcoming extends React.Component {
  constructor(props) {
    super(props);

    this.onTicketTypeChange = this.onTicketTypeChange.bind(this);
    this.onStatusChange = this.onStatusChange.bind(this);

    this.state = {
      ticketTypes: [],
      statuses: []
    };
  }

  componentDidMount() {
    const { inputData } = this.props;
    if (typeof inputData !== 'undefined' && typeof inputData.ticket_type !== 'undefined' && typeof inputData.status !== 'undefined') {
      const { session: { departmentIDs, routes, departments }, handlers: { updateDepartments } } = this.props;
      updateDepartments(inputData.ticket_type, inputData.status, departmentIDs, routes, departments);
    }
  }
  
  componentWillReceiveProps(nextProps) {
    const { session: { routes } } = nextProps;
    const include = [];

    // Фильтрация по статусам == null, т.к - данный компонент используется только для создания заявок
    filterActual(routes)
      .filter(x => x.status_from == null).forEach(x => {
        for (let i = 0; i < x.ticket_class.length; i++) {
          const letter = x.ticket_class.charAt(i);
          if (include.indexOf(letter.toUpperCase()) < 0 && letter !== 'D') {
            include.push(letter);
          }
        }
      });
    
    const ticketTypes = [];
    include.sort((a, b) => (a < b) ? -1 : (a > b) ? 1 : 0).forEach(ticketClass => {
      getTicketTypesByClass(ticketClass).forEach(x => ticketTypes.push(x));
    });

    this.setState({
      ticketTypes: ticketTypes.map(x => createOption(x))
    });
  }

  onTicketTypeChange(ticketType) {
    const { session: { departmentIDs, routes, departments }, handlers: { updateDepartments } } = this.props;
    
    if (typeof ticketType === 'undefined') {
      this.setState({ statuses: [] });
    } else if (ticketType === 'F') {
      const filtered = filterActual(routes).filter(x => x.ticket_class.indexOf(ticketType[0].toUpperCase()) >= 0).map(y => y.status_to);
      const statuses = filtered.filter((x, index) => filtered.indexOf(x) == index).filter(x => x == 2).map(x => ({ label:ticketStatusConverter(x), value: x }));

      this.props.actions.actionInitializeForm('wizard-request', { status_from: null });
      this.setState({ statuses });
      
      if (statuses.length > 0) {
        this.props.actions.actionChangeForm('wizard-request', 'status', statuses[0].value);
        updateDepartments(ticketType, statuses[0].value, departmentIDs, routes, departments);
      }
    } else {
      const filtered = filterActual(routes).filter(x => x.ticket_class.indexOf(ticketType[0].toUpperCase()) >= 0).map(y => y.status_to);
      const statuses = filtered.filter((x, index) => filtered.indexOf(x) == index).filter(x => x == 0 || x == 1).map(x => ({ label:ticketStatusConverter(x), value: x }));

      this.props.actions.actionInitializeForm('wizard-request', { status_from: null });
      this.setState({ statuses });
      
      if (statuses.length > 0) {
        this.props.actions.actionChangeForm('wizard-request', 'status', statuses[0].value);
        updateDepartments(ticketType, statuses[0].value, departmentIDs, routes, departments);
      }
    }
  }

  onStatusChange(statusTo) {
    const { inputData, session: { departmentIDs, routes, departments }, handlers: { updateDepartments } } = this.props;
    updateDepartments(inputData.ticketType, statusTo, departmentIDs, routes, departments);
  }

  render() {
    const { departments } = this.props;
    return (
      <div className="col-md-12">
        <div className="form-group">
          <label>Тип заявки</label>
          <div className="input-group">
            <Field name="ticket_type"
              component={SelectRedux}
              options={this.state.ticketTypes}
              handlers={{ onChange: this.onTicketTypeChange }}
              placeholder="Выберите тип заявки"
              isClearable={false}
              matchPos="start"
            />
          </div>
        </div>
        { this.props.inputData.ticket_type && getSLVisibility(this.props.inputData.ticket_type[0].toUpperCase()) && 
          <div className="form-group">
            <div className="row">
              <div className="col-md-8">
                <label>Уровень срочности</label>
                <div className="input-group">
                  <Field name="sl"
                    component={SelectRedux}
                    options={[{ label: 'SL1', value: "0"}, { label: 'SL2', value: "1"}, { label: 'SL3', value: "2"}]}
                    placeholder="Выберите срочность заявки"
                    isSearchable={false}
                    isClearable={false}
                  />
                </div>
              </div>
              <div className="col-md-4">
                <label className="left-checkbox" >
                  <span>Ночная смена</span>
                  <Field name="is_night"
                    component={FormFieldCheckbox}
                    value={this.props.inputData.is_night}
                  />
                </label>
              </div>
            </div>
          </div>
        }
        {
          this.state.statuses.length > 0 &&
          <Field name="status" className="hidden"
            component={SelectRedux}
            options={this.state.statuses}
            handlers={{ onChange: this.onStatusChange }}
            value={{value:this.props.inputData.status}}
            placeholder="Выберите статус заявки"
            readOnly={this.state.statuses.length > 1}
            isClearable={false}
          />
        }
        <div className="form-group">
          <label>Исполнитель</label>
          <div className="input-group">
            <Field name="department_id"
                component={SelectRedux}
                options={departments.map(x => ({ label: x.display_name, value: x.id.toString() }))}
                isSearchable={false}
                labelKey="display_name"
                valueKey="id"
                value={this.props.inputData.department_id}
              />
          </div>
        </div>
      </div>
    );
  }
}

WizardFormOutcoming.propTypes = {
  actions: PropTypes.shape({
    actionInitializeForm: PropTypes.func.isRequired,
    actionChangeForm: PropTypes.func.isRequired
  }).isRequired,
  handlers: PropTypes.shape({
    updateDepartments: PropTypes.func.isRequired
  }),
  session: PropTypes.shape({
    routes: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    departmentIDs: PropTypes.array.isRequired
  }),
  departments: PropTypes.array.isRequired,
  inputData: PropTypes.shape({
    ticket_type: PropTypes.string,
    is_night: PropTypes.bool,
  }).isRequired
};

WizardFormOutcoming.defaultTypes = {
  inputData: {
    is_night: false
  }
};