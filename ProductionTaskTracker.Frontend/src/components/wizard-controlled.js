import PropTypes from 'prop-types';
import React from 'react';
import { reduxForm } from 'redux-form/immutable';

import { Wizard } from './wizard';

import { WizardFormControlled } from './wizard-form-controlled';
import { WizardFormWellPicker } from './wizard-form-well-picker';
import { WizardFormControlledPreview } from './wizard-form-controlled-preview';
import { WizardStepForm } from './wizard-step-form';

import { PROPERTY_LIST_CONTROLLED } from '../constants/configuration';
import { filterActual } from '../utilities/routes';

const WizardStep = reduxForm({ 
  form: 'wizard-controlled',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false
})(WizardStepForm);

export class WizardControlled extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      configuration: PROPERTY_LIST_CONTROLLED,
      departments: []
    };

    this.updateDepartments = this.updateDepartments.bind(this);
  }

  updateDepartments(ticketType, statusTo, departmentIDs, routes, departments) {
    if (typeof ticketType !== 'undefined') {
      const filtered = filterActual(routes)
        .filter(x => x.ticket_class.indexOf(ticketType[0].toUpperCase()) >= 0 && x.department_id_from == departmentIDs && x.status_from == null && x.status_to == statusTo)
        .sort((a, b) => a.executor_id > b.executor_id ? 1 : -1)
        .map(x => x.executor_id);

      this.setState({
        departments: 
          filtered.filter((x, index) => filtered.indexOf(x) == index)
            .map(x => departments.filter(y => y.id == x)[0])
      });
    }
  }

  render() {
    const {
      configuration
    } = this.state;
    
    const {
      mode,
      handlers: { 
        onSubmit
      },
      actions,
      session
    } = this.props;

    switch (mode) {
      case 'add': {
        const step = {
          index: 0,
          text: "Тип заявки",
          component: <WizardFormControlled {... {
            actions,
            handlers: {
              updateDepartments: this.updateDepartments
            },
            session,
            departments: this.state.departments,
            inputData: session.wizardControlled.inputData
          }} />
        };

        let stepNext = step;
        if (
          typeof session.wizardControlled.inputData.ticket_type !== 'undefined' &&
          session.wizardControlled.inputData.ticket_type !== null &&
          typeof session.wizardControlled.inputData.department_id !== 'undefined' &&
          session.wizardControlled.inputData.department_id !== null
        ) {
          step.next = [{
            index: 0,
            handler: () => Promise.resolve(),
            text: "Выбор скважины",
            component: <WizardFormWellPicker {... {
              actions,
              departmentIDs: session.wizardControlled.inputData.department_id,
              wells: session.wells
            }} />
          }];
          stepNext = step.next[0];

          stepNext.next = [{
            index: 0,
            handler: () => Promise.resolve(),
            text: "Параметры заявки",
            component: <div {... {
              actions,
              inputData: session.wizardControlled.inputData,
              configuration,
              pullerTypes: session.pullerTypes,
              isPullerTypesLoading: session.isPullerTypesLoading
            }} />
          }];
        
          const {
            ticket_type,
            well_id,
            department_id,
            repair_type,
            event_type,
            pump_id,
            pump_depth
          } = session.wizardControlled.inputData;

          const template = {
            ticket_type,
            well_id,
            department_id,
            repair_type,
            event_type,
            pump_id,
            pump_depth
          };

          stepNext.next[0].next = [{
            index: 0,
            handler: () => Promise.resolve(),
            text: "Предпросмотр",
            component: <WizardFormControlledPreview {... {
              configuration,
              session,
              actions,
              template
            }} />
          }];

          stepNext.next[0].next[0].next = [{
            index: 0,
            handler: () => {
              actions.actionControlledCreate(session.wizardControlled.inputData).then(() => { onSubmit(); });
              return Promise.reject();
            },
            text: "Подтвердить"
          }];
        }

        return <Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[step]} component={WizardStep}/>;
      }
      case 'edit': {
        const step = {
          index: 0,
          handler: () => Promise.resolve(),
          text: "Параметры заявки",
          component: <div {... {
            actions,
            inputData: session.wizardControlled.inputData,
            configuration,
            pullerTypes: session.pullerTypes,
            isPullerTypesLoading: session.isPullerTypesLoading
          }}/> 
        };

        const {
          ticket_type,
          well_id,
          department_id,
          repair_type,
          event_type,
          pump_id,
          pump_depth
        } = session.wizardControlled.inputData;

        const template = {
          ticket_type,
          well_id,
          department_id,
          repair_type,
          event_type,
          pump_id,
          pump_depth
        };

        step.next = [{
          index: 0,
          handler: () => Promise.resolve(),
          text: "Предпросмотр",
          component: <WizardFormControlledPreview {... {
            configuration,
            session,
            actions,
            template
          }} />
        }];

        step.next[0].next = [{
          index: 0,
          handler: () => {
            actions.actionControlledUpdate(session.wizardControlled.inputData).then(() => { onSubmit(); });
            return Promise.reject();
          },
          text: "Подтвердить"
        }];

        return <Wizard actions={this.props.actions} history={this.props.session.wizardHistory} steps={[step]} component={WizardStep}/>;
      }
    }
  }
}

WizardControlled.propTypes = {
  mode: PropTypes.string.isRequired,
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    wizardControlled: PropTypes.shape({
      inputData: PropTypes.object.isRequired
    }).isRequired,
    departments: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    isFieldsLoading: PropTypes.bool.isRequired,
    clusters: PropTypes.array.isRequired,
    isClustersLoading: PropTypes.bool.isRequired,
    pullerTypes: PropTypes.array.isRequired,
    wizardHistory: PropTypes.array
  }),
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};
