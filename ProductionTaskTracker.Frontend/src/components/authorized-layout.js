import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import classnames from 'classnames';

import { RouteExtended } from './route-extended';

import { NavigationBrand } from './navigation-brand';
import { NavigationMenu } from './navigation-menu';
import { NavigationMenuLink } from './navigation-menu-link';
import { NavigationMenuStaticLink } from './navigation-menu-static-link';
import { NavigationMenuIconToggle } from './navigation-menu-icon-toggle';
import { Notifications } from './notifications';

import { HelpPage } from '../components/help-page';
import { IncomingPage } from '../components/incoming-page';
import { OutcomingPage } from '../components/outcoming-page';
import { ControlledPage } from '../components/controlled-page';
import { HistoryPage } from '../components/history-page';
import { WellPage } from '../components/well-page';
import { ReportPage } from '../components/report-page';
import { AdministratePage } from '../components/administrate-page';
import { LogoutPage } from '../components/logout-page';
import { NotFoundPage } from '../components/not-found-page';

import rolenameConverter from '../utilities/rolename-converter';

const REFRESH_DURATION = 30*1000;

export class AuthorizedLayout extends React.Component {
  constructor(props) {
    super(props);
    this.onNotificationClick = this.onNotificationClick.bind(this);
    this.onNotificationsFxToggleClick = this.onNotificationsFxToggleClick.bind(this);
    this.onLogoutClick = this.onLogoutClick.bind(this);
    this.invalidateData = this.invalidateData.bind(this);
  }

  componentDidMount() {
    this.invalidateData();
    this.intervalID = setInterval(this.invalidateData, REFRESH_DURATION);
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  onNotificationClick() {
    const { settings: { notificationsToggled }, actions: { actionSettingsSet } } = this.props;
    actionSettingsSet('notificationsToggled', !notificationsToggled);
  }
  
  onNotificationsFxToggleClick() {
    const { settings: { notificationsFxToggled }, actions: { actionSettingsSet } } = this.props;
    actionSettingsSet('notificationsFxToggled', !notificationsFxToggled);
  }
  
  onLogoutClick() {
    const { actions: { actionLogout } } = this.props;
    actionLogout();
  }
  
  invalidateData() {
    this.props.actions.actionIncomingUnreadedFilter();
  }

  render() {
    const { 
      session,
      settings,
      actions
    } = this.props;

    return (
      <div className={classnames('authorized-layout', { 'sidebar-content': settings.notificationsToggled })}>
        <div className="row border-bottom">
          <nav className="navbar navbar-static-top">
            <NavigationBrand />
            <div className="navbar-collapse collapse" id="navbar">
              <NavigationMenu>
                <NavigationMenuLink to="/tickets/incoming">Входящие</NavigationMenuLink>
                <NavigationMenuLink to="/tickets/outcoming">Отправленные</NavigationMenuLink>
                <NavigationMenuLink to="/controlled-wells">ВНР</NavigationMenuLink>
                <NavigationMenuLink to="/wells/history">История</NavigationMenuLink>
                <NavigationMenuLink to="/wells">Скважины</NavigationMenuLink>
                <NavigationMenuLink to="/reports">Отчеты</NavigationMenuLink>
                {typeof session.membershipRolenames !== 'undefined' && session.membershipRolenames.indexOf('administrator') > -1 ? <NavigationMenuLink to="/administrate">Администрирование</NavigationMenuLink> : null}
                <NavigationMenuStaticLink to="/static/Руководство%20пользователя%20СУПЗ.pdf">?</NavigationMenuStaticLink>
              </NavigationMenu>
              <NavigationMenu className="navbar-top-links navbar-right">
                <NavigationMenuIconToggle className="navbar-profile" icon="fa-user">{`${session.memberships.filter(x=> x.id == session.membershipID)[0].fullname}, ${session.positionName}, ${rolenameConverter(session.positionRolename)}`}</NavigationMenuIconToggle>
                { settings.notificationsToggled
                  ? null
                  : <NavigationMenuIconToggle icon="fa-bell" quantity={session.notifications.length} handleClick={this.onNotificationClick}/>
                }
                <NavigationMenuIconToggle icon="fa-sign-out" handleClick={this.onLogoutClick}>Выход</NavigationMenuIconToggle>
              </NavigationMenu>
            </div>
          </nav>
        </div>
        { settings.notificationsToggled
          ? (
              <Notifications
                actions={actions}
                notifications={session.notifications}
                notificationsFxToggled={settings.notificationsFxToggled}
                onNotificationClick={this.onNotificationClick}
                onNotificationsFxToggleClick={this.onNotificationsFxToggleClick}
                onLogoutClick={this.onLogoutClick}
              />
            )
          : null
        }
        <Switch>
          <RouteExtended path="/tickets/incoming"
            component={IncomingPage}
            {...{
              settings,
              session,
              actions
            }}
          />
          <RouteExtended path="/tickets/outcoming" 
            component={OutcomingPage} 
            {...{
              settings,
              session,
              actions
            }}
          />
          <RouteExtended path="/controlled-wells" 
            component={ControlledPage} 
            {...{
              settings,
              session,
              actions
            }}
          />
          <RouteExtended path="/wells/history"
            component={HistoryPage}
            {...{
              session,
              actions
            }}
          />
          <RouteExtended path="/wells"
            component={WellPage} 
            {...{
              session,
              actions
            }}
          />
          <RouteExtended path="/reports" 
            component={ReportPage} 
            {...{
              settings,
              session,
              actions
            }}
          />
          <RouteExtended path="/administrate"
            component={AdministratePage} 
            {...{
              session,
              actions
            }}
          />
          <Route path="/help" component={HelpPage} />
          <Route path="/logout" component={LogoutPage} />
          <Route path="/404" component={NotFoundPage} />
          <Redirect exact path="/" to="/tickets/incoming" />
          <Redirect to="/404" />
        </Switch>
      </div>
    );
  }
}

AuthorizedLayout.propTypes = {
  settings: PropTypes.shape({
    notificationsToggled: PropTypes.bool.isRequired,
    notificationsFxToggled: PropTypes.bool.isRequired
  }).isRequired,
  actions: PropTypes.shape({
    actionSettingsSet: PropTypes.func.isRequired,
    actionIncomingUnreadedFilter: PropTypes.func.isRequired
  }).isRequired,
  session: PropTypes.shape({
    notifications: PropTypes.array.isRequired,
    membershipID: PropTypes.string.isRequired,
    positionRolename: PropTypes.string.isRequired,
    membershipRolenames: PropTypes.array.isRequired
  }).isRequired,
  children: PropTypes.node
};
