import PropTypes from 'prop-types';
import React from 'react';

export class InformationAttachments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      attachments: []
    };
  }

  componentDidMount() {
    const { attachmentID } = this.props;
    if (typeof attachmentID === 'undefined' || attachmentID === null) {
      return;
    }
    
    this.props.actions.actionAttachmentsGet(this.props.attachmentID).then(x => {
      this.setState({ attachments: x.payload });
    });
  }

  render() {
    return (
      <div>
        <label className="control-label">Вложения:</label>
        <div className="display-list">
          {this.state.attachments.map(x => <div key={x.filename} className="badge"><a href={x.uri} target="_blank">{x.filename} - {Math.floor(x.filesize / 1024)} Кб</a></div>)}
        </div>
      </div>
    );
  }
}

InformationAttachments.propTypes = {
  actions: PropTypes.shape({
    actionAttachmentsGet: PropTypes.func.isRequired
  }),
  attachmentID: PropTypes.string.isRequired
};