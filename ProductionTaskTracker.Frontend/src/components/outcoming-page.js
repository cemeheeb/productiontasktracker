import PropTypes from 'prop-types';
import React from 'react';
import Modal from 'react-overlays/lib/Modal';

import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { OutcomingPageHeader } from './outcoming-page-header';
import { OutcomingTable } from './outcoming-table';

import { ModalHeader } from './modal-header';
import { WizardIncomingWell } from './wizard-incoming-well';
import { WizardIncomingWellGroup } from './wizard-incoming-well-group';
import { WizardOutcomingWell } from './wizard-outcoming-well';
import { WizardOutcomingWellGroup } from './wizard-outcoming-well-group';
import { WizardIncomingMeasureFacility } from './wizard-incoming-measure-facility';
import { WizardOutcomingMeasureFacility } from './wizard-outcoming-measure-facility';

import ticketTypeConverter from '../utilities/ticket-type-converter';
import ticketStatusConverter from '../utilities/ticket-status-converter';

import { filterActual } from '../utilities/routes';
import { encodeUri } from '../utilities/base64';

const REFRESH_DURATION = 2 * 60 * 1000;

export class OutcomingPage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.onRefreshData = this.onRefreshData.bind(this);

    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);

    this.onAdd = this.onAdd.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onRemove = this.onRemove.bind(this);

    this.onWizardCancel = this.onWizardCancel.bind(this);
    this.onWizardSubmit = this.onWizardSubmit.bind(this);

    this.onFilterChange = this.onFilterChange.bind(this);
    this.invalidateData = this.invalidateData.bind(this);

    this.state = {
      isMounted: false,
      data: [],
      dateBegin: props.session.dateBegin,
      dateEnd: props.session.dateEnd,
      filter: {},
      filterOptions: {},
      index: 1,
      size: 30,
      sizeTotal: 0,
      statuses: filterActual(this.props.session.routes)
        .filter(x => x.status_from === null && this.props.session.departmentIDs.indexOf(x.department_id_from) > -1)
        .map(x => x.status_to),
      isLoading: false
    };
  }

  componentDidMount() {
    this.intervalID = setInterval(this.invalidateData, REFRESH_DURATION);
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
    this.intervalID = 0;
  }

  onRefreshData(dateBegin, dateEnd, resetPage = true) {
    this.setState({ dateBegin, dateEnd, index: resetPage ? 1 : this.props.session.page, isLoading: true }, () => {
      this.invalidateData();
    });
  }

  onPageChange(index, size) {
    this.setState({ index, size, isLoading: true }, () => {
      this.props.actions.actionSessionPageChange(index);
      this.invalidateData();
    });
  }

  onSizePerPageList(size) {
    this.setState({ size }, () => {
      this.invalidateData();
    });
  }

  onAdd() {
    const { actionWizardRequest } = this.props.actions;
    actionWizardRequest();
  }

  onEdit(row) {
    const { actionWizardResponse } = this.props.actions;
    actionWizardResponse(row.behaviour === 'Group' ? row.id : row.ticket_detail_id, row.behaviour);
  }

  onRemove(row) {
    if (!confirm('Вы действительно хотите отменить заявку?')) {
      return;
    }

    const { actionIncomingTicketMovement, actionIncomingTicketGroupMovement } = this.props.actions;
    if (row.behaviour === 'Group') {
      actionIncomingTicketGroupMovement({
        id: row.id,
        status: 13
      }).then(() => {
        this.onRefreshData(this.state.dateBegin, this.state.dateEnd, false);
      });
    } else {
      actionIncomingTicketMovement({
        ticket_id: row.id,
        status: 13
      }).then(() => {
        this.onRefreshData(this.state.dateBegin, this.state.dateEnd, false);
      });
    }
  }

  onWizardCancel() {
    const { actions: { actionWizardReset, actionResetForm } } = this.props;

    actionWizardReset();
    actionResetForm("wizard-request");
    actionResetForm("wizard-response");
  }

  onWizardSubmit() {
    this.onWizardCancel();
    this.onRefreshData(this.state.dateBegin, this.state.dateEnd, false);
  }

  onFilterChange(filter) {
    const filterKeys = Object.keys(filter);
    if (filterKeys.length == 0) {
      this.setState({
        filter: {}
      }, () => this.invalidateData());
      return;
    }


    const nextFilter = {};
    filterKeys.forEach(filterKey => {
      const previousFilter = filter[filterKey].value;
      nextFilter[previousFilter.filterName] = previousFilter.value;
    });

    this.setState({
      filter: nextFilter
    }, () => this.invalidateData());
  }

  extractData(row) {
    const {
      id,
      ticket_type,
      status,
      department_id_from,
      department_id_to,
      well_id,
      field_сode,
      cluster,
      registered,
      completed
    } = row;

    return {
      id,
      ticket_type,
      status,
      department_id_from,
      department_id_to,
      well_id,
      field_сode,
      cluster,
      registered,
      completed
    };
  }

  invalidateData() {
    this.setState({ isLoading: true }, () => {
      const uri = encodeUri(JSON.stringify(this.state.filter));
      this.props.actions.actionOutcomingFilter(uri, this.state.dateBegin, this.state.dateEnd, this.state.index, this.state.size)
        .then(response => {
          this.setState({ isLoading: false }, () => {
            if (response.error) {
              return;
            }

            const { data, size_total } = response.payload;
            for (let row of data) {
              row.ticket_type_description = ticketTypeConverter(row.ticket_type);
              row.status_description = ticketStatusConverter(row.status);
            }

            this.setState({ data, filterOptions: response.payload.filter_options, sizeTotal: size_total }, () => {
              if (this.intervalID > 0) {
                clearInterval(this.intervalID);
                this.intervalID = setInterval(this.invalidateData, REFRESH_DURATION);
              }
            });
          });
        });
    });
  }

  render() {
    const {
      data,
      filter,
      filterOptions,
      dateBegin,
      dateEnd,
      index,
      size,
      sizeTotal,
      statuses,
      isLoading
    } = this.state;

    const {
      session,
      actions
    } = this.props;

    return (
      <div>
        <OutcomingPageHeader actions={actions} session={session} handlers={{ onRefreshData: this.onRefreshData, onAdd: statuses.length > 0 ? this.onAdd : null }} enabled={!isLoading} />
        <div className="loading-bar-wrapper"><LoadingBar className="loading-bar" /></div>
        <div className="container-fluid">
          <div className="wrapper wrapper-content">
            <OutcomingTable {...{
              data,
              handlers: {
                onFilterChange: this.onFilterChange,
                onPageChange: this.onPageChange,
                onSizePerPageList: this.onSizePerPageList,
                onAdd: this.onAdd,
                onEdit: this.onEdit,
                onRemove: this.onRemove
              },
              filter,
              filterOptions,
              dateBegin,
              dateEnd,
              isLoading,
              index,
              size,
              sizeTotal
            }} />
          </div>
        </div>
        {session.wizardRequest.isVisible &&
          <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" backdrop="static" show={session.wizardRequest.isVisible} onHide={this.onWizardCancel}>
            <div className="layout-modal-dialog" >
              <ModalHeader title={`Регистрация заявки ${session.wizardRequest.inputData && session.wizardRequest.inputData.ticket_type ? ticketTypeConverter(session.wizardRequest.inputData.ticket_type) : ''}`} handlers={{ onClose: this.onWizardCancel }} />
              <hr />
              {
                session.wizardRequest.isMeasureFacility == true ?
                  <WizardOutcomingMeasureFacility
                    session={session}
                    actions={actions}
                    handlers={{
                      onSubmit: this.onWizardSubmit
                    }}
                  /> :
                  typeof session.wizardRequest.inputData.wells !== 'undefined' && session.wizardRequest.inputData.wells.length > 1 ?
                    <WizardOutcomingWellGroup
                      session={session}
                      actions={actions}
                      handlers={{
                        onSubmit: this.onWizardSubmit
                      }}
                    />
                    :
                    <WizardOutcomingWell
                      session={session}
                      actions={actions}
                      handlers={{
                        onSubmit: this.onWizardSubmit
                      }}
                    />
              }
            </div>
          </Modal>
        }
        {session.wizardResponse.isVisible &&
          <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" backdrop="static" show={session.wizardResponse.isVisible} onHide={this.onWizardCancel}>
            <div className="layout-modal-dialog" >
              <ModalHeader title={`Детали заявки №${typeof session.wizardResponse.ticketDetail !== 'undefined' ? session.wizardResponse.ticketDetail.id : ''}`} handlers={{ onClose: this.onWizardCancel }} />
              <hr />
              {session.wizardResponse.isMeasureFacility == true ?
                <WizardIncomingMeasureFacility
                  session={session}
                  actions={actions}
                  handlers={{
                    onSubmit: this.onWizardSubmit
                  }}
                />
                : (
                  session.wizardResponse.ticketDetail.behaviour === 'Group' ?
                    <WizardIncomingWellGroup
                      session={session}
                      actions={actions}
                      handlers={{
                        onSubmit: this.onWizardSubmit
                      }}
                    /> :
                    <WizardIncomingWell
                      session={session}
                      actions={actions}
                      handlers={{
                        onSubmit: this.onWizardSubmit
                      }}
                    />
                )
              }
            </div>
          </Modal>
        }
      </div>
    );
  }
}

OutcomingPage.propTypes = {
  session: PropTypes.shape({
    page: PropTypes.number.isRequired,
    dateBegin: PropTypes.object.isRequired,
    dateEnd: PropTypes.object.isRequired,
    departmentIDs: PropTypes.array.isRequired,
    positionRolename: PropTypes.string.isRequired,
    wizardRequest: PropTypes.object.isRequired,
    wizardResponse: PropTypes.object.isRequired,
    routes: PropTypes.array.isRequired
  }).isRequired,
  actions: PropTypes.object.isRequired
};