import PropTypes from 'prop-types';
import React from 'react';
import { NavLink, Switch, Redirect } from 'react-router-dom';

import { AdministrateDictionaryPage } from './administrate-dictionary-page';
import { RouteExtended } from './route-extended';

export class AdministrateDictionariesPage extends React.Component {
  render() {
    const { session, actions } = this.props;

    return (
      <div className="administrative-dictionaries-page administrative-page">
        <div className="clearfix">
          <ul className="nav navbar-nav table-type-nav">
            <li className="nav navbar-nav"><NavLink to="/administrate/dictionaries/repair-type-e">Типы ремонтов ЭЦН</NavLink></li>
            <li className="nav navbar-nav"><NavLink to="/administrate/dictionaries/repair-type-s">Типы ремонтов ШГН</NavLink></li>
            <li className="nav navbar-nav"><NavLink to="/administrate/dictionaries/puller-type">Типы съемников</NavLink></li>
          </ul>
        </div>
        <Switch>
          <RouteExtended path="/administrate/dictionaries/:dictionary" component={AdministrateDictionaryPage} {... { session, actions }} />
          <Redirect to="/administrate/dictionaries/repair-type-e" />
        </Switch>
      </div>
    );
  }
}

AdministrateDictionariesPage.propTypes = {
  session: PropTypes.shape({
    dictionaries: PropTypes.object.isRequired,
  }).isRequired,
  actions: PropTypes.object.isRequired
};