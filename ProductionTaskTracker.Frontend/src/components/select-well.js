import PropTypes from 'prop-types';
import React from 'react';

import classnames from 'classnames';

import debounce from 'debounce-promise';

import Select from 'react-select';

export class SelectWell extends React.Component {
  constructor(props) {
    super(props);
    
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);

    this.loadOptions = debounce(this.loadOptions.bind(this), 500);
  }

  onBlur() {
    const { input: { onBlur, value } } = this.props;
    return onBlur(value);
  }

  onChange(value) {
    const { input: { onChange }, handlers } = this.props;
    handlers && handlers.onChange && handlers.onChange(value == null ? null : value.value);
    return onChange(value == null ? null : value.value);
  }

  prepareOptions(options) {
    return options
      .map(x => x.shop).filter((value, index, self) => self.indexOf(value) === index) // По каждому цеху один раз
      .map(x => ({
        label: x, 
        options: options
          .filter((value) => value.shop === x)
          .map(x => ({
            ...x, 
            label: `№ ${x.code}, ${!x.cluster || (`куст ${x.cluster},`)} ${x.field}`,
            value: x.id
          }))
      }));
  }

  loadOptions(search) {
    if (!search) {
			return Promise.resolve({ options: [] });
    }

    return this.props.actions.actionDictionaryDepartmentsWellFilter(search, this.props.departmentIDs, 0, 30)
      .then(
        response => Promise.resolve({ options: this.prepareOptions(response.payload.data) }),
        () => {
          return Promise.resolve({ options: [] });
        }
      );
  }

  render() {
    const { input: { value, onFocus }, meta: { touched, error }, tabIndex } = this.props;
    return (
      <div className="select-container">
        <Select.Async
          name="select-well"
          className={classnames(touched && error ? 'error' : null)}
          placeholder="Введите номер скважины"
          value={value}
          loadOptions={this.loadOptions}
          onBlur={this.onBlur}
          onChange={this.onChange}
          onFocus={onFocus}
          tabIndex={tabIndex}
        />
      </div>
    );
  }
}

SelectWell.propTypes = {
  actions: PropTypes.shape({
    actionDictionaryDepartmentsWellFilter: PropTypes.func.isRequired
  }),
  departmentIDs: PropTypes.array.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func
  }),
  values: PropTypes.array,
  tabIndex: PropTypes.string
};