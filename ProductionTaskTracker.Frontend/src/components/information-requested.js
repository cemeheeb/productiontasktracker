import PropTypes from 'prop-types';
import React from 'react';

export class InformationRequested extends React.Component {
  render() {

    return (
        <div className="display-list">
          {this.props.parameters.map(parameter => (<div className="badge" key={parameter.key}>{parameter.label}</div>))}
        </div>
    );
  }
}

InformationRequested.propTypes = {
  parameters: PropTypes.array.isRequired
};
