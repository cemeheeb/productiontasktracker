import PropTypes from 'prop-types';
import React from 'react';
import { reduxForm, } from 'redux-form/immutable';

import { Wizard } from './wizard';
import { WizardFormOutcoming } from './wizard-form-outcoming';
import { WizardFormWellPicker } from './wizard-form-well-picker';
import { WizardFormPropertyEditorWell } from './wizard-form-property-editor-well';
import { WizardFormPreview } from './wizard-form-preview';
import { WizardStepForm } from './wizard-step-form';
import { validate } from './outcoming-wizard-validate-well';

import extractDepartmentsBranch from '../utilities/extract-departments-branch';

import { PROPERTY_LIST_OUTCOMING } from '../constants/configuration';

import { filterActual } from '../utilities/routes';

const WizardStep = reduxForm({
  form: 'wizard-request',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false,
  validate
})(WizardStepForm);

export class WizardOutcomingWell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      configuration: PROPERTY_LIST_OUTCOMING,
      departments: [],
      attachments: []
    };

    this.updateDepartments = this.updateDepartments.bind(this);
    this.actionAttachmentsUpload = this.actionAttachmentsUpload.bind(this);
  }

  updateDepartments(ticketType, statusTo, departmentIDs, routes, departments) {
    if (typeof ticketType !== 'undefined') {
      const filtered = filterActual(routes)
        .filter(x => x.ticket_class.indexOf(ticketType[0].toUpperCase()) >= 0 && departmentIDs.indexOf(x.department_id_from) > -1 && x.status_from == null && x.status_to == statusTo)
        .sort((a, b) => a.executor_id > b.executor_id ? 1 : (a.executor_id < b.executor_id ? -1 : 0))
        .map(x => x.executor_id);

      const departmentsWithoutChildrens = filtered.filter((x, index) => filtered.indexOf(x) == index)
        .filter(x => departments.filter(y => y.parent_id == x).length == 0)
        .map(x => departments.filter(y => y.id == x)[0]);

      const departmentsWithChildrens = filtered.filter((x, index) => filtered.indexOf(x) == index)
        .filter(x => departments.filter(y => y.parent_id == x).length > 0)
        .map(x => departments.filter(y => y.id == x)[0]);
      
      this.setState({
        departments: [...departmentsWithoutChildrens, ...departments.filter(x => departmentsWithChildrens.filter(y => y.id == x.parent_id).length > 0)]
      });
    }
  }

  actionAttachmentsUpload(attachments) {
    const { actions: { actionAttachmentsUpload, actionChangeForm } } = this.props;

    return actionAttachmentsUpload(attachments).then(x => {
      this.setState({attachments: x.payload}, () => {
        actionChangeForm('wizard-request', 'attachment_id', x.payload.length > 0 && x.payload[0] !== null ? x.payload[0].attachment_id : null);
      });
      return Promise.resolve(x);
    });
  }
  
  onPrevious() {
  }

  onNext() {
  }

  render() {
    const {
      configuration,
      attachments
    } = this.state;

    const {
      handlers: {
        onSubmit
      },
      actions,
      session
    } = this.props;

    const targetDepartmentID = typeof session.wizardRequest.inputData.ticket_type === 'undefined' ? 0 :
      filterActual(session.routes)
        .filter(x => x.ticket_class.indexOf(session.wizardRequest.inputData.ticket_type[0].toUpperCase()) >= 0)
        .filter(x => x.status_to == session.wizardRequest.inputData.status && session.departmentIDs.indexOf(x.department_id_from) > -1 )
        .map(x => x.department_id_to)[0];

    const stepCommit = {
      index: 0,
      handler: () => {
        actions.actionTicketRegistration(session.wizardRequest.inputData, targetDepartmentID).then(() => { onSubmit(); });
        return Promise.reject();
      },
      text: "Подтвердить"
    };

    const ticketDetail = Object.assign({}, session.wizardRequest.inputData);
    if (typeof ticketDetail.wells !== 'undefined' && typeof ticketDetail.wells[0] !== 'undefined') {
      ticketDetail.well_id = ticketDetail.wells[0].id;
    }

    const previewValues = Object.assign({}, configuration.propertyValues[ticketDetail.ticket_type]);
    if (typeof ticketDetail.ticket_type !== 'undefined') {
      if (typeof configuration.propertyValues[ticketDetail.ticket_type].switch !== 'undefined') {
        Object.keys(configuration.propertyValues[ticketDetail.ticket_type].switch).forEach(x => {
          if (typeof ticketDetail[x] !== 'undefined' && ticketDetail[x] != null) {
            const switchCaseProperties = configuration.propertyValues[ticketDetail.ticket_type].switch[x][ticketDetail[x]];
            if (typeof switchCaseProperties !== 'undefined' && switchCaseProperties !== null) {
              Object.keys(switchCaseProperties).forEach(property => {
                previewValues[property] = {};
              });
            }
          }
        });
      }

      previewValues['note'] = {};  
    }
    
    const stepPreview = {
      handler: () => Promise.resolve(),
      text: "Предпросмотр",
      component: <WizardFormPreview {... {
        propertyValues: previewValues,
        session,
        actions,
        ticketDetail,
        behaviour: "Single",
        isMeasureFacility: false
      }} />,
      next: [stepCommit]
    };

    const stepParameters = {
      handler: () => Promise.resolve(),
      text: "Параметры заявки",
      component: <WizardFormPropertyEditorWell {... {
        form: 'wizard-request',
        actions: { ...actions, actionAttachmentsUpload: this.actionAttachmentsUpload },
        session,
        inputData: session.wizardRequest.inputData,
        propertyValues: configuration.propertyValues[session.wizardRequest.inputData.ticket_type],
        pullerTypes: session.pullerTypes,
        isPullerTypesLoading: session.isPullerTypesLoading,
        attachments
      }} />,
      next: [stepPreview]
    };

    const stepTicketType = {
      text: "Тип заявки",
      component: <WizardFormOutcoming {... {
        actions,
        handlers: {
          updateDepartments: this.updateDepartments
        },
        session,
        departments: this.state.departments,
        inputData: session.wizardRequest.inputData
      }} />,
    };

    if (
      typeof session.wizardRequest.inputData.ticket_type !== 'undefined' &&
      session.wizardRequest.inputData.ticket_type !== null &&
      typeof session.wizardRequest.inputData.department_id !== 'undefined' &&
      session.wizardRequest.inputData.department_id !== null
    ) {
      if (['F', 'G'].indexOf(session.wizardRequest.inputData.ticket_type) < 0) {
        stepTicketType.next = [{
          index: 0,
          handler: () => Promise.resolve(),
          text: "Выбор объекта",
          component: <WizardFormWellPicker {... {
            session,
            actions,
            departmentIDs: extractDepartmentsBranch(session.wizardRequest.inputData.department_id, session.departments)
          }}
          />,
          next: stepParameters == null ? [] : [stepParameters]
        }];
      } else {
        stepTicketType.next = stepParameters == null ? [] : [stepParameters];
      }
    }

    return <Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[stepTicketType]} component={WizardStep} />;
  }
}

WizardOutcomingWell.propTypes = {
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    wizardRequest: PropTypes.shape({
      inputData: PropTypes.object.isRequired
    }).isRequired,
    departments: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    isFieldsLoading: PropTypes.bool.isRequired,
    clusters: PropTypes.array.isRequired,
    isClustersLoading: PropTypes.bool.isRequired,
    pullerTypes: PropTypes.array.isRequired,
    wizardHistory: PropTypes.array
  }),
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};
