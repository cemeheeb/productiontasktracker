import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';

import { PageHeader } from './page-header';

export class PumpPageHeader extends React.Component {
  constructor(props) {
    super(props);

    this.onImportClick = this.onImportClick.bind(this);
    this.onRefreshDataClick = this.onRefreshDataClick.bind(this);
  }
  
  componentDidMount() {
    this.onRefreshDataClick();
  }

  onImportClick() {
    const { handlers: { onImport }, enabled } = this.props;
    if (enabled) {
      onImport();
    }
  }

  onRefreshDataClick() {
    const { handlers: { onRefreshData }, enabled } = this.props;
    if (enabled) {
      onRefreshData();
    }
  }

  render() {
    const { enabled } = this.props;

    return (
      <PageHeader title="Насосы">
        <form role="form" className="page-filter-form form-inline">
          <div className="form-group">
            <div className="input-group">
              <button type="button" title="Импорт данных из ЭПУ" className="btn btn-primary" onClick={this.onImportClick}><i className="fa fa-sign-in" /></button>
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <button type="button" title="Обновить данные" className={classnames("btn", "btn-error", {"btn-disabled" : !enabled})} onClick={this.onRefreshDataClick}><i className="fa fa-refresh" /></button>
            </div>
          </div>
        </form>
      </PageHeader>
    );
  }
}

PumpPageHeader.propTypes = {
    handlers: PropTypes.shape({ 
      onImport: PropTypes.func.isRequired,
      onRefreshData: PropTypes.func.isRequired
    }).isRequired,
    enabled: PropTypes.bool.isRequired
};
