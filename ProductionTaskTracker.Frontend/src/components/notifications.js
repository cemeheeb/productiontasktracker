import React from 'react';
import PropTypes from 'prop-types';
import ReactHowler from 'react-howler';

import { API_ROOT } from '../constants/configuration';
import { Notification } from './notification';

export class Notifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      soundPlaying: false
    };

    this.onSoundEnd = this.onSoundEnd.bind(this);
    this.onCloseAllClick = this.onCloseAllClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.notifications !== 'undefined' && nextProps.notifications !== null) {
      this.setState({soundPlaying: nextProps.notifications.length > this.props.notifications.length});
    }
  }

  onSoundEnd() {
    this.setState({soundPlaying: false});
  }

  onCloseAllClick() {
    const { actions: { actionInformationTicketDetailVisitPush } } = this.props;
    actionInformationTicketDetailVisitPush(this.props.notifications.map(x => x.ticket_movement_id));
  }

  render() {
    const { notifications, notificationsFxToggled, onNotificationsFxToggleClick, onNotificationClick } = this.props;
    return (
      <div className="sidebar-panel">
        <div className="sidebar-title text-center">Уведомления</div>
        <button className="m-t-sm btn btn-warning btn-block btn-outline" onClick={onNotificationsFxToggleClick}>{ !notificationsFxToggled ? "Включить звук" : "Выключить звук"}</button>
        <button className="m-t-sm btn btn-primary btn-block btn-outline" onClick={onNotificationClick}>Скрыть</button>
        <button className="m-t-sm btn btn-info btn-block btn-outline" onClick={this.onCloseAllClick}>Закрыть все</button>
        <ul className="todo-list ui-sortable">
          {notifications.slice(0, 5).map(x => <Notification actions={this.props.actions} key={x.ticket_movement_id} data={x}/> )}
        </ul>
        <ReactHowler src={`${API_ROOT}/static/notification.mp3`} playing={this.state.soundPlaying && notificationsFxToggled} onEnd={this.onSoundEnd} loop={false}/>
      </div>
    );
  }
}

Notifications.propTypes = {
  actions: PropTypes.shape({
    actionInformationTicketDetailVisitPush: PropTypes.func.isRequired,
    actionWizardRequest: PropTypes.func.isRequired,
    actionWizardResponse: PropTypes.func.isRequired
  }).isRequired,
  notifications: PropTypes.array.isRequired,
  notificationsFxToggled: PropTypes.bool.isRequired,
  onNotificationClick: PropTypes.func.isRequired,
  onNotificationsFxToggleClick: PropTypes.func.isRequired
};
