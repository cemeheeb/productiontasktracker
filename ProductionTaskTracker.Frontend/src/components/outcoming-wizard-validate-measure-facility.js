import moment from 'moment';
import { PROPERTY_LIST_OUTCOMING } from '../constants/configuration';

const validateMoment = (values, field) => {
  const value = values.get(field);
  const valueMoment = moment(value, 'DD.MM.YYYY HH:mm');

  return values.has(field) && value !== null && valueMoment !== '01.01.0001 00:00' && valueMoment.isValid();
};

export const validate = values => {
  const errors = {};

  const requiredText = 'Поле обязательно для заполнения';

  if (!values.has('ticket_type') || values.get('ticket_type') == null) {
    errors.ticket_type = requiredText;
  } else {
    const ticketType = values.get('ticket_type');
    Object.keys(PROPERTY_LIST_OUTCOMING.propertyValues[ticketType]).forEach(x => {
      if (['note'].indexOf(x) >= 0) {
        return;
      }

      if (!values.has(x) || values.get(x) == null) {
        errors[x] = requiredText;
      }
    });
  }

  if (!validateMoment(values, 'time_completion')) {
    errors.time_completion = requiredText;
  }

  if (!values.has('department_id') || values.get('department_id') == null) {
    errors.department_id = requiredText;
  }

  if (!values.has('cluster_code') || values.get('cluster_code') == null) {
    errors.cluster_code = requiredText;
  }

  if (!values.has('field_code') || values.get('field_code') == null) {
    errors.field_code = requiredText;
  }

  return errors;
};