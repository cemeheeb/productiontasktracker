import PropTypes from 'prop-types';
import React from 'react';

import Tree, { TreeNode } from 'rc-tree';
import 'rc-tree/assets/index.css';

function toTreeData(data) {
  const map = {};
  const roots = [];
  
  data.forEach((node, index) => {
    map[node.id] = index;
    node.children = [];
  });

  data.forEach((node) => {
    if (node.parent_id !== null) {
      data[map[node.parent_id]].children.push(node);
    } else {
      roots.push(node);
    }
  });

  return roots;
}

function renderIcon(node) {
  return (typeof node.id === 'string' && node.id[0] === 'd') || node.id === null ? <i className="fa fa-industry" /> : <i className="fa fa-user" />;
}

function renderTreeData(treeData) {
  return treeData.map((node) => node.children && node.children.length ? <TreeNode key={node.id} title={node.title} icon={renderIcon(node)}>{renderTreeData(node.children)}</TreeNode> : <TreeNode key={node.id} title={node.title} icon={renderIcon(node)} />);
}

export class AdministrateDepartmentsTree extends React.Component {
  constructor(props) {
    super(props);
    this.onToggle = this.onToggle.bind(this);
  }

  onToggle(node) {
    this.props.handlers.onSelect(node.id);
  }

  render() {
    const { data, handlers: { onSelect } } = this.props;
    const treeData = toTreeData(data.map(x => ({ id: x.id, parent_id: x.parent_id, title: x.display_name })));
    return (
      <div className="administrative-departments-tree">
        <Tree onSelect={onSelect} selectedKeys={this.props.selectedNode ? [this.props.selectedNode.id.toString()] : []} showCheckedStrategy="SHOW_ALL" showLine>
          {renderTreeData(treeData)}
        </Tree>
      </div>
    );
  }
}

AdministrateDepartmentsTree.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onSelect: PropTypes.func.isRequired
  }).isRequired,
  selectedNode: PropTypes.object
};
