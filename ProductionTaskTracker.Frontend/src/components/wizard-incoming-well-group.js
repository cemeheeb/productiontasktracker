import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import { reduxForm, FieldArray } from 'redux-form/immutable';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import { Wizard } from './wizard';

import { WizardFormPropertyEditorWellGroup } from './wizard-form-property-editor-well-group';
import { WizardFormPreview } from './wizard-form-preview';

//import ticketStatusConvert from '../utilities/ticket-status-converter';
import ticketStatusConverterToVerb from '../utilities/ticket-status-converter-to-verb';

import { WizardStepForm } from './wizard-step-form';
import { validate } from './incoming-wizard-validate-well-group';
import { PROPERTY_LIST_INCOMING, PROPERTY_LIST_OUTCOMING } from '../constants/configuration';
import { PROGRESS_READY, COMPLETED } from '../constants/ticket-statuses';

import { filterActual } from '../utilities/routes';

const WizardStep = reduxForm({
  form: 'wizard-response',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false,
  validate
})(WizardStepForm);

export class WizardIncomingWellGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pumps: [],
      isPumpsLoading: false,
      attachments: []
    };

    this.actionAttachmentsUpload = this.actionAttachmentsUpload.bind(this);
    this.renderPropertyEditor = this.renderPropertyEditor.bind(this);
  }

  actionAttachmentsUpload(attachments, tag) {
    const { actions: { actionAttachmentsTicketDetailUpload, actionChangeForm } } = this.props;

    return actionAttachmentsTicketDetailUpload(attachments, this.props.session.wizardResponse.ticketDetail.id).then(x => {
      this.setState({ attachments: x.payload }, () =>
        actionChangeForm('wizard-response', `${tag}.attachment_id`, x.payload.length > 0 && x.payload[0] !== null ? x.payload[0].attachment_id : null)
      );
      return Promise.resolve(x);
    });
  }

  renderPropertyEditor({ fields, meta: { error }, propertyValues }) {
    const {
      attachments
    } = this.state;

    const {
      actions,
      session
    } = this.props;

    return (
      <div>
        <Tabs forceRenderTabPanel>
          <TabList> {fields.map((field, index) => <Tab key={field}>{session.wizardResponse.inputData.wells[index].code}</Tab>)} </TabList>
          {
            fields.map((field, index) =>
              <TabPanel key={field}>
                <WizardFormPropertyEditorWellGroup {... {
                  id: field,
                  parameter: typeof session.wizardResponse.inputData.parameters !== 'undefined' ? session.wizardResponse.inputData.parameters[index] : undefined,
                  actions: { ...actions, actionAttachmentsUpload: this.actionAttachmentsUpload },
                  session,
                  inputData: session.wizardResponse.inputData,
                  propertyValues: propertyValues[session.wizardResponse.inputData.wells[index].id.toString()],
                  pullerTypes: session.pullerTypes,
                  isPullerTypesLoading: session.isPullerTypesLoading,
                  attachments
                }} />
              </TabPanel>
            )
          }
        </Tabs>
        {error && <span className="error-box">{error}</span>}
      </div>
    );
  }

  onPrevious() {
  }

  onNext() {
  }


  
  render() {
    const {
      session: {
        departmentIDs,
        wizardResponse: {
          inputData,
          ticketDetail
        },
        routes
      },
      handlers: {
        onSubmit
      },
      actions
    } = this.props;

    if (ticketDetail == null)
      return null;
    
    const previewValues = {};

    let requestable = [];
    const outcomingConfiguration = PROPERTY_LIST_OUTCOMING.propertyValues[ticketDetail.ticket_type];
    if (typeof outcomingConfiguration.requested !== 'undefined' && typeof outcomingConfiguration.requested.options !== 'undefined') {
      requestable = outcomingConfiguration.requested.options.map(x => x.value);
    }

    // Исходящие
    if (ticketDetail.parameters !== 'undefined') {
      ticketDetail.parameters.forEach(parameter => {
        previewValues[parameter.well_id] = {};

        Object.keys(parameter).forEach(x => {
          switch (x) {
            case 'id':
            case 'ticket_id':
            case 'ticket_created':
            case 'membership_id':
            case 'department_id':
            case 'well_id':
            case 'mf_position':
            case 'field_code':
            case 'cluster':
            case 'status':
            case 'sl':
            case 'is_night':
            case 'ticket_type':
              break;
            default:
              previewValues[parameter.well_id][x] = {};
              break;
          }
        });

        if (typeof parameter.requested === "string") {
          parameter.requested.split(',').forEach(x => {
            previewValues[parameter.well_id][x] = {};
          });
        }

        // Входящие отображаем только для заявок в статусе после отработки
        if (ticketDetail.status > 5) {
          const incomingConfiguration = PROPERTY_LIST_INCOMING.propertyValues[parameter.ticket_type];
          if (typeof incomingConfiguration.switch !== 'undefined') {
            Object.keys(incomingConfiguration.switch).forEach(x => {
              const switchCaseProperties = incomingConfiguration.switch[x][parameter[x]];
              Object.keys(switchCaseProperties).forEach(property => {
                previewValues[parameter.well_id][property] = {};
              });
            });
          }

          delete previewValues[parameter.well_id]['requested'];
        }

        if (typeof outcomingConfiguration.switch !== 'undefined') {
          Object.keys(outcomingConfiguration.switch).forEach(x => {
            const switchCaseProperties = outcomingConfiguration.switch[x][parameter[x]];
            Object.keys(switchCaseProperties).forEach(property => {
              previewValues[parameter.well_id][property] = {};
            });
          });
        }

        delete previewValues[parameter.well_id]['note'];
        previewValues[parameter.well_id]['note'] = {};
      });
    }

    const {
      note, // eslint-disable-line no-unused-vars
      ...other
    } = inputData;

    const ticketDetailPreview = Object.assign({}, other, ticketDetail);
    const step = {
      index: 0,
      text: "Предпросмотр",
      component: <WizardFormPreview {... {
        propertyValues: previewValues,
        session: this.props.session,
        actions,
        behaviour: "Group",
        ticketDetail: ticketDetailPreview,
        isMeasureFacility: false
      }} />
    };

    let nextStep = step;

    const statusesNonFiltered = filterActual(routes, moment(ticketDetail.ticket_created))
      .filter(x => x.ticket_class.indexOf(ticketDetail.ticket_type[0].toUpperCase()) > -1)
      .filter(x => x.status_from == ticketDetail.status && departmentIDs.indexOf(x.department_id_from) > -1 && (x.department_id_to == ticketDetail.department_id || x.department_id_to == 2))
      .map(x => x.status_to);

    const statuses = new Set(statusesNonFiltered).toJSON();

    if (statuses.length > 0) {
      nextStep.next = statuses.map((x, i) => {
        const propertyValues = {};

        const text = ticketStatusConverterToVerb(x);
        switch (x) {
          case COMPLETED:
          case PROGRESS_READY: {
            // Исключаем из выборки параметры которые находятся в перечне запрашиваемых 
            // параметров по данному типу заявки, но которые не были запрошены при формировании запроса
            ticketDetail.parameters.forEach(parameter => {
              const propertyValuesTicketType = { ...PROPERTY_LIST_INCOMING.propertyValues[ticketDetail.ticket_type] };
              const propertyValuesToDelete = [];
              const requested = typeof parameter.requested !== 'undefined' && parameter.requested !== null ? parameter.requested.split(',') : [];

              requestable.forEach(value => {
                if (requested.indexOf(value) < 0) {
                  propertyValuesToDelete.push(value);
                }
              });

              propertyValuesToDelete.forEach(value => {
                delete propertyValuesTicketType[value.toString()];
              });

              propertyValues[parameter.well_id.toString()] = propertyValuesTicketType;
            });
            break;
          }
          default: {
            // Для всех остальных
            ticketDetail.parameters.forEach(parameter => {
              propertyValues[parameter.well_id.toString()] = { note: {} };
            });
          }
        }

        const output = {
          index: i,
          handler: () => Promise.resolve(),
          text,
          component: <FieldArray name="parameters" component={this.renderPropertyEditor} propertyValues={propertyValues} />
        };

        const ticketDetailPrepared = Object.assign(
          {},
          ticketDetail,
          inputData,
          { status: x }
        );

        output.next = [{
          index: 0,
          handler: () => {
            actions.actionIncomingTicketGroupMovement(ticketDetailPrepared).then(() => onSubmit());
            return Promise.reject();
          },
          text: 'Подтвердить'
        }];

        return output;
      });
    }

    return <Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[step]} component={WizardStep} />;
  }
}

WizardIncomingWellGroup.propTypes = {
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    positionRolename: PropTypes.string.isRequired,
    wizardResponse: PropTypes.shape({
      ticketDetail: PropTypes.object,
      inputData: PropTypes.object.isRequired
    }).isRequired,
    routes: PropTypes.array.isRequired,
    pullerTypes: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    departmentIDs: PropTypes.array.isRequired,
    wizardHistory: PropTypes.array
  }).isRequired,
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};