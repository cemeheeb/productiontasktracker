import PropTypes from 'prop-types';
import React from 'react';

import { Field, reduxForm } from 'redux-form/immutable';
import { FormFieldMinimal } from './form-field-minimal';

class Form extends React.Component {
  render() {
    const { title, handlers: { onSave, onCancel } } = this.props;

    return (
      <div>
        <h1>{title}</h1>
        <div className="form-group">
          <label>Название передприятия</label>
          <div className="input-group">
            <Field name="full_name"
              component={FormFieldMinimal}
              placeholder="Название передприятия"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Сокращенное название предприятия</label>
          <div className="input-group">
            <Field name="display_name"
              component={FormFieldMinimal}
              placeholder="Сокращенное название предприятия"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Код ЦДНГ</label>
          <div className="input-group">
            <Field name="shop_code"
              component={FormFieldMinimal}
              placeholder="Код"
            />
          </div>
        </div>
        <div className="input-group">
          <div className="pull-right">
            <button className="m-t-sm btn btn-primary btn-outline" onClick={onSave} >Сохранить</button>
            <button className="m-t-sm btn btn-danger btn-outline" onClick={onCancel}>Отмена</button>
          </div>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  title: PropTypes.string,
  handlers: PropTypes.object.isRequired
};

export const AdministrateDepartmentsForm = reduxForm({ form: 'administrate-department' })(Form);