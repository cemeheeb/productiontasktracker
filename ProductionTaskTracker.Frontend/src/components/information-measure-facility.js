import PropTypes from 'prop-types';
import React from 'react';

import { DisplayTable } from './display-table';
import { DisplayTableText } from './display-table-text';

export class InformationMeasureFacility extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentDisplayName: null,
      fieldDisplayName: null,
      position: null
    };
  }

  componentDidMount() {
    const { actions: { actionInformationField }, fieldCode } = this.props;
    actionInformationField(fieldCode).then(
      x => this.setState({ fieldDisplayName: x.payload.name })
    );
  }

  render() {
    const {
      cluster,
      position
    } = this.props;

    const {
      fieldDisplayName,
    } = this.state;

    return (
      <div className="form-group">
        <div className="col-md-6">
          <DisplayTable>
            <DisplayTableText key="field" label="Месторождение" value={fieldDisplayName}/>
            <DisplayTableText key="cluster" label="Куст" value={cluster}/>
            <DisplayTableText key="position" label="Позиция" value={position}/>
          </DisplayTable>
        </div>
      </div>
    );
  }
}

InformationMeasureFacility.propTypes = {
  fieldCode: PropTypes.string.isRequired,
  cluster: PropTypes.string.isRequired,
  position: PropTypes.number.isRequired,
  actions: PropTypes.shape({
    actionInformationField: PropTypes.func.isRequired
  }).isRequired
};