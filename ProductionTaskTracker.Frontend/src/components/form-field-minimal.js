import PropTypes from 'prop-types';
import React from 'react';
import CreatableSelect from 'react-select/lib/Creatable';
import classnames from 'classnames';

export class FormFieldMinimal extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onRemoveClick = this.onRemoveClick.bind(this);

    const {
      input: { value }
    } = props;

    this.state = {
      options: [],
      selected: value,
      isDisabled: true
    };
  }

  componentDidMount() {
    const { actions: { actionAutocompleteOptionFilter }, membershipID, input: { name } } = this.props;
    const self = this;
    actionAutocompleteOptionFilter(membershipID, this.convertName(name)).then((response) => self.setState({ options: response.payload.map(x => ({ value: x.text, label: x.text })) }));
  }

  componentWillReceiveProps(props) {
    const { input: { value } } = props;
    const { selected } = this.state;
    if (selected !== value) {
      this.setState({ selected: value });
    }
  }

  convertName(name) {
    return name.split('.').pop();
  }

  onChange(value) {
    const { input: { onChange }, valueKey } = this.props;
    const { options } = this.state;
    const selected = options.filter(x => x[valueKey || 'value'] == value[valueKey || 'value'])[0];

    if (value !== null) {
      this.setState({ selected });
      return onChange(value[valueKey || "value"]);
    }

    return onChange(null);
  }

  onCreate(value) {
    const { options } = this.state;
    const option = { label: value, value };
    this.setState({
      options: [...options, option],
      selected: options.length
    });
  }

  onRemoveClick(event) {
    const {
      membershipID,
      actions: { actionAutocompleteOptionRemove },
      input: {
        name,
        value
      }
    } = this.props;

    const self = this;
    if (value && value !== '' && confirm('Вы действительно хотите удалить этот вариант?')) {
      this.setState({
        isDisabled: true
      }, () => {
        actionAutocompleteOptionRemove(membershipID, this.convertName(name), value).then(() => {
          const { actions: { actionAutocompleteOptionFilter }, membershipID, input: { name } } = self.props;
          actionAutocompleteOptionFilter(membershipID, self.convertName(name)).then((response) => self.setState({ options: response.payload.map(x => ({ label: x.text, value: x.text })), isDisabled: false }));
        });
      });
    }

    event.preventDefault();
  }

  promptTextCreator() {
    return "Нажмите для добавления";
  }

  render() {
    const {
      className,
      placeholder,
      input: {
        value,
        onFocus
      },
      meta: {
        error
      }
    } = this.props;

    const {
      options,
      selected
    } = this.state;

    const props = {
      ...this.props,
    };

    if (typeof selected !== 'undefined' && selected !== null && options.length > 0 && options.length < selected) {
      props.options = [
        ...options,
        { value: options[selected], label: options[selected] }
      ];
    } else {
      props.options = options;
    }

    return (
      <div className="select-container-inline">
        <CreatableSelect
          {...props}
          className={classnames(className, error ? 'error' : null)}
          onChange={this.onChange}
          onFocus={onFocus}
          value={value}
          placeholder={placeholder}
          promptTextCreator={this.promptTextCreator}
          styles={{ container: styles => ({ ...styles, float:'left', width: '255px' }) }}
          isClearable
        />
        <button className="btn btn-danger" onClick={this.onRemoveClick}><i className={classnames("fa", "fa-trash")} /></button>
      </div>
    );
  }
}

FormFieldMinimal.propTypes = {
  form: PropTypes.string,
  actions: PropTypes.object.isRequired,
  membershipID: PropTypes.string.isRequired,
  input: PropTypes.object,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  meta: PropTypes.object,
  tabIndex: PropTypes.string,
  labelKey: PropTypes.string,
  valueKey: PropTypes.string
};