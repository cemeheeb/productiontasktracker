import PropTypes from 'prop-types';
import React from 'react';

export class DisplayTable extends React.Component {
  render() {
    return (
      <table className="table display-table">
        <tbody>
          {this.props.children}
        </tbody>
      </table>
    );
  }
}

DisplayTable.propTypes = {
  children: PropTypes.node
};
