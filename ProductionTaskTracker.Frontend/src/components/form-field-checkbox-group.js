import PropTypes from 'prop-types';
import React from 'react';

export class FormFieldCheckboxGroup extends React.Component {
  render() {
    return (
      <div className="table-display-table-field">
        { 
          this.props.options.map((option, index) => (
          <div className="checkbox" key={index}>
            <label>
              <input type="checkbox"
                name={`${name}[${index}]`}
                value={option.value}
                checked={this.props.input.value.split(',').indexOf(option.value) !== -1}
                onChange={event => {
                  const newValue = this.props.input.value ? this.props.input.value.split(',') : [];
                  if(event.target.checked) {
                    newValue.push(option.value);
                  } else {
                    newValue.splice(newValue.indexOf(option.value), 1);
                  }
                  return this.props.input.onChange(newValue.join());
                }}/>
              {option.label}
            </label>
          </div>))
        }
      </div>
    );
  }
}

FormFieldCheckboxGroup.propTypes = {
  options: PropTypes.array.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object,
  placeholder: PropTypes.string
};
