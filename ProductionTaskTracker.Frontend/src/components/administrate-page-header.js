import React from 'react';
import { NavLink } from 'react-router-dom';

import { PageHeader } from './page-header';

export class AdministratePageHeader extends React.Component {
  constructor(props) {
    super(props);

    this.onTabClick = this.onTabClick.bind(this);
  }

  onTabClick() {
  }

  render() {
    return (
      <PageHeader title="Администрирование">
        <form role="form" className="page-filter-form form-inline">
          <div className="form-group">
            <div className="input-group">
              <ul className="nav navbar-nav table-type-nav">
                <li className="nav navbar-nav"><NavLink to="/administrate/departments">Подразделения</NavLink></li>
                <li className="nav navbar-nav"><NavLink to="/administrate/memberships">Пользователи</NavLink></li>
                <li className="nav navbar-nav"><NavLink to="/administrate/dictionaries">Справочники</NavLink></li>          
              </ul>
            </div>
          </div>
        </form>
      </PageHeader>
    );
  }
}