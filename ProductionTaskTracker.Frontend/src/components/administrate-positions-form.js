import PropTypes from 'prop-types';
import React from 'react';

import { Field, reduxForm } from 'redux-form/immutable';
import { FormFieldMinimal } from './form-field-minimal';
import { SelectRedux } from './select-redux';

const rolenames = [
  { label: 'Руководитель ЦИО', value: 'director_cio' },
  { label: 'Заместитель руководителя ЦИО', value: 'subdirector_cio' },
  { label: 'Диспетчер ЦИО', value: 'dispatcher_cio' },
  { label: 'Специалист ЦИО', value: 'specialist_cio' },
  { label: 'Ответственный специалист группы ЦИО', value: 'specialist_dispatcher_cio' },
  { label: 'Начальник смены ЦИО', value: 'superviser_cio' },
  { label: 'Руководитель ЦДНГ', value: 'director_cdng' },
  { label: 'Лин.руководитель ЦДНГ', value: 'manager_cdng' },
  { label: 'Диспетчер ЦДНГ', value: 'dispatcher_cdng' },
  { label: 'Исполнитель ЦДНГ', value: 'specialist_cdng' }
];

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { title } = this.props;

    return (
      <div>
        <h1>{title}</h1>
        <div className="form-group">
          <label>Название должности</label>
          <div className="input-group">
            <Field name="name"
              component={FormFieldMinimal}
              placeholder="Название передприятия"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Роль</label>
          <div className="input-group">
            <Field name="position_role_name"
              component={SelectRedux}
              options={rolenames}
              isSearchable={false}
              placeholder="Роль"
            />
          </div>
        </div>
        <div className="input-group">
          <div className="administrative-toolbar pull-right">
            <button className="m-t-sm btn btn-primary btn-outline" onClick={this.props.handlers.onSave} >Сохранить</button>
            <button className="m-t-sm btn btn-danger btn-outline" onClick={this.props.handlers.onCancel}>Отмена</button>
          </div>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  title: PropTypes.string,
  handlers: PropTypes.object.isRequired
};

export const AdministratePositionsForm = reduxForm({ form: 'administrate-position' })(Form);