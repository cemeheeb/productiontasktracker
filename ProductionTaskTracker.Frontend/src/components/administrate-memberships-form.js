import PropTypes from 'prop-types';
import React from 'react';
import { reduxForm, Field } from 'redux-form/immutable';

import { FormFieldMinimal } from './form-field-minimal';

class Form extends React.PureComponent {
  render() {
    const { title, isNew } = this.props;

    return (
      <div>
        <h1>{title}</h1>
        <div className="form-group">
          <label>Логин</label>
          <div className="input-group">
            <Field name="username" readOnly={!isNew}
              component={FormFieldMinimal}
              placeholder="Логин"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Имя</label>
          <div className="input-group">
            <Field name="firstname"
              component={FormFieldMinimal}
              placeholder="Имя"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Фамилия</label>
          <div className="input-group">
            <Field name="lastname"
              component={FormFieldMinimal}
              placeholder="Фамилия"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Отчество</label>
          <div className="input-group">
            <Field name="patronym"
              component={FormFieldMinimal}
              placeholder="Отчество"
            />
          </div>
        </div>
        <div className="form-group">
          <label>Локальный пользователь</label>
          <div className="input-group">
            <Field name="is_local" type="checkbox"
              component={FormFieldMinimal}
            />
          </div>
        </div>
        <div className="form-group">
          <label>Административная роль</label>
          <div className="input-group">
            <Field name="rolename"
              component={FormFieldMinimal}
              placeholder="Роль"
              readOnly
            />
          </div>
        </div>
        <div className="input-group">
          <div className="pull-right">
            <button className="m-t-sm btn btn-primary btn-outline" onClick={this.props.handlers.onSave} >Сохранить</button>
            <button className="m-t-sm btn btn-danger btn-outline" onClick={this.props.handlers.onCancel}>Отмена</button>
          </div>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  title: PropTypes.string,
  handlers: PropTypes.object.isRequired,
  isNew: PropTypes.bool.isRequired
};

export const AdministrateMembershipsForm = reduxForm({
  form: 'administrate-membership'
})(Form);