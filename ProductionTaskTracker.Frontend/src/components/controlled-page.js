import PropTypes from 'prop-types';
import React from 'react';
import Modal from 'react-overlays/lib/Modal';

import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { ControlledPageHeader } from './controlled-page-header';
import { ControlledTable } from './controlled-table';
import { WizardControlled } from './wizard-controlled';

import { ModalHeader } from './modal-header';

export class ControlledPage extends React.Component {
  constructor(props) {
    super(props);

    this.onRefreshData = this.onRefreshData.bind(this);

    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);

    this.onAdd = this.onAdd.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onRemove = this.onRemove.bind(this);

    this.onWizardCancel = this.onWizardCancel.bind(this);
    this.onWizardSubmit = this.onWizardSubmit.bind(this);

    this.invalidateData = this.invalidateData.bind(this);

    this.state = {
      data: [],
      index: 1,
      size: 30,
      sizeTotal: 0,
      isBusy: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings.controlledFilter !== this.props.settings.controlledFilter) {
      this.invalidateData(nextProps.settings.controlledFilter);
    }
  }

  onRefreshData() {
    this.setState({index: 1, isBusy: true}, () => {
      this.invalidateData(this.props.settings.controlledFilter);
    });
  }

  onPageChange(index, size) {
    this.setState({ index, size, isBusy: true }, () => {
      this.invalidateData(this.props.settings.controlledFilter);
    });
  }

  onSizePerPageList(size) {
    this.setState({ size }, () => {
      this.invalidateData(this.props.settings.controlledFilter);
    });
  }

  onAdd() {
    const { actionWizardControlled } = this.props.actions;
    this.setState({ mode: "add" }, () => {
      actionWizardControlled();
    });
  }

  onEdit(row) {
    const { actionInitializeForm, actionWizardControlled } = this.props.actions;
    this.setState({ mode: "edit" }, () => {
      actionInitializeForm('wizard-controlled', {
        id: row.id,
        ticket_type: row.ticket_type,
        well_id: row.well_id,
        department_id: row.department_id,
        repair_type: row.repair_type,
        event_type: row.event_type,
        pump_id: row.pump_id,
        pump_depth: row.pump_depth
      }),
      actionWizardControlled();
    });
  }
 
  onRemove(row) {
    const { actionControlledDelete } = this.props.actions;
    actionControlledDelete(row.id).then(() => {
      this.onRefreshData();
    });
  }

  onWizardCancel() {
    const { actions: { actionWizardReset, actionResetForm } } = this.props;

    actionWizardReset();
    actionResetForm("wizard-controlled");
  }

  onWizardSubmit() {
    this.onRefreshData();
    this.onWizardCancel();
  }  
  
  invalidateData(filter) {
    this.props.actions.actionControlledFilter(filter, this.state.index, this.state.size)
    .then(response => {
      this.setState({ isBusy: false });

      if (response.error) {
        return;
      }

      const { data, size_total } = response.payload;
      this.setState({ data, sizeTotal: size_total });
    });
  }

  render() {
    const { data, index, size, sizeTotal, isBusy } = this.state;

    const {
      session,
      actions
    } = this.props;

    return (
      <div>
        <ControlledPageHeader 
          handlers={{ 
            onRefreshData: this.onRefreshData,
            onAdd: this.onAdd,
          }}
          enabled={!isBusy}
        />
        <div className="loading-bar-wrapper"><LoadingBar className="loading-bar"/></div>
        <div className="container-fluid">
          <div className="wrapper wrapper-content">
            <ControlledTable {...{
              data,
              handlers: {
                onPageChange: this.onPageChange,
                onSizePerPageList: this.onSizePerPageList,
                onAdd: this.onAdd,
                onEdit: this.onEdit,
                onRemove: this.onRemove
              },
              index,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
              size,
              sizeTotal
            }}/>
          </div>
        </div>
        {
          this.state.mode == "add" &&
          (
            <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" backdrop="static" show={session.wizardControlled.isVisible} onHide={this.onWizardCancel}>
              <div className="layout-modal-dialog" >
                <ModalHeader title="Постановка скважины на ВНР" handlers={{onClose: this.onWizardCancel}} />
                <hr /> 
                <WizardControlled
                  mode="add"
                  session={session}
                  actions={actions}
                  handlers={{
                    onSubmit: this.onWizardSubmit
                  }}
                />
              </div>
            </Modal>
          )
        }
        {
          this.state.mode == "edit" &&
          (
            <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" backdrop="static" show={session.wizardControlled.isVisible} onHide={this.onWizardCancel}>
              <div className="layout-modal-dialog" >
                <ModalHeader title="Редактирование записи" handlers={{onClose: this.onWizardCancel}} />
                <hr />
                <WizardControlled
                  mode="edit"
                  session={session}
                  actions={actions}
                  handlers={{
                    onSubmit: this.onWizardSubmit
                  }}
                />
              </div>
            </Modal>
          )
        }
      </div>
    );
  }
}

ControlledPage.propTypes = {
  settings: PropTypes.shape({
    controlledFilter: PropTypes.string.isRequired
  }).isRequired,
  session: PropTypes.shape({
    positionRolename: PropTypes.string.isRequired,
    wizardControlled: PropTypes.shape({
      isVisible:PropTypes.bool.isRequired
    }).isRequired,
    routes: PropTypes.array.isRequired
  }).isRequired,
  actions: PropTypes.object.isRequired
};