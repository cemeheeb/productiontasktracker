import PropTypes from 'prop-types';
import React from 'react';
import { reduxForm } from 'redux-form/immutable';

import { Wizard } from './wizard';

import { WizardFormWell } from './wizard-form-well';
import { WizardStepForm } from './wizard-step-form';

const WizardStep = reduxForm({ 
  form: 'wizard-well',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: false
})(WizardStepForm);

export class WizardWell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onPrevious() {
  }

  onNext() {
  }

  render() {
    const {
      handlers: {
        onSubmit
      },
      actions,
      session
    } = this.props;

    const step = {
      index: 0,
      text: "Тип заявки",
      component: <WizardFormWell {... {
        actions,
        session,
        inputData: session.wizardWell.inputData
      }}/>
    };

    step.next = [{
      handler: () => {
        actions.actionWellCreate(session.wizardWell.inputData).then(() => { onSubmit(); });
        return Promise.reject();
      },
      text: "Подтвердить"
    }];

    return <Wizard isLocked={this.props.session.requestCount > 0} actions={this.props.actions} history={this.props.session.wizardHistory} steps={[step]} component={WizardStep}/>;
  }
}

WizardWell.propTypes = {
  session: PropTypes.shape({
    requestCount: PropTypes.number.isRequired,
    wizardWell: PropTypes.shape({
      inputData: PropTypes.object.isRequired
    }).isRequired,
    wizardHistory: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    history: PropTypes.array
  }),
  actions: PropTypes.object.isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired
  }).isRequired
};
