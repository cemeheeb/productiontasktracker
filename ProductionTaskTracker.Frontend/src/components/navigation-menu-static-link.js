import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { API_ROOT } from '../constants/configuration';

export class NavigationMenuStaticLink extends React.PureComponent {
  render() {
    const { children, to } = this.props;

    return (
        <li className={classnames("nav", "navbar-nav", this.props.className)}>
            <a href={`${API_ROOT}${to}`} target="_blank">{children}</a>
        </li>
    );
  }
}

NavigationMenuStaticLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.node
};


