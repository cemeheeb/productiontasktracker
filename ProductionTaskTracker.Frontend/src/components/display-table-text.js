import PropTypes from 'prop-types';
import React from 'react';

export class DisplayTableText extends React.Component {
  render() {
    const { label, value } = this.props;
    return (
      <tr>
          <td className="text-left">{label}:</td>
          <td className="text-right">{typeof value !== 'undefined' ? value : null}</td>
      </tr>
    );
  }
}

DisplayTableText.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array
  ])
};
