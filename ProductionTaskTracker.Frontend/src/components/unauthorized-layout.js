import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { NavigationBrand } from './navigation-brand';
import { NavigationMenu } from './navigation-menu';
import { NavigationMenuLink } from './navigation-menu-link';
import { NavigationMenuStaticLink } from './navigation-menu-static-link';

import { LoginPage } from './login-page';
import { HelpPage } from './help-page';

export class UnauthorizedLayout extends React.Component {
  render() {
    return (
      <div className="unauthorized-layout col-lg-12">
        <div className="row border-bottom">
          <nav className="navbar navbar-static-top">
            <NavigationBrand />
            <div className="navbar-collapse collapse" id="navbar">
              <NavigationMenu>
                <NavigationMenuLink to="/login">Вход</NavigationMenuLink>
              </NavigationMenu>
              <NavigationMenu className="navbar-right">
                <NavigationMenuStaticLink to="/static/Руководство%20пользователя%20СУПЗ.pdf">?</NavigationMenuStaticLink>
              </NavigationMenu>
            </div>
          </nav>
        </div>
        <div className="row"><LoadingBar className = "loading-bar"/></div>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <Route path="/help" component={HelpPage} />
          <Redirect to="/login" />
        </Switch>
      </div>
    );
  }
}

UnauthorizedLayout.propTypes = {
  children: PropTypes.node
};