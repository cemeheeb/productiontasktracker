import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { DateRangePicker } from 'react-dates';
import Select from 'react-select';
import classnames from 'classnames';
import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar';

import { PageHeader } from './page-header';
import DateRangePhrases from '../constants/date-range-phrases';
import { TicketTypes } from '../constants/ticket-types';
import { REPORT_TYPE_A, REPORT_TYPE_B } from '../constants/report-types';
import ticketTypeConverter from '../utilities/ticket-type-converter';
import slConverter from '../utilities/sl-converter';
import { filterActual } from '../utilities/routes';

export class ReportPage extends React.Component {
  constructor(props) {
    super(props);
    
    const filtered = filterActual(props.session.routes)
        .filter(x => x.ticket_class.indexOf(x.department_id_from == props.session.departmentIDs && x.status_from == null))
        .map(x => x.executor_id);

    const departments = filtered.filter((x, index) => filtered.indexOf(x) == index)
        .map(x => this.props.session.departments.filter(y => y.id == x)[0])
        .sort((a, b) => a.id > b.id ? 1 : (a.id < b.id ? -1 : 0))
        .map(x => ({ label: x.display_name, value: x.id }));

    const ticketTypes = TicketTypes.map(x => ({ label: ticketTypeConverter(x), value: x }));

    const month = (new Date().getMonth().valueOf() + 1).toString();
    this.state = {
      dateBegin: moment(`01 ${month.length == 1 ? '0' + month : month} ${new Date().getFullYear().valueOf()}`, 'DD MM YYYY'),
      dateEnd: moment(new Date()),
      reportTypes: [
        { label: 'Тип 1', value: REPORT_TYPE_A },
        { label: 'Тип 2', value: REPORT_TYPE_B },
      ],
      reportTypeSelected: REPORT_TYPE_A,
      slList: [
        { label: slConverter(0), value: 0 },
        { label: slConverter(1), value: 1 },
        { label: slConverter(2), value: 2 }
      ],
      slSelected: null,
      showExpired: false,
      departments,
      departmentSelected: [],
      ticketTypes,
      ticketTypeSelected: [],
      wells: [],
      wellSelected: [],
      focusedInput: null,
      isBusy: false
    };

    this.onDatesChange = this.onDatesChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.onReportTypeChange = this.onReportTypeChange.bind(this);
    this.onDepartmentChange = this.onDepartmentChange.bind(this);
    this.onTicketTypeChange = this.onTicketTypeChange.bind(this);
    this.onShowExpiredChange = this.onShowExpiredChange.bind(this);
    this.onSlChange = this.onSlChange.bind(this);
    this.onWellChange = this.onWellChange.bind(this);
    this.onWellInputChange = this.onWellInputChange.bind(this);

    this.onReportClick = this.onReportClick.bind(this);
  }

  onDatesChange({ startDate, endDate }) {
    this.setState({ dateBegin: startDate, dateEnd: endDate });
  }

  onFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  onReportTypeChange(option) {
    this.setState({ reportTypeSelected: option.value });
  }

  onDepartmentChange(departmentSelected) {
    this.setState({ departmentSelected, wells: [], wellSelected: [] });
  }

  onWellChange(wellSelected) {
    this.setState({
			wellSelected,
    });
  }

  onWellInputChange(inputValue) {
    if (!inputValue) {
			this.setState({ wells: [] });
    }
    
    this.props.actions.actionDictionaryDepartmentsWellFilter(inputValue, this.state.departmentSelected.map(x => x.value), 0, 30)
      .then(
        response => {
          this.setState({ wells: this.wellPrepareOptions(response.payload.data) });
        }
      ); 
  }

  onTicketTypeChange(ticketTypeSelected) {
    this.setState({
      ticketTypeSelected
    });
  }

  onSlChange(slSelected) {
    this.setState({
      slSelected
    });
  }

  onShowExpiredChange(event) {
    this.setState({ showExpired: event.target.checked });
  }

  onReportClick() {
    const { isBusy } = this.state;
    const { actions: { actionReportMakeA, actionReportMakeB } } = this.props;
    const { departmentSelected, wellSelected, ticketTypeSelected, slSelected, showExpired } = this.state;

    if (!isBusy) {
      const { dateBegin, dateEnd, reportTypeSelected } = this.state;
      
      switch (reportTypeSelected) {
        case REPORT_TYPE_A: {
          actionReportMakeA(
            dateBegin,
            dateEnd,
            departmentSelected.map(x => x.value),
            wellSelected.map(x => x.value),
            ticketTypeSelected.map(x => x.value),
            slSelected === null ? null : slSelected.value,
            showExpired
          );
          break;
        }
        case REPORT_TYPE_B: {
          actionReportMakeB(dateBegin, dateEnd);
          break;
        }
      }
    }
  }

  wellPrepareOptions(options) {
    return options
      .map(x => x.shop).filter((value, index, self) => self.indexOf(value) === index).sort((a, b) => a > b ? 1 : (a < b ? -1 : 0)) // По каждому цеху один раз
      .map(x => ({
        label: x, 
        options: options
          .filter((value) => value.shop === x)
          .map(x => ({
            ...x, 
            label: `№ ${x.code}, ${!x.cluster || (`куст ${x.cluster},`)} ${x.field}`,
            value: x.id
          }))
      }));
  }

  render() {
    const { 
      dateBegin,
      dateEnd,
      reportTypes,
      reportTypeSelected,
      departments,
      departmentSelected,
      wells,
      wellSelected,
      ticketTypes,
      ticketTypeSelected,
      slList,
      slSelected,
      focusedInput,
      isBusy
    } = this.state;

    return (
      <div>
        <PageHeader title="Отчёты"/>
        <div  className="wrapper"><LoadingBar className = "loading-bar"/></div>
        <div className = "container-fluid">
          <div className = "wrapper wrapper-content weapper-report">
            <form role="form">
              <div className="col-md-3">
                <div className="form-group">
                  <label>За период</label>
                  <div className="input-daterange">
                    <DateRangePicker
                      displayFormat="DD.MM.YYYY"
                      startDate={dateBegin}
                      endDate={dateEnd}
                      onDatesChange={this.onDatesChange}
                      focusedInput={focusedInput}
                      onFocusChange={this.onFocusChange}
                      isOutsideRange={() => false}
                      phrases={DateRangePhrases}
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="form-group hidden">
                  <label>По типу отчёта</label>
                  <Select
                    className="report"
                    options={reportTypes}
                    value={reportTypeSelected}
                    onChange={this.onReportTypeChange}
                    placeholder="Выберите тип отчёта"
                    isClearable={false}
                  />
                </div>
                <div className="form-group">
                  <label>По ЦДНГ</label>
                  <Select
                    className="report"
                    options={departments}
                    value={departmentSelected}
                    onChange={this.onDepartmentChange}
                    placeholder="Выберите ЦДНГ"
                    isMulti
                  />
                </div>
                <div className="form-group">
                  <label>По типам заявок</label>
                  <Select
                    className="report"
                    options={ticketTypes}
                    value={ticketTypeSelected}
                    onChange={this.onTicketTypeChange}
                    placeholder="Выберите тип заявки"
                    isMulti
                  />
                </div>
              </div>
              <div className="col-md-3">
                <div className="form-group">
                  <label>По скважинам</label>
                  <Select 
                    className="report"
                    onInputChange={this.onWellInputChange}
                    onChange={this.onWellChange}
                    options={wells}
                    value={wellSelected}
                    placeholder="Выберите скважину"
                    isMulti
                  />
                </div>
                <div className="form-group">
                  <label>По уровню срочности</label>
                  <Select
                    className="report"
                    options={slList}
                    value={slSelected}
                    onChange={this.onSlChange}
                    placeholder="Выберите уровень срочности"
                    isClearable
                  />
                </div>
                <div className="form-group">
                  <label>Только истекшие</label>
                  <input type="checkbox" defaultChecked={this.state.showExpired} onChange={this.onShowExpiredChange} />
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <button type="button" className={classnames("btn btn-primary pull-right", {"disabled" : isBusy})} onClick={this.onReportClick}>Сформировать</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

ReportPage.propTypes = {
  session: PropTypes.shape({
    departmentIDs: PropTypes.array.isRequired,
    routes: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    wells: PropTypes.array.isRequired
  }).isRequired,
  actions: PropTypes.shape({
    actionDictionaryDepartmentsWellFilter: PropTypes.func.isRequired,
    actionReportMakeA: PropTypes.func.isRequired,
    actionReportMakeB: PropTypes.func.isRequired
  }).isRequired,
};