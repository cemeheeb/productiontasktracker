import PropTypes from 'prop-types';
import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import dateFormatter from '../utilities/date-formatter';

function commandCellFormatter(cell, row) { // eslint-disable-line no-unused-vars
  return `<i class='glyphicon glyphicon-usd'></i>`;
}


export class HistoryTable extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { index, size, sizeTotal, data } = this.props;
    return (nextProps.index !== index || nextProps.size !== size || nextProps.sizeTotal !== sizeTotal || nextProps.data !== data);
  }

  render() {
    const { data } = this.props;
    return (
      <div>
        <BootstrapTable hover condensed remote pagination
          data={data}
          options={{ 
            expandRowBgColor: 'rgb(242, 255, 163)',
            noDataText: 'Нет данных',
            paginationPosition: 'top'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
          selectRow={{
            mode: 'radio',
            clickToSelect: true,
            className: 'table-row-selected',
            hideSelectColumn: true
          }}
        >
          <TableHeaderColumn width="100" dataField="id" isKey={true}>Код</TableHeaderColumn>
          <TableHeaderColumn width="150" dataField="date" dataFormat={dateFormatter} >Дата</TableHeaderColumn>
          <TableHeaderColumn width="125" dataField="accepted" >Принята в</TableHeaderColumn>
          <TableHeaderColumn width="125" dataField="completed" >Выполнена в</TableHeaderColumn>
          <TableHeaderColumn width="175" dataField="executor">Исполнитель</TableHeaderColumn>
          <TableHeaderColumn dataField="actions" tdStyle={{ whiteSpace: 'normal' }}>Принятые меры</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

HistoryTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};