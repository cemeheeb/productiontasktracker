import PropTypes from 'prop-types';
import React from 'react';

import classnames from 'classnames';

export class FormFieldNumeric extends React.Component {
  render() {
    const { input, className, placeholder, readOnly, tabIndex, meta: { touched, error, warning } } = this.props;

    return (
      <div className="table-display-table-field">
        <input className={classnames(className, 'form-control', error ? 'error' : null)} {...input} placeholder={placeholder} type="number" readOnly={readOnly} tabIndex={tabIndex} />
        {touched && (warning && <span>{warning}</span>)}
      </div>
    );
  }
}

FormFieldNumeric.propTypes = {
  input: PropTypes.object,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  meta: PropTypes.object,
  tabIndex: PropTypes.string,
  readOnly: PropTypes.bool
};
