import PropTypes from 'prop-types';
import React from 'react';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export class ControlledTable extends React.Component {
  constructor(props) {
    super(props);

    this.onAdd = this.onAdd.bind(this);
    this.onEdit = this.onEdit.bind(this);

    this.buttonFormatter = this.buttonFormatter.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    const { index, size, sizeTotal, data } = this.props;
    return (nextProps.index !== index || nextProps.size !== size || nextProps.sizeTotal !== sizeTotal || nextProps.data !== data);
  }

  onAdd() {
    this.props.handlers.onAdd();
  }

  onEdit(row) {
    this.props.handlers.onEdit(row);
  }

  onRemove(row) {
    if (!confirm('Вы хотите удалить запись?')) {
      return;
    }

    this.props.handlers.onRemove(row);
  }

  buttonFormatter(cell, row) {
    return (
      <div>
        <button type="button" className="btn btn-warning" title="Просмотреть" onClick={() => this.onEdit(row)} ><i className="fa fa-bars" /></button>
        <button type="button" className="btn btn-danger" title="Удалить" onClick={() => this.onRemove(row)} ><i className="fa fa-times" /></button>
      </div>
    );
  }

  ticketTypeFormatter(cell) {
    return (
      cell == 'D1' ? 'ВНР' : 'Подконтрольные'
    );
  }

  render() {
    const { index, size, sizeTotal, handlers: { onPageChange, onSizePerPageList }, data } = this.props;

    return (
      <div>
        <BootstrapTable hover condensed remote pagination
          data={data}
          fetchInfo={{ dataTotalSize: sizeTotal }}
          trClassName={this.trClassFormat}
          options={{ 
            page: index,
            sizePerPage: size,
            onPageChange,
            onSizePerPageList,
            expandRowBgColor: 'rgb(242, 255, 163)',
            onRowDoubleClick: this.onEdit,
            noDataText: 'Нет данных',
            paginationPosition: 'top'
          }}
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
        >
          <TableHeaderColumn
            width="82"
            dataFormat={this.buttonFormatter} 
          />

          <TableHeaderColumn 
            width="100"
            dataField="id"
            dataAlign="right"
            isKey={true}
          >Код</TableHeaderColumn>

          <TableHeaderColumn
            width="150"
            dataField="field"
          >Месторождение</TableHeaderColumn>

          <TableHeaderColumn
            width="100"
            dataField="cluster"
          >Куст</TableHeaderColumn>

          <TableHeaderColumn 
            width="150"
            dataField="well"
          >Скважина</TableHeaderColumn>

          <TableHeaderColumn
            width="200"
            dataField="repair_type"
          >Тип ремонта</TableHeaderColumn>

          <TableHeaderColumn
            width="200"
            dataField=""
          >Вид мероприятия</TableHeaderColumn>
          
          <TableHeaderColumn
            dataField=""
          >Параметры</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

ControlledTable.propTypes = {
  data: PropTypes.array.isRequired,
  handlers: PropTypes.shape({
    onPageChange: PropTypes.func.isRequired,
    onSizePerPageList: PropTypes.func.isRequired,
    onAdd: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
  }).isRequired,
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  sizeTotal: PropTypes.number.isRequired
};
