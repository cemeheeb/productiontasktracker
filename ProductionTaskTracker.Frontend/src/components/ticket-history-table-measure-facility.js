import PropTypes from 'prop-types';
import React from 'react';
import Modal from 'react-overlays/lib/Modal';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';

import { ModalHeader } from './modal-header';
import { TicketDetailVisitTable } from './ticket-detail-visit-table';
import { InformationTicketDetailMeasureFacility } from './information-ticket-detail-measure-facility';

export class TicketHistoryTableMeasureFacility extends React.Component {
  constructor(props) {
    super(props);

    this.operationFormatter = this.operationFormatter.bind(this);

    this.onDetailModal = this.onDetailModal.bind(this);
    this.onDetailModalCancel = this.onDetailModalCancel.bind(this);
    this.onDetailVisitModal = this.onDetailVisitModal.bind(this);
    this.onDetailVisitModalCancel = this.onDetailVisitModalCancel.bind(this);

    this.state = {
      isDetailModalVisible: false,
      isDetailModalVisitVisible: false
    };
  }

  dateFormatter(cell) {
    return moment(cell).format("DD.MM.YYYY HH:mm");
  }

  operationFormatter(cell, row) {
    let i = 0;
    return (
      <div>
        <button className="btn btn-primary" onClick={() => this.onDetailModal(row.ticket_detail)}><i className="fa fa-file-text-o" /></button>
        <button className="btn btn-primary" onClick={() => this.onDetailVisitModal(row.ticket_detail_visits.map(x => Object.assign({}, x, { id: i++ })))}><i className="fa fa-eye" /></button>
      </div>
    );
  }

  onDetailModal(ticketDetail) {
    this.setState({ ticketDetail, isDetailModalVisible: true });
  }

  onDetailModalCancel() {
    this.setState({ isDetailModalVisible: false });
  }

  onDetailVisitModal(ticketDetailVisits) {
    this.setState({ ticketDetailVisits, isDetailVisitModalVisible: true });
  }

  onDetailVisitModalCancel() {
    this.setState({ isDetailVisitModalVisible: false });
  }

  render() {
    const { ticketDetail, isDetailModalVisible, ticketDetailVisits, isDetailVisitModalVisible } = this.state;
    const { data } = this.props;

    const props = { ... this.props };
    props.ticketDetail = ticketDetail;

    return (
      <div>
        <BootstrapTable hover condensed remote
          data={data}
          options={{
            expandRowBgColor: 'rgb(242, 255, 163)',
            noDataText: 'Нет данных',
            paginationPosition: 'top'
          }}
          className="table-history"
          tableHeaderClass="table-header"
          tableContainerClass="table-container"
          selectRow={{
            mode: 'radio',
            clickToSelect: true,
            className: 'table-row-selected',
            hideSelectColumn: true
          }}
        >
          <TableHeaderColumn width="60" dataField="id" isKey={true} hidden={true}>Код</TableHeaderColumn>
          <TableHeaderColumn width="100" dataFormat={this.operationFormatter}>Операции</TableHeaderColumn>
          <TableHeaderColumn width="120" dataField="status">Статус</TableHeaderColumn>
          <TableHeaderColumn dataField="membership_display_name">Пользователь</TableHeaderColumn>
          <TableHeaderColumn width="150" dataField="timestamp" dataFormat={this.dateFormatter}>Дата</TableHeaderColumn>
        </BootstrapTable>
        {isDetailModalVisible &&
          <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" show={isDetailModalVisible} onHide={this.onDetailModalCancel}>
            <div className="layout-modal-dialog" >
              <ModalHeader title="Детали заявки" handlers={{ onClose: this.onDetailModalCancel }} />
              <hr />
              <InformationTicketDetailMeasureFacility {...props} />
            </div>
          </Modal>
        }
        {isDetailVisitModalVisible &&
          <Modal className="layout-modal" backdropClassName="layout-modal-backdrop" show={isDetailVisitModalVisible} onHide={this.onDetailVisitModalCancel}>
            <div className="layout-modal-dialog" >
              <ModalHeader title="Просмотры заявки" handlers={{ onClose: this.onDetailVisitModalCancel }} />
              <hr />
              <TicketDetailVisitTable {...this.props} {... { data: ticketDetailVisits }} />
            </div>
          </Modal>
        }
      </div>
    );
  }
}

TicketHistoryTableMeasureFacility.propTypes = {
  propertyValues: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
  actions: PropTypes.shape({
    actionInformationTicketDetailVisits: PropTypes.func.isRequired
  }).isRequired,
  ticketDetail: PropTypes.object,
  data: PropTypes.array.isRequired
};