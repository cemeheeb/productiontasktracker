import PropTypes from 'prop-types';
import React from 'react';

import { Field } from 'redux-form/immutable';

import { SelectRedux } from './select-redux';

export class WizardFormMeasureFacility extends React.Component {
  constructor(props) {
    super(props);

    this.onFieldChange = this.onFieldChange.bind(this);
    this.onClusterChange = this.onClusterChange.bind(this);
  }

  componentDidMount() {
    this.props.actions.actionDictionaryFieldMeasureFacilityFilter();
  }

  onFieldChange(value) {
    this.props.actions.actionDictionaryClusterFilter(value);
  }

  onClusterChange(value) {
    const { inputData: { field_code, department_id } } = this.props;
    this.props.actions.actionDictionaryMeasureFacilityPositionsFilter(field_code, value, department_id);
  }

  render() {
    return (
      <div className="col-md-12">
        <div className="form-group">
          <label>Месторождение</label>
          <div className="input-group">
            <Field name="field_code"
              component={SelectRedux}
              handlers={{ onChange: this.onFieldChange }}
              options={this.props.fields.maps(x => ({lable: x.name, value: x.code}))}
              isSearchable={false}
              value={this.props.fields.filter(x => x.code === this.props.inputData.field_code).map(x => ({label: x.name, value: x.code}))}
            />
          </div>
        </div>
        <div className="form-group">
          <label>Куст</label>
          <div className="input-group">
            <Field name="cluster"
              component={SelectRedux}
              handlers={{ onChange: this.onClusterChange }}
              options={this.props.clusters.map(x => ({ label: x, value: x }))}
              isSearchable={true}
              value={this.props.inputData.cluster}
            />
          </div>
        </div>
        <div className="form-group">
          <label>Позиция АГЗУ</label>
          <div className="input-group">
            <Field name="mf_position"
              component={SelectRedux}
              options={this.props.measureFacilityPositions.map(x => ({ label: x, value: x }))}
              isSearchable={false}
              value={this.props.inputData.mf_position}
            />
          </div>
        </div>
      </div>
    );
  }
}

WizardFormMeasureFacility.propTypes = {
  actions: PropTypes.shape({
    actionDictionaryFieldMeasureFacilityFilter: PropTypes.func.isRequired,
    actionDictionaryClusterFilter: PropTypes.func.isRequired,
    actionDictionaryMeasureFacilityPositionsFilter: PropTypes.func.isRequired
  }).isRequired,
  inputData: PropTypes.shape({
    department_id: PropTypes.number,
    field_code: PropTypes.string,
    cluster: PropTypes.string,
    mf_position: PropTypes.number
  }).isRequired,
  fields: PropTypes.array.isRequired,
  isFieldsLoading: PropTypes.bool.isRequired,
  clusters: PropTypes.array.isRequired,
  isClustersLoading: PropTypes.bool.isRequired,
  measureFacilityPositions: PropTypes.array.isRequired,
  isMeasureFacilityPositionsLoading: PropTypes.bool
};

WizardFormMeasureFacility.defaultProps ={
  clusters: [],
  isClustersLoading: false
};