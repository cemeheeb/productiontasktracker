import React from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';

class InstantUpload extends React.Component {
  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);
  }
  
  onDrop(attachments) {
    this.props.actions.actionImportUpload(attachments);
  }

  render() {
    return (
      <Dropzone className="dropzone" onDrop={this.onDrop} >
        <span>После заполнения бланка:<br />Перетащите файл сюда или кликните мышью для выбора вручную</span>
      </Dropzone>
    );
  }
}

InstantUpload.propTypes = {
  actions: PropTypes.object
};

export { InstantUpload };