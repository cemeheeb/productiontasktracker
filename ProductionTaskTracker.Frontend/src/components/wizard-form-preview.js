import PropTypes from "prop-types";
import React from "react";

import { InformationTicketDetailMeasureFacility } from "./information-ticket-detail-measure-facility";
import { InformationTicketDetailWell } from "./information-ticket-detail-well";
import { InformationTicketDetailWellGroup } from "./information-ticket-detail-well-group";

import { TicketHistoryTableMeasureFacility } from "./ticket-history-table-measure-facility";
import { TicketHistoryTableWell } from "./ticket-history-table-well";
import { TicketHistoryTableWellGroup } from "./ticket-history-table-well-group";

import { Tabs } from "./tabs";
import { TabPane } from "./tab-pane";

import ticketStatusConverter from '../utilities/ticket-status-converter';
import { TicketPrintable } from '../constants/ticket-printable';

export class WizardFormPreview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isTicketHistoryVisible: false,
      ticketHistoryData: [],
      pump: null,
      contractor: null
    };

    this.actionInformationTicketHistory = this.actionInformationTicketHistory.bind(this);
    this.actionInformationTicketHistoryGroup = this.actionInformationTicketHistoryGroup.bind(this);
  }

  componentDidMount() {
    const { id, ticket_id, behaviour, pump_id, contractor_id } = this.props.ticketDetail;
    if (typeof id !== 'undefined' && id !== null) {
      behaviour === 'Group' ? this.actionInformationTicketHistoryGroup(id) : this.actionInformationTicketHistory(ticket_id);
    }

    if (typeof pump_id !== 'undefined' && pump_id !== null) {
      this.props.actions.actionDictionaryPumpFilter(pump_id).then(
        x => {
          this.setState({ pump: x.payload.title });
        }
      );
    }

    if (typeof contractor_id !== 'undefined' && contractor_id !== null) {
      this.props.actions.actionDictionaryContractorFilter(contractor_id).then(
        x => {
          this.setState({ contractor: x.payload.name });
        }
      );
    }
  }

  actionInformationTicketHistory(id) {
    if (this.state.ticketHistoryData && this.state.ticketHistoryData.length > 0) {
      return Promise.resolve(this.state.ticketHistoryData);
    }

    this.setState({ isTicketHistoryLoading: true });
    return this.props.actions.actionInformationTicketHistory(id)
      .then(response => {
        const ticketHistoryData = response.payload.map(x => ({ ...x, ...{ status: ticketStatusConverter(x.status) } }));
        this.setState({ ticketHistoryData, isTicketHistoryLoading: false });
        return Promise.resolve(response.payload);
      });
  }

  actionInformationTicketHistoryGroup(id) {
    if (this.state.ticketHistoryData && this.state.ticketHistoryData.length > 0) {
      return Promise.resolve(this.state.ticketHistoryData);
    }

    this.setState({ isTicketHistoryLoading: true });
    return this.props.actions.actionInformationTicketHistoryGroup(id)
      .then(response => {
        if (typeof response.payload === 'undefined') {
          this.setState({ isTicketHistoryLoading: false });
          return Promise.reject();
        }

        const data = {};
        response.payload.forEach(x => {
          data[x.ticket_id] = x.ticket_history.map(xx => ({ ...xx, ...{ status: ticketStatusConverter(xx.status) }}));
        });

        this.setState({ ticketHistoryData: data, isTicketHistoryLoading: false });
        return Promise.resolve(data);
      });
  }

  render() {
    const {
      isMeasureFacility,
      ticketDetail
    } = this.props;

    if (typeof ticketDetail === 'undefined' || ticketDetail === null) {
      return null;
    }

    const tabPanes = [];
    tabPanes.push(
      <TabPane key="information" label="Информация">
        <div className="wrapper">
          {isMeasureFacility === true ? <InformationTicketDetailMeasureFacility {... this.props} /> : (typeof ticketDetail.wells !== 'undefined' && ticketDetail.wells.length > 1 ? <InformationTicketDetailWellGroup {... this.props} /> : <InformationTicketDetailWell {... this.props} />)}
        </div>
      </TabPane>
    );

    if (ticketDetail.behaviour === 'Group') {
      if (typeof this.state.ticketHistoryData !== 'undefined' && Object.keys(this.state.ticketHistoryData).length > 0) {
        tabPanes.push(<TabPane key="history" label="История"><TicketHistoryTableWellGroup {... this.props} data={this.state.ticketHistoryData} /></TabPane>);
      }
    } else {
      if (typeof this.state.ticketHistoryData !== 'undefined' && this.state.ticketHistoryData.length > 0) {
        const component = isMeasureFacility ? <TicketHistoryTableMeasureFacility {...this.props} data={this.state.ticketHistoryData} /> : <TicketHistoryTableWell {...this.props} data={this.state.ticketHistoryData} />;
        tabPanes.push(<TabPane key="history" label="История">{component}</TabPane>);
      }
    }
    const isPrintable = TicketPrintable.indexOf(ticketDetail.ticket_type) > -1 && ticketDetail.id;

    return (
      <div>
        {isPrintable && < button className="btn btn-success pull-left" style={{ marginRight: '5px' }} onClick={() => this.props.actions.actionExportDocument(ticketDetail.ticket_type, ticketDetail.department_id, ticketDetail.sl, ticketDetail.is_night, ticketDetail.parameters.map(x => x.id))}>Распечатать</button>}
        <Tabs>
          {tabPanes}
        </Tabs>
      </div>
    );
  }
}

WizardFormPreview.propTypes = {
  session: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  ticketDetail: PropTypes.object,
  isMeasureFacility: PropTypes.bool
};