CREATE OR REPLACE PACKAGE BODY OPER.TT_ROUTER AS

  FUNCTION Validate(
    positionSource IN NUMBER, 
    positionDestination IN NUMBER,
    status IN NUMBER,
    routeDate DATE
  ) RETURN BOOLEAN
  AS
  BEGIN
    RETURN NULL;
  END;

  FUNCTION GetAllowedStatuses(
    positionSource IN NUMBER, 
    positionDestination IN NUMBER,
    routeDate DATE
  ) RETURN TStatusTable
  AS
  BEGIN
    RETURN NULL;
  END;

END TT_ROUTER;

/
