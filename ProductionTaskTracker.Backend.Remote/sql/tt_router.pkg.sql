CREATE OR REPLACE PACKAGE TT_ROUTER AS

  TYPE TStatusTable IS TABLE OF NUMBER;

  FUNCTION Validate(
    positionSource IN NUMBER, 
    positionDestination IN NUMBER,
    status IN NUMBER,
    routeDate DATE
  ) RETURN BOOLEAN;

  FUNCTION GetAllowedStatuses(
    positionSource IN NUMBER, 
    positionDestination IN NUMBER,
    routeDate DATE
  ) RETURN TStatusTable;

END TT_ROUTER;
/
