insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0972', 'ООО "Трайкан Велл Сервис"', 'ООО "Трайкан Велл Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0973', 'Сев-Кавказ НИПИ', 'Сев-Кавказ НИПИ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0974', 'ТСГЭ', 'ТСГЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0976', 'ПГП2', 'ПГП2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0977', 'NoyabrskNefteGaz', 'NoyabrskNefteGaz');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0978', 'ЗАО "Глобалресорс"', 'ЗАО "Глобалресорс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0979', 'Комитет по НГМР ХМАО', 'Комитет по НГМР ХМАО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0980', 'ООО "БУТТ"', 'ООО "БУТТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0981', 'ОАО "ЮНГФ"', 'ОАО "ЮНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0982', 'ООО "ГНПЦ ПурГео"', 'ООО "ГНПЦ ПурГео"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0983', 'ООО "КатОбьнефть"', 'ООО "КатОбьнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0984', 'ООО "НПРС-1"', 'ООО "НПРС-1"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0985', 'ООО "Везерфорд"', 'ООО "Везерфорд"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0986', 'ОАО НК "Паритет"', 'ОАО НК "Паритет"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0987', 'ТННЦ', 'ТННЦ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0988', 'ООО "ТЕРРА-проект"', 'ООО "ТЕРРА-проект"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0989', 'ООО "ОТО Рекавери"', 'ООО "ОТО Рекавери"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0990', 'ООО "ГЕОЭКОСЕРВИС"', 'ООО "ГЕОЭКОСЕРВИС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0302', 'НГДУ "ПН"', 'НГДУ "ПН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0307', 'НГДУ "ДН"', 'НГДУ "ДН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0308', 'НГДУ "КН"', 'НГДУ "КН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0327', 'УОиДСП', 'УОиДСП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0603', 'ООО "ЮГРАнефть"', 'ООО "ЮГРАнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0605', 'ЗАО "ЛУКОЙЛ-АИК"', 'ЗАО "ЛУКОЙЛ-АИК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0610', 'HГДУ "PИTЭKHEФTЬ"', 'HГДУ "PИTЭKHEФTЬ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0612', 'ООО " СНС" ', 'ООО " СНС" ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0620', 'СП "Катконефть"', 'СП "Катконефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0631', 'ЗAO "EГAHOЙЛ"', 'ЗAO "EГAHOЙЛ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0640', 'ЗСФ ООО "БК" "Евразия"', 'ЗСФ ООО "БК" "Евразия"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0673', 'КЭГБ-1', 'КЭГБ-1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0991', 'Нарыкарская НРЭ', 'Нарыкарская НРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0992', 'ФГУГП "Баженовская ГЭ"', 'ФГУГП "Баженовская ГЭ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9001', 'Суруханский машзавод', 'Суруханский машзавод');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9002', 'АО "ПКНМ"', 'АО "ПКНМ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9003', 'SBS Driling Prod_Sys', 'SBS Driling Prod_Sys');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9004', 'Bolland', 'Bolland');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9005', 'Трайко индастриз инк', 'Трайко индастриз инк');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9006', 'ОАО "Ижнефтемаш"', 'ОАО "Ижнефтемаш"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9007', 'АО "ЭЛКАМ-нефтемаш"', 'АО "ЭЛКАМ-нефтемаш"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9008', 'ООО СП "Аксельсон-Кубань"', 'ООО СП "Аксельсон-Кубань"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9009', 'ЛУКОЙЛ-Бакы', 'ЛУКОЙЛ-Бакы');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0202', 'НГДУ "УН"', 'НГДУ "УН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0744', 'ЭГЭБ-3', 'ЭГЭБ-3');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0749', 'УУБР №1', 'УУБР №1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0637', 'ТПП ОАО МПК "АННГ"', 'ТПП ОАО МПК "АННГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0641', 'Мирненское ЛУК-Бур', 'Мирненское ЛУК-Бур');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0643', 'Сургутское УБР', 'Сургутское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0644', 'Бавлинское УБР', 'Бавлинское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0645', 'Бугульминское УБР', 'Бугульминское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0646', 'МИНГЕО', 'МИНГЕО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0647', 'БНЭ', 'БНЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0648', 'Покачевское УБР', 'Покачевское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0649', 'Мензелинское УБР', 'Мензелинское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0650', 'Западно-Сибирское УБР', 'Западно-Сибирское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0652', 'Новомолодежное УБР', 'Новомолодежное УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0719', 'Мегионское УБР', 'Мегионское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0728', 'Мирненское УБР', 'Мирненское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0729', 'МГРЭ', 'МГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0734', 'АНРЭ', 'АНРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0735', 'СНРЭ', 'СНРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0104', 'ЦБПО по ЭПУ', 'ЦБПО по ЭПУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0109', 'УРС', 'УРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0110', 'УПНП и КРС-1', 'УПНП и КРС-1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0111', 'УПНП и КРС-2', 'УПНП и КРС-2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0113', 'УСПТГ', 'УСПТГ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0114', 'ЦТБ', 'ЦТБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0115', 'УПТО и КО', 'УПТО и КО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0116', 'УТТ-1', 'УТТ-1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0117', 'УТТ-2', 'УТТ-2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0118', 'УТТ-3', 'УТТ-3');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0119', 'УТТ-4', 'УТТ-4');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0120', 'Покачевское УТТ', 'Покачевское УТТ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0205', 'УГР', 'УГР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0206', 'УТиК', 'УТиК');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0215', 'УРС', 'УРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0306', 'СМУ', 'СМУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0313', 'УПНПиКРС', 'УПНПиКРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0317', 'УПТОиКО', 'УПТОиКО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0330', 'УРС', 'УРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0332', 'УТТ-6', 'УТТ-6');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0406', 'ЦБПО НПО', 'ЦБПО НПО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0407', 'УРС', 'УРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0409', 'УТТ №1', 'УТТ №1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0829', 'ООО "РЕМБУРСЕРВИС"', 'ООО "РЕМБУРСЕРВИС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0102', 'НГДУ"ЛН"', 'НГДУ"ЛН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0105', 'ПУ"АИСнефть"', 'ПУ"АИСнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0106', 'УТН', 'УТН');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0107', 'УЭЭС и Э', 'УЭЭС и Э');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0108', 'НАЦ', 'НАЦ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0112', 'ЛУМС', 'ЛУМС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0121', 'Трест-площадка ЛНДСР', 'Трест-площадка ЛНДСР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0122', 'Трест "ЛНС"', 'Трест "ЛНС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0203', 'УЦБПО по ПРН и БО', 'УЦБПО по ПРН и БО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0207', 'БПТО и КО №1', 'БПТО и КО №1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0208', 'БПТО и КО №2', 'БПТО и КО №2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0209', 'УТТ №1', 'УТТ №1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0210', 'УТТ №2', 'УТТ №2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0211', 'СУУТ', 'СУУТ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0303', 'УТТ-3', 'УТТ-3');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0304', 'ВУЭЭС и ЭО', 'ВУЭЭС и ЭО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0309', 'УТТ-5', 'УТТ-5');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0310', 'УТТ-1', 'УТТ-1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0311', 'УТТ-2', 'УТТ-2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0312', 'КНГП', 'КНГП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0314', 'ЦБПО ЭПУ', 'ЦБПО ЭПУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0315', 'ЦБПО БНО', 'ЦБПО БНО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0316', 'ЦБПО ПиРНОСТ', 'ЦБПО ПиРНОСТ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0318', 'ЦТБ', 'ЦТБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0319', 'УТС', 'УТС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0320', 'КНДСР', 'КНДСР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0321', 'ПУ "КАСУН"', 'ПУ "КАСУН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0322', 'УНИР', 'УНИР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0325', 'ВТФ', 'ВТФ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0331', 'ОИК ПФ "КогалымНИПИнефть"', 'ОИК ПФ "КогалымНИПИнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0402', 'ДРСУ ТПП"ПНГ"', 'ДРСУ ТПП"ПНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0405', 'ЦБПО и ЭПУ', 'ЦБПО и ЭПУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0408', 'ПУ "АИСнефть"', 'ПУ "АИСнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0410', 'УТТ №2', 'УТТ №2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0604', 'ЗАО "Турсунт"', 'ЗАО "Турсунт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0606', 'ЗАО "ГОЛОЙЛ"', 'ЗАО "ГОЛОЙЛ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0608', 'АО "ЛУКОЙЛ-Татарстан"', 'АО "ЛУКОЙЛ-Татарстан"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0609', '"ЗАО "РИТЭК - Прогресс"', '"ЗАО "РИТЭК - Прогресс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0613', 'ЗАО "Евросиб"', 'ЗАО "Евросиб"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0614', 'ОАО "ЛУКОЙЛ-Кубань"', 'ОАО "ЛУКОЙЛ-Кубань"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0615', 'WOODBINE', 'WOODBINE');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0616', 'ТОО "БИАЛ"', 'ТОО "БИАЛ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0618', 'ООО "Когалым-Термо"', 'ООО "Когалым-Термо"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0619', 'СК "ПетроАльянс"', 'СК "ПетроАльянс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR5001', 'ООО "ЕАЕ-Консалт"', 'ООО "ЕАЕ-Консалт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR5002', 'ООО "Урай НПО-Сервис"', 'ООО "Урай НПО-Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0621', 'МД Сейс ПС', 'МД Сейс ПС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0622', 'ОАО "ОТО"', 'ОАО "ОТО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0623', 'АООТ "ЛУКОЙЛ-Юг"', 'АООТ "ЛУКОЙЛ-Юг"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0624', 'ЗАО "ЛУКОЙЛ ЭПУ Сервис"', 'ЗАО "ЛУКОЙЛ ЭПУ Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0625', 'WORCOVER', 'WORCOVER');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0626', 'ОАО "Нефтестрой"', 'ОАО "Нефтестрой"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0627', 'ООО "ТермНефтеРемонт"', 'ООО "ТермНефтеРемонт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0628', 'АООТ "ЛТЕ"', 'АООТ "ЛТЕ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0630', 'ЗСНТБ', 'ЗСНТБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0632', '"БашНИПИ"', '"БашНИПИ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0633', 'ОАО "Сибнефть-ННГГФ"', 'ОАО "Сибнефть-ННГГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0634', 'КРС "ГН"', 'КРС "ГН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0635', 'КРС "WOODBINE"', 'КРС "WOODBINE"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0636', 'ОАО "Обьнефтегазгеология"', 'ОАО "Обьнефтегазгеология"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0642', 'Когалымское ЛУК-Бур', 'Когалымское ЛУК-Бур');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0651', 'Урайское "ЛУК-БУР"', 'Урайское "ЛУК-БУР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0653', 'ЭГЭБ-1', 'ЭГЭБ-1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0654', 'ЭГЭБ-2', 'ЭГЭБ-2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0657', 'ОАО "Покачевская УГР"', 'ОАО "Покачевская УГР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0658', 'Мегионская ГФЭ', 'Мегионская ГФЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0659', 'ОАО "КрУГР"', 'ОАО "КрУГР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0660', 'Сургутская ГФЭ', 'Сургутская ГФЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0661', 'ОАО "ТНГФК"', 'ОАО "ТНГФК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0662', 'АО "Сиал"', 'АО "Сиал"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0663', 'ОАО "БашНГФК"', 'ОАО "БашНГФК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0664', 'ОАО "КНГФ"', 'ОАО "КНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0665', 'Лангепасское УГГФР', 'Лангепасское УГГФР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0666', 'ЗАО "РТС"', 'ЗАО "РТС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0667', 'ООО"Урайнефтегеофизика"', 'ООО"Урайнефтегеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0668', 'АОЗТ "Синко-ремонт"', 'АОЗТ "Синко-ремонт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0669', 'АООТ КРС"Сибирь-Прогресс"', 'АООТ КРС"Сибирь-Прогресс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0670', 'ОАО "ННГФ"', 'ОАО "ННГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0671', 'УБГФЭ', 'УБГФЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0672', 'Урайская ЭГБ', 'Урайская ЭГБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0675', 'АОЗТ "Мегионгеология"', 'АОЗТ "Мегионгеология"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0676', 'Урайское УГР', 'Урайское УГР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0677', 'ООО"ЛППпоКРНСТехносервис"', 'ООО"ЛППпоКРНСТехносервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0679', 'ORS "OIL ОRSL"', 'ORS "OIL ОRSL"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0680', 'ТОО "КБ"', 'ТОО "КБ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0681', 'АСНБ', 'АСНБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0682', 'СНБ', 'СНБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0683', 'КМБ "Лангепас"', 'КМБ "Лангепас"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0684', 'Внешэкономбанк', 'Внешэкономбанк');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0685', '"Междун.аэроп."Когалым"', '"Междун.аэроп."Когалым"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0686', 'АО "ЛУКОЙЛ-Авиа"', 'АО "ЛУКОЙЛ-Авиа"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0687', 'ТОО "Типография"', 'ТОО "Типография"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0688', 'ФСТК "ЛУКТУР"', 'ФСТК "ЛУКТУР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0689', 'ЗАО "КВАНТ-Х"', 'ЗАО "КВАНТ-Х"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0690', 'СМУ-3', 'СМУ-3');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0691', 'Нефтемонтаж', 'Нефтемонтаж');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0692', 'НГКМ', 'НГКМ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0693', 'ООО "СМУ-4"', 'ООО "СМУ-4"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0694', 'СМУ-2', 'СМУ-2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0695', 'ВолгоградНИПИнефть', 'ВолгоградНИПИнефть');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0696', '"ГТНГ"', '"ГТНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0697', 'ГИПроВосток-нефть', 'ГИПроВосток-нефть');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0698', 'НИПИгазпереработка', 'НИПИгазпереработка');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0699', 'ЗАО "НГФК"', 'ЗАО "НГФК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0702', 'Трест "ТНГФ"', 'Трест "ТНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0703', 'ПО "ТНГФ"', 'ПО "ТНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0704', 'ГГП "ХГФ"', 'ГГП "ХГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0705', 'ПГО "ХГФ"', 'ПГО "ХГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0706', 'ЗСНГФ', 'ЗСНГФ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0707', 'Трест "ТЮМНГФ"', 'Трест "ТЮМНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0708', 'ПО "ТЮМНГФ"', 'ПО "ТЮМНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0709', 'Трест "БНГФ"', 'Трест "БНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0710', 'ПО "БНГФ"', 'ПО "БНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0711', '"Востокбурвод"', '"Востокбурвод"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0712', 'ПО "КНГ"', 'ПО "КНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0713', 'ПО "ЛНГ"', 'ПО "ЛНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0714', 'ПО "УНГ"', 'ПО "УНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0715', 'Главтюменьгеология', 'Главтюменьгеология');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0716', 'Главтюменьнефтегаз', 'Главтюменьнефтегаз');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0717', 'Роскомгеология', 'Роскомгеология');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0718', 'ЗСРГЦ', 'ЗСРГЦ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0720', 'ХМГРЭ', 'ХМГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0721', 'ВКНРЭ', 'ВКНРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0722', 'ШНРЭ', 'ШНРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0723', 'Конд.экспедиция', 'Конд.экспедиция');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0724', 'Краснолен.эксп.', 'Краснолен.эксп.');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0725', 'МД СЕЙС', 'МД СЕЙС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0726', 'ТОО "Новые технологии"', 'ТОО "Новые технологии"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0727', 'Повховское УБР', 'Повховское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0730', 'БУБР', 'БУБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0731', 'БУБР №1', 'БУБР №1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0732', 'БУБР №2', 'БУБР №2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0733', 'УУБР №2', 'УУБР №2');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0736', 'НУБР №1', 'НУБР №1');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0737', 'ЕУБР', 'ЕУБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0738', 'Трест "СНГФ"', 'Трест "СНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0739', 'Мозыр.эксп.', 'Мозыр.эксп.');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0740', 'Полтавское УБР', 'Полтавское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0741', 'УПНП и КРС', 'УПНП и КРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0742', 'БНРЭ ГбЗС', 'БНРЭ ГбЗС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0743', 'ШКБ', 'ШКБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0745', 'УУБР ПО"УНГ"', 'УУБР ПО"УНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0746', 'ШПГК', 'ШПГК');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0747', 'УПГК', 'УПГК');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0748', 'Трест"КНГФ"', 'Трест"КНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0750', 'СУ-36', 'СУ-36');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0751', 'СУ-75', 'СУ-75');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0752', 'СМУ-5', 'СМУ-5');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0753', 'ЦРТ НГДУ"ЛН"', 'ЦРТ НГДУ"ЛН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0754', 'СМУ НГДУ', 'СМУ НГДУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0755', 'ПМК-1 тр.ЛНС', 'ПМК-1 тр.ЛНС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0756', 'ПМК-2 тр.ЛНС', 'ПМК-2 тр.ЛНС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0757', 'ПМК-2 "Динамид"', 'ПМК-2 "Динамид"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0758', '"Строитель"', '"Строитель"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0759', 'СУ-8 "СТПС"', 'СУ-8 "СТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0760', 'СУ-43 "МТПС"', 'СУ-43 "МТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0761', 'МГА "Лан-Сиб"', 'МГА "Лан-Сиб"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0762', 'АООТ "СтрСер"', 'АООТ "СтрСер"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0763', '"УНИКОРД"', '"УНИКОРД"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0764', 'МГА', 'МГА');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0765', 'СМУ-12 "МНС"', 'СМУ-12 "МНС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0766', 'КПТ-4 "МТПС"', 'КПТ-4 "МТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0767', 'КПТ-5 "МТПС"', 'КПТ-5 "МТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0768', 'КПТ-4 "СмТПС"', 'КПТ-4 "СмТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0769', 'СМУ-1 "СТПС"', 'СМУ-1 "СТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0770', 'СМУ-14 "СмТПС"', 'СМУ-14 "СмТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0771', 'СМУ-14 "СТП+В119"', 'СМУ-14 "СТП+В119"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0772', 'СМУ-43 "СмТПС"', 'СМУ-43 "СмТПС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0773', 'ВИГАС', 'ВИГАС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0774', 'РОСФЛЕКС', 'РОСФЛЕКС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0775', 'ФАРВАТЕР', 'ФАРВАТЕР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0776', 'СМУ-4 УФА', 'СМУ-4 УФА');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0777', 'ЛИМБ', 'ЛИМБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0778', 'НБС', 'НБС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0779', '"СИБ-САГА"', '"СИБ-САГА"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0780', 'ПМК-3', 'ПМК-3');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0781', '"ГВН"', '"ГВН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0782', 'ТюменНИИгипрогаз', 'ТюменНИИгипрогаз');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0783', 'ТАТНЕФТЬ', 'ТАТНЕФТЬ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0784', 'ОАО "ТАТНИПИнефть"', 'ОАО "ТАТНИПИнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0785', 'ТАТНИИ', 'ТАТНИИ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0786', '"ЛУКойл-ЛНГ"', '"ЛУКойл-ЛНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0787', 'ООО «ЛУКОЙЛ-Инжиниринг»', 'ООО «ЛУКОЙЛ-Инжиниринг»');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0788', '"СургутНИПИ"', '"СургутНИПИ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0789', 'ЭГЭБ-1 МУБР', 'ЭГЭБ-1 МУБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0790', '"ДИНАМИД"', '"ДИНАМИД"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0791', 'СФ ГТНГ', 'СФ ГТНГ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0792', 'ЮГНП', 'ЮГНП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0793', '"СИБНЕФТЬ"', '"СИБНЕФТЬ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0794', '"НЕФТЕРЕМОНТ"', '"НЕФТЕРЕМОНТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0795', '"УзСИНКНЕФТЬ"', '"УзСИНКНЕФТЬ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0796', 'ЛУХТП', 'ЛУХТП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0797', 'ООО "НГД"', 'ООО "НГД"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0799', 'Ремонт и ЛТД и Ко', 'Ремонт и ЛТД и Ко');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0801', 'СИБНИИНП', 'СИБНИИНП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0802', 'ГТЭ', 'ГТЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0803', 'Нижневартовск НИПИ', 'Нижневартовск НИПИ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0804', 'ЦЛГТНГ', 'ЦЛГТНГ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0805', 'СибГеоЦентр', 'СибГеоЦентр');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0806', 'АО "СибКОР"', 'АО "СибКОР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0807', 'ЭИЦ', 'ЭИЦ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0808', 'ВНИИГИК', 'ВНИИГИК');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0809', 'ВНИГНИ', 'ВНИГНИ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0810', 'ВНИИНефть', 'ВНИИНефть');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0811', 'ВНИИНП', 'ВНИИНП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0812', 'ПермНИПИнефть', 'ПермНИПИнефть');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0813', 'ЗапСибНИИГеофизика', 'ЗапСибНИИГеофизика');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0814', 'ОАО "ПермНИПИнефть"', 'ОАО "ПермНИПИнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0815', 'ЗАО "Геофизмаш"', 'ЗАО "Геофизмаш"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0816', 'АОЗТ "Синко-ремонт"', 'АОЗТ "Синко-ремонт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0817', 'ООО "КрНГФ"', 'ООО "КрНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0818', 'ТУПНП и КРС-3', 'ТУПНП и КРС-3');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0819', 'НГДУ "Л-Н"', 'НГДУ "Л-Н"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0825', 'ООО"Шаимгеонефть"', 'ООО"Шаимгеонефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0826', 'КТУПО"Когалымнефтегаз"', 'КТУПО"Когалымнефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0827', 'Белорусское УБР', 'Белорусское УБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0611', 'КЗХ', 'КЗХ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0204', 'УЭЭСиЭ "Урайэнергонефть"', 'УЭЭСиЭ "Урайэнергонефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0607', 'ЗАО "ЛУКОЙЛ Петр.Сервис"', 'ЗАО "ЛУКОЙЛ Петр.Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0629', '"Севернефтепереработка"', '"Севернефтепереработка"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0655', '"Хантымансийскгеофизика"', '"Хантымансийскгеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0656', 'ООО "ТНГ-Групп"', 'ООО "ТНГ-Групп"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0678', '"ЛУКойл-Мегионгеология"', '"ЛУКойл-Мегионгеология"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0798', '"Тюменнефтегеофизика"', '"Тюменнефтегеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0920', 'исп.ГРП ТПП"ПНГ"(П,В)', 'исп.ГРП ТПП"ПНГ"(П,В)');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0921', 'исп.ГРП ТПП"КНГ"(Т,Д)', 'исп.ГРП ТПП"КНГ"(Т,Д)');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0922', 'исп.ГРП ТПП"КНГ"(Я)', 'исп.ГРП ТПП"КНГ"(Я)');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0401', 'ТПП "ПНГ"', 'ТПП "ПНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0828', 'ООО"Ватойлнефтедобыча"', 'ООО"Ватойлнефтедобыча"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0617', 'OAO "HЛВ"', 'OAO "HЛВ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0822', 'ЗАО "ГЕО-технология"', 'ЗАО "ГЕО-технология"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0820', 'НГДУ "Урьевнефть"', 'НГДУ "Урьевнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0821', 'ЗАО ПГО "ТПГ"', 'ЗАО ПГО "ТПГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0823', 'УБНРЭ', 'УБНРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0824', 'ООО "Прогресс"', 'ООО "Прогресс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0345', 'ООО "КРС "Евразия"', 'ООО "КРС "Евразия"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9018', 'Обуховский завод', 'Обуховский завод');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0858', 'ООО "Пургеофизика"', 'ООО "Пургеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0859', 'Тазовская неф.разв.эксп.', 'Тазовская неф.разв.эксп.');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0860', 'ОАО "Тугра-нефть"', 'ОАО "Тугра-нефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0889', 'ООО "Мегион-Сервис"', 'ООО "Мегион-Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0103', 'НГДУ"ПН"', 'НГДУ"ПН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0925', 'ЦИТС Ватьеган', 'ЦИТС Ватьеган');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0926', 'ЦИТС Повх', 'ЦИТС Повх');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0927', 'ЦИТС Ю.Ягун', 'ЦИТС Ю.Ягун');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0928', 'ЦИТСы ТПП КНГ', 'ЦИТСы ТПП КНГ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0929', 'Когалымский регион', 'Когалымский регион');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0861', 'ЗАО "Геоварт"', 'ЗАО "Геоварт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0890', 'АОЗТ "ТОЧМАШ"', 'АОЗТ "ТОЧМАШ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9020', 'МНГМ', 'МНГМ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9021', 'НПМ Тюмень', 'НПМ Тюмень');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0123', 'ТК "Лангепас"', 'ТК "Лангепас"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0124', 'СБ', 'СБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0125', 'У и ЭСО', 'У и ЭСО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0126', 'ПУТ и ОП', 'ПУТ и ОП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0127', 'СХК "Агросервис"', 'СХК "Агросервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0201', 'ТПП "Урайнефтегаз"', 'ТПП "УНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0212', 'ТД', 'ТД');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0213', 'Гостиница "Турсунт"', 'Гостиница "Турсунт"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0214', 'СБ', 'СБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0217', 'УКС', 'УКС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0305', 'ПЗ', 'ПЗ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0323', 'СБ', 'СБ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0324', 'Редакция "НК"', 'Редакция "НК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0326', 'КУЭРОГХ', 'КУЭРОГХ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0328', 'УОП', 'УОП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0329', 'ТК "Инфосервис"', 'ТК "Инфосервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0403', 'СБ ТПП "ПНГ"', 'СБ ТПП "ПНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0404', 'СХК "Агросервис"', 'СХК "Агросервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0411', 'ТК "Ракурс"', 'ТК "Ракурс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0602', 'ООО "ЛЗС"', 'ООО "ЛЗС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0831', 'Управление "РЕМЕКОЛ"', 'Управление "РЕМЕКОЛ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0832', 'ССУ', 'ССУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0891', 'ООО "ГеоДейтаКонсалтинг"', 'ООО "ГеоДейтаКонсалтинг"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0892', 'ООО "Геонефтегаз"', 'ООО "Геонефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0893', 'ЗАО "ПНГРП"', 'ЗАО "ПНГРП"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0894', 'ООО "ТГК"', 'ООО "ТГК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0895', 'ОАО "ХМНГГ"', 'ОАО "ХМНГГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0896', 'ОАО "Пайтых-Ойл"', 'ОАО "Пайтых-Ойл"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0897', 'ОАО НПФ"Сейсм.технол"', 'ОАО НПФ"Сейсм.технол"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0898', 'ООО "Геохим"', 'ООО "Геохим"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0899', 'ООО "Техноком-Т"', 'ООО "Техноком-Т"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0900', 'НИИГИГ ТюмГНГУ', 'НИИГИГ ТюмГНГУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0901', 'УНГРЭ', 'УНГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0993', 'ООО "БИС"', 'ООО "БИС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0994', 'ООО "ПрогрессНефтеСервис"', 'ООО "ПрогрессНефтеСервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0995', 'НПУ "РИТЭКБелоярскнефть"', 'НПУ "РИТЭКБелоярскнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0996', 'НГДУ "РИТЭКХанты-мансийск', 'НГДУ "РИТЭКХанты-мансийск');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0997', 'Ког.цех ТКРС "АРГОС"-ЧУРС', 'Ког.цех ТКРС "АРГОС"-ЧУРС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0998', 'ООО "НТС"', 'ООО "НТС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0862', 'ОАО "Находканефтегаз"', 'ОАО "Находканефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0863', 'ООО НК "ГЕОПЛАСТ"', 'ООО НК "ГЕОПЛАСТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0930', 'ООО ЛЗС(консолидированное', 'ООО ЛЗС(консолидированное');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0130', 'ООО "Л-П УРС"', 'ООО "Л-П УРС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0230', 'ООО "УУРС"', 'ООО "УУРС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0340', 'ООО "КУРС"', 'ООО "КУРС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0902', 'УРС ООО "ЛЗС"', 'УРС ООО "ЛЗС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0903', 'ООО "ЧУРС"', 'ООО "ЧУРС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0904', 'ТФ ООО"КогалымНИПИнефть"', 'ТФ ООО"КогалымНИПИнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0905', 'ООО "НК КНГ"', 'ООО "НК КНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0906', 'КНГРЭ', 'КНГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0907', 'ООО "КНГ-Сервис"', 'ООО "КНГ-Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0908', 'ООО "ИНГС"', 'ООО "ИНГС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0909', 'ЗАО "БКС"', 'ЗАО "БКС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0910', 'Назымская НГРЭ', 'Назымская НГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0911', 'ОАО "СУСС"', 'ОАО "СУСС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0912', 'ОАО "ПНО"', 'ОАО "ПНО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0913', 'ЗАО ГРК "СЭ"', 'ЗАО ГРК "СЭ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0914', 'МиМГО', 'МиМГО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0915', 'ЯГЭ', 'ЯГЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0916', 'Пургеофизика', 'Пургеофизика');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0917', 'ЭКСИС', 'ЭКСИС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0918', 'НИЦ "Югранефтегаз"', 'НИЦ "Югранефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0919', 'МНП "ГЕОДАТА"', 'МНП "ГЕОДАТА"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0940', 'ГЕОПРОЕКТ', 'ГЕОПРОЕКТ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0941', 'ООО НПП "Недра"', 'ООО НПП "Недра"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0942', 'ПУБР', 'ПУБР');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0943', 'ПНГ Бурение', 'ПНГ Бурение');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0944', 'ТСНГРЭ', 'ТСНГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0945', 'ООО "Тюменьгеофизика"', 'ООО "Тюменьгеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0923', 'ЦИТС Дружное', 'ЦИТС Дружное');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0924', 'ЦИТС Тевлин', 'ЦИТС Тевлин');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0836', 'ЭГЭБ-4', 'ЭГЭБ-4');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0218', 'УОП', 'УОП');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0219', 'ТРК "Спектр"', 'ТРК "Спектр"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0837', 'ЗАО "ЭСТТ-нефть"', 'ЗАО "ЭСТТ-нефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0864', 'ЗАО "НЕДРА-КОНСАЛТ"', 'ЗАО "НЕДРА-КОНСАЛТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0865', 'ООО "ШАНСКо"', 'ООО "ШАНСКо"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0946', 'ООО "РИДО"', 'ООО "РИДО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0947', 'ООО "Геойлбент"', 'ООО "Геойлбент"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0948', 'ООО "БК-Север"', 'ООО "БК-Север"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0949', 'ОАО "ЯНГГ"', 'ОАО "ЯНГГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0950', 'УГЭ', 'УГЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0951', 'ЗАО "ЯНГГ" филиал УГЭ', 'ЗАО "ЯНГГ" филиал УГЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0830', 'ОАО "Ямалгеофизика"', 'ОАО "Ямалгеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0101', 'ТПП "ЛНГ"', 'ТПП "ЛНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0833', 'ЗАО НПП "Гелий"', 'ЗАО НПП "Гелий"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0834', 'ООО "Прогресс"', 'ООО "Прогресс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0835', 'ЗАО "КрНГФ"', 'ЗАО "КрНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0866', 'ЗАО СП "МеКаМинефть"', 'ЗАО СП "МеКаМинефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0932', 'УУ ООО"ПНС"', 'УУ ООО"ПНС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0933', 'ООО "СУМР"', 'ООО "СУМР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0934', 'НФ ООО "СГК"', 'НФ ООО "СГК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0935', 'УСО ООО "ЛЗС"', 'УСО ООО "ЛЗС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0936', 'СЦПО ТПП "УНГ"', 'СЦПО ТПП "УНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0937', 'ООО "УНГГ"', 'ООО "УНГГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0938', 'ООО "Волганефтьсервис"', 'ООО "Волганефтьсервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0939', 'ДООО"Бургаз"Ф"Тюменбургаз', 'ДООО"Бургаз"Ф"Тюменбургаз');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0952', 'ЗАО НБК', 'ЗАО НБК');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0953', 'ООО "Недра-Аудит"', 'ООО "Недра-Аудит"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0954', 'ЯНГРЭ', 'ЯНГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0955', 'ООО НПО "НГТ"', 'ООО НПО "НГТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0601', 'СП и АО', 'СП и АО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9019', 'Ремонт РКНМ', 'Ремонт РКНМ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0867', 'ОАО "ЦГЭ"', 'ОАО "ЦГЭ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0868', 'ФГУП "СНИИГГиМС"', 'ФГУП "СНИИГГиМС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0869', 'ЗАО "НЕФТЕКОМ"', 'ЗАО "НЕФТЕКОМ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0870', 'ООО "РНТЦ"', 'ООО "РНТЦ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0871', 'ОАО НПЦ "Тверьгеофизика"', 'ОАО НПЦ "Тверьгеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0872', 'ООО "НПО СибТехНефть"', 'ООО "НПО СибТехНефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0873', 'Пар.Геофиз.Сервисз,ЛТД.', 'Пар.Геофиз.Сервисз,ЛТД.');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0874', 'ООО "Комп.Дружба-инжен."', 'ООО "Комп.Дружба-инжен."');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0875', 'АО "Актуальная геология"', 'АО "Актуальная геология"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0876', 'ООО "ГеоПЕТРОЦЕНТР"', 'ООО "ГеоПЕТРОЦЕНТР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0877', 'ООО "ГЕОВЕРС"', 'ООО "ГЕОВЕРС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0878', 'ФГУП "ИГ и РГИ"', 'ФГУП "ИГ и РГИ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0879', 'ООО НТПЦ "Сеноман"', 'ООО НТПЦ "Сеноман"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0880', 'НИИМиМ им.Н.Г.Чеботарева', 'НИИМиМ им.Н.Г.Чеботарева');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0881', 'ООО НПО "Интеграл-Х"', 'ООО НПО "Интеграл-Х"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0882', '"НТЦ ПТ+"', '"НТЦ ПТ+"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0883', 'ИПНЭ', 'ИПНЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0956', 'ЮТС НГРЭ', 'ЮТС НГРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0957', 'ООО"Геология Резервуара"', 'ООО"Геология Резервуара"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0838', 'ООО "ГСиНТ"', 'ООО "ГСиНТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0839', 'ООО "Ремнефтесервис"', 'ООО "Ремнефтесервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0884', 'ФГУП "ЗапСибГеоНАЦ"', 'ФГУП "ЗапСибГеоНАЦ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0958', 'ООО"ЦСМРнефть"', 'ООО"ЦСМРнефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0959', 'ООО "Интегра-Сервисы"', 'ООО "Интегра-Сервисы"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0960', 'ГУП ХМАО "ХМГГП"', 'ГУП ХМАО "ХМГГП"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0961', 'НГК', 'НГК');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0962', 'НГТ', 'НГТ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0963', 'ХМГТ', 'ХМГТ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0964', 'ЗСГУ', 'ЗСГУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0965', 'ООО "Геошельф-С" ', 'ООО "Геошельф-С" ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0966', 'ЗАО "БИКОМ"', 'ЗАО "БИКОМ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0967', 'ООО "Геосейссервис"', 'ООО "Геосейссервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0968', 'ООО "УАГС"', 'ООО "УАГС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0969', '"НИПИнефтегаз"', '"НИПИнефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0970', 'НИПИ "ИНПЕТРО"', 'НИПИ "ИНПЕТРО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0971', 'Мегионская НРЭ', 'Мегионская НРЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0845', 'ЗАО НПЦ "СибГео"', 'ЗАО НПЦ "СибГео"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0843', 'ЗАО "ИНЕФ"', 'ЗАО "ИНЕФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0844', 'ЗАО "Арктикнефтегаз"', 'ЗАО "Арктикнефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0846', 'ЗАО "ГЕОКОСМОС"', 'ЗАО "ГЕОКОСМОС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0847', 'ЗАО "НИЦ "Югранефтегаз"', 'ЗАО "НИЦ "Югранефтегаз"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0848', 'ОАО "Пурнефтегазгеология"', 'ОАО "Пурнефтегазгеология"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0849', 'ОАО "БК "Пурнефтегазгеоло', 'ОАО "БК "Пурнефтегазгеоло');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0850', 'ООО  "ЯмалГИС-Сервис"', 'ООО  "ЯмалГИС-Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0851', 'ООО "Геоконтроль"', 'ООО "Геоконтроль"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0852', 'ОАО "СибНАЦ"', 'ОАО "СибНАЦ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9010', 'УралЛУКтрубмаш', 'УралЛУКтрубмаш');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9011', 'Scott', 'Scott');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9012', 'MVA Gmbh', 'MVA Gmbh');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9013', 'TBS Trading Bisnes', 'TBS Trading Bisnes');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9014', 'ОАО "ПНИТИ"', 'ОАО "ПНИТИ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9015', 'Шуллер Блекман', 'Шуллер Блекман');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9016', 'Тульский машзавод', 'Тульский машзавод');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9017', 'Союэнефтеоборудование', 'Союэнефтеоборудование');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0216', 'ДРСУ', 'ДРСУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0853', 'УП "ПНГГ"', 'УП "ПНГГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0854', 'ГНИГП"БелГЕО"', 'ГНИГП"БелГЕО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0501', 'ТПП "ЯНГ"', 'ТПП "ЯНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0885', 'ООО "СУМР"', 'ООО "СУМР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0840', 'Oilwell Overseas CoLtd', 'Oilwell Overseas CoLtd');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0841', 'ЗАО "СпецБУРИнвест"', 'ЗАО "СпецБУРИнвест"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0842', 'НГКОАО"Ямалнефтегаздобыча', 'НГКОАО"Ямалнефтегаздобыча');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0886', 'Филиал ООО "НБК" "ЗапСиб"', 'Филиал ООО "НБК" "ЗапСиб"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9099', 'Прочие импортные', 'Прочие импортные');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9000', 'история', 'история');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR9098', 'Прочие отечественные', 'Прочие отечественные');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0855', 'НПЦ "Тюменьгеофизика"', 'НПЦ "Тюменьгеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0856', 'ОАО "Ямал-ГИС"', 'ОАО "Ямал-ГИС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0857', 'Пол.геофиз.эксп."ГлавТюГ"', 'Пол.геофиз.эксп."ГлавТюГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0888', 'ЭНПЦ ООО "РИТЭК"', 'ЭНПЦ ООО "РИТЭК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0887', 'ЗАО ПВП "АБС"', 'ЗАО ПВП "АБС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0150', 'ООО "ИМСС"', 'ООО "ИМСС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0251', 'ООО "ТИС" WFT', 'ООО "ТИС" WFT');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0252', 'Baker Hughes', 'Baker Hughes');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0399', 'HALLIBURTON', 'HALLIBURTON');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0594', '"ЭРИЕЛЛ Нефтегазсервис"', '"ЭРИЕЛЛ Нефтегазсервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1005', '"НИИНЕФТЕОТДАЧА"', '"НИИНЕФТЕОТДАЧА"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1006', 'ЦНТУ "Наука"', 'ЦНТУ "Наука"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1007', 'ИХН СО РАН', 'ИХН СО РАН');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1008', 'ТОО "ПОМО"', 'ТОО "ПОМО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1009', 'ОАО "Сибнефтегеофизика"', 'ОАО "Сибнефтегеофизика"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1010', 'Туринская ГЭ', 'Туринская ГЭ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0499', 'ООО "КСС"', 'ООО "КСС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1011', 'ЯГФ-Восток', 'ЯГФ-Восток');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0999', 'ТО ЦКР РОСНЕДРА по ЯНАО', 'ТО ЦКР РОСНЕДРА по ЯНАО');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1000', 'РОСНЕДРА', 'РОСНЕДРА');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1001', 'ООО "ОНГФ"', 'ООО "ОНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1002', 'ООО "ГП"-"ИГФ"', 'ООО "ГП"-"ИГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1003', 'ООО НПФ "Бинар"', 'ООО НПФ "Бинар"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR1004', 'ОАО «ГЕОТЕК Сейсморазведка»', 'ОАО «ГЕОТЕК Сейсморазведка»');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0341', 'ООО "Гидроимпульс"', 'ООО "Гидроимпульс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0342', 'ООО "НТС"', 'ООО "НТС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0343', 'ООО "АРГОС-СУМР"', 'ООО "АРГОС-СУМР"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0598', 'ООО "МЕГАЦЕНТР-ПЛЮС"', 'ООО "МЕГАЦЕНТР-ПЛЮС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0599', 'Schlumberger', 'Schlumberger');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0250', 'ООО "ТСГК"', 'ООО "ТСГК"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0344', 'ООО "БВС Евразия"', 'ООО "БВС Евразия"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0596', 'ООО "Заполярстройресурс"', 'ООО "Заполярстройресурс"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0597', 'ООО "Обьнефтесервис"', 'ООО "Обьнефтесервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0253', 'ООО НПЦ "Геостра"', 'ООО НПЦ "Геостра"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0595', 'ООО "РАРИТЕТ"', 'ООО "РАРИТЕТ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0254', 'Филиал ЗАО "ССК" УЦС', 'Филиал ЗАО "ССК" УЦС');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0151', 'ЗАО "Универсалнефтеотдача"', 'ЗАО "Универсалнефтеотдача"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0255', 'ООО "ЛАРГЕО"', 'ООО "ЛАРГЕО"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0593', 'ООО "Белоруснефть-Сибирь"', 'ООО "Белоруснефть-Сибирь"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0389', 'ФГУП "ВСЕГЕИ"', 'ФГУП "ВСЕГЕИ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0390', 'ОАО "Газпромнефть-ННГГФ"', 'ОАО "Газпромнефть-ННГГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0396', 'ООО "РУ-Энерджи КРС-МГ"', 'ООО "РУ-Энерджи КРС-МГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0397', 'ООО "Азимут ИТС"', 'ООО "Азимут ИТС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0001', 'ТПП "ПовхНГ"', 'ТПП "ПовхНГ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0391', 'ООО "ТЕХГЕОСЕРВИС"', 'ООО "ТЕХГЕОСЕРВИС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0346', 'ФГБОУ ВПО УГНТУ', 'ФГБОУ ВПО УГНТУ');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0347', 'ЗАО "НвБН"', 'ЗАО "НвБН"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0393', 'ЗАО "ТБС"', 'ЗАО "ТБС"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0256', 'ООО "Интегра-Сервисы"', 'ООО "Интегра-Сервисы"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0152', 'ЗАО «СНПХ»', 'ЗАО «СНПХ»');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0592', 'ООО "Транс - Технолоджи"', 'ООО "Транс - Технолоджи"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0392', 'ООО НК "Мастер-нефть"', 'ООО НК "Мастер-нефть"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0394', 'ООО "БСК Сокол"', 'ООО "БСК Сокол"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0395', 'ЗАО МИПГУ "Химеко- Сервис"', 'ЗАО МИПГУ "Химеко- Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0398', 'Scientific Drilling', 'Scientific Drilling');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0591', 'ЗАО «Полярэкс»', 'ЗАО «Полярэкс»');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0589', 'ООО "Норд-Сервис"', 'ООО "Норд-Сервис"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0259', 'ООО «ТПБ»', 'ООО «ТПБ»');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0412', 'ЗАО "Эмант"', 'ЗАО "Эмант"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0257', 'УЭ ООО "СГК-Бурение"', 'УЭ ООО "СГК-Бурение"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0258', 'ООО "СИА-ЛАБ"', 'ООО "СИА-ЛАБ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0388', 'ООО "УдНГФ"', 'ООО "УдНГФ"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0590', 'ООО "ФХС Поиск"', 'ООО "ФХС Поиск"');

insert into oper.tt_enterprises (id, code, name, display_name)
values (oper.tt_enterprise_sequence.nextval, 'PR0301', 'ТПП Когалымнефтегаз', 'ТПП КНГ');

commit;