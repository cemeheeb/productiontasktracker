﻿using Ninject.Modules;
using Ninject.Web.Common;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Production {
    using Business.Services;
    using Services;

    public class Module : NinjectModule {
        public override void Load() {
            Bind<IDatabaseNetwareService>().To<DatabaseNetwareService>().InRequestScope();
            Bind<IDatabaseExecuteService>().To<DatabaseExecuteService>().InRequestScope();
            Bind<IHistoryService>().To<HistoryService>().InRequestScope();
            Bind<IPumpService>().To<PumpService>().InRequestScope();
            Bind<IWellService>().To<WellService>().InRequestScope();
        }
    }
}
