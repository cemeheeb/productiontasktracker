﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Production.Services {
    using Business.Services;
    using Models;

    public class HistoryService : IHistoryService {

        #region Fields

        private readonly ILogger _logger;
        private readonly IDatabaseNetwareService _databaseNetwareService;
        private readonly IDatabaseExecuteService _databaseExecuteService;
        private readonly IWellService _wellService;
        private readonly DateTimeFormatInfo _dateTimeFormatInfo;

        #endregion
        
        public HistoryService(ILoggerFactory loggerFactory, IDatabaseNetwareService databaseNetwareService, IDatabaseExecuteService databaseExecuteService, IWellService wellService) {
            _logger = loggerFactory.GetLogger("trace");
            _databaseNetwareService = databaseNetwareService;
            _databaseExecuteService = databaseExecuteService;
            _wellService = wellService;

            var culture = CultureInfo.CreateSpecificCulture("en-US");
            _dateTimeFormatInfo = culture.DateTimeFormat;
            _dateTimeFormatInfo.DateSeparator = "/";
        }

        public IEnumerable<HistoryModel> GetHistory(string field, string cluster, string well) {
            _logger.Info($"GetHistory: field={field}; cluster={cluster}; well={well}");
            var remoteWellID = _wellService.GetRemoteWellID(field, cluster, well);
            return GetHistoryByRemoteWellID(remoteWellID);
        }

        public IEnumerable<HistoryModel> GetHistoryBetween(string field, string cluster, string well, DateTime dateStart, DateTime dateEnd) {
            try {
                _logger.Info($"GetHistoryBetween: field={field}; cluster={cluster}; well={well}; dateStart={dateStart}; dateEnd={dateEnd}");
                var remoteWellID = _wellService.GetRemoteWellID(field, cluster, well);
                return GetHistoryBetweenByRemoteWellID(remoteWellID, dateStart, dateEnd);
            } catch (Exception exception) {
                _logger.ErrorException("GetHistoryBetween", exception);
                throw;
            }
        }

        public IEnumerable<HistoryModel> GetHistoryByRemoteWellID(string remoteWellID) {
            try {
                _logger.Info("GetHistoryByEpuWellID: remoteWellID={0}", remoteWellID);
                return _databaseNetwareService.NetWareGet(() => _databaseExecuteService.ExecuteReader("LepusD", $"select [Дата:], [Насос], U, I, [Принята в:], [Принята на:], [Выполнена в:], [Принятые меры:], [Исполнитель:], [Исполнитель1:], [Время выезда], [Время возврата], [В базу внес], [Время прбытия], [Время нач работ], [Время ожидания], [ID] from [Сводка] where [КодСкв]=\"{remoteWellID}\" order by [дата:] desc", new List<HistoryModel>(), HistoryModel.Build));
            } catch (Exception exception) {
                _logger.ErrorException("GetHistoryByRemoteWellID", exception);
                throw;
            }
        }

        public IEnumerable<HistoryModel> GetHistoryBetweenByRemoteWellID(string remoteWellID, DateTime dateStart, DateTime dateEnd) {
            try {
                _logger.Info("GetHistoryBetweenByEpuWellID: remoteWellID={0}; dateStart={1}; dateEnd={2}", remoteWellID, dateEnd, dateEnd);
                return _databaseNetwareService.NetWareGet(() => _databaseExecuteService.ExecuteReader("LepusD", $"select [Дата:], [Насос], U, I, [Принята в:], [Принята на:], [Выполнена в:], [Принятые меры:], [Исполнитель:], [Исполнитель1:], [Время выезда], [Время возврата], [В базу внес], [Время прбытия], [Время нач работ], [Время ожидания], [ID] from [Сводка] where [Дата:] between #{dateStart.ToString("", _dateTimeFormatInfo)}# and #{dateEnd.ToString("", _dateTimeFormatInfo)}# and [КодСкв]=\"{remoteWellID}\" order by [дата:] desc", new List<HistoryModel>(), HistoryModel.Build));
            } catch (Exception exception) {
                _logger.ErrorException("GetHistoryBetweenByRemoteWellID", exception);
                throw;
            }
        }
    }
}
