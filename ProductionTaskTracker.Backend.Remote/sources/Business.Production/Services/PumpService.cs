﻿using System;
using System.Collections.Generic;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Production.Services {
    using Business.Services;
    using Models;

    public class PumpService : IPumpService {

        #region Fields

        private readonly ILogger _logger;
        private readonly IDatabaseNetwareService _databaseNetwareService;
        private readonly IDatabaseExecuteService _databaseExecuteService;

        #endregion
        
        public PumpService(ILoggerFactory loggerFactory, IDatabaseNetwareService databaseNetwareService, IDatabaseExecuteService databaseExecuteService) {
            _logger = loggerFactory.GetLogger("trace");
            _databaseNetwareService = databaseNetwareService;
            _databaseExecuteService = databaseExecuteService;
        }

        public IEnumerable<PumpModel> GetPumps() {
            try {
                _logger.Info("GetPumps");
                return _databaseNetwareService.NetWareGet(() => _databaseExecuteService.ExecuteReader("LepusK", @"
                    select
                        ЭЦН.СчетчикЭЦН,
                        ЭЦН.Насос,
                        ЭЦН.ТипУЭЦН,
                        ЭЦН.Габарит, 
                        ЭЦН.Номинальная_подача,
                        ЭЦН.Номинальный_напор,
                        ЭЦН.Кол1,
                        ЭЦН.Кол2,
                        ЭЦН.Кол3,
                        ЭЦН.Завод,
                        ЭЦН.Изгот
                    from [Справочник_ЭЦН] AS ЭЦН
                    ", new List<PumpModel>(), PumpModel.Build));
            } catch (Exception exception) {
                _logger.ErrorException("GetPumps", exception);
                throw;
            }
        }
    }
}
