﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Production.Services {
    using Business.Services;

    public class DatabaseExecuteService: IDatabaseExecuteService {

        #region Fields

        private readonly ILogger _logger;

        private string _connectionStringName;
        private DbProviderFactory _dbProviderFactory;

        #endregion

        public DatabaseExecuteService(ILoggerFactory loggerFactory) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public void Initialize(string connectionStringName) {
            _connectionStringName = connectionStringName;
            _dbProviderFactory = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings[connectionStringName].ProviderName);
        }

        public IList<T> ExecuteReader<T>(string connectionStringName, string query, IList<T> list, Func<IDataReader, T> readFunction) {
            Initialize(connectionStringName);
            using (var connection = GetConnection()) {
                using (var command = connection.CreateCommand()) {
                    command.CommandText = query;
                    _logger.Info(query);

                    using (var reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            list.Add(readFunction(reader));
                        }
                    }
                }
            }
            return list;
        }

        public T ExecuteReader<T>(string connectionStringName, string query) {
            Initialize(connectionStringName);

            T result;
            using (var connection = GetConnection()) {
                using (var command = connection.CreateCommand()) {
                    command.CommandText = query;
                    _logger.Info(query);
                    result = (T)command.ExecuteScalar();
                }
            }

            return result;
        }

        private IDbConnection GetConnection() {
            var connection = _dbProviderFactory.CreateConnection();
            if (connection == null)
                return null;

            connection.ConnectionString = ConfigurationManager.ConnectionStrings[_connectionStringName].ConnectionString;

            try {
                _logger.Info("db {0} is exists - {1}", @"\\LUKOIL-EPU\access\ACCESS\LepusD.mdb", File.Exists(@"\\LUKOIL-EPU\access\ACCESS\LepusD.mdb"));
                _logger.Info("db {0} is exists - {1}", @"\\LUKOIL-EPU\access\ACCESS\LepusK.mdb", File.Exists(@"\\LUKOIL-EPU\access\ACCESS\LepusK.mdb"));

                connection.Open();
            } catch (Exception e) {
                _logger.Error(e, "Невозможно получить доступ к файлам базы данных");
                throw;
            }

            return connection;
        }

    }
}
