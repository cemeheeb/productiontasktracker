﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Production.Services {
    using Business.Services;

    public class DatabaseNetwareService: IDatabaseNetwareService {
        #region DllImport

        [DllImport("calwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWCallsInit(int _in, int _out);

        [DllImport("locwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWFreeUnicodeTables();

        [DllImport("netwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWDSCreateContextHandle(out UInt32 newHandle);

        [DllImport("netwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWDSFreeContext(UInt32 context);

        [DllImport("netwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWDSLogin(UInt32 context, UInt32 optionsFlag, string objectName, string password,
                                            UInt32 validityPeriod);

        [DllImport("netwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWDSLogout(UInt32 context);

        [DllImport("netwin32.dll", CharSet = CharSet.Ansi)]
        private static extern int NWDSSetContext(UInt32 context, Int16 key, string value);

        #endregion

        #region Fields

        private readonly ILogger _logger;
        private uint _context;

        #endregion

        public DatabaseNetwareService(ILoggerFactory loggerFactory) {
            _logger = loggerFactory.GetLogger("trace");

            if (ConfigurationManager.AppSettings["NovellHost"] == null ||
                ConfigurationManager.AppSettings["NovellTree"] == null ||
                ConfigurationManager.AppSettings["NovellContext"] == null ||
                ConfigurationManager.AppSettings["NovellUserName"] == null ||
                ConfigurationManager.AppSettings["NovellPassword"] == null)
                return;

            Connect(ConfigurationManager.AppSettings["NovellHost"],
                    ConfigurationManager.AppSettings["NovellTree"],
                    ConfigurationManager.AppSettings["NovellContext"],
                    ConfigurationManager.AppSettings["NovellUserName"],
                    ConfigurationManager.AppSettings["NovellPassword"]);
        }

        public bool Connect(string serverName, string tree, string usrContext, string usrName, string usrPwd) {
            try {

                int rCode;
                if ((rCode = NWCallsInit(0, 0)) != 0)
                    ThrowException("NWCallsInit", rCode);
                if ((rCode = NWDSCreateContextHandle(out _context)) != 0)
                    ThrowException("NWDSCreateContextHandle", rCode);
                if ((rCode = NWDSSetContext(_context, 11, tree)) != 0)
                    ThrowException("NWDSSetContext Tree", rCode);
                if ((rCode = NWDSSetContext(_context, 3, usrContext)) != 0)
                    ThrowException("NWDSSetContext UserContext", rCode);
                if ((rCode = NWDSLogin(_context, 0, usrName, usrPwd, 0)) != 0)
                    ThrowException("NWDSLogin", rCode);

                _logger.Info("NetWare LogIn");
                return true;
            } catch (Exception exception) {
                _logger.Error(exception.Message);
                return false;
            }
        }

        public bool Disconnect() {
            try {

                var rCode = 0;
                if ((rCode = NWDSLogout(_context)) != 0)
                    ThrowException("NWDSLogout", rCode);
                if ((rCode = NWDSFreeContext(_context)) != 0)
                    ThrowException("NWDSFreeContext", rCode);
                if ((rCode = NWFreeUnicodeTables()) != 0)
                    ThrowException("NWFreeUnicodeTables", rCode);

                return true;
            } catch (Exception ex) {
                _logger.Error(ex.ToString());
                return false;
            }
        }

        public T NetWareGet<T>(Func<T> func) {
            T result;
            result = func();
            return result;
        }

        public void Dispose() {
            Disconnect();
        }

        private void ThrowException(string error, int rCode) {
            throw new Exception($"{error}: Error code [{rCode}]");
        }
    }
}
