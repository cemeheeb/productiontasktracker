﻿using System;
using System.Collections.Generic;

using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Production.Services {
    using Business.Services;
    using Models;

    public class WellService: IWellService {
        #region Fields

        private readonly ILogger _logger;
        private readonly IDatabaseNetwareService _databaseNetwareService;
        private readonly IDatabaseExecuteService _databaseExecuteService;

        #endregion

        public WellService(ILoggerFactory loggerFactory, IDatabaseNetwareService databaseNetwareService, IDatabaseExecuteService databaseExecuteService) {
            _logger = loggerFactory.GetLogger("trace");
            _databaseNetwareService = databaseNetwareService;
            _databaseExecuteService = databaseExecuteService;
        }

        public string GetRemoteWellID(string field, string cluster, string well) {
            try {
                _logger.Info($"GetEpuWellId: ms={field};ks={cluster};skv={well}");
                var output = _databaseNetwareService.NetWareGet(() =>
                    _databaseExecuteService.ExecuteReader<string>("LepusK", $@"
                        select [КодСкв] 
                        from [Идентификация скважины]
                        where [М-е]=""{field}"" and [Куст]=""{cluster}"" and TRIM(UCase([Скв])) like TRIM(""{well.ToUpper().Replace('P', 'Р').Replace('B', 'В').Replace('K', 'К').Replace('O', 'О').Replace('C', 'С')}"")
                    ")
                );
                _logger.Info(output);
                return output;
            } catch (Exception exception) {
                _logger.ErrorException("", exception);
                throw;
            }
        }

        public string GetRemoteWellIDByShop(string well, string cluster, int shop, string cits) {
            try {
                _logger.Info("GetEpuWellIdByShop: skv={0};kust={1};shopNum={2};cits={3}", well, cluster, shop, cits);
                var targetWellID = _databaseNetwareService.NetWareGet(() =>
                                         _databaseExecuteService.ExecuteReader<string>("LepusK",
                                             $@"select [КодСкв] from [Идентификация скважины] 
                                               where [Куст]=""{cluster}"" and TRIM(UCase([Скв])) like TRIM(""%{
                                                 well.ToUpper()
                                                     .Replace('P', 'Р')
                                                     .Replace('B', 'В')
                                                     .Replace('K', 'К')
                                                     .Replace('O', 'О')
                                                     .Replace('C', 'С')
                                                 }%"")
                                                    and [ЦИТС] = ""{cits}"" and [ЦДНГ]={shop}"));
                _logger.Info(targetWellID);
                return targetWellID;
            } catch (Exception exception) {
                _logger.ErrorException("GetRemoteWellIDByShop", exception);
                throw;
            }
        }
    }
}
