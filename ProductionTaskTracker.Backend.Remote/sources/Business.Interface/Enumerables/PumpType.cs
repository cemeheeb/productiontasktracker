﻿namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Enumerables {
    /// <summary>
    /// Тип насоса
    /// </summary>
    public enum PumpType {
        // ШГН
        SGN,
        // ЭЦН
        ECN
    }
}