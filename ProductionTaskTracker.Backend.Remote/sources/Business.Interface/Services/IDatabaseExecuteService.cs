﻿using System;
using System.Collections.Generic;
using System.Data;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Services {
    public interface IDatabaseExecuteService {
        IList<T> ExecuteReader<T>(string connectionStringName, string query, IList<T> list, Func<IDataReader, T> readFunction);

        T ExecuteReader<T>(string connectionStringName, string query);
    }
}
