﻿namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Services {
    public interface IWellService {
        string GetRemoteWellID(string field, string cluster, string well);

        string GetRemoteWellIDByShop(string well, string cluster, int shop, string cits);
    }
}
