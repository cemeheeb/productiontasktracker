﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Services {
    public interface IDatabaseNetwareService {
        bool Connect(string serverName, string tree, string userContext, string username, string password);

        bool Disconnect();

        T NetWareGet<T>(Func<T> func);
    }
}
