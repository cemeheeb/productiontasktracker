﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Services {
    using Models;

    public interface IPumpService {

        IEnumerable<PumpModel> GetPumps();
    }
}
