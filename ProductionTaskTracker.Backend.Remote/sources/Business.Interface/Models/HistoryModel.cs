﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Globalization;

using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Models {
    public class HistoryModel {
        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Дата")]
        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [Display(Name = "Насос")]
        [JsonProperty(PropertyName = "pump")]
        public string Pump { get; set; }

        [Display(Name = "U")]
        [JsonProperty(PropertyName = "u")]
        public string U { get; set; }

        [Display(Name = "I")]
        [JsonProperty(PropertyName = "i")]
        public string I { get; set; }

        [Display(Name = "Принята в")]
        [JsonProperty(PropertyName = "adopted")]
        public DateTime Adopted { get; set; }

        [Display(Name = "Принята на")]
        [JsonProperty(PropertyName = "adopted_by")]
        public DateTime AdoptedBy { get; set; }

        [Display(Name = "Выпонена в")]
        [JsonProperty(PropertyName = "performed")]
        public DateTime Performed { get; set; }

        [Display(Name = "Принятые меры")]
        [JsonProperty(PropertyName = "actions")]
        public string Actions { get; set; }

        [Display(Name = "Исполнитель")]
        [JsonProperty(PropertyName = "performer")]
        public string Performer { get; set; }

        [Display(Name = "Исполнитель1")]
        [JsonProperty(PropertyName = "performer_1")]
        public string Performer1 { get; set; }

        [Display(Name = "Время выезда")]
        [JsonProperty(PropertyName = "timeout")]
        public DateTime TimeOut { get; set; }

        [Display(Name = "Время возврата")]
        [JsonProperty(PropertyName = "return_time")]
        public DateTime ReturnTime { get; set; }

        [Display(Name = "В базу внес")]
        [JsonProperty(PropertyName = "user_insert")]
        public string UserInsert { get; set; }

        [Display(Name = "Время прибытия")]
        [JsonProperty(PropertyName = "arrival_time")]
        public DateTime ArrivalTime { get; set; }

        [Display(Name = "Начала работ")]
        [JsonProperty(PropertyName = "start_works")]
        public DateTime StartWorks { get; set; }

        [Display(Name = "Время ожидания")]
        [JsonProperty(PropertyName = "weaiting_time")]
        public DateTime WaitingTime { get; set; }

        [Display(Name = "Вид работ")]
        [JsonProperty(PropertyName = "work_type")]
        public string WorkType { get; set; }

        public static HistoryModel Build(IDataReader dataReader) {
            return new HistoryModel {
                Id = !dataReader.IsDBNull(16) ? dataReader.GetInt32(16) : -1,
                Date = !dataReader.IsDBNull(0) ? dataReader.GetDateTime(0) : DateTime.MinValue,
                Pump = !dataReader.IsDBNull(1) ? dataReader.GetString(1) : "",
                U = !dataReader.IsDBNull(2) ? dataReader.GetDouble(2).ToString(CultureInfo.InvariantCulture) : "",
                I = !dataReader.IsDBNull(3) ? dataReader.GetDouble(3).ToString(CultureInfo.InvariantCulture) : "",
                Adopted = !dataReader.IsDBNull(4) ? dataReader.GetDateTime(4) : DateTime.MinValue,
                AdoptedBy = !dataReader.IsDBNull(5) ? dataReader.GetDateTime(5) : DateTime.MinValue,
                Performed = !dataReader.IsDBNull(6) ? dataReader.GetDateTime(6) : DateTime.MinValue,
                Actions = !dataReader.IsDBNull(7) ? dataReader.GetString(7) : "",
                Performer = !dataReader.IsDBNull(8) ? dataReader.GetString(8) : "",
                Performer1 = !dataReader.IsDBNull(9) ? dataReader.GetString(9) : "",
                TimeOut = !dataReader.IsDBNull(10) ? dataReader.GetDateTime(10) : DateTime.MinValue,
                ReturnTime = !dataReader.IsDBNull(11) ? dataReader.GetDateTime(11) : DateTime.MinValue,
                UserInsert = !dataReader.IsDBNull(12) ? dataReader.GetString(12) : "",
                ArrivalTime = !dataReader.IsDBNull(13) ? dataReader.GetDateTime(13) : DateTime.MinValue,
                StartWorks = !dataReader.IsDBNull(14) ? dataReader.GetDateTime(14) : DateTime.MinValue,
                WaitingTime = !dataReader.IsDBNull(15) ? dataReader.GetDateTime(15) : DateTime.MinValue,
            };
        }
    }
}