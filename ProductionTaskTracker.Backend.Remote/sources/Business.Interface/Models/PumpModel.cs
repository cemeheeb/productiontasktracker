﻿using System.ComponentModel.DataAnnotations;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Models {

    public class PumpModel {
        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Наименование")]
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [Display(Name = "Тип насоса")]
        [JsonProperty(PropertyName = "type_ecn")]
        public string TypeECN { get; set; }

        [Display(Name = "Гб")]
        [JsonProperty(PropertyName = "gb")]
        public string Gb { get; set; }

        [Display(Name = "Номинальный поток")]
        [JsonProperty(PropertyName = "nominal_flow")]
        public short NominalFlow { get; set; }

        [Display(Name = "Номинальная подача")]
        [JsonProperty(PropertyName = "nominal_feed")]
        public short NominalFeed { get; set; }

        [Display(Name = "Дополнительная информация 1")]
        [JsonProperty(PropertyName = "col1")]
        public int Col1 { get; set; }

        [Display(Name = "Дополнительная информация 2")]
        [JsonProperty(PropertyName = "col2")]
        public int Col2 { get; set; }
        
        [Display(Name = "Дополнительная информация 3")]
        [JsonProperty(PropertyName = "col3")]
        public int Col3 { get; set; }

        [Display(Name = "Производитель")]
        [JsonProperty(PropertyName = "factory")]
        public string Factory { get; set; }

        [Display(Name = "Страна")]
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        public static PumpModel Build(IDataReader reader) {
            return new PumpModel {
                Id = reader.GetInt32(0),
                Title = !reader.IsDBNull(1) ? reader.GetString(1) : "",
                TypeECN = !reader.IsDBNull(2) ? reader.GetString(2) : "",
                Gb = !reader.IsDBNull(3) ? reader.GetString(3) : "",
                NominalFlow = !reader.IsDBNull(4) ? reader.GetInt16(4) : (short)0,
                NominalFeed = !reader.IsDBNull(5) ? reader.GetInt16(5) : (short)0,
                Col1 = !reader.IsDBNull(6) ? reader.GetInt32(6) : 0,
                Col2 = !reader.IsDBNull(7) ? reader.GetInt32(7) : 0,
                Col3 = !reader.IsDBNull(8) ? reader.GetInt32(8) : 0,
                Factory = !reader.IsDBNull(9) ? reader.GetString(9) : "",
                Country = !reader.IsDBNull(10) ? reader.GetString(10) : "",
            };
        }
    }
}