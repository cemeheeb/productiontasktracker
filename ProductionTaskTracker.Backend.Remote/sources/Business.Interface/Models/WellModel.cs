﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.Business.Models {
    public class WellModel {
        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "ЭПУ Код")]
        [JsonProperty(PropertyName = "remote_id")]
        public string RemoteID { get; set; }

        [Display(Name = "Месторождение")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [Display(Name = "Куст")]
        [JsonProperty(PropertyName = "cluster")]
        public string Cluster { get; set; }

        [Display(Name = "Номер")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }
    }
}