﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI {
    public class ExceptionLoggerFilter : IExceptionFilter {
        private readonly ILogger _logger;

        public ExceptionLoggerFilter(ILoggerFactory loggerFactory) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public virtual void OnException(ExceptionContext filterContext) {
            if (filterContext == null) {
                throw new ArgumentNullException(nameof(filterContext));
            }

            var exception = filterContext.Exception;
            var code = new HttpException(null, exception).GetHttpCode();
            if (code != 500 && code != 400 && code != 401 && code != 403)
                return;
            
            _logger.ErrorException("Global Exception Handler: ", exception);
        }

        public bool AllowMultiple => true;

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken) {
            return Task.Factory.StartNew(() => {
                _logger.ErrorException("Global Exception Handler: ", actionExecutedContext.Exception);
            }, cancellationToken);
        }
    }
}