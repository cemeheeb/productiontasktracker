﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI {
    public class LoggingMiddleware: OwinMiddleware {
        #region Fields

        private readonly ILogger _logger;

        #endregion

        public LoggingMiddleware(OwinMiddleware next, ILogger logger)
            :base(next) {
            _logger = logger;
        }

        public override async Task Invoke(IOwinContext context) {
            var timestamp = DateTime.Now;
            var unique = Guid.NewGuid().ToString("N").Substring(0, 8);

            try {
                _logger.Debug($"{context.Request.Method} {context.Request.Uri} [{unique}]");
                await Next.Invoke(context);
                _logger.Debug($"{context.Request.Method} [{unique}] Успех [{(DateTime.Now - timestamp).Milliseconds} мс]");
            } catch (Exception exception) {
                _logger.ErrorException($"{context.Request.Method} [{unique}] Исключение [{(DateTime.Now - timestamp).Milliseconds} мс]", exception);
                throw;
            }
        }
    }

    internal static class LoggingMiddlewareHandler {
        public static IAppBuilder UseLoggingMiddleware(this IAppBuilder application, ILogger logger) {
            application.Use<LoggingMiddleware>(logger);
            return application;
        }
    }
}