﻿using System;
using System.Reflection;
using System.Web.Http;
using Microsoft.Owin;
using Newtonsoft.Json;
using Ninject;
using Ninject.Extensions.Logging;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

using System.Diagnostics;

[assembly: OwinStartup(typeof(EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI.Startup))]
namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI {
    public class Startup {
        public void Configuration(IAppBuilder application) {
            var eventSourceName = "Remote.WebAPI";
            if (!EventLog.SourceExists(eventSourceName)) {
                EventLog.CreateEventSource(eventSourceName, "DebugLogger");
            }
            EventLog.WriteEntry(eventSourceName, "Entry point reached");

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            };

            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            RegisterServices(kernel);

            var loggerFactory = kernel.Get<ILoggerFactory>();
            var logger = loggerFactory.GetLogger("file");
            logger.Info("Start");

            try {
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                application.UseNinjectMiddleware(() => kernel);
                application.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

                var webApiConfiguration = new HttpConfiguration();
                webApiConfiguration.Filters.Add(new ExceptionLoggerFilter(loggerFactory));
                webApiConfiguration.MapHttpAttributeRoutes();
                application.UseNinjectWebApi(webApiConfiguration);
                
                application.UseLoggingMiddleware(logger);
                EventLog.WriteEntry(eventSourceName, "Startup succeded");
            } catch (Exception exception) {
                EventLog.WriteEntry(eventSourceName, $"Error: {exception.Message}");
                logger.FatalException("Процедура конфигурации системы вызвала исключение", exception);
                throw;
            }
        }

        private void RegisterServices(IKernel kernel) {
            kernel.Load(new Business.Production.Module());
        }
    }
}
