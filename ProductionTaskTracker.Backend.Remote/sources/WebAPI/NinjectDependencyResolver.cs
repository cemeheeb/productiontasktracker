﻿using System.Web.Http.Dependencies;
using Ninject;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI {

    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver {
        #region Fields 

        private readonly IKernel _kernel;

        #endregion

        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel) {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope() {
            return new NinjectDependencyScope(_kernel.BeginBlock());
        }
    }
}