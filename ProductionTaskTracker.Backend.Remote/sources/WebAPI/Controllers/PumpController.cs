﻿using System.Web.Http;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI.Controllers {
    using Business.Services;

    [RoutePrefix("api")]
    public class PumpController : ApiController {
        #region Fields

        private readonly ILogger _logger;
        private readonly IPumpService _pumpService;

        #endregion

        public PumpController(ILoggerFactory loggerFactory, IPumpService pumpService) {
            _logger = loggerFactory.GetLogger("trace");
            _pumpService = pumpService;
        }

        [HttpGet]
        [Route("pumps")]
        public IHttpActionResult GetPumps() {
            _logger.Info("PumpController.GetPumps()");
            return Json(_pumpService.GetPumps());
        }
    }
}