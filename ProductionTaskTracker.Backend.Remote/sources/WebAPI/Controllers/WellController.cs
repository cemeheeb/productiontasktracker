﻿using System.Web.Http;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI.Controllers {
    using Business.Services;

    [RoutePrefix("api")]
    public class WellController : ApiController {
        #region Fields

        private readonly IWellService _wellService;

        #endregion

        public WellController(IWellService wellService) {
            _wellService = wellService;
        }
    }
}