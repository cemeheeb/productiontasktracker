﻿using System;
using System.Web.Http;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Remote.WebAPI.Controllers {
    using Business.Services;

    [RoutePrefix("api")]
    public class HistoryController : ApiController {
        #region Fields

        private readonly ILogger _logger;
        private readonly IHistoryService _historyService;

        #endregion

        public HistoryController(ILoggerFactory loggerFactory, IHistoryService historyService) {
            _logger = loggerFactory.GetLogger("file");
            _historyService = historyService;
        }

        [HttpGet]
        [Route("history/repairs")]
        public IHttpActionResult GetHistory([FromUri]string field, [FromUri]string cluster, [FromUri]string well) {
            _logger.Info($"GetHistory(${field}, ${cluster}, ${well})");
            return Json(_historyService.GetHistory(field, cluster, well));
        }

        [HttpGet]
        [Route("history/repairs")]
        public IHttpActionResult GetHistoryBetween([FromUri]string field, [FromUri]string cluster, [FromUri]string well, [FromUri(Name = "date_begin")]DateTime dateBegin, [FromUri(Name = "date_end")]DateTime dateEnd) {
            _logger.Info($"GetHistory(${field}, ${cluster}, ${well}, ${dateBegin}, ${dateEnd})");
            return Json(_historyService.GetHistoryBetween(field, cluster, well, dateBegin, dateEnd));
        }
    }
}