ALTER TABLE TT_TICKET_DETAIL_A2 ADD (
  ROTATION_FREQUENCY_DELTA NUMBER(15, 3)
);

--ALTER TABLE TT_TICKET_DETAIL_A4 DROP COLUMN DMG;

ALTER TABLE TT_TICKET_DETAIL_A4 ADD DMG_TYPE VARCHAR2(50);
ALTER TABLE TT_TICKET_DETAIL_A4 ADD PULLER VARCHAR2(100);

ALTER TABLE TT_TICKET_DETAIL_B1 ADD ( 
  Q                             NUMBER(15, 3),
  H_STATIC                      NUMBER(15, 3),
  PRESSURE                      NUMBER(15, 3),
  PRESSURE_ENVIRONMENT          NUMBER(15, 3),
  PRESSURE_ENGINE               NUMBER(15, 3),
  TEMPERATURE_ENVIRONMENT       NUMBER(15, 3),
  TEMPERATURE_ENGINE            NUMBER(15, 3),
  AMPERAGE                      NUMBER(15, 3),
  LOADING                       NUMBER(15, 3),
  ROTATION_FREQUENCY            NUMBER(15, 3),
  H_DYNAMIC_15                  NUMBER(15, 3),
  PRESSURE_15                   NUMBER(15, 3),
  Q_15                          NUMBER(15, 3),
  PRESSURE_ENVIRONMENT_15       NUMBER(15, 3),
  PRESSURE_ENGINE_15            NUMBER(15, 3),
  TEMPERATURE_ENVIRONMENT_15    NUMBER(15, 3),
  TEMPERATURE_ENGINE_15         NUMBER(15, 3),
  AMPERAGE_15                   NUMBER(15, 3),
  LOADING_15                    NUMBER(15, 3),
  ROTATION_FREQUENCY_15         NUMBER(15, 3),
  H_DYNAMIC_30                  NUMBER(15, 3),
  PRESSURE_30                   NUMBER(15, 3),
  Q_30                          NUMBER(15, 3),
  PRESSURE_ENVIRONMENT_30       NUMBER(15, 3),
  PRESSURE_ENGINE_30            NUMBER(15, 3),
  TEMPERATURE_ENVIRONMENT_30    NUMBER(15, 3),
  TEMPERATURE_ENGINE_30         NUMBER(15, 3),
  AMPERAGE_30                   NUMBER(15, 3),
  LOADING_30                    NUMBER(15, 3),
  ROTATION_FREQUENCY_30         NUMBER(15, 3)
);

--alter table TT_TICKET_DETAIL_B2 drop COLUMN PRESSURE_BEFORE;
--alter table TT_TICKET_DETAIL_B2 drop COLUMN TEMPERATURE_BEFORE;
--alter table TT_TICKET_DETAIL_B2 drop COLUMN PRESSURE_AFTER;
--alter table TT_TICKET_DETAIL_B2 drop COLUMN TEMPERATURE_AFTER;

ALTER TABLE TT_TICKET_DETAIL_B2 ADD (
  PRESSURE_ENVIRONMENT_BEFORE    NUMBER(15,3),
  PRESSURE_ENGINE_BEFORE         NUMBER(15,3),
  TEMPERATURE_ENVIRONMENT_BEFORE NUMBER(15,3),
  TEMPERATURE_ENGINE_BEFORE      NUMBER(15,3),
  PRESSURE_ENVIRONMENT_AFTER     NUMBER(15,3),
  PRESSURE_ENGINE_AFTER          NUMBER(15,3),
  TEMPERATURE_ENVIRONMENT_AFTER  NUMBER(15,3),
  TEMPERATURE_ENGINE_AFTER       NUMBER(15,3)
);

--alter table TT_TICKET_DETAIL_B3 drop COLUMN PRESSURE_BEFORE;
--alter table TT_TICKET_DETAIL_B3 drop COLUMN TEMPERATURE_BEFORE;
--alter table TT_TICKET_DETAIL_B3 drop COLUMN PRESSURE_AFTER;
--alter table TT_TICKET_DETAIL_B3 drop COLUMN TEMPERATURE_AFTER;

alter table TT_TICKET_DETAIL_B3 add (
  PRESSURE_ENVIRONMENT_BEFORE NUMBER(15,3),
  PRESSURE_ENGINE_BEFORE NUMBER(15,3),
  TEMPERATURE_ENVIRONMENT_BEFORE NUMBER(15,3),
  TEMPERATURE_ENGINE_BEFORE NUMBER(15,3),
  PRESSURE_ENVIRONMENT_AFTER NUMBER(15,3),
  PRESSURE_ENGINE_AFTER NUMBER(15,3),
  TEMPERATURE_ENVIRONMENT_AFTER NUMBER(15,3),
  TEMPERATURE_ENGINE_AFTER NUMBER(15,3)
);


alter table TT_TICKET_DETAIL_B4 drop column DMG_BEFORE;
alter table TT_TICKET_DETAIL_B4 add DMG_BEFORE NUMBER(1);


alter table TT_TICKET_DETAIL_B5 add (
  DAMPING_VOLUME_CYCLE_1  NUMBER(15,3),
  WEIGHT_CYCLE_1          NUMBER(15,3),
  DAMPING_VOLUME_CYCLE_2  NUMBER(15,3),
  WEIGHT_CYCLE_2          NUMBER(15,3),
  DAMPING_VOLUME_CYCLE_3  NUMBER(15,3),
  WEIGHT_CYCLE_3          NUMBER(15,3),
  DAMPING_VOLUME_CYCLE_4  NUMBER(15,3),
  WEIGHT_CYCLE_4          NUMBER(15,3)
);

alter table TT_TICKET_DETAIL_B8 add REQUEST_TYPE VARCHAR(64) DEFAULT 'KVU' NOT NULL;

alter table TT_TICKET_DETAIL_B9 add WORKED NUMBER(1);

alter table TT_TICKET_DETAIL_B17 add (
  PRESSURE_ENVIRONMENT NUMBER(15,3),
  PRESSURE_ENGINE NUMBER(15,3),
  TEMPERATURE_ENVIRONMENT NUMBER(15,3),
  TEMPERATURE_ENGINE NUMBER(15,3)
);

alter table TT_TICKET_DETAIL_B17 add Q NUMBER(15,3);

alter table TT_TICKET_DETAIL_C1 add BRIGADE_TYPE VARCHAR2(64);

delete from tt_wells where shop_code not in ('ZX0051', 'ZX0052', 'ZX0053', 'ZX0054', 'ZX0055');

--
--DROP TABLE TT_REGISTER CASCADE CONSTRAINTS;
--
--CREATE TABLE TT_REGISTER
--(
--  TICKET_ID                       NUMBER(15)    NOT NULL,
--  TICKET_TYPE                     VARCHAR2(3 CHAR) NOT NULL,
--  TICKET_TYPE_NUM                 NUMBER(2)     NOT NULL,
--  TICKET_TYPE_CODE                NUMBER(2),
--  TICKET_TYPE_NAME                VARCHAR2(256 CHAR) NOT NULL,
--  DEPARTMENT_ID                   NUMBER(15)    NOT NULL,
--  DEPARTMENT_NAME                 VARCHAR2(256 CHAR) NOT NULL,
--  DEPARTMENT_DISPLAY_NAME         VARCHAR2(256 CHAR) NOT NULL,
--  CREATED                         DATE          DEFAULT SYSDATE               NOT NULL,
--  COMPLETION                      DATE,
--  MOVEMENT_ID                     NUMBER(15)    NOT NULL,
--  MOVEMENT_MEMBERSHIP_ID          NUMBER(15)    NOT NULL,
--  MOVEMENT_MEMBERSHIP_NAME        VARCHAR2(255 BYTE) NOT NULL,
--  MOVEMENT_POSITION_ID            NUMBER(15)    NOT NULL,
--  MOVEMENT_DEPARTMENT_ID          NUMBER(15)    NOT NULL,
--  MOVEMENT_DEPARTMENT_NAME        VARCHAR2(256 CHAR) NOT NULL,
--  MOVEMENT_DISPLAY_NAME           VARCHAR2(256 CHAR) NOT NULL,
--  MOVEMENT_STATUS                 INTEGER       DEFAULT 0                     NOT NULL,
--  MOVEMENT_STATUS_NAME            VARCHAR2(256 CHAR),
--  MOVEMENT_CREATED                DATE          DEFAULT SYSDATE               NOT NULL,
--  MOVEMENT_MEMBERSHIP_FIRST_ID    NUMBER(15)    NOT NULL,
--  MOVEMENT_MEMBERSHIP_FIRST_NAME  VARCHAR2(255 BYTE) NOT NULL,
--  MOVEMENT_DEPARTMENT_FIRST_ID    NUMBER(15)    NOT NULL,
--  MOVEMENT_DEPARTMENT_FIRST_NAME  VARCHAR2(256 CHAR) NOT NULL,
--  MOVEMENT_FIRST_DISPLAY_NAME     VARCHAR2(256 CHAR) NOT NULL,
--  DETAIL_ID                       NUMBER(15)    NOT NULL,
--  DETAIL_WELL_ID                  NUMBER(15),
--  DETAIL_WELL                     VARCHAR2(10 BYTE),
--  DETAIL_FIELD_CODE               CHAR(6 BYTE),
--  DETAIL_FIELD_NAME               VARCHAR2(256 CHAR),
--  DETAIL_CLUSTER_CODE             VARCHAR2(10 BYTE),
--  DETAIL_NOTE                     VARCHAR2(256 CHAR),
--  DETAIL_SL                       INTEGER,
--  DETAIL_MEMBERSHIP_ID            NUMBER(15)    NOT NULL,
--  DETAIL_EXPIRED                  DATE,
--  DETAIL_REQUESTED                VARCHAR2(1024 BYTE),
--  QJ                              NUMBER(15,3),
--  EQUIPMENT_VERIFICATION          VARCHAR2(256 CHAR),
--  VALVE_VERIFICATION              VARCHAR2(256 CHAR),
--  QJ_LOCKED                       NUMBER(15,3),
--  QJ_REDIRECT                     NUMBER(15,3),
--  PUMP_CRIMPING                   NUMBER(1),
--  PRESSURE_BUFFER                 NUMBER(15,3),
--  H_DYNAMIC                       NUMBER(15,3),
--  PRESSURE                        NUMBER(15,3),
--  AMPERAGE                        NUMBER(15,3),
--  LOADING                         NUMBER(15,3),
--  ROTATION_FREQUENCY              NUMBER(15,3),
--  PRESSURE_ENVIRONMENT            NUMBER(15,3),
--  PRESSURE_ENGINE                 NUMBER(15,3),
--  TEMPERATURE_ENVIRONMENT         NUMBER(15,3),
--  TEMPERATURE_ENGINE              NUMBER(15,3),
--  UNION_ACTION_TYPE               VARCHAR2(32 BYTE),
--  ROCK_FREQUENCY                  NUMBER(15,3),
--  WHEELING                        NUMBER(15,3),
--  WATERING                        NUMBER(1),
--  KVCH                            NUMBER(1),
--  MULTI_COMPONENT                 NUMBER(1),
--  CARBONATE                       NUMBER(1),
--  INHIBITOR                       NUMBER(1),
--  VOLUME                          NUMBER(15,3),
--  PULLER_TYPE_ID                  NUMBER(15),
--  VOLTAGE                         NUMBER(15,3),
--  DMG                             NUMBER(1),
--  START_TIME                      DATE,
--  TREATMENT                       VARCHAR2(32 BYTE),
--  REPAIR_TYPE                     VARCHAR2(32 BYTE),
--  PUMP_ID                         NUMBER(15),
--  PUMP_DEPTH                      NUMBER(15,3),
--  WEIGHT                          NUMBER(15,3),
--  H_DYNAMIC_MINIMAL               NUMBER(15,3),
--  TIME_COMPLETION                 DATE,
--  CALLED_AGENTS                   VARCHAR2(128 BYTE),
--  CRIMPING                        NUMBER(1),
--  CYCLES                          NUMBER(15,3),
--  PREPARING                       NUMBER(1),
--  PRESSURE_CRIMPING               NUMBER(15,3),
--  REQUESTED_EPU                   NUMBER(1),
--  REQUESTED_CNIPR                 NUMBER(1),
--  REASON                          VARCHAR2(255 BYTE),
--  MF_POSITION                     INTEGER,
--  TICKET_TYPE_B17                 VARCHAR2(3 BYTE),
--  DEBIT                           NUMBER(15,3),
--  UA                              NUMBER(1),
--  KTPN                            NUMBER(1),
--  SK                              NUMBER(1),
--  AGZU                            NUMBER(1),
--  BG                              NUMBER(1),
--  BRIGADE                         INTEGER,
--  START_TIME_PLAN                 DATE,
--  WELL_AREA                       VARCHAR2(64 BYTE),
--  H_STATIC                        NUMBER(15,3),
--  LOCK_AVAILABLE                  NUMBER(1),
--  SERVICE_AREA                    NUMBER(1),
--  BALANCE_DELTA                   NUMBER(1),
--  PILE_FIELD_DEMONTAGE            NUMBER(1),
--  REPAIR_TYPE_ID                  NUMBER(15),
--  EVENT_TYPE                      VARCHAR2(64 BYTE),
--  DURATION                        NUMBER(15,3),
--  OPERATION_MODE                  NUMBER(15,3),
--  PROBE_TIME                      DATE,
--  ACCEPTED                        DATE,
--  COMPLETED                       DATE
--)
--LOGGING 
--NOCOMPRESS 
--NOCACHE
--NOPARALLEL
--MONITORING;

-- � ��� �������
ALTER TABLE TT_REGISTER ADD (
    AMPERAGE_15                     NUMBER(15,3),
    AMPERAGE_30                     NUMBER(15,3),
    AMPERAGE_AFTER                  NUMBER(15,3),
    AMPERAGE_BEFORE                 NUMBER(15,3),
    AMPERAGE_REVERSE                NUMBER(15,3),
    BRIGADE_TYPE                    VARCHAR2(64 BYTE),
    CLUSTER_CODE                    VARCHAR2(10 BYTE),
    CONTRACTOR_ID                   NUMBER(15),
    CRIMPING_BEGIN                  NUMBER(15,3),
    CRIMPING_DURATION               NUMBER(15,3),
    CRIMPING_END                    NUMBER(15,3),
    DAMPING_VOLUME                  NUMBER(15,3),
    DAMPING_VOLUME_CYCLE_1          NUMBER(15,3),
    DAMPING_VOLUME_CYCLE_2          NUMBER(15,3),
    DAMPING_VOLUME_CYCLE_3          NUMBER(15,3),
    DAMPING_VOLUME_CYCLE_4          NUMBER(15,3),
    DMG_BEFORE                      NUMBER(1),
    DMG_TYPE                        VARCHAR2(50 BYTE),
    EXPIRED                         DATE,
    H_DYNAMIC_15                    NUMBER(15,3),
    H_DYNAMIC_30                    NUMBER(15,3),
    H_DYNAMIC_PRESSURE_AFTER_WASH   NUMBER(15,3),
    H_DYNAMIC_PRESSURE_DURATION     NUMBER(15,3),
    H_STATIC_AFTER_CRIMPING         NUMBER(15,3),
    H_STATIC_AFTER_STOP             NUMBER(15,3),
    LIQUID_PROBE                    NUMBER(1),
    LOADING_15                      NUMBER(15,3),
    LOADING_30                      NUMBER(15,3),
    LOADING_BEFORE                  NUMBER(15,3),
    LOADING_REVERSE                 NUMBER(15,3),
    MIXTURE                         NUMBER(1),
    NEED_CRIMPING                   NUMBER(1),
    NOTE                            VARCHAR2(256 CHAR),
    PHASE_CHECK                     VARCHAR2(150 BYTE),
    POWER_OFF                       NUMBER(1),
    PRESSURE_15                     NUMBER(15,3),
    PRESSURE_30                     NUMBER(15,3),
    PRESSURE_ADPM_BEGIN             NUMBER(15,3),
    PRESSURE_ADPM_END               NUMBER(15,3),
    PRESSURE_AFTER                  NUMBER(15,3),
    PRESSURE_AFTER_DAMPING          NUMBER(1),
    PRESSURE_BEFORE                 NUMBER(15,3),
    PRESSURE_ENGINE_15              NUMBER(15,3),
    PRESSURE_ENGINE_30              NUMBER(15,3),
    PRESSURE_ENGINE_AFTER           NUMBER(15,3),
    PRESSURE_ENGINE_BEFORE          NUMBER(15,3),
    PRESSURE_ENVIRONMENT_15         NUMBER(15,3),
    PRESSURE_ENVIRONMENT_30         NUMBER(15,3),
    PRESSURE_ENVIRONMENT_AFTER      NUMBER(15,3),
    PRESSURE_ENVIRONMENT_BEFORE     NUMBER(15,3),
    PROTECTION_CORRECTION           NUMBER(15,3),
    PULLER                          VARCHAR2(100 BYTE),
    PUMP_PRESSURE                   NUMBER(15,3),
    PUMP_TEMPERATURE                NUMBER(15,3),
    PURGE                           NUMBER(1),
    Q                               NUMBER(15,3),
    Q_15                            NUMBER(15,3),
    Q_30                            NUMBER(15,3),
    QJ_MANUAL                       NUMBER(15,3),
    QJ_MANUAL_AFTER                 NUMBER(15,3),
    QJ_MANUAL_BEFORE                NUMBER(15,3),
    QJ_REVERSE                      NUMBER(15,3),
    QN_VOLUME_BEFORE_COLLECTOR      NUMBER(15,3),
    QN_VOLUME_BEFORE_LOSS           NUMBER(15,3),
    REQUEST_TYPE                    VARCHAR2(64 BYTE),
    REQUESTED                       VARCHAR2(1024 BYTE),
    ROTATION_CHANGE                 NUMBER(15,3),
    ROTATION_FREQUENCY_15           NUMBER(15,3),
    ROTATION_FREQUENCY_30           NUMBER(15,3),
    ROTATION_FREQUENCY_DELTA        NUMBER(15,3),
    SHOP_CODE                       CHAR(6 BYTE),
    SL                              INTEGER,
    TEMPERATURE_AFTER               NUMBER(15,3),
    TEMPERATURE_BEFORE              NUMBER(15,3),
    TEMPERATURE_ENGINE_15           NUMBER(15,3),
    TEMPERATURE_ENGINE_30           NUMBER(15,3),
    TEMPERATURE_ENGINE_AFTER        NUMBER(15,3),
    TEMPERATURE_ENGINE_BEFORE       NUMBER(15,3),
    TEMPERATURE_ENVIRONMENT_15      NUMBER(15,3),
    TEMPERATURE_ENVIRONMENT_30      NUMBER(15,3),
    TEMPERATURE_ENVIRONMENT_AFTER   NUMBER(15,3),
    TEMPERATURE_ENVIRONMENT_BEFORE  NUMBER(15,3),
    TEMPERATURE_INTERVAL            NUMBER(15,3),
    TREATMENT_DATEBEGIN             DATE,
    TREATMENT_DATEEND               DATE,
    VOLTAGE_AFTER                   NUMBER(15,3),
    WASH_PRESSURE_BEGIN             NUMBER(15,3),
    WASH_PRESSURE_END               NUMBER(15,3),
    WEIGHT_CYCLE_1                  NUMBER(15,3),
    WEIGHT_CYCLE_2                  NUMBER(15,3),
    WEIGHT_CYCLE_3                  NUMBER(15,3),
    WEIGHT_CYCLE_4                  NUMBER(15,3),
    WORKED                          NUMBER(1),
    WASH_VOLUME                     NUMBER(15,3),
    VOLUME_BEFORE_LOSS              NUMBER(15,3),
    VOLUME_BEFORE_COLLECTOR         NUMBER(15,3),
    PROBE_VOLUME                    NUMBER(15,3),
    STOP_REASON                     VARCHAR(256),
    REQUEST_REASON                  VARCHAR(256),
    TREATMENT_TIME                  DATE,
    TREATMENT_DATE_BEGIN            DATE,
    TREATMENT_DATE_END              DATE,
    OBTAIN_TIME                     DATE,
    PADDING_TIME                    DATE
);

ALTER TABLE TT_REGISTER ADD (
    OBTAIN_TIME DATE,
    PADDING_TIME DATE
);


delete from tt_ticket_type where ticket_type in ('C2','C3','C4','C5');

update tt_ticket_type set ticket_type = 'C' where ticket_type = 'C1';