﻿SET DEFINE OFF;
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (0, 'director_cio', 'Руководитель ЦИО', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (1, 'subdirector_cio', 'Заместитель руководителя ЦИО', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (2, 'dispatcher_cio', 'Диспетчер ЦИО', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (3, 'specialist_cio', 'Специалист ЦИО', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (4, 'specialist_dispatcher_cio', 'Ответственный специалист группы ЦИО', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (5, 'superviser_cio', 'Начальник смены ЦИО', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (100, 'director_cdng', 'Руководитель ЦДНГ', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (101, 'manager_cdng', 'Лин. руководитель ЦДНГ', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (102, 'dispatcher_cdng', 'Диспетчер ЦДНГ', 0);
Insert into TT_POSITION_ROLES
   (id, name, display_name, deleted)
 Values
   (103, 'specialist_cdng', 'Исполнитель ЦДНГ', 0);
COMMIT;
