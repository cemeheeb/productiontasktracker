﻿SET DEFINE OFF;
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('A1', 1, 1, 'Отклонение от режима работы');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('A2', 1, 2, 'Изменение режима работы');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('A3', 1, 3, 'Отбор проб');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('A4', 1, 4, 'Снятие данных ТМС, ДМГ');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B1', 2, 1, 'Запуск скважины');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B2', 2, 2, 'Проведение Указание №354');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B3', 2, 3, 'Отбор проб');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B4', 2, 4, 'Снятие данных ТМС, ДМГ');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B5', 2, 5, 'Глушение скважины');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B6', 2, 6, 'Хим. обработка КЗХ');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B7', 2, 7, 'Оперссовка ГНО');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B8', 2, 8, 'Остановка скважины на исследование');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B9', 2, 9, 'Остановка скважины по технологическим причинам');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B10', 2, 10, 'Комиссионный разбор АГЗУ');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B11', 2, 11, 'Комиссионный разбор показаний СВУ');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B12', 2, 12, 'Вызов в бригаду ТРС');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B13', 2, 13, 'Вызов в бригаду КРС');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B14', 2, 14, 'Вызов в бригаду бурения');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B15', 2, 15, 'Вызов в бригаду хим. звена');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B16', 2, 16, 'Вызов к партии ГИС');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B17', 2, 17, 'Работа с ЭПУ');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('B18', 2, 18, 'Осмотр оборудования');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('C1', 3, 1, 'Подготовка скважины под ТРС');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('C2', 3, 2, 'Подготовка скважины под КРС');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('C3', 3, 3, 'Подготовка скважины под ГИС');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('C4', 3, 4, 'Подготовка скважины под ЦНИПР');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('C5', 3, 5, 'Подготовка скважины под хим. звено');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('D1', 4, 1, 'Скважины ВНР');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('D2', 4, 2, 'Скважины подконтрольные');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('E1', 5, 1, 'Отбор проб на воду по графику');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('E2', 5, 2, 'Отбор проб на КВЧ по графику');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('E3', 5, 3, 'Снятие ТМС по графику');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('E4', 5, 4, 'Проведение обработок скребком по графику');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('E5', 5, 5, 'Снятие динамограмм по графику');
Insert into TT_TICKET_TYPE
   (ticket_type, type, code, name)
 Values
   ('E6', 5, 6, 'Отбивка уровней по графику');
Insert into TT_TICKET_TYPE
   (ticket_type, type, name)
 Values
   ('F', 6, 'Информационная заявка от цеха в ЦИО');
Insert into TT_TICKET_TYPE
   (ticket_type, type, name)
 Values
   ('G', 7, 'Информационная заявка от ЦИО в цех');
COMMIT;
