﻿SET DEFINE OFF;
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('PT_', 0, 'Борец БСВП-2');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('PT_', 1, 'Ритэкс БСИ');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('PT_', 2, 'ИРЗ (ИЖ)');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('PT_', 3, 'Электон БСИ-04');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('PT_', 4, 'USB флеш носитель');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RT_', 0, 'ВНР после (ТРС, КРС, БВС, Осв )');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 1, 'ЭЦН');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 2, 'Куст/скв.');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 3, 'ГНО');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 4, 'Нсп');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 5, 'Объем раствора глушения');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 6, 'Уд. Вес');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 7, 'Нст/Рз');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 8, 'ТМС');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 9, 'Iп');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 10, 'Нд/Рз');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 11, 'Iр');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 12, 'Загрузка');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 13, 'Частота');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 14, 'Изоляция');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 15, 'ТМС');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 16, 'Дебит');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 17, 'Приток');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTE', 18, 'Проба визуально');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 19, 'Куст/скв.');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 20, 'ГНО');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 21, 'Нсп');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 22, 'Объем раствора глушения');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 23, 'Уд. Вес');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 24, 'Нст /Рз');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 25, 'ДМГ ( чило качаний, длина хода)');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 26, 'Нд/Рз');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 27, 'Дебит');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 28, 'Приток');
Insert into TT_CLASSIFIERS
   (kind, id, name)
 Values
   ('RTS', 29, 'Проба визуально');
COMMIT;
