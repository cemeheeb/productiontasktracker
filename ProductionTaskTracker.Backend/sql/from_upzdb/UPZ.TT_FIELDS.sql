﻿SET DEFINE OFF;
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0000', 'Не выделено');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0029', 'Супринское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0049', 'Малошушминское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0051', 'Северо-Покамасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0078', 'Луговое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0080', 'Восточно-Янчинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0081', 'Южно-Янчинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0082', 'Жилинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0083', 'Мартовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0084', 'Западно-Новоаганское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0101', 'Трехозерное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0102', 'Мортымья-Тетеревское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0103', 'Средне-Мулымьинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0104', 'Убинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0105', 'Северо-Даниловское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0106', 'Мулымьинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0107', 'Даниловское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0108', 'Толумское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0109', 'Потанай-Картопьинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0111', 'Филипповское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0113', 'Яхлинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0116', 'Ловинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0120', 'Западно-Тугровское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0121', 'Умытьинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0122', 'Средне-Кондинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0125', 'Мансингьянское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0136', 'Лазаревское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0141', 'Южно-Эйтьянское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0143', 'Шушминское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0144', 'Большое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0146', 'Ключевое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0154', 'Восточно-Каюмовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0164', 'Тапское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0170', 'Сыморьяхское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0176', 'Западно-Таркосалинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0177', 'Кечимовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0184', 'Западно-Могутлорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0192', 'Ольховское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0193', 'Ростовцевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0196', 'Утреннее');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0199', 'Северо-Конитлорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0225', 'Когалымское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0239', 'Яркое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0244', 'Экутальское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0248', 'Южно-Тарасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0262', 'Танеевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0263', 'Покамасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0265', 'Мишаевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0266', 'Новоортъягунское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0280', 'Харампурское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0284', 'Равенское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0286', 'Вынгаяхинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0287', 'Грибное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0290', 'Выинтойское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0292', 'Свободное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0293', 'Южно-Ягунское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0303', 'Локосовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0314', 'Северо-Поточное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0315', 'Покачевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0317', 'Ватьеганское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0323', 'Урьевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0326', 'Повховское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0330', 'Нонг-Еганское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0331', 'Поточное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0336', 'Лас-Еганское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0337', 'Нивагальское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0342', 'Курраганское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0349', 'Южно-Покачевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0354', 'Урайское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0374', 'Тарасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0378', 'Северо-Семивидовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0382', 'Чумпасское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0397', 'Западно-Котухтинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0400', 'MS0400');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0405', 'Северо-Покачевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0416', 'Южно-Выинтойское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0417', 'Восточно-Перевальное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0418', 'Восточно-Придорожное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0421', 'Северо-Тарасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0425', 'Западно-Покамасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0426', 'Южно-Покамасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0427', 'Мало-Ключевое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0430', 'Дружное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0431', 'Кочевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0432', 'Северо-Ягунское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0434', 'Марталлеровское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0436', 'Средненазымское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0437', 'Галяновское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0439', 'Апрельское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0446', 'Усть-Харампурское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0463', 'Малокартопьинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0478', 'Трехбугорное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0479', 'Комсомольское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0480', 'Губкинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0481', 'Северо-Губкинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0489', 'Узбекское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0490', 'Славинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0496', 'Барсуковское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0500', 'MS0500');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0505', 'Северо-Когалымское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0506', 'Кустовое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0507', 'Северо-Кочевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0516', 'ЮККУН-ЁГАНСКОЕ');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0522', 'Салекаптское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0526', 'Промысловое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0539', 'Красноленинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0557', 'Тазовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0560', 'Геофизическое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0563', 'Еты-Пуровское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0580', 'Голевое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0581', 'Егурьяхское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0598', 'Центральное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0601', 'Могутлорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0603', 'Источное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0607', 'Западно-Харампурское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0609', 'Имилорское+Зап-Имилорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0610', 'Пайтыхское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0611', 'Восточно-Икилорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0615', 'Западно-Имилорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0617', 'Тевлинско-Русскинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0631', 'Северо-Егурьяхское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0632', 'Рославльское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0636', 'Пальниковское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0643', 'Западно-Семивидовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0644', 'Тангинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0657', 'Южно-Мессояхское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0665', 'Находкинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0675', 'Гыданское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0676', 'Солетское+Хановейское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0688', 'Хамбатейское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0697', 'Селивониковское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0702', 'Тальниковое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0715', 'Южно-Конитлорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0727', 'Мангазейское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0740', 'Перекатное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0747', 'Северо-Ватьёганское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0751', 'Усть-Котухтинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0798', 'Хултурское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0799', 'Андреевское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0835', 'Ванское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0836', 'Ванкорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0839', 'Восточно-Тарасовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0845', 'Штормовое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0848', 'Пякяхинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0856', 'Чухлорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0857', 'Ладертойское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0862', 'Хальмерпаютинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0880', 'Северо-Хальмерпаютинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0883', 'Польемское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0891', 'Новомостовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0900', 'Кумалиягунское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0915', 'Присклоновое');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0921', 'Известинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0922', 'Северо-Айваседапуровское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0923', 'Восточно-Бугорное');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0927', 'Западно-Новомостовское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0973', 'Западно-Икилорское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0975', 'Западно-Славинское');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MS0999', 'Разведочные площади');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MSNAME', 'месторождение');
Insert into TT_FIELDS
   (code, name)
 Values
   ('MSNULL', 'Отсутствует');
COMMIT;
