﻿SET DEFINE OFF;
Insert into TT_MEMBERSHIP_ROLES
   (id, name, display_name, deleted)
 Values
   (0, 'administrator', 'Администратор', 0);
Insert into TT_MEMBERSHIP_ROLES
   (id, name, display_name, deleted)
 Values
   (1, 'user', 'Пользователь', 0);
COMMIT;
