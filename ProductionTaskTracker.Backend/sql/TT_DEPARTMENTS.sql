﻿SET DEFINE OFF;
Insert into TT_DEPARTMENTS
   (id, name, display_name, enterprise_code, deleted)
 Values
   (0, 'ТПП "Когалымнефтегаз"', 'ТПП "КНГ"', 'PR0301', 0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, deleted)
 Values
   (1, 0, 'Центральная технологическая служба', 'ЦИТС', 0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, deleted)
 Values
   (2, 1, 'Центр интегрированных операций', 'ЦИО', 0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, shop_code, 
    deleted)
 Values
   (8, 0, 'Цех добычи нефти и газа 1', 'ЦДНГ-1 (Я)', 'ZX0051', 
    0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, shop_code, 
    deleted)
 Values
   (9, 0, 'Цех добычи нефти и газа 2', 'ЦДНГ-2 (Я)', 'ZX0052', 
    0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, shop_code, 
    deleted)
 Values
   (10, 0, 'Цех добычи нефти и газа 3', 'ЦДНГ-3 (Я)', 'ZX0053', 
    0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, shop_code, 
    deleted)
 Values
   (11, 0, 'Цех добычи нефти и газа 4', 'ЦДНГ-4 (Я)', 'ZX0054', 
    0);
Insert into TT_DEPARTMENTS
   (id, parent_id, name, display_name, shop_code, 
    deleted)
 Values
   (12, 0, 'Цех добычи нефти и газа 5', 'ЦДНГ-5 (Я)', 'ZX0055', 
    0);
COMMIT;
