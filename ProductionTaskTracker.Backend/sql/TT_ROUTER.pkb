
DROP PACKAGE BODY TT_ROUTER;

CREATE OR REPLACE PACKAGE BODY OPER.TT_ROUTER AS
  -- ����� 0,
  -- ������ 1,
  -- ������� 2,
  -- ��������� 3,
  -- ������������ 4,
  -- � ������ 5,
  -- ���������� 6,
  -- �������������� - ������������ �������� ������������� 7,
  -- �� ��������� 8,
  -- ����������� - ������������ ������������� 9,
  -- ������ 12
  PROCEDURE REGISTER_CIO_TO_CDNG_ABCDEG(
    department_id_cio IN NUMBER,
    department_id_cdng IN NUMBER,
    date_begin DATE,
    date_end DATE
  )
  AS
    cioDispatcherTicketRouteID NUMBER;
  BEGIN
  
    -- ���
    
    --��������� ��� - ����
    --����� -> �������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'G', 'dispatcher_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);

    --���������� -> �� ��������� 
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cio', department_id_cio, 6, department_id_cdng, 8, department_id_cdng, date_begin, date_end);
    
    --���������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cio', department_id_cio, 6, department_id_cdng, 9, department_id_cdng, date_begin, date_end);


    -- ������� ���������� ��� - ����
    
    --����� -> �������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'specialist_dispatcher_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'G', 'specialist_dispatcher_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);

    --���������� -> �� ��������� 
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'specialist_dispatcher_cio', department_id_cio, 6, department_id_cdng, 8, department_id_cdng, date_begin, date_end);
    
    --���������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'specialist_dispatcher_cio', department_id_cio, 6, department_id_cdng, 9, department_id_cdng, date_begin, date_end);

 
    --���������� ��� - ����

    --����� -> �������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'specialist_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'G', 'specialist_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);

    --���������� -> �� ��������� 
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'specialist_cio', department_id_cio, 6, department_id_cdng, 8, department_id_cdng, date_begin, date_end);
    
    --���������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'specialist_cio', department_id_cio, 6, department_id_cdng, 9, department_id_cdng, date_begin, date_end);
  
    
    --��������� ����� ��� - ����
    
    --����� -> �������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'superviser_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'G', 'superviser_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    --���������� -> �� ��������� 
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'superviser_cio', department_id_cio, 6, department_id_cdng, 8, department_id_cdng, date_begin, date_end);
      
    --���������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'superviser_cio', department_id_cio, 6, department_id_cdng, 9, department_id_cdng, date_begin, date_end);
  
    
    --���. ���������� ��� - ����

    --����� -> �������
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'subdirector_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'G', 'subdirector_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    --���������� -> �� ��������� 
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'subdirector_cio', department_id_cio, 6, department_id_cdng, 8, department_id_cdng, date_begin, date_end);
    
    --���������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'subdirector_cio', department_id_cio, 6, department_id_cdng, 9, department_id_cdng, date_begin, date_end);

    
    --��������� ��� - ���� 
    
    --����� -> �������
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'G', 'director_cio', department_id_cio, NULL, department_id_cdng, 1, department_id_cdng, date_begin, date_end);
    
    --���������� -> �� ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cio', department_id_cio, 6, department_id_cdng, 8, department_id_cdng, date_begin, date_end);
    
    --���������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cio', department_id_cio, 6, department_id_cdng, 9, department_id_cdng, date_begin, date_end);





    -- ����

    --��������� ����

    --������� -> ��������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cdng', department_id_cdng, 1, department_id_cdng, 2, department_id_cdng, date_begin, date_end);

    --������� -> ����������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cdng', department_id_cdng, 1, department_id_cio, 6, department_id_cdng, date_begin, date_end);

    --�������� -> ����������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cdng', department_id_cdng, 2, department_id_cio, 6, department_id_cdng, date_begin, date_end);

    --�� ��������� -> ����������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'dispatcher_cdng', department_id_cdng, 8, department_id_cio, 6, department_id_cdng, date_begin, date_end);
              
    --������������ ���� - ���

    --�������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cdng', department_id_cdng, 2, department_id_cio, 3, department_id_cdng, date_begin, date_end);
 
    --�� ��������� -> ���������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cdng', department_id_cdng, 8, department_id_cio, 3, department_id_cdng, date_begin, date_end);

    --�������� -> ����������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cdng', department_id_cdng, 2, department_id_cio, 6, department_id_cdng, date_begin, date_end);

    --�� ��������� -> ����������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'director_cdng', department_id_cdng, 8, department_id_cio, 6, department_id_cdng, date_begin, date_end);
   
  
    --���. ������������ ����
  
    --�������� -> ����������
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'manager_cdng', department_id_cdng, 2, department_id_cio, 6, department_id_cdng, date_begin, date_end);

    --�� ��������� -> ���������� 
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'ABCDE', 'manager_cdng', department_id_cdng, 8, department_id_cio, 6, department_id_cdng, date_begin, date_end);
  END;

  PROCEDURE REGISTER_CIO_TO_CDNG_F(
    department_id_cio IN NUMBER,
    department_id_cdng IN NUMBER,
    date_begin DATE,
    date_end DATE
  )
  AS
  BEGIN
    --������������ ���� - ���
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'F', 'director_cdng', department_id_cdng, NULL, department_id_cio, 8, department_id_cio, date_begin, date_end);

    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'F', 'director_cdng', department_id_cdng, 2, department_id_cio, 8, department_id_cio, date_begin, date_end);
    
    --���. ������������ ����
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'F', 'director_cdng', department_id_cdng, NULL, department_id_cio, 2, department_id_cio, date_begin, date_end);

    --��������� ����
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'F', 'dispatcher_cdng', department_id_cdng, NULL, department_id_cdng, 2, department_id_cio, date_begin, date_end);
        
    --����������� ����
    insert into tt_ticket_routes (id, ticket_class, position_role, department_id_from, status_from, department_id_to, status_to, executor_id, date_begin, date_end)
        values (TT_TICKET_ROUTE_SEQUENCE.nextval, 'F', 'specialist_cdng', department_id_cdng, NULL, department_id_cdng, 2, department_id_cio, date_begin, date_end);
  END;
  
  
  PROCEDURE REGISTER_CIO_TO_CDNG_ROUTES(
    department_id_cio IN NUMBER,
    department_id_cdng IN NUMBER,
    date_begin DATE,
    date_end DATE
  )
  AS
    cioDispatcherTicketRouteID NUMBER;
  BEGIN
    REGISTER_CIO_TO_CDNG_ABCDEG(department_id_cio, department_id_cdng, date_begin, date_end);
    REGISTER_CIO_TO_CDNG_F(department_id_cio, department_id_cdng, date_begin, date_end);
  END;
END TT_ROUTER;
/
