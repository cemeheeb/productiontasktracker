﻿SET DEFINE OFF;
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0001', 'ЦДНГ-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0002', 'ЦДНГ-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0003', 'ЦДНГ-3');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0004', 'ЦДНГ-4');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0005', 'ЦДНГ-5');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0006', 'ЦДНГ-6');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0007', 'ЦДНГ-7');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0008', 'ЦДНГ-8');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0009', 'ЦДНГ-9');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0010', 'ЦДНГ-10');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0011', 'ЦДНГ-1 (Д)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0012', 'ЦДНГ-2 (К)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0013', 'ЦДНГ-3 (Д)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0021', 'ЦДHГ-1 ПH');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0022', 'ЦДНГ-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0023', 'ЦДHГ-3 ПH');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0024', 'ЦДHГ-4 (Т)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0025', 'ЦДHГ-5 (Т)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0026', 'ЦДHГ-6 (Т)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0027', 'ЦДHГ-7 (Т)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0028', 'ЦДHГ-8 (Т)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0031', 'ЦДНГ-1 (В)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0032', 'ЦДНГ-2 (В)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0033', 'ЦДНГ-3 (В)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0034', 'ЦДНГ-4 (В)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0035', 'ЦДНГ-5 (В)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0036', 'ЦДНГ-6 (В)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0041', 'ЦДНГ-1 (П)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0042', 'ЦДНГ-2 (П)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0043', 'ЦДНГ-3 (П)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0044', 'ЦДНГ-4 (П)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0045', 'ЦДНГ-5 (П)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0051', 'ЦДНГ-1 (Я)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0052', 'ЦДНГ-2 (Я)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0053', 'ЦДНГ-3 (Я)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0054', 'ЦДНГ-4 (Я)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0055', 'ЦДНГ-5 (Я)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0060', 'ЦДН "ЕГАНОЙЛ"');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0070', 'УДНГ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0091', 'ЦНИПР');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0092', 'ХАЛ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0093', 'ЛХАИ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0094', 'УДГ и ИС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0095', 'АЛФХИ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0111', 'ЦДНГ-11');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0112', 'ЦДНГ-12');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0200', 'ЦД и РС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0210', 'УД и РС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0300', 'ЦППН');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0301', 'ЦППН-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0302', 'ЦППН-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0303', 'ЦППН-3');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0304', 'ЦППН-4');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0400', 'ЦПРС УРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0401', 'ЦПРС-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0402', 'ЦПРС-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0403', 'ЦПРС-3');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0404', 'ЦПРС-4');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0405', 'ЦПРС-5');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0450', 'Уч-к ПРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0500', 'ЦКРС УРС ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0501', 'ЦКРС-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0502', 'ЦКРС-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0503', 'ЦКРС-3');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0504', 'ЦКРС-4');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0505', 'ЦКРС-5');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0506', 'ЦКРС-1"Евразия"');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0507', 'ЦКРС-2"Евразия"');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0508', 'УКРС ООО "КРС Евразия""');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0550', 'УКРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0553', 'ЦКРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0554', 'УППН СГМ и ПРСКЛМ НГП ПГМ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0555', 'УППН ЮТМ НГП ПГМ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0556', 'КНС-1УДНиГ СГМиПРСКЛМ НГП ПГМ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0557', 'КНС-3УДНиГ СГМиПРСКЛМ НГП ПГМ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0558', 'КНС УДНиГ ЮТМ НГП ПГМ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0559', 'ЗАО "ЛУК-АИК"');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0560', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0561', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0562', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0563', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0564', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0565', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0566', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0567', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0568', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0569', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0570', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0571', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0572', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0573', 'УЦ ПРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0574', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0575', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0576', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0577', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0578', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0579', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0580', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0581', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0582', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0583', 'УЦ ЭГЭБ-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0584', 'РИТС-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0585', 'РИТС-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0586', 'ЦДНГ-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0587', 'ЦДНГ-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0588', 'НГП');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0589', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0590', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0591', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0592', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0593', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0594', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0595', 'ЖЭЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0596', 'НГКП ПМ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0597', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0598', 'УЦ "АРГОС"-ЧУРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0599', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0600', 'ЦКПРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0700', 'ЦКР и ОС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0751', 'УКР и ОС №1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0752', 'УКР и ОС №2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0753', 'УКР и ОС №3');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0811', 'ЦПНП-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0812', 'ЦПНП-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0821', 'УПНП');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0831', 'УРГП');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0841', 'АСПО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0851', 'ЦГРП');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0861', 'СРИР');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0871', 'ЦРИР и ХТП');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0900', 'ЦППД');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0901', 'ЦППД-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0902', 'ЦППД-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1010', 'маркш.служб');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1020', 'ЦИТ и АСУ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1030', 'Цех  интеграц');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1040', 'СПГР');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1050', 'ГТС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1060', 'Тем. партия');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1101', 'ЦСТГ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1105', 'ЦПТГ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1150', 'НПЗ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1211', 'ЦТБ цех-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1212', 'ЦТБ цех-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1230', 'ЦАП');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1250', 'ЦТОРСТ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1260', 'ПСМТО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1310', 'ЦАП и КРТС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1320', 'ПРЦО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1330', 'ПРЦЭ и Э');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1340', 'ПРЦ-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1350', 'ПРЦ-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1360', 'ЦРИО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1370', 'ЦСО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1380', 'ЦРВПО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1390', 'БПТО и КО');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1400', 'ЦАП и КРТС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1410', 'ЦАП и КРТС-1');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1420', 'ЦАП и КРТС-2');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1600', 'ЦТС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1700', 'ЦИТС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1800', 'Уч.комб.');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1801', 'УЦ КРС');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1802', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1803', 'УЦ');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1804', 'УЦ КРС «УНО»');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1805', 'УЦ "Везерфорз"');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZXNAME', 'цех');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX5001', 'Отдел программного обеспечения');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZXNULL', 'Отсутствует');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0000', 'ЦЕХА');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0046', 'ЦДНГ-6 (П)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0061', 'ЦДНГ-1 (ПГМ)');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX0305', 'Цех добычи нефти и газа Пуровской группы месторождений');
Insert into TT_SHOPS
   (code, name)
 Values
   ('ZX1806', 'Условный цех ООО НПП «Недра»');
COMMIT;
