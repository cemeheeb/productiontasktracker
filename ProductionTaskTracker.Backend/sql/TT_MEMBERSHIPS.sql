﻿SET DEFINE OFF;
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (10000, 'WinService', ' ', ' ', ' ', 
    TO_DATE('01/31/2018 09:25:29', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (97, 'RyabovaIA', 'Ильвина', 'Рябова', 'Александровна', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (99, 'director_cio', ' ', 'Тестовый#1', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (100, 'nach_smen_cio', ' ', 'Тестовый#2', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (101, 'dispatcher_cio', ' ', 'Тестовый#3', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (102, 'specialist_cio', ' ', 'Тестовый#4', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (103, 'director_cdng', ' ', 'Тестовый#5', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (104, 'manager_cdng', ' ', 'Тестовый#6', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (105, 'dispatcher_cdng', ' ', 'Тестовый#7', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (0, 'PICHUZHKINRM', 'Роман', 'Пичужкин', 'Михайлович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (1, 'GATAULLINIM', 'Ильнур', 'Гатауллин', 'Магнавиевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (2, 'KudryavtsevFP', 'Филипп', 'Кудрявцев', 'Владимирович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (3, 'ChistotinIA', 'Иван', 'Чистотин', 'Анатольевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (4, 'ZerschikovLV', 'Лев', 'Зерщиков', 'Васильевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (5, 'NachSmUYM', ' ', 'Нач. смены ЦИТС', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (6, 'IslamovRF', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Ришат', 
    'Исламов', 'Фанилович', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (7, 'MutalimovHM', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Хизри', 
    'Муталимов', 'Магомедович', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (8, 'YamaletdinovIF', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Ильнур', 
    'Ямалетдинов', 'Фаисович', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (9, 'RomanenkoAS', 'Алексей', 'Романенко', 'Сергеевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (10, 'ZavarzinAV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Алексей', 
    'Заварзин', 'Витальевич', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (11, 'DispCUY', ' ', 'Инженер - технолог ЦИТС', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (12, 'MitrofanovaNV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Наталья', 
    'Митрофанова', 'Владимировна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (13, 'ShershenNM', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Надежда', 
    'Шершень', 'Михайловна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (14, 'SuncovaTV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Татьяна', 
    'Сунцова', 'Валерьевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (15, 'GumerovaAH', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Алсу', 
    'Гумерова', 'Хатиповна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (16, 'StadnikON', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Оксана', 
    'Стадник', 'Николаевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (17, 'UmurakovPL', 'Павел', 'Умураков', 'Леонидович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (18, 'SurguchevAA', 'Алексей', 'Сургучев', 'Анатольевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (19, 'BurkhanovFR', 'Фидус', 'Бурханов', 'Разитович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (20, 'SadrislamovII', 'Ильдус', 'Садрисламов', 'Ильдарович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (21, 'YakushkinAB', 'Антон', 'Якушкин', 'Борисович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (22, 'ZalisniyDI', 'Дмитрий', 'Залисный', 'Иванович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (23, 'PenyaevVS', 'Владимир', 'Пеняев', 'Сергеевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (24, 'TrofimovIS', 'Иван', 'Трофимов', 'Семенович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (25, 'ChulyakminMarM', 'Марсель', 'Чулякмин', 'Мансурович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (26, 'GadzhievMG', 'Мурад', 'Гаджиев', 'Гаджиевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (27, 'MerzlyakovSA', 'Сергей', 'Мерзляков', 'Александрович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (28, 'ElizarovAVl', 'Алексей', 'Елизаров', 'Владимирович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (29, 'SharafutdinovRAi', 'Рустам', 'Шарафутдинов', 'Айратович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (30, 'VakhitovIA', 'Ирек', 'Вахитов', 'Асгатович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (31, 'LohanovaSI', 'Светлана', 'Лоханова', 'Ивановна', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (32, 'MingazovaET', 'Элина', 'Мингазова', 'Талгатовна', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (33, 'GontarGM', 'Галина', 'Гонтарь', 'Михайловна', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (34, 'VildanovTF', 'Тимур', 'Вильданов', 'Флоридович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (35, 'DyachenkoSAn', 'Сергей', 'Дьяченко', 'Андреевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (36, 'MahmutovRRad', 'Раиль', 'Махмутов', 'Радикович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (37, 'AhatovDA', 'Динар', 'Ахатов', 'Айратович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (38, 'GalievLR', 'Ленар', 'Галиев', 'Разимович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (39, 'NasibulinDN', 'Денис', 'Насибулин', 'Наильевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (40, 'TurinVV', 'Валерий', 'Тюрин', 'Владимирович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (41, 'VasilenkoAA', 'Андрей', 'Василенко', 'Александрович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (42, 'MagomedovNG', 'Нуричу', 'Магомедов', 'Гусейнович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (43, 'TolstikovAleV', 'Александр', 'Толстиков', 'Владимирович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (44, 'GancevVS', 'Владимир', 'Ганцев', 'Сергеевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (45, 'DispC1UY', ' ', 'Оператор ПУ ЦДНГ1', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (46, 'KazakkulovaSA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Регина', 
    'Фаттахова', 'Айратовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (47, 'FattahovaRA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Светлана', 
    'Казаккулова', 'Александровна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (48, 'LihachevaIS', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Ирина', 
    'Лихачева', 'Станиславовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (49, 'KislenkovaEA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Екатерина', 
    'Кисленкова', 'Анатольевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (50, 'SidorkinaKS', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Катерина', 
    'Сидоркина', 'Сергеевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (51, 'DevyataikinDN', 'Денис', 'Девятайкин', 'Николаевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (52, 'SergeevMA', 'Михаил', 'Сергеев', 'Анатольевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (53, 'kovalevno', 'Никита', 'Ковалев', 'Олегович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (54, 'GalimovRN', 'Радик', 'Галимов', 'Наилевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (55, 'MaschenkoSN', 'Сергей', 'Мащенко', 'Николаевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (56, 'dilmievbs', 'Булат', 'Дильмиев', 'Салаватович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (57, 'DispC2UY', ' ', 'Оператор ПУ ЦДНГ2', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (58, 'ArtemevaTN', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Татьяна', 
    'Артемьева', 'Николаевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (59, 'PirmatovaVR', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Вефа', 
    'Пирматова', 'Рамазановна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (60, 'TangatarovaID', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Ирина', 
    'Тангатарова', 'Дилшатовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (61, 'GudkovaMA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Мария', 
    'Гудкова', 'Альбертовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (62, 'KalashnikUI', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Юлия', 
    'Калашник', 'Игоревна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (63, 'EremeevDA', 'Дмитрий', 'Еремеев', 'Анатольевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (64, 'NosachenkoEA', 'Евгений', 'Носаченко', 'Анатольевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (65, 'zmanovskyea', 'Евгений', 'Змановский', 'Александрович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (66, 'SherbakovFV', 'Филипп', 'Щербаков', 'Викторович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (67, 'IsakovVV', 'Виктор', 'Исаков', 'Владимирович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (68, 'AkhmetovIL', 'Ильшат', 'Ахметов', 'Ленарисович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (69, 'DispC3_UY', ' ', 'Оператор ПУ ЦДНГ3', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (70, 'GuminskayaMI', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Марина', 
    'Гуминская', 'Игоревна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (71, 'MakuhinaOV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Оксана', 
    'Макухина', 'Владимировна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (72, 'BuravovaDS', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Дина', 
    'Буравова', 'Сергеевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (73, 'VoytovichKA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Кристина', 
    'Войтович', 'Андреевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (74, 'IlashkoVN', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Виктория', 
    'Илашко', 'Николаевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (75, 'SnegirevAP', 'Алексей', 'Снегирёв', 'Петрович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (76, 'PustovitOV', 'Олег', 'Пустовит', 'Вячеславович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (77, 'SaitovDE', 'Динар', 'Саитов', 'Эльмирович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (78, 'KrylovND', 'Николай', 'Крылов', 'Дмитриевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (79, 'DernovSN', 'Сергей', 'Дернов', 'Николаевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (80, 'DispCDNG4UY', ' ', 'Оператор ПУ ЦДНГ4', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (81, 'IlinaII', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Инна', 
    'Ильина', 'Ивановна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (82, 'SitdikovaZV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Зоя', 
    'Ситдикова', 'Витальевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (83, 'ZaharovaVR', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Валерия', 
    'Захарова', 'Расуловна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (84, 'DgangarovaTA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Татьяна', 
    'Джангарова', 'Александровна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (85, 'AmirovaFU', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Фируза', 
    'Амирова', 'Ульфатовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (86, 'VajnyakAIv', 'Андрей', 'Важняк', 'Иванович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (87, 'BeysVY', 'Вадим', 'Бейс', 'Юрьевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (88, 'YakivchikIV', 'Иван', 'Якивчик', 'Васильевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (89, 'ShipunovYA', 'Юрий', 'Шипунов', 'Анатольевич', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (90, 'KhajrullinLS', 'Ленар', 'Хайруллин', 'Сиренович', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, firstname, lastname, patronym, 
    created, deleted)
 Values
   (91, 'DispC5UY', ' ', 'Оператор ПУ ЦДНГ5', ' ', 
    TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (92, 'SahipovaGM', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Гульназ', 
    'Сахипова', 'Маратовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (93, 'BacLV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Людмила', 
    'Бац', 'Васильевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (94, 'AzanovaTA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Татьяна', 
    'Азанова', 'Александровна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (95, 'TalipovaGV', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Гузель', 
    'Талипова', 'Вазихатовна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into TT_MEMBERSHIPS
   (id, username, password, password_salt, firstname, 
    lastname, patronym, created, deleted)
 Values
   (96, 'HusnutdinovaGA', '0paLEHpRUzfcu1Kpp1Nu6A==', '--------------------------------', 'Гельфара', 
    'Хуснутдинова', 'Абдулхаевна', TO_DATE('01/28/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 0);
COMMIT;
