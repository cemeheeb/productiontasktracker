select *
from (
    select distinct movement_department_id Code, movement_department_name Value
    from tt_register
    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
        and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
        and upper(movement_department_first_name) like upper(:a) and  upper(movement_department_name) like upper(?) and  
        movement_department_first_id = :department_id
) order by Code;

delete from tberror;
select * from tberror order by dt;


select * from tt_register_pending;

select * from tt_ticket_movements order by id desc;

insert into tt_register_pending (ticket_movement_id) values(4774);
call tt_register_pkg.calculate_register();

select * from tt_register where movement_id = 4774;


select 
    ticket_id as "TicketID",
    ticket_type as "TicketType",
    movement_status as "TicketStatus",
    movement_first_display_name as "DepartmentFrom",
    movement_department_first_id as "DepartmentIDFrom",
    department_display_name as "DepartmentTo",
    department_id as "DepartmentIDTo",
    detail_field_name as "Field",
    detail_field_code as "FieldCode",
    detail_cluster_code as "Cluster",
    detail_well as "Well",
    detail_well_id as "WellID",
    created as "Created",
    detail_expired as "Expired",
    completed as "Completed",
    detail_id as "TicketDetailID",
    detail_sl as "SL",
    0 as "IsNight",
    case when movement_status <> 9 and completed < created then 1 else 0 end as "IsRepeated"
from tt_register
where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
       and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id) AND
       movement_department_first_id = :departmentID
order by ticket_id desc;


select * from (
    select 
        ticket_id as "TicketID",
        ticket_type as "TicketType",
        movement_status as "TicketStatus",
        movement_first_display_name as "DepartmentFrom",
        movement_department_first_id as "DepartmentIDFrom",
        department_display_name as "DepartmentTo",
        department_id as "DepartmentIDTo",
        detail_field_name as "Field",
        detail_field_code as "FieldCode",
        detail_cluster_code as "Cluster",
        detail_well as "Well",
        detail_well_id as "WellID",
        created as "Created",
        detail_expired as "Expired",
        completed as "Completed",
        detail_id as "TicketDetailID",
        detail_sl as "SL",
        DECODE(movement_membership_first_id, :membershipID, 1, 0) as "AllowUnregister",
        0 as "IsNight",
        case when movement_status <> 9 and completed < created then 1 else 0 end as "IsRepeated"
    from tt_register
    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
           and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id) and
           movement_department_first_id = :departmentID
    order by ticket_id desc
);

select * from tt_register order by movement_id desc;







select * from tt_register order by movement_id desc;

select * from tt_ticket_movements order by id desc;

drop table tt_register_pending;
create global temporary table tt_register_pending (ticket_movement_id NUMBER CONSTRAINT TT_RP_TICKET_MOVEMENT_ID NOT NULL ) ON COMMIT DELETE ROWS;

CREATE TABLE TT_SHOPS
(
  CODE  CHAR(6 CHAR) CONSTRAINT TT_S_CODE       NOT NULL,
  NAME  VARCHAR2(250 CHAR) CONSTRAINT TT_S_NAME NOT NULL
);


select * from tt_memberships;