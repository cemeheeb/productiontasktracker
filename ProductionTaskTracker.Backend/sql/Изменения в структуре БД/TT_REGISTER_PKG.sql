CREATE OR REPLACE PACKAGE BODY tt_register_pkg AS

   PROCEDURE fill_detail_A1(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          (qj, equipment_verification, valve_verification, qj_locked, qj_redirect, crimping, pressure_buffer, h_dynamic, pressure,
                        amperage, loading, rotation_frequency, pressure_environment, pressure_engine, temperature_environment, temperature_engine
                      ) =
                (SELECT qj, equipment_verification, valve_verification, qj_locked, qj_redirect, crimping, pressure_buffer, h_dynamic, pressure,
                     amperage, loading, rotation_frequency, pressure_environment, pressure_engine, temperature_environment, temperature_engine
                   FROM tt_ticket_detail_a1
                  WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_A2(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( union_action_type, rock_frequency, ROTATION_FREQUENCY_DELTA, wheeling
                      ) =
                (SELECT union_action_type, rock_frequency, ROTATION_FREQUENCY_DELTA, wheeling
                   FROM tt_ticket_detail_a2
                  WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_A3(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( watering, kvch, multi_component, carbonate, inhibitor, volume
                      ) =
                (SELECT watering, kvch, multi_component, carbonate, inhibitor, volume
                   FROM tt_ticket_detail_a3
                  WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;


   PROCEDURE fill_detail_A4(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
            SET          ( puller_type_id,  voltage,  amperage,  loading,  rotation_frequency,  pressure_environment,  pressure_engine,  temperature_environment,  temperature_engine,  dmg_type, puller
                      ) =
                (SELECT puller_type_id,  voltage,  amperage,  loading,  rotation_frequency,  pressure_environment,  pressure_engine,  temperature_environment,  temperature_engine,  dmg_type, puller
                   FROM tt_ticket_detail_a4
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B1(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( start_time, treatment, repair_type, pump_id, pump_depth, damping_volume, weight, h_dynamic_minimal,
                        Q, H_STATIC, PRESSURE, PRESSURE_ENVIRONMENT, PRESSURE_ENGINE, TEMPERATURE_ENVIRONMENT,
                        TEMPERATURE_ENGINE, AMPERAGE, LOADING, ROTATION_FREQUENCY, H_DYNAMIC_15, PRESSURE_15, Q_15,
                        PRESSURE_ENVIRONMENT_15, PRESSURE_ENGINE_15, TEMPERATURE_ENVIRONMENT_15, 
                        TEMPERATURE_ENGINE_15, AMPERAGE_15, LOADING_15, ROTATION_FREQUENCY_15, H_DYNAMIC_30,
                        PRESSURE_30, Q_30, PRESSURE_ENVIRONMENT_30, PRESSURE_ENGINE_30, TEMPERATURE_ENVIRONMENT_30,
                        TEMPERATURE_ENGINE_30, AMPERAGE_30, LOADING_30, ROTATION_FREQUENCY_30
                      ) =
                (SELECT start_time, treatment, repair_type, pump_id, pump_depth, volume damping_volume, weight, h_dynamic_minimal,
                        Q, H_STATIC, PRESSURE, PRESSURE_ENVIRONMENT, PRESSURE_ENGINE, TEMPERATURE_ENVIRONMENT,
                        TEMPERATURE_ENGINE, AMPERAGE, LOADING, ROTATION_FREQUENCY, H_DYNAMIC_15, PRESSURE_15, Q_15,
                        PRESSURE_ENVIRONMENT_15, PRESSURE_ENGINE_15, TEMPERATURE_ENVIRONMENT_15, 
                        TEMPERATURE_ENGINE_15, AMPERAGE_15, LOADING_15, ROTATION_FREQUENCY_15, H_DYNAMIC_30,
                        PRESSURE_30, Q_30, PRESSURE_ENVIRONMENT_30, PRESSURE_ENGINE_30, TEMPERATURE_ENVIRONMENT_30,
                        TEMPERATURE_ENGINE_30, AMPERAGE_30, LOADING_30, ROTATION_FREQUENCY_30
                   FROM tt_ticket_detail_b1
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B2(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, volume, weight, called_agents, QJ_MANUAL_BEFORE, AMPERAGE_BEFORE, 
                        LOADING_BEFORE, ROTATION_FREQUENCY, ROTATION_CHANGE, 
                        PHASE_CHECK, H_STATIC_AFTER_STOP, CRIMPING_BEGIN, CRIMPING_END, CRIMPING_DURATION, 
                        H_STATIC_AFTER_CRIMPING, WASH_PRESSURE_BEGIN, WASH_PRESSURE_END, H_DYNAMIC_PRESSURE_AFTER_WASH, 
                        H_DYNAMIC_PRESSURE_DURATION, QJ_MANUAL_AFTER, AMPERAGE_AFTER, VOLTAGE_AFTER, 
                        PRESSURE_ENVIRONMENT_BEFORE, PRESSURE_ENGINE_BEFORE, 
                        TEMPERATURE_ENVIRONMENT_BEFORE, TEMPERATURE_ENGINE_BEFORE, PRESSURE_ENVIRONMENT_AFTER, 
                        PRESSURE_ENGINE_AFTER, TEMPERATURE_ENVIRONMENT_AFTER, TEMPERATURE_ENGINE_AFTER
                      ) =
                (SELECT time_completion, volume, weight, called_agents, QJ_MANUAL_BEFORE, AMPERAGE_BEFORE, 
                        LOADING_BEFORE, ROTATION_FREQUENCY, ROTATION_CHANGE, 
                        PHASE_CHECK, H_STATIC_AFTER_STOP, CRIMPING_BEGIN, CRIMPING_END, CRIMPING_DURATION, 
                        H_STATIC_AFTER_CRIMPING, WASH_PRESSURE_BEGIN, WASH_PRESSURE_END, H_DYNAMIC_PRESSURE_AFTER_WASH, 
                        H_DYNAMIC_PRESSURE_DURATION, QJ_MANUAL_AFTER, AMPERAGE_AFTER, VOLTAGE_AFTER, 
                        PRESSURE_ENVIRONMENT_BEFORE, PRESSURE_ENGINE_BEFORE, 
                        TEMPERATURE_ENVIRONMENT_BEFORE, TEMPERATURE_ENGINE_BEFORE, PRESSURE_ENVIRONMENT_AFTER, 
                        PRESSURE_ENGINE_AFTER, TEMPERATURE_ENVIRONMENT_AFTER, TEMPERATURE_ENGINE_AFTER
                   FROM tt_ticket_detail_b2
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B3(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, volume, weight, need_crimping, QJ_MANUAL_BEFORE, AMPERAGE_BEFORE, 
                        LOADING_BEFORE, ROTATION_FREQUENCY, PRESSURE_BEFORE, TEMPERATURE_BEFORE, 
                        H_STATIC_AFTER_STOP, CRIMPING_BEGIN, CRIMPING_END, CRIMPING_DURATION, 
                        H_STATIC_AFTER_CRIMPING, WASH_PRESSURE_BEGIN, WASH_PRESSURE_END, 
                        H_DYNAMIC_PRESSURE_AFTER_WASH, H_DYNAMIC_PRESSURE_DURATION, QJ_MANUAL_AFTER, 
                        AMPERAGE_AFTER, VOLTAGE_AFTER, PRESSURE_AFTER, TEMPERATURE_AFTER, 
                        PRESSURE_ENVIRONMENT_BEFORE, PRESSURE_ENGINE_BEFORE, TEMPERATURE_ENVIRONMENT_BEFORE, 
                        TEMPERATURE_ENGINE_BEFORE, PRESSURE_ENVIRONMENT_AFTER, PRESSURE_ENGINE_AFTER, 
                        TEMPERATURE_ENVIRONMENT_AFTER, TEMPERATURE_ENGINE_AFTER
                      ) =
                (SELECT time_completion, volume, weight, need_crimping, QJ_MANUAL_BEFORE, AMPERAGE_BEFORE, 
                        LOADING_BEFORE, ROTATION_FREQUENCY, PRESSURE_BEFORE, TEMPERATURE_BEFORE, 
                        H_STATIC_AFTER_STOP, CRIMPING_BEGIN, CRIMPING_END, CRIMPING_DURATION, 
                        H_STATIC_AFTER_CRIMPING, WASH_PRESSURE_BEGIN, WASH_PRESSURE_END, 
                        H_DYNAMIC_PRESSURE_AFTER_WASH, H_DYNAMIC_PRESSURE_DURATION, QJ_MANUAL_AFTER, 
                        AMPERAGE_AFTER, VOLTAGE_AFTER, PRESSURE_AFTER, TEMPERATURE_AFTER, 
                        PRESSURE_ENVIRONMENT_BEFORE, PRESSURE_ENGINE_BEFORE, TEMPERATURE_ENVIRONMENT_BEFORE, 
                        TEMPERATURE_ENGINE_BEFORE, PRESSURE_ENVIRONMENT_AFTER, PRESSURE_ENGINE_AFTER, 
                        TEMPERATURE_ENVIRONMENT_AFTER, TEMPERATURE_ENGINE_AFTER
                   FROM tt_ticket_detail_b3
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B4(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, volume, TREATMENT_DATEBEGIN, TREATMENT_DATEEND, QN_VOLUME_BEFORE_LOSS, 
                        QN_VOLUME_BEFORE_COLLECTOR, TEMPERATURE_INTERVAL, PRESSURE_ADPM_BEGIN, PRESSURE_ADPM_END, 
                        QJ_MANUAL, DMG_BEFORE
                      ) =
                (SELECT time_completion, volume, TREATMENT_DATEBEGIN, TREATMENT_DATEEND, QN_VOLUME_BEFORE_LOSS, 
                        QN_VOLUME_BEFORE_COLLECTOR, TEMPERATURE_INTERVAL, PRESSURE_ADPM_BEGIN, PRESSURE_ADPM_END, 
                        QJ_MANUAL, DMG_BEFORE
                   FROM tt_ticket_detail_b4
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B5(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, damping_volume, weight, cycles, PRESSURE_AFTER_DAMPING, LIQUID_PROBE, 
                        DAMPING_VOLUME_CYCLE_1, WEIGHT_CYCLE_1, DAMPING_VOLUME_CYCLE_2, WEIGHT_CYCLE_2, 
                        DAMPING_VOLUME_CYCLE_3, WEIGHT_CYCLE_3, DAMPING_VOLUME_CYCLE_4, WEIGHT_CYCLE_4
                      ) =
                (SELECT time_completion, volume DAMPING_VOLUME, weight, cycles, PRESSURE_AFTER_DAMPING, LIQUID_PROBE, 
                        DAMPING_VOLUME_CYCLE_1, WEIGHT_CYCLE_1, DAMPING_VOLUME_CYCLE_2, WEIGHT_CYCLE_2, 
                        DAMPING_VOLUME_CYCLE_3, WEIGHT_CYCLE_3, DAMPING_VOLUME_CYCLE_4, WEIGHT_CYCLE_4
                   FROM tt_ticket_detail_b5
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B6(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, preparing, MIXTURE
                      ) =
                (SELECT time_completion, preparing, MIXTURE
                   FROM tt_ticket_detail_b6
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B7(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, pressure_crimping
                      ) =
                (select time_completion, pressure_crimping
                   FROM tt_ticket_detail_b7
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B8(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
            SET       ( time_completion, requested_epu, requested_cnipr, H_DYNAMIC, PRESSURE, 
                        H_STATIC, PRESSURE_AFTER, POWER_OFF, PURGE, REQUEST_TYPE
                      ) =
                (select time_completion, requested_epu, requested_cnipr, H_DYNAMIC, PRESSURE, 
                        H_STATIC, PRESSURE_AFTER, POWER_OFF, PURGE, REQUEST_TYPE
                   FROM tt_ticket_detail_b8
               WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B9(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
            SET          ( time_completion, STOP_REASON, WORKED
                      ) =
                (select time_completion, reason, WORKED
                   FROM tt_ticket_detail_b9
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B10(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
            SET          ( mf_position, time_completion, reason
                      ) =
                (SELECT mf_position, time_completion, reason
                   FROM tt_ticket_detail_b10
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B11(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
            SET          ( time_completion, reason
                      ) =
                (SELECT time_completion, reason
                   FROM tt_ticket_detail_b11
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B12(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
            SET          ( time_completion, CONTRACTOR_ID, BRIGADE, reason
                      ) =
                (SELECT time_completion, CONTRACTOR_ID, BRIGADE, reason
                   FROM tt_ticket_detail_b12
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B13(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, h_dynamic, PRESSURE, AMPERAGE, VOLTAGE, 
                        LOADING, ROTATION_FREQUENCY, PROTECTION_CORRECTION, ROTATION_CHANGE, QJ_REVERSE, 
                        AMPERAGE_REVERSE, LOADING_REVERSE, PRESSURE_ENVIRONMENT, PRESSURE_ENGINE, 
                        TEMPERATURE_ENVIRONMENT, TEMPERATURE_ENGINE, Q
                      ) =
                (SELECT time_completion, h_dynamic, PRESSURE, AMPERAGE, VOLTAGE, 
                        LOADING, ROTATION_FREQUENCY, PROTECTION_CORRECTION, ROTATION_CHANGE, QJ_REVERSE, 
                        AMPERAGE_REVERSE, LOADING_REVERSE, PRESSURE_ENVIRONMENT, PRESSURE_ENGINE, 
                        TEMPERATURE_ENVIRONMENT, TEMPERATURE_ENGINE, Q
                   FROM tt_ticket_detail_b17
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_B18(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( time_completion, reason, ua, ktpn, sk, agzu, bg
                      ) =
                (SELECT time_completion, reason, ua, ktpn, sk, agzu, bg
                   FROM tt_ticket_detail_b18
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_c(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( brigade, start_time_plan, well_area, h_static, lock_available, service_area, balance_delta, PRESSURE, BRIGADE_TYPE
                      ) =
                (select brigade, start_time_plan, well_area, h_static, lock_available, service_area, balance_delta, PRESSURE, BRIGADE_TYPE
                   FROM tt_ticket_detail_c1
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_D1(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( repair_type_id, event_type, pump_id, pump_depth, qj, h_dynamic, pressure, amperage, loading, rotation_frequency,
                       pressure_engine, pressure_environment, temperature_engine, temperature_environment, duration, operation_mode
                      ) =
                (select repair_type_id, event_type, pump_id, pump_depth, qj, h_dynamic, pressure, amperage, loading, rotation_frequency,
                       pressure_engine, pressure_environment, temperature_engine, temperature_environment, duration, operation_mode
                   FROM tt_ticket_detail_d1
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_D2(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( repair_type_id, event_type, pump_id, pump_depth, qj, h_dynamic, pressure, amperage, loading, rotation_frequency,
                       pressure_engine, pressure_environment, temperature_engine, temperature_environment, duration, operation_mode
                      ) =
                (select repair_type_id, event_type, pump_id, pump_depth, qj, h_dynamic, pressure, amperage, loading, rotation_frequency,
                       pressure_engine, pressure_environment, temperature_engine, temperature_environment, duration, operation_mode
                   FROM tt_ticket_detail_d2
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_E1(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( probe_time
                      ) =
                (select probe_time
                   FROM tt_ticket_detail_e1
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_E2(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( probe_time
                      ) =
                (select probe_time
                   FROM tt_ticket_detail_e2
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_E3(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( probe_time, puller_type_id, voltage, amperage, loading, rotation_frequency, pressure_engine, pressure_environment, temperature_engine, temperature_environment, duration
                      ) =
                (select probe_time, puller_type_id, voltage, amperage, loading, rotation_frequency, pressure_engine, pressure_environment, temperature_engine, temperature_environment, duration
                   FROM tt_ticket_detail_e3
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_E4(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( treatment_time
                      ) =
                (select probe_time
                   FROM tt_ticket_detail_e4
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_E5(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( obtain_time
                      ) =
                (select probe_time
                   FROM tt_ticket_detail_e5
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

   PROCEDURE fill_detail_E6(p_detail_id IN NUMBER) IS
   BEGIN
      UPDATE tt_register
         SET          ( padding_time
                      ) =
                (select probe_time
                   FROM tt_ticket_detail_e6
            WHERE id = p_detail_id)
       WHERE detail_id = p_detail_id;
   END;

    PROCEDURE fill_detail_E7(p_detail_id IN NUMBER) IS
    BEGIN
        UPDATE tt_register SET (TIME_COMPLETION, H_DYNAMIC, PRESSURE) = (
            select time_completion, h_dynamic, pressure
            from tt_ticket_detail_e7 
            where id = p_detail_id
        ) WHERE detail_id = p_detail_id;
    END;
   
    PROCEDURE fill_detail_E8(p_detail_id IN NUMBER) IS
    BEGIN
        UPDATE tt_register SET (TIME_COMPLETION, SCRAPED, SCRAP_PROBLEM) = (
            select time_completion, scraped, scrap_problem
            from tt_ticket_detail_e8 
            where id = p_detail_id
        ) WHERE detail_id = p_detail_id;
    END;
   
   PROCEDURE fill_register (p_movement_id IN NUMBER)
   IS
      v_detail_id     tt_register.detail_id%TYPE;
      v_ticket_type   tt_register.ticket_type%TYPE;
   BEGIN
      delete from tt_register WHERE movement_id = p_movement_id;
      insert into tt_register (  ticket_id, ticket_type, ticket_type_num, ticket_type_code, ticket_type_name, department_id, department_name, department_display_name, created, completion,
                                 --
                                 movement_id, movement_membership_id, movement_membership_name, movement_position_id, movement_department_id, movement_department_name,
                                 movement_display_name, movement_status, movement_status_name, movement_created,
                                 movement_membership_first_id, movement_membership_first_name,
                                 movement_department_first_id, movement_department_first_name, movement_first_display_name,
                                 --
                                 detail_id, detail_well_id, detail_well, detail_field_code, detail_field_name, detail_cluster_code, detail_note, detail_sl, detail_membership_id,
                                 detail_expired, detail_requested, accepted, completed
                               )
               select
                       data_current.ticket_id,
                       ticket.ticket_type,
                       ticket.type as ticket_type_num,
                       ticket.code as ticket_type_code,
                       ticket.type_name as ticket_type_name,
                       ticket.department_id,
                       ticket.name as department_name,
                       ticket.display_name as department_display_name,
                       ticket.created,
                       DECODE (data_current.status, 9, data_current.created, null) as completion,
                       --
                       data_current.id as movement_id,
                       data_current.membership_id as movement_membership_id,
                       data_current.membership_name as movement_membership_name,
                       data_current.position_id as movement_position_id,
                       data_current.department_id as movement_department_id,
                       data_current.name as movement_department_name,
                       data_current.display_name as movement_display_name,
                       data_current.status as movement_status,
                       data_current.status_name as movement_status_name,
                       data_current.created as movement_created,
                       data_first.membership_id as movement_membership_first_id,
                       data_first.membership_name  as movement_membership_first_name,
                       data_first.department_id   as  movement_department_first_id,
                       data_first.name as movement_department_first_name,
                       data_first.display_name as movement_first_display_name,
                       --
                       data_current.ticket_detail_id as detail_id,
                       tdb.well_id as detail_well_id,
                       w.code as detail_well,
                       nvl(tdb.field_code, w.field_code) as detail_field_code,
                       nvl(tdb_f.name, w_f.name) as detail_field_name,
                       nvl(tdb.cluster_code, w.cluster_code) as detail_cluster_code,
                       tdb.note as detail_note,
                       tdb.sl as detail_sl,
                       tdb.membership_id as detail_membership_id,
                       tdb.expired as detail_expired,
                       tdb.requested as detail_requested,
                       data_accepted.created as accepted,
                       data_completed.created as completed
                 from ( select tm.ticket_id, tm.id, tm.ticket_detail_id, tm.membership_id,
                              m.lastname || DECODE ( TRIM(firstname), ' ', ' ' || SUBSTR(TRIM( firstname), 1, 1) || '.', '')
                                         || DECODE ( TRIM(patronym), ' ', ' ' || SUBSTR(TRIM( patronym), 1, 1) || '.', '') as membership_name,
                               tm.position_id, tm.department_id, d.name, d.display_name, tm.status, s.name as status_name, tm.created
                          from tt_ticket_movements tm
                          join tt_departments d on d.id = tm.department_id
                          join tt_memberships m on tm.membership_id = m.id
                          join tt_status s on  tm.status = s.status
                         where tm.deleted = 0
                           and tm.id = p_movement_id
                       ) data_current
                 join ( select t.id, t.ticket_type, n.type, n.code, n.name as type_name, t.department_id, d.name, d.display_name, t.created
                          from tt_tickets t
                          join tt_departments d on d.id = t.department_id
                          join tt_ticket_type n on n.ticket_type = t.ticket_type
                         where t.deleted = 0
                      ) ticket  on data_current.ticket_id = ticket.id
                 join (select ticket_id, tm.id, tm.membership_id,
                              m.lastname || DECODE ( TRIM(firstname), ' ', ' ' || SUBSTR(TRIM( firstname), 1, 1) || '.', '')
                                         || DECODE ( TRIM(patronym), ' ', ' ' || SUBSTR(TRIM( patronym), 1, 1) || '.', '') as membership_name,
                              d.id department_id, d.name, d.display_name, tm.deleted
                         from tt_ticket_movements tm
                         join tt_departments d on d.id = tm.department_id
                         join tt_memberships m on tm.membership_id = m.id
                         where tm.status = 0 and tm.deleted = 0
                      ) data_first on data_current.ticket_id = data_first.ticket_id and data_first.deleted = 0
                 left join (select ticket_id, tm.created
                         from tt_ticket_movements tm
                         join tt_departments d on d.id = tm.department_id
                         join tt_memberships m on tm.membership_id = m.id
                         where tm.status = 2 and tm.deleted = 0
                      ) data_accepted on data_current.ticket_id = data_accepted.ticket_id
                 left join (select ticket_id, tm.created
                         from tt_ticket_movements tm
                         join tt_departments d on d.id = tm.department_id
                         join tt_memberships m on tm.membership_id = m.id
                         where tm.status = 9 and tm.deleted = 0
                      ) data_completed on data_current.ticket_id = data_completed.ticket_id
                 left join tt_ticket_detail_base tdb on tdb.id = data_current.ticket_detail_id
                 left join tt_wells w on w.id = tdb.well_id
                 left join tt_fields w_f on w_f.code = w.field_code
                 left join tt_fields tdb_f on tdb_f.code = tdb.field_code;
            --returning ticket_type, detail_id into v_ticket_type, v_detail_id;

      SELECT ticket_type, detail_id
        INTO v_ticket_type, v_detail_id
        FROM tt_register
      WHERE movement_id = p_movement_id;

      CASE v_ticket_type
         WHEN 'A1' THEN fill_detail_A1(v_detail_id);
         WHEN 'A2' THEN fill_detail_A2(v_detail_id);
         WHEN 'A3' THEN fill_detail_A3(v_detail_id);
         WHEN 'A4' THEN fill_detail_A4(v_detail_id);
         WHEN 'B1' THEN fill_detail_B1(v_detail_id);
         WHEN 'B2' THEN fill_detail_B2(v_detail_id);
         WHEN 'B3' THEN fill_detail_B3(v_detail_id);
         WHEN 'B4' THEN fill_detail_B4(v_detail_id);
         WHEN 'B5' THEN fill_detail_B5(v_detail_id);
         WHEN 'B6' THEN fill_detail_B6(v_detail_id);
         WHEN 'B7' THEN fill_detail_B7(v_detail_id);
         WHEN 'B8' THEN fill_detail_B8(v_detail_id);
         WHEN 'B9' THEN fill_detail_B9(v_detail_id);
         WHEN 'B10' THEN fill_detail_B10(v_detail_id);
         WHEN 'B11' THEN fill_detail_B11(v_detail_id);
         WHEN 'B12' THEN fill_detail_B12(v_detail_id);
         WHEN 'B13' THEN fill_detail_B13(v_detail_id);
         WHEN 'C' THEN fill_detail_C(v_detail_id);
         WHEN 'D1' THEN fill_detail_D1(v_detail_id);
         WHEN 'D2' THEN fill_detail_D2(v_detail_id);
         WHEN 'E1' THEN fill_detail_E1(v_detail_id);
         WHEN 'E2' THEN fill_detail_E2(v_detail_id);
         WHEN 'E3' THEN fill_detail_E3(v_detail_id);
         WHEN 'E4' THEN fill_detail_E4(v_detail_id);
         WHEN 'E5' THEN fill_detail_E5(v_detail_id);
         WHEN 'E6' THEN fill_detail_E6(v_detail_id);
         WHEN 'E7' THEN fill_detail_E7(v_detail_id);
         WHEN 'E8' THEN fill_detail_E8(v_detail_id);
         ELSE
         BEGIN
            INSERT INTO TBERROR (MESSAGE, DT, DESCRIPTION)
            VALUES ('tt_register.calculate_register error, unknown ticket_type - ' || v_ticket_type, SYSDATE,  v_detail_id);
         END;
      END CASE;

      UPDATE tt_register
         SET time_completion = null
       WHERE time_completion = to_date('01/01/0001', 'DD/MM/YYYY')
         and movement_id = p_movement_id;

   EXCEPTION
   WHEN OTHERS THEN
      INSERT INTO TBERROR (MESSAGE, DT, DESCRIPTION)
        VALUES ('fill_detail_' || v_ticket_type, SYSDATE,  v_detail_id);
  end;

  procedure calculate_register is
  begin
    for v_rec in (select id from tt_ticket_movements where deleted = 0 and id not in (select distinct movement_id from tt_register))
    loop
      fill_register(v_rec.id);
    end loop;
  end;

  procedure full_recalc_register is
  begin

    for v_rec in (select distinct id from tt_ticket_movements where deleted = 0)
    loop
      fill_register(v_rec.id);
    end loop;
  end;

end tt_register_pkg;
/
