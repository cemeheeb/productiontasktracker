/* Formatted on 07.12.2018 15:40:32 (QP5 v5.227.12220.39754) */
UPDATE tt_positions
   SET department_id = 15
 WHERE id = 10003;

ALTER TABLE tt_departments ADD HEAD_ID NUMBER DEFAULT 2;

UPDATE tt_departments
   SET head_id = id
 WHERE id NOT IN (15, 16, 17, 18, 19, 20, 21, 22);

SELECT * FROM tt_ticket_detail_b1;

CREATE TABLE TT_TICKET_DETAIL_B1A
(
   ID                     NUMBER (15) CONSTRAINT TT_TD_B1A_ID NOT NULL,
   START_TIME             DATE CONSTRAINT TT_TD_B1A_ST NOT NULL,
   REQUEST_TYPE           VARCHAR2 (32 BYTE),
   START_TIME_FACT        DATE,
   H_STATIC               NUMBER (15, 3),
   PRESSURE               NUMBER (15, 3),
   PRESSURE_ENVIRONMENT   NUMBER (15, 3),
   PRESSURE_ENGINE        NUMBER (15, 3),
   H_STATIC_PIPE          NUMBER (15, 3),
   PRESSURE_PIPE          NUMBER (15, 3)
)
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TT_TD_B1A_PK
   ON TT_TICKET_DETAIL_B1A (ID)
   LOGGING
   NOPARALLEL;

ALTER TABLE TT_TICKET_DETAIL_B1A ADD (CONSTRAINT TT_TD_B1A_PK PRIMARY KEY (ID) USING INDEX TT_TD_B1A_PK ENABLE VALIDATE);



CREATE TABLE TT_TICKET_DETAIL_B1B
(
   ID               NUMBER (15) CONSTRAINT TT_TD_B1B_ID NOT NULL,
   START_TIME       DATE CONSTRAINT TT_TD_B1B_ST NOT NULL,
   TREATMENT        VARCHAR2 (32 BYTE),
   REPAIR_TYPE      VARCHAR2 (32 BYTE),
   PUMP_TYPESIZE    VARCHAR2 (32 BYTE),
   pump_depth       NUMBER(15, 3),
   volume           NUMBER(15, 3),
   weight           NUMBER(15, 3),
   UNION_DIAMETER   NUMBER (15, 3)
)
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TT_TD_B1B_PK
   ON TT_TICKET_DETAIL_B1B (ID)
   LOGGING
   NOPARALLEL;

ALTER TABLE TT_TICKET_DETAIL_B1B ADD (CONSTRAINT TT_TD_B1B_PK PRIMARY KEY (ID) USING INDEX TT_TD_B1B_PK ENABLE VALIDATE);

SELECT *
  FROM tt_ticket_routes
 WHERE ticket_class LIKE '%B%';

SELECT * FROM tt_register;

ALTER TABLE tt_register ADD pump_typesize VARCHAR(32 BYTE);
ALTER TABLE tt_register ADD union_diameter NUMBER(15, 3);

ALTER TABLE tt_register ADD start_time_fact DATE;
ALTER TABLE tt_register ADD h_static_pipe NUMBER(15, 3);
ALTER TABLE tt_register ADD pressure_pipe NUMBER(15, 3);

CALL tt_register_pkg.calculate_register ();

  SELECT *
    FROM tt_register
ORDER BY ticket_id DESC;

SELECT *
  FROM tt_ticket_detail_base
 WHERE id = 4756;


SELECT *
  FROM tt_tickets
 WHERE id = 1841;

DELETE FROM tt_ticket_detail_base
      WHERE ticket_id = 1837;

SELECT * FROM tt_ticket_detail_b1a order by id desc;

INSERT INTO tt_ticket_type (ticket_type,
                            TYPE,
                            code,
                            name)
     VALUES ('B1A',
             2,
             11,
             '������ �������� ��������');

INSERT INTO tt_ticket_type (ticket_type,
                            TYPE,
                            code,
                            name)
     VALUES ('B1B',
             2,
             12,
             '������ �������������� ��������');





alter table tt_ticket_detail_a1 add ground_equipment_verification varchar(256);
             
alter table tt_ticket_detail_a2 add union_dynamic NUMBER(15, 3);
alter table tt_ticket_detail_a2 add plunger_length NUMBER(15, 3);
alter table tt_ticket_detail_a2 add well_period_mode NUMBER(15, 3);
alter table tt_ticket_detail_a2 add analysis_oil_component NUMBER(1);
alter table tt_ticket_detail_a2 add analysis_complex NUMBER(1);

alter table tt_ticket_detail_a4 add gathering NUMBER(1);
alter table tt_ticket_detail_a4 add separate_expense NUMBER(15, 3);
alter table tt_ticket_detail_a4 add separate_humidity NUMBER(15, 3);
alter table tt_ticket_detail_a4 add separate_pressure NUMBER(15, 3);
alter table tt_ticket_detail_a4 add separate_temperature NUMBER(15, 3);

alter table tt_ticket_detail_b1 add well_purpose VARCHAR2(20);
alter table tt_ticket_detail_b1 add research VARCHAR2(10);
alter table tt_ticket_detail_b1 add pressure_ecn_minimal NUMBER(15, 3);

alter table tt_ticket_detail_b2 add washing NUMBER(15, 3);
alter table tt_ticket_detail_b2 add crimping NUMBER(15, 3);

alter table tt_ticket_detail_b3 add washing NUMBER(15, 3);
alter table tt_ticket_detail_b3 add crimping NUMBER(15, 3);







alter table tt_register add ground_equipment_verification varchar(256);
alter table tt_register add research varchar(10);
alter table tt_register add pressure_ecn_minimal NUMBER(15, 3);
             


alter table tt_register add well_purpose VARCHAR2(20);
alter table tt_register add union_dynamic NUMBER(15, 3);
alter table tt_register add plunger_length NUMBER(15, 3);
alter table tt_register add well_period_mode NUMBER(15, 3);
alter table tt_register add analysis_oil_component NUMBER(1);
alter table tt_register add analysis_complex NUMBER(1);

alter table tt_register add gathering NUMBER(1);
alter table tt_register add separate_expense NUMBER(15, 3);
alter table tt_register add separate_humidity NUMBER(15, 3);
alter table tt_register add separate_pressure NUMBER(15, 3);
alter table tt_register add separate_temperature NUMBER(15, 3);

alter table tt_register add washing NUMBER(15, 3);