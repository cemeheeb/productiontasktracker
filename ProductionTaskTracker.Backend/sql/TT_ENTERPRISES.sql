﻿SET DEFINE OFF;
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (176, 'PR0687', 'ТОО "Типография"', 'ТОО "Типография"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (177, 'PR0688', 'ФСТК "ЛУКТУР"', 'ФСТК "ЛУКТУР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (178, 'PR0689', 'ЗАО "КВАНТ-Х"', 'ЗАО "КВАНТ-Х"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (179, 'PR0690', 'СМУ-3', 'СМУ-3');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (180, 'PR0691', 'Нефтемонтаж', 'Нефтемонтаж');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (181, 'PR0692', 'НГКМ', 'НГКМ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (182, 'PR0693', 'ООО "СМУ-4"', 'ООО "СМУ-4"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (183, 'PR0694', 'СМУ-2', 'СМУ-2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (184, 'PR0695', 'ВолгоградНИПИнефть', 'ВолгоградНИПИнефть');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (185, 'PR0696', '"ГТНГ"', '"ГТНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (186, 'PR0697', 'ГИПроВосток-нефть', 'ГИПроВосток-нефть');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (187, 'PR0698', 'НИПИгазпереработка', 'НИПИгазпереработка');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (188, 'PR0699', 'ЗАО "НГФК"', 'ЗАО "НГФК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (189, 'PR0702', 'Трест "ТНГФ"', 'Трест "ТНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (190, 'PR0703', 'ПО "ТНГФ"', 'ПО "ТНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (191, 'PR0704', 'ГГП "ХГФ"', 'ГГП "ХГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (192, 'PR0705', 'ПГО "ХГФ"', 'ПГО "ХГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (193, 'PR0706', 'ЗСНГФ', 'ЗСНГФ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (194, 'PR0707', 'Трест "ТЮМНГФ"', 'Трест "ТЮМНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (195, 'PR0708', 'ПО "ТЮМНГФ"', 'ПО "ТЮМНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (196, 'PR0709', 'Трест "БНГФ"', 'Трест "БНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (197, 'PR0710', 'ПО "БНГФ"', 'ПО "БНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (198, 'PR0711', '"Востокбурвод"', '"Востокбурвод"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (199, 'PR0712', 'ПО "КНГ"', 'ПО "КНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (200, 'PR0713', 'ПО "ЛНГ"', 'ПО "ЛНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (201, 'PR0714', 'ПО "УНГ"', 'ПО "УНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (202, 'PR0715', 'Главтюменьгеология', 'Главтюменьгеология');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (203, 'PR0716', 'Главтюменьнефтегаз', 'Главтюменьнефтегаз');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (204, 'PR0717', 'Роскомгеология', 'Роскомгеология');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (205, 'PR0718', 'ЗСРГЦ', 'ЗСРГЦ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (206, 'PR0720', 'ХМГРЭ', 'ХМГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (207, 'PR0721', 'ВКНРЭ', 'ВКНРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (208, 'PR0722', 'ШНРЭ', 'ШНРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (209, 'PR0723', 'Конд.экспедиция', 'Конд.экспедиция');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (210, 'PR0724', 'Краснолен.эксп.', 'Краснолен.эксп.');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (211, 'PR0725', 'МД СЕЙС', 'МД СЕЙС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (212, 'PR0726', 'ТОО "Новые технологии"', 'ТОО "Новые технологии"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (213, 'PR0727', 'Повховское УБР', 'Повховское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (214, 'PR0730', 'БУБР', 'БУБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (215, 'PR0731', 'БУБР №1', 'БУБР №1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (216, 'PR0732', 'БУБР №2', 'БУБР №2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (217, 'PR0733', 'УУБР №2', 'УУБР №2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (218, 'PR0736', 'НУБР №1', 'НУБР №1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (219, 'PR0737', 'ЕУБР', 'ЕУБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (220, 'PR0738', 'Трест "СНГФ"', 'Трест "СНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (221, 'PR0739', 'Мозыр.эксп.', 'Мозыр.эксп.');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (222, 'PR0740', 'Полтавское УБР', 'Полтавское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (223, 'PR0741', 'УПНП и КРС', 'УПНП и КРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (224, 'PR0742', 'БНРЭ ГбЗС', 'БНРЭ ГбЗС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (225, 'PR0743', 'ШКБ', 'ШКБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (226, 'PR0745', 'УУБР ПО"УНГ"', 'УУБР ПО"УНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (227, 'PR0746', 'ШПГК', 'ШПГК');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (228, 'PR0747', 'УПГК', 'УПГК');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (229, 'PR0748', 'Трест"КНГФ"', 'Трест"КНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (230, 'PR0750', 'СУ-36', 'СУ-36');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (231, 'PR0751', 'СУ-75', 'СУ-75');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (232, 'PR0752', 'СМУ-5', 'СМУ-5');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (233, 'PR0753', 'ЦРТ НГДУ"ЛН"', 'ЦРТ НГДУ"ЛН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (234, 'PR0754', 'СМУ НГДУ', 'СМУ НГДУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (235, 'PR0755', 'ПМК-1 тр.ЛНС', 'ПМК-1 тр.ЛНС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (236, 'PR0756', 'ПМК-2 тр.ЛНС', 'ПМК-2 тр.ЛНС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (237, 'PR0757', 'ПМК-2 "Динамид"', 'ПМК-2 "Динамид"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (238, 'PR0758', '"Строитель"', '"Строитель"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (239, 'PR0759', 'СУ-8 "СТПС"', 'СУ-8 "СТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (240, 'PR0760', 'СУ-43 "МТПС"', 'СУ-43 "МТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (241, 'PR0761', 'МГА "Лан-Сиб"', 'МГА "Лан-Сиб"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (242, 'PR0762', 'АООТ "СтрСер"', 'АООТ "СтрСер"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (243, 'PR0763', '"УНИКОРД"', '"УНИКОРД"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (244, 'PR0764', 'МГА', 'МГА');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (245, 'PR0765', 'СМУ-12 "МНС"', 'СМУ-12 "МНС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (246, 'PR0766', 'КПТ-4 "МТПС"', 'КПТ-4 "МТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (247, 'PR0767', 'КПТ-5 "МТПС"', 'КПТ-5 "МТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (248, 'PR0768', 'КПТ-4 "СмТПС"', 'КПТ-4 "СмТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (249, 'PR0769', 'СМУ-1 "СТПС"', 'СМУ-1 "СТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (250, 'PR0770', 'СМУ-14 "СмТПС"', 'СМУ-14 "СмТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (251, 'PR0771', 'СМУ-14 "СТП+В119"', 'СМУ-14 "СТП+В119"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (252, 'PR0772', 'СМУ-43 "СмТПС"', 'СМУ-43 "СмТПС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (253, 'PR0773', 'ВИГАС', 'ВИГАС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (254, 'PR0774', 'РОСФЛЕКС', 'РОСФЛЕКС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (255, 'PR0775', 'ФАРВАТЕР', 'ФАРВАТЕР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (256, 'PR0776', 'СМУ-4 УФА', 'СМУ-4 УФА');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (257, 'PR0777', 'ЛИМБ', 'ЛИМБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (258, 'PR0778', 'НБС', 'НБС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (259, 'PR0779', '"СИБ-САГА"', '"СИБ-САГА"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (260, 'PR0780', 'ПМК-3', 'ПМК-3');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (261, 'PR0781', '"ГВН"', '"ГВН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (262, 'PR0782', 'ТюменНИИгипрогаз', 'ТюменНИИгипрогаз');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (263, 'PR0783', 'ТАТНЕФТЬ', 'ТАТНЕФТЬ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (264, 'PR0784', 'ОАО "ТАТНИПИнефть"', 'ОАО "ТАТНИПИнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (265, 'PR0785', 'ТАТНИИ', 'ТАТНИИ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (266, 'PR0786', '"ЛУКойл-ЛНГ"', '"ЛУКойл-ЛНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (267, 'PR0787', 'ООО «ЛУКОЙЛ-Инжиниринг»', 'ООО «ЛУКОЙЛ-Инжиниринг»');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (268, 'PR0788', '"СургутНИПИ"', '"СургутНИПИ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (269, 'PR0789', 'ЭГЭБ-1 МУБР', 'ЭГЭБ-1 МУБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (270, 'PR0790', '"ДИНАМИД"', '"ДИНАМИД"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (271, 'PR0791', 'СФ ГТНГ', 'СФ ГТНГ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (272, 'PR0792', 'ЮГНП', 'ЮГНП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (273, 'PR0793', '"СИБНЕФТЬ"', '"СИБНЕФТЬ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (274, 'PR0794', '"НЕФТЕРЕМОНТ"', '"НЕФТЕРЕМОНТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (275, 'PR0795', '"УзСИНКНЕФТЬ"', '"УзСИНКНЕФТЬ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (276, 'PR0796', 'ЛУХТП', 'ЛУХТП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (277, 'PR0797', 'ООО "НГД"', 'ООО "НГД"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (278, 'PR0799', 'Ремонт и ЛТД и Ко', 'Ремонт и ЛТД и Ко');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (279, 'PR0801', 'СИБНИИНП', 'СИБНИИНП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (280, 'PR0802', 'ГТЭ', 'ГТЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (281, 'PR0803', 'Нижневартовск НИПИ', 'Нижневартовск НИПИ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (282, 'PR0804', 'ЦЛГТНГ', 'ЦЛГТНГ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (283, 'PR0805', 'СибГеоЦентр', 'СибГеоЦентр');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (284, 'PR0806', 'АО "СибКОР"', 'АО "СибКОР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (285, 'PR0807', 'ЭИЦ', 'ЭИЦ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (286, 'PR0808', 'ВНИИГИК', 'ВНИИГИК');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (287, 'PR0809', 'ВНИГНИ', 'ВНИГНИ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (288, 'PR0810', 'ВНИИНефть', 'ВНИИНефть');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (289, 'PR0811', 'ВНИИНП', 'ВНИИНП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (290, 'PR0812', 'ПермНИПИнефть', 'ПермНИПИнефть');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (291, 'PR0813', 'ЗапСибНИИГеофизика', 'ЗапСибНИИГеофизика');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (292, 'PR0814', 'ОАО "ПермНИПИнефть"', 'ОАО "ПермНИПИнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (293, 'PR0815', 'ЗАО "Геофизмаш"', 'ЗАО "Геофизмаш"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (294, 'PR0816', 'АОЗТ "Синко-ремонт"', 'АОЗТ "Синко-ремонт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (295, 'PR0817', 'ООО "КрНГФ"', 'ООО "КрНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (296, 'PR0818', 'ТУПНП и КРС-3', 'ТУПНП и КРС-3');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (297, 'PR0819', 'НГДУ "Л-Н"', 'НГДУ "Л-Н"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (298, 'PR0825', 'ООО"Шаимгеонефть"', 'ООО"Шаимгеонефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (299, 'PR0826', 'КТУПО"Когалымнефтегаз"', 'КТУПО"Когалымнефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (300, 'PR0827', 'Белорусское УБР', 'Белорусское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (301, 'PR0611', 'КЗХ', 'КЗХ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (302, 'PR0204', 'УЭЭСиЭ "Урайэнергонефть"', 'УЭЭСиЭ "Урайэнергонефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (303, 'PR0607', 'ЗАО "ЛУКОЙЛ Петр.Сервис"', 'ЗАО "ЛУКОЙЛ Петр.Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (304, 'PR0629', '"Севернефтепереработка"', '"Севернефтепереработка"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (305, 'PR0655', '"Хантымансийскгеофизика"', '"Хантымансийскгеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (306, 'PR0656', 'ООО "ТНГ-Групп"', 'ООО "ТНГ-Групп"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (307, 'PR0678', '"ЛУКойл-Мегионгеология"', '"ЛУКойл-Мегионгеология"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (308, 'PR0798', '"Тюменнефтегеофизика"', '"Тюменнефтегеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (309, 'PR0920', 'исп.ГРП ТПП"ПНГ"(П,В)', 'исп.ГРП ТПП"ПНГ"(П,В)');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (310, 'PR0921', 'исп.ГРП ТПП"КНГ"(Т,Д)', 'исп.ГРП ТПП"КНГ"(Т,Д)');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (311, 'PR0922', 'исп.ГРП ТПП"КНГ"(Я)', 'исп.ГРП ТПП"КНГ"(Я)');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (312, 'PR0401', 'ТПП "ПНГ"', 'ТПП "ПНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (313, 'PR0828', 'ООО"Ватойлнефтедобыча"', 'ООО"Ватойлнефтедобыча"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (314, 'PR0617', 'OAO "HЛВ"', 'OAO "HЛВ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (315, 'PR0822', 'ЗАО "ГЕО-технология"', 'ЗАО "ГЕО-технология"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (316, 'PR0820', 'НГДУ "Урьевнефть"', 'НГДУ "Урьевнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (317, 'PR0821', 'ЗАО ПГО "ТПГ"', 'ЗАО ПГО "ТПГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (318, 'PR0823', 'УБНРЭ', 'УБНРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (319, 'PR0824', 'ООО "Прогресс"', 'ООО "Прогресс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (320, 'PR0345', 'ООО "КРС "Евразия"', 'ООО "КРС "Евразия"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (321, 'PR9018', 'Обуховский завод', 'Обуховский завод');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (322, 'PR0858', 'ООО "Пургеофизика"', 'ООО "Пургеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (323, 'PR0859', 'Тазовская неф.разв.эксп.', 'Тазовская неф.разв.эксп.');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (324, 'PR0860', 'ОАО "Тугра-нефть"', 'ОАО "Тугра-нефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (325, 'PR0889', 'ООО "Мегион-Сервис"', 'ООО "Мегион-Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (326, 'PR0103', 'НГДУ"ПН"', 'НГДУ"ПН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (327, 'PR0925', 'ЦИТС Ватьеган', 'ЦИТС Ватьеган');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (328, 'PR0926', 'ЦИТС Повх', 'ЦИТС Повх');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (329, 'PR0927', 'ЦИТС Ю.Ягун', 'ЦИТС Ю.Ягун');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (330, 'PR0928', 'ЦИТСы ТПП КНГ', 'ЦИТСы ТПП КНГ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (331, 'PR0929', 'Когалымский регион', 'Когалымский регион');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (332, 'PR0861', 'ЗАО "Геоварт"', 'ЗАО "Геоварт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (333, 'PR0890', 'АОЗТ "ТОЧМАШ"', 'АОЗТ "ТОЧМАШ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (334, 'PR9020', 'МНГМ', 'МНГМ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (335, 'PR9021', 'НПМ Тюмень', 'НПМ Тюмень');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (336, 'PR0123', 'ТК "Лангепас"', 'ТК "Лангепас"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (337, 'PR0124', 'СБ', 'СБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (338, 'PR0125', 'У и ЭСО', 'У и ЭСО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (339, 'PR0126', 'ПУТ и ОП', 'ПУТ и ОП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (340, 'PR0127', 'СХК "Агросервис"', 'СХК "Агросервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (341, 'PR0201', 'ТПП "Урайнефтегаз"', 'ТПП "УНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (342, 'PR0212', 'ТД', 'ТД');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (343, 'PR0213', 'Гостиница "Турсунт"', 'Гостиница "Турсунт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (344, 'PR0214', 'СБ', 'СБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (345, 'PR0217', 'УКС', 'УКС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (346, 'PR0305', 'ПЗ', 'ПЗ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (347, 'PR0323', 'СБ', 'СБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (348, 'PR0324', 'Редакция "НК"', 'Редакция "НК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (349, 'PR0326', 'КУЭРОГХ', 'КУЭРОГХ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (350, 'PR0328', 'УОП', 'УОП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (351, 'PR0329', 'ТК "Инфосервис"', 'ТК "Инфосервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (352, 'PR0403', 'СБ ТПП "ПНГ"', 'СБ ТПП "ПНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (353, 'PR0404', 'СХК "Агросервис"', 'СХК "Агросервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (354, 'PR0411', 'ТК "Ракурс"', 'ТК "Ракурс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (355, 'PR0602', 'ООО "ЛЗС"', 'ООО "ЛЗС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (356, 'PR0831', 'Управление "РЕМЕКОЛ"', 'Управление "РЕМЕКОЛ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (357, 'PR0832', 'ССУ', 'ССУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (358, 'PR0891', 'ООО "ГеоДейтаКонсалтинг"', 'ООО "ГеоДейтаКонсалтинг"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (359, 'PR0892', 'ООО "Геонефтегаз"', 'ООО "Геонефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (360, 'PR0893', 'ЗАО "ПНГРП"', 'ЗАО "ПНГРП"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (361, 'PR0894', 'ООО "ТГК"', 'ООО "ТГК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (362, 'PR0895', 'ОАО "ХМНГГ"', 'ОАО "ХМНГГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (363, 'PR0896', 'ОАО "Пайтых-Ойл"', 'ОАО "Пайтых-Ойл"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (364, 'PR0897', 'ОАО НПФ"Сейсм.технол"', 'ОАО НПФ"Сейсм.технол"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (365, 'PR0898', 'ООО "Геохим"', 'ООО "Геохим"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (366, 'PR0899', 'ООО "Техноком-Т"', 'ООО "Техноком-Т"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (367, 'PR0900', 'НИИГИГ ТюмГНГУ', 'НИИГИГ ТюмГНГУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (368, 'PR0901', 'УНГРЭ', 'УНГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (369, 'PR0993', 'ООО "БИС"', 'ООО "БИС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (370, 'PR0994', 'ООО "ПрогрессНефтеСервис"', 'ООО "ПрогрессНефтеСервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (371, 'PR0995', 'НПУ "РИТЭКБелоярскнефть"', 'НПУ "РИТЭКБелоярскнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (372, 'PR0996', 'НГДУ "РИТЭКХанты-мансийск', 'НГДУ "РИТЭКХанты-мансийск');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (373, 'PR0997', 'Ког.цех ТКРС "АРГОС"-ЧУРС', 'Ког.цех ТКРС "АРГОС"-ЧУРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (374, 'PR0998', 'ООО "НТС"', 'ООО "НТС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (375, 'PR0862', 'ОАО "Находканефтегаз"', 'ОАО "Находканефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (376, 'PR0863', 'ООО НК "ГЕОПЛАСТ"', 'ООО НК "ГЕОПЛАСТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (377, 'PR0930', 'ООО ЛЗС(консолидированное', 'ООО ЛЗС(консолидированное');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (378, 'PR0130', 'ООО "Л-П УРС"', 'ООО "Л-П УРС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (379, 'PR0230', 'ООО "УУРС"', 'ООО "УУРС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (380, 'PR0340', 'ООО "КУРС"', 'ООО "КУРС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (381, 'PR0902', 'УРС ООО "ЛЗС"', 'УРС ООО "ЛЗС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (382, 'PR0903', 'ООО "ЧУРС"', 'ООО "ЧУРС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (383, 'PR0904', 'ТФ ООО"КогалымНИПИнефть"', 'ТФ ООО"КогалымНИПИнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (384, 'PR0905', 'ООО "НК КНГ"', 'ООО "НК КНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (385, 'PR0906', 'КНГРЭ', 'КНГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (386, 'PR0907', 'ООО "КНГ-Сервис"', 'ООО "КНГ-Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (387, 'PR0908', 'ООО "ИНГС"', 'ООО "ИНГС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (388, 'PR0909', 'ЗАО "БКС"', 'ЗАО "БКС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (389, 'PR0910', 'Назымская НГРЭ', 'Назымская НГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (390, 'PR0911', 'ОАО "СУСС"', 'ОАО "СУСС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (391, 'PR0912', 'ОАО "ПНО"', 'ОАО "ПНО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (392, 'PR0913', 'ЗАО ГРК "СЭ"', 'ЗАО ГРК "СЭ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (393, 'PR0914', 'МиМГО', 'МиМГО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (394, 'PR0915', 'ЯГЭ', 'ЯГЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (395, 'PR0916', 'Пургеофизика', 'Пургеофизика');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (396, 'PR0917', 'ЭКСИС', 'ЭКСИС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (397, 'PR0918', 'НИЦ "Югранефтегаз"', 'НИЦ "Югранефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (398, 'PR0919', 'МНП "ГЕОДАТА"', 'МНП "ГЕОДАТА"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (399, 'PR0940', 'ГЕОПРОЕКТ', 'ГЕОПРОЕКТ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (400, 'PR0941', 'ООО НПП "Недра"', 'ООО НПП "Недра"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (401, 'PR0942', 'ПУБР', 'ПУБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (402, 'PR0943', 'ПНГ Бурение', 'ПНГ Бурение');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (403, 'PR0944', 'ТСНГРЭ', 'ТСНГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (404, 'PR0945', 'ООО "Тюменьгеофизика"', 'ООО "Тюменьгеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (405, 'PR0923', 'ЦИТС Дружное', 'ЦИТС Дружное');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (406, 'PR0924', 'ЦИТС Тевлин', 'ЦИТС Тевлин');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (407, 'PR0836', 'ЭГЭБ-4', 'ЭГЭБ-4');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (408, 'PR0218', 'УОП', 'УОП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (409, 'PR0219', 'ТРК "Спектр"', 'ТРК "Спектр"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (410, 'PR0837', 'ЗАО "ЭСТТ-нефть"', 'ЗАО "ЭСТТ-нефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (411, 'PR0864', 'ЗАО "НЕДРА-КОНСАЛТ"', 'ЗАО "НЕДРА-КОНСАЛТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (412, 'PR0865', 'ООО "ШАНСКо"', 'ООО "ШАНСКо"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (413, 'PR0946', 'ООО "РИДО"', 'ООО "РИДО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (414, 'PR0947', 'ООО "Геойлбент"', 'ООО "Геойлбент"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (415, 'PR0948', 'ООО "БК-Север"', 'ООО "БК-Север"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (416, 'PR0949', 'ОАО "ЯНГГ"', 'ОАО "ЯНГГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (417, 'PR0950', 'УГЭ', 'УГЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (418, 'PR0951', 'ЗАО "ЯНГГ" филиал УГЭ', 'ЗАО "ЯНГГ" филиал УГЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (419, 'PR0830', 'ОАО "Ямалгеофизика"', 'ОАО "Ямалгеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (420, 'PR0101', 'ТПП "ЛНГ"', 'ТПП "ЛНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (421, 'PR0833', 'ЗАО НПП "Гелий"', 'ЗАО НПП "Гелий"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (422, 'PR0834', 'ООО "Прогресс"', 'ООО "Прогресс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (423, 'PR0835', 'ЗАО "КрНГФ"', 'ЗАО "КрНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (424, 'PR0866', 'ЗАО СП "МеКаМинефть"', 'ЗАО СП "МеКаМинефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (425, 'PR0932', 'УУ ООО"ПНС"', 'УУ ООО"ПНС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (426, 'PR0933', 'ООО "СУМР"', 'ООО "СУМР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (427, 'PR0934', 'НФ ООО "СГК"', 'НФ ООО "СГК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (428, 'PR0935', 'УСО ООО "ЛЗС"', 'УСО ООО "ЛЗС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (429, 'PR0936', 'СЦПО ТПП "УНГ"', 'СЦПО ТПП "УНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (430, 'PR0937', 'ООО "УНГГ"', 'ООО "УНГГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (431, 'PR0938', 'ООО "Волганефтьсервис"', 'ООО "Волганефтьсервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (432, 'PR0939', 'ДООО"Бургаз"Ф"Тюменбургаз', 'ДООО"Бургаз"Ф"Тюменбургаз');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (433, 'PR0952', 'ЗАО НБК', 'ЗАО НБК');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (434, 'PR0953', 'ООО "Недра-Аудит"', 'ООО "Недра-Аудит"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (435, 'PR0954', 'ЯНГРЭ', 'ЯНГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (436, 'PR0955', 'ООО НПО "НГТ"', 'ООО НПО "НГТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (437, 'PR0601', 'СП и АО', 'СП и АО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (438, 'PR9019', 'Ремонт РКНМ', 'Ремонт РКНМ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (439, 'PR0867', 'ОАО "ЦГЭ"', 'ОАО "ЦГЭ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (440, 'PR0868', 'ФГУП "СНИИГГиМС"', 'ФГУП "СНИИГГиМС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (441, 'PR0869', 'ЗАО "НЕФТЕКОМ"', 'ЗАО "НЕФТЕКОМ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (442, 'PR0870', 'ООО "РНТЦ"', 'ООО "РНТЦ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (443, 'PR0871', 'ОАО НПЦ "Тверьгеофизика"', 'ОАО НПЦ "Тверьгеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (444, 'PR0872', 'ООО "НПО СибТехНефть"', 'ООО "НПО СибТехНефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (445, 'PR0873', 'Пар.Геофиз.Сервисз,ЛТД.', 'Пар.Геофиз.Сервисз,ЛТД.');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (446, 'PR0874', 'ООО "Комп.Дружба-инжен."', 'ООО "Комп.Дружба-инжен."');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (447, 'PR0875', 'АО "Актуальная геология"', 'АО "Актуальная геология"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (448, 'PR0876', 'ООО "ГеоПЕТРОЦЕНТР"', 'ООО "ГеоПЕТРОЦЕНТР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (449, 'PR0877', 'ООО "ГЕОВЕРС"', 'ООО "ГЕОВЕРС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (450, 'PR0878', 'ФГУП "ИГ и РГИ"', 'ФГУП "ИГ и РГИ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (451, 'PR0879', 'ООО НТПЦ "Сеноман"', 'ООО НТПЦ "Сеноман"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (452, 'PR0880', 'НИИМиМ им.Н.Г.Чеботарева', 'НИИМиМ им.Н.Г.Чеботарева');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (453, 'PR0881', 'ООО НПО "Интеграл-Х"', 'ООО НПО "Интеграл-Х"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (454, 'PR0882', '"НТЦ ПТ+"', '"НТЦ ПТ+"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (455, 'PR0883', 'ИПНЭ', 'ИПНЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (456, 'PR0956', 'ЮТС НГРЭ', 'ЮТС НГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (457, 'PR0957', 'ООО"Геология Резервуара"', 'ООО"Геология Резервуара"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (458, 'PR0838', 'ООО "ГСиНТ"', 'ООО "ГСиНТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (459, 'PR0839', 'ООО "Ремнефтесервис"', 'ООО "Ремнефтесервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (460, 'PR0884', 'ФГУП "ЗапСибГеоНАЦ"', 'ФГУП "ЗапСибГеоНАЦ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (461, 'PR0958', 'ООО"ЦСМРнефть"', 'ООО"ЦСМРнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (462, 'PR0959', 'ООО "Интегра-Сервисы"', 'ООО "Интегра-Сервисы"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (463, 'PR0960', 'ГУП ХМАО "ХМГГП"', 'ГУП ХМАО "ХМГГП"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (464, 'PR0961', 'НГК', 'НГК');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (465, 'PR0962', 'НГТ', 'НГТ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (466, 'PR0963', 'ХМГТ', 'ХМГТ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (467, 'PR0964', 'ЗСГУ', 'ЗСГУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (468, 'PR0965', 'ООО "Геошельф-С" ', 'ООО "Геошельф-С" ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (469, 'PR0966', 'ЗАО "БИКОМ"', 'ЗАО "БИКОМ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (470, 'PR0967', 'ООО "Геосейссервис"', 'ООО "Геосейссервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (471, 'PR0968', 'ООО "УАГС"', 'ООО "УАГС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (472, 'PR0969', '"НИПИнефтегаз"', '"НИПИнефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (473, 'PR0970', 'НИПИ "ИНПЕТРО"', 'НИПИ "ИНПЕТРО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (474, 'PR0971', 'Мегионская НРЭ', 'Мегионская НРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (475, 'PR0845', 'ЗАО НПЦ "СибГео"', 'ЗАО НПЦ "СибГео"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (476, 'PR0843', 'ЗАО "ИНЕФ"', 'ЗАО "ИНЕФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (477, 'PR0844', 'ЗАО "Арктикнефтегаз"', 'ЗАО "Арктикнефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (478, 'PR0846', 'ЗАО "ГЕОКОСМОС"', 'ЗАО "ГЕОКОСМОС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (479, 'PR0847', 'ЗАО "НИЦ "Югранефтегаз"', 'ЗАО "НИЦ "Югранефтегаз"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (480, 'PR0848', 'ОАО "Пурнефтегазгеология"', 'ОАО "Пурнефтегазгеология"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (481, 'PR0849', 'ОАО "БК "Пурнефтегазгеоло', 'ОАО "БК "Пурнефтегазгеоло');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (482, 'PR0850', 'ООО  "ЯмалГИС-Сервис"', 'ООО  "ЯмалГИС-Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (483, 'PR0851', 'ООО "Геоконтроль"', 'ООО "Геоконтроль"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (484, 'PR0852', 'ОАО "СибНАЦ"', 'ОАО "СибНАЦ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (485, 'PR9010', 'УралЛУКтрубмаш', 'УралЛУКтрубмаш');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (486, 'PR9011', 'Scott', 'Scott');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (487, 'PR9012', 'MVA Gmbh', 'MVA Gmbh');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (488, 'PR9013', 'TBS Trading Bisnes', 'TBS Trading Bisnes');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (489, 'PR9014', 'ОАО "ПНИТИ"', 'ОАО "ПНИТИ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (490, 'PR9015', 'Шуллер Блекман', 'Шуллер Блекман');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (491, 'PR9016', 'Тульский машзавод', 'Тульский машзавод');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (492, 'PR9017', 'Союэнефтеоборудование', 'Союэнефтеоборудование');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (493, 'PR0216', 'ДРСУ', 'ДРСУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (494, 'PR0853', 'УП "ПНГГ"', 'УП "ПНГГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (495, 'PR0854', 'ГНИГП"БелГЕО"', 'ГНИГП"БелГЕО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (496, 'PR0501', 'ТПП "ЯНГ"', 'ТПП "ЯНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (497, 'PR0885', 'ООО "СУМР"', 'ООО "СУМР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (498, 'PR0840', 'Oilwell Overseas CoLtd', 'Oilwell Overseas CoLtd');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (499, 'PR0841', 'ЗАО "СпецБУРИнвест"', 'ЗАО "СпецБУРИнвест"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (500, 'PR0842', 'НГКОАО"Ямалнефтегаздобыча', 'НГКОАО"Ямалнефтегаздобыча');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (501, 'PR0886', 'Филиал ООО "НБК" "ЗапСиб"', 'Филиал ООО "НБК" "ЗапСиб"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (502, 'PR9099', 'Прочие импортные', 'Прочие импортные');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (503, 'PR9000', 'история', 'история');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (504, 'PR9098', 'Прочие отечественные', 'Прочие отечественные');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (505, 'PR0855', 'НПЦ "Тюменьгеофизика"', 'НПЦ "Тюменьгеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (506, 'PR0856', 'ОАО "Ямал-ГИС"', 'ОАО "Ямал-ГИС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (507, 'PR0857', 'Пол.геофиз.эксп."ГлавТюГ"', 'Пол.геофиз.эксп."ГлавТюГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (508, 'PR0888', 'ЭНПЦ ООО "РИТЭК"', 'ЭНПЦ ООО "РИТЭК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (509, 'PR0887', 'ЗАО ПВП "АБС"', 'ЗАО ПВП "АБС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (510, 'PR0150', 'ООО "ИМСС"', 'ООО "ИМСС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (511, 'PR0251', 'ООО "ТИС" WFT', 'ООО "ТИС" WFT');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (512, 'PR0252', 'Baker Hughes', 'Baker Hughes');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (513, 'PR0399', 'HALLIBURTON', 'HALLIBURTON');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (514, 'PR0594', '"ЭРИЕЛЛ Нефтегазсервис"', '"ЭРИЕЛЛ Нефтегазсервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (515, 'PR1005', '"НИИНЕФТЕОТДАЧА"', '"НИИНЕФТЕОТДАЧА"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (516, 'PR1006', 'ЦНТУ "Наука"', 'ЦНТУ "Наука"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (517, 'PR1007', 'ИХН СО РАН', 'ИХН СО РАН');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (518, 'PR1008', 'ТОО "ПОМО"', 'ТОО "ПОМО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (519, 'PR1009', 'ОАО "Сибнефтегеофизика"', 'ОАО "Сибнефтегеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (520, 'PR1010', 'Туринская ГЭ', 'Туринская ГЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (521, 'PR0499', 'ООО "КСС"', 'ООО "КСС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (522, 'PR1011', 'ЯГФ-Восток', 'ЯГФ-Восток');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (523, 'PR0999', 'ТО ЦКР РОСНЕДРА по ЯНАО', 'ТО ЦКР РОСНЕДРА по ЯНАО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (524, 'PR1000', 'РОСНЕДРА', 'РОСНЕДРА');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (525, 'PR1001', 'ООО "ОНГФ"', 'ООО "ОНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (526, 'PR1002', 'ООО "ГП"-"ИГФ"', 'ООО "ГП"-"ИГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (527, 'PR1003', 'ООО НПФ "Бинар"', 'ООО НПФ "Бинар"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (528, 'PR1004', 'ОАО «ГЕОТЕК Сейсморазведка»', 'ОАО «ГЕОТЕК Сейсморазведка»');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (529, 'PR0341', 'ООО "Гидроимпульс"', 'ООО "Гидроимпульс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (530, 'PR0342', 'ООО "НТС"', 'ООО "НТС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (531, 'PR0343', 'ООО "АРГОС-СУМР"', 'ООО "АРГОС-СУМР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (532, 'PR0598', 'ООО "МЕГАЦЕНТР-ПЛЮС"', 'ООО "МЕГАЦЕНТР-ПЛЮС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (533, 'PR0599', 'Schlumberger', 'Schlumberger');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (534, 'PR0250', 'ООО "ТСГК"', 'ООО "ТСГК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (535, 'PR0344', 'ООО "БВС Евразия"', 'ООО "БВС Евразия"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (536, 'PR0596', 'ООО "Заполярстройресурс"', 'ООО "Заполярстройресурс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (537, 'PR0597', 'ООО "Обьнефтесервис"', 'ООО "Обьнефтесервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (538, 'PR0253', 'ООО НПЦ "Геостра"', 'ООО НПЦ "Геостра"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (539, 'PR0595', 'ООО "РАРИТЕТ"', 'ООО "РАРИТЕТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (540, 'PR0254', 'Филиал ЗАО "ССК" УЦС', 'Филиал ЗАО "ССК" УЦС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (541, 'PR0151', 'ЗАО "Универсалнефтеотдача"', 'ЗАО "Универсалнефтеотдача"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (542, 'PR0255', 'ООО "ЛАРГЕО"', 'ООО "ЛАРГЕО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (543, 'PR0593', 'ООО "Белоруснефть-Сибирь"', 'ООО "Белоруснефть-Сибирь"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (544, 'PR0389', 'ФГУП "ВСЕГЕИ"', 'ФГУП "ВСЕГЕИ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (545, 'PR0390', 'ОАО "Газпромнефть-ННГГФ"', 'ОАО "Газпромнефть-ННГГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (546, 'PR0396', 'ООО "РУ-Энерджи КРС-МГ"', 'ООО "РУ-Энерджи КРС-МГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (547, 'PR0397', 'ООО "Азимут ИТС"', 'ООО "Азимут ИТС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (548, 'PR0001', 'ТПП "ПовхНГ"', 'ТПП "ПовхНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (549, 'PR0391', 'ООО "ТЕХГЕОСЕРВИС"', 'ООО "ТЕХГЕОСЕРВИС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (550, 'PR0346', 'ФГБОУ ВПО УГНТУ', 'ФГБОУ ВПО УГНТУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (551, 'PR0347', 'ЗАО "НвБН"', 'ЗАО "НвБН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (552, 'PR0393', 'ЗАО "ТБС"', 'ЗАО "ТБС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (553, 'PR0256', 'ООО "Интегра-Сервисы"', 'ООО "Интегра-Сервисы"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (554, 'PR0152', 'ЗАО «СНПХ»', 'ЗАО «СНПХ»');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (555, 'PR0592', 'ООО "Транс - Технолоджи"', 'ООО "Транс - Технолоджи"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (556, 'PR0392', 'ООО НК "Мастер-нефть"', 'ООО НК "Мастер-нефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (557, 'PR0394', 'ООО "БСК Сокол"', 'ООО "БСК Сокол"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (558, 'PR0395', 'ЗАО МИПГУ "Химеко- Сервис"', 'ЗАО МИПГУ "Химеко- Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (559, 'PR0398', 'Scientific Drilling', 'Scientific Drilling');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (560, 'PR0591', 'ЗАО «Полярэкс»', 'ЗАО «Полярэкс»');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (561, 'PR0589', 'ООО "Норд-Сервис"', 'ООО "Норд-Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (562, 'PR0259', 'ООО «ТПБ»', 'ООО «ТПБ»');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (563, 'PR0412', 'ЗАО "Эмант"', 'ЗАО "Эмант"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (564, 'PR0257', 'УЭ ООО "СГК-Бурение"', 'УЭ ООО "СГК-Бурение"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (565, 'PR0258', 'ООО "СИА-ЛАБ"', 'ООО "СИА-ЛАБ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (566, 'PR0388', 'ООО "УдНГФ"', 'ООО "УдНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (567, 'PR0590', 'ООО "ФХС Поиск"', 'ООО "ФХС Поиск"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (568, 'PR0301', 'ТПП Когалымнефтегаз', 'ТПП КНГ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (0, 'PR0000', 'Предприятие не задано', 'Предприятие не задано');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (1, 'PR0972', 'ООО "Трайкан Велл Сервис"', 'ООО "Трайкан Велл Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (2, 'PR0973', 'Сев-Кавказ НИПИ', 'Сев-Кавказ НИПИ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (3, 'PR0974', 'ТСГЭ', 'ТСГЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (4, 'PR0976', 'ПГП2', 'ПГП2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (5, 'PR0977', 'NoyabrskNefteGaz', 'NoyabrskNefteGaz');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (6, 'PR0978', 'ЗАО "Глобалресорс"', 'ЗАО "Глобалресорс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (7, 'PR0979', 'Комитет по НГМР ХМАО', 'Комитет по НГМР ХМАО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (8, 'PR0980', 'ООО "БУТТ"', 'ООО "БУТТ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (9, 'PR0981', 'ОАО "ЮНГФ"', 'ОАО "ЮНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (10, 'PR0982', 'ООО "ГНПЦ ПурГео"', 'ООО "ГНПЦ ПурГео"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (11, 'PR0983', 'ООО "КатОбьнефть"', 'ООО "КатОбьнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (12, 'PR0984', 'ООО "НПРС-1"', 'ООО "НПРС-1"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (13, 'PR0985', 'ООО "Везерфорд"', 'ООО "Везерфорд"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (14, 'PR0986', 'ОАО НК "Паритет"', 'ОАО НК "Паритет"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (15, 'PR0987', 'ТННЦ', 'ТННЦ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (16, 'PR0988', 'ООО "ТЕРРА-проект"', 'ООО "ТЕРРА-проект"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (17, 'PR0989', 'ООО "ОТО Рекавери"', 'ООО "ОТО Рекавери"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (18, 'PR0990', 'ООО "ГЕОЭКОСЕРВИС"', 'ООО "ГЕОЭКОСЕРВИС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (19, 'PR0302', 'НГДУ "ПН"', 'НГДУ "ПН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (20, 'PR0307', 'НГДУ "ДН"', 'НГДУ "ДН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (21, 'PR0308', 'НГДУ "КН"', 'НГДУ "КН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (22, 'PR0327', 'УОиДСП', 'УОиДСП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (23, 'PR0603', 'ООО "ЮГРАнефть"', 'ООО "ЮГРАнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (24, 'PR0605', 'ЗАО "ЛУКОЙЛ-АИК"', 'ЗАО "ЛУКОЙЛ-АИК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (25, 'PR0610', 'HГДУ "PИTЭKHEФTЬ"', 'HГДУ "PИTЭKHEФTЬ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (26, 'PR0612', 'ООО " СНС" ', 'ООО " СНС" ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (27, 'PR0620', 'СП "Катконефть"', 'СП "Катконефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (28, 'PR0631', 'ЗAO "EГAHOЙЛ"', 'ЗAO "EГAHOЙЛ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (29, 'PR0640', 'ЗСФ ООО "БК" "Евразия"', 'ЗСФ ООО "БК" "Евразия"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (30, 'PR0673', 'КЭГБ-1', 'КЭГБ-1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (31, 'PR0991', 'Нарыкарская НРЭ', 'Нарыкарская НРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (32, 'PR0992', 'ФГУГП "Баженовская ГЭ"', 'ФГУГП "Баженовская ГЭ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (33, 'PR9001', 'Суруханский машзавод', 'Суруханский машзавод');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (34, 'PR9002', 'АО "ПКНМ"', 'АО "ПКНМ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (35, 'PR9003', 'SBS Driling Prod_Sys', 'SBS Driling Prod_Sys');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (36, 'PR9004', 'Bolland', 'Bolland');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (37, 'PR9005', 'Трайко индастриз инк', 'Трайко индастриз инк');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (38, 'PR9006', 'ОАО "Ижнефтемаш"', 'ОАО "Ижнефтемаш"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (39, 'PR9007', 'АО "ЭЛКАМ-нефтемаш"', 'АО "ЭЛКАМ-нефтемаш"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (40, 'PR9008', 'ООО СП "Аксельсон-Кубань"', 'ООО СП "Аксельсон-Кубань"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (41, 'PR9009', 'ЛУКОЙЛ-Бакы', 'ЛУКОЙЛ-Бакы');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (42, 'PR0202', 'НГДУ "УН"', 'НГДУ "УН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (43, 'PR0744', 'ЭГЭБ-3', 'ЭГЭБ-3');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (44, 'PR0749', 'УУБР №1', 'УУБР №1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (45, 'PR0637', 'ТПП ОАО МПК "АННГ"', 'ТПП ОАО МПК "АННГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (46, 'PR0641', 'Мирненское ЛУК-Бур', 'Мирненское ЛУК-Бур');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (47, 'PR0643', 'Сургутское УБР', 'Сургутское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (48, 'PR0644', 'Бавлинское УБР', 'Бавлинское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (49, 'PR0645', 'Бугульминское УБР', 'Бугульминское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (50, 'PR0646', 'МИНГЕО', 'МИНГЕО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (51, 'PR0647', 'БНЭ', 'БНЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (52, 'PR0648', 'Покачевское УБР', 'Покачевское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (53, 'PR0649', 'Мензелинское УБР', 'Мензелинское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (54, 'PR0650', 'Западно-Сибирское УБР', 'Западно-Сибирское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (55, 'PR0652', 'Новомолодежное УБР', 'Новомолодежное УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (56, 'PR0719', 'Мегионское УБР', 'Мегионское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (57, 'PR0728', 'Мирненское УБР', 'Мирненское УБР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (58, 'PR0729', 'МГРЭ', 'МГРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (59, 'PR0734', 'АНРЭ', 'АНРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (60, 'PR0735', 'СНРЭ', 'СНРЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (61, 'PR0104', 'ЦБПО по ЭПУ', 'ЦБПО по ЭПУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (62, 'PR0109', 'УРС', 'УРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (63, 'PR0110', 'УПНП и КРС-1', 'УПНП и КРС-1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (64, 'PR0111', 'УПНП и КРС-2', 'УПНП и КРС-2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (65, 'PR0113', 'УСПТГ', 'УСПТГ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (66, 'PR0114', 'ЦТБ', 'ЦТБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (67, 'PR0115', 'УПТО и КО', 'УПТО и КО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (68, 'PR0116', 'УТТ-1', 'УТТ-1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (69, 'PR0117', 'УТТ-2', 'УТТ-2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (70, 'PR0118', 'УТТ-3', 'УТТ-3');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (71, 'PR0119', 'УТТ-4', 'УТТ-4');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (72, 'PR0120', 'Покачевское УТТ', 'Покачевское УТТ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (73, 'PR0205', 'УГР', 'УГР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (74, 'PR0206', 'УТиК', 'УТиК');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (75, 'PR0215', 'УРС', 'УРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (76, 'PR0306', 'СМУ', 'СМУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (77, 'PR0313', 'УПНПиКРС', 'УПНПиКРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (78, 'PR0317', 'УПТОиКО', 'УПТОиКО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (79, 'PR0330', 'УРС', 'УРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (80, 'PR0332', 'УТТ-6', 'УТТ-6');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (81, 'PR0406', 'ЦБПО НПО', 'ЦБПО НПО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (82, 'PR0407', 'УРС', 'УРС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (83, 'PR0409', 'УТТ №1', 'УТТ №1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (84, 'PR0829', 'ООО "РЕМБУРСЕРВИС"', 'ООО "РЕМБУРСЕРВИС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (85, 'PR0102', 'НГДУ"ЛН"', 'НГДУ"ЛН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (86, 'PR0105', 'ПУ"АИСнефть"', 'ПУ"АИСнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (87, 'PR0106', 'УТН', 'УТН');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (88, 'PR0107', 'УЭЭС и Э', 'УЭЭС и Э');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (89, 'PR0108', 'НАЦ', 'НАЦ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (90, 'PR0112', 'ЛУМС', 'ЛУМС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (91, 'PR0121', 'Трест-площадка ЛНДСР', 'Трест-площадка ЛНДСР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (92, 'PR0122', 'Трест "ЛНС"', 'Трест "ЛНС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (93, 'PR0203', 'УЦБПО по ПРН и БО', 'УЦБПО по ПРН и БО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (94, 'PR0207', 'БПТО и КО №1', 'БПТО и КО №1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (95, 'PR0208', 'БПТО и КО №2', 'БПТО и КО №2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (96, 'PR0209', 'УТТ №1', 'УТТ №1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (97, 'PR0210', 'УТТ №2', 'УТТ №2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (98, 'PR0211', 'СУУТ', 'СУУТ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (99, 'PR0303', 'УТТ-3', 'УТТ-3');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (100, 'PR0304', 'ВУЭЭС и ЭО', 'ВУЭЭС и ЭО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (101, 'PR0309', 'УТТ-5', 'УТТ-5');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (102, 'PR0310', 'УТТ-1', 'УТТ-1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (103, 'PR0311', 'УТТ-2', 'УТТ-2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (104, 'PR0312', 'КНГП', 'КНГП');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (105, 'PR0314', 'ЦБПО ЭПУ', 'ЦБПО ЭПУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (106, 'PR0315', 'ЦБПО БНО', 'ЦБПО БНО');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (107, 'PR0316', 'ЦБПО ПиРНОСТ', 'ЦБПО ПиРНОСТ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (108, 'PR0318', 'ЦТБ', 'ЦТБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (109, 'PR0319', 'УТС', 'УТС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (110, 'PR0320', 'КНДСР', 'КНДСР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (111, 'PR0321', 'ПУ "КАСУН"', 'ПУ "КАСУН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (112, 'PR0322', 'УНИР', 'УНИР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (113, 'PR0325', 'ВТФ', 'ВТФ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (114, 'PR0331', 'ОИК ПФ "КогалымНИПИнефть"', 'ОИК ПФ "КогалымНИПИнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (115, 'PR0402', 'ДРСУ ТПП"ПНГ"', 'ДРСУ ТПП"ПНГ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (116, 'PR0405', 'ЦБПО и ЭПУ', 'ЦБПО и ЭПУ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (117, 'PR0408', 'ПУ "АИСнефть"', 'ПУ "АИСнефть"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (118, 'PR0410', 'УТТ №2', 'УТТ №2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (119, 'PR0604', 'ЗАО "Турсунт"', 'ЗАО "Турсунт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (120, 'PR0606', 'ЗАО "ГОЛОЙЛ"', 'ЗАО "ГОЛОЙЛ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (121, 'PR0608', 'АО "ЛУКОЙЛ-Татарстан"', 'АО "ЛУКОЙЛ-Татарстан"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (122, 'PR0609', '"ЗАО "РИТЭК - Прогресс"', '"ЗАО "РИТЭК - Прогресс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (123, 'PR0613', 'ЗАО "Евросиб"', 'ЗАО "Евросиб"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (124, 'PR0614', 'ОАО "ЛУКОЙЛ-Кубань"', 'ОАО "ЛУКОЙЛ-Кубань"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (125, 'PR0615', 'WOODBINE', 'WOODBINE');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (126, 'PR0616', 'ТОО "БИАЛ"', 'ТОО "БИАЛ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (127, 'PR0618', 'ООО "Когалым-Термо"', 'ООО "Когалым-Термо"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (128, 'PR0619', 'СК "ПетроАльянс"', 'СК "ПетроАльянс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (129, 'PR5001', 'ООО "ЕАЕ-Консалт"', 'ООО "ЕАЕ-Консалт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (130, 'PR5002', 'ООО "Урай НПО-Сервис"', 'ООО "Урай НПО-Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (131, 'PR0621', 'МД Сейс ПС', 'МД Сейс ПС');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (132, 'PR0622', 'ОАО "ОТО"', 'ОАО "ОТО"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (133, 'PR0623', 'АООТ "ЛУКОЙЛ-Юг"', 'АООТ "ЛУКОЙЛ-Юг"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (134, 'PR0624', 'ЗАО "ЛУКОЙЛ ЭПУ Сервис"', 'ЗАО "ЛУКОЙЛ ЭПУ Сервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (135, 'PR0625', 'WORCOVER', 'WORCOVER');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (136, 'PR0626', 'ОАО "Нефтестрой"', 'ОАО "Нефтестрой"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (137, 'PR0627', 'ООО "ТермНефтеРемонт"', 'ООО "ТермНефтеРемонт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (138, 'PR0628', 'АООТ "ЛТЕ"', 'АООТ "ЛТЕ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (139, 'PR0630', 'ЗСНТБ', 'ЗСНТБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (140, 'PR0632', '"БашНИПИ"', '"БашНИПИ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (141, 'PR0633', 'ОАО "Сибнефть-ННГГФ"', 'ОАО "Сибнефть-ННГГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (142, 'PR0634', 'КРС "ГН"', 'КРС "ГН"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (143, 'PR0635', 'КРС "WOODBINE"', 'КРС "WOODBINE"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (144, 'PR0636', 'ОАО "Обьнефтегазгеология"', 'ОАО "Обьнефтегазгеология"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (145, 'PR0642', 'Когалымское ЛУК-Бур', 'Когалымское ЛУК-Бур');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (146, 'PR0651', 'Урайское "ЛУК-БУР"', 'Урайское "ЛУК-БУР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (147, 'PR0653', 'ЭГЭБ-1', 'ЭГЭБ-1');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (148, 'PR0654', 'ЭГЭБ-2', 'ЭГЭБ-2');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (149, 'PR0657', 'ОАО "Покачевская УГР"', 'ОАО "Покачевская УГР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (150, 'PR0658', 'Мегионская ГФЭ', 'Мегионская ГФЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (151, 'PR0659', 'ОАО "КрУГР"', 'ОАО "КрУГР"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (152, 'PR0660', 'Сургутская ГФЭ', 'Сургутская ГФЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (153, 'PR0661', 'ОАО "ТНГФК"', 'ОАО "ТНГФК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (154, 'PR0662', 'АО "Сиал"', 'АО "Сиал"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (155, 'PR0663', 'ОАО "БашНГФК"', 'ОАО "БашНГФК"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (156, 'PR0664', 'ОАО "КНГФ"', 'ОАО "КНГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (157, 'PR0665', 'Лангепасское УГГФР', 'Лангепасское УГГФР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (158, 'PR0666', 'ЗАО "РТС"', 'ЗАО "РТС"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (159, 'PR0667', 'ООО"Урайнефтегеофизика"', 'ООО"Урайнефтегеофизика"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (160, 'PR0668', 'АОЗТ "Синко-ремонт"', 'АОЗТ "Синко-ремонт"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (161, 'PR0669', 'АООТ КРС"Сибирь-Прогресс"', 'АООТ КРС"Сибирь-Прогресс"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (162, 'PR0670', 'ОАО "ННГФ"', 'ОАО "ННГФ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (163, 'PR0671', 'УБГФЭ', 'УБГФЭ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (164, 'PR0672', 'Урайская ЭГБ', 'Урайская ЭГБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (165, 'PR0675', 'АОЗТ "Мегионгеология"', 'АОЗТ "Мегионгеология"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (166, 'PR0676', 'Урайское УГР', 'Урайское УГР');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (167, 'PR0677', 'ООО"ЛППпоКРНСТехносервис"', 'ООО"ЛППпоКРНСТехносервис"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (168, 'PR0679', 'ORS "OIL ОRSL"', 'ORS "OIL ОRSL"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (169, 'PR0680', 'ТОО "КБ"', 'ТОО "КБ"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (170, 'PR0681', 'АСНБ', 'АСНБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (171, 'PR0682', 'СНБ', 'СНБ');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (172, 'PR0683', 'КМБ "Лангепас"', 'КМБ "Лангепас"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (173, 'PR0684', 'Внешэкономбанк', 'Внешэкономбанк');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (174, 'PR0685', '"Междун.аэроп."Когалым"', '"Междун.аэроп."Когалым"');
Insert into TT_ENTERPRISES
   (id, code, name, display_name)
 Values
   (175, 'PR0686', 'АО "ЛУКОЙЛ-Авиа"', 'АО "ЛУКОЙЛ-Авиа"');
COMMIT;
