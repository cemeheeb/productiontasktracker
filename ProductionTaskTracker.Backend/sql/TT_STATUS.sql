﻿SET DEFINE OFF;
Insert into TT_STATUS
   (status, name)
 Values
   (0, 'Новая');
Insert into TT_STATUS
   (status, name)
 Values
   (1, 'Подано');
Insert into TT_STATUS
   (status, name)
 Values
   (2, 'Принято');
Insert into TT_STATUS
   (status, name)
 Values
   (3, 'Отклонено');
Insert into TT_STATUS
   (status, name)
 Values
   (4, 'Согласованно');
Insert into TT_STATUS
   (status, name)
 Values
   (5, 'В работе');
Insert into TT_STATUS
   (status, name)
 Values
   (6, 'Отработано');
Insert into TT_STATUS
   (status, name)
 Values
   (7, 'Подтвержденная');
Insert into TT_STATUS
   (status, name)
 Values
   (8, 'На доработке');
Insert into TT_STATUS
   (status, name)
 Values
   (9, 'Выполненная');
Insert into TT_STATUS
   (status, name)
 Values
   (12, 'Ознакомленная');
Insert into TT_STATUS
   (status, name)
 Values
   (13, 'Снятая');
COMMIT;
