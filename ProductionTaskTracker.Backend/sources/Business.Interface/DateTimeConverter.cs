﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business {
    public class DateTimeConverter: JsonConverter
{
    public override bool CanConvert(Type objectType) {
        return (objectType == typeof(DateTime));
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
        var date = (DateTime)value;
        writer.WriteValue(date.ToString("dd.MM.yyyy HH:mm"));
    }

    public override bool CanRead => true;

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            if (reader.TokenType != JsonToken.String) {
                throw new Exception($"Unexpected token parsing date. Expected Integer, got {reader.TokenType}.");
            }

            var provider = CultureInfo.InvariantCulture;
            return DateTime.ParseExact(reader.ReadAsString(), "dd.MM.yyyy HH:mm", provider); ;
        }
    }
}