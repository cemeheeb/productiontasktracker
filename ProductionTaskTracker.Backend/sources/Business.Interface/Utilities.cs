﻿using System;
using System.Text.RegularExpressions;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails;

namespace EAEConsult.ProductionTaskTracker.Backend.Business {
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Domain.Enumerables;
    using Domain.Entities;

    public static class TicketExtensions {
        public static string GetTicketClass(this Ticket ticket) {
            return Utilities.GetTicketClass(ticket.TicketType);
        }

        public static string GetTicketTypeDescription(this Ticket ticket) {
            return Utilities.GetTicketTypeDescription(ticket.TicketType);
        }
    }

    public static class Utilities {
        public static Type GetTicketTypeFromEnumerable(TicketType ticketType) {
            return Type.GetType($"EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails.TicketDetail{ticketType.ToString().ToUpper()}, EAEConsult.ProductionTaskTracker.Backend.Domain");
        }

        public static string PropertyToString(PropertyInfo property, object target) {
            switch (property.Name) {
                case "Agzu":
                case "BalanceDelta":
                case "Bg":
                case "Carbonate":
                case "Crimping":
                case "Dmg":
                case "DMGBefore":
                case "Inhibitor":
                case "Ktpn":
                case "Kvch":
                case "LiquidProbe":
                case "LockAvailable":
                case "Mixture":
                case "MultiComponent":
                case "NeedCrimping":
                case "PileFieldDemontage":
                case "PowerOff":
                case "Preparing":
                case "PressureAfterDamping":
                case "PumpCrimping":
                case "Purge":
                case "RequestedCNIPR":
                case "RequestedEPU":
                case "ServiceArea":
                case "Sk":
                case "Ua":
                case "Watering":
                case "Worked":
                    var value = property.GetValue(target) as bool?;
                    return GetBooleanDescription(value);
                case "Created":
                case "Accepted":
                case "Completed":
                case "Execution":
                case "ObtainTime":
                case "PaddingTime":
                case "ProbeTime":
                case "StartTime":
                case "StartTimePlan":
                case "TimeCompletion":
                case "TreatmentDateBegin":
                case "TreatmentDateEnd":
                case "TreatmentTime":
                    return $"{(DateTime)property.GetValue(target):dd.MM.yyyy HH:mm}";
                case "BrigadeType":
                    return GetBrigadeTypeDescription(property.GetValue(target).ToString());
                case "DMGType":
                    return GetDMGTypeDescription(property.GetValue(target).ToString());
                case "TicketType":
                    return GetTicketTypeDescription(property.GetValue(target).ToString());
                case "EquipmentVerification":
                case "ValveVerification":
                    return GetVerificationStateDescription(property.GetValue(target).ToString());
                case "UnionAction":
                    return GetUnionActionTypeDescription(property.GetValue(target).ToString());
                case "Treatment":
                    return GetTreatmentTypeDescription(property.GetValue(target).ToString());
                case "RepairType":
                    return GetWellRepairTypeDescription(property.GetValue(target).ToString());
                case "CalledAgents":
                    return GetOrder354AgentTypeDescription(property.GetValue(target).ToString());
                case "WellArea":
                    return GetWellAreaDescription(property.GetValue(target).ToString());
                case "EventType":
                    return GetWellEventTypeDescription(property.GetValue(target).ToString());
                case "StopReason":
                    return GetStopReasonDescription(property.GetValue(target).ToString());
                case "MovementStatus":
                    return GetTicketStatusDescription(property.GetValue(target).ToString());
                default:
                    return property.GetValue(target).ToString();
            }
        }


        public static string GetTicketClass(TicketType ticketType) {
            switch (ticketType) {
                case TicketType.A1:
                case TicketType.A2:
                case TicketType.A3:
                case TicketType.A4:
                    return "A";
                case TicketType.B1:
                case TicketType.B1A:
                case TicketType.B1B:
                case TicketType.B2:
                case TicketType.B3:
                case TicketType.B4:
                case TicketType.B5:
                case TicketType.B6:
                case TicketType.B7:
                case TicketType.B8:
                case TicketType.B9:
                case TicketType.B10:
                case TicketType.B11:
                case TicketType.B12:
                case TicketType.B13:
                case TicketType.B14:
                    return "B";
                case TicketType.C:
                    return "C";
                case TicketType.D1:
                case TicketType.D2:
                    return "D";
                case TicketType.E1:
                case TicketType.E2:
                case TicketType.E3:
                case TicketType.E4:
                case TicketType.E5:
                case TicketType.E6:
                case TicketType.E7:
                case TicketType.E8:
                    return "E";
                case TicketType.F:
                    return "F";
                case TicketType.G:
                    return "G";
                default:
                    return String.Empty;
            }
        }

        public static string GetTicketTypeDescription(string ticketType) {
            TicketType enumeration;
            Enum.TryParse(ticketType, out enumeration);
            return GetTicketTypeDescription(enumeration);
        }

        public static string GetTicketTypeDescription(TicketType? ticketType) {
            switch (ticketType) {
                case TicketType.A1:
                    return "Отклонение от режима работы";
                case TicketType.A2:
                    return "Изменение режима работы";
                case TicketType.A3:
                    return "Отбор проб";
                case TicketType.A4:
                    return "Снятие данных ТМС, ДМГ";
                case TicketType.B1:
                    return "Запуск скважины";
                case TicketType.B2:
                    return "Проведение мероприятий согласно приказам №354";
                case TicketType.B3:
                    return "Проведение промывки, опрессовки ГНО";
                case TicketType.B4:
                    return "Проведение обработки скважины горячей нефтью";
                case TicketType.B5:
                    return "Глушение скважины";
                case TicketType.B6:
                    return "Хим. обработка КЗХ";
                case TicketType.B7:
                    return "Оперссовка ГНО";
                case TicketType.B8:
                    return "Остановка скважины на исследование";
                case TicketType.B9:
                    return "Остановка скважины по технологическим причинам";
                case TicketType.B10:
                    return "Комиссионный разбор АГЗУ";
                case TicketType.B11:
                    return "Комиссионный разбор показаний СВУ";
                case TicketType.B12:
                    return "Вызов в бригаду (ТРС, КРС, Бурения, хим. Звена, партии ГИС)";
                case TicketType.B13:
                    return "Работа с ЭПУ";
                case TicketType.B14:
                    return "Осмотр оборудования";
                case TicketType.C:
                    return "Подготовка скважины под ТРС, КРС, ГИС, ЦНИПР, хим. Звено";
                case TicketType.D1:
                    return "Скважины ВНР";
                case TicketType.D2:
                    return "Скважины подконтрольные";
                case TicketType.E1:
                    return "Отбор проб на воду по графику";
                case TicketType.E2:
                    return "Отбор проб на КВЧ по графику";
                case TicketType.E3:
                    return "Снятие ТМС по графику";
                case TicketType.E4:
                    return "Проведение обработок скребком по графику";
                case TicketType.E5:
                    return "Снятие динамограмм по графику";
                case TicketType.E6:
                    return "Отбивка уровней по графику";
                case TicketType.E7:
                    return "Отбитие контрольного Нд";
                case TicketType.E8:
                    return "Прогнать контрольный скребок";
                case TicketType.F:
                    return "Информационная заявка от цеха в ЦИО";
                case TicketType.G:
                    return "Информационная заявка от ЦИО в цех";
                default:
                    return String.Empty;
            }
        }

        public static string GetTicketStatusDescription(string ticketStatus) {
            TicketStatus enumeration;
            Enum.TryParse(ticketStatus, out enumeration);
            return GetTicketStatusDescription(enumeration);
        }

        public static string GetTicketStatusDescription(TicketStatus? ticketStatus) {
            switch (ticketStatus) {
                case TicketStatus.Created:
                    return "Новая";
                case TicketStatus.Filed:
                    return "Поданная";
                case TicketStatus.Accepted:
                    return "Принятая";
                case TicketStatus.Rejected:
                    return "Отклоненная";
                case TicketStatus.Agreed:
                    return "Согласованная";
                case TicketStatus.Progress:
                    return "В работе";
                case TicketStatus.ProgressReady:
                    return "Отработано";
                case TicketStatus.ProgressConfirmed:
                    return "Подтвержденная - подтверждено линейным руководителем";
                case TicketStatus.ProgressRejected:
                    return "На доработке";
                case TicketStatus.Completed:
                    return "Выполненная - подтверждено руководителем";
                case TicketStatus.Cancelled:
                    return "Снятая";
                default:
                    return String.Empty;
            }
        }

        public static string GetBooleanDescription(bool? state) {
            switch (state) {
                case true:
                    return "Да";
                case false:
                    return "Нет";
            }
            return "";
        }

        public static string GetVerificationStateDescription(VerificationState? state) {
            switch (state) {
                case VerificationState.Accepted:
                    return "Подтверждено";
                case VerificationState.Rejected:
                    return "Отклонено";
            }

            return "";
        }

        public static string GetVerificationStateDescription(string state) {
            if (String.IsNullOrWhiteSpace(state)) {
                return "";
            }

            VerificationState verificationState;
            Enum.TryParse(state, out verificationState);

            return GetVerificationStateDescription(verificationState);
        }

        public static string GetUnionActionTypeDescription(UnionActionType? type) {
            switch (type) {
                case UnionActionType.Installed:
                    return "Установка";
                case UnionActionType.Replaced:
                    return "Снятие";
                case UnionActionType.Uninstalled:
                    return "Замена";
            }

            return "";
        }

        public static string GetUnionActionTypeDescription(string type) {
            if (String.IsNullOrWhiteSpace(type)) {
                return "";
            }

            UnionActionType unionActionType;
            Enum.TryParse(type, out unionActionType);
            
            return GetUnionActionTypeDescription(unionActionType);
        }

        public static string GetTreatmentTypeDescription(TreatmentType? type) {
            switch (type) {
                case TreatmentType.Chemical:
                    return "Хим. Звено";
                case TreatmentType.CurveLevelRepair:
                    return "Текущий ремонт";
                case TreatmentType.WellRepair:
                    return "ТиКРС";
            }

            return "";
        }

        public static string GetTreatmentTypeDescription(string type) {
            if (String.IsNullOrWhiteSpace(type)) {
                return "";
            }

            TreatmentType treatmentType;
            Enum.TryParse(type, out treatmentType);

            return GetTreatmentTypeDescription(treatmentType);
        }

        public static string GetWellRepairTypeDescription(WellRepairType? type) {
            switch (type) {
                case WellRepairType.Development:
                    return "Освоение";
                case WellRepairType.Full:
                    return "Капитальный ремонт";
                case WellRepairType.Support:
                    return "Текущий ремонт";
                case WellRepairType.Physical:
                    return "ФХМУН";
            }

            return "";
        }

        public static string GetWellRepairTypeDescription(string type) {
            if (String.IsNullOrWhiteSpace(type)) {
                return "";
            }

            WellRepairType repairType;
            Enum.TryParse(type, out repairType);
            
            return GetWellRepairTypeDescription(repairType);
        }

        public static string GetOrder354AgentTypeDescription(Order354AgentType? type) {
            switch (type) {
                case Order354AgentType.TKRS:
                    return "ТиКРС";
                case Order354AgentType.KNPO:
                    return "КНПО-С";
                case Order354AgentType.EPU:
                    return "ЭПУ";
            }

            return "";
        }

        public static string GetOrder354AgentTypeDescription(string type) {
            if (String.IsNullOrWhiteSpace(type)) {
                return "";
            }

            Order354AgentType agentType;
            Enum.TryParse(type, out agentType);

            return GetOrder354AgentTypeDescription(agentType);
        }

        public static string GetWellEventTypeDescription(WellEventType? type) {
            switch (type) {
                case WellEventType.Shift:
                    return "Смена";
                case WellEventType.Cleaning:
                    return "ОПЗ";
            }

            return "";
        }

        public static string GetWellEventTypeDescription(string type) {
            if (String.IsNullOrWhiteSpace(type)) {
                return "";
            }

            WellEventType wellEventType;
            Enum.TryParse(type, out wellEventType);

            return GetWellEventTypeDescription(wellEventType);
        }

        public static string GetStopReasonDescription(string value) {
            if (String.IsNullOrWhiteSpace(value)) {
                return "";
            }

            StopReason reason;
            Enum.TryParse(value, out reason);

            return GetStopReasonDescription(reason);
        }

        public static string GetStopReasonDescription(StopReason? reason) {
            switch (reason) {
                case StopReason.TKRS:
                    return "ТКРС";
                case StopReason.Other:
                    return "Прочие";
            }

            return "";
        }

        public static string GetWellAreaDescription(WellArea? type) {
            switch (type) {
                case WellArea.Backfill:
                    return "Отсыпка";
                case WellArea.Prepare:
                    return "Подготовка";
            }

            return "";
        }

        public static string GetWellAreaDescription(string value) {
            if (String.IsNullOrWhiteSpace(value)) {
                return "";
            }

            WellArea wellAreaType;
            Enum.TryParse(value, out wellAreaType);

            return GetWellAreaDescription(wellAreaType);
        }

        private static string GetBrigadeTypeDescription(string value) {
            if (String.IsNullOrWhiteSpace(value)) {
                return "";
            }

            BrigadeType type;
            Enum.TryParse(value, out type);

            return GetBrigadeTypeDescription(type);
        }
        public static string GetBrigadeTypeDescription(BrigadeType? type) {
            switch (type) {
                case BrigadeType.CNIPR:
                    return "ЦНИПР";
                case BrigadeType.Chemical:
                    return "Хим. звено";
                case BrigadeType.GIS:
                    return "ГИС";
                case BrigadeType.KRS:
                    return "КРС";
                case BrigadeType.TRS:
                    return "ТРС";
            }

            return "";
        }

        public static string GetDMGTypeDescription(string value) {
            if (String.IsNullOrWhiteSpace(value)) {
                return "";
            }

            DMGType type;
            Enum.TryParse(value, out type);

            return GetDMGTypeDescription(type);
        }

        public static string GetDMGTypeDescription(DMGType? type) {
            switch (type) {
                case DMGType.ValveType:
                    return "Тест клапанов";
                case DMGType.CyclesCounter:
                    return "Количество циклов";
            }

            return "";
        }

        public static int GetTimeDeltaSL(int level) {
            switch (level) {
                case 0: //SL1
                    return 3;
                case 1: //SL2
                    return 8;
                case 2: //SL3
                    return 24;
                default:
                    return 0;
            }
        }
    }

    public static class TicketMovementExtensions {
        public static string GetStatusDescription(this TicketMovement ticketMovement) {
            return Utilities.GetTicketStatusDescription(ticketMovement.Status);
        }
    }

    public static class TicketDetailExtensions {
        public static TicketType GetTicketType(this TicketDetailBase ticketDetail) {
            var regex = new Regex("TicketDetail(?<ticketType>.+)");
            var match = regex.Match(ticketDetail.GetType().Name);

            TicketType ticketType;
            if (!Enum.TryParse(match.Groups["ticketType"].Value, true, out ticketType) || ticketType == TicketType.Undefined) {
                throw new Exception("Неизвестный тип заявки");
            }

            return ticketType;
        }

        public static void SetExpiredAtNow(this TicketDetailBase ticketDetail) {
            if (!ticketDetail.SL.HasValue || ticketDetail.Ticket.IsNight) {
                return;
            }

            var timeDelta = Utilities.GetTimeDeltaSL(ticketDetail.SL.Value);
            var now = DateTime.Now;

            // Округление до минут
            now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);

            if (ticketDetail.SL.Value == 0) {
                ticketDetail.Expired = now.AddHours(timeDelta);
            } else {
                // В случае Sl2, SL3
                var nowBegin = now.Date.AddHours(8);
                var nowEnd = now.Date.AddHours(11).AddMinutes(30);

                if (now > nowEnd) {
                    ticketDetail.Expired = nowBegin.AddDays(1).AddHours(timeDelta);
                } else if (now > nowBegin) {
                    ticketDetail.Expired = now.AddHours(timeDelta);
                } else {
                    ticketDetail.Expired = nowBegin.AddHours(timeDelta);
                }
            }
        }

        public static void CopyRequestPropertiesFrom(this TicketDetailBase ticketDetail, TicketDetailBase ticketDetailTarget) {
            ticketDetail.SL = ticketDetailTarget.SL;
            ticketDetail.Expired = ticketDetailTarget.Expired;

            switch (ticketDetailTarget.GetTicketType()) {
                case TicketType.A1: {
                    var source = (TicketDetailA1) ticketDetailTarget;
                    var destination = (TicketDetailA1) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                } break;
                case TicketType.A2: {
                    var source = (TicketDetailA2) ticketDetailTarget;
                    var destination = (TicketDetailA2) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                } break;
                case TicketType.A3: {
                    var source = (TicketDetailA3) ticketDetailTarget;
                    var destination = (TicketDetailA3) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.ProbeVolume = destination.ProbeVolume ?? source.ProbeVolume;
                } break;
                case TicketType.A4: {
                    var source = (TicketDetailA4) ticketDetailTarget;
                    var destination = (TicketDetailA4) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                } break;
                case TicketType.B1: {
                        var source = (TicketDetailB1) ticketDetailTarget;
                        var destination = (TicketDetailB1) ticketDetail;
                        destination.Well = source.Well;
                        destination.StartTime = source.StartTime;
                        destination.Research = source.Research;
                        destination.Pump = source.Pump;
                        destination.PressureEcnMinimal = source.PressureEcnMinimal;
                    } break;
                case TicketType.B1A: {
                        var source = (TicketDetailB1A)ticketDetailTarget;
                        var destination = (TicketDetailB1A)ticketDetail;
                        destination.Well = source.Well;
                        destination.StartTime = source.StartTime;
                        destination.StartTimeFact = destination.StartTimeFact ?? source.StartTimeFact;
                        destination.RequestType = source.RequestType;
                    }
                    break;
                case TicketType.B1B: {
                        var source = (TicketDetailB1B)ticketDetailTarget;
                        var destination = (TicketDetailB1B)ticketDetail;
                        destination.Well = source.Well;
                        destination.StartTime = source.StartTime;
                        destination.Treatment = source.Treatment;
                        destination.RepairType = source.RepairType;
                        destination.PumpTypeSize = source.PumpTypeSize;
                        destination.PumpDepth = source.PumpDepth;
                        destination.DampingVolume = source.DampingVolume;
                        destination.DampingWeight = source.DampingWeight;
                        destination.UnionDiameter = source.UnionDiameter;
                    }
                    break;
                case TicketType.B2: {
                    var source = (TicketDetailB2) ticketDetailTarget;
                    var destination = (TicketDetailB2) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.WashVolume = source.WashVolume;
                    destination.Weight = source.Weight;
                    destination.CalledAgents = source.CalledAgents;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B3: {
                    var source = (TicketDetailB3) ticketDetailTarget;
                    var destination = (TicketDetailB3) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.NeedCrimping = destination.NeedCrimping ?? source.NeedCrimping;
                    destination.WashVolume = source.WashVolume;
                    destination.Weight = source.Weight;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B4: {
                    var source = (TicketDetailB4) ticketDetailTarget;
                    var destination = (TicketDetailB4) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.WashVolume = source.WashVolume;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                    }
                    break;
                case TicketType.B5: {
                    var source = (TicketDetailB5) ticketDetailTarget;
                    var destination = (TicketDetailB5) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.DampingVolume = source.DampingVolume;
                    destination.Weight = source.Weight;
                    destination.Cycles = source.Cycles;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B6: {
                    var source = (TicketDetailB6) ticketDetailTarget;
                    var destination = (TicketDetailB6) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                    }
                    break;
                case TicketType.B7: {
                    var source = (TicketDetailB7) ticketDetailTarget;
                    var destination = (TicketDetailB7) ticketDetail;
                    destination.Well = source.Well;
                    destination.PressureCrimping = source.PressureCrimping;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B8: {
                    var source = (TicketDetailB8) ticketDetailTarget;
                    var destination = (TicketDetailB8) ticketDetail;
                    destination.Well = source.Well;
                    destination.RequestType = source.RequestType;
                    destination.Requested = source.Requested;
                    destination.PowerOff = destination.PowerOff.HasValue ? destination.PowerOff : source.PowerOff;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B9: {
                    var source = (TicketDetailB9) ticketDetailTarget;
                    var destination = (TicketDetailB9) ticketDetail;
                    destination.Well = source.Well;
                    destination.StopReason = source.StopReason;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B10: {
                    var source = (TicketDetailB10) ticketDetailTarget;
                    var destination = (TicketDetailB10) ticketDetail;
                    destination.Field = source.Field;
                    destination.Cluster = source.Cluster;
                    destination.Shop = source.Shop;
                    destination.MeasureFacilityPosition = source.MeasureFacilityPosition;
                    destination.RequestReason = source.RequestReason;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B11: {
                    var source = (TicketDetailB11) ticketDetailTarget;
                    var destination = (TicketDetailB11) ticketDetail;
                    destination.Well = source.Well;
                    destination.RequestReason = source.RequestReason;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                    }
                break;
                case TicketType.B12: {
                    var source = (TicketDetailB12) ticketDetailTarget;
                    var destination = (TicketDetailB12) ticketDetail;
                    destination.Well = source.Well;
                    destination.Contractor = source.Contractor;
                    destination.RequestReason = source.RequestReason;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.B13: {
                    var source = (TicketDetailB13) ticketDetailTarget;
                    var destination = (TicketDetailB13) ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                } break;
                case TicketType.C: {
                    var source = (TicketDetailC)ticketDetailTarget;
                    var destination = (TicketDetailC)ticketDetail;
                    destination.Well = source.Well;
                    destination.Requested = source.Requested;
                }
                break;
                case TicketType.D1: {
                    var source = (TicketDetailD1) ticketDetailTarget;
                    var destination = (TicketDetailD1) ticketDetail;
                    destination.Well = source.Well;
                    destination.RepairType = source.RepairType;
                    destination.EventType = source.EventType;
                    destination.Pump = source.Pump;
                    destination.PumpDepth = source.PumpDepth;
                } break;
                case TicketType.D2: {
                    var source = (TicketDetailD2) ticketDetailTarget;
                    var destination = (TicketDetailD2) ticketDetail;
                    destination.Well = source.Well;
                    destination.RepairType = source.RepairType;
                    destination.EventType = source.EventType;
                    destination.Pump = source.Pump;
                    destination.PumpDepth = source.PumpDepth;
                } break;
                case TicketType.E1: {
                    var source = (TicketDetailE1) ticketDetailTarget;
                    var destination = (TicketDetailE1) ticketDetail;
                    destination.Well = source.Well;
                    destination.ProbeTime = destination.ProbeTime.HasValue ? destination.ProbeTime : source.ProbeTime;
                } break;
                case TicketType.E2: {
                    var source = (TicketDetailE2) ticketDetailTarget;
                    var destination = (TicketDetailE2) ticketDetail;
                    destination.Well = source.Well;
                    destination.ProbeTime = destination.ProbeTime.HasValue ? destination.ProbeTime : source.ProbeTime;
                } break;
                case TicketType.E3: {
                    var source = (TicketDetailE3) ticketDetailTarget;
                    var destination = (TicketDetailE3) ticketDetail;
                    destination.Well = source.Well;
                    destination.PullerType = source.PullerType;
                    destination.ProbeTime = destination.ProbeTime.HasValue ? destination.ProbeTime : source.ProbeTime;
                } break;
                case TicketType.E4: {
                    var source = (TicketDetailE4) ticketDetailTarget;
                    var destination = (TicketDetailE4) ticketDetail;
                    destination.Well = source.Well;
                    destination.TreatmentTime = destination.TreatmentTime.HasValue ? destination.TreatmentTime : source.TreatmentTime;
                } break;
                case TicketType.E5: {
                    var source = (TicketDetailE5) ticketDetailTarget;
                    var destination = (TicketDetailE5) ticketDetail;
                    destination.Well = source.Well;
                    destination.ObtainTime = destination.ObtainTime.HasValue ? destination.ObtainTime : source.ObtainTime;
                } break;
                case TicketType.E6: {
                    var source = (TicketDetailE6) ticketDetailTarget;
                    var destination = (TicketDetailE6) ticketDetail;
                    destination.Well = source.Well;
                    destination.PaddingTime = destination.PaddingTime.HasValue ? destination.PaddingTime : source.PaddingTime;
                } break;
                case TicketType.E7: {
                    var source = (TicketDetailE7)ticketDetailTarget;
                    var destination = (TicketDetailE7)ticketDetail;
                    destination.Well = source.Well;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                }
                break;
                case TicketType.E8: {
                    var source = (TicketDetailE8)ticketDetailTarget;
                    var destination = (TicketDetailE8)ticketDetail;
                    destination.Well = source.Well;
                    destination.TimeCompletion = destination.TimeCompletion > DateTime.MinValue ? destination.TimeCompletion : source.TimeCompletion;
                }
                break;
            }
        }
    }
}
