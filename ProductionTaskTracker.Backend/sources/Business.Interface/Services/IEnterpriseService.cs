﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Infrastructure.Models;
    using Models;

    public interface IEnterpriseService {
        EnterpriseModel Get(long id);

        EnterpriseModel Get(string code);

        PageContainerModel<EnterpriseModel> Search(string search, int index, int size);

        PageContainerModel<EnterpriseModel> Search(int index, int size);
    }
}
