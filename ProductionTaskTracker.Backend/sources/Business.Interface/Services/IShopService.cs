﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IShopService {
        IEnumerable<ShopModel> GetAll();

        ShopModel GetByCode(string code);
    }
}
