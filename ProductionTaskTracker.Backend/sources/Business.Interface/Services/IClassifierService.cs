﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IClassifierService {
        IEnumerable<ClassifierModel> Get(string dictionaryType, int index, int size, out int sizeTotal);
    }
}
