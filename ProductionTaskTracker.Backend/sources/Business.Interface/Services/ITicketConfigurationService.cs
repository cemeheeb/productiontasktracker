﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models.Configuration;

    public interface ITicketConfigurationService {
        IEnumerable<PropertyTypeModel> GetConfiguration();
    }
}
