﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Entities;
    using Models;

    public interface ITicketRouteService {
        IEnumerable<TicketRouteModel> GetTicketRouteMapJson(Position position);

        IEnumerable<Shop> GetShopsByDepartment(Department department);

        IEnumerable<Shop> GetShopsByDepartment(PositionRole positionRole, Department department);

    }
}
