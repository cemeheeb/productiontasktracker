﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Enumerables;
    using Domain.Entities;
    using Infrastructure.Models;
    using Models;

    public interface IControlledWellService {
        PageContainerModel<ControlledWellTableRecordModel> GetByFilter(ControlledWellFilter filter, int index, int size);

        ControlledWellTableRecordModel CreateControlledWell(Membership membership, Position position, ControlledWellCreateModel model);

        ControlledWellTableRecordModel UpdateControlledWell(long id, ControlledWellUpdateModel model);
    }
}
