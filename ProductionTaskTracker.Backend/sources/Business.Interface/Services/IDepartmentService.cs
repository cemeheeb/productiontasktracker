﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IDepartmentService {
        IEnumerable<DepartmentModel> GetAll();

        DepartmentModel Create(DepartmentModel model);

        void Update(DepartmentModel model);

        void Remove(long id);
    }
}
