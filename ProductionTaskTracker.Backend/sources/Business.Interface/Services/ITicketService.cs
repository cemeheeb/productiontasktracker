﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Entities;
    using Models;

    /// <summary>
    /// Служба управления заявками
    /// </summary>
    public interface ITicketService {
        /// <summary>
        /// Получение списка входящих заявок пользователя за период
        /// </summary>
        PageTicketRecordContainerModel GetIncoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size);

        /// <summary>
        /// Получение списка исходящих заявок пользователя за период
        /// </summary>
        PageTicketRecordContainerModel GetOutcoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size);

        /// <summary>
        /// Регистрация заявки в системе
        /// </summary>
        TicketTableRecordModel RegisterTicket(Membership membership, TicketRegistrationModel model);

        /// <summary>
        /// Пакетная регистрация заявки в системе
        /// </summary>
        IEnumerable<TicketTableRecordModel> RegisterTicketGroup(Membership membership, TicketRegistrationGroupModel model);
    }
}
