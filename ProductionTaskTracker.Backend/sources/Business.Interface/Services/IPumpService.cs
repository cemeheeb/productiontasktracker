﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Infrastructure.Models;
    using Models;

    public interface IPumpService {
        PageContainerModel<PumpModel> Search(string search, int index, int size);

        PumpModel Find(long id);

        void Import();
    }
}
