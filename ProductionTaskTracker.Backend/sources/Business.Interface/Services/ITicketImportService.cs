﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Entities.TicketDetails;
    using Domain.Entities;
    using Domain.Enumerables;

    public interface ITicketImportService {
        void GenerateDirective354Blank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails);
        void GenerateDirectiveVNRBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails);
        void GenerateDirectiveWarmTreatmentBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails);
        void GenerateDirectiveWashingBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails);
        void GenerateUniversalBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails);
    }
}
