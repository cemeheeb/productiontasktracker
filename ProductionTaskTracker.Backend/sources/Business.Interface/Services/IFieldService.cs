﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IFieldService {
        FieldModel Get(string fieldCode);

        IEnumerable<FieldModel> GetByShopCodes(string[] shopCodes);

        IEnumerable<FieldModel> GetAll();
    }
}
