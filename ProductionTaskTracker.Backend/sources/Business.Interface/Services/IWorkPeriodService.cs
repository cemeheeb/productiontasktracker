﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IWorkPeriodService {
        IEnumerable<WorkPeriodModel> GetActivePeriods();

        void BeginWorkPeriod(WorkPeriodBeginModel model);

        void EndWorkPeriod(long id);
    }
}
