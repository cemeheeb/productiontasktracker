﻿using EAEConsult.ProductionTaskTracker.Backend.Business.Models;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Entities;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    public interface IPositionService {
        IEnumerable<PositionModel> GetAll();
        PositionModel Create(PositionModel model);
        void Update(PositionModel model);
        void Remove(long id);
    }
}
