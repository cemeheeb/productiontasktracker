﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IRepairTypeService {
        IEnumerable<ClassifierModel> Get();
        IEnumerable<ClassifierModel> GetE();
        IEnumerable<ClassifierModel> GetS();
    }
}
