﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IHistoryService {
        IEnumerable<HistoryModel> GetHistory(string field, string cluster, string well);

        IEnumerable<HistoryModel> GetHistoryBetween(string field, string cluster, string well, DateTime dateBegin, DateTime dateEnd);

        IEnumerable<HistoryModel> GetHistoryByRemoteWellID(string remoteWellID);

        IEnumerable<HistoryModel> GetHistoryBetweenByRemoteWellID(string remoteWellID, DateTime dateBegin, DateTime dateEnd);
    }
}
