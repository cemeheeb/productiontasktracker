﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Entities;
    using Infrastructure.Models;
    using Models;

    public interface IWellRegularService {
        PageContainerModel<WellModel> Search(string search, IEnumerable<Department> department, int index, int size);

        PageContainerModel<WellModel> Search(string search, int index, int size);

        WellInformationModel GetWellInformation(long wellID);

        PageContainerModel<WellHistoryModel> GetHistory(long id, DateTime dateBegin, DateTime dateEnd, int index, int size);
    }
}