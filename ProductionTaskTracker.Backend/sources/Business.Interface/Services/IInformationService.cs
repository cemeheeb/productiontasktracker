﻿
namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Models;

    public interface IInformationService {
        DepartmentFullModel GetDepartment(long id);
    }
}