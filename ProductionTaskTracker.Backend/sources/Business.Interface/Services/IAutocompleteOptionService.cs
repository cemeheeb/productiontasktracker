﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Entities;
    using Models;

    public interface IAutocompleteOptionService {
        IEnumerable<AutocompleteOptionModel> Filter(Membership membership, string parameter);
    }
}
