﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {

    public class TicketHistoryContainerModel {
        [Display(Name = "История просмотров заявок")]
        [JsonProperty(PropertyName = "ticket_history")]
        public IEnumerable<TicketHistoryModel> TicketHistoryModels { get; set; }

        [Display(Name = "Идентификатор заявки")]
        [JsonProperty(PropertyName = "ticket_id")]
        public long TicketID { get; set; }
    }
}
