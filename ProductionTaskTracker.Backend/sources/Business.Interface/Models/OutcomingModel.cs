﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;
    using Domain.Enumerables;

    public class OutcomingModel {
        public OutcomingModel(Ticket ticket) {
            Id = ticket.Id;

            var ordered = ticket.TicketMovements.OrderBy(x => x.Id);
            var firstTicketMovement = ordered.First();
            var lastTicketMovement = ordered.Last();

            DepartmentIDFrom = firstTicketMovement.Position.Department.Id;
            DepartmentFrom = firstTicketMovement.Position.Department.FullName;

            DepartmentIDTo = ticket.Department.Id;
            DepartmentTo = ticket.Department.FullName;

            TicketType = ticket.TicketType;
            TicketTypeDescription = ticket.GetTicketTypeDescription();

            switch (ticket.TicketType) {
                // В случае если детали заявки содержат информацию по скважине
                case TicketType.A1:
                case TicketType.A2:
                case TicketType.A3: 
                case TicketType.A4: 
                case TicketType.B1:
                case TicketType.B1A:
                case TicketType.B1B:
                case TicketType.B2: 
                case TicketType.B3:
                case TicketType.B4:
                case TicketType.B5:
                case TicketType.B6:
                case TicketType.B7:
                case TicketType.B8:
                case TicketType.B9:
                case TicketType.B11:
                case TicketType.B12:
                case TicketType.B13:
                case TicketType.B14:
                case TicketType.D1:
                case TicketType.D2:
                case TicketType.E1:
                case TicketType.E2:
                case TicketType.E3:
                case TicketType.E4:
                case TicketType.E5:
                case TicketType.E6:
                case TicketType.E7:
                case TicketType.E8:
                {
                    var wellProperty = firstTicketMovement.TicketDetail.GetType().GetProperty("Well");
                    if (wellProperty != null) {
                        var well = wellProperty.GetValue(firstTicketMovement.TicketDetail, null) as WellRegular;
                        Field = well?.Field.Name;
                        FieldСode = well?.Field.Code;
                        Cluster = well?.Cluster;
                        Well = well?.Code;
                        WellID = well?.Id;
                    }

                    break;
                }
                // В случае если детали заявки содержат информацию о месторождении
                case TicketType.B10: {
                    var fieldProperty = firstTicketMovement.TicketDetail.GetType().GetProperty("Field");
                    if (fieldProperty != null) {
                        var field = fieldProperty.GetValue(firstTicketMovement.TicketDetail, null) as Field;
                        Field = field?.Name;
                        FieldСode = field?.Code;
                    }

                    var clusterProperty = firstTicketMovement.TicketDetail.GetType().GetProperty("Cluster");
                    if (clusterProperty != null) {
                        Cluster = clusterProperty.GetValue(firstTicketMovement.TicketDetail, null) as string;
                    }

                    break;
                }
            }

            TicketStatusDescription = lastTicketMovement.GetStatusDescription();
            Registered = ticket.Created;
        }

        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Тип")]
        [JsonProperty(PropertyName = "ticket_type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketType TicketType { get; set; }

        [Display(Name = "Вид работ")]
        [JsonProperty(PropertyName = "ticket_type_description")]
        public string TicketTypeDescription { get; set; }

        [Display(Name = "Название предприятия-заказчика")]
        [JsonProperty(PropertyName = "department_from")]
        public string DepartmentFrom { get; set; }

        [Display(Name = "Код предприятия-заказчика")]
        [JsonProperty(PropertyName = "department_id_from")]
        public long? DepartmentIDFrom { get; set; }

        [Display(Name = "Название предприятия-исполнителя")]
        [JsonProperty(PropertyName = "department_to")]
        public string DepartmentTo { get; set; }

        [Display(Name = "Код предприятия-исполнителя")]
        [JsonProperty(PropertyName = "department_id_to")]
        public long? DepartmentIDTo { get; set; }
        
        [Display(Name = "Название месторождение")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [Display(Name = "Код месторождения")]
        [JsonProperty(PropertyName = "field_сode")]
        public string FieldСode { get; set; }

        [Display(Name = "Куст")]
        [JsonProperty(PropertyName = "cluster")]
        public string Cluster { get; set; }

        [Display(Name = "Название скважины")]
        [JsonProperty(PropertyName = "well")]
        public string Well { get; set; }

        [Display(Name = "Код скважина")]
        [JsonProperty(PropertyName = "well_id")]
        public long? WellID { get; set; }

        [Display(Name = "Статус")]
        [JsonProperty(PropertyName = "ticket_status")]
        public TicketStatus TicketStatus { get; set; }

        [Display(Name = "Текущий статус заявки")]
        [JsonProperty(PropertyName = "ticket_status_description")]
        public string TicketStatusDescription { get; set; }

        [Display(Name = "Дата и время подачи заявки")]
        [JsonProperty(PropertyName = "registered")]
        public DateTime? Registered { get; set; }

        [Display(Name = "Срок выполнения заявки")]
        [JsonProperty(PropertyName = "completed")]
        public DateTime? Completed { get; set; }
    }
}
