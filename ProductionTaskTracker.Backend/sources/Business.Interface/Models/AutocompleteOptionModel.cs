﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class AutocompleteOptionModel {
        [Display(Name = "Значение параметра")]
        [JsonProperty(PropertyName = "text")]
        public virtual string Text { get; set; }

        [Display(Name = "Вес")]
        [JsonProperty(PropertyName = "weight")]
        public virtual int Weight { get; set; }
    }
}
