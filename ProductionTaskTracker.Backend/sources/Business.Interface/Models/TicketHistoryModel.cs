﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;
    using Domain.Enumerables;

    public class TicketHistoryModel {
        public TicketHistoryModel(TicketMovement movement, IEnumerable<TicketDetailVisitModel> visits) {
            Timestamp = movement.Created;
            Status = movement.Status;
            TicketDetail = AutoMapper.Mapper.Map<TicketDetailModel>(movement.TicketDetail);
            TicketDetail.DepartmentID = movement.TicketDetail.Ticket.Department.Id;
            TicketDetail.Status = (int)movement.Status;
            MembershipDisplayName = movement.Membership.DisplayName;
            TicketDetailVisits = visits;
        }

        [Display(Name = "Дата и время изменения")]
        [JsonProperty(PropertyName = "timestamp")]
        public DateTime Timestamp { get; set; }

        [Display(Name = "Статус")]
        [JsonProperty(PropertyName = "status")]
        public TicketStatus Status { get; set; }

        [Display(Name = "Детали заявки")]
        [JsonProperty(PropertyName = "ticket_detail")]
        public TicketDetailModel TicketDetail { get; set; }

        [Display(Name = "История просмотров заявок")]
        [JsonProperty(PropertyName = "ticket_detail_visits")]
        public IEnumerable<TicketDetailVisitModel> TicketDetailVisits { get; set; }

        [Display(Name = "ФИО пользователя")]
        [JsonProperty(PropertyName = "membership_display_name")]
        public string MembershipDisplayName { get; set; }
    }
}
