﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class FileModel {
        [Display(Name = "Имя файла")]
        [JsonProperty(PropertyName = "filename")]
        public string FileName { get; set; }

        [Display(Name = "Размер файла")]
        [JsonProperty(PropertyName = "filesize")]
        public long FileSize { get; set; }

        [Display(Name = "Путь к файлу на сервере")]
        [JsonProperty(PropertyName = "uri")]
        public string Uri { get; set; }
    }
}
