﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Enumerables;
    using Domain.Entities;

    public class ControlledWellCreateModel {
        public ControlledWellCreateModel() {
        }

        public ControlledWellCreateModel(ControlledWell entity) {
            Id = entity.Id;
            WellID = entity.Well.Id;
            TicketType = entity.TicketType;
            RepairType = entity.RepairType;
            EventType = entity.EventType;
            PumpID = entity.Pump.Id;
            PumpDepth = entity.PumpDepth;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Идентификатор скважины")]
        [JsonProperty(PropertyName = "well_id")]
        public long WellID { get; set; }

        [Display(Name = "Исполнитель")]
        [JsonProperty(PropertyName = "department_id")]
        public long DepartmentID { get; set; }

        [Display(Name = "Тип заявки")]
        [JsonProperty(PropertyName = "ticket_type")]
        public TicketType TicketType { get; set; }

        [Display(Name = "Идентификатор типа ремонта")]
        [JsonProperty(PropertyName = "repair_type")]
        public WellRepairType RepairType { get; set; }

        [Display(Name = "Тип мероприятия")]
        [JsonProperty(PropertyName = "event_type")]
        public WellEventType EventType { get; set; }

        [Display(Name = "Идентификатор насоса")]
        [JsonProperty(PropertyName = "pump_id")]
        public long PumpID { get; set; }

        [Display(Name = "Глубина спуска насоса")]
        [JsonProperty(PropertyName = "pump_depth")]
        public decimal PumpDepth{ get; set; }
    }
}
