﻿using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class TicketRegistrationModel : TicketDetailModel {
        // Отделение ответственное за ответ по заявке
        [JsonProperty(PropertyName = "department_id_to", NullValueHandling = NullValueHandling.Ignore)]
        public long DepartmentIDTo { get; set; }
    }
}
