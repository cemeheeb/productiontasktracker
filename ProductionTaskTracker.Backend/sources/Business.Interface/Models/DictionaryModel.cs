﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Infrastructure.Models;

    public class DictionaryModel: PageContainerModel<ClassifierModel> {
        public DictionaryModel(string dictionary, IEnumerable<ClassifierModel> data): base(data) {
            Dictionary = dictionary;
            SizeTotal = data.Count();
        }

        [Display(Name = "Идентификатор справочника")]
        [JsonProperty(PropertyName = "dictionary")]
        public string Dictionary { get; set; }
    }
}
