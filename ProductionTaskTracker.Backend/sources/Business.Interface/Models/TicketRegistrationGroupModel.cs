﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using System.Collections.Generic;
    using Domain.Enumerables;

    public class TicketRegistrationGroupModel : TicketDetailTargetModel {
        // Тип заявки
        [JsonProperty(PropertyName = "ticket_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketType TicketType { get; set; }

        // SL Уровень
        [JsonProperty(PropertyName = "sl", NullValueHandling = NullValueHandling.Ignore)]
        public int? SL { get; set; }

        // Ночная смена
        [JsonProperty(PropertyName = "is_night", NullValueHandling = NullValueHandling.Ignore)]
        public bool IsNight { get; set; }

        // Статус
        [JsonProperty(PropertyName = "status", NullValueHandling = NullValueHandling.Ignore)]
        public int Status { get; set; }

        // Отделение ответственное за ответ по заявке
        [JsonProperty(PropertyName = "department_id_to", NullValueHandling = NullValueHandling.Ignore)]
        public long DepartmentIDTo { get; set; }

        // Исполнитель
        [JsonProperty(PropertyName = "department_id", NullValueHandling = NullValueHandling.Ignore)]
        public long DepartmentID { get; set; }

        // Параметры
        [JsonProperty(PropertyName = "parameters", Required = Required.Always)]
        public List<TicketDetailModel> Parameters { get; set; }
    }
}
