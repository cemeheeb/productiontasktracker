﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Enumerables;

    public class ControlledWellUpdateModel {
        [Display(Name = "Идентификатор типа ремонта")]
        [JsonProperty(PropertyName = "repair_type")]
        public WellRepairType RepairType { get; set; }

        [Display(Name = "Тип мероприятия")]
        [JsonProperty(PropertyName = "event_type")]
        public WellEventType EventType { get; set; }

        [Display(Name = "Идентификатор насоса")]
        [JsonProperty(PropertyName = "pump_id")]
        public long PumpID { get; set; }

        [Display(Name = "Глубина спуска насоса")]
        [JsonProperty(PropertyName = "pump_depth")]
        public decimal PumpDepth{ get; set; }
    }
}
