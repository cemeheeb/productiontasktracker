﻿using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Enumerables;

    public class TicketMovementRegistrationGroupModel {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public long Id { get; set; }
        
        [JsonProperty(PropertyName = "status", NullValueHandling = NullValueHandling.Ignore)]
        public int Status { get; set; }

        [JsonProperty(PropertyName = "ticket_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketType TicketType { get; set; }

        [JsonProperty(PropertyName = "department_id", NullValueHandling = NullValueHandling.Ignore)]
        public long DepartmentID { get; set; }

        [JsonProperty(PropertyName = "parameters", NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<TicketDetailModel> Parameters { get; set; }
    }
}
