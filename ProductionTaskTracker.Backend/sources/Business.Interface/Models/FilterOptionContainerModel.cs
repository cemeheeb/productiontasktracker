﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.DTO;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Абстрактная модель-обертка реализуюшая механизм т.н. пагинации
    /// </summary>
    public class FilterOptionContainerModel: JObject {
        public FilterOptionContainerModel(IDictionary<string, IEnumerable<FilterOptionModel>> data) {
            foreach (var dataKey in data.Keys) {
                Add(new JProperty(dataKey, JArray.FromObject(data[dataKey])));
            }
        }

        public sealed override void Add(object content) {
            base.Add(content);
        }
    }
}