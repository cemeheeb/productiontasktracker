﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.DTO;
    using Domain.Enumerables;

    public class WellHistoryModel {
        public WellHistoryModel(RegisterRecord record) {
            Id = record.TicketID;
            Date = record.Created;
            Accepted = record.Accepted?.ToString("HH:mm") ?? "";
            Completed = record.Completed?.ToString("HH:mm") ?? "";
            Executor = record.DepartmentName;

            Actions = AggregateFilledParameters(record);
        }
        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Дата")]
        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }
        
        [Display(Name = "Принята в")]
        [JsonProperty(PropertyName = "accepted")]
        public string Accepted { get; set; }

        [Display(Name = "Выпонена в")]
        [JsonProperty(PropertyName = "completed")]
        public string Completed { get; set; }

        [Display(Name = "Исполнитель")]
        [JsonProperty(PropertyName = "executor")]
        public string Executor { get; set; }

        [Display(Name = "Принятые меры")]
        [JsonProperty(PropertyName = "actions")]
        public string Actions { get; set; }
    
        private string AggregateFilledParameters(RegisterRecord record) {
            var output = new StringBuilder();
            
            foreach (var property in typeof(RegisterRecord).GetProperties()) {
                var propertyName = property.Name;
                var propertyValue = property.GetValue(record);

                if (string.IsNullOrEmpty(propertyValue?.ToString())) {
                    continue;
                }

                switch (propertyName) {
                    case "Qj":
                        output.AppendFormat("Qж={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "QjLocked":
                        output.AppendFormat("Qж на закрытый ОК={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "QjRedirect":
                        output.AppendFormat("Qж через другой отвод={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "EquipmentVerification":
                        output.AppendFormat("{0}", Utilities.GetVerificationStateDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "ValveVerification":
                        output.AppendFormat("Обр.кл.={0}", Utilities.GetVerificationStateDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "PumpCrimping":
                        output.AppendFormat("{0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "PressureBuffer":
                        output.AppendFormat("Рбуф={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "HDynamic":
                        output.AppendFormat("Нд={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Pressure":
                        output.AppendFormat("Рзатр={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Voltage":
                        output.AppendFormat("U(В)={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Amperage":
                        output.AppendFormat("I(A)={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Loading":
                        output.AppendFormat("Загр.(%)={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "RotationFrequency":
                        output.AppendFormat("N (Об/мин)={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "PressureEnvironment":
                        output.AppendFormat("Рср={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "PressureEngine":
                        output.AppendFormat("Рмасла={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "TemperatureEnvironment":
                        output.AppendFormat("Тср={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "TemperatureEngine":
                        output.AppendFormat("Тмасла={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "UnionActionType":
                        output.AppendFormat("{0}", Utilities.GetUnionActionTypeDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "RockFrequency":
                        output.AppendFormat("Числ.кач.СК={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Wheeling":
                        output.AppendFormat("Изм.об.={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Watering":
                        output.AppendFormat("На обводненность={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Kvch":
                        output.AppendFormat("На содержание КВЧ={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "MultiComponent":
                        output.AppendFormat("6-и компонентные={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Carbonate":
                        output.AppendFormat("На карбонатную стабильностьc={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Inhibitor":
                        output.AppendFormat("На остаточное содержание ингибитора={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Volume":
                    case "WashVolume":
                        output.AppendFormat("V={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "VolumeBeforeCollector":
                        output.AppendFormat("t(до паден. Р)={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "VolumeBeforeLoss":
                        output.AppendFormat("t(от паден. Р до Рраб. Кол.))={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "PullerTypeName":
                        output.AppendFormat("{0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Dmg":
                        output.AppendFormat("Тип ДМГ={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Treatment":
                        output.AppendFormat("{0}", Utilities.GetTreatmentTypeDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "StartTime":
                    case "RepairType":
                    case "PumpName":
                    case "TimeCompletion":
                    case "OperationMode":
                    case "ProbeTime":
                        output.AppendFormat("{0:dd.MM.yyyy hh:mm}", propertyValue);
                        output.Append("; ");
                        break;
                    case "PumpDepth":
                        output.AppendFormat("Нсп={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Weight":
                        output.AppendFormat("Y={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "DampingVolume":
                        output.AppendFormat("Vгл.={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "HDynamicMinimal":
                        output.AppendFormat("Нд.мин={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "CalledAgents":
                        output.AppendFormat("{0}", Utilities.GetOrder354AgentTypeDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "Crimping":
                        output.AppendFormat("Опрессовать={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Cycles":
                        output.AppendFormat("Циклы={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Preparing":
                        output.AppendFormat("{0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "PressureCrimping":
                        output.AppendFormat("Р(опр.)={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "RequestedEPU":
                        output.AppendFormat("Вызов ЭПУ ({0})", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "RequestedCNIPR":
                        output.AppendFormat("Вызов ЦНИПР({0})", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Reason":
                        output.AppendFormat("Причина({0})", propertyValue);
                        output.Append("; ");
                        break;
                    case "MeasureFacilityPosition":
                        output.AppendFormat("АГЗУ={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Q":
                        output.AppendFormat("Qреж.={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "Ua":
                        output.AppendFormat("УА={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Ktpn":
                        output.AppendFormat("КТПН={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Sk":
                        output.AppendFormat("СК={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Agzu":
                        output.AppendFormat("АГЗУ={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Bg":
                        output.AppendFormat("БГ={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "Brigade":
                        output.AppendFormat("№бр.={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "StartTimePlan":
                        output.AppendFormat("Предп.вр.пост.бр.={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "WellArea":
                        output.AppendFormat("{0}", Utilities.GetWellAreaDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "HStatic":
                        output.AppendFormat("Нст={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "HStaticPipe":
                        output.AppendFormat("Нст в НКТ/в затрубе={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "PressurePipe":
                        output.AppendFormat("Ризб в НКТ/в затрубе={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "LockAvailable":
                        output.AppendFormat("Зап.устр.={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "ServiceArea":
                        output.AppendFormat("Пл.обсл.={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "BalanceDelta":
                        output.AppendFormat("ОГБ={0}", Utilities.GetBooleanDescription((bool)propertyValue));
                        output.Append("; ");
                        break;
                    case "RepairTypeName":
                        output.AppendFormat("{0}", Utilities.GetWellRepairTypeDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "EventType":
                        output.AppendFormat("{0}", Utilities.GetWellEventTypeDescription(propertyValue.ToString()));
                        output.Append("; ");
                        break;
                    case "Scraped":
                        output.AppendFormat("Обр.скр.={0}", propertyValue);
                        output.Append("; ");
                        break;
                    case "ScrapProblem":
                        output.AppendFormat("Осл.обр.={0}", propertyValue);
                        output.Append("; ");
                        break;
                }
            }

            if (!string.IsNullOrEmpty(record.DetailNote)) {
                output.Append(" ");
                output.Append(record.DetailNote);
            }

            return output.ToString();
        }
    }
}