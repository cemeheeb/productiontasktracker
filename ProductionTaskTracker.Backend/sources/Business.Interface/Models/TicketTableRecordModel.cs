﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.DTO;
    using Domain.Enumerables;
    using Newtonsoft.Json.Converters;

    public class TicketTableRecordModel {
        public TicketTableRecordModel() {
        }
        
        public TicketTableRecordModel(TicketTableRecord entity) {
            ID = entity.TicketGroupID ?? entity.TicketID ?? 0;
            TicketID = entity.TicketID;
            TicketType = entity.TicketType;
            TicketGroupID = entity.TicketGroupID;
            TicketStatus = entity.TicketStatus;
            DepartmentFrom = entity.DepartmentFrom;
            DepartmentIDFrom = entity.DepartmentIDFrom;
            DepartmentTo = entity.DepartmentTo;
            DepartmentIDTo = entity.DepartmentIDTo;
            Field = entity.Field;
            FieldCode = entity.FieldCode;
            Cluster = entity.Cluster;
            Well = entity.Well;
            WellID = entity.WellID;

            if (entity.Expired.HasValue) {
                IsExpired = DateTime.Now > entity.Expired && (!entity.Completed.HasValue || entity.Completed > entity.Expired);
            } else {
                IsExpired = false;
            }
            
            TicketDetailID = entity.TicketDetailID;
            SL = entity.SL;
            AllowUnregister = entity.AllowUnregister;
            Created = entity.Created;
            Expired = entity.Expired;
            IsNight = entity.IsNight;
            IsRepeated = entity.IsRepeated;
        }

        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long ID { get; set; }

        [Display(Name = "Идентификатор заявки")]
        [JsonProperty(PropertyName = "ticket_id")]
        public long? TicketID { get; set; }

        [Display(Name = "Идентификатор группы")]
        [JsonProperty(PropertyName = "ticket_group_id")]
        public long? TicketGroupID { get; set; }

        [Display(Name = "Тип")]
        [JsonProperty(PropertyName = "ticket_type")]
        public string TicketType { get; set; }

        [Display(Name = "Статус")]
        [JsonProperty(PropertyName = "status")]
        public int TicketStatus { get; set; }

        [Display(Name = "Название предприятия-заказчика")]
        [JsonProperty(PropertyName = "department_from")]
        public string DepartmentFrom { get; set; }

        [Display(Name = "Код предприятия-заказчика")]
        [JsonProperty(PropertyName = "department_id_from")]
        public long? DepartmentIDFrom { get; set; }

        [Display(Name = "Название предприятия-исполнителя")]
        [JsonProperty(PropertyName = "department_to")]
        public string DepartmentTo { get; set; }

        [Display(Name = "Код предприятия-исполнителя")]
        [JsonProperty(PropertyName = "department_id_to")]
        public long? DepartmentIDTo { get; set; }

        [Display(Name = "Название месторождение")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [Display(Name = "Код месторождения")]
        [JsonProperty(PropertyName = "field_сode")]
        public string FieldCode { get; set; }

        [Display(Name = "Куст")]
        [JsonProperty(PropertyName = "cluster")]
        public string Cluster { get; set; }

        [Display(Name = "Название скважины")]
        [JsonProperty(PropertyName = "well")]
        public string Well { get; set; }

        [Display(Name = "Код скважина")]
        [JsonProperty(PropertyName = "well_id")]
        public long? WellID { get; set; }

        [Display(Name = "С истекшим сроком")]
        [JsonProperty(PropertyName = "is_expired")]
        public bool IsExpired { get; set; }


        [Display(Name = "Детали заявки")]
        [JsonProperty(PropertyName = "ticket_detail_id")]
        public long TicketDetailID { get; set; }

        [Display(Name = "Дата и время подачи заявки")]
        [JsonProperty(PropertyName = "created")]
        public DateTime Created { get; set; }

        [Display(Name = "Срок выполнения заявки")]
        [JsonProperty(PropertyName = "expired", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Expired { get; set; }

        [Display(Name = "Уровень доступа")]
        [JsonProperty(PropertyName = "sl", NullValueHandling = NullValueHandling.Ignore)]
        public int? SL { get; set; }

        [Display(Name = "Разрешено снимать с регистрации")]
        [JsonProperty(PropertyName = "allow_unregister")]
        public bool AllowUnregister { get; set; }

        [Display(Name = "Признак ночной заявки")]
        [JsonProperty(PropertyName = "is_night")]
        public bool IsNight { get; set; }

        [Display(Name = "Признак повторной заявки")]
        [JsonProperty(PropertyName = "is_repeated")]
        public bool IsRepeated { get; set; }

        [JsonProperty(PropertyName = "behaviour", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketActionBehaviour Behaviour => TicketGroupID.HasValue ? TicketActionBehaviour.Group : TicketActionBehaviour.Single;
    }
}