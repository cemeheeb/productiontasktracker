﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class FieldModel {
        public FieldModel(Field entity) {
            Code = entity.Code;
            Name = entity.Name;
        }

        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Display(Name = "Название")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
