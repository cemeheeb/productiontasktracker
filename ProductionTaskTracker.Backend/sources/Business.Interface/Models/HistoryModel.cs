﻿using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class HistoryModel {
        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Дата")]
        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [Display(Name = "Насос")]
        [JsonProperty(PropertyName = "pump")]
        public string Pump { get; set; }

        [Display(Name = "U")]
        [JsonProperty(PropertyName = "u")]
        public string U { get; set; }

        [Display(Name = "I")]
        [JsonProperty(PropertyName = "i")]
        public string I { get; set; }

        [Display(Name = "Принята в")]
        [JsonProperty(PropertyName = "adopted")]
        public DateTime Adopted { get; set; }

        [Display(Name = "Принята на")]
        [JsonProperty(PropertyName = "adopted_by")]
        public string AdoptedBy { get; set; }

        [Display(Name = "Выпонена в")]
        [JsonProperty(PropertyName = "performed")]
        public DateTime Performed { get; set; }

        [Display(Name = "Принятые меры")]
        [JsonProperty(PropertyName = "actions")]
        public string Actions { get; set; }

        [Display(Name = "Исполнитель")]
        [JsonProperty(PropertyName = "performer")]
        public string Performer { get; set; }

        [Display(Name = "Исполнитель1")]
        [JsonProperty(PropertyName = "performer_1")]
        public string Performer1 { get; set; }

        [Display(Name = "Время выезда")]
        [JsonProperty(PropertyName = "timeout")]
        public DateTime TimeOut { get; set; }

        [Display(Name = "Время возврата")]
        [JsonProperty(PropertyName = "return_time")]
        public DateTime ReturnTime { get; set; }

        [Display(Name = "В базу внес")]
        [JsonProperty(PropertyName = "user_insert")]
        public string UserInsert { get; set; }

        [Display(Name = "Время прибытия")]
        [JsonProperty(PropertyName = "arrival_time")]
        public DateTime ArrivalTime { get; set; }

        [Display(Name = "Начала работ")]
        [JsonProperty(PropertyName = "start_works")]
        public DateTime StartWorks { get; set; }

        [Display(Name = "Время ожидания")]
        [JsonProperty(PropertyName = "weaiting_time")]
        public DateTime WaitingTime { get; set; }

        [Display(Name = "Вид работ")]
        [JsonProperty(PropertyName = "work_type")]
        public string WorkType { get; set; }
    }
}