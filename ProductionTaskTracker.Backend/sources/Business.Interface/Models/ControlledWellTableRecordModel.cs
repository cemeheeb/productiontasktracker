﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;
    using Domain.Enumerables;

    public class ControlledWellTableRecordModel {
        public ControlledWellTableRecordModel() {
        }

        public ControlledWellTableRecordModel(ControlledWell entity) {
            Id = entity.Id;
            TicketType = entity.TicketType;
            DepartmentID = entity.Executor.Id;
            WellID = entity.Well.Id;
            Well = entity.Well.Code;
            Field = entity.Well.Field.Name;
            FieldCode = entity.Well.Field.Code;
            Cluster = entity.Well.Cluster;
            RepairType = entity.RepairType;
            EventType = entity.EventType;
            PumpID = entity.Pump.Id;
            PumpDepth = entity.PumpDepth;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Тип заявки")]
        [JsonProperty(PropertyName = "ticket_type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketType TicketType { get; set; }
        
        [Display(Name = "Название месторождение")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [Display(Name = "Код месторождения")]
        [JsonProperty(PropertyName = "field_сode")]
        public string FieldCode { get; set; }

        [Display(Name = "Куст")]
        [JsonProperty(PropertyName = "cluster")]
        public string Cluster { get; set; }

        [Display(Name = "Название скважины")]
        [JsonProperty(PropertyName = "well")]
        public string Well { get; set; }

        [Display(Name = "Код скважины")]
        [JsonProperty(PropertyName = "well_id")]
        public long WellID { get; set; }

        [Display(Name = "Исполнитель")]
        [JsonProperty(PropertyName = "department_id")]
        public long DepartmentID { get; set; }
        
        [Display(Name = "Тип ремонта")]
        [JsonProperty(PropertyName = "repair_type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellRepairType RepairType { get; set; }

        [Display(Name = "Тип мероприятия")]
        [JsonProperty(PropertyName = "event_type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellEventType EventType { get; set; }

        [Display(Name = "Код насоса")]
        [JsonProperty(PropertyName = "pump_id")]
        public long PumpID { get; set; }

        [Display(Name = "Глубина спуска")]
        [JsonProperty(PropertyName = "pump_depth")]
        public decimal PumpDepth { get; set; }
    }
}
