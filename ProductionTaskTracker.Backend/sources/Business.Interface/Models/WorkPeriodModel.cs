﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class WorkPeriodModel {
        public WorkPeriodModel() {
        }

        public WorkPeriodModel(WorkPeriod entity) {
            ID = entity.Id;
            PositionID = entity.Position.Id;
            MembershipID = entity.Membership.Id;
            MembershipFullname = entity.Membership.FullName;
            DateBegin = entity.DateBegin;
            DateEnd = entity.DateEnd;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long ID { get; set; }

        [Display(Name = "Идентификатор пользователя")]
        [JsonProperty(PropertyName = "membership_id")]
        public long MembershipID { get; set; }

        [Display(Name = "Идентификатор должности")]
        [JsonProperty(PropertyName = "position_id")]
        public long PositionID { get; set; }

        [Display(Name = "ФИО пользователя")]
        [JsonProperty(PropertyName = "membership_fullname")]
        public string MembershipFullname { get; set; }

        [Display(Name = "Дата начала")]
        [JsonProperty(PropertyName = "date_begin")]
        public DateTime DateBegin { get; set; }

        [Display(Name = "Дата окончания")]
        [JsonProperty(PropertyName = "date_end")]
        public DateTime? DateEnd { get; set; }
    }
}
