﻿using System.ComponentModel.DataAnnotations;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class RegistrationModel {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [StringLength(128, ErrorMessage = "Значение поля {0} должно быть не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Введенный пароль и его подтверждение не совпадают.")]
        [Display(Name = "Подтверждение пароля")]
        public string PasswordConfirmation { get; set; }
    }
}