﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class PositionModel {
        public PositionModel(){
        }

        public PositionModel(Position entity) {
            Id = entity.Id;
            DepartmentID = entity.Department.Id;
            PositionRoleName = entity.Role.Name;
            Name = entity.Name;
            DisplayName = entity.DisplayName;
        }

        [Display(Name = "Идетификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Название для отображения")]
        [JsonProperty(PropertyName = "department_id")]
        public long? DepartmentID { get; set; }

        [Display(Name = "Название для отображения")]
        [JsonProperty(PropertyName = "position_role_name")]
        public string PositionRoleName { get; set; }

        [Display(Name = "Полное название должности")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [Display(Name = "Название для отображения")]
        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }
    }
}