﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Infrastructure.Models;

    /// <summary>
    /// Абстрактная модель-обертка реализуюшая механизм т.н. пагинации
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageTicketRecordContainerModel: PageContainerModel<TicketTableRecordModel> {
        public PageTicketRecordContainerModel(IEnumerable<TicketTableRecordModel> data, IDictionary<string, IEnumerable<FilterOptionModel>> filterOptionData): base(data) {
            FilterOptions = new FilterOptionContainerModel(filterOptionData);
        }

        [Display(Name = "Данные для фильтров")]
        [JsonProperty(PropertyName = "filter_options")]
        public FilterOptionContainerModel FilterOptions { get; }
    }
}