﻿using System;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class TicketRouteModel {
        public TicketRouteModel(TicketRoute entity) {
            TicketClass = entity.TicketClass;
            DepartmentIDFrom = entity.DepartmentFrom.Id;

            if (entity.StatusFrom.HasValue) {
                StatusFrom = (int)entity.StatusFrom.Value;
            }

            DepartmentIDTo = entity.DepartmentTo.Id;
            StatusTo = (int)entity.StatusTo;
            ExecutorID = entity.Executor.Id;
            PositionRole = entity.PositionRole;
            DateBegin = entity.DateBegin;
            DateEnd = entity.DateEnd;
        }

        [JsonProperty(PropertyName = "ticket_class")]
        public string TicketClass { get; set; }

        [JsonProperty(PropertyName = "department_id_from")]
        public long DepartmentIDFrom { get; set; }

        [JsonProperty(PropertyName = "status_from")]
        public int? StatusFrom { get; set; }

        [JsonProperty(PropertyName = "department_id_to")]
        public long DepartmentIDTo { get; set; }

        [JsonProperty(PropertyName = "status_to")]
        public int StatusTo { get; set; }

        [JsonProperty(PropertyName = "executor_id")]
        public long ExecutorID { get; set; }

        [JsonProperty(PropertyName = "position_role")]
        public string PositionRole { get; set; }

        [JsonProperty(PropertyName = "date_begin")]
        public DateTime DateBegin { get; set; }

        [JsonProperty(PropertyName = "date_end")]
        public DateTime? DateEnd { get; set; }
    }
}
