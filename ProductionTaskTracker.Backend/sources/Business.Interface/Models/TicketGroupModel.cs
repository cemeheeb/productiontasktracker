﻿using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using System.Collections.Generic;
    using Domain.Enumerables;
    using Newtonsoft.Json.Converters;

    public class TicketGroupModel {
        [JsonProperty(PropertyName = "id")]
        public long? ID { get; set; }

        [JsonProperty(PropertyName = "ticket_type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketType TicketType { get; set; }

        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }

        [JsonProperty(PropertyName = "department_id")]
        public long DepartmentID { get; set; }

        [JsonProperty(PropertyName = "wells")]
        public List<WellModel> Wells { get; set; }

        [JsonProperty(PropertyName = "parameters")]
        public List<TicketDetailModel> Parameters { get; set; }

        [JsonProperty(PropertyName = "sl")]
        public int? SL { get; set; }

        [JsonProperty(PropertyName = "is_night")]
        public bool IsNight { get; set; }
    }
}
