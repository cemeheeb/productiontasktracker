﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class EnterpriseModel {
        public EnterpriseModel() {
        }

        public EnterpriseModel(Enterprise entity) {
            Id = entity.Id;
            Code = entity.Code;
            Name = entity.Name;
            DisplayName = entity.DisplayName;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Display(Name = "Название")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [Display(Name = "Сокращенное название")]
        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }
    }
}
