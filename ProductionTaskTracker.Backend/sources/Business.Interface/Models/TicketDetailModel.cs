﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Enumerables;

    public class TicketDetailModel : TicketDetailTargetModel {
        [JsonProperty(PropertyName = "need_crimping", NullValueHandling = NullValueHandling.Ignore)]
        public bool? NeedCrimping { get; set; }

        [JsonProperty(PropertyName = "crimping_begin", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CrimpingBegin { get; set; }

        [JsonProperty(PropertyName = "crimping_end", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CrimpingEnd { get; set; }

        [JsonProperty(PropertyName = "crimping_duration", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CrimpingDuration { get; set; }

        [JsonProperty(PropertyName = "h_static_after_crimping", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HStaticAfterCrimping { get; set; }

        [JsonProperty(PropertyName = "wash_pressure_begin", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? WashPressureBegin { get; set; }

        [JsonProperty(PropertyName = "wash_pressure_end", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? WashPressureEnd { get; set; }

        [JsonProperty(PropertyName = "h_dynamic_pressure_after_wash", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HDynamicPressureAfterWash { get; set; }

        [JsonProperty(PropertyName = "h_dynamic_pressure_duration", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HDynamicPressureDuration { get; set; }

        [JsonProperty(PropertyName = "pressure_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureAfter { get; set; }

        [JsonProperty(PropertyName = "purge", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Purge { get; set; }

        [JsonProperty(PropertyName = "amperage_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AmperageAfter { get; set; }

        [JsonProperty(PropertyName = "voltage_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? VoltageAfter { get; set; }

        [JsonProperty(PropertyName = "qj_manual", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? QjManual { get; set; }

        [JsonProperty(PropertyName = "qj_manual_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? QjManualBefore { get; set; }

        [JsonProperty(PropertyName = "qj_manual_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? QjManualAfter { get; set; }

        [JsonProperty(PropertyName = "amperage_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AmperageBefore { get; set; }

        [JsonProperty(PropertyName = "loading_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? LoadingBefore { get; set; }

        [JsonProperty(PropertyName = "pressure_engine_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEngineBefore { get; set; }

        [JsonProperty(PropertyName = "pressure_engine_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEngineAfter { get; set; }

        [JsonProperty(PropertyName = "pressure_environment_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEnvironmentBefore { get; set; }

        [JsonProperty(PropertyName = "pressure_environment_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEnvironmentAfter { get; set; }

        [JsonProperty(PropertyName = "temperature_engine_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEngineBefore { get; set; }

        [JsonProperty(PropertyName = "temperature_engine_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEngineAfter { get; set; }

        [JsonProperty(PropertyName = "temperature_environment_before", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEnvironmentBefore { get; set; }

        [JsonProperty(PropertyName = "temperature_environment_after", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEnvironmentAfter { get; set; }

        [JsonProperty(PropertyName = "h_static_after_stop", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HStaticAfterStop { get; set; }

        [JsonProperty(PropertyName = "rotation_change", NullValueHandling = NullValueHandling.Ignore)]
        public string RotationChange { get; set; }

        [JsonProperty(PropertyName = "phase_check", NullValueHandling = NullValueHandling.Ignore)]
        public string PhaseCheck { get; set; }

        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "ticket_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? TicketID { get; set; }

        [JsonProperty(PropertyName = "ticket_created", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime TicketCreated { get; set; }

        [JsonProperty(PropertyName = "ticket_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TicketType TicketType { get; set; }

        [JsonProperty(PropertyName = "membership_id", NullValueHandling = NullValueHandling.Ignore)]
        public long MembershipID { get; set; }

        [JsonProperty(PropertyName = "department_id", NullValueHandling = NullValueHandling.Ignore)]
        public long DepartmentID { get; set; }
        
        [JsonProperty(PropertyName = "status", NullValueHandling = NullValueHandling.Ignore)]
        public int Status{ get; set; }

        [JsonProperty(PropertyName = "requested", NullValueHandling = NullValueHandling.Ignore)]
        public string Requested { get; set; }

        [JsonProperty(PropertyName = "called_agents", NullValueHandling = NullValueHandling.Ignore)]
        public string CalledAgents { get; set; }

        [JsonProperty(PropertyName = "watering", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Watering { get; set; }

        [JsonProperty(PropertyName = "kvch", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Kvch { get; set; }

        [JsonProperty(PropertyName = "multi_component", NullValueHandling = NullValueHandling.Ignore)]
        public bool? MultiComponent { get; set; }

        [JsonProperty(PropertyName = "carbonate", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Carbonate { get; set; }

        [JsonProperty(PropertyName = "inhibitor", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Inhibitor { get; set; }

        [JsonProperty(PropertyName = "requested_epu", NullValueHandling = NullValueHandling.Ignore)]
        public bool? RequestedEPU { get; set; }

        [JsonProperty(PropertyName = "requested_cnipr", NullValueHandling = NullValueHandling.Ignore)]
        public bool? RequestedCNIPR { get; set; }

        [JsonProperty(PropertyName = "power_off", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PowerOff { get; set; }

        [JsonProperty(PropertyName = "liquid_probe", NullValueHandling = NullValueHandling.Ignore)]
        public bool? LiquidProbe { get; set; }

        [JsonProperty(PropertyName = "pressure_after_damping", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PressureAfterDamping { get; set; }

        [JsonProperty(PropertyName = "probe_time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? ProbeTime { get; set; }

        [JsonProperty(PropertyName = "treatment_time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? TreatmentTime { get; set; }

        [JsonProperty(PropertyName = "obtain_time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? ObtainTime { get; set; }

        [JsonProperty(PropertyName = "padding_time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? PaddingTime { get; set; }

        [JsonProperty(PropertyName = "start_time_plan", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? StartTimePlan { get; set; }

        [JsonProperty(PropertyName = "start_time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? StartTime { get; set; }

        [JsonProperty(PropertyName = "start_time_fact", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? StartTimeFact { get; set; }

        [JsonProperty(PropertyName = "time_completion", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? TimeCompletion { get; set; }

        [JsonProperty(PropertyName = "q", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Q { get; set; }

        [JsonProperty(PropertyName = "cycles", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Cycles { get; set; }

        [JsonProperty(PropertyName = "damping_volume_cycle_1", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingVolumeCycleA { get; set; }

        [JsonProperty(PropertyName = "weight_cycle_1", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingWeightCycleA { get; set; }

        [JsonProperty(PropertyName = "damping_volume_cycle_2", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingVolumeCycleB { get; set; }

        [JsonProperty(PropertyName = "weight_cycle_2", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingWeightCycleB { get; set; }

        [JsonProperty(PropertyName = "damping_volume_cycle_3", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingVolumeCycleC { get; set; }

        [JsonProperty(PropertyName = "weight_cycle_3", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingWeightCycleC { get; set; }

        [JsonProperty(PropertyName = "damping_volume_cycle_4", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingVolumeCycleD { get; set; }

        [JsonProperty(PropertyName = "weight_cycle_4", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? DampingWeightCycleD { get; set; }

        [JsonProperty(PropertyName = "amperage", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Amperage { get; set; }
        
        [JsonProperty(PropertyName = "balance_delta", NullValueHandling = NullValueHandling.Ignore)]
        public bool? BalanceDelta { get; set; }

        [JsonProperty(PropertyName = "equipment_verification", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public VerificationState? EquipmentVerification { get; set; }

        [JsonProperty(PropertyName = "ground_equipment_verification", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public VerificationState? GroundEquipmentVerification { get; set; }

        [JsonProperty(PropertyName = "event_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellEventType? EventType { get; set; }

        [JsonProperty(PropertyName = "repair_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellRepairType? RepairType { get; set; }

        [JsonProperty(PropertyName = "dmg_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public DMGType? DMGType { get; set; }

        [JsonProperty(PropertyName = "puller", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Puller { get; set; }

        [JsonProperty(PropertyName = "crimping", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Crimping { get; set; }

        [JsonProperty(PropertyName = "dmg", NullValueHandling = NullValueHandling.Ignore)]
        public bool? DMG { get; set; }

        [JsonProperty(PropertyName = "dmg_before", NullValueHandling = NullValueHandling.Ignore)]
        public bool? DMGBefore { get; set; }

        [JsonProperty(PropertyName = "treatment_datebegin", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? TreatmentDateBegin { get; set; }

        [JsonProperty(PropertyName = "treatment_dateend", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? TreatmentDateEnd { get; set; }

        [JsonProperty(PropertyName = "qn_volume_before_loss", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? VolumeBeforeLoss { get; set; }

        [JsonProperty(PropertyName = "qn_volume_before_collector", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? VolumeBeforeCollector { get; set; }

        [JsonProperty(PropertyName = "temperature_interval", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureInterval { get; set; }

        [JsonProperty(PropertyName = "pressure_adpm_begin", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureADPMBegin { get; set; }

        [JsonProperty(PropertyName = "pressure_adpm_end", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureADPMEnd { get; set; }

        [JsonProperty(PropertyName = "duration", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Duration { get; set; }

        [JsonProperty(PropertyName = "h_static", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HStatic { get; set; }
        
        [JsonProperty(PropertyName = "h_dynamic", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HDynamic { get; set; }

        [JsonProperty(PropertyName = "h_dynamic_minimal", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HDynamicMinimal { get; set; }

        [JsonProperty(PropertyName = "qj_locked", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? QjLocked { get; set; }

        [JsonProperty(PropertyName = "qj_redirect", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? QjRedirect { get; set; }

        [JsonProperty(PropertyName = "loading", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Loading { get; set; }

        [JsonProperty(PropertyName = "lock_available", NullValueHandling = NullValueHandling.Ignore)]
        public bool? LockAvailable { get; set; }

        [JsonProperty(PropertyName = "operation_mode", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? OperationMode { get; set; }

        [JsonProperty(PropertyName = "pile_field_demontage", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PileFieldDemontage { get; set; }

        [JsonProperty(PropertyName = "preparing", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Preparing { get; set; }

        [JsonProperty(PropertyName = "mixture", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Mixture { get; set; }

        [JsonProperty(PropertyName = "pressure", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Pressure { get; set; }

        [JsonProperty(PropertyName = "pressure_buffer", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureBuffer { get; set; }

        [JsonProperty(PropertyName = "pressure_engine", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEngine { get; set; }

        [JsonProperty(PropertyName = "pressure_crimping", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureCrimping { get; set; }

        [JsonProperty(PropertyName = "pressure_environment", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEnvironment { get; set; }

        [JsonProperty(PropertyName = "pressure_redundant", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureRedundant { get; set; }

        [JsonProperty(PropertyName = "pump_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? PumpID { get; set; }

        [JsonProperty(PropertyName = "pump_depth", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PumpDepth { get; set; }

        [JsonProperty(PropertyName = "pump_typesize", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public PumpTypeSize? PumpTypeSize { get; set; }

        [JsonProperty(PropertyName = "pump_crimping", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PumpCrimping { get; set; }

        [JsonProperty(PropertyName = "pump_washing", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PumpWashing { get; set; }

        [JsonProperty(PropertyName = "puller_type_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? PullerTypeID { get; set; }

        [JsonProperty(PropertyName = "contractor_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? ContractorID { get; set; }

        [JsonProperty(PropertyName = "qj", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Qj { get; set; }

        [JsonProperty(PropertyName = "rock_frequency", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RockFrequency { get; set; }
        
        [JsonProperty(PropertyName = "rotation_frequency", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RotationFrequency { get; set; }

        [JsonProperty(PropertyName = "rotation_frequency_delta", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RotationFrequencyDelta { get; set; }

        [JsonProperty(PropertyName = "service_area", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ServiceArea { get; set; }

        [JsonProperty(PropertyName = "worked", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Worked { get; set; }

        [JsonProperty(PropertyName = "revision_svu", NullValueHandling = NullValueHandling.Ignore)]
        public bool? RevisionSVU { get; set; }

        [JsonProperty(PropertyName = "replace_svu", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ReplaceSVU { get; set; }

        [JsonProperty(PropertyName = "treatment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TreatmentType? Treatment { get; set; }

        [JsonProperty(PropertyName = "temperature_engine", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEngine { get; set; }

        [JsonProperty(PropertyName = "temperature_environment", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEnvironment { get; set; }

        [JsonProperty(PropertyName = "union_action_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public UnionActionType? UnionAction { get; set; }

        [JsonProperty(PropertyName = "valve_verification", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public VerificationState? ValveVerification { get; set; }

        [JsonProperty(PropertyName = "voltage", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Voltage { get; set; }

        [JsonProperty(PropertyName = "volume", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Volume { get; set; }

        [JsonProperty(PropertyName = "probe_volume", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ProbeVolume { get; set; }

        [JsonProperty(PropertyName = "damping_volume", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? DampingVolume { get; set; }

        [JsonProperty(PropertyName = "wash_volume", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? WashVolume { get; set; }

        [JsonProperty(PropertyName = "weight", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Weight { get; set; }

        [JsonProperty(PropertyName = "damping_weight", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? DampingWeight { get; set; }

        [JsonProperty(PropertyName = "wheeling", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Wheeling { get; set; }

        [JsonProperty(PropertyName = "repair_type_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? RepairTypeID { get; set; }

        [JsonProperty(PropertyName = "reason", NullValueHandling = NullValueHandling.Ignore)]
        public string Reason { get; set; }

        [JsonProperty(PropertyName = "stop_reason", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public StopReason? StopReason { get; set; }

        [JsonProperty(PropertyName = "request_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestType? RequestType { get; set; }

        [JsonProperty(PropertyName = "request_reason", NullValueHandling = NullValueHandling.Ignore)]
        public string RequestReason { get; set; }

        [JsonProperty(PropertyName = "h_dynamic_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HDynamic15 { get; set; }

        [JsonProperty(PropertyName = "pressure_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Pressure15 { get; set; }

        [JsonProperty(PropertyName = "q_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Q15 { get; set; }

        [JsonProperty(PropertyName = "pressure_environment_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEnvironment15 { get; set; }

        [JsonProperty(PropertyName = "pressure_engine_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEngine15 { get; set; }

        [JsonProperty(PropertyName = "temperature_environment_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEnvironment15 { get; set; }

        [JsonProperty(PropertyName = "temperature_engine_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEngine15 { get; set; }

        [JsonProperty(PropertyName = "amperage_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Amperage15 { get; set; }

        [JsonProperty(PropertyName = "loading_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Loading15 { get; set; }

        [JsonProperty(PropertyName = "rotation_frequency_15", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RotationFrequency15 { get; set; }

        [JsonProperty(PropertyName = "h_dynamic_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HDynamic30 { get; set; }

        [JsonProperty(PropertyName = "pressure_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Pressure30 { get; set; }

        [JsonProperty(PropertyName = "q_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Q30 { get; set; }

        [JsonProperty(PropertyName = "pressure_environment_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEnvironment30 { get; set; }

        [JsonProperty(PropertyName = "pressure_engine_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEngine30 { get; set; }

        [JsonProperty(PropertyName = "temperature_environment_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEnvironment30 { get; set; }

        [JsonProperty(PropertyName = "temperature_engine_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TemperatureEngine30 { get; set; }

        [JsonProperty(PropertyName = "amperage_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Amperage30 { get; set; }

        [JsonProperty(PropertyName = "loading_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Loading30 { get; set; }

        [JsonProperty(PropertyName = "rotation_frequency_30", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RotationFrequency30 { get; set; }

        [JsonProperty(PropertyName = "qj_reverse", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? QjReverse { get; set; }

        [JsonProperty(PropertyName = "amperage_reverse", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AmperageReverse { get; set; }

        [JsonProperty(PropertyName = "loading_reverse", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? LoadingReverse { get; set; }

        [JsonProperty(PropertyName = "protection_correction", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ProtectionCorrection { get; set; }

        [JsonProperty(PropertyName = "scraped", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Scraped { get; set; }

        [JsonProperty(PropertyName = "scrap_problem", NullValueHandling = NullValueHandling.Ignore)]
        public string ScrapProblem { get; set; }

        [JsonProperty(PropertyName = "note", NullValueHandling = NullValueHandling.Ignore)]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "attachment_id", NullValueHandling = NullValueHandling.Ignore)]
        public string AttachmentID { get; set; }

        [JsonProperty(PropertyName = "sl")]
        public int? SL { get; set; }

        [JsonProperty(PropertyName = "is_night")]
        public bool IsNight { get; set; }

        [JsonProperty(PropertyName = "oil_component", NullValueHandling = NullValueHandling.Ignore)]
        public virtual bool? OilComponent{ get; set; }

        [JsonProperty(PropertyName = "complex", NullValueHandling = NullValueHandling.Ignore)]
        public virtual bool? Complex { get; set; }

        [JsonProperty(PropertyName = "gathering", NullValueHandling = NullValueHandling.Ignore)]
        public virtual bool? Gathering { get; set; }

        [JsonProperty(PropertyName = "separate_expense", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? SeparateExpense { get; set; }

        [JsonProperty(PropertyName = "separate_humidity", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? SeparateHumidity { get; set; }

        [JsonProperty(PropertyName = "separate_pressure", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? SeparatePressure { get; set; }

        [JsonProperty(PropertyName = "separate_temperature", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal? SeparateTemperature { get; set; }

        [JsonProperty(PropertyName = "h_static_pipe", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HStaticPipe { get; set; }

        [JsonProperty(PropertyName = "pressure_pipe", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressurePipe { get; set; }

        [JsonProperty(PropertyName = "union_dynamic", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? UnionDynamic { get; set; }

        [JsonProperty(PropertyName = "union_diameter", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? UnionDiameter { get; set; }

        [JsonProperty(PropertyName = "plunger_length", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PlungerLength { get; set; }


        [JsonProperty(PropertyName = "shank_length", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ShankLength { get; set; }

        [JsonProperty(PropertyName = "pressure_ecn_minimal", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PressureEcnMinimal { get; set; }

        [JsonProperty(PropertyName = "well_period_mode", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellPeriodMode? WellPeriodMode { get; set; }

        [JsonProperty(PropertyName = "well_purpose", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellPurpose? WellPurpose { get; set; }

        [JsonProperty(PropertyName = "research", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Research? Research { get; set; }
    }
}
