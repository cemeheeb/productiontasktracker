﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class TicketMovementNotificationModel {
        [Display(Name = "Код заявки")]
        [JsonProperty(PropertyName = "ticket_id")]
        public long TicketID { get; set; }

        [Display(Name = "Код движения заявки")]
        [JsonProperty(PropertyName = "ticket_movement_id")]
        public long TicketMovementID { get; set; }

        [Display(Name = "Код детали заявки")]
        [JsonProperty(PropertyName = "ticket_detail_id")]
        public long TicketDetailID { get; set; }

        [Display(Name = "Тип заявки")]
        [JsonProperty(PropertyName = "ticket_type")]
        public string TicketType { get; set; }

        [Display(Name = "Статус")]
        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }

        [Display(Name = "Дата и время движения заявки")]
        [JsonProperty(PropertyName = "created")]
        public DateTime Created { get; set; }
    }
}
