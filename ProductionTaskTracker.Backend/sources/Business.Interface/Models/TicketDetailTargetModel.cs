﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Enumerables;

    public class TicketDetailTargetModel {
        [JsonProperty(PropertyName = "well_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? WellID { get; set; }

        [JsonProperty(PropertyName = "field_code", NullValueHandling = NullValueHandling.Ignore)]
        public string FieldCode { get; set; }

        [JsonProperty(PropertyName = "cluster", NullValueHandling = NullValueHandling.Ignore)]
        public string Cluster { get; set; }

        [JsonProperty(PropertyName = "mf_position", NullValueHandling = NullValueHandling.Ignore)]
        public int? MeasureFacilityPosition { get; set; }

        [JsonProperty(PropertyName = "well_area", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public WellArea? WellArea { get; set; }

        [JsonProperty(PropertyName = "brigade_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public BrigadeType? BrigadeType { get; set; }

        [JsonProperty(PropertyName = "brigade", NullValueHandling = NullValueHandling.Ignore)]
        public int? Brigade { get; set; }
    }
}
