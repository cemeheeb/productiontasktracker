﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class WorkPeriodBeginModel {
        [Display(Name = "Идентификатор пользователя")]
        [JsonProperty(PropertyName = "membership_id")]
        public long MembershipID { get; set; }

        [Display(Name = "Идентификатор должности")]
        [JsonProperty(PropertyName = "position_id")]
        public long PositionID { get; set; }

        [Display(Name = "Дата начала")]
        [JsonProperty(PropertyName = "date")]
        public DateTime? Date { get; set; }
    }
}
