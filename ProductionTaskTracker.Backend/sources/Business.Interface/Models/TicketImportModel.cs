﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Enumerables;

    public class TicketImportModel {
        // Тип заявки
        public TicketType ticket_type { get; set; }

        // Исполнитель
        public long department_id { get; set; }

        // SL Уровень
        public int? sl { get; set; }

        // Ночная смена
        public bool is_night { get; set; }

        // Группа заявок
        public long[] ticket_detail_ids { get; set; }

    }
}
