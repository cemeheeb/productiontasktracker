﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class AttachmentFileModel: FileModel {
        [Display(Name = "Идентификатор вложения")]
        [JsonProperty(PropertyName = "attachment_id")]
        public string AttachmentID { get; set; }
    }
}
