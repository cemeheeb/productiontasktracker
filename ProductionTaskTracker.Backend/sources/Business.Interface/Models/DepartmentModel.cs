﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class DepartmentModel {
        public DepartmentModel() {
        }

        public DepartmentModel(Department entity) {
            Id = entity.Id;
            ParentID = entity.Parent?.Id;
            FullName = entity.FullName;
            DisplayName = entity.DisplayName;
            ParentID = entity.Parent != null ? (long?)entity.Parent.Id : null;
            ShopCode = entity.Shop != null ? entity.Shop.Code : null;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Полное название")]
        [JsonProperty(PropertyName = "full_name")]
        public string FullName { get; set; }

        [Display(Name = "Сокращенное название")]
        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }

        [Display(Name = "Идентификатор родительского подразделения")]
        [JsonProperty(PropertyName = "parent_id")]
        public long? ParentID { get; set; }

        [Display(Name = "Код цеха")]
        [JsonProperty(PropertyName = "shop_code")]
        public string ShopCode { get; set; }
    }
}
