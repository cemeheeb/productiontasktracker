﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class WorkPeriodEndModel {
        [Display(Name = "Идентификатор пользователя")]
        [JsonProperty(PropertyName = "position_id")]
        public long PositionID { get; set; }

        [Display(Name = "Дата окончания")]
        [JsonProperty(PropertyName = "date")]
        public DateTime? Date { get; set; }
    }
}