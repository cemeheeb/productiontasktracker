﻿using System.ComponentModel.DataAnnotations;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;
    using Newtonsoft.Json.Converters;

    public class ClassifierModel {
        public ClassifierModel() {}

        public ClassifierModel(Classifier entity) {
            Id = entity.Id;
            Kind = entity.Kind;
            Name = entity.Name;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        [Display(Name = "Тип")]
        [JsonProperty(PropertyName = "kind")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ClassifierKind Kind { get; set; }

        [Display(Name = "Наименование")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}