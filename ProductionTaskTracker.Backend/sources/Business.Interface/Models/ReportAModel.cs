﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class ReportAModel {
        public DateTime date_begin { get; set; }

        public DateTime date_end { get; set; }

        public bool? show_expired { get; set; }

        public long[] departments { get; set; }

        public long[] wells { get; set; }

        public string[] ticket_types { get; set; }

        public int? sl { get; set; }

    }
}
