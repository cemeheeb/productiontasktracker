﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using System.ComponentModel.DataAnnotations;
    using Domain.DTO;
    using Newtonsoft.Json;

    public class FilterOptionModel {
        public FilterOptionModel(FilterOption filterOption) {
            Code = filterOption.Code;
            Value = filterOption.Value;
        }

        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Display(Name = "Значение")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}
