﻿using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class TicketDetailVisitModel {
        public TicketDetailVisitModel(TicketVisit entity) {
            Membership = entity.Membership.DisplayName;
            Created = entity.Created;
        }

        [Display(Name = "Пользователь")]
        [JsonProperty(PropertyName = "membership_display_name")]
        public string Membership { get; set; }

        [Display(Name = "Дата создания")]
        [JsonProperty(PropertyName = "created")]
        public DateTime Created { get; set; }
    }
}
