﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class ImportFileModel: FileModel {
        [Display(Name = "Идентификатор бланка")]
        [JsonProperty(PropertyName = "import_id")]
        public string ImportID { get; set; }
    }
}
