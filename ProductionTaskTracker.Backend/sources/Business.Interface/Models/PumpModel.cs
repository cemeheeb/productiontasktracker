﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    public class PumpModel {
        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Наименование")]
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [Display(Name = "Тип насоса")]
        [JsonProperty(PropertyName = "type_ecn")]
        public string TypeECN { get; set; }

        [Display(Name = "Гб")]
        [JsonProperty(PropertyName = "gb")]
        public string Gb { get; set; }

        [Display(Name = "Номинальный поток")]
        [JsonProperty(PropertyName = "nominal_flow")]
        public short NominalFlow { get; set; }

        [Display(Name = "Номинальная подача")]
        [JsonProperty(PropertyName = "nominal_feed")]
        public short NominalFeed { get; set; }

        [Display(Name = "Дополнительная информация 1")]
        [JsonProperty(PropertyName = "col1")]
        public int Col1 { get; set; }

        [Display(Name = "Дополнительная информация 2")]
        [JsonProperty(PropertyName = "col2")]
        public int Col2 { get; set; }

        [Display(Name = "Дополнительная информация 3")]
        [JsonProperty(PropertyName = "col3")]
        public int Col3 { get; set; }

        [Display(Name = "Производитель")]
        [JsonProperty(PropertyName = "factory")]
        public string Factory { get; set; }

        [Display(Name = "Страна")]
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
    }
}