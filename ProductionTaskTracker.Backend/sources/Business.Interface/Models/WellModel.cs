﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class WellModel {
        public WellModel() {
        }

        public WellModel(Well entity) {
            Id = entity.Id;
            Code = entity.Code;
            Shop = entity.Shop.Name;
            Field = entity.Field.Name;
            Cluster = entity.Cluster;
        }

        [Display(Name = "Код")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Номер")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Display(Name = "Месторождение")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [Display(Name = "Куст")]
        [JsonProperty(PropertyName = "cluster")]
        public string Cluster { get; set; }

        [Display(Name = "Цех")]
        [JsonProperty(PropertyName = "shop")]
        public string Shop { get; set; }
    }
}