﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class DepartmentFullModel {
        public DepartmentFullModel(Department entity) {
            Id = entity.Id;
            FullName = entity.FullName;
            DisplayName = entity.DisplayName;

            if (entity.Parent != null)
                Parent = new DepartmentFullModel(entity.Parent);
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Родительское подразделение")]
        [JsonProperty(PropertyName = "parent")]
        public DepartmentFullModel Parent { get; set; }

        [Display(Name = "Полное название")]
        [JsonProperty(PropertyName = "full_name")]
        public string FullName { get; set; }

        [Display(Name = "Сокращенное название")]
        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }
    }
}
