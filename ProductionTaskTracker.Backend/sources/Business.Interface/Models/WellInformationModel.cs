﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models {
    using Domain.Entities;

    public class WellInformationModel {
        public WellInformationModel(WellExecutionMode executionMode, Well well) {
            Id = well.Id;
            Shop = well.Shop.Name;
            Field = well.Field.Name;
            Cluster = well.Cluster;
            Code = well.Code;
            Qj = executionMode?.Qj;
            Qn = executionMode?.Qn;
            Timestamp = executionMode?.Timestamp;
        }

        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Цех")]
        [JsonProperty(PropertyName = "shop")]
        public string Shop { get; set; }

        [Display(Name = "Месторождение")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [Display(Name = "Куст")]
        [JsonProperty(PropertyName = "cluster")]
        public string Cluster { get; set; }

        [Display(Name = "Номер")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Display(Name = "Дебит жидкости")]
        [JsonProperty(PropertyName = "qj")]
        public decimal? Qj { get; set; }

        [Display(Name = "Дебит нефти")]
        [JsonProperty(PropertyName = "qn")]
        public decimal? Qn { get; set; }

        [Display(Name = "Дата замера")]
        [JsonProperty(PropertyName = "timestamp")]
        public DateTime? Timestamp { get; set; }
    }
}
