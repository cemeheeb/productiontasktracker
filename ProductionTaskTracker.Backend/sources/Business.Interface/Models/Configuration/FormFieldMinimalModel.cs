﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public class FormFieldMinimalModel : PropertyTypeModel {
        public override string Component => "FormFieldMinimal";

        [Display(Name = "Тип")]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [Display(Name = "Единица измерения")]
        [JsonProperty(PropertyName = "units")]
        public string Units { get; set; }

        [Display(Name = "Минимум")]
        [JsonProperty(PropertyName = "min")]
        public string Min { get; set; }
    }
}
