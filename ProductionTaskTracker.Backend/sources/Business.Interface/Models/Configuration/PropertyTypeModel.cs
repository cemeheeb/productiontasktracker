﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public abstract class PropertyTypeModel {
        [Display(Name = "Название")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; }

        [Display(Name = "Компонент")]
        [JsonProperty(PropertyName = "component")]
        public abstract string Component { get; }
    }
}
