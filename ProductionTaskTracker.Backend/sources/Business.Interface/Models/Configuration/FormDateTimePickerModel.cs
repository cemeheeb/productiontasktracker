﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public class FormDateTimePickerModel : PropertyTypeModel {
        public override string Component => "FormDateTimePicker";
    }
}
