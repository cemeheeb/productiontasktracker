﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public class SelectPumpModel : PropertyTypeModel {
        public override string Component => "SelectPump";
    }
}
