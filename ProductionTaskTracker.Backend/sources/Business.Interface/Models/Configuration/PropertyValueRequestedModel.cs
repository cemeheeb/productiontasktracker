﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public class PropertyValueRequestedModel {
        List<string> Options { get; set; }
    }
}
