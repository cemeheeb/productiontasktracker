﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public class SelectPullerTypeModel : PropertyTypeModel {
        public override string Component => "SelectPullerType";
    }
}
