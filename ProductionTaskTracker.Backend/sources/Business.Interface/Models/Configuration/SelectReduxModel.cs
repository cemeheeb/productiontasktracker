﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Models.Configuration {
    public class SelectReduxModel: PropertyTypeModel {
        public override string Component => "SelectRedux";

        [Display(Name = "Позиция")]
        [JsonProperty(PropertyName = "mathPos")]
        public string MathPosition { get; set; }

        [Display(Name = "Множество")]
        [JsonProperty(PropertyName = "multi")]
        public bool Multi { get; set; }

        [Display(Name = "Простые значения")]
        [JsonProperty(PropertyName = "simpleValue")]
        public bool SimpleValue { get; set; }
    }
}
