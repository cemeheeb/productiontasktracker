﻿using Ninject.Modules;
using Ninject.Web.Common;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure {
    using Services;

    public class Module : NinjectModule {
        public override void Load() {
            Bind<ISettingsService>().To<SettingsService>().InSingletonScope();
            Bind<IFilesystemService>().To<FilesystemService>().InSingletonScope();
            Bind<IMembershipService>().To<MembershipService>().InRequestScope();
        }
    }
}
