﻿using System;
using System.Configuration;
using System.Web.Hosting;
using System.IO;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Services {
    using Domain.Entities;

    public class FilesystemService: IFilesystemService {
        #region Fields

        private readonly ILogger _logger;
        private readonly string _rootDirectoryPath;
        private readonly string _rootUrl;

        #endregion

        public FilesystemService(ILoggerFactory loggerFactory) {
            _logger = loggerFactory.GetLogger("trace");
            _rootDirectoryPath = ConfigurationManager.AppSettings["RootDirectoryPath"];
            _rootUrl = ConfigurationManager.AppSettings["RootUrl"];
        }

        public string GetMembershipTemporaryPath(Membership membership) {
            try {
                var output = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, _rootDirectoryPath,
                membership.UserName, "temporary");
                if (!Directory.Exists(output)) {
                    Directory.CreateDirectory(output);
                }

                return output;
            } catch (Exception exception) {
                _logger.ErrorException("Не удалось получить пользовательский каталог временных файлов", exception);
                throw;
            }
        }

        public Uri GetMembershipTemporaryUri(Membership membership) {
            return new Uri(new Uri(_rootUrl), $"static/{membership.UserName}/temporary/");
        }

        public string GetMembershipReportPath(Membership membership, string uid) {
            try {
                var output = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, _rootDirectoryPath, membership.UserName, "reports", uid);
                if (!Directory.Exists(output)) {
                    Directory.CreateDirectory(output);
                }

                return output;
            } catch (Exception exception) {
                _logger.ErrorException("Не удалось получить пользовательский каталог файлов отчётов", exception);
                throw;
            }
        }
        
        public Uri GetMembershipReportUri(Membership membership, string uid) {
            return new Uri(new Uri(_rootUrl), $"static/{membership.UserName}/reports/{uid}/");
        }

        public string GetMembershipImportPath(Membership membership, string uid) {
            try {
                var output = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, _rootDirectoryPath, membership.UserName, "imports", uid);
                if (!Directory.Exists(output)) {
                    Directory.CreateDirectory(output);
                }

                return output;
            } catch (Exception exception) {
                _logger.ErrorException("Не удалось получить пользовательский каталог для импорта пакета заявок", exception);
                throw;
            }
        }

        public Uri GetMembershipImportUri(Membership membership, string uid) {
            return new Uri(new Uri(_rootUrl), $"static/{membership.UserName}/imports/{uid}/");
        }

        public string GetAttachmentPath() {
            try {
                var output = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, _rootDirectoryPath, "attachments");
                if (!Directory.Exists(output)) {
                    Directory.CreateDirectory(output);
                }

                return output;
            } catch (Exception exception) {
                _logger.ErrorException("Не удалось получить пользовательский каталог файлов вложений", exception);
                throw;
            }
        }

        public Uri GetAttachmentUri() {
            return new Uri(new Uri(_rootUrl), $"static/attachments/");
        }
    }
}
