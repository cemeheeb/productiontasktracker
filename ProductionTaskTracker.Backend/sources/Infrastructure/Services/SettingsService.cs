﻿using System.Configuration;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Services {
    public class SettingsService: ISettingsService {
        public string GetActiveDirectoryLpadPath() {
            return "LDAP://CORP";
        }

        public string GetRemoteServer() {
            return ConfigurationManager.AppSettings["RemoteServer"];
        }
    }
}
