﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure {
    /// <summary>
    /// Исключение предназначенное для выбрасывания в инфраструктурном слое
    /// </summary>
    public class InfrastructureException: Exception {
        public InfrastructureException()
            : base("Нарушена работа программной инфраструктуры сервера. Свяжитесь с администратором ПО") {
        }

        public InfrastructureException(string message)
            : base(message) {
        }

        public InfrastructureException(string message, Exception innerException)
            : base(message, innerException) {
        }
    }

    /// <summary>
    /// Исключение предназначенное для выбрасывания при работе с токеном
    /// </summary>
    public class TokenException: InfrastructureException {
        public TokenException()
            : base("Токен некорректен или устарел") {
        }

        public TokenException(string message)
            : base(message) {
        }
    }
}
