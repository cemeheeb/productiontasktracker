﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService {
    [RunInstaller(true)]
    public class ProjectInstaller : Installer {
        public ProjectInstaller() {
#if RELEASE_TEST
            var serviceInstaller = new ServiceInstaller {
                DisplayName = "Production Task Tracker Scheduler Service Testing Mode",
                Description = "Служба формирования отложенных заявок в системе УПЗ. Режим тестирования",
                ServiceName = "ProductionTaskTracker.Service.Test",
                StartType = ServiceStartMode.Manual
            };
#else
            var serviceInstaller = new ServiceInstaller {
                DisplayName = "Production Task Tracker Scheduler Service",
                Description = "Служба формирования отложенных заявок в системе УПЗ",
                ServiceName = "ProductionTaskTracker.Service",
                StartType = ServiceStartMode.Manual
            };
#endif
            var serviceProcessInstaller = new ServiceProcessInstaller {
                Account = ServiceAccount.LocalSystem,
                Password = null,
                Username = null
            };

            Installers.AddRange(new Installer[] {
                serviceInstaller,
                serviceProcessInstaller
            });
        }
    }
}
