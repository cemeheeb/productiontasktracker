﻿using System.ServiceProcess;
using Quartz;
using Quartz.Impl;

using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService {
    using System;
    using System.Linq;
    using AutoMapper;
    using Business;
    using DataAccess.Repositories;
    using DataAccess;
    using DataAccess.Services;
    using Domain.Entities;
    using Domain.Entities.TicketDetails;
    using Domain.Enumerables;
    using Jobs;

    public sealed partial class SchedulerService : ServiceBase, ISchedulerService {
        #region Fields

        private readonly IScheduler _scheduler;

        private readonly ILogger _logger;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMembershipRepository _membershipRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly ITicketMovementRepository _ticketMovementRepository;
        private readonly IRegisterService _registerService;

        #endregion

        public SchedulerService() {
            InitializeEventLogService();
            EventLog.WriteEntry("Инициализация приложения");
        }

        private void InitializeEventLogService() {
#if !RELEASE
            EventLog.Source = "ProductionTaskTracker.Service.Test";
#else
            EventLog.Source = "ProductionTaskTracker.Service";
#endif
        }

        public SchedulerService(
            ILoggerFactory loggerFactory, 
            IUnitOfWorkFactory unitOfWorkFactory,
            IMembershipRepository membershipRepository, 
            ITicketRepository ticketRepository, 
            ITicketMovementRepository ticketMovementRepository, 
            IRegisterService registerService
        ) {
            InitializeEventLogService();
            InitializeComponent();
            
            _logger = loggerFactory.GetLogger("trace");

            _unitOfWorkFactory = unitOfWorkFactory;
            _membershipRepository = membershipRepository;
            _ticketRepository = ticketRepository;
            _ticketMovementRepository = ticketMovementRepository;
            _registerService = registerService;

            _logger.Info("Запуск планировщика задач");
            EventLog.WriteEntry("Запуск планировщика задач");
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();
        }

        public void Initialize() {
            _logger.Info("Инициализация заданий для планировщика");
            EventLog.WriteEntry("Инициализация заданий для планировщика");

            // Ночные заявки
            // Задания для работы ночным персоналом формируются в течении дня
            // выгружаются в 18:00

            _logger.Info("Инициализация заданий для работы ночным персоналом формируются в течении дня");
            var targetJob = JobBuilder.Create<FillNightJob>()
                .WithIdentity("FillNightJob")
                .Build();
            targetJob.JobDataMap.Put("Logger", _logger);
            targetJob.JobDataMap.Put("UnitOfWorkFactory", _unitOfWorkFactory);
            targetJob.JobDataMap.Put("MembershipRepository", _membershipRepository);
            targetJob.JobDataMap.Put("TicketRepository", _ticketRepository);
            targetJob.JobDataMap.Put("TicketMovementRepository", _ticketMovementRepository);
            targetJob.JobDataMap.Put("RegisterService", _registerService);

            var targetTrigger = TriggerBuilder.Create()
                .WithIdentity("FillNightJob")
                .StartNow()
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(18, 0))
                .Build();

            _scheduler.ScheduleJob(targetJob, targetTrigger);
#if !RELEASE
            _scheduler.TriggerJob(targetJob.Key);
#endif

            // Регулярные утренние заявки
            // Задания сформированные с 8:00 до 11:30 
            // выгружаются в 11:30
            _logger.Info("Инициализация заданий сформированных с 8:00 до 11:30");
            targetJob = JobBuilder.Create<FillRegularJob>()
                .WithIdentity("FillRegularMorningJob")
                .Build();
            targetJob.JobDataMap.Put("Logger", _logger);
            targetJob.JobDataMap.Put("UnitOfWorkFactory", _unitOfWorkFactory);
            targetJob.JobDataMap.Put("MembershipRepository", _membershipRepository);
            targetJob.JobDataMap.Put("TicketRepository", _ticketRepository);
            targetJob.JobDataMap.Put("TicketMovementRepository", _ticketMovementRepository);
            targetJob.JobDataMap.Put("RegisterService", _registerService);

            targetTrigger = TriggerBuilder.Create()
                .WithIdentity("FillRegularMorningJob")
                .StartNow()
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(11, 30))
                .Build();
            _scheduler.ScheduleJob(targetJob, targetTrigger);

            // Регулярные заявки за рабочий день
            // Задания сформированные с 12:00 до 7:30 
            // выгружаются в 7:30
            _logger.Info("Инициализация заданий сформированных с 12:00 до 7:30");
            targetJob = JobBuilder.Create<FillRegularJob>()
                .WithIdentity("FillRegularDayJob")
                .Build();
            targetJob.JobDataMap.Put("Logger", _logger);
            targetJob.JobDataMap.Put("UnitOfWorkFactory", _unitOfWorkFactory);
            targetJob.JobDataMap.Put("MembershipRepository", _membershipRepository);
            targetJob.JobDataMap.Put("TicketRepository", _ticketRepository);
            targetJob.JobDataMap.Put("TicketMovementRepository", _ticketMovementRepository);
            targetJob.JobDataMap.Put("RegisterService", _registerService);

            targetTrigger = TriggerBuilder.Create()
                .WithIdentity("FillRegularDayJob")
                .StartNow()
#if !RELEASE
                .WithSchedule(CronScheduleBuilder.CronSchedule("0 0/1 * * * ?")).Build();
#else
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(7, 30)).Build();
#endif
            _scheduler.ScheduleJob(targetJob, targetTrigger);
            _scheduler.TriggerJob(targetTrigger.JobKey);

            _scheduler.Start();
        }

        protected override void OnStart(string[] args) {
            _logger.Info("Запуск службы");
            EventLog.WriteEntry("Запуск службы");
            Initialize();
        }

        protected override void OnPause() {
            _logger.Info("Приостановка службы");
            EventLog.WriteEntry("Приостановка службы");

            _scheduler.PauseAll();
            base.OnPause();
        }

        protected override void OnContinue() {
            _logger.Info("Восстановление работы службы");
            EventLog.WriteEntry("Восстановление работы службы");

            _scheduler.ResumeAll();
            base.OnContinue();
        }

        protected override void OnStop() {
            _logger.Info("Остановка службы");
            EventLog.WriteEntry("Остановка службы");

            _scheduler.Shutdown();
        }
    }
}
