﻿using Ninject.Modules;

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService {
    using DataAccess.NHibernate;
    using DataAccess;

    public class Module : NinjectModule {
        public override void Load() {
            if (Kernel == null) {
                return;
            }

            Kernel.Bind<ISessionProvider>().To<SessionProvider>().InSingletonScope();
            Kernel.Bind<IUnitOfWorkFactory>().To<UnitOfWorkFactory>().InSingletonScope();
            Kernel.Bind<ISchedulerService>().To<SchedulerService>().InSingletonScope();
        }
    }
}
