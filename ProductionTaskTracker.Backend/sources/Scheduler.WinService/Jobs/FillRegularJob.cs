﻿using System;
using System.Linq;
using AutoMapper;
using Ninject.Extensions.Logging;
using Quartz;

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService.Jobs {
    using System.Data;
    using DataAccess.Repositories;
    using Domain.Entities;
    using Domain.Entities.TicketDetails;
    using Domain.Enumerables;
    using DataAccess;
    using Business;
    using DataAccess.Services;

    public class FillRegularJob: IJob {
        public void Execute(IJobExecutionContext context) {
            var logger = (ILogger)context.JobDetail.JobDataMap.Get("Logger");
            var unitOfWorkFactory = (IUnitOfWorkFactory) context.JobDetail.JobDataMap.Get("UnitOfWorkFactory");
            var membershipRepository = (IMembershipRepository)context.JobDetail.JobDataMap.Get("MembershipRepository");
            var ticketRepository = (ITicketRepository)context.JobDetail.JobDataMap.Get("TicketRepository");
            var registerService = (IRegisterService)context.JobDetail.JobDataMap.Get("RegisterService");

            logger.Info("Активация регулярной подачи заявок");

            try {
                using (var unitOfWork = unitOfWorkFactory.Create()) {
                    var membership = membershipRepository.GetWinServiceUser();
                    var ticketPendings = ticketRepository
                        .GetTicketPendings()
                        .Where(x => !x.SL.HasValue)
                        .Where(x => !x.IsNight)
                        .ToList();

                    logger.Info(ticketPendings.Count > 0
                        ? $"Обнаружено заявок: {ticketPendings.Count}"
                        : "Заявок не обнаружено.");

                    var tickets = ticketPendings.Select(ticketPending => {
                        var ticket = ticketRepository.Find(ticketPending.TicketID);
                        var ticketMovementLast = ticket.TicketMovements.OrderBy(x => x.Created).ThenBy(x => x.Id).Last();
                        var ticketDetail = ticketMovementLast.TicketDetail;

                        // Если уровень срочности задан и не задан явно срок исполнения заявки, необходимо обновить срок исполнения в соответствии с правилами
                        if (!ticketDetail.Expired.HasValue) {
                            var ticketDetailType = Utilities.GetTicketTypeFromEnumerable(ticket.TicketType);
                            ticketDetail = (TicketDetailBase)Mapper.Map(ticketDetail, ticketDetailType, ticketDetailType);
                            var ticketDetailB6 = ticketDetail as TicketDetailB6;
                            if (ticketDetailB6 != null) {
                                logger.Info($"ticketDetailB6: mixture: {ticketDetailB6.Mixture}, preparing: {ticketDetailB6.Preparing}");
                            }
                        }

                        var ticketMovement = new TicketMovement {
                            Membership = membership,
                            Department = ticketMovementLast.Department,
                            Position = ticketMovementLast.Position,
                            Ticket = ticketMovementLast.Ticket,
                            TicketDetail = ticketDetail,
                            Status = TicketStatus.Filed,
                            Created = DateTime.Now,
                            Deleted = ticketMovementLast.Deleted
                        };

                        ticket.TicketMovements.Add(ticketMovement);
                        return ticket;
                    });

                    foreach (var ticket in tickets) {
                        try {
                            ticketRepository.Create(ticket);
                            foreach (var ticketMovement in ticket.TicketMovements) {
                                registerService.PendingInclude(ticketMovement.Id);
                                logger.Info($"ticketMovement: id: {ticketMovement.Id}, status: {ticketMovement.Status}, created: {ticketMovement.Created}");
                            }
                            logger.Info($"Заявка №{ticket.Id}:{ticket.GetTicketTypeDescription()} [{ticket.TicketMovements.Last().Id}] для {ticket.Department.FullName} успешно переведена в статус {Utilities.GetTicketStatusDescription(TicketStatus.Filed)}");
                        } catch (Exception exception) {
                            logger.Error(exception, "Не удалось сформировать заявку");
                        }
                    }

                    registerService.CalculateRegister();
                    unitOfWork.Commit();
                }

                logger.Info("Подача регулярных заявок успешно завершена.");
            } catch (Exception exception) {
                logger.Error(exception, "Не удалось выполнить задание");
            }
        }
    }
}
