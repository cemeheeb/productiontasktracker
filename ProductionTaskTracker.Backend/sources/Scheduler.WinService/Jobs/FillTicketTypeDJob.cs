﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject.Extensions.Logging;
using Quartz;

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService.Jobs {
    using Domain.Entities.TicketDetails;
    using DataAccess.Repositories;
    using Domain.Entities;
    using Domain.Enumerables;
    using DataAccess;
    using Business;

    public class FillTicketTypeDJob: IJob {
        public void Execute(IJobExecutionContext context) {
            var logger = (ILogger)context.JobDetail.JobDataMap.Get("Logger");

            var unitOfWorkFactory = (IUnitOfWorkFactory)context.JobDetail.JobDataMap.Get("UnitOfWorkFactory");
            var membershipRepository = (IMembershipRepository)context.JobDetail.JobDataMap.Get("MembershipRepository");
            var ticketRepository = (ITicketRepository)context.JobDetail.JobDataMap.Get("TicketRepository");
            var controlledWellRepository = (IControlledWellRepository)context.JobDetail.JobDataMap.Get("ControlledWellRepository");

            logger.Info("Активация отложенной подачи заявок по скважинам на ВНР и подконтрольным скважинам");

            try {
                using (var unitOfWork = unitOfWorkFactory.Create()) {
                    var membership = membershipRepository.GetWinServiceUser();

                    var controlledWells = controlledWellRepository
                        .GetNotGeneratedAt(ControlledWellFilter.All, DateTime.Now.Date.AddHours(7).AddMinutes(30))
                        .ToList();

                    logger.Info(controlledWells.Count > 0
                        ? $"Обнаружено заявок: {controlledWells.Count}"
                        : "Заявок не обнаружено.");

                    var tickets = controlledWells
                        .Select(x => {
                            var ticket = new Ticket {
                                Created = DateTime.Now,
                                Deleted = false,
                                Department = x.Executor,
                                TicketType = x.TicketType
                            };

                            TicketDetailBase ticketDetail = null;
                            switch (x.TicketType) {
                                case TicketType.D1:
                                    ticketDetail = new TicketDetailD1 {
                                        Well = x.Well,
                                        RepairType = x.RepairType,
                                        EventType = x.EventType,
                                        Pump = x.Pump,
                                        PumpDepth = x.PumpDepth,
                                        Ticket = ticket,
                                        Membership = membership
                                    };
                                    break;
                                case TicketType.D2:
                                    ticketDetail = new TicketDetailD2 {
                                        Well = x.Well,
                                        RepairType = x.RepairType,
                                        EventType = x.EventType,
                                        Pump = x.Pump,
                                        PumpDepth = x.PumpDepth,
                                        Ticket = ticket,
                                        Membership = membership
                                    };
                                    break;
                            }

                            x.Recent = DateTime.Now;
                            controlledWellRepository.Update(x);

                            ticket.TicketMovements = new HashSet<TicketMovement> {
                                new TicketMovement {
                                    Created = DateTime.Now,
                                    Department = x.Department,
                                    Membership = x.Membership,
                                    Position = x.Position,
                                    Ticket = ticket,
                                    TicketDetail = ticketDetail,
                                    Status = TicketStatus.Created,
                                    Deleted = false
                                },
                                new TicketMovement {
                                    Created = DateTime.Now.AddSeconds(1),
                                    Department = x.Executor,
                                    Membership = x.Membership,
                                    Position = x.Position,
                                    Ticket = ticket,
                                    TicketDetail = ticketDetail,
                                    Status = TicketStatus.Filed,
                                    Deleted = false
                                }
                            };

                            return ticket;
                        });

                    foreach (var ticket in tickets) {
                        try {
                            ticketRepository.Create(ticket);
                            logger.Info($"Заявка №{ticket.Id}:{ticket.GetTicketTypeDescription()} для {ticket.Department.FullName} успешно зарегистрирована и переведена в статус {Utilities.GetTicketStatusDescription(TicketStatus.Filed)}");
                        } catch (Exception) {
                            logger.Info("Ошибка при подаче заявки по скважине ВНР или подконтрольной.");
                        }
                    }

                    unitOfWork.Commit();
                    logger.Info("Подача заявок по скважинам ВНР и подконтрольным успешно завершена.");
                }
            } catch (Exception exception) {
                logger.ErrorException("Ошибка выполнения задания по скважине ВНР или подконтрольной.", exception);
                throw;
            }
        }
    }
}
