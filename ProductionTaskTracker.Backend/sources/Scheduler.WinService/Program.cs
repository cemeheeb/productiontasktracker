﻿using System;
using System.Diagnostics;
using System.IO;
using Ninject;
using Ninject.Extensions.Logging;

#if (DEBUG)
    using System.Threading;
#else
    using System.ServiceProcess;    
#endif

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService {

    internal static class Program {
        private static void Main(string[] args) {
#if RELEASE_TEST
            var eventSourceName = "ProductionTaskTracker.Service.Test";
#else
            var eventSourceName = "ProductionTaskTracker.Service";
#endif

            if (!EventLog.SourceExists(eventSourceName))
                EventLog.CreateEventSource(eventSourceName, "Application");

            EventLog.WriteEntry(eventSourceName, "Зпуск");
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            var logger = CompositionRoot.Kernel.Get<ILoggerFactory>().GetLogger("trace");
            logger.Info("Вход в главную процедуру");
            EventLog.WriteEntry(eventSourceName, "Вход в главную процедуру");

            try {
                CompositionRoot.Wire(new Business.Module());
                CompositionRoot.Wire(new DataAccess.NHibernate.Module());
                CompositionRoot.Wire(new Module());

                var service = CompositionRoot.Resolve<ISchedulerService>();
#if DEBUG
                service.Initialize();
                while (true) {
                    Thread.Sleep(1000);
                }
#else
                var servicesToRun = new [] {
                    service as ServiceBase
                };

                ServiceBase.Run(servicesToRun);
#endif

                logger.Error("Работа службы прекращена. Смотрите журнал событий ОС");
            } catch (Exception exception) {
                EventLog.WriteEntry(eventSourceName, exception.Message + "\n\n" + exception.InnerException?.Message + exception.InnerException?.InnerException?.Message + "\n\n" + exception.StackTrace);
                logger?.Fatal(exception, exception.Message);
            }
        }
    }
}
