using System;
using Ninject;
using Ninject.Modules;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Scheduler.WinService {
    public class CompositionRoot {
        #region Fields

        private static StandardKernel _ninjectKernel;

        #endregion

        public static void Wire(NinjectModule module) {
            CreateKernel();
            _ninjectKernel.Load(module);
        }

        public static T Resolve<T>() {
            CreateKernel();
            return _ninjectKernel.Get<T>();
        }

        public static object Resolve(Type type) {
            CreateKernel();
            return _ninjectKernel.Get(type);
        }

        public static StandardKernel Kernel {
            get {
                CreateKernel();
                return _ninjectKernel;
            }
        }

        #region Internal

        private static void CreateKernel() {
            if (_ninjectKernel == null) {
                _ninjectKernel = new StandardKernel(new NinjectSettings { LoadExtensions = true });
            }
        }

        #endregion
    }
}