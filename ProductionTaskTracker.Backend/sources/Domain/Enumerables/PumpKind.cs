﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Тип насоса
    /// </summary>
    public enum PumpKind {
        // ШГН
        SGN,
        // ЭЦН
        ECN
    }
}