﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Вид мероприятия
    /// </summary>
    public enum WellEventType {
        // Смена
        Shift,
        // ОПЗ
        Cleaning
    }
}
