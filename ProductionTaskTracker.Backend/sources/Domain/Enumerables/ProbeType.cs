﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Тип пробы
    /// </summary>
    public enum ProbeType {
        Watercut,           // На обводненность
        SuspendedParticles, // На содержание КВЧ
        MultiComponent,     // 6-компонентные
        Carbonate,          // На карбонатную стабильность
        Inhibitor           // На обводненность
    }
}
