﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    [Flags]
    public enum TicketExtraStatus {
        // Ознакомленная
        Visited = 0x0,
        // Истёкшая
        Expired = 0x1
    }
}
