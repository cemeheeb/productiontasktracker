﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum RequestType {
        KVU,
        KVD,
        KPD
    }
}
