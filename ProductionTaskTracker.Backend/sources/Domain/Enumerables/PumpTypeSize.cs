﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Тип насоса
    /// </summary>
    public enum PumpTypeSize {
        // ФЛ
        Basic,
        // ФЛ + Пакер
        BasicPacker
    }
}