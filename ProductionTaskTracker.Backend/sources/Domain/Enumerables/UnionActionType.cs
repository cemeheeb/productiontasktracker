﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum UnionActionType {
        Installed,
        Uninstalled,
        Replaced
    }
}
