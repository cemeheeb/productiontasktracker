﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum BrigadeType {
        TRS,
        KRS,
        GIS,
        CNIPR,
        Chemical
    }
}
