﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum Research {
        KVU,
        KVD,
        NST,
        TKRS,
        Other
    }
}
