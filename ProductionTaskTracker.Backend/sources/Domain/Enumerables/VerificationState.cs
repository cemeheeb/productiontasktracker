﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Подтверждение
    /// </summary>
    public enum VerificationState {
        // Отклонено
        Rejected = 0,
        // Подтверждено
        Accepted = 1 
    }
}
