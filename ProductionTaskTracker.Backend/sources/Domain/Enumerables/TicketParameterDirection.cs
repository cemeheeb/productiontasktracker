﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    [Flags]
    public enum TicketParameterDirection {
        Forward = 0x1,
        Backward = 0x2,
        Both = Forward | Backward
    }
}
