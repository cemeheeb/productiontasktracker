﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum ClassifierKind {
        PT_,    // Классификатор типов съемников
        RT_,    // ЭЦН, ШГН
        RTE,    // ЭЦН
        RTS     // ШГН
    }
}
