﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum Order354AgentType {
        TKRS,
        KNPO,
        EPU
    }
}
