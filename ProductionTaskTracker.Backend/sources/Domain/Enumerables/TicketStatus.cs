﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
  [Flags]
  public enum TicketStatus {
    // Новая
    Created = 0,
    // Подано
    Filed = 1,
    // Принято
    Accepted = 2,
    // Отклонено
    Rejected = 3,
    // Согласованно
    Agreed = 4,
    // В работе
    Progress = 5,
    // Отработано
    ProgressReady = 6,
    // Подтвержденная - подтверждено линейным руководителем
    ProgressConfirmed = 7,
    // На доработке
    ProgressRejected = 8,
    // Выполненная - подтверждено руководителем
    Completed = 9,
    // Ознакомленная
    Visited = 12,
    // Снятая
    Cancelled = 13
  }
}
