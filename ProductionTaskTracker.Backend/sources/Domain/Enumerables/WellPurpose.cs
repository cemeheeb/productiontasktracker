﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum WellPurpose {
        Production,
        Injection,
        Water
    }
}
