﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum RequestedService {
        EPU,
        CNIPR
    }
}
