﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum WellPeriodMode {
        // КПР
        KPR,
        // АПВ
        APV
    }
}
