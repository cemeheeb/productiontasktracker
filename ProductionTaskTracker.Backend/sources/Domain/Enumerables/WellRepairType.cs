﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Вид ремонта
    /// </summary>
    public enum WellRepairType {
        // Текущий ремонт
        Support,
        // Капитальный ремонт
        Full,
        // Освоение
        Development,
        // ФХМУН
        Physical
    }
}
