﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum TreatmentType {
        WellRepair,
        CurveLevelRepair,
        Chemical
    }
}
