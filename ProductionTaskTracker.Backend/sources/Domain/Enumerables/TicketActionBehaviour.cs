﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum TicketActionBehaviour {
        Single,
        Group
    }
}
