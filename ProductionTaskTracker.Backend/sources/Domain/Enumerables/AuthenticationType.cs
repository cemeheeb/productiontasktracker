﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Метод авторизации
    /// </summary>
    public enum AuthenticationType {
        ActiveDirectory,    // Доменная авторизация
        Internal            // Внутренний механизм
    }
}