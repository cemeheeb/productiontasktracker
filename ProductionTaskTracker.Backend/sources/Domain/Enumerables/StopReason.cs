﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum StopReason {
        TKRS,
        Other
    }
}
