﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    public enum ControlledWellFilter {
        All,
        VNR,
        Other
    }
}
