﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// Территория отсыпки
    /// </summary>
    public enum WellArea {
        Prepare, // Подготовка
        Backfill // Отсыпка
    }
}
