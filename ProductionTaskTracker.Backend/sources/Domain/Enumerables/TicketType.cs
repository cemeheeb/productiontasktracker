namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables {
    /// <summary>
    /// ��� ������
    /// </summary>
    public enum TicketType {
        Undefined,
        A1, A2, A3, A4,
        B1, B1A, B1B, B2, B3, B4, B5, B6, B7, B8, B9, B10, B11, B12, B13, B14,
        C,
        D1, D2,
        E1, E2, E3, E4, E5, E6, E7, E8,
        F,
        G
    }
}
