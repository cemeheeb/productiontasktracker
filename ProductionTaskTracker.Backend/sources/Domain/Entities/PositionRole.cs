﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    public class PositionRole : Entity {
        /// <summary>
        /// Название
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Отображаемое название
        /// </summary>
        public virtual string DisplayName { get; set; }
    }
}