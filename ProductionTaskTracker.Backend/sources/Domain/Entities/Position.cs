﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {

    /// <summary>
    /// Должность
    /// </summary>
    public class Position: Entity {
        /// <summary>
        /// Подразделение
        /// </summary>
        public virtual Department Department { get; set; }
        
        /// <summary>
        /// Полное название должности
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Название для отображения
        /// </summary>
        public virtual string DisplayName { get; set; }

        /// <summary>
        /// Должностная роль
        /// </summary>
        public virtual PositionRole Role { get; set; }
    }
}
