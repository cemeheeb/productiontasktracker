﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Базовая сущность
    /// </summary>
    public abstract class Entity {
        public Entity() {}

        public Entity(long id) {
            Id = id;
        }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public virtual long Id { get; }

        /// <summary>
        /// Флаг сообщающий о том удалена ли запись
        /// </summary>
        public virtual bool Deleted { get; set; }

        #region Comparison

        public override bool Equals(object target) {
            if (target == null)
                return false;

            var entity = target as Entity;
            return entity?.Id.Equals(Id) ?? target == this;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        #endregion
    }
}
