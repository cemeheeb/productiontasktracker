﻿using System;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    public class TicketVisit: Entity {
        /// <summary>
        /// Детали заявки
        /// </summary>
        public virtual TicketDetailBase TicketDetail { get; set; }

        /// <summary>
        /// Движение заявки
        /// </summary>
        public virtual TicketMovement TicketMovement { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public virtual Position Position { get; set; }

        /// <summary>
        /// Дата просмотра
        /// </summary>
        public virtual DateTime Created { get; set; }

        /// <summary>
        /// Признак фактического просмотра
        /// </summary>
        public virtual bool IsViewed { get; set; }
    }
}
