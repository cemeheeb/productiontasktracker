﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    using Enumerables;

    public class Classifier {
        /// <summary>
        /// Код
        /// </summary>
        public virtual long Id { get; set; }

        /// <summary>
        /// Категория классификатора
        /// </summary>
        public virtual ClassifierKind Kind { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public virtual string Name { get; set; }

        #region Comparison

        public override bool Equals(object other) {
            var target = other as Classifier;

            if (ReferenceEquals(null, target)) {
                return false;
            }

            if (ReferenceEquals(this, target)) {
                return true;
            }

            return Id == target.Id && Kind == target.Kind;
        }

        public override int GetHashCode() {
            unchecked {
                int hash = GetType().GetHashCode();
                hash = (hash * 31) ^ Id.GetHashCode();
                hash = (hash * 31) ^ Kind.GetHashCode();

                return hash;
            }
        }

        #endregion
    }
}
