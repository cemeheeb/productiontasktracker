namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// ��������
    /// </summary>
    public abstract class Well : Entity {
        public Well() {}

        public Well(long id): base(id) {}

        /// <summary>
        /// ���
        /// </summary>
        public virtual Shop Shop { get; set; }

        /// <summary>
        /// �������������
        /// </summary>
        public virtual Field Field { get; set; }

        /// <summary>
        /// ��� �����������
        /// </summary>
        public virtual string EnterpriseCode { get; set; }

        /// <summary>
        /// ��� ��������
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// ����
        /// </summary>
        public virtual string Cluster { get; set; }
    }
}
