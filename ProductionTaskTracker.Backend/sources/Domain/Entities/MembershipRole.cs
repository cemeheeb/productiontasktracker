﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Роль пользователя системы
    /// </summary>
    public class MembershipRole : Entity {
        /// <summary>
        /// Название
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Отображаемое название
        /// </summary>
        public virtual string DisplayName { get; set; }
    }
}