﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    using Enumerables;

    /// <summary>
    /// Заявка
    /// </summary>
    public class Ticket : Entity {
        /// <summary>
        /// Тип заявки
        /// </summary>
        public virtual TicketType TicketType { get; set; }
        
        /// <summary>
        /// Идентификатор группы, если представлена
        /// </summary>
        public virtual long? GroupID { get; set; }

        /// <summary>
        /// Предприятие исполнитель
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// История движения заявки
        /// </summary>
        public virtual ISet<TicketMovement> TicketMovements { get; set; }

        /// <summary>
        /// Признак является ли заявка ночной
        /// </summary>
        public virtual bool IsNight { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime Created { get; set; } = DateTime.Now;
    }
}
