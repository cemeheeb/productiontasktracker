﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Базовая сущность
    /// </summary>
    public abstract class DictionaryEntity {
        /// <summary>
        /// Код
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public virtual string Name { get; set; }

        #region Comparison

        public override bool Equals(object target) {
            if (target == null)
                return false;

            var entity = target as DictionaryEntity;
            return entity?.Code.Equals(Code) ?? target == this;
        }

        public override int GetHashCode() {
            return Code.GetHashCode();
        }

        #endregion
    }
}
