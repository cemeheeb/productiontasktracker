﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    using Enumerables;

    /// <summary>
    /// Маршрут заявок
    /// </summary>
    public class TicketRoute: Entity {
        /// <summary>
        /// Класс заявки в форате строки 'ABCDEFG'
        /// </summary>
        public virtual string TicketClass { get; set; }

        /// <summary>
        /// Должность для которой применяется правило
        /// </summary>
        public virtual string PositionRole { get; set; }

        /// <summary>
        /// Статус для которого применяется правило
        /// </summary>
        public virtual TicketStatus? StatusFrom { get; set; }
        
        /// <summary>
        /// Статус назначается при движении по маршруту
        /// </summary>
        public virtual TicketStatus StatusTo { get; set; }

        /// <summary>
        /// Предприятие/Подразделение на которое распространяется правило
        /// </summary>
        public virtual Department DepartmentFrom { get; set; }

        /// <summary>
        /// Предприятие/Подразделение - пункт назначения передвижения заявки
        /// </summary>
        public virtual Department DepartmentTo { get; set; }

        /// <summary>
        /// Предприятие/Подразделение исполнитель
        /// </summary>
        public virtual Department Executor { get; set; }

        /// <summary>
        /// Дата начала действия маршрута
        /// </summary>
        public virtual DateTime DateBegin { get; set; }

        /// <summary>
        /// Дата окончания действия маршрута
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }
    }
}
