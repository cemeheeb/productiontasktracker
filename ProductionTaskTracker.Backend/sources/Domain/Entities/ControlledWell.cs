﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    using Enumerables;

    public class ControlledWell : Entity {
        /// <summary>
        /// Скважина
        /// </summary>
        public virtual WellRegular Well { get; set; }

        /// <summary>
        /// Заказчик
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// Исполнитель
        /// </summary>
        public virtual Department Executor { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public virtual Position Position { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// Тип заявки
        /// </summary>
        public virtual TicketType TicketType { get; set; }

        /// <summary>
        /// Тип ремонта
        /// </summary>
        public virtual WellRepairType RepairType { get; set; }

        /// <summary>
        /// Тип мероприятия
        /// </summary>
        public virtual WellEventType EventType { get; set; }

        /// <summary>
        /// Насос
        /// </summary>
        public virtual Pump Pump { get; set; }

        /// <summary>
        /// Глубина спуска насоса
        /// </summary>
        public virtual decimal PumpDepth { get; set; }

        /// <summary>
        /// Время последнего
        /// </summary>
        public virtual DateTime Recent { get; set; }
    }
}