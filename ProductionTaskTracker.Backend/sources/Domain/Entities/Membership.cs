﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Учетная запись пользователя системы
    /// </summary>
    public class Membership : Entity {
        /// <summary>
        /// Фактическое имя пользователя
        /// </summary>
        public virtual string FirstName { get; set; }

        /// <summary>
        /// Фактическая фамилия пользователя
        /// </summary>
        public virtual string LastName { get; set; }

        /// <summary>
        /// Фактическое отчество пользователя
        /// </summary>
        public virtual string Patronym { get; set; }

        /// <summary>
        /// Полное имя пользователя системы
        /// </summary>
        public virtual string FullName => $"{LastName} {FirstName} {Patronym}";

        /// <summary>
        /// Имя пользователя системы в формате: "фамилия + инициалы"
        /// </summary>
        public virtual string DisplayName => $"{LastName} {FirstName?.ToUpperInvariant()[0]}.{Patronym?.ToUpperInvariant()[0]}.";

        /// <summary>
        /// Идентификатор учетной записи в системе
        /// </summary>
        public virtual string UserName { get; set; }

        /// <summary>
        /// Пароль пользователя в локальной системе учетных записей (НЕ ActiveDirectory!)
        /// </summary>
        public virtual string Password { get; set; }

        /// <summary>
        /// Сгененрированный набор символов для усложнения процесса подбора пароля методом Bruteforce
        /// </summary>
        public virtual string PasswordSalt { get; set; }

        /// <summary>
        /// Список ролей закрепленных за пользователем
        /// </summary>
        public virtual ISet<MembershipRole> Roles { get; set; }

        /// <summary>
        /// Дата и время регистрации пользователя
        /// </summary>
        public  virtual DateTime Created { get; set; }
    }
}

