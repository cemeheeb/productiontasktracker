﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Подразделение
    /// </summary>
    public class Department : Entity {
        /// <summary>
        /// Родительское подразделение
        /// </summary>
        public virtual Department Parent { get; set; }

        /// <summary>
        /// Подразделение делегат
        /// </summary>
        public virtual Department Head { get; set; }

        /// <summary>
        /// Цех
        /// </summary>
        public virtual Shop Shop { get; set; }

        /// <summary>
        /// Полное название подразделения
        /// </summary>
        public virtual string FullName { get; set; }

        /// <summary>
        /// Отображаемое названия подразделение
        /// </summary>
        public virtual string DisplayName { get; set; }

        /// <summary>
        /// Подразделения
        /// </summary>
        public virtual IEnumerable<Department>  Children { get; set; }
    }
}
