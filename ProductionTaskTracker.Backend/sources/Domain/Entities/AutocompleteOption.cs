﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    public class AutocompleteOption: Entity {
        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual Membership Membership { get; set; }
        
        /// <summary>
        /// Название параметра
        /// </summary>
        public virtual string Parameter { get; set; }
        
        /// <summary>
        /// Значение опции
        /// </summary>
        public virtual string Text { get; set; }

        /// <summary>
        /// Вес
        /// </summary>
        public virtual int Weight { get; set; }

        #region Comparison

        protected bool Equals(AutocompleteOption other) {
            return Membership == other.Membership && Parameter == other.Parameter;
        }

        public override int GetHashCode() {
            return Membership.GetHashCode() % Parameter.GetHashCode();
        }

        #endregion
    }
}
