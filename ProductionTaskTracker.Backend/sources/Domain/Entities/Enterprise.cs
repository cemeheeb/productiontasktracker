﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Предприятие
    /// </summary>
    public class Enterprise : Entity {
        /// <summary>
        /// Код предприятия
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Полное название предприятия
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Сокращенное название предприятия
        /// </summary>
        public virtual string DisplayName { get; set; }
    }
}
