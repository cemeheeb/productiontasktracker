﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Утвержденный режим работы скважины
    /// </summary>
    public class WellExecutionMode {
        /// <summary>
        /// Дебит жидкости
        /// </summary>
        public virtual decimal Qj { get; set; }

        /// <summary>
        /// Дебит нефти
        /// </summary>
        public virtual decimal Qn { get; set; }

        /// <summary>
        /// Дата актуальности информации
        /// </summary>
        public virtual DateTime Timestamp { get; set; }
    }
}
