﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Снятие данных ТМС (внеплановые), ДМГ
    /// </summary>
    public class TicketDetailA4: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Напряжение
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "voltage")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Voltage { get; set; }

        /// <summary>
        /// Ток
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "amperage")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Amperage { get; set; }

        /// <summary>
        /// Загрузка
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "loading")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Loading { get; set; }

        /// <summary>
        /// Частота вращения
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "rotation_frequency")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency { get; set; }

        /// <summary>
        /// Давление среды
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_environment")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment { get; set; }

        /// <summary>
        /// Температура среды
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_environment")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironment { get; set; }

        /// <summary>
        /// Давление мотора
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_engine")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine { get; set; }

        /// <summary>
        /// Температура мотора
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_engine")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngine { get; set; }

        /// <summary>
        /// Тип ДМГ
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "dmg_type")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual string DMGType { get; set; }

        /// <summary>
        /// Тип съемника - в текстовом варианте
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual string Puller { get; set; }

        /// <summary>
        /// Тип съемника - в текстовом варианте
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Gathering { get; set; }
        
        /// <summary>
        /// Тип съемника - в текстовом варианте
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? SeparateExpense { get; set; }
        
        /// <summary>
        /// Тип съемника - в текстовом варианте
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? SeparateHumidity { get; set; }

        /// <summary>
        /// Тип съемника - в текстовом варианте
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? SeparatePressure { get; set; }

        /// <summary>
        /// Тип съемника - в текстовом варианте
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? SeparateTemperature { get; set; }
        
    }
}
