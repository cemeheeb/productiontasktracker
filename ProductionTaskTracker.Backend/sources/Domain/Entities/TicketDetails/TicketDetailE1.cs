﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Отбор проб на воду по графику
    /// </summary>
    public class TicketDetailE1: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время отборы
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? ProbeTime { get; set; }
    }
}
