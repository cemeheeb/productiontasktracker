using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;

    /// <summary>
    /// ���������� �������� ��� ���
    /// </summary>
    public class TicketDetailC : TicketDetailBase {
        /// <summary>
        /// �������������� ���� ���������� ������ ���
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime StartTimePlan { get; set; }

        /// <summary>
        /// ��� �������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual BrigadeType BrigadeType { get; set; }

        /// <summary>
        /// ����� �������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual int Brigade { get; set; }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual WellArea WellArea { get; set; }

        /// <summary>
        /// ����������� �������
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_static")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStatic { get; set; }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure { get; set; }

        /// <summary>
        /// ������� � ����������� �������� ���������
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "lock_available")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? LockAvailable { get; set; }

        /// <summary>
        /// ������� �������� ������������
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "service_area")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? ServiceArea { get; set; }

        /// <summary>
        /// ���������� ������� ���������
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "balance_delta")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? BalanceDelta { get; set; }
    }
}
