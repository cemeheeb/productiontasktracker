﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Проведение промывки ГО
    /// </summary>
    public class TicketDetailB4 : TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Объем промывки (м3)	
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? WashVolume { get; set; }

        /// <summary>
        /// Время начала проведения обработки (чч:мин)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_date_begin")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? TreatmentDateBegin { get; set; }

        /// <summary>
        /// Время окончания проведения обработки (чч:мин)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "treatment_date_end")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? TreatmentDateEnd { get; set; }

        /// <summary>
        /// Снятие ДМГ перед ГО
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "dmg_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? DMGBefore { get; set; }

        /// <summary>
        /// от начала закачки до момента начала падения давления (м3)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "volume_before_loss")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? VolumeBeforeLoss { get; set; }

        /// <summary>
        /// от начала падения давления до Рраб в коллекторе (м3)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "volume_before_collector")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? VolumeBeforeCollector { get; set; }

        /// <summary>
        /// Интервал фактических температур (Гр С)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_interval")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureInterval { get; set; }

        /// <summary>
        /// Давление на АДПМ начальное (Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_adpm_begin")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureADPMBegin { get; set; }

        /// <summary>
        /// Давление на АДПМ конечное (Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_adpm_end")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureADPMEnd { get; set; }

        /// <summary>
        /// Ручной замер дебита скважины(м3/сут)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "qj_manual")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? QjManual { get; set; }
    }
}
