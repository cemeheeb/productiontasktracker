﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Отбор проб (внеплановые)
    /// </summary>
    public class TicketDetailA3: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// На обводненность
        /// </summary>
        [TicketDetailPropertyRequestable(Name="watering")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Watering { get; set; }

        /// <summary>
        /// На содержание КВЧ
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "kvch")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Kvch { get; set; }

        /// <summary>
        /// 6-и компонентные
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "multi_component")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? MultiComponent { get; set; }

        /// <summary>
        /// На карбонатную стабильность
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "carbonate")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Carbonate { get; set; }

        /// <summary>
        /// На остаточное содержание ингибитора
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "inhibitor")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Inhibitor { get; set; }
        
        /// <summary>
        /// На остаточное содержание нефтепродуктов
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "oil_component")]
        public virtual bool? OilComponent { get; set; }

        /// <summary>
        /// Комплексный анализ
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "complex")]
        public virtual bool? Complex { get; set; }


        /// <summary>
        /// Объем пробы
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        [TicketDetailPropertyRequestable(Name = "probe_volume")]
        public virtual decimal? ProbeVolume { get; set; }
    }
}
