﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Остановка скважины по техническим причинам
    /// </summary>
    public class TicketDetailB9 : TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Причина остановки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual string StopReason { get; set; }

        /// <summary>
        /// Индикатор отработки по причине остановки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Worked { get; set; }
    }
}
