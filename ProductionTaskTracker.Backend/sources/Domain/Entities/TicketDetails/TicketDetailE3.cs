﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    public class TicketDetailE3 : TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время отбора
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? ProbeTime { get; set; }

        /// <summary>
        /// Тип съёмника
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual Classifier PullerType { get; set; }

        /// <summary>
        /// Напряжение
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Voltage { get; set; }

        /// <summary>
        /// Ток
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Amperage { get; set; }

        /// <summary>
        /// Загрузка
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Loading { get; set; }

        /// <summary>
        /// Частота вращения
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency { get; set; }

        /// <summary>
        /// Давление среды
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment { get; set; }

        /// <summary>
        /// Давление масла двигателя
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine { get; set; }

        /// <summary>
        /// Температура среды
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironment { get; set; }

        /// <summary>
        /// Температура масла двигателя
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngine { get; set; }

        /// <summary>
        /// Время и режим работы
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Duration { get; set; }
    }
}
