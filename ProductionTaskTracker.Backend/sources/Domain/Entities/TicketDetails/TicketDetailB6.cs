﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Хим. обработка КЗХ
    /// </summary>
    public class TicketDetailB6: TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Подготовка скважины
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "preparing")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Preparing { get; set; }

        /// <summary>
        /// ВУС на затрубе
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "mixture")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Mixture { get; set; }
    }
}
