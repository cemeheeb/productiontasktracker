﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Комиссионый разбор АГЗУ
    /// </summary>
    public class TicketDetailB10: TicketDetailBase, ITicketDetailTargetMeasureFacility, ITimeCompletionProvider {
        /// <summary>
        /// Цех
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual Shop Shop { get; set; }

        /// <summary>
        /// Позиция АГЗУ
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual int MeasureFacilityPosition { get; set; }

        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Причина вызова
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual string RequestReason { get; set; }
    }
}
