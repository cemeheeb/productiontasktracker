namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;

    public class TicketDetailA1 : TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// ����� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "qj")]
        public virtual decimal? Qj { get; set; }

        /// <summary>
        /// �������� ������������ ���
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "equipment_verification")]
        public virtual VerificationState? EquipmentVerification { get; set; }

        /// <summary>
        /// �������� ��������� �������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "valve_verification")]
        public virtual VerificationState? ValveVerification { get; set; }

        /// <summary>
        /// ����� �������� �� �������� ��
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "qj_locked")]
        public virtual decimal? QjLocked { get; set; }

        /// <summary>
        /// ����� �������� ����� ������ �����
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "qj_redirect")]
        public virtual decimal? QjRedirect { get; set; }

        /// <summary>
        /// ���������� ���
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "crimping")]
        public virtual bool? Crimping { get; set; }

        /// <summary>
        /// �������� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "pressure_buffer")]
        public virtual decimal? PressureBuffer { get; set; }

        /// <summary>
        /// ������������ �������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "h_dynamic")]
        public virtual decimal? HDynamic { get; set; }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "pressure")]
        public virtual decimal? Pressure { get; set; }

        /// <summary>
        /// ���
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "amperage")]
        public virtual decimal? Amperage { get; set; }

        /// <summary>
        /// ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "loading")]
        public virtual decimal? Loading { get; set; }

        /// <summary>
        /// ������� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "rotation_frequency")]
        public virtual decimal? RotationFrequency { get; set; }

        /// <summary>
        /// �������� �����
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "pressure_environment")]
        public virtual decimal? PressureEnvironment { get; set; }

        /// <summary>
        /// ����������� �����
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "temperature_environment")]
        public virtual decimal? TemperatureEnvironment { get; set; }

        /// <summary>
        /// �������� ����� ���������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "pressure_engine")]
        public virtual decimal? PressureEngine { get; set; }

        /// <summary>
        /// ����������� ����� ���������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "temperature_engine")]
        public virtual decimal? TemperatureEngine { get; set; }

        /// <summary>
        /// ������� ���
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "revision_svu")]
        public virtual bool? RevisionSVU { get; set; } 

        /// <summary>
        /// ������ ���
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "replace_svu")]
        public virtual bool? ReplaceSVU { get; set; }


        /// <summary>
        /// �������� ��������� ������������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        [TicketDetailPropertyRequestable(Name = "ground_equipment_verification")]
        public virtual VerificationState? GroundEquipmentVerification { get; set; }
    }
}
