﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    public class TicketDetailE7 : TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        public virtual DateTime? TimeCompletion { get; set; }

        /// <summary>
        /// Нд (от 0-3000м.)
        /// </summary>
        public virtual decimal? HDynamic { get; set; }

        /// <summary>
        /// Рзатр (число от 0-70)
        /// </summary>
        public virtual decimal? Pressure { get; set; }
    }
}

