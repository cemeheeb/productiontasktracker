﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Комиссионый разбор СВУ
    /// </summary>
    public class TicketDetailB11: TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Причина остановки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual string RequestReason { get; set; }
    }
}
