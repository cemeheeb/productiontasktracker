﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Проведение обработок скребком по графику
    /// </summary>
    public class TicketDetailE4: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Дата и время обработки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? TreatmentTime { get; set; }
    }
}
