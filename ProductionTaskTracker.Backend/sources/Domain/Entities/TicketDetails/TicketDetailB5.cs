﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Глушение скважины
    /// </summary>
    public class TicketDetailB5: TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Объём
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal DampingVolume { get; set; }

        /// <summary>
        /// Удельгый вес
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal Weight { get; set; }

        /// <summary>
        /// Количество циклов
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual int Cycles { get; set; }

        /// <summary>
        /// Объем глушения 1 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingVolumeCycleA { get; set; }

        /// <summary>
        /// Удельный вес 1 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingWeightCycleA { get; set; }

        /// <summary>
        /// Объем глушения 2 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingVolumeCycleB { get; set; }

        /// <summary>
        /// Удельный вес 2 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingWeightCycleB { get; set; }

        /// <summary>
        /// Объем глушения 3 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingVolumeCycleC { get; set; }

        /// <summary>
        /// Удельный вес 3 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingWeightCycleC { get; set; }

        /// <summary>
        /// Объем глушения 4 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingVolumeCycleD { get; set; }

        /// <summary>
        /// Удельный вес 4 цикла
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? DampingWeightCycleD { get; set; }

        /// <summary>
        /// Замерить Ризб после глушения (Мпа)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_after_damping")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? PressureAfterDamping { get; set; }

        /// <summary>
        /// Отобрать конечную пробу жидкости глушения	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "liquid_probe")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? LiquidProbe { get; set; }
    }
}
