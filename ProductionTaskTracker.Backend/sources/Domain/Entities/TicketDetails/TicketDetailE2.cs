﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Отбор проб на КВЧ по графику
    /// </summary>
    public class TicketDetailE2: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время отбора
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? ProbeTime { get; set; }
    }
}
