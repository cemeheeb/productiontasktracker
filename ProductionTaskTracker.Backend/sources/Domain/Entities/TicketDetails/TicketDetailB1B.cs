﻿using System;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Запуск скважины
    /// </summary>
    public class TicketDetailB1B: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время запуска
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual DateTime StartTime { get; set; }

        // Тип обработки
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual TreatmentType Treatment { get; set; }

        // Тип обработки
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual WellRepairType RepairType { get; set; }

        // Типоразмер ГНО
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual PumpTypeSize PumpTypeSize { get; set; }

        // Диаметр штуцера
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal? UnionDiameter { get; set; }

        // Глубина спуска ГНО
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal PumpDepth { get; set; }

        // Объем
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal DampingVolume { get; set; }

        // Удельный вес
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal DampingWeight { get; set; }
    }
}

