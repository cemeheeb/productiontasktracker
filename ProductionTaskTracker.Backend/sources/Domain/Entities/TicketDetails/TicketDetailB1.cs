﻿using System;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Запуск скважины
    /// </summary>
    public class TicketDetailB1: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время запуска
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual DateTime StartTime { get; set; }

        // Дебит
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Q { get; set; }

        // Hст
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStatic { get; set; }

        // Pзатр, P
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure { get; set; }

        // Pсреды
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment { get; set; }

        // Pмасла
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine { get; set; }

        // Tсреды
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironment { get; set; }

        // Tмасла
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngine { get; set; }

        // Ток
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Amperage { get; set; }

        // Загрузка
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Loading { get; set; }

        // Частота вращения
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency { get; set; }

        // Hдин через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HDynamic15 { get; set; }

        // Pзатр, P через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure15 { get; set; }

        // Дебит через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Q15 { get; set; }

        // Pсреды через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment15 { get; set; }

        // Pмасла через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine15 { get; set; }

        // Tсреды через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironment15 { get; set; }

        // Tмасла через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngine15 { get; set; }

        // Ток через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Amperage15 { get; set; }

        // Загрузка через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Loading15 { get; set; }

        // Частота вращения через 15 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency15 { get; set; }

        // Hдин через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HDynamic30 { get; set; }

        // Pзатр, P через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure30 { get; set; }

        // Дебит через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Q30 { get; set; }

        // Pсреды через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment30 { get; set; }

        // Pмасла через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine30 { get; set; }

        // Tсреды через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironment30 { get; set; }

        // Tмасла через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngine30 { get; set; }

        // Ток через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Amperage30 { get; set; }

        // Загрузка через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Loading30 { get; set; }

        // Частота вращения через 30 мин
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency30 { get; set; }

        /// <summary>
        /// Тип ремонта
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual WellRepairType RepairType { get; set; }

        /// <summary>
        /// ГНО
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual Pump Pump { get; set; }

        /// <summary>
        /// Глубина спуска ГНО
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal? PumpDepth { get; set; }

        /// <summary>
        /// Объем
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal? DampingVolume { get; set; }

        /// <summary>
        /// Удельный вес
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal? DampingWeight { get; set; }

        /// <summary>
        /// Минимально допустимый динамический уровень
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal HDynamicMinimal { get; set; }

        /// <summary>
        /// Назначение скважины "Добывающая", "Нагнетательная", "Водозаборная"
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual WellPurpose WellPurpose { get; set; }

        /// <summary>
        /// После чего 'КВУ', 'КВД', 'НСТ', 'ТиКРС', 'Прочее'
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual Research Research { get; set; }

        /// <summary>
        /// Длина хвостовика
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal ShankLength { get; set; }

        /// <summary>
        /// Минимально допустимое давление на приеме УЭЦН
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal PressureEcnMinimal { get; set; }
    }
}
