﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Отбивка уровней по графику
    /// </summary>
    public class TicketDetailE6: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Дата и время отбивки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? PaddingTime { get; set; }
    }
}
