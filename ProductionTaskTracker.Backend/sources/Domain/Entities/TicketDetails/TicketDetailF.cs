﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Информационная заявка от цеха в ЦИО
    /// </summary>
    public class TicketDetailF: TicketDetailBase, ITicketDetailTargetWell {
    }
}
