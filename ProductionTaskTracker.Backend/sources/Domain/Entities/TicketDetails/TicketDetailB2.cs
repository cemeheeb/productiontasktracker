﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Проведение указания №354
    /// </summary>
    public class TicketDetailB2: TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Дата и время старта
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Объём
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal WashVolume { get; set; }

        /// <summary>
        /// Удельгый вес
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal Weight { get; set; }

        /// <summary>
        /// Вызов представителей ТиКРС, КНПО-С, ЭПУ ...
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual string CalledAgents { get; set; }

        /// <summary>
        /// Ручной замер дебита скважины
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "qj_manual_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? QjManualBefore { get; set; }

        /// <summary>
        /// Ток раб (А)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "amperage_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? AmperageBefore { get; set; }

        /// <summary>
        /// Загрузка (%)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "loading_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? LoadingBefore { get; set; }

        /// <summary>
        /// Частота вращения ( об/мин.)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "rotation_frequency")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency { get; set; }

        /// <summary>
        /// Рсреды до
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_environment_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironmentBefore { get; set; }

        /// <summary>
        /// Рмасла до
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_engine_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngineBefore { get; set; }

        /// <summary>
        /// Тсреды(ГрС) до
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_environment_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironmentBefore { get; set; }

        /// <summary>
        /// Тмасла(ГрС) до
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_engine_before")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngineBefore { get; set; }

        /// <summary>
        /// Смена вращения
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "rotation_change")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual string RotationChange { get; set; }

        /// <summary>
        /// Проверка фазировки
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "phase_check")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual string PhaseCheck { get; set; }

        /// <summary>
        /// Нст после остановки ГНО(м)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_static_after_stop")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStaticAfterStop { get; set; }

        /// <summary>
        /// Опрессовка Рнач(Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "crimping_begin")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? CrimpingBegin { get; set; }

        /// <summary>
        /// Опрессовка Ркон(Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "crimping_end")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? CrimpingEnd { get; set; }

        /// <summary>
        /// Время опрессовки(мин)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "crimping_duration")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? CrimpingDuration { get; set; }

        /// <summary>
        /// Нст после опрессовки ГНО(м)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_static_after_crimping")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStaticAfterCrimping { get; set; }

        /// <summary>
        /// Рнач промывки(Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "wash_pressure_begin")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? WashPressureBegin { get; set; }

        /// <summary>
        /// Ркон промывки(Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "wash_pressure_end")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? WashPressureEnd { get; set; }

        /// <summary>
        /// Ндин/Рзатр после промывки(м/Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_dynamic_pressure_after_wash")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HDynamicPressureAfterWash { get; set; }

        /// <summary>
        /// Время Ндин/Рзатр( 15мин м/Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_dynamic_pressure_duration")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HDynamicPressureDuration { get; set; }

        /// <summary>
        /// Ручной замер дебита скважины(м3/сут)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "qj_manual_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? QjManualAfter { get; set; }

        /// <summary>
        /// Ток раб(А)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "amperage_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? AmperageAfter { get; set; }

        /// <summary>
        /// Напряжение(В)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "voltage_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? VoltageAfter { get; set; }

        /// <summary>
        /// Рсреды после
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_environment_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironmentAfter { get; set; }

        /// <summary>
        /// Рмасла после
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_engine_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngineAfter { get; set; }

        /// <summary>
        /// Тсреды(ГрС) после
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_environment_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironmentAfter { get; set; }

        /// <summary>
        /// Тмасла(ГрС) после
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_engine_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngineAfter { get; set; }

        /// <summary>
        /// Промывка ГНО
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pump_washing")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? PumpWashing { get; set; }

        /// <summary>
        /// Опрессовка ГНО
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pump_crimping")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? PumpCrimping { get; set; }
    }
}
