﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;
    using Interfaces;

    /// <summary>
    /// Работа с ЭПУ
    /// </summary>
    public class TicketDetailB13: TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Дебит
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "q")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Q { get; set; }

        /// <summary>
        /// Динамический уровень
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_dynamic")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual string HDynamic { get; set; }

        /// <summary>
        /// Pзатр
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure { get; set; }

        /// <summary>
        /// Ток раб (А)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "amperage")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Amperage { get; set; }

        /// <summary>
        /// Напряжение (В)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "voltage")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Voltage { get; set; }

        /// <summary>
        /// Загрузка (%)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "loading")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Loading { get; set; }

        /// <summary>
        /// Частота вращения ( об/мин.)	
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "rotation_frequency")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? RotationFrequency { get; set; }

        /// <summary>
        /// Рсреды
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_environment")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment { get; set; }

        /// <summary>
        /// Рмасла
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_engine")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine { get; set; }

        /// <summary>
        /// Тсреды
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_environment")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEnvironment { get; set; }

        /// <summary>
        /// Тмасла
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "temperature_engine")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? TemperatureEngine { get; set; }

        /// <summary>
        /// Корректировка защиты
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "protection_correction")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? ProtectionCorrection { get; set; }

        /// <summary>
        /// Смена вращения
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "rotation_change")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual string RotationChange { get; set; }

        /// <summary>
        /// Дебит на обратном вращении (м3/сут)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "qj_reverse")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? QjReverse { get; set; }

        /// <summary>
        /// Ток раб на обратном вращении (А)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "amperage_reverse")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? AmperageReverse { get; set; }

        /// <summary>
        /// Загрузка на обрат.вращении (%)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "loading_reverse")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? LoadingReverse { get; set; }
    }
}
