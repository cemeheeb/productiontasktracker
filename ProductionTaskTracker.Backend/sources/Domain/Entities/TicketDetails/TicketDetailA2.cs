using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// ��������� ������ ������ �������
    /// </summary>
    public class TicketDetailA2 : TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// �������� ��� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual UnionActionType? UnionAction { get; set; }

        /// <summary>
        /// ��������� ����� ������� ��
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? RockFrequency { get; set; }

        /// <summary>
        /// ��������� ������� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? RotationFrequencyDelta { get; set; }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual decimal? Wheeling { get; set; }

        /// <summary>
        /// �������� �������
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal? UnionDynamic { get; set; }

        /// <summary>
        /// ��������� ����� ���� �������� ���� [�]
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual decimal? PlungerLength { get; set; }

        /// <summary>
        /// ����� ������ ������������� ������� (��� ���)
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual WellPeriodMode? WellPeriodMode { get; set; }
    }
}
