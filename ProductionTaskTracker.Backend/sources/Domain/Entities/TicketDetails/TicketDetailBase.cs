﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using System.Linq;
    using Enumerables;

    /// <summary>
    /// Базовый класс - заявка
    /// </summary>
    public abstract class TicketDetailBase: Entity {
        /// <summary>
        /// Скважина
        /// </summary>
        public virtual WellRegular Well { get; set; }

        /// <summary>
        /// Меторождение
        /// </summary>
        public virtual Field Field { get; set; }

        /// <summary>
        /// Куст
        /// </summary>
        public virtual string Cluster { get; set; }

        /// <summary>
        /// Заявка
        /// </summary>
        public virtual Ticket Ticket { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// SL Уровень
        /// </summary>
        public virtual int? SL { get; set; }

        /// <summary>
        /// Дата устаревания
        /// </summary>
        public virtual DateTime? Expired { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual string Note { get; set; }

        /// <summary>
        /// Поля для заполнения по запросу
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual string Requested { get; set; }

        /// <summary>
        /// Идентификатор вложения документов
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual string AttachmentID { get; set; }
    }
}
