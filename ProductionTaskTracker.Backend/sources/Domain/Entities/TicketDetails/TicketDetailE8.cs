﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    public class TicketDetailE8 : TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        public virtual DateTime? TimeCompletion { get; set; }
        
        /// <summary>
        /// Проведена обработка скребком
        /// </summary>
        public virtual decimal? Scraped { get; set; }

        /// <summary>
        /// Осложнения при обработке
        /// </summary>
        public virtual string ScrapProblem { get; set; }
    }
}

