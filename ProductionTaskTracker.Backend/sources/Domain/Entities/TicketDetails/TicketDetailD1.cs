
namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Enumerables;

    /// <summary>
    /// �������� ���
    /// </summary>
    public class TicketDetailD1 : TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// ��� �������
        /// </summary>
        public virtual WellRepairType RepairType { get; set; }

        /// <summary>
        /// ��� �����������
        /// </summary>
        public virtual WellEventType EventType { get; set; }

        /// <summary>
        /// �����
        /// </summary>
        public virtual Pump Pump { get; set; }

        /// <summary>
        /// ������� ������ ������
        /// </summary>
        public virtual decimal PumpDepth { get; set; }

        /// <summary>
        /// ����� ��������
        /// </summary>
        public virtual decimal? Qj { get; set; }

        /// <summary>
        /// ������������ �������
        /// </summary>
        public virtual decimal? HDynamic { get; set; }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        public virtual decimal? Pressure { get; set; }

        /// <summary>
        /// ���
        /// </summary>
        public virtual decimal? Amperage { get; set; }

        /// <summary>
        /// ��������
        /// </summary>
        public virtual decimal? Loading { get; set; }

        /// <summary>
        /// ������� ��������
        /// </summary>
        public virtual decimal? RotationFrequency { get; set; }

        /// <summary>
        /// �������� �����
        /// </summary>
        public virtual decimal? PressureEnvironment { get; set; }

        /// <summary>
        /// �������� ����� ���������
        /// </summary>
        public virtual decimal? PressureEngine { get; set; }

        /// <summary>
        /// ����������� �����
        /// </summary>
        public virtual decimal? TemperatureEnvironment { get; set; }

        /// <summary>
        /// ����������� ����� ���������
        /// </summary>
        public virtual decimal? TemperatureEngine { get; set; }

        /// <summary>
        /// ����� � ����� ������
        /// </summary>
        public virtual decimal? Duration { get; set; }

        /// <summary>
        /// ����� ������
        /// </summary>
        public virtual decimal? OperationMode { get; set; }
    }
}
