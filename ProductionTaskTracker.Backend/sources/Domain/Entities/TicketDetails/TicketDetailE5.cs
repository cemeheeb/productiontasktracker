﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Снятие динамограмм по графику
    /// </summary>
    public class TicketDetailE5: TicketDetailBase, ITicketDetailTargetWell {
        /// <summary>
        /// Дата и время снятия
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? ObtainTime { get; set; }
    }
}
