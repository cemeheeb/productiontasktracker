﻿using System;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    using Interfaces;

    /// <summary>
    /// Остановка скажины на исследование
    /// </summary>
    public class TicketDetailB8: TicketDetailBase, ITicketDetailTargetWell, ITimeCompletionProvider {
        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Both)]
        public virtual DateTime TimeCompletion { get; set; }

        /// <summary>
        /// Тип заявки
        /// </summary>
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual RequestType RequestType { get; set; }

        /// <summary>
        /// Ндин (м)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_dynamic")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HDynamic { get; set; }

        /// <summary>
        /// Рзатр (Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure { get; set; }

        /// <summary>
        /// Нст (м)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "h_static")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStatic { get; set; }

        /// <summary>
        /// Рзатр через 15 минут (Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_after")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureAfter { get; set; }

        /// <summary>
        /// Оключение питания СУ (да/нет)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "power_off")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? PowerOff { get; set; }

        /// <summary>
        /// прокачка (продувка) коллектора в зимний период
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "purge")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? Purge { get; set; }

        /// <summary>
        /// Вызов ЭПУ сервиса
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "requested_epu")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? RequestedEPU { get; set; }

        /// <summary>
        /// Вызов специалистов ЦНИПР
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "requested_cnipr")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual bool? RequestedCNIPR { get; set; }

        /// <summary>
        /// Ризб(ыточное) (Мпа)
        /// </summary>
        [TicketDetailPropertyRequestable(Name = "pressure_redundant")]
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureRedundant { get; set; }
    }
}
