﻿using System;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails {
    /// <summary>
    /// Запуск скважины
    /// </summary>
    public class TicketDetailB1A: TicketDetailBase, ITicketDetailTargetWell {
        /// Время запуска
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual DateTime StartTime { get; set; }
        
        /// Тип запроса
        [TicketParameterDirection(Direction = TicketParameterDirection.Forward)]
        public virtual RequestType RequestType { get; set; }

        /// Время запуска факт
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual DateTime? StartTimeFact { get; set; }

        // Hст
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStatic { get; set; }

        // Pзатр, P
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? Pressure { get; set; }

        // Pсреды
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEnvironment { get; set; }

        // Pмасла
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressureEngine { get; set; }

        // Hст в затрубе
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? HStaticPipe { get; set; }

        // Pизб в затрубе
        [TicketParameterDirection(Direction = TicketParameterDirection.Backward)]
        public virtual decimal? PressurePipe { get; set; }
    }
}

