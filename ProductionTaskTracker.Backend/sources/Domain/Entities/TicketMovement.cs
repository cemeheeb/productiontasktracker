using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    using TicketDetails;
    using Enumerables;

    /// <summary>
    /// ��� �������� ������
    /// </summary>
    public class TicketMovement : Entity {
        /// <summary>
        /// ������
        /// </summary>
        public virtual Ticket Ticket { get; set; }

        /// <summary>
        /// ������� ������ �����������
        /// </summary>
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// ��������� �����������
        /// </summary>
        public virtual Position Position { get; set; }

        /// <summary>
        /// ������������� �����������/�������������
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// ������ �� ������
        /// </summary>
        public virtual TicketDetailBase TicketDetail { get; set; }

        /// <summary>
        /// �������� ������ ������
        /// </summary>
        public virtual TicketStatus Status { get; set; }

        /// <summary>
        /// ���� � ����� ��������
        /// </summary>
        public virtual DateTime Created { get; set; } = DateTime.Now;

        public override bool Equals(object target) {
            return base.Equals(target);
        }

        protected bool Equals(TicketMovement other) {
            return base.Equals(other) && Equals(Ticket, other.Ticket) && Created.Equals(other.Created);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = base.GetHashCode();
                hashCode = (hashCode*397) ^ (Ticket != null ? Ticket.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Created.GetHashCode();
                return hashCode;
            }
        }
    }
}
