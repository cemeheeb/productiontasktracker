﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Скважина
    /// </summary>
    public class WellRegular: Well {
        public WellRegular() {

        }

        public WellRegular(long id): base(id) {
        }
    }
}
