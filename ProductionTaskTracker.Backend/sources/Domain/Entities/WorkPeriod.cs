﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// Рабочий период
    /// </summary>
    public class WorkPeriod: Entity {
        /// <summary>
        /// Учетная запись пользователя
        /// </summary>
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public virtual Position Position { get; set; }

        /// <summary>
        /// Дата начала действия должности
        /// </summary>
        public virtual DateTime DateBegin { get; set; }

        /// <summary>
        /// Дата окончания действия должности
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }
    }
}
