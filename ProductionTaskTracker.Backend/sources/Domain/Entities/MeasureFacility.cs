﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    /// <summary>
    /// АГЗУ
    /// </summary>
    public class MeasureFacility: Entity {
        /// <summary>
        /// Цех
        /// </summary>
        public virtual Shop Shop { get; set; }

        /// <summary>
        /// Месторождение
        /// </summary>
        public virtual Field Field { get; set; }

        /// <summary>
        /// Куст
        /// </summary>
        public virtual string Cluster { get; set; }

        /// <summary>
        /// Позиция
        /// </summary>
        public virtual int Position { get; set; }
    }
}
