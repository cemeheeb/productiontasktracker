namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Entities {
    using Enumerables;

    /// <summary>
    /// �����. ����� ��������� � �� ��� �������
    /// </summary>
    public class Pump : Entity {
        /// <summary>
        /// ������������
        /// </summary>
        public virtual string Title { get; set; }
        
        /// <summary>
        /// ��� ����
        /// </summary>
        public virtual string TypeECN { get; set; }

        /// <summary>
        /// ��
        /// </summary>
        public virtual string Gb { get; set; }

        /// <summary>
        /// ����������� �����
        /// </summary>
        public virtual long NominalFlow { get; set; }

        /// <summary>
        /// ����������� ������
        /// </summary>
        public virtual long NominalFeed { get; set; }

        /// <summary>
        /// �������������� ���������� 1
        /// </summary>
        public virtual long Col1 { get; set; }

        /// <summary>
        /// �������������� ���������� 2
        /// </summary>
        public virtual long Col2 { get; set; }

        /// <summary>
        /// �������������� ���������� 3
        /// </summary>
        public virtual long Col3 { get; set; }

        /// <summary>
        /// �������������
        /// </summary>
        public virtual string Factory { get; set; }

        /// <summary>
        /// ������
        /// </summary>
        public virtual string Country { get; set; }
    }
}