﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.DTO {
    using System;

    public class TicketMovementNotification {
        /// <summary>
        /// Код заявки
        /// </summary>
        public virtual long TicketID { get; set; }

        /// <summary>
        /// Код детали заявки
        /// </summary>
        public virtual long TicketDetailID { get; set; }

        /// <summary>
        /// Код движения заявки
        /// </summary>
        public virtual long TicketMovementID { get; set; }

        /// <summary>
        /// Тип заявки
        /// </summary>
        public virtual string TicketType { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// Дата и время движения заявки
        /// </summary>
        public virtual DateTime Created { get; set; }
    }
}
