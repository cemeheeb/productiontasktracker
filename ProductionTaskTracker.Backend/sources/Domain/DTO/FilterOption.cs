﻿
namespace EAEConsult.ProductionTaskTracker.Backend.Domain.DTO {
    public class FilterOption {
        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }
    }
}
