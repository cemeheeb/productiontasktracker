﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.DTO {
    using System;
    using Newtonsoft.Json;

    public class TicketFilter {
        /// <summary>
        /// Код типа заявки
        /// </summary>
        [JsonProperty(PropertyName = "ticket_type")]
        public string TicketType { get; set; }

        /// <summary>
        /// Наименование типа заявки
        /// </summary>
        //[JsonProperty(PropertyName = "ticket_type_name")]
        //public string TicketTypeName { get; set; }

        /// <summary>
        /// Код статуса заявки
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public int? TicketStatus { get; set; }

        /// <summary>
        /// Наименование статуса заявки
        /// </summary>
        //[JsonProperty(PropertyName = "status_name")]
        //public string TicketStatusName { get; set; }

        /// <summary>
        /// Наименование предприятия заказчика
        /// </summary>
        //[JsonProperty(PropertyName = "department_name_from")]
        //public string DepartmentNameFrom { get; set; }

        /// <summary>
        /// Идентификатор предприятия заказчика
        /// </summary>
        [JsonProperty(PropertyName = "department_from")]
        public long? DepartmentIDFrom { get; set; }

        /// <summary>
        /// Наименование предприятия исполнителя
        /// </summary>
        //[JsonProperty(PropertyName = "department_name_to")]
        //public string DepartmentNameTo { get; set; }

        /// <summary>
        /// Идентификатор предприятия исполнителя
        /// </summary>
        [JsonProperty(PropertyName = "department_to")]
        public long? DepartmentIDTo { get; set; }

        /// <summary>
        /// Наименование месторождения
        /// </summary>
        [JsonProperty(PropertyName = "field")]
        public string FieldCode { get; set; }

        /// <summary>
        /// Наименование месторождения
        /// </summary>
        //[JsonProperty(PropertyName = "field_name")]
        //public string FieldName { get; set; }

        /// <summary>
        /// Код скважины
        /// </summary>
        [JsonProperty(PropertyName = "well")]
        public long? WellID { get; set; }

        /// <summary>
        /// Наименование скважины
        /// </summary>
        //[JsonProperty(PropertyName = "well_name")]
        //public string WellName { get; set; }

        /// <summary>
        /// Время создания заявки
        /// </summary>
        [JsonProperty(PropertyName = "created")]
        public DateTime? Created { get; set; }

        /// <summary>
        /// Время выполнения заявки
        /// </summary>
        [JsonProperty(PropertyName = "completed")]
        public DateTime? Completed { get; set; }

        /// <summary>
        /// Уровень срочности SL
        /// </summary>
        [JsonProperty(PropertyName = "sl")]
        public int? SL { get; set; }
    }
}
