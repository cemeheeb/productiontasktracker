﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.DTO {
    using System;

    /// <summary>
    /// Данные в таблице входящих и исходящих заявок
    /// </summary>
    public class TicketTableRecord {
        /// <summary>
        /// Идентификатор заявки
        /// </summary>
        public virtual long? TicketID { get; set; }

        /// <summary>
        /// Идентификатор группы
        /// </summary>
        public virtual long? TicketGroupID { get; set; }

        /// <summary>
        /// Тип заявки
        /// </summary>
        public virtual string TicketType { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual int TicketStatus { get; set; }

        /// <summary>
        /// Название предприятия-заказчика
        /// </summary>
        public virtual string DepartmentFrom { get; set; }

        /// <summary>
        /// Код предприятия-заказчика
        /// </summary>
        public virtual long? DepartmentIDFrom { get; set; }

        /// <summary>
        /// Название предприятия-исполнителя
        /// </summary>
        public virtual string DepartmentTo { get; set; }

        /// <summary>
        /// Код предприятия-исполнителя
        /// </summary>
        public virtual long? DepartmentIDTo { get; set; }

        /// <summary>
        /// Название месторождение
        /// </summary>
        public virtual string Field { get; set; }

        /// <summary>
        /// Код месторождения
        /// </summary>
        public virtual string FieldCode { get; set; }

        /// <summary>
        /// Куст
        /// </summary>
        public virtual string Cluster { get; set; }

        /// <summary>
        /// Название скважины
        /// </summary>
        public virtual string Well { get; set; }

        /// <summary>
        /// Код скважины
        /// </summary>
        public virtual long? WellID { get; set; }

        /// <summary>
        /// Дата и время подачи заявки
        /// </summary>
        public virtual DateTime Created { get; set; }

        /// <summary>
        /// Срок выполнения заявки
        /// </summary>
        public virtual DateTime? Expired { get; set; }

        /// <summary>
        /// Дата и время фактического выполнения
        /// </summary>
        public virtual DateTime? Completed { get; set; }

        /// <summary>
        /// Детали заявки
        /// </summary>
        public virtual long TicketDetailID { get; set; }

        /// <summary>
        /// Уровень срочности
        /// </summary>
        public virtual int? SL { get; set; }

        /// <summary>
        /// Общее количество записей в выборке
        /// </summary>
        public virtual int SizeTotal { get; set; }

        /// <summary>
        /// Разрешено ли снятие с учёта
        /// </summary>
        public virtual bool AllowUnregister { get; set; }

        /// <summary>
        /// Признак ночной заявки
        /// </summary>
        public virtual bool IsNight { get; set; }

        /// <summary>
        /// Признак повторной заявки
        /// </summary>
        public virtual bool IsRepeated { get; set; }
    }
}
