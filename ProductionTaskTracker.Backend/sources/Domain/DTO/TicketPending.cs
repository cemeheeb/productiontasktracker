﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.DTO {
    using System;
    using Enumerables;

    public class TicketPending {
        public virtual long TicketID { get; set; }

        public virtual string TicketTypeString { get; set; }

        public TicketType TicketType {
            get {
                TicketType output;
                return Enum.TryParse(TicketTypeString, out output) ? output : TicketType.Undefined;
            }
        }

        public virtual int? SL { get; set; }

        public virtual bool IsNight { get; set; }
    }
}
