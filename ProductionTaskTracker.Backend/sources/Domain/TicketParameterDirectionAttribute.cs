﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain {
    using Enumerables;

    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class TicketParameterDirectionAttribute: System.Attribute {
        public TicketParameterDirection Direction { get; set; }
    }
}
