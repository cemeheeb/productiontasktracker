﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain {
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class TicketDetailPropertyRequestableAttribute : System.Attribute {
        public string Name { get; set; }
    }
}
