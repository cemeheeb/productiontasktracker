﻿namespace EAEConsult.ProductionTaskTracker.Backend.Domain.Interfaces {
    using System;

    public interface ITimeCompletionProvider {
        DateTime TimeCompletion { get; }
    }
}
