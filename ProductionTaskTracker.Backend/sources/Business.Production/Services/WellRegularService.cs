﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Services;
    using DataAccess.Repositories;
    using Domain.Entities;
    using Infrastructure.Models;
    using Models;

    public class WellRegularService: IWellRegularService {
        #region Fields

        private readonly IWellRegularRepository _wellRegularRepository;
        private readonly IRegisterService _registerService;

        #endregion

        public WellRegularService(IWellRegularRepository wellRegularRepository, IRegisterService registerService) {
            _wellRegularRepository = wellRegularRepository;
            _registerService = registerService;
        }

        public PageContainerModel<WellModel> Search(string search, Department department, int index, int size) {
            int sizeTotal;

            var output =
                _wellRegularRepository
                    .Search(search, department, index, size, out sizeTotal)
                    .Select(x => new WellModel(x));

            return new PageContainerModel<WellModel>(output) { SizeTotal = sizeTotal };
        }

        public PageContainerModel<WellModel> Search(string search, IEnumerable<Department> departments, int index, int size) {
            int sizeTotal;

            var output =
                _wellRegularRepository
                    .Search(search, departments, index, size, out sizeTotal)
                    .Select(x => new WellModel(x));

            return new PageContainerModel<WellModel>(output) { SizeTotal = sizeTotal };
        }

        public PageContainerModel<WellModel> Search(string code, int index, int size) {
            int sizeTotal;

            var output =
                _wellRegularRepository
                    .Search(code, index, size, out sizeTotal)
                    .Select(x => new WellModel(x));

            return new PageContainerModel<WellModel>(output) { SizeTotal = sizeTotal };
        }

        public WellInformationModel GetWellInformation(long wellID) {
            var well = _wellRegularRepository.Find(wellID);
            if (well == null)
                return null;

            var executionMode = _wellRegularRepository.GetExecutionMode(wellID);
            return new WellInformationModel(executionMode, well);
        }

        public PageContainerModel<WellHistoryModel> GetHistory(long id, DateTime dateBegin, DateTime dateEnd, int index, int size) {
            int sizeTotal;
            var output = _registerService.GetByWellCode(id, dateBegin, dateEnd, index, size, out sizeTotal).Select(x => new WellHistoryModel(x));
            return new PageContainerModel<WellHistoryModel>(output) { SizeTotal = sizeTotal };
        }
    }
}
