﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Domain.Enumerables;
    using Models;
    using System;

    public class ClassifierService: IClassifierService {
        #region Fields

        private readonly IClassifierRepository _repository;

    #endregion

        public ClassifierService(IClassifierRepository repository) {
            _repository = repository;
        }

        ClassifierKind GetKind(string type) {
            ClassifierKind output;
            if (!Enum.TryParse(type, out output)) {
                throw new Exception("Тип справочника не зрегистрирвован");
            }

            return output;
        }

        public IEnumerable<ClassifierModel> Get(string type, int index, int size, out int sizeTotal) {
            var kind = GetKind(type);
            return _repository
                    .Filter(kind, index, size, out sizeTotal)
                    .ToList().Select(x => new ClassifierModel(x));
        }
    }
}
