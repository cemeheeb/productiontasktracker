﻿using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Domain.Enumerables;
    using Domain.Entities;
    using Infrastructure.Models;
    using Models;

    public class ControlledWellService : IControlledWellService {
        #region Fields

        private readonly IDepartmentRepository _departmentRepository;
        private readonly IWellRegularRepository _wellRegularRepository;
        private readonly IPumpRepository _pumpRepository;
        private readonly IControlledWellRepository _controlledWellRepository;

        #endregion

        public ControlledWellService(IDepartmentRepository departmentRepository, IControlledWellRepository controlledWellRepository, IWellRegularRepository wellRegularRepository, IPumpRepository pumpRepository) {
            _departmentRepository = departmentRepository;
            _controlledWellRepository = controlledWellRepository;
            _wellRegularRepository = wellRegularRepository;
            _pumpRepository = pumpRepository;
        }

        public PageContainerModel<ControlledWellTableRecordModel> GetByFilter(ControlledWellFilter filter, int index, int size) {
            int sizeTotal;
            var output = _controlledWellRepository.GetByFilter(filter, index, size, out sizeTotal);
            return new PageContainerModel<ControlledWellTableRecordModel>(output.Select(x => new ControlledWellTableRecordModel(x))) { SizeTotal = sizeTotal };
        }

        public ControlledWellTableRecordModel CreateControlledWell(Membership membership, Position position, ControlledWellCreateModel model) {
            var entity = AutoMapper.Mapper.Map<ControlledWell>(model);
            entity.Membership = membership;
            entity.Position = position;
            entity.Executor = _departmentRepository.Find(model.DepartmentID);
            entity.Department = position.Department;
            entity.Well = _wellRegularRepository.Find(model.WellID);
            entity.Pump = _pumpRepository.Find(model.PumpID);
            return new ControlledWellTableRecordModel(_controlledWellRepository.Create(entity));
        }

        public ControlledWellTableRecordModel UpdateControlledWell(long id, ControlledWellUpdateModel model) {
            var entity = _controlledWellRepository.Find(id);
            entity.EventType = model.EventType;
            entity.RepairType = model.RepairType;
            entity.Pump = _pumpRepository.Find(model.PumpID);
            entity.PumpDepth = model.PumpDepth;
            _controlledWellRepository.Update(entity);
            return new ControlledWellTableRecordModel(entity);
        }
    }
}
