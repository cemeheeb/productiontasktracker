﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Infrastructure.Services;
    using Models;

    public class HistoryService : IHistoryService {
        #region Fields

        private readonly string _remoteServer;
        private readonly ILogger _logger;

        #endregion

        public HistoryService(ILoggerFactory loggerFactory, ISettingsService settingsService) {
            _logger = loggerFactory.GetLogger("trace");
            _remoteServer = settingsService.GetRemoteServer();
        }

        public IEnumerable<HistoryModel> GetHistory(string field, string cluster, string well) {
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri(_remoteServer);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                _logger.Info($"GetHistory - _remoteServer:${_remoteServer}");

                var response = client.GetAsync(GetHistoryQueryString(field, cluster, well)).Result;
                _logger.Info($"GetHistory - GetHistoryQueryString:${GetHistoryQueryString(field, cluster, well)}");
                _logger.Info(response.Content.ToString());

                var content = response.Content.ReadAsStringAsync().Result;
                _logger.Info($"content:${_remoteServer}");

                return response.IsSuccessStatusCode ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<HistoryModel>>(content) : null;
            }
        }

        public IEnumerable<HistoryModel> GetHistoryBetween(string field, string cluster, string well, DateTime dateBegin, DateTime dateEnd) {
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri(_remoteServer);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                _logger.Info($"GetHistoryBetween - _remoteServer:${_remoteServer}");

                var response = client.GetAsync(GetHistoryQueryBetweenString(field, cluster, well, dateBegin, dateEnd)).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                _logger.Info($"content:${_remoteServer}");

                return response.IsSuccessStatusCode ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<HistoryModel>>(content) : null;
            }
        }

        public IEnumerable<HistoryModel> GetHistoryByRemoteWellID(string remoteWellID) {
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri(_remoteServer);
                _logger.Info($"GetHistoryByRemoteWellID - _remoteServer:${_remoteServer}");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(GetWellIDQueryString(remoteWellID)).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                _logger.Info($"content:${_remoteServer}");

                return response.IsSuccessStatusCode ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<HistoryModel>>(content) : null;
            }
        }

        public IEnumerable<HistoryModel> GetHistoryBetweenByRemoteWellID(string remoteWellID, DateTime dateBegin, DateTime dateEnd) {
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri(_remoteServer);
                _logger.Info($"GetHistoryBetweenByRemoteWellID - _remoteServer:${_remoteServer}");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(GetWellIDQueryBetweenString(remoteWellID, dateBegin, dateEnd)).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                _logger.Info($"content:${_remoteServer}");

                return response.IsSuccessStatusCode ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<HistoryModel>>(content) : null;
            }
        }

        private string GetHistoryQueryString(string field, string cluster, string well) {
            var parameters = new NameValueCollection {
                {"field", field},
                {"cluster", cluster},
                {"well", well}
            };

            return $"{_remoteServer}/history/repairs{ToQueryString(parameters)}";
        }

        private string GetHistoryQueryBetweenString(string field, string cluster, string well, DateTime dateBegin, DateTime dateEnd) {
            var parameters = new NameValueCollection {
                {"field", field},
                {"cluster", cluster},
                {"well", well},
                {"dateBegin", dateBegin.ToUniversalTime().ToString("s")},
                {"dateEnd", dateEnd.ToUniversalTime().ToString("s")}
            };

            return $"{_remoteServer}/history/repairs{ToQueryString(parameters)}";
        }

        private string GetWellIDQueryString(string remoteWellID) {
            var parameters = new NameValueCollection {
                {"wellID", remoteWellID}
            };

            return $"{_remoteServer}/wells/{ToQueryString(parameters)}";
        }

        private string GetWellIDQueryBetweenString(string remoteWellID, DateTime dateBegin, DateTime dateEnd) {
            var parameters = new NameValueCollection {
                {"wellID", remoteWellID},
                {"dateBegin", dateBegin.ToUniversalTime().ToString("s")},
                {"dateEnd", dateEnd.ToUniversalTime().ToString("s")}
            };

            return $"{_remoteServer}/wells/{ToQueryString(parameters)}";
        }

        private static string ToQueryString(NameValueCollection nvc) {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select $"{HttpUtility.UrlEncode(key)}={HttpUtility.UrlEncode(value)}")
                .ToArray();
            return "?" + string.Join("&", array);
        }
    }
}
