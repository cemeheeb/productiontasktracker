﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using System;
    using DataAccess.Repositories;
    using Domain.Entities;
    using Models;

    public class PositionService : IPositionService {
        #region Fileds

        private IPositionRepository _positionRepository;
        private IDepartmentRepository _departmentRepository;

        #endregion

        public PositionService(IPositionRepository positionRepository, IDepartmentRepository departmentRepository) {
            _positionRepository = positionRepository;
            _departmentRepository = departmentRepository;
        }

        public PositionModel Create(PositionModel model) {
            var department = _departmentRepository.Find(model.DepartmentID.Value);
            var output = _positionRepository.Create(new Position {
                Department = department,
                Role = _positionRepository.FindRole(department , model.PositionRoleName),
                Name = model.Name,
                DisplayName = model.DisplayName
            });
            return new PositionModel(output);
        }

        public IEnumerable<PositionModel> GetAll() {
            return _positionRepository.GetAll().OrderBy(x => x.DisplayName).Select(x => new PositionModel(x));
        }

        public void Update(PositionModel model)
        {
            var output = _positionRepository.Find(model.Id);

            output.Name = model.Name;
            output.DisplayName = model.DisplayName;
            output.Role = _positionRepository.FindRole(output.Department, model.PositionRoleName);

            _positionRepository.Update(output);
        }

        public void Remove(long id)
        {
            var output = _positionRepository.Find(id);
            output.Deleted = true;
            _positionRepository.Update(output);
        }
    }
}
