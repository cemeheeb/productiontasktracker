﻿using System.Linq;
using AutoMapper;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Infrastructure.Models;
    using DataAccess.Repositories;
    using Domain.Entities;
    using Models;

    public class EnterpriseService: IEnterpriseService {
        #region Fields

        private readonly IEnterpriseRepository _enterpriseRepository;

        #endregion

        public EnterpriseService(IEnterpriseRepository enterpriseRepository) {
            _enterpriseRepository = enterpriseRepository;
        }

        public EnterpriseModel Get(long id) {
            return new EnterpriseModel(_enterpriseRepository.Find(id));
        }

        public EnterpriseModel Get(string code) {
            return new EnterpriseModel(_enterpriseRepository.Find(code));
        }

        public PageContainerModel<EnterpriseModel> Search(string search, int index, int size) {
            int sizeTotal;

            var output =
                _enterpriseRepository
                    .Search(search, index, size, out sizeTotal)
                    .Select(Mapper.Map<Enterprise, EnterpriseModel>);

            return new PageContainerModel<EnterpriseModel>(output) { SizeTotal = sizeTotal };
        }

        public PageContainerModel<EnterpriseModel> Search(int index, int size) {
            int sizeTotal;

            var output =
                _enterpriseRepository
                    .Search(null, index, size, out sizeTotal)
                    .Select(Mapper.Map<Enterprise, EnterpriseModel>);

            return new PageContainerModel<EnterpriseModel>(output) { SizeTotal = sizeTotal };
        }
    }
}
