﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Domain.Enumerables;
    using Models;

    public class RepairTypeService : IRepairTypeService {
        #region Fields

        private readonly IClassifierRepository _repository;

        #endregion

        public RepairTypeService(IClassifierRepository repository) {
            _repository = repository;
        }

        public IEnumerable<ClassifierModel> Get() {
            return _repository.Filter(ClassifierKind.RT_).ToList().Select(x => new ClassifierModel(x));
        }

        public IEnumerable<ClassifierModel> GetE() {
            return _repository.Filter(ClassifierKind.RTE).ToList().Select(x => new ClassifierModel(x));
        }

        public IEnumerable<ClassifierModel> GetS() {
            return _repository.Filter(ClassifierKind.RTS).ToList().Select(x => new ClassifierModel(x));
        }
    }
}
