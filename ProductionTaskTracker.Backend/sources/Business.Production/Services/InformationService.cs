﻿namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Models;

    public class InformationService : IInformationService {
        #region Fields

        private readonly IDepartmentRepository _departmentRepository;

        #endregion

        public InformationService(IDepartmentRepository departmentRepository) {
            _departmentRepository = departmentRepository;
        }

        public DepartmentFullModel GetDepartment(long id) {
            return new DepartmentFullModel(_departmentRepository.Find(id));
        }
    }
}
