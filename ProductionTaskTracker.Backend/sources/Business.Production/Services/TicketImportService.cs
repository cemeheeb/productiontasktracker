﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using SpreadsheetGear;
using System.Globalization;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain;
    using Domain.Entities;
    using Domain.Enumerables;
    using DataAccess.Repositories;
    using Domain.Entities.TicketDetails;

    public class TicketImportService: ITicketImportService {
        #region Fields

        private readonly ITicketRepository _ticketRepository;

        #endregion
         
        public TicketImportService(ITicketRepository ticketRepository) {
            _ticketRepository = ticketRepository;
        }

        public void GenerateDirective354Blank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails) {
            ValidateTicketType(ticketType);

            var ticketDetailList = ticketDetails.ToList();
            var assembly = Assembly.GetExecutingAssembly();

            // Чтение шаблона из ресурсов сборки
            var workbook = Factory
                .GetWorkbookSet(CultureInfo.CurrentCulture)
                .Workbooks
                .OpenFromStream(assembly.GetManifestResourceStream("EAEConsult.ProductionTaskTracker.Backend.Business.Templates.TemplateDirective354Blank.xls"));
            var worksheetTemplate = workbook.Worksheets["template"];
            var ru = CultureInfo.GetCultureInfo("ru-RU");

            var scanline = 0;
            foreach (var ticketDetail in ticketDetailList) {
                var worksheet = worksheetTemplate.CopyAfter(worksheetTemplate) as IWorksheet;
                worksheet.Name = ticketDetail.Ticket.Id.ToString();
                worksheet.Cells.Replace("{DATE.DAY}", $"«{ticketDetail.Ticket.Created.Day.ToString()}»", LookAt.Whole, SearchOrder.ByRows, true);
                worksheet.Cells.Replace("{DATE.MONTH}", ru.DateTimeFormat.MonthGenitiveNames[ticketDetail.Ticket.Created.Month - 1], LookAt.Whole, SearchOrder.ByRows, true);
                worksheet.Cells.Replace("{DATE.YEAR}", $"{ticketDetail.Ticket.Created.Year.ToString()}г.", LookAt.Whole, SearchOrder.ByRows, true);
                worksheet.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheet.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheet.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                scanline++;
            }

            workbook.Worksheets["template"].Delete();

            workbook.SaveAs(filename, FileFormat.Excel8);
        }

        public void GenerateDirectiveVNRBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails) {
            ValidateTicketType(ticketType);

            var ticketDetailList = ticketDetails.ToList();
            var assembly = Assembly.GetExecutingAssembly();

            // Чтение шаблона из ресурсов сборки
            var workbook = Factory
                .GetWorkbookSet(CultureInfo.CurrentCulture)
                .Workbooks
                .OpenFromStream(assembly.GetManifestResourceStream("EAEConsult.ProductionTaskTracker.Backend.Business.Templates.TemplateVNRBlank.xls"));

            var worksheetTemplateA = workbook.Worksheets["templateA"];
            var worksheetTemplateB = workbook.Worksheets["templateB"];

            var scanline = 0;
            foreach (var ticketDetail in ticketDetailList) {
                var worksheetA = worksheetTemplateA.CopyAfter(worksheetTemplateA) as IWorksheet;
                worksheetA.Name = $"{ticketDetail.Ticket.Id.ToString()}- Приложение 3";
                worksheetA.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetA.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetA.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                var worksheetB = worksheetTemplateB.CopyAfter(worksheetTemplateB) as IWorksheet;
                worksheetB.Name = $"{ticketDetail.Ticket.Id.ToString()}- Приложение 4";
                worksheetB.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetB.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetB.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                scanline++;
            }

            workbook.Worksheets["templateA"].Delete();
            workbook.Worksheets["templateB"].Delete();
            
            workbook.SaveAs(filename, FileFormat.Excel8);
        }

        public void GenerateDirectiveWarmTreatmentBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails) {
            ValidateTicketType(ticketType);

            var ticketDetailList = ticketDetails.ToList();
            var assembly = Assembly.GetExecutingAssembly();

            // Чтение шаблона из ресурсов сборки
            var workbook = Factory
                .GetWorkbookSet(CultureInfo.CurrentCulture)
                .Workbooks
                .OpenFromStream(assembly.GetManifestResourceStream("EAEConsult.ProductionTaskTracker.Backend.Business.Templates.TemplateWarmTreatmentBlank.xls"));

            var worksheetTemplateA = workbook.Worksheets["templateA"];
            var worksheetTemplateB = workbook.Worksheets["templateB"];

            var scanline = 0;
            foreach (var ticketDetail in ticketDetailList) {
                var worksheetA = worksheetTemplateA.CopyAfter(worksheetTemplateA) as IWorksheet;
                worksheetA.Name = $"{ticketDetail.Ticket.Id.ToString()}- Приложение 3";
                worksheetA.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetA.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetA.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                var worksheetB = worksheetTemplateB.CopyAfter(worksheetTemplateB) as IWorksheet;
                worksheetB.Name = $"{ticketDetail.Ticket.Id.ToString()}- Приложение 4";
                worksheetB.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetB.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetB.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                scanline++;
            }

            workbook.Worksheets["templateA"].Delete();
            workbook.Worksheets["templateB"].Delete();

            workbook.SaveAs(filename, FileFormat.Excel8);
        }

        public void GenerateDirectiveWashingBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails) {
            ValidateTicketType(ticketType);

            var ticketDetailList = ticketDetails.ToList();
            var assembly = Assembly.GetExecutingAssembly();

            // Чтение шаблона из ресурсов сборки
            var workbook = Factory
                .GetWorkbookSet(CultureInfo.CurrentCulture)
                .Workbooks
                .OpenFromStream(assembly.GetManifestResourceStream("EAEConsult.ProductionTaskTracker.Backend.Business.Templates.TemplateWashingBlank.xls"));

            var worksheetTemplateA = workbook.Worksheets["templateA"];
            var worksheetTemplateB = workbook.Worksheets["templateB"];

            var scanline = 0;
            foreach (var ticketDetail in ticketDetailList) {
                var worksheetA = worksheetTemplateA.CopyAfter(worksheetTemplateA) as IWorksheet;
                worksheetA.Name = $"{ticketDetail.Ticket.Id.ToString()}- Приложение 3";
                worksheetA.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetA.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetA.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                var worksheetB = worksheetTemplateB.CopyAfter(worksheetTemplateB) as IWorksheet;
                worksheetB.Name = $"{ticketDetail.Ticket.Id.ToString()}- Приложение 4";
                worksheetB.Cells.Replace("{WELL}", ticketDetail.Well.Code, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetB.Cells.Replace("{CLUSTER}", ticketDetail.Well.Cluster, LookAt.Whole, SearchOrder.ByRows, true);
                worksheetB.Cells.Replace("{FIELD}", ticketDetail.Well.Field.Name, LookAt.Whole, SearchOrder.ByRows, true);
                scanline++;
            }

            workbook.Worksheets["templateA"].Delete();
            workbook.Worksheets["templateB"].Delete();

            workbook.SaveAs(filename, FileFormat.Excel8);
        }

        public void GenerateUniversalBlank(string filename, TicketType ticketType, Department department, int? sl, bool isNight, IEnumerable<TicketDetailBase> ticketDetails) {
            // Алгоритм подготовки бланка-шаблона для импорта данных:
            // 0. Валидация параметров, поддерживаемые типы [1.1, 1.2, 1.3, 1.4, 2.6, 2.7, 2.8, 2.9, 2.11, 2.13, 3]
            // 1. Формирование документа

            // 0 
            ValidateTicketType(ticketType);

            var ticketDetailList = ticketDetails.ToList();

            // 2
            var assembly = Assembly.GetExecutingAssembly();

            // Чтение шаблона из ресурсов сборки
            var workbook = Factory
                .GetWorkbookSet(System.Globalization.CultureInfo.CurrentCulture)
                .Workbooks
                .OpenFromStream(assembly.GetManifestResourceStream("EAEConsult.ProductionTaskTracker.Backend.Business.Templates.TemplateImportBlank.xls"));
            var worksheet = workbook.Worksheets["Данные"];

            // Заполнение статической части документа
            worksheet.Cells.Replace("{TicketType}", Utilities.GetTicketTypeDescription(ticketType), LookAt.Whole, SearchOrder.ByRows, true);
            worksheet.Cells.Replace("{SL}", $"SL{sl + 1}", LookAt.Whole, SearchOrder.ByRows, true);
            worksheet.Cells.Replace("{Department}", department.DisplayName, LookAt.Whole, SearchOrder.ByRows, true);
            worksheet.Cells.Replace("{IsNight}", isNight ? "Да" : "Нет", LookAt.Whole, SearchOrder.ByRows, true);

            var ticketClassType = Utilities.GetTicketTypeFromEnumerable(ticketType);
            var ticketDetailRequestableProperties = EnumerateRequestableProperties(ticketClassType);

            // Получение шаблонов строк: первая, средняя, последняя соответственно
            var rowTemplateFirst = worksheet.Cells[GetRowNum(worksheet, "Row.First"), 0].EntireRow;
            var rowTemplateMiddle = worksheet.Cells[GetRowNum(worksheet, "Row.Middle"), 0].EntireRow;
            var rowTemplateLast = worksheet.Cells[GetRowNum(worksheet, "Row.Last"), 0].EntireRow;

            // Заполнение динамической части документа
            var scanline = 0;
            var dictionaryColumnNums = new Dictionary<string, int>();

            var wellParameters = new List<string> {
                "Field",
                "Well",
                "Cluster"
            };

            foreach (var wellParameter in wellParameters) {
                dictionaryColumnNums[wellParameter] = GetColumnNum(worksheet, wellParameter);
            }


            var propsRequested = new HashSet<string>();
            foreach (var ticketDetail in ticketDetailList) {
                foreach (var requested in ticketDetail.Requested.Split(',')) {
                    propsRequested.Add(string.Concat(requested.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries).Select(x => char.ToUpperInvariant(x[0]) + x.Substring(1, x.Length - 1)).Aggregate(string.Empty, (a, b) => a + b)));
                }
            }

            var tableHeaderRow = GetRowNum(worksheet, "Table.Headers");
            var props = ticketClassType.GetProperties().ToList();
            foreach (var property in props) {
                var column = GetColumnNum(worksheet, property.Name);
                if (column < 0 || (Attribute.IsDefined(property, typeof(TicketDetailPropertyRequestableAttribute)) && !propsRequested.Contains(property.Name) )) {
                    continue;
                }

                worksheet.Cells[0, column].EntireColumn.Hidden = false;
            }

            foreach (var ticketDetail in ticketDetailList) {
                // Определение типа шаблона для вставляемой строки
                var rowTemplate = scanline == 0 ? rowTemplateFirst : scanline == ticketDetailList.Count - 1 ? rowTemplateLast : rowTemplateMiddle;

                // Оформление
                rowTemplate.Copy(worksheet.Cells[tableHeaderRow + scanline + 1, 0], PasteType.All, PasteOperation.None, false, false);

                worksheet.Cells[tableHeaderRow + scanline + 1, dictionaryColumnNums["Field"]].Value = ticketDetail.Well.Field.Name;
                worksheet.Cells[tableHeaderRow + scanline + 1, dictionaryColumnNums["Well"]].Value = ticketDetail.Well.Code;
                worksheet.Cells[tableHeaderRow + scanline + 1, dictionaryColumnNums["Cluster"]].Value = ticketDetail.Well.Cluster;

                scanline++;
            }

            workbook.SaveAs(filename, FileFormat.Excel8);
        }

        private int GetRowNum(IWorksheet worksheet, string key) {
            var founded = worksheet.Cells.Find($"{{{key}}}", null, FindLookIn.Values, LookAt.Whole, SearchOrder.ByColumns, SearchDirection.Next, true);
            return founded?.Row ?? -1;
        }

        private int GetColumnNum(IWorksheet worksheet, string key) {
            var founded = worksheet.Cells.Find($"{{{key}}}", null, FindLookIn.Values, LookAt.Whole, SearchOrder.ByColumns, SearchDirection.Next, true);
            return founded?.Column ?? -1;
        }

        private static void ValidateTicketType(TicketType ticketType) {
            // Подготовка массива типов с разрешенными заявками
            var ticketTypes = new[] {
                TicketType.A1, TicketType.A2, TicketType.A3, TicketType.A4,
                TicketType.B1B, TicketType.B2, TicketType.B6, TicketType.B7, TicketType.B8, TicketType.B9, TicketType.B11, TicketType.B13,
                TicketType.C
            };

            if (!ticketTypes.Contains(ticketType)) {
                throw new Exception("Неподдерживаемый тип заявки");
            }
        }

        internal List<string> EnumerateRequestableProperties(Type ticketDetailType) {
            return ticketDetailType
                .GetProperties()
                .Where(x => Attribute.IsDefined(x, typeof(TicketParameterDirectionAttribute)))
                .Where(x => x.GetCustomAttributes(true).Where(z => z is TicketParameterDirectionAttribute).Select(z => (TicketParameterDirectionAttribute)z).Any(z => z.Direction == TicketParameterDirection.Backward))
                .Select(propertyInfo => propertyInfo.Name)
                .ToList();
        }
    }
}
