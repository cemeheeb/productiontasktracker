﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Domain.Entities;
    using Models;

    public class AutocompleteOptionService : IAutocompleteOptionService {

        #region Fields

        private IAutocompleteOptionRepository _autocompleteOptionRepository;

        #endregion

        public AutocompleteOptionService(IAutocompleteOptionRepository autocompleteOptionRepository) {
            _autocompleteOptionRepository = autocompleteOptionRepository;
        }

        public IEnumerable<AutocompleteOptionModel> Filter(Membership membership, string parameter) {
            return _autocompleteOptionRepository.Filter(membership, parameter).Select(x=>new AutocompleteOptionModel { Text = x.Text, Weight = x.Weight} );
        }
    }
}
