﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

using AutoMapper;
using Ninject.Infrastructure.Language;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Infrastructure.Services;
    using Domain.Entities;
    using Models;
    using Infrastructure.Models;

    public class PumpService : IPumpService {
        #region Fields

        private readonly string _remoteServer;
        private readonly IPumpRepository _pumpRepository;

        #endregion

        public PumpService(IPumpRepository pumpRepository, ISettingsService settingsService) {
            _pumpRepository = pumpRepository;
            _remoteServer = settingsService.GetRemoteServer();
        }

        public PageContainerModel<PumpModel> Search(string search, int index, int size) {
            int sizeTotal;

            var output =
                _pumpRepository
                    .Search(search, index, size, out sizeTotal)
                    .Select(Mapper.Map<Pump, PumpModel>);

            return new PageContainerModel<PumpModel>(output) { SizeTotal = sizeTotal };
        }

        public PumpModel Find(long id) {
            var pump = _pumpRepository.Find(id);
            return pump == null ? new PumpModel() : Mapper.Map<Pump, PumpModel>(pump);
        }

        public void Import() {
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri(_remoteServer);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync($"{_remoteServer}/pumps").Result;
                var content = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode) {
                    return;
                }

                var pumps = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PumpModel>>(content);

                _pumpRepository.Clear();
                pumps.Select(Mapper.Map<PumpModel, Pump>).Map(x => _pumpRepository.Create(x));
            }
        }
    }
}
