﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Domain.Entities;
    using Models;

    public class TicketRouteService: ITicketRouteService {

        #region Fields

        private readonly ITicketRouteRepository _ticketRouteRepository;
        
        #endregion

        public TicketRouteService(ITicketRouteRepository ticketRouteRepository) {
            _ticketRouteRepository = ticketRouteRepository;
        }

        public IEnumerable<TicketRouteModel> GetTicketRouteMapJson(Position position) {
            return _ticketRouteRepository.GetRoutes(position.Role, position.Department).Select(x => new TicketRouteModel(x));
        }

        public IEnumerable<Shop> GetShopsByDepartment(Department department) {
            return _ticketRouteRepository
                .GetRoutes(department)
                .Where(x => !x.StatusFrom.HasValue && x.DateBegin < DateTime.Now && (!x.DateEnd.HasValue || x.DateEnd > DateTime.Now))
                .Select(x => x.DepartmentTo.Shop)
                .Distinct();
        }

        public IEnumerable<Shop> GetShopsByDepartment(PositionRole positionRole, Department department) {
            return _ticketRouteRepository
                .GetRoutes(positionRole, department)
                .Where(x => !x.StatusFrom.HasValue && x.DateBegin < DateTime.Now && (!x.DateEnd.HasValue || x.DateEnd > DateTime.Now))
                .Select(x => x.DepartmentTo.Shop)
                .Distinct();
        }
    }
}
