﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Models;

    public class FieldService: IFieldService {
        #region Fields

        private readonly IFieldDictionaryRepository _fieldDictionaryRepository;

        #endregion

        public FieldService(IFieldDictionaryRepository fieldDictionaryRepository) {
            _fieldDictionaryRepository = fieldDictionaryRepository;
        }

        public FieldModel Get(string fieldCode) {
            return new FieldModel(_fieldDictionaryRepository.Find(fieldCode));
        }

        public IEnumerable<FieldModel> GetByShopCodes(string[] shopCodes) {
            return _fieldDictionaryRepository
                .GetByShopCodes(shopCodes)
                .Select(x => new FieldModel(x));
        }

        public IEnumerable<FieldModel> GetAll() {
            return _fieldDictionaryRepository
                .GetAll()
                .Where(x => x.Code != "MS0000" && x.Code != "MSNULL" && x.Code != "MSNAME")
                .Select(x => new FieldModel(x));
        }
    }
}
