﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.DictionaryRepositories;
    using DataAccess.Repositories;
    using Domain.Entities;
    using Models;

    public class DepartmentService : IDepartmentService {
        #region Fields

        private readonly IShopDictionaryRepository _shopRepository;
        private readonly IDepartmentRepository _departmentRepository;

        #endregion

        public DepartmentService(IShopDictionaryRepository shopRepository, IDepartmentRepository departmentRepository) {
            _shopRepository = shopRepository;
            _departmentRepository = departmentRepository;
        }

        public DepartmentModel Create(DepartmentModel model) {
            var entity = _departmentRepository.Create(new Department() {
                Shop = _shopRepository.Find(model.ShopCode),
                DisplayName = model.DisplayName,
                FullName = model.FullName,
                Parent = model.ParentID.HasValue ? _departmentRepository.Find(model.ParentID.Value) : null
            });

            return entity != null ? new DepartmentModel(entity) : null;
        }

        public void Update(DepartmentModel model) {
            var entity = _departmentRepository.Find(model.Id);
            entity.FullName = model.FullName;
            entity.DisplayName = model.DisplayName;
            entity.Shop = !string.IsNullOrEmpty(model.ShopCode) ? _shopRepository.Find(model.ShopCode) : null;
            _departmentRepository.Update(entity);
        }

        public void Remove(long id) {
            var entity = _departmentRepository.Find(id);
            entity.Deleted = true;
            _departmentRepository.Update(entity);
        }

        public IEnumerable<DepartmentModel> GetAll() {
            return _departmentRepository
                .GetAll()
                .OrderBy(x => x.FullName)
                .Select(x => new DepartmentModel(x));
        }
    }
}
