﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Collections.Generic;

using AutoMapper;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using Domain.Entities.TicketDetails;
    using Domain.Entities;
    using Domain.Enumerables;
    using DataAccess.Repositories;
    using DataAccess.Services;
    using Domain.Interfaces;
    using Models;
    using Ninject.Infrastructure.Language;

    public class TicketService : ITicketService {
        #region Fields

        private readonly ILogger _logger;
        private readonly IRegisterService _registerService;
        private readonly ITicketRepository _ticketRepository;
        private readonly ITicketDetailRepository _ticketDetailRepository;
        private readonly IAutocompleteOptionRepository _autocompleteOptionRepository;
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IWellRegularRepository _wellRegularRepository;
        private readonly IClassifierRepository _classifierRepository;
        private readonly IFieldDictionaryRepository _fieldDictionaryRepository;
        private readonly IMeasureFacilityRepository _measureFacilityRepository;
        private readonly IPumpRepository _pumpRepository;
        private readonly ITicketMovementRepository _ticketMovementRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IDepartmentRepository _departmentRepository;

        #endregion

        public TicketService(ILoggerFactory loggerFactory, 
            IRegisterService registerService,
            ITicketRepository ticketRepository, 
            ITicketDetailRepository ticketDetailRepository,
            IAutocompleteOptionRepository autocompleteOptionRepository,
            IEnterpriseRepository enterpriseRepository,
            IPumpRepository pumpRepository,
            IClassifierRepository classifierRepository,
            IFieldDictionaryRepository fieldDictionaryRepository,
            IWellRegularRepository wellRegularRepository, 
            IMeasureFacilityRepository measureFacilityRepository,
            ITicketMovementRepository ticketMovementRepository,
            IWorkPeriodRepository workPeriodRepository, 
            IDepartmentRepository departmentRepository
            ) {

            _logger = loggerFactory.GetLogger("trace");
            _registerService = registerService;
            _ticketRepository = ticketRepository;
            _ticketDetailRepository = ticketDetailRepository;
            _autocompleteOptionRepository = autocompleteOptionRepository;
            _enterpriseRepository = enterpriseRepository;
            _fieldDictionaryRepository = fieldDictionaryRepository;
            _wellRegularRepository = wellRegularRepository;
            _measureFacilityRepository = measureFacilityRepository;
            _pumpRepository = pumpRepository;
            _classifierRepository = classifierRepository;
            _ticketMovementRepository = ticketMovementRepository;
            _workPeriodRepository = workPeriodRepository;
            _departmentRepository = departmentRepository;
        }
        
        public PageTicketRecordContainerModel GetIncoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size) {
            int sizeTotal;

            var data =
                _ticketRepository
                    .GetIncoming(filter, membership, department, dateBegin, dateEnd, index, size, out sizeTotal)
                    .Select(x => new TicketTableRecordModel(x));

            var filterOptions = _ticketRepository
                .GetIncomingFilterOptions(filter, membership, department, dateBegin, dateEnd);

            var filterOptionsPrepared = new Dictionary<string, IEnumerable<FilterOptionModel>>();
            filterOptions.Keys.Map(x => filterOptionsPrepared[x] = filterOptions[x].Select(option => new FilterOptionModel(option)));

            return new PageTicketRecordContainerModel(data, filterOptionsPrepared) {SizeTotal = sizeTotal};
        }

        public PageTicketRecordContainerModel GetOutcoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size) {
            int sizeTotal;

            var data =
                _ticketRepository
                    .GetOutcoming(filter, membership, department, dateBegin, dateEnd, index, size, out sizeTotal)
                    .Select(x => new TicketTableRecordModel(x));

            var filterOptions = _ticketRepository
                .GetOutcomingFilterOptions(filter, membership, department, dateBegin, dateEnd);

            var filterOptionsPrepared = new Dictionary<string, IEnumerable<FilterOptionModel>>();
            filterOptions.Keys.Map(x => filterOptionsPrepared[x] = filterOptions[x].Select(option => new FilterOptionModel(option)));

            return new PageTicketRecordContainerModel(data, filterOptionsPrepared) { SizeTotal = sizeTotal };
        }

        public TicketTableRecordModel RegisterTicket(Membership membership, TicketRegistrationModel model) {
            return RegisterTicketInternal(membership, model.TicketType, model.DepartmentID, model.IsNight, model.SL, new List <TicketDetailModel> { model }).First();
        }

        public IEnumerable<TicketTableRecordModel> RegisterTicketGroup(Membership membership, TicketRegistrationGroupModel model) {
            return RegisterTicketInternal(membership, model.TicketType, model.DepartmentID, model.IsNight, model.SL, model.Parameters);
        }

        IEnumerable<TicketTableRecordModel> RegisterTicketInternal(Membership membership, TicketType ticketType, long departmentID, bool isNight, int? sl, IEnumerable<TicketDetailModel> models) {
            var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
            if (workPeriod == null)
                throw new AuthenticationException("Истек срок действия аккаунта");

            var output = new List<TicketTableRecordModel>();
            var modelList = models.ToList();
            var ticketGroupID = modelList.Count > 1 ? (long?)_ticketRepository.GetTicketIDSequenceNext() : null;
            foreach (var model in modelList) {
                var ticket = new Ticket {
                    TicketType = ticketType,
                    Department = _departmentRepository.Find(departmentID),
                    GroupID = ticketGroupID,
                    Created = DateTime.Now,
                    IsNight = isNight
                };

                ticket = _ticketRepository.Create(ticket);

                var ticketDetail = (TicketDetailBase)Mapper.Map(model, typeof(TicketDetailModel), Utilities.GetTicketTypeFromEnumerable(ticketType));
                ticketDetail.Well = model.WellID.HasValue ? _wellRegularRepository.Find(model.WellID.Value) : null;

                if (string.IsNullOrEmpty(model.FieldCode)) {
                    ticketDetail.Field = null;
                }

                switch (ticketType) {
                    case TicketType.B1:
                        var ticketDetailB1 = (TicketDetailB1)ticketDetail;
                        if (model.PumpID.HasValue) {
                            ticketDetailB1.Pump = _pumpRepository.Find(model.PumpID.Value);
                        }
                        break;
                    case TicketType.B8:
                        var ticketDetailB8 = (TicketDetailB8)ticketDetail;
                        ticketDetailB8.PowerOff = ticketDetailB8.PowerOff ?? false;
                        break;
                    case TicketType.B10:
                        var ticketDetailB10 = (TicketDetailB10)ticketDetail;
                        ticketDetailB10.Field = _fieldDictionaryRepository.Find(model.FieldCode);
                        ticketDetailB10.Shop = _measureFacilityRepository.GetShopByMeasureFacilityPosition(ticketDetailB10.Field.Code, ticketDetailB10.Cluster, ticketDetailB10.MeasureFacilityPosition);
                        break;
                    case TicketType.B12:
                        var ticketDetailB12 = (TicketDetailB12)ticketDetail;
                        if (model.ContractorID.HasValue) {
                            ticketDetailB12.Contractor = _enterpriseRepository.Find(model.ContractorID.Value);
                        }
                        break;
                }

                ticketDetail.Membership = membership;
                ticketDetail.Ticket = ticket;
                ticketDetail.SL = sl;

                _autocompleteOptionRepository.Activate(membership, "note", ticketDetail.Note);

                var ticketMovement = _ticketMovementRepository.Create(new TicketMovement {
                    Membership = membership,
                    Position = workPeriod.Position,
                    Department = workPeriod.Position.Department,
                    Status = TicketStatus.Created,
                    Ticket = ticket,
                    TicketDetail = ticketDetail
                });

                _registerService.PendingInclude(ticketMovement.Id);

                // Заявки с уровнем срочности перманентно переводятся в статус ПОДАННАЯ
                if ((sl.HasValue && !ticket.IsNight) || (ticketType == TicketType.F) || (ticketType == TicketType.G)) {
                    // При этом рассчитывается срок исполнения заявки
                    var ticketDetailAccepted = (TicketDetailBase)Mapper.Map(model, typeof(TicketDetailModel), Utilities.GetTicketTypeFromEnumerable(ticketType));
                    ticketDetailAccepted.CopyRequestPropertiesFrom(ticketDetail);
                    ticketDetailAccepted.Well = model.WellID.HasValue ? _wellRegularRepository.Find(model.WellID.Value) : null;

                    ticketDetailAccepted.Membership = membership;
                    ticketDetailAccepted.Ticket = ticket;
                    ticketDetailAccepted.SL = sl;

                    if (string.IsNullOrEmpty(model.FieldCode)) {
                        ticketDetailAccepted.Field = null;
                    }

                    if (!ticketDetail.Expired.HasValue) {
                        var ticketDetailTimeCompletionProvider = ticketDetail as ITimeCompletionProvider;
                        if (ticketDetailTimeCompletionProvider != null) {
                            ticketDetailAccepted.Expired = ticketDetailTimeCompletionProvider.TimeCompletion;
                        } else if (ticketDetail.SL.HasValue) {
                            ticketDetailAccepted.SetExpiredAtNow();
                        }
                    }

                    var ticketMovementNext = _ticketMovementRepository.Create(new TicketMovement {
                        Membership = membership,
                        Position = workPeriod.Position,
                        Department = ticket.Department,
                        Status = TicketStatus.Filed,
                        Ticket = ticket,
                        TicketDetail = ticketDetailAccepted
                    });
                    //_autocompleteOptionRepository.Activate(membership);
                    _registerService.PendingInclude(ticketMovementNext.Id);
                }


                var fieldName = string.Empty;
                var fieldCode = string.Empty;
                var cluster = string.Empty;

                if (ticketDetail.Well != null) {
                    fieldName = ticketDetail.Well.Field.Name;
                    fieldCode = ticketDetail.Well.Field.Code;
                    cluster = ticketDetail.Well.Cluster;
                } else if (ticketDetail.Field != null) {
                    fieldName = ticketDetail.Field.Name;
                    fieldCode = ticketDetail.Field.Code;
                    cluster = ticketDetail.Cluster;
                }

                output.Add(new TicketTableRecordModel {
                    TicketID = ticket.Id,
                    TicketType = ticket.TicketType.ToString(),
                    TicketStatus = (int)TicketStatus.Filed,
                    TicketGroupID = ticket.GroupID,
                    Field = fieldName,
                    FieldCode = fieldCode,
                    Cluster = cluster,
                    Well = ticketDetail.Well?.Code,
                    WellID = ticketDetail.Well?.Id,
                    DepartmentFrom = workPeriod.Position.Department.DisplayName,
                    DepartmentIDFrom = workPeriod.Position.Department.Id,
                    DepartmentTo = ticketMovement.Department.DisplayName,
                    DepartmentIDTo = ticketMovement.Department.Id,
                    TicketDetailID = ticketDetail.Id,
                    SL = ticketDetail.SL,
                    Created = ticket.Created,
                    AllowUnregister = true,
                    IsNight = ticket.IsNight
                });
            }

            _registerService.CalculateRegister();
            return output;
        }
    }
}
