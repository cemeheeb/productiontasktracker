﻿using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.DictionaryRepositories;
    using Models;
    
    public class ShopService: IShopService {
        #region Fields

        private readonly IShopDictionaryRepository _repository;

        #endregion

        public ShopService(IShopDictionaryRepository repository) {
            _repository = repository;
        }

        public IEnumerable<ShopModel> GetAll() {
            return _repository.GetAll().Select(x => new ShopModel(x));
        }

        public ShopModel GetByCode(string code) {
            return new ShopModel(_repository.Find(code));
        }
    }
}
