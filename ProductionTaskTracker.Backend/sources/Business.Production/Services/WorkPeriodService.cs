﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.Business.Services {
    using DataAccess.Repositories;
    using Models;

    public class WorkPeriodService: IWorkPeriodService
    {
        #region Fields

        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IMembershipRepository _membershipRepository;

        #endregion

        public WorkPeriodService(IWorkPeriodRepository workPeriodRepository, IPositionRepository positionRepository, IMembershipRepository membershipRepository) {
            _workPeriodRepository = workPeriodRepository;
            _positionRepository = positionRepository;
            _membershipRepository = membershipRepository;
        }

        public void BeginWorkPeriod(WorkPeriodBeginModel model)
        {
            var membership = _membershipRepository.Find(model.MembershipID);
            var position = _positionRepository.Find(model.PositionID);
            _workPeriodRepository.Create(new Domain.Entities.WorkPeriod() { DateBegin = model.Date ?? DateTime.Now.Date, Membership = membership, Position = position });
        }

        public void EndWorkPeriod(long id) {
            var workPeriod = _workPeriodRepository.Find(id);
            workPeriod.DateEnd = DateTime.Now;
            _workPeriodRepository.Update(workPeriod);
        }


        public IEnumerable<WorkPeriodModel> GetActivePeriods() {
            return _workPeriodRepository.GetActive().Select(x => new WorkPeriodModel(x));
        }

    }
}
