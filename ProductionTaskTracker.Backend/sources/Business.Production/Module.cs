﻿using AutoMapper;
using Ninject.Modules;
using Ninject.Web.Common;

namespace EAEConsult.ProductionTaskTracker.Backend.Business {
    using System.Linq;
    using Domain.DTO;
    using Domain.Entities.TicketDetails;
    using Domain.Entities;
    using Services;
    using Models;

    public class Module : NinjectModule {
        public override void Load() {
            Bind<IAutocompleteOptionService>().To<AutocompleteOptionService>().InRequestScope();
            Bind<IInformationService>().To<InformationService>().InRequestScope();
            Bind<IDepartmentService>().To<DepartmentService>().InRequestScope();
            Bind<IControlledWellService>().To<ControlledWellService>().InRequestScope();
            Bind<IEnterpriseService>().To<EnterpriseService>().InRequestScope();
            Bind<IPositionService>().To<PositionService>().InRequestScope();
            Bind<IWorkPeriodService>().To<WorkPeriodService>().InRequestScope();
            Bind<IFieldService>().To<FieldService>().InRequestScope();
            Bind<IPumpService>().To<PumpService>().InRequestScope();
            Bind<IClassifierService>().To<ClassifierService>().InRequestScope();
            Bind<IRepairTypeService>().To<RepairTypeService>().InRequestScope();
            Bind<IHistoryService>().To<HistoryService>().InRequestScope();
            Bind<IShopService>().To<ShopService>().InRequestScope();
            Bind<ITicketService>().To<TicketService>().InRequestScope();
            Bind<ITicketRouteService>().To<TicketRouteService>().InRequestScope();
            Bind<ITicketImportService>().To<TicketImportService>().InRequestScope();
            Bind<IWellRegularService>().To<WellRegularService>().InRequestScope();

            Mapper.Initialize(configuration => {
                configuration.CreateMap<TicketMovementNotification, TicketMovementNotificationModel>();

                configuration.CreateMap<ControlledWellCreateModel, ControlledWell>();
                configuration.CreateMap<ControlledWell, ControlledWellCreateModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.PumpID, option => option.MapFrom(x => x.Pump.Id))
                    .ReverseMap();

                configuration.CreateMap<Enterprise, EnterpriseModel>()
                    .ReverseMap();

                configuration.CreateMap<Pump, PumpModel>()
                    .ReverseMap();

                configuration.CreateMap<TicketDetailA1, TicketDetailA1>();
                configuration.CreateMap<TicketDetailA1, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailA2, TicketDetailA2>();
                configuration.CreateMap<TicketDetailA2, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailA3, TicketDetailA3>();
                configuration.CreateMap<TicketDetailA3, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailA4, TicketDetailA4>();
                configuration.CreateMap<TicketRegistrationModel, TicketDetailA4>();
                configuration.CreateMap<TicketDetailA4, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB1, TicketDetailB1>();
                configuration.CreateMap<TicketDetailB1, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB1A, TicketDetailB1A>();
                configuration.CreateMap<TicketDetailB1A, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB1B, TicketDetailB1B>();
                configuration.CreateMap<TicketDetailB1B, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB2, TicketDetailB2>();
                configuration.CreateMap<TicketDetailB2, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB3, TicketDetailB3>();
                configuration.CreateMap<TicketDetailB3, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB4, TicketDetailB4>();
                configuration.CreateMap<TicketDetailB4, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB5, TicketDetailB5>();
                configuration.CreateMap<TicketDetailB5, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB6, TicketDetailB6>();
                configuration.CreateMap<TicketDetailB6, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB7, TicketDetailB7>();
                configuration.CreateMap<TicketDetailB7, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB8, TicketDetailB8>();
                configuration.CreateMap<TicketDetailB8, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB9, TicketDetailB9>();
                configuration.CreateMap<TicketDetailB9, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB10, TicketDetailB10>();
                configuration.CreateMap<TicketDetailB10, TicketDetailModel>()
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Well.Id, x => x.Ignore())
                    .ForPath(destination => destination.Well.Code, x => x.Ignore());

                configuration.CreateMap<TicketDetailB11, TicketDetailB11>();
                configuration.CreateMap<TicketDetailB11, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB12, TicketDetailB12>();
                configuration.CreateMap<TicketDetailB12, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailB13, TicketDetailB13>();
                configuration.CreateMap<TicketDetailB13, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());
                
                configuration.CreateMap<TicketDetailC, TicketDetailC>();
                configuration.CreateMap<TicketDetailC, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailD1, TicketDetailD1>();
                configuration.CreateMap<TicketDetailD1, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailD2, TicketDetailD2>();
                configuration.CreateMap<TicketDetailD2, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE1, TicketDetailE1>();
                configuration.CreateMap<TicketDetailE1, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE2, TicketDetailE2>();
                configuration.CreateMap<TicketDetailE2, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE3, TicketDetailE3>();
                configuration.CreateMap<TicketDetailE3, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore())
                    .ForPath(destination => destination.PullerType.Id, x => x.Ignore())
                    .ForPath(destination => destination.PullerType.Name, x => x.Ignore()); ;

                configuration.CreateMap<TicketDetailE4, TicketDetailE4>();
                configuration.CreateMap<TicketDetailE4, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE5, TicketDetailE5>();
                configuration.CreateMap<TicketDetailE5, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE6, TicketDetailE6>();
                configuration.CreateMap<TicketDetailE6, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE7, TicketDetailE7>();
                configuration.CreateMap<TicketDetailE7, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailE8, TicketDetailE8>();
                configuration.CreateMap<TicketDetailE8, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailF, TicketDetailF>();
                configuration.CreateMap<TicketDetailF, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());

                configuration.CreateMap<TicketDetailG, TicketDetailG>();
                configuration.CreateMap<TicketDetailG, TicketDetailModel>()
                    .ForMember(destination => destination.WellID, option => option.MapFrom(x => x.Well.Id))
                    .ForMember(destination => destination.FieldCode, option => option.MapFrom(x => x.Field.Code))
                    .ForMember(destination => destination.TicketID, option => option.MapFrom(x => x.Ticket.Id))
                    .ForMember(destination => destination.TicketCreated, option => option.MapFrom(x => x.Ticket.Created))
                    .ForMember(destination => destination.MembershipID, option => option.MapFrom(x => x.Membership.Id))
                    .ForMember(destination => destination.TicketType, option => option.MapFrom(x => x.Ticket.TicketType))
                    .ReverseMap()
                    .ForPath(destination => destination.Field.Code, x => x.Ignore())
                    .ForPath(destination => destination.Field.Name, x => x.Ignore());
            });
        }
    }
}
