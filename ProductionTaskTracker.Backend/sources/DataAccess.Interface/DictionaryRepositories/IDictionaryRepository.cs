﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.DictionaryRepositories {
    using Domain.Entities;

    public interface IDictionaryRepository<out TEntity> where TEntity : DictionaryEntity {
        IEnumerable<TEntity> GetAll();

        TEntity Find(string code);
    }
}