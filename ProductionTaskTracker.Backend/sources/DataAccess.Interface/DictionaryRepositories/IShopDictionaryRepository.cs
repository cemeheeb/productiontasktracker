﻿namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.DictionaryRepositories {
    using Domain.Entities;

    public interface IShopDictionaryRepository : IDictionaryRepository<Shop> {
    }
}
