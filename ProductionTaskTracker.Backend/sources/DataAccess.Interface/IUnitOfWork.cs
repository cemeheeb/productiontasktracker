﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess {
    public interface IUnitOfWork: IDisposable {
        /// <summary>
        /// Сохранить изменения в базу
        /// </summary>
        void Commit();
    }
}
