﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Services {
    using Domain.DTO;

    public interface IRegisterService {
        void PendingInclude(long id);

        void CalculateRegister();

        IEnumerable<RegisterRecord> GetByWellCode(long wellID, DateTime dateBegin, DateTime dateEnd, int index, int size, out int sizeTotal);
    }
}
