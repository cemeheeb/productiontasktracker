﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Services {
    using Domain.DTO;

    public interface IReportDataService {
        IEnumerable<RegisterRecord> GetReportAData(DateTime dateBegin, DateTime dateEnd, long[] departments, long[] wells, string[] ticketTypes, int? SL, bool expiredOnly);
    }
}
