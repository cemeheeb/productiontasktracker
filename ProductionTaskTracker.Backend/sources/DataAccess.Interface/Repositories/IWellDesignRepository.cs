﻿namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IWellDesignRepository : IRepository<WellDesign> {
    }
}
