﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IWellRegularRepository: IRepository<WellRegular> {
        IEnumerable<WellRegular> Search(string search, int index, int size, out int sizeTotal);

        IEnumerable<WellRegular> Search(string search, Department department, int index, int size, out int sizeTotal);

        IEnumerable<WellRegular> Search(string search, IEnumerable<Department> departments, int index, int size, out int sizeTotal);

        WellExecutionMode GetExecutionMode(long wellID);

        IEnumerable<string> GetMeasureFacilityClusters(string[] shopCode, string fieldCode);
    }
}
