﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Enumerables;
    using Domain.Entities;

    public interface IClassifierRepository {
        Classifier Create(Classifier classifierEntity);

        Classifier Find(ClassifierKind kind, long id);

        IEnumerable<Classifier> Filter(ClassifierKind kind, int index, int size, out int sizeTotal);

        IEnumerable<Classifier> Filter(ClassifierKind kind);

        void Update(Classifier entity);
    }
}