﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using DictionaryRepositories;
    using Domain.Entities;

    public interface IFieldDictionaryRepository: IDictionaryRepository<Field> {
        IEnumerable<Field> Search(string shop, string search, int index, int size, out int sizeTotal);

        IEnumerable<Field> GetByShopCodes(string[] shopCodes);

        IEnumerable<Field> GetMeasurePositionFields(IEnumerable<string> shopCodes);
    }
}
