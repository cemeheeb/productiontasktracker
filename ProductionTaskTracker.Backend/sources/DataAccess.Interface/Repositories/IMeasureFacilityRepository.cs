﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IMeasureFacilityRepository : IRepository<MeasureFacility> {
        IEnumerable<int> GetPositions(string[] shopCode, string fieldCode, string cluster);

        Shop GetShopByMeasureFacilityPosition(string code, string cluster, int measureFacilityPosition);
    }
}
