﻿using System.Collections.Generic;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface ITicketRouteRepository : IRepository<TicketRoute> {
        /// <summary>
        /// Возвращает список маршрутов по классу заявки, идентификатору подразделения
        /// </summary>
        /// <param name="department">Идентификатор подразделения</param>
        /// <returns></returns>
        IEnumerable<TicketRoute> GetRoutes(Department department);

        /// <summary>
        /// Возвращает список маршрутов по классу заявки, должностной роли и идентификатору подразделения
        /// </summary>
        /// <param name="positionRole">Должностная роль</param>
        /// <param name="department">Идентификатор подразделения</param>
        /// <returns></returns>
        IEnumerable<TicketRoute> GetRoutes(PositionRole positionRole, Department department);

        /// <summary>
        /// Проверка возможности совершения перехода заявки в указанный статус
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="position"></param>
        /// <param name="statusFrom"></param>
        /// <param name="statusTo"></param>
        /// <returns></returns>
        bool Validate(Ticket ticket, Position position, TicketStatus? statusFrom, TicketStatus statusTo);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="position"></param>
        /// <param name="statusFrom"></param>
        /// <param name="statusTo"></param>
        /// <returns></returns>
        IEnumerable<Department> GetAllowedDepartments(Ticket ticket, Position position, TicketStatus? statusFrom, TicketStatus statusTo);
    }
}
