﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.DTO;
    using Domain.Entities;

    /// <summary>
    /// Заявка
    /// </summary>
    public interface ITicketRepository: IRepository<Ticket> {
        /// <summary>
        /// Получение списка входящих заявок пользователя на указанную дату
        /// </summary>
        IEnumerable<TicketTableRecord> GetIncoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size, out int sizeTotal);

        /// <summary>
        /// Получает допустимые значения для выбранного фильтра по переданному значению фильтра для таблицы входящих заявок
        /// </summary>
        IDictionary<string, IEnumerable<FilterOption>> GetIncomingFilterOptions(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd);

        /// <summary>
        /// Получение списка исходящих заявок пользователя на указанную дату
        /// </summary>
        IEnumerable<TicketTableRecord> GetOutcoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size, out int sizeTotal);

        /// <summary>
        /// Получает допустимые значения для выбранного фильтра по переданному значению фильтра для таблицы исходящих заявок
        /// </summary>
        IDictionary<string, IEnumerable<FilterOption>> GetOutcomingFilterOptions(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd);

        /// <summary>
        /// Возвращает список заявок перешедших в статус "Новый" в указанный период
        /// </summary>
        IEnumerable<TicketPending> GetTicketPendings();

        /// <summary>
        /// Возвращает номер следующий свободный группы для группы
        /// </summary>
        /// <returns></returns>
        long GetTicketIDSequenceNext();

        /// <summary>
        /// Возвращает список заявок по идентификатору группы
        /// </summary>
        /// <param name="ticketGroupID"></param>
        IEnumerable<Ticket> FindByGroupID(long ticketGroupID);
    }
}
