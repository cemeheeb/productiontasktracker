﻿
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.DTO;
    using Domain.Entities;

    public interface ITicketMovementRepository : IRepository<TicketMovement> {
        IEnumerable<TicketMovementNotification> GetNotifications(Membership membership, Department department);

        TicketMovement GetLatest(Ticket ticket);
    }
}
