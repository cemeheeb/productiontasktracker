﻿
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IPumpRepository : IRepository<Pump> {
        /// <summary>
        /// Возвращает список насосов удовлетворяющих условию поиска search
        /// </summary>
        IEnumerable<Pump> Search(string search, int index, int size, out int sizeTotal);

        /// <summary>
        /// Удаление всех насосов из БД ЭПУ
        /// </summary>
        void Clear();
    }
}
