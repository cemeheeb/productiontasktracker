﻿using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;
    using Domain.Enumerables;


    public interface IControlledWellRepository: IRepository<ControlledWell> {
        IEnumerable<ControlledWell> GetByFilter(ControlledWellFilter filter, int index, int size, out int sizeTotal);

        IEnumerable<ControlledWell> GetNotGeneratedAt(ControlledWellFilter filter, DateTime time);
    }
}
