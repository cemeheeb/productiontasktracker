﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface ITicketVisitRepository : IRepository<TicketVisit> {
        IEnumerable<TicketVisit> GetByTicketDetailID(long ticketDetailID);
    }
}
