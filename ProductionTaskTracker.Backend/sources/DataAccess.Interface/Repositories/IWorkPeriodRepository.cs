﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IWorkPeriodRepository : IRepository<WorkPeriod> {
        IEnumerable<WorkPeriod> GetByMembership(Membership membership);

        WorkPeriod GetByMembershipActive(Membership membership);

        IEnumerable<WorkPeriod> GetActive();
    }
}
