﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IMembershipRepository : IRepository<Membership> {
        Membership FindByUserName(string userName);

        IEnumerable<Membership> GetAll();

        Membership GetWinServiceUser();

        IEnumerable<Membership> Search(int index, int size, out int sizeTotal);
    }
}