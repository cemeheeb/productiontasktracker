﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities.TicketDetails;

    public interface ITicketDetailRepository : IRepository<TicketDetailBase> {
        IEnumerable<TicketDetailBase> FindIn(long[] ticketDetailIDList);
    }
}
