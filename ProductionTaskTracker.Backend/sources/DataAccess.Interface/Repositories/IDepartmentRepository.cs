﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IDepartmentRepository : IRepository<Department> {
        IEnumerable<Department> GetAll();

        Department FindByName(string name);
    }
}
