﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IAutocompleteOptionRepository {
        /// <summary>
        /// Получение подсказок по полю
        /// </summary>
        /// <param name="parameter">Название параметра</param>
        /// <returns></returns>
        IEnumerable<AutocompleteOption> Filter(Membership membership, string parameter);

        /// <summary>
        /// Регистрация слова или его использования
        /// </summary>
        /// <param name="parameter">Название параметра</param>
        /// <param name="text">Значение</param>
        void Activate(Membership membership, string parameter, string text);

        /// <summary>
        /// Удаление параметра, запись с параметром удаляется физически из БД
        /// </summary>
        /// <param name="parameter">Название параметра</param>
        /// <param name="text">Значение</param>
        void Remove(Membership membership, string parameter, string text);
    }
}
