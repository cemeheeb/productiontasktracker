﻿

using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IEnterpriseRepository : IRepository<Enterprise> {
        Enterprise Find(string code);

        IEnumerable<Enterprise> Search(string search, int index, int size, out int sizeTotal);
    }
}
