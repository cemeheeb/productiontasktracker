﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IRepository<TEntity> where TEntity : Entity {
        IEnumerable<TEntity> GetAll();

        TEntity Find(long id);

        IEnumerable<TEntity> Find(long[] ids);

        TEntity Create(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
    }
}