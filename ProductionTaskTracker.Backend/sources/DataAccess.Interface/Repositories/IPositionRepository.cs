﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.Repositories {
    using Domain.Entities;

    public interface IPositionRepository : IRepository<Position> {
        IEnumerable<Position> FindPositionsByRolename(Department department, string rolename);

        PositionRole FindRole(Department department, string rolename);
    }
}
