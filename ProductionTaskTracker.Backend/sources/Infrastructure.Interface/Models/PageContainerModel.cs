﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Models {
    /// <summary>
    /// Абстрактная модель-обертка реализуюшая механизм т.н. пагинации
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageContainerModel<T> {
        public PageContainerModel(IEnumerable<T> data) {
            Data = data;
        }

        [Display(Name = "Данные")]
        [JsonProperty(PropertyName = "data")]
        public IEnumerable<T> Data { get; }

        [Display(Name = "Количество заявок без страничной фильтрации")]
        [JsonProperty(PropertyName = "size_total")]
        public int SizeTotal { get; set; }
    }
}