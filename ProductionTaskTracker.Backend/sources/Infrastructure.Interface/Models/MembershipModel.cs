﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Models {
    using Domain.Entities;
    using System;
    using System.Linq;

    public class MembershipModel {
        public MembershipModel() {
        }

        public MembershipModel(Membership membership)
        {
            Id = membership.Id;
            IsLocal = !string.IsNullOrEmpty(membership.Password);
            FirstName = membership.FirstName;
            LastName = membership.LastName;
            Patronym = membership.Patronym;
            UserName = membership.UserName;
            FullName = membership.FullName;
            Roles = membership.Roles != null && membership.Roles.Count > 0 ? membership.Roles.Select(x=>x.Name).Aggregate((x, y) => x + ',' + y) : null;
            Created = membership.Created;
        }

        [Required]
        [Display(Name = "Идентификатор")]
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [Display(Name = "Имя пользователя")]
        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        [Display(Name = "Признак локального пользователя")]
        [JsonProperty(PropertyName = "is_local")]
        public bool IsLocal { get; set; }

        [Display(Name = "Имя")]
        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [JsonProperty(PropertyName = "lastname")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        [JsonProperty(PropertyName = "patronym")]
        public string Patronym { get; set; }

        [Display(Name = "ФИО")]
        [JsonProperty(PropertyName = "fullname")]
        public string FullName { get; set; }

        [Display(Name = "Административные роли")]
        [JsonProperty(PropertyName = "rolename")]
        public string Roles { get; set; }

        [Display(Name = "ФИО")]
        [JsonProperty(PropertyName = "created")]
        public DateTime? Created { get; set; }
    }
}
