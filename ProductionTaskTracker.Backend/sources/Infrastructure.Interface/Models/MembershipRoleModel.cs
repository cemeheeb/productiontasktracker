﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Models {
    public class MembershipRoleModel {
        public MembershipRoleModel(Domain.Entities.MembershipRole role) {
            Name = role.Name;
            DisplayName = role.DisplayName;
        }

        [Required]
        [Display(Name = "Наименование")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Отображаемое название")]
        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }
    }
}
