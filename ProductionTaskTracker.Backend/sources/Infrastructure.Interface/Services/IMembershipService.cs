﻿using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Services {
    using Domain.Entities;
    using Models;

    public interface IMembershipService {
        IEnumerable<MembershipModel> GetAll();

        PageContainerModel<MembershipModel> Search(int index, int size);

        /// <summary>
        /// Возвращает экземпляр Membership в случае если переданы действительные имя пользователя и пароль
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// /// <param name="password">Пароль пользователя</param>
        /// <returns></returns>
        Membership ValidateLocal(string userName, string password);

        /// <summary>
        /// Возвращает экземпляр Membership в случае если пользователь с указанным именем найден в ActiveDirectory и не заблокирован
        /// </summary>
        /// <param name="userName">Имя пользователя в ActiveDirectory</param>
        /// /// <param name="password">Пароль пользователя</param>
        /// <returns></returns>
        Membership ValidateActiveDirectory(string userName, string password, string domain);

        MembershipModel Create(MembershipModel model);

        MembershipModel Update(MembershipModel model);
    }
}