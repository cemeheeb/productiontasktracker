﻿namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Services {
    public interface ISettingsService {
        string GetActiveDirectoryLpadPath();

        string GetRemoteServer();
    }
}
