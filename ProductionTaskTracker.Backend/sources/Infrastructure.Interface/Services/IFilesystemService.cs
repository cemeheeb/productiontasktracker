﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Infrastructure.Services {
    using Domain.Entities;

    public interface IFilesystemService {
        string GetMembershipTemporaryPath(Membership membership);

        Uri GetMembershipTemporaryUri(Membership membership);
        
        string GetMembershipReportPath(Membership membership, string uid);

        Uri GetMembershipReportUri(Membership membership, string uid);

        string GetMembershipImportPath(Membership membership, string uid);

        Uri GetMembershipImportUri(Membership membership, string uid);

        string GetAttachmentPath();

        Uri GetAttachmentUri();
    }
}
