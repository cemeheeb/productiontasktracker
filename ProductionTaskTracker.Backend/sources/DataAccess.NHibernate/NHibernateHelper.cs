﻿using System;

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

using NHibernate;
using NHibernate.Context;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    using Mappings;

    internal static class NHibernateHelper {
        #region Fields

        private static readonly object LockObject = new object();
        private static ISessionFactory _sessionFactory;
        
        #endregion

        public static ISessionFactory SessionFactory {
            get {
                if (_sessionFactory == null) {
                    lock (LockObject) {
                        if (_sessionFactory != null)
                            return _sessionFactory;

                        _sessionFactory = Fluently.Configure()
                            .Database(OracleClientConfiguration.Oracle10.AdoNetBatchSize(50).ConnectionString(x => x.FromConnectionStringWithKey("default")))
#if RELEASE || RELEASE_TEST
                            .CurrentSessionContext("thread_static")
#else
                            .CurrentSessionContext("web")
#endif
                            .Mappings(x => x.FluentMappings
                                .AddFromAssemblyOf<MembershipMap>()
                                .Conventions
                                    .Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())
                            )
                            .ExposeConfiguration(configuration => configuration.SetInterceptor(new QueryLoggerInterceptor()))
                            .BuildSessionFactory();
                    }

                    return _sessionFactory;
                }

                return _sessionFactory;
            }
        }


        public static ISession GetSession() {
            if (CurrentSessionContext.HasBind(SessionFactory))
                return SessionFactory.GetCurrentSession();

            throw new InvalidOperationException("Database access logic cannot be used, if session not opened. Implicitly session usage not allowed now. Please open session explicitly through UnitOfWorkFactory.StartLongConversation method");
        }
    }
}
