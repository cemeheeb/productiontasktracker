﻿using NHibernate;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    public interface ISessionProvider {
        ISession Session { get; }
    }
}
