﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using NHibernate.Transform;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    public class ResultTransformer: IResultTransformer {
        #region Fields

        private readonly Type _result;
        private readonly PropertyInfo[] _properties;

        #endregion

        #region IResultTransformer

        public IList TransformList(IList collection) {
            return collection;
        }

        public ResultTransformer(Type result, params string[] names) {
            this._result = result;
            _properties = names.Select(result.GetProperty).ToArray();
        }

        public object TransformTuple(object[] tuple, string[] aliases) {
            var instance = Activator.CreateInstance(_result);
            for (var i = 0; i < tuple.Length; i++) {
                var t = _properties[i].Name;

                var value = tuple[i].GetType().GetProperty(t).GetValue(tuple[i], null);
                instance.GetType().GetProperty(t).SetValue(instance, value, null);
            }
            return instance;
        }

        #endregion
    }
}
