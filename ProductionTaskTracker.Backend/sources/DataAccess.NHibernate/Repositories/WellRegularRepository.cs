﻿using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class WellRegularRepository: Repository<WellRegular>, IWellRegularRepository {
        public WellRegularRepository(Ninject.Extensions.Logging.ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<WellRegular> Search(string search, int index, int size, out int sizeTotal) {
            var countCriteria = Session.CreateCriteria<Well>("wells").SetProjection(Projections.Count("Id"));
            var criteria = Session.CreateCriteria<Well>("wells");

            if (!string.IsNullOrEmpty(search)) {
                countCriteria.Add(Restrictions.Like("wells.Code", search, MatchMode.Start));
                criteria.Add(Restrictions.Like("wells.Code", search, MatchMode.Start));
            }

            sizeTotal = countCriteria.UniqueResult<int>();

            var output = criteria.AddOrder(Order.Asc("wells.Id"))
                .SetFirstResult((index - 1) * size)
                .SetMaxResults(size)
                .List<WellRegular>();

            return output;
        }

        public IEnumerable<WellRegular> Search(string search, Department department, int index, int size, out int sizeTotal) {
            var output = Session.CreateCriteria<WellRegular>("wells")
                .Add(Restrictions.Like("wells.Code", search, MatchMode.Start).IgnoreCase())
                .Add(Restrictions.Eq("wells.Shop.Code", department.Shop.Code))
                .AddOrder(Order.Asc("wells.Code"))
                .SetFirstResult((index - 1) * size)
                .SetMaxResults(size)
                .List<WellRegular>();

            sizeTotal = output.Count;
            return output;
        }

        public IEnumerable<WellRegular> Search(string search, IEnumerable<Department> departments, int index, int size, out int sizeTotal) {
            var output = Session.CreateCriteria<WellRegular>("wells")
                .Add(Restrictions.Like("wells.Code", search, MatchMode.Start).IgnoreCase())
                .Add(Restrictions.In("wells.Shop.Code", departments.Where(x => x.Shop != null).Select(x => x.Shop.Code).ToList()))
                .AddOrder(Order.Asc("wells.Code"))
                .SetFirstResult((index - 1) * size)
                .SetMaxResults(size)
                .List<WellRegular>();

            sizeTotal = output.Count;
            return output;
        }

        public WellExecutionMode GetExecutionMode(long wellID) {
            return Session.CreateSQLQuery(@"
                    select qj_1 Qj, qn_1 Qn, SYSDATE Timestamp
                    from zam z
                    where sk_1 = :wellID and rownum = 1
                ")
                .AddScalar("Qj", NHibernateUtil.Decimal)
                .AddScalar("Qn", NHibernateUtil.Decimal)
                .AddScalar("Timestamp", NHibernateUtil.DateTime)
                .SetParameter("wellID", wellID)
                .SetResultTransformer(Transformers.AliasToBean<WellExecutionMode>())
                .UniqueResult<WellExecutionMode>();
        }

        public IEnumerable<string> GetMeasureFacilityClusters(string[] shopCodes, string fieldCode) {
            var prepared = shopCodes.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (prepared.IsEmpty() || string.IsNullOrEmpty(fieldCode))
                return new string[0];

            return Session.CreateSQLQuery(@"
                    select distinct cluster_code
                    from tt_measure_facilities
                    where shop_code in (:shopCodes) and field_code = :fieldCode
                    order by cluster_code
                ")
                .AddScalar("cluster_code", NHibernateUtil.String)
                .SetParameterList("shopCodes", prepared)
                .SetParameter("fieldCode", fieldCode)
                .List<string>();
        }
    }
}
