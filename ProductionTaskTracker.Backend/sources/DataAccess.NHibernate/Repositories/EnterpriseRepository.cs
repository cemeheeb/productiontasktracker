﻿using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class EnterpriseRepository : Repository<Enterprise>, IEnterpriseRepository {
        #region Fields

        private readonly ILogger _logger;

        #endregion

        public EnterpriseRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public Enterprise Find(string code) {
            try {
                return Session.CreateCriteria<Enterprise>("enterprises").Add(Restrictions.Eq("enterprises.code", code)).UniqueResult() as Enterprise;
            } catch (Exception exception) {
                _logger.ErrorException($"Не удалось найти {typeof(Enterprise)}. #{code}", exception);
            }

            return null;
        }

        public IEnumerable<Enterprise> Search(string search, int index, int size, out int sizeTotal) {
            var countCriteria = Session.CreateCriteria<Enterprise>("enterprises").SetProjection(Projections.Count("Id"));
            var criteria = Session.CreateCriteria<Enterprise>("enterprises");

            if (!string.IsNullOrEmpty(search)) {
                countCriteria.Add(Restrictions.Like("enterprises.Name", search, MatchMode.Anywhere));
                criteria.Add(Restrictions.Like("enterprises.Name", search, MatchMode.Anywhere));
            }

            sizeTotal = countCriteria.AddOrder(Order.Asc("enterprises.Name")).UniqueResult<int>();

            var output = criteria
                .AddOrder(Order.Asc("enterprises.Name"))
                .SetFirstResult(index * size)
                .SetMaxResults(size)
                .List<Enterprise>();

            return output;
        }
    }
}
