﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using System;
    using DataAccess.Repositories;
    using Domain.Entities;

    public class DepartmentRepository: Repository<Department>, IDepartmentRepository {
        public DepartmentRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<Department> GetAll() {
            var output = Session
                .CreateCriteria<Department>()
                .Add(Restrictions.Eq("Deleted", false))
                .SetResultTransformer(Transformers.DistinctRootEntity)
                .List<Department>();
            return output;
        }

        public Department FindByName(string name) {
            return Session.Query<Department>().FirstOrDefault(x => (x.DisplayName == name || x.FullName == name) && !x.Deleted);
        }
    }
}
