﻿using System;
using System.Collections.Generic;

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;
using Ninject.Extensions.Logging;
using ILoggerFactory = Ninject.Extensions.Logging.ILoggerFactory;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;
    using Domain.Enumerables;

    public class ControlledWellRepository : Repository<ControlledWell>, IControlledWellRepository {
        #region

        private readonly ILogger _logger;

        #endregion

        public ControlledWellRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public IEnumerable<ControlledWell> GetByFilter(ControlledWellFilter filter, int index, int size, out int sizeTotal) {
            var criteria = Session.CreateCriteria<ControlledWell>()
                .Add(Restrictions.Eq("Deleted", false));

            switch (filter) {
                case ControlledWellFilter.VNR:
                    criteria = criteria.Add(Restrictions.Eq("TicketType", "D1"));
                    break;
                case ControlledWellFilter.Other:
                    criteria = criteria.Add(Restrictions.Not(Restrictions.Eq("TicketType", "D1")));
                    break;
            }

            sizeTotal = ((ICriteria)criteria.Clone())
                .SetProjection(Projections.Count(Projections.Id()))
                .UniqueResult<int>();

            var output = criteria
                .AddOrder(Order.Asc("Id"))
                .SetFirstResult(index * size)
                .SetMaxResults(size)
                .List<ControlledWell>();

            return output;
        }

        public IEnumerable<ControlledWell> GetNotGeneratedAt(ControlledWellFilter filter, DateTime time) {
            var criteria = Session.CreateCriteria<ControlledWell>()
                .Add(Restrictions.Eq("Deleted", false));

            switch (filter) {
                case ControlledWellFilter.VNR:
                    criteria = criteria.Add(Restrictions.Eq("TicketType", "D1"));
                    break;
                case ControlledWellFilter.Other:
                    criteria = criteria.Add(Restrictions.Not(Restrictions.Eq("TicketType", "D1")));
                    break;
            }

            var output = criteria
                .Add(Restrictions.Lt("Recent", time))
                .AddOrder(Order.Asc("Id"))
                .List<ControlledWell>();

            var criteriaImpl = (CriteriaImpl)criteria.Add(Restrictions.Lt("Recent", time)).AddOrder(Order.Asc("Id"));
            var sessionImpl = (SessionImpl)criteriaImpl.Session;
            var factory = (SessionFactoryImpl)sessionImpl.SessionFactory;
            var implementors = factory.GetImplementors(criteriaImpl.EntityOrClassName);
            var loader = new CriteriaLoader((IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]), factory, criteriaImpl, implementors[0], sessionImpl.EnabledFilters);
            _logger.Info(">>>>>>>");
            _logger.Info(loader.SqlString.ToString());

            return output;
        }
    }
}
