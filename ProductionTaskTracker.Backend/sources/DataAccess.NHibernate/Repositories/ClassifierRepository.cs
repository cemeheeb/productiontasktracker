﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Enumerables;
    using Domain.Entities;
    using System;

    public class ClassifierRepository : IClassifierRepository {
        #region Fields

        private readonly ISessionProvider _sessionProvider;

        #endregion

        public ClassifierRepository(ISessionProvider sessionProvider) {
            _sessionProvider = sessionProvider;
        }
        
        protected ISession Session => _sessionProvider.Session;

        public Classifier Create(Classifier entity) {
            var dialect = Session.GetSessionImplementation().Factory.Dialect;
            var sqlQuery = dialect.GetSequenceNextValString("HIBERNATE_SEQUENCE");
            entity.Id = Convert.ToInt64(Session.CreateSQLQuery(sqlQuery).UniqueResult<decimal>());
            return Session.Save(entity) as Classifier;
        }

        public Classifier Find(ClassifierKind kind, long id) {
            return Session.CreateCriteria<Classifier>().Add(Restrictions.Eq("Kind", kind)).Add(Restrictions.Eq("Id", id)).UniqueResult<Classifier>();
        }

        private ICriteria CreateCriteriaByKind(ClassifierKind kind) {
            var criteria = Session.CreateCriteria<Classifier>();
            return kind == ClassifierKind.RT_ ? 
                criteria.Add(Restrictions.In("Kind", new[] { ClassifierKind.RTE, ClassifierKind.RTS })) : 
                criteria.Add(Restrictions.Eq("Kind", kind));
        }

        public IEnumerable<Classifier> Filter(ClassifierKind kind, int index, int size, out int sizeTotal) {
            sizeTotal = CreateCriteriaByKind(kind)
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return CreateCriteriaByKind(kind)
                .SetFirstResult((index - 1) * size)
                .SetMaxResults(size)
                .List<Classifier>();
        }

        public IEnumerable<Classifier> Filter(ClassifierKind kind) {
            var output = Session.CreateCriteria<Classifier>();
            switch (kind)
            {
                case ClassifierKind.RT_:
                    output = output.Add(Restrictions.In("Kind", new[] { ClassifierKind.RTE, ClassifierKind.RTS }));
                    break;
                default:
                    output = output.Add(Restrictions.Eq("Kind", kind));
                    break;
            }

            return output.List<Classifier>();
        }

        public void Delete(Classifier entity) {
            Session.Delete(entity);
        }

        public void Update(Classifier entity) {
            Session.Update(entity);
        }
    }
}
