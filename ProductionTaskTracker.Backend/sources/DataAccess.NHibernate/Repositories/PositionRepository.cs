﻿using System.Collections.Generic;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using System;
    using DataAccess.Repositories;
    using Domain.Entities;

    public class PositionRepository: Repository<Position>, IPositionRepository {
        public PositionRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<Position> FindPositionsByRolename(Department department, string rolename) {
            return Session.CreateCriteria<Position>("p")
                .CreateCriteria("Rolenames", "r", JoinType.InnerJoin)
                .Add(Restrictions.Eq("p.Department", department))
                .Add(Restrictions.Eq("r.Name", rolename))
                .List<Position>();
        }

        public PositionRole FindRole(Department department, string rolename) {
            return Session.CreateCriteria<PositionRole>()
                .Add(Restrictions.Eq("Name", rolename))
                .UniqueResult<PositionRole>();
        }
    }
}
