﻿using System;
using System.Collections.Generic;
using Ninject.Extensions.Logging;
using NH = NHibernate;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class AutocompleteOptionRepository : Repository<AutocompleteOption>, IAutocompleteOptionRepository {
        public AutocompleteOptionRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider): base(loggerFactory, sessionProvider) {
        }
        public IEnumerable<AutocompleteOption> Filter(Membership membership, string parameter) {
            var options = Session
                .QueryOver<AutocompleteOption>()
                .Where(x => x.Membership.Id == membership.Id)
                .Where(x => x.Parameter == parameter)
                .List();

            return options;
        }

        public void Activate(Membership membership, string parameter, string text) {
            if (string.IsNullOrEmpty(text)) {
                return;
            }

            var options = Session
                .QueryOver<AutocompleteOption>()
                .Where(x => x.Membership.Id == membership.Id)
                .Where(x => x.Parameter == parameter)
                .Where(x => x.Text == text).List();

            if (options.Count > 0) {
                var option = options[0];
                option.Weight++;
                Session.Save(option);
            } else {
                var newOption = new AutocompleteOption() { Membership = membership, Parameter = parameter, Text = text, Weight = 0 };
                Session.Save(newOption);
            }
        }

        public void Remove(Membership membership, string parameter, string text) {
            var options = Session
                .QueryOver<AutocompleteOption>()
                .Where(x => x.Membership.Id == membership.Id)
                .Where(x => x.Parameter == parameter)
                .Where(x => x.Text == text).List();

            if (options.Count > 0) {
                Session.Delete(options[0]);
            }
        }
    }
}
