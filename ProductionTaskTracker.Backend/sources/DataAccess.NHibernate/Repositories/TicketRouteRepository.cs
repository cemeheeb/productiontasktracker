﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Enumerables;
    using Domain.Entities;

    public class TicketRouteRepository: Repository<TicketRoute>, ITicketRouteRepository {

        #region Ninject

        private readonly ILogger _logger;

        #endregion

        public TicketRouteRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public IEnumerable<TicketRoute> GetRoutes(Department department) {
            return Session
                .CreateCriteria<TicketRoute>()
                .Add(Restrictions.Eq("Deleted", false))
                .Add(Restrictions.Eq("DepartmentFrom", department))
                .List<TicketRoute>();
        }

        public IEnumerable<TicketRoute> GetRoutes(PositionRole positionRole, Department department) {
            var scanline = department;
            while (scanline.Parent != null) {
                var ticketRoutes = Session
                    .CreateCriteria<TicketRoute>()
                    .Add(Restrictions.Eq("Deleted", false))
                    .Add(Restrictions.Eq("DepartmentFrom", scanline))
                    .Add(Restrictions.Eq("PositionRole", positionRole.Name))
                    .List<TicketRoute>();

                if (ticketRoutes.Count > 0)
                    return ticketRoutes;

                scanline = scanline.Parent;
            }

            return new TicketRoute[0];
        }
        
        public bool Validate(Ticket ticket, Position position, TicketStatus? statusFrom, TicketStatus statusTo) {
            // необходимо получить полный список родителей, т.к дочерние подразделения наследуют права родительских
            var positionDepartmentParents = position.Department.ExtractParents();

            return Session
                .CreateCriteria<TicketRoute>()
                .Add(Restrictions.Like("TicketClass", $"{ticket.TicketType.ToString()[0]}", MatchMode.Anywhere))
                .Add(Restrictions.Eq("PositionRole", position.Role.Name))
                .Add(Restrictions.In("DepartmentFrom", positionDepartmentParents))
                .Add(Restrictions.Eq("StatusFrom", statusFrom.HasValue ? (object)statusFrom.Value : DBNull.Value))
                .Add(Restrictions.Lt("DateBegin", ticket.Created))
                .Add(Restrictions.Disjunction().Add(Restrictions.Ge("DateEnd", ticket.Created)).Add(Restrictions.IsNull("DateEnd")))
                .Add(Restrictions.Eq("StatusTo", statusTo))
                .SetProjection(Projections.Count("Id"))
                .UniqueResult<int>() > 0;
        }

        public IEnumerable<Department> GetAllowedDepartments(Ticket ticket, Position position, TicketStatus? statusFrom, TicketStatus statusTo) {
            var positionDepartmentParents = position.Department.ExtractParents();

            return Session.CreateCriteria<TicketRoute>()
                .Add(Restrictions.Like("TicketClass", $"{ticket.TicketType.ToString()[0]}", MatchMode.Anywhere))
                .Add(Restrictions.Eq("PositionRole", position.Role.Name))
                .Add(Restrictions.In("DepartmentFrom", positionDepartmentParents))
                .Add(Restrictions.Eq("StatusFrom", statusFrom.HasValue ? (object)statusFrom.Value : DBNull.Value))
                .Add(Restrictions.Lt("DateBegin", ticket.Created))
                .Add(Restrictions.Disjunction().Add(Restrictions.Ge("DateEnd", ticket.Created)).Add(Restrictions.IsNull("DateEnd")))
                .Add(Restrictions.Eq("StatusTo", statusTo))
                .List<TicketRoute>()
                .Where(x => x.Deleted == false)
                .Select(x => x.DepartmentTo);
        }
    }
}
