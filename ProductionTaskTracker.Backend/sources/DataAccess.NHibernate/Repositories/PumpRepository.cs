﻿using System.Collections.Generic;
using NHibernate.Criterion;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class PumpRepository: Repository<Pump>, IPumpRepository {
        public PumpRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<Pump> Search(string search, int index, int size, out int sizeTotal) {
            var countCriteria = Session.CreateCriteria<Pump>("pumps").SetProjection(Projections.Count("Id"));
            var criteria = Session.CreateCriteria<Pump>("pumps");

            if (!string.IsNullOrEmpty(search)) {
                countCriteria.Add(Restrictions.InsensitiveLike("pumps.Title", search.Trim(), MatchMode.Anywhere));
                criteria.Add(Restrictions.InsensitiveLike("pumps.Title", search.Trim(), MatchMode.Anywhere));
            }

            sizeTotal = countCriteria.UniqueResult<int>();

            var output = criteria
                .AddOrder(Order.Asc("pumps.Title"))
                .SetFirstResult(index * size)
                .SetMaxResults(size)
                .List<Pump>();
            
            return output;
        }

        public void Clear() {
            // Удаление старых записей
            Session.CreateSQLQuery("truncate table tt_pumps").ExecuteUpdate();
        }
    }
}
