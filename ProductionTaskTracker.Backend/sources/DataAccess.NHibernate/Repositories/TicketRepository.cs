﻿using System;
using System.Text;
using System.Collections.Generic;

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using ILoggerFactory = Ninject.Extensions.Logging.ILoggerFactory;
using Newtonsoft.Json;
using System.Linq;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.DTO;
    using Domain.Entities;

    public class TicketRepository: Repository<Ticket>, ITicketRepository {

        #region Constants

        static readonly string[] TicketFilterNames = {
            "department_name_from",
            "department_name_to",
            "ticket_type_name",
            "field_name",
            "well_name",
            "status_name",
            "sl"
        };

        #endregion

        #region Fields
        private Ninject.Extensions.Logging.ILogger _logger;
        #endregion

        public TicketRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public IEnumerable<TicketTableRecord> GetIncoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size, out int sizeTotal) {
            dateEnd = dateEnd.Date.AddDays(1);

            IDictionary<string, object> parameters = new Dictionary<string, object>();
            var conditions = string.Empty;

            var ticketFilter = ParseFilter(filter);
            PrepareFilterConditions(ticketFilter, ref conditions, ref parameters);

            var departments = department.Head.Children.Select(x => x.Id.ToString()).ToList();
            departments.Add(department.Id.ToString());

            var childrenDepartments = departments.Aggregate((a, b) => $"{a}, {b}");

            var queryString = $@"
                select COUNT(*) ""TotalSize""
                from (
                    select ticket_id id
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                        and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                        and ticket_group_id is null
                        and {conditions} 
                        department_id in ({childrenDepartments}) and movement_status > 0
                    union
                    select ticket_group_id id
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                        and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                        and ticket_group_id is not null
                        and {conditions} 
                        department_id in ({childrenDepartments}) and movement_status > 0
                    group by ticket_group_id
                )
            ";

            var sqlQuery = Session.CreateSQLQuery(queryString)
                .AddScalar("TotalSize", NHibernateUtil.Int32)
                .SetParameter("dateBegin", dateBegin)
                .SetParameter("dateEnd", dateEnd);

            foreach (var parameterKey in parameters.Keys) {
                sqlQuery.SetParameter(parameterKey, parameters[parameterKey]);
            }

            sizeTotal = sqlQuery.UniqueResult<int>();

            queryString = $@"
                select * 
                from (
                    select 
                        ticket_id as ""TicketID"",
                        ticket_group_id as ""TicketGroupID"",
                        ticket_type as ""TicketType"",
                        movement_status as ""TicketStatus"",
                        movement_first_display_name as ""DepartmentFrom"",
                        movement_department_first_id as ""DepartmentIDFrom"",
                        department_display_name as ""DepartmentTo"",
                        department_id as ""DepartmentIDTo"",
                        detail_field_name as ""Field"",
                        detail_field_code as ""FieldCode"",
                        detail_cluster_code as ""Cluster"",
                        detail_well as ""Well"",
                        detail_well_id as ""WellID"",
                        created as ""Created"",
                        detail_expired as ""Expired"",
                        completed as ""Completed"",
                        detail_id as ""TicketDetailID"",
                        detail_sl as ""SL"",
                        DECODE(movement_membership_first_id, :membershipID, 1, 0) as ""AllowUnregister"",
                        is_night as ""IsNight"",
                        is_repeated as ""IsRepeated"",
                        movement_created
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                        and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                        and ticket_group_id is null
                        and {conditions} 
                        department_id in ({childrenDepartments}) and movement_status > 0
                    union
                    select
                        NULL as ""TicketID"",
                        ticket_group_id as ""TicketGroupID"",
                        ticket_type as ""TicketType"",
                        movement_status as ""TicketStatus"",
                        movement_first_display_name as ""DepartmentFrom"",
                        movement_department_first_id as ""DepartmentIDFrom"",
                        department_display_name as ""DepartmentTo"",
                        department_id as ""DepartmentIDTo"",
                        '-' as ""Field"",
                        '-' as ""FieldCode"",
                        '-' as ""Cluster"",
                        '-' as ""Well"",
                        NULL as ""WellID"",
                        min(created) as ""Created"",
                        NULL as ""Expired"",
                        min(completed) as ""Completed"",
                        NULL as ""TicketDetailID"",
                        detail_sl as ""SL"",
                        DECODE(movement_membership_first_id, :membershipID, 1, 0) as ""AllowUnregister"",
                        is_night as ""IsNight"",
                        is_repeated as ""IsRepeated"",
                        min(movement_created) movement_created
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                        and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                        and ticket_group_id is not null
                        and {conditions} 
                        department_id  in ({childrenDepartments}) and movement_status > 0
                    group by ticket_group_id, ticket_type, is_night, movement_status, movement_membership_first_id, movement_first_display_name, movement_department_first_id, department_display_name, department_id, detail_sl, is_repeated
                )
                order by ""IsRepeated"" desc nulls last, ""Created"" desc
            ";



            sqlQuery = Session
                .CreateSQLQuery(queryString)
                .AddScalar("TicketID", NHibernateUtil.Int64)
                .AddScalar("TicketGroupID", NHibernateUtil.Int64)
                .AddScalar("TicketType", NHibernateUtil.String)
                .AddScalar("TicketStatus", NHibernateUtil.Int32)
                .AddScalar("DepartmentFrom", NHibernateUtil.String)
                .AddScalar("DepartmentIDFrom", NHibernateUtil.Int64)
                .AddScalar("DepartmentTo", NHibernateUtil.String)
                .AddScalar("DepartmentIDTo", NHibernateUtil.Int64)
                .AddScalar("Field", NHibernateUtil.String)
                .AddScalar("FieldCode", NHibernateUtil.String)
                .AddScalar("Cluster", NHibernateUtil.String)
                .AddScalar("Well", NHibernateUtil.String)
                .AddScalar("WellID", NHibernateUtil.Int64)
                .AddScalar("Created", NHibernateUtil.DateTime)
                .AddScalar("Expired", NHibernateUtil.DateTime)
                .AddScalar("TicketDetailID", NHibernateUtil.Int64)
                .AddScalar("SL", NHibernateUtil.Int32)
                .AddScalar("AllowUnregister", NHibernateUtil.Boolean)
                .AddScalar("IsNight", NHibernateUtil.Boolean)
                .AddScalar("IsRepeated", NHibernateUtil.Boolean)
                .SetFirstResult(index*size)
                .SetMaxResults(size)
                .SetParameter("membershipID", membership.Id)
                .SetParameter("dateBegin", dateBegin)
                .SetParameter("dateEnd", dateEnd)
                .SetResultTransformer(Transformers.AliasToBean<TicketTableRecord>());

            foreach (var parameterKey in parameters.Keys) {
                sqlQuery.SetParameter(parameterKey, parameters[parameterKey]);
            }

            return sqlQuery.List<TicketTableRecord>();
        }

        public IDictionary<string, IEnumerable<FilterOption>> GetIncomingFilterOptions(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd) {
            // Инициализация
            var conditions = string.Empty;
            IDictionary<string, object> parameters = new Dictionary<string, object>();

            var ticketFilter = ParseFilter(filter);
            PrepareFilterConditions(ticketFilter, ref conditions, ref parameters);

            var output = new Dictionary<string, IEnumerable<FilterOption>>();
            foreach (var filterName in TicketFilterNames) {
                string select;
                if (!TryGetFilterSelect(filterName, out select)) {
                    continue;
                }

                var departments = department.Head.Children.Select(x => x.Id.ToString()).ToList();
                departments.Add(department.Id.ToString());

                var childrenDepartments = departments.Aggregate((a, b) => $"{a}, {b}");

                var queryString = $@"
                    select *
                    from (
                        select distinct {select}
                        from tt_register
                        where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                            and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                            and {conditions} 
                            department_id in ({childrenDepartments}) and movement_status > 0
                    ) order by Code
                ";

                var sqlQuery = Session
                    .CreateSQLQuery(queryString)
                    .AddScalar("Code", NHibernateUtil.String)
                    .AddScalar("Value", NHibernateUtil.String)
                    .SetParameter("dateBegin", dateBegin)
                    .SetParameter("dateEnd", dateEnd);

                foreach (var parameterKey in parameters.Keys) {
                    sqlQuery.SetParameter(parameterKey, parameters[parameterKey]);
                }

                output.Add(filterName, sqlQuery.SetResultTransformer(Transformers.AliasToBean<FilterOption>()).List<FilterOption>());
            }

            return output;
        }

        public IEnumerable<TicketTableRecord> GetOutcoming(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd, int index, int size, out int sizeTotal) {
            dateEnd = dateEnd.Date.AddDays(1);

            IDictionary<string, object> parameters = new Dictionary<string, object>();
            var conditions = string.Empty;

            var ticketFilter = ParseFilter(filter);
            PrepareFilterConditions(ticketFilter, ref conditions, ref parameters);

            var departments = department.Head.Children.Select(x => x.Id.ToString()).ToList();
            departments.Add(department.Id.ToString());

            var childrenDepartments = departments.Aggregate((a, b) => $"{a}, {b}");

            var queryStringCount = $@"
                select COUNT(*) ""TotalSize""
                from (
                    select ticket_id id
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                            and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                            and ticket_group_id is null
                            and {conditions} 
                            movement_department_first_id in ({childrenDepartments})
                    union
                    select ticket_group_id id
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                            and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                            and ticket_group_id is not null
                            and {conditions} 
                            movement_department_first_id in ({childrenDepartments})
                    group by ticket_group_id
                )
            ";

            var sqlQueryCount = Session
                .CreateSQLQuery(queryStringCount)
                .AddScalar("TotalSize", NHibernateUtil.Int32)
                .SetParameter("dateBegin", dateBegin)
                .SetParameter("dateEnd", dateEnd);

            foreach (var parameterKey in parameters.Keys) {
                sqlQueryCount.SetParameter(parameterKey, parameters[parameterKey]);
            }

            sizeTotal = sqlQueryCount.UniqueResult<int>();

            var queryString = $@"
                select * from (
                    select 
                        ticket_id as ""TicketID"",
                        ticket_group_id as ""TicketGroupID"",
                        ticket_type as ""TicketType"",
                        movement_status as ""TicketStatus"",
                        movement_first_display_name as ""DepartmentFrom"",
                        movement_department_first_id as ""DepartmentIDFrom"",
                        department_display_name as ""DepartmentTo"",
                        department_id as ""DepartmentIDTo"",
                        detail_field_name as ""Field"",
                        detail_field_code as ""FieldCode"",
                        detail_cluster_code as ""Cluster"",
                        detail_well as ""Well"",
                        detail_well_id as ""WellID"",
                        created as ""Created"",
                        detail_expired as ""Expired"",
                        completed as ""Completed"",
                        detail_id as ""TicketDetailID"",
                        detail_sl as ""SL"",
                        DECODE(movement_membership_first_id, :membershipID, 1, 0) as ""AllowUnregister"",
                        is_night as ""IsNight"",
                        is_repeated as ""IsRepeated"",
                        movement_created
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                           and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                            and ticket_group_id is null
                           and {conditions} 
                           movement_department_first_id in ({childrenDepartments})
                    union
                    select 
                        NULL as ""TicketID"",
                        ticket_group_id as ""TicketGroupID"",
                        ticket_type as ""TicketType"",
                        movement_status as ""TicketStatus"",
                        movement_first_display_name as ""DepartmentFrom"",
                        movement_department_first_id as ""DepartmentIDFrom"",
                        department_display_name as ""DepartmentTo"",
                        department_id as ""DepartmentIDTo"",
                        '-' as ""Field"",
                        '-' as ""FieldCode"",
                        '-' as ""Cluster"",
                        '-' as ""Well"",
                        NULL as ""WellID"",
                        min(created) as ""Created"",
                        NULL as ""Expired"",
                        min(completed) as ""Completed"",
                        NULL as ""TicketDetailID"",
                        detail_sl as ""SL"",
                        DECODE(movement_membership_first_id, :membershipID, 1, 0) as ""AllowUnregister"",
                        is_night as ""IsNight"",
                        is_repeated as ""IsRepeated"",
                        min(movement_created) movement_created
                    from tt_register
                    where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                           and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                           and ticket_group_id is not null
                           and {conditions} 
                           movement_department_first_id in ({childrenDepartments})
                    group by ticket_group_id, ticket_type, is_night, movement_status, movement_membership_first_id, movement_first_display_name, movement_department_first_id, department_display_name, department_id, detail_sl, is_repeated
                )
                order by ""IsRepeated"" desc nulls last, ""Created"" desc
            ";

            var sqlQuery = Session
                .CreateSQLQuery(queryString)
                .AddScalar("TicketID", NHibernateUtil.Int64)
                .AddScalar("TicketType", NHibernateUtil.String)
                .AddScalar("TicketStatus", NHibernateUtil.Int32)
                .AddScalar("TicketGroupID", NHibernateUtil.Int64)
                .AddScalar("DepartmentFrom", NHibernateUtil.String)
                .AddScalar("DepartmentIDFrom", NHibernateUtil.Int64)
                .AddScalar("DepartmentTo", NHibernateUtil.String)
                .AddScalar("DepartmentIDTo", NHibernateUtil.Int64)
                .AddScalar("Field", NHibernateUtil.String)
                .AddScalar("FieldCode", NHibernateUtil.String)
                .AddScalar("Cluster", NHibernateUtil.String)
                .AddScalar("Well", NHibernateUtil.String)
                .AddScalar("WellID", NHibernateUtil.Int64)
                .AddScalar("Created", NHibernateUtil.DateTime)
                .AddScalar("Expired", NHibernateUtil.DateTime)
                .AddScalar("TicketDetailID", NHibernateUtil.Int64)
                .AddScalar("SL", NHibernateUtil.Int32)
                .AddScalar("AllowUnregister", NHibernateUtil.Boolean)
                .AddScalar("IsNight", NHibernateUtil.Boolean)
                .AddScalar("IsRepeated", NHibernateUtil.Boolean)
                .SetFirstResult(index*size)
                .SetMaxResults(size)
                .SetParameter("membershipID", membership.Id)
                .SetParameter("dateBegin", dateBegin)
                .SetParameter("dateEnd", dateEnd);

            foreach (var parameterKey in parameters.Keys) {
                sqlQuery.SetParameter(parameterKey, parameters[parameterKey]);
            }

            var output = sqlQuery.SetResultTransformer(Transformers.AliasToBean<TicketTableRecord>()).List<TicketTableRecord>();

            return output;
        }

        public IDictionary<string, IEnumerable<FilterOption>> GetOutcomingFilterOptions(string filter, Membership membership, Department department, DateTime dateBegin, DateTime dateEnd) {
            // Инициализация
            var conditions = string.Empty;
            IDictionary<string, object> parameters = new Dictionary<string, object>();

            var ticketFilter = ParseFilter(filter);
            PrepareFilterConditions(ticketFilter, ref conditions, ref parameters);

            var output = new Dictionary<string, IEnumerable<FilterOption>>();
            foreach (var filterName in TicketFilterNames) {
                string select;
                if (!TryGetFilterSelect(filterName, out select)) {
                    continue;
                };

                var queryString = $@"
                    select *
                    from (
                        select distinct {select}
                        from tt_register
                        where (created between :dateBegin and :dateEnd or movement_created between :dateBegin and :dateEnd)
                            and (ticket_id, movement_id) IN (select ticket_id, max(movement_id) from tt_register group by ticket_id)
                            and {conditions} 
                            movement_department_first_id in (select id from tt_departments where id = :departmentID or parent_id = :departmentID)
                    ) order by Code
                ";
                
                var sqlQuery = Session
                    .CreateSQLQuery(queryString)
                    .AddScalar("Code", NHibernateUtil.String)
                    .AddScalar("Value", NHibernateUtil.String)
                    .SetParameter("departmentID", department.Id)
                    .SetParameter("dateBegin", dateBegin)
                    .SetParameter("dateEnd", dateEnd);

                foreach (var parameterKey in parameters.Keys) {
                    sqlQuery.SetParameter(parameterKey, parameters[parameterKey]);
                }

                output.Add(filterName, sqlQuery.SetResultTransformer(Transformers.AliasToBean<FilterOption>()).List<FilterOption>());
            }

            return output;
        }

        bool TryGetFilterSelect(string filterName, out string select) {
            switch (filterName) {
                case "ticket_type_name":
                    select = "ticket_type Code, ticket_type_num || '.' || ticket_type_code Value";
                    return true;
                case "well_name":
                    select = "detail_well_id Code, detail_well Value";
                    return true;
                case "department_name_from":
                    select = "movement_department_first_id Code, movement_first_display_name Value";
                    return true;
                case "department_name_to":
                    select = "department_id Code, department_display_name Value";
                    return true;
                case "status_name":
                    select = "movement_status Code, movement_status_name Value";
                    return true;
                case "field_name":
                    select = "detail_field_code Code, detail_field_name Value";
                    return true;
                case "sl":
                    select = "detail_sl Code, DECODE(detail_sl, 0, 'SL1', 1, 'SL2', 2, 'SL3') Value";
                    return true;
                default:
                    select = string.Empty;
                    return false;
            }
        }

        public IEnumerable<TicketPending> GetTicketPendings() {
            var output = Session.CreateSQLQuery(@"
                    select ticket_id as ""TicketID"", is_night as ""IsNight"", ticket_type as ""TicketTypeString"", sl as ""SL""
                    from (
                        select tm.ticket_id, t.ticket_type, t.is_night, td.sl, tm.status, row_number() over (partition by tm.ticket_id order by tm.id desc) rn
                        from tt_ticket_movements tm
                            join tt_ticket_detail_base td 
                                on td.id = tm.ticket_detail_id
                            join tt_tickets t
                                on t.id = tm.ticket_id
                    ) where rn = 1 and status = 0
                ")
            .AddScalar("TicketID", NHibernateUtil.Int64)
            .AddScalar("IsNight", NHibernateUtil.Boolean)
            .AddScalar("TicketTypeString", NHibernateUtil.String)
            .AddScalar("SL", NHibernateUtil.Int32)
            .SetResultTransformer(Transformers.AliasToBean<TicketPending>())
            .List<TicketPending>();

            return output;
        }

        public IEnumerable<TicketPending> GetFailedTicketPendings() {
            var output = Session.CreateSQLQuery(@"
                    select ticket_id as ""TicketID"", is_night as ""IsNight"", ticket_type as ""TicketTypeString"", sl as ""SL""
                    from (
                        select tm.ticket_id, t.ticket_type, t.is_night, td.sl, tm.status, row_number() over (partition by tm.ticket_id order by tm.id desc) rn
                        from tt_ticket_movements tm
                            join tt_ticket_detail_base td 
                                on td.id = tm.ticket_detail_id
                            join tt_tickets t
                                on t.id = tm.ticket_id
                    ) where rn = 1 and status = 0 and ticket_id in (select ticket_id from tt_ticket_failures)
                ")
            .AddScalar("TicketID", NHibernateUtil.Int64)
            .AddScalar("IsNight", NHibernateUtil.Boolean)
            .AddScalar("TicketTypeString", NHibernateUtil.String)
            .AddScalar("SL", NHibernateUtil.Int32)
            .SetResultTransformer(Transformers.AliasToBean<TicketPending>())
            .List<TicketPending>();

            return output;
        }

        private TicketFilter ParseFilter(string filter) {
            var filterData = Encoding.UTF8.GetString(Convert.FromBase64String(filter));
            return JsonConvert.DeserializeObject<TicketFilter>(filterData);
        }

        private void PrepareFilterConditions(TicketFilter ticketFilter, ref string conditions, ref IDictionary<string, object> parameters) {
            // Фильтрация по тип заявки, приоритет имеет фильтр по наименованию, необходимо для автодополнения
            if (!string.IsNullOrEmpty(ticketFilter.TicketType)) {
                conditions += " ticket_type = :ticket_type and ";
                parameters["ticket_type"] = ticketFilter.TicketType;
            }

            if (ticketFilter.WellID.HasValue) {
                conditions += " detail_well_id = :well and ";
                parameters["well"] = ticketFilter.WellID;
            }

            if (!string.IsNullOrEmpty(ticketFilter.FieldCode)) {
                conditions += " detail_field_code = :field and ";
                parameters["field"] = ticketFilter.FieldCode;
            }

            if (ticketFilter.DepartmentIDFrom.HasValue) {
                conditions += " movement_department_first_id = :department_from and ";
                parameters["department_from"] = ticketFilter.DepartmentIDFrom;
            }

            if (ticketFilter.DepartmentIDTo.HasValue) {
                conditions += " department_id = :department_to and ";
                parameters["department_to"] = ticketFilter.DepartmentIDTo;
            }

            if (ticketFilter.TicketStatus.HasValue) {
                conditions += " movement_status = :status and ";
                parameters["status"] = ticketFilter.TicketStatus;
            }

            if (ticketFilter.SL.HasValue) {
                conditions += " detail_sl = :sl and ";
                parameters["sl"] = ticketFilter.SL.Value;
            }

            if (ticketFilter.Created.HasValue) {
                conditions += " created = :created and ";
                parameters["created"] = ticketFilter.Created.Value.Date;
            }

            if (ticketFilter.Completed.HasValue) {
                conditions += " movement_created = :completed and ";
                parameters["completed"] = ticketFilter.Completed.Value.Date;
            }
        }

        public long GetTicketIDSequenceNext() {
            return Session.GetSequenceNextValue("TT_TICKET_SEQUENCE");
        }

        public IEnumerable<Ticket> FindByGroupID(long ticketGroupID) {
            return Session
                .CreateCriteria<Ticket>()
                .Add(Restrictions.Eq("GroupID", ticketGroupID))
                .SetResultTransformer(Transformers.DistinctRootEntity)
                .List<Ticket>();
        }
    }
}