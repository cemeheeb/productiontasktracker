﻿using System.Collections.Generic;
using NHibernate.Criterion;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using System;
    using DataAccess.Repositories;
    using Domain.Entities;

    public class MembershipRepository : Repository<Membership>, IMembershipRepository {
        public MembershipRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public Membership FindByUserName(string userName) {
            return Session.CreateCriteria<Membership>()
                .Add(Restrictions.Eq("UserName", userName))
                .UniqueResult<Membership>();
        }

        public IEnumerable<Membership> GetAll() {
            return Session.CreateCriteria<Membership>()
                .Add(Restrictions.Eq("Deleted", false))
                .List<Membership>();
        }

        public Membership GetWinServiceUser() {
            return Session.CreateCriteria<Membership>()
                .Add(Restrictions.Eq("UserName", "WinService"))
                .UniqueResult<Membership>();
        }

        public IEnumerable<Membership> Search(int index, int size, out int sizeTotal) {
            sizeTotal = Session.CreateCriteria<Membership>()
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return Session.CreateCriteria<Membership>()
                .AddOrder(Order.Asc("LastName"))
                .SetFirstResult((index - 1) * size)
                .SetMaxResults(size)
                .List<Membership>();
        }
    }
}
