﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;

using ILoggerFactory = Ninject.Extensions.Logging.ILoggerFactory;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.DTO;
    using Domain.Entities;

    public class TicketMovementRepository: Repository<TicketMovement>, ITicketMovementRepository {
        #region Ninject

        private readonly ILogger _logger;
        
        #endregion

        public TicketMovementRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
            _logger = loggerFactory.GetLogger("trace");
        }

        public IEnumerable<TicketMovementNotification> GetNotifications(Membership membership, Department department) {
            var queryString = @"
                select 
                    t.id as ""TicketID"",
                    data_last.id as ""TicketMovementID"",
                    data_last.ticket_detail_id as ""TicketDetailID"",
                    t.ticket_type as ""TicketType"",
                    data_last.status as ""Status"",
                    data_last.created as ""Created""
                from tt_tickets t
                    join (
                        select
                            id, 
                            ticket_id,
                            max(ticket_detail_id) over (partition by ticket_id) ticket_detail_id,
                            created,
                            status,
                            row_number() over (partition by ticket_id order by id desc) rn
                        from tt_ticket_movements
                        where deleted = 0
                    ) data_last on t.id = data_last.ticket_id and rn = 1
                    left join (
                        select
                            max(id) id,
                            ticket_movement_id,
                            membership_id
                        from tt_ticket_visits
                        where membership_id = :membershipID
                        group by ticket_movement_id, membership_id
                    ) data_visit on data_visit.ticket_movement_id = data_last.id
                where data_visit.id is null and t.department_id=:departmentID and data_last.status = 1
                order by t.id desc
            ";

            var output = Session
                .CreateSQLQuery(queryString)
                .AddScalar("TicketID", NHibernateUtil.Int64)
                .AddScalar("TicketMovementID", NHibernateUtil.Int64)
                .AddScalar("TicketDetailID", NHibernateUtil.Int64)
                .AddScalar("TicketType", NHibernateUtil.String)
                .AddScalar("Status", NHibernateUtil.Int32)
                .AddScalar("Created", NHibernateUtil.DateTime)
                .SetParameter("membershipID", membership.Id)
                .SetParameter("departmentID", department.Id)
                .SetResultTransformer(Transformers.AliasToBean<TicketMovementNotification>()).List<TicketMovementNotification>();

            return output;
        }

        public TicketMovement GetLatest(Ticket ticket) {
            var output = Session.CreateCriteria<TicketMovement>("movements")
            .Add(Restrictions.Eq("movements.Ticket", ticket))
            .AddOrder(Order.Desc("movements.Id"))
            .SetMaxResults(1)
            .UniqueResult() as TicketMovement;

            return output;
        }
    }
}
