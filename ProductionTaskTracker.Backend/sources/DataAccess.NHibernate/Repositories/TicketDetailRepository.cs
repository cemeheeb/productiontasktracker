﻿using System.Collections.Generic;
using NHibernate.Criterion;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using Domain.Entities.TicketDetails;
    using DataAccess.Repositories;

    public class TicketDetailRepository: Repository<TicketDetailBase>, ITicketDetailRepository {
        public TicketDetailRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<TicketDetailBase> FindIn(long[] ticketDetailIDList) {
            return Session
                .CreateCriteria<TicketDetailBase>()
                .Add(Restrictions.In("Id", ticketDetailIDList))
                .List<TicketDetailBase>();
        }

    }
}
