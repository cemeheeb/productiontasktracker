﻿using System.Collections.Generic;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class MeasureFacilityRepository : Repository<MeasureFacility>, IMeasureFacilityRepository {
        public MeasureFacilityRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {

        }
        
        public IEnumerable<int> GetPositions(string[] shopCodes, string fieldCode, string cluster) {
            return Session
                .CreateCriteria<MeasureFacility>()
                .Add(Restrictions.In("Shop.Code", shopCodes))
                .Add(Restrictions.Eq("Field.Code", fieldCode))
                .Add(Restrictions.Eq("Cluster", cluster))
                .SetProjection(Projections.Property("Position"))
                .List<int>();
        }

        public Shop GetShopByMeasureFacilityPosition(string fieldCode, string cluster, int measureFacilityPosition) {
            return Session
                .CreateCriteria<MeasureFacility>()
                .Add(Restrictions.Eq("Field.Code", fieldCode))
                .Add(Restrictions.Eq("Cluster", cluster))
                .Add(Restrictions.Eq("Position", measureFacilityPosition))
                .SetProjection(Projections.Property("Shop"))
                .UniqueResult<Shop>();
        }
    }
}
