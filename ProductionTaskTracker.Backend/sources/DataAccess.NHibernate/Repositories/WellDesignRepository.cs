﻿using System.Collections.Generic;
using NHibernate.Criterion;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class WellDesignRepository: Repository<WellDesign>, IWellDesignRepository {
        public WellDesignRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<WellDesign> GetByCode(string code, int index, int size) {
            return Session.CreateCriteria<WellDesign>("wells")
                .Add(Restrictions.Like("wells.Code", code, MatchMode.Start))
                .SetFirstResult(index * size)
                .SetMaxResults(size)
                .List<WellDesign>();
        }
    }
}
