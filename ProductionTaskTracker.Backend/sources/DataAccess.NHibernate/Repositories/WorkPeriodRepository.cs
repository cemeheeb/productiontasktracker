﻿using System.Collections.Generic;
using NHibernate.Criterion;
using NHibernate.Util;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using System;
    using System.Linq;
    using DataAccess.Repositories;
    using Domain.Entities;

    public class WorkPeriodRepository: Repository<WorkPeriod>, IWorkPeriodRepository {
        public WorkPeriodRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }


        public IEnumerable<WorkPeriod> GetByMembership(Membership membership) {
            return Session
                .CreateCriteria<WorkPeriod>()
                .Add(Restrictions.Eq("Membership", membership))
                .AddOrder(Order.Asc("id"))
                .List<WorkPeriod>();
        }

        public WorkPeriod GetByMembershipActive(Membership membership) {
            return GetByMembership(membership).FirstOrDefault(x => x.DateBegin <= DateTime.Now && (!x.DateEnd.HasValue || x.DateEnd >= DateTime.Now));
        }

        public IEnumerable<WorkPeriod> GetActive() {
            return Session
                .CreateCriteria<WorkPeriod>()
                .Add(Restrictions.IsNull("DateEnd"))
                .AddOrder(Order.Asc("Id"))
                .List<WorkPeriod>();
        }
    }
}
