﻿using System.Collections.Generic;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public class TicketMovementVisitRepository: Repository<TicketVisit>, ITicketVisitRepository {
        public TicketMovementVisitRepository(ILoggerFactory loggerFactory, ISessionProvider sessionProvider) : base(loggerFactory, sessionProvider) {
        }

        public IEnumerable<TicketVisit> GetByTicketDetailID(long ticketDetailID) {
            return Session
                .CreateCriteria<TicketVisit>("tv")
                .SetProjection(Projections.ProjectionList()
                    .Add(Projections.Alias(Projections.GroupProperty("tv.TicketDetail"), "TicketDetail"))
                    .Add(Projections.Alias(Projections.GroupProperty("tv.TicketMovement"), "TicketMovement"))
                    .Add(Projections.Alias(Projections.GroupProperty("tv.Membership"), "Membership"))
                    .Add(Projections.Alias(Projections.GroupProperty("tv.Position"), "Position"))
                    .Add(Projections.Alias(Projections.Min("Created"), "Created"))
                )
                .Add(Restrictions.Eq("tv.IsViewed", true))
                .Add(Restrictions.Eq("tv.TicketDetail.Id", ticketDetailID))
                .SetResultTransformer(Transformers.AliasToBean(typeof(TicketVisit)))
                .AddOrder(Order.Asc("Created"))
                .List<TicketVisit>();
        }
    }
}
