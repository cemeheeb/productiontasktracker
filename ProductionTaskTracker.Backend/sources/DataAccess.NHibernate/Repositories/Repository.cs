﻿using System;
using System.Collections.Generic;
using System.Linq;

using NHibernate;
using NHibernate.Criterion;

using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Repositories {
    using DataAccess.Repositories;
    using Domain.Entities;

    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity {
        #region Fields

        private readonly ILogger _logger;
        private readonly ISessionProvider _sessionProvider;

        #endregion

        protected Repository(Ninject.Extensions.Logging.ILoggerFactory loggerFactory, ISessionProvider sessionProvider) {
            _logger = loggerFactory.GetLogger("trace");
            _sessionProvider = sessionProvider;
        }

        protected ISession Session => _sessionProvider.Session;

        public IEnumerable<TEntity> GetAll() {
            try {
                return Session.CreateCriteria<TEntity>().List<TEntity>();
            } catch (Exception exception) {
                _logger.ErrorException($"Не удалось получить данные", exception);
            }

            return null;
        }

        public TEntity Find(long id) {
            try {
                return Session.Get<TEntity>(id);
            } catch(Exception exception) { 
                _logger.ErrorException($"Не удалось найти {typeof(TEntity)}. #{id}", exception);
            }

            return null;
        }

        public IEnumerable<TEntity> Find(long[] ids) {
            try {
                var output = Session
                    .CreateCriteria(typeof(TEntity))
                    .Add(Restrictions.In("Id", ids))
                    .List<TEntity>();

                output = ids.Join(output, id => id, x => x.Id, (i, r) => r).ToList();
                return output.Distinct();
            } catch (Exception exception) {
                _logger.ErrorException($"Ошибка при получении списка {typeof(TEntity)}. #{ids}", exception);
            }

            return null;
        }

        public TEntity Create(TEntity entity) {
            var output = Session.Save(entity);
            Session.Flush();
            return Session.Get(typeof(TEntity), output) as TEntity;
        }

        public void Update(TEntity entity) {
            Session.Update(entity);
        }

        public void Delete(long id) {
            Delete(Session.Load<TEntity>(id));
        }

        public void Delete(TEntity entity) {
            entity.Deleted = true;
            Update(entity);
        }

    }
}
