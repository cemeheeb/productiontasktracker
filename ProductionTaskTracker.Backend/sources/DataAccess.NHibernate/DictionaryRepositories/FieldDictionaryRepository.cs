﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.DictionaryRepositories {
    using Domain.Entities;
    using DataAccess.Repositories;

    public class FieldDictionaryRepository : DictionaryRepository<Field>, IFieldDictionaryRepository {
        public FieldDictionaryRepository(ISessionProvider sessionProvider) : base(sessionProvider) {
        }

        public IEnumerable<Field> Search(string shop, string search, int index, int size, out int sizeTotal) {
            var output = Session.CreateCriteria<Field>("fields")
                .Add(Restrictions.Eq("fields.Shop", shop))
                .Add(Restrictions.Like("fields.Name", search, MatchMode.Anywhere))
                .SetFirstResult(index * size)
                .SetMaxResults(size)
                .List<Field>();

            sizeTotal = output.Count;
            return output;
        }

        public IEnumerable<Field> GetByShopCodes(string[] shopCodes) {
            return Session.CreateCriteria<WellRegular>("wells")
                .CreateCriteria("wells.Shop", "Shop", JoinType.InnerJoin)
                .CreateCriteria("wells.Field", "Field", JoinType.InnerJoin)
                .Add(Restrictions.In("wells.Shop.Code", shopCodes.ToList()))
                .SetProjection(
                    Projections.Distinct(
                        Projections.ProjectionList()
                            .Add(Projections.Alias(Projections.Property("Field.Code"), "Code"))
                            .Add(Projections.Alias(Projections.Property("Field.Name"), "Name"))
                    )
                )
                .SetResultTransformer(Transformers.AliasToBean<Field>())
                .List<Field>();
        }

        public IEnumerable<Field> GetMeasurePositionFields(IEnumerable<string> shopCodes) {
            var command = @"
                select distinct f.code as Code, f.name as Name
                from (select distinct shop_code, field_code from tt_wells) l
                    join tt_fields f 
                        on f.code = l.field_code
                    join tt_measure_facilities mf
                        on mf.shop_code = l.shop_code and mf.field_code = f.code
                where l.shop_code in (:shops)
                order by f.name
            ";

            var output = Session
                .CreateSQLQuery(command)
                .AddScalar("Code", NHibernateUtil.String)
                .AddScalar("Name", NHibernateUtil.String)
                .SetResultTransformer(Transformers.AliasToBean<Field>())
                .SetParameterList("shops", shopCodes)
                .List<Field>();

            return output;
        }
    }
}
