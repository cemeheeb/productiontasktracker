﻿namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.DictionaryRepositories {
    using DataAccess.DictionaryRepositories;
    using Domain.Entities;

    public class ShopDictionaryRepository: DictionaryRepository<Shop>, IShopDictionaryRepository {
        public ShopDictionaryRepository(ISessionProvider sessionProvider) : base(sessionProvider) {
        }
    }
}
