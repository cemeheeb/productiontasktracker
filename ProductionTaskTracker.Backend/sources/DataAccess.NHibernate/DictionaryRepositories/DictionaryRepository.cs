﻿using System.Collections.Generic;
using NHibernate;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.DictionaryRepositories {
    using DataAccess.DictionaryRepositories;
    using Domain.Entities;

    public class DictionaryRepository<TEntity> : IDictionaryRepository<TEntity> where TEntity : DictionaryEntity {
        #region Fields

        private readonly ISessionProvider _sessionProvider;
        
        #endregion

        public DictionaryRepository(ISessionProvider sessionProvider) {
            _sessionProvider = sessionProvider;
        }

        protected ISession Session => _sessionProvider.Session;

        public IEnumerable<TEntity> GetAll() {
            return Session.CreateCriteria<TEntity>().List<TEntity>();
        }

        public TEntity Find(string code) {
            return Session.Get<TEntity>(code);
        }
    }
}
