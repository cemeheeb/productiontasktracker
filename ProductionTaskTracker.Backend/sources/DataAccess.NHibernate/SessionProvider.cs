﻿using NHibernate;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    using NHibernate;
    public class SessionProvider : ISessionProvider {
        #region ISessionProvider Members

        public ISession Session => NHibernateHelper.GetSession();

        #endregion
    }
}