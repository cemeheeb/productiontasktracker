﻿using System;
using System.Data;

using NHibernate;
using NHibernate.Context;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    public class UnitOfWork: IUnitOfWork {
        #region Fields

        private readonly ISession _session;
        private ITransaction _transaction;

        #endregion

        public UnitOfWork(ISession session, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted) {
            if (session == null) {
                throw new ArgumentNullException(nameof(session));
            }

            _session = session;
            _transaction = _session.BeginTransaction(isolationLevel);

            CurrentSessionContext.Bind(_session);
        }

        #region IUnitOfWork Members

        public void Dispose() {
            if (!_transaction.WasCommitted && !_transaction.WasRolledBack) {
                _transaction.Rollback();
            }

            _transaction.Dispose();
            _transaction = null;

            CurrentSessionContext.Unbind(_session.SessionFactory);
            _session.Dispose();
        }

        public void Commit() {
            _transaction.Commit();
        }

        #endregion
    }
}
