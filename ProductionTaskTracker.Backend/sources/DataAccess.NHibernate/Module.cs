﻿
using Ninject.Modules;
using Ninject.Web.Common;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    using DataAccess.Repositories;
    using DataAccess.DictionaryRepositories;
    using DataAccess.Services;

    using Repositories;
    using DictionaryRepositories;
    using Services;

    public class Module : NinjectModule {
        public override void Load() {
            // Repositories
            Bind<IAutocompleteOptionRepository>().To<AutocompleteOptionRepository>().InRequestScope();
            Bind<IDepartmentRepository>().To<DepartmentRepository>().InRequestScope();
            Bind<IEnterpriseRepository>().To<EnterpriseRepository>().InRequestScope();
            Bind<IControlledWellRepository>().To<ControlledWellRepository>().InRequestScope();
            Bind<IMeasureFacilityRepository>().To<MeasureFacilityRepository>().InRequestScope();
            Bind<IMembershipRepository>().To<MembershipRepository>().InRequestScope();
            Bind<ITicketRepository>().To<TicketRepository>().InRequestScope();
            Bind<ITicketVisitRepository>().To<TicketMovementVisitRepository>().InRequestScope();
            Bind<ITicketDetailRepository>().To<TicketDetailRepository>().InRequestScope();
            Bind<ITicketMovementRepository>().To<TicketMovementRepository>().InRequestScope();
            Bind<ITicketRouteRepository>().To<TicketRouteRepository>().InRequestScope();
            Bind<IPositionRepository>().To<PositionRepository>().InRequestScope();
            Bind<IPumpRepository>().To<PumpRepository>().InRequestScope();
            Bind<IClassifierRepository>().To<ClassifierRepository>().InRequestScope();
            Bind<IWellRegularRepository>().To<WellRegularRepository>().InRequestScope();
            Bind<IWellDesignRepository>().To<WellDesignRepository>().InRequestScope();
            Bind<IWorkPeriodRepository>().To<WorkPeriodRepository>().InRequestScope();

            // DictionaryRepositories
            Bind<IShopDictionaryRepository>().To<ShopDictionaryRepository>().InRequestScope();
            Bind<IFieldDictionaryRepository>().To<FieldDictionaryRepository>().InRequestScope();

            // Services
            Bind<IReportDataService>().To<ReportDataService>().InRequestScope();
            Bind<IRegisterService>().To<RegisterService>().InRequestScope();
        }
    }
}
