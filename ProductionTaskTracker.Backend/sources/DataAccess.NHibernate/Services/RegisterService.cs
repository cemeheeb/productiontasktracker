﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Transform;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Services {
    using System.Data;
    using DataAccess.Services;
    using Domain.DTO;

    public class RegisterService: IRegisterService {

        #region Fields

        private readonly ISessionProvider _sessionProvider;

        #endregion

        public RegisterService(ISessionProvider sessionProvider) {
            _sessionProvider = sessionProvider;
        }

        public void PendingInclude(long id) {
            var command = _sessionProvider.Session.Connection.CreateCommand();
            _sessionProvider.Session.Transaction.Enlist(command);

            var parameter = command.CreateParameter();
            parameter.ParameterName = "ticketMovementID";
            parameter.DbType = DbType.Int64;
            parameter.Value = id;
            command.Parameters.Add(parameter);

            command.CommandText = "insert into tt_register_pending (ticket_movement_id) values (:ticketMovementID)";
            command.ExecuteNonQuery();
        }

        public void CalculateRegister() {
            _sessionProvider.Session.Flush();

            var command = _sessionProvider.Session.Connection.CreateCommand();
            _sessionProvider.Session.Transaction.Enlist(command);
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "tt_register_pkg.calculate_register";
            command.ExecuteNonQuery();
        }

        public IEnumerable<RegisterRecord> GetByWellCode(long wellID, DateTime dateBegin, DateTime dateEnd, int index, int size, out int sizeTotal) {
            var sqlQuery = _sessionProvider.Session.CreateSQLQuery(@"
                select count(*) ""TotalSize""
                    from tt_register r
                    left join tt_classifiers c1 on r.puller_type_id = c1.id and c1.kind ='PT_'
                    left join tt_classifiers c2 on r.repair_type_id = c1.id and c1.kind like'PR%'
                    left join tt_pumps p on r.pump_id = p.id                        
                where (ticket_id, movement_id) IN (SELECT ticket_id, max(movement_id) FROM tt_register r group by ticket_id) 
                  and detail_well_id = :wellID
                  and r.created between :dateBegin and :dateEnd"
            );

            sizeTotal = sqlQuery
                .AddScalar("TotalSize", NHibernateUtil.Int32)
                .SetParameter("wellID", wellID)
                .SetParameter("dateBegin", dateBegin)
                .SetParameter("dateEnd", dateEnd)
                .UniqueResult<int>();

            sqlQuery = _sessionProvider.Session.CreateSQLQuery(@"
                    select 
                        AGZU Agzu,
                        BALANCE_DELTA BalanceDelta,
                        BG Bg,
                        CARBONATE Carbonate,
                        CRIMPING Crimping,
                        DMG Dmg,
                        DMG_BEFORE DMGBefore,
                        INHIBITOR Inhibitor,
                        KTPN Ktpn,
                        KVCH Kvch,
                        LIQUID_PROBE LiquidProbe,
                        LOCK_AVAILABLE LockAvailable,
                        MIXTURE Mixture,
                        MULTI_COMPONENT MultiComponent,
                        NEED_CRIMPING NeedCrimping,
                        PILE_FIELD_DEMONTAGE PileFieldDemontage,
                        POWER_OFF PowerOff,
                        PREPARING Preparing,
                        PRESSURE_AFTER_DAMPING PressureAfterDamping,
                        PUMP_CRIMPING PumpCrimping,
                        PURGE Purge,
                        REQUESTED_CNIPR RequestedCNIPR,
                        REQUESTED_EPU RequestedEPU,
                        SERVICE_AREA ServiceArea,
                        SK Sk,
                        UA Ua,
                        WATERING Watering,
                        WORKED Worked,

                        AMPERAGE Amperage,
                        AMPERAGE_15 Amperage15,
                        AMPERAGE_30 Amperage30,
                        AMPERAGE_BEFORE AmperageBefore,
                        AMPERAGE_AFTER AmperageAfter,
                        AMPERAGE_REVERSE AmperageReverse,
                        CRIMPING_BEGIN CrimpingBegin,
                        CRIMPING_DURATION CrimpingDuration,
                        CRIMPING_END CrimpingEnd,
                        DAMPING_VOLUME DampingVolume,
                        DAMPING_VOLUME_CYCLE_1 DampingVolumeCycleA,
                        DAMPING_VOLUME_CYCLE_2 DampingVolumeCycleB,
                        DAMPING_VOLUME_CYCLE_3 DampingVolumeCycleC,
                        DAMPING_VOLUME_CYCLE_4 DampingVolumeCycleD,
                        WEIGHT_CYCLE_1 DampingWeightCycleA,
                        WEIGHT_CYCLE_2 DampingWeightCycleB,
                        WEIGHT_CYCLE_3 DampingWeightCycleC,
                        WEIGHT_CYCLE_4 DampingWeightCycleD,
                        DEBIT Debit,
                        DURATION Duration,
                        H_DYNAMIC HDynamic,
                        H_DYNAMIC_15 HDynamic15,
                        H_DYNAMIC_30 HDynamic30,
                        H_DYNAMIC_MINIMAL HDynamicMinimal,
                        H_DYNAMIC_PRESSURE_AFTER_WASH HDynamicPressureAfterWash,
                        H_DYNAMIC_PRESSURE_DURATION HDynamicPressureDuration,
                        H_STATIC HStatic,
                        H_STATIC_AFTER_CRIMPING HStaticAfterCrimping,
                        H_STATIC_AFTER_STOP HStaticAfterStop,
                        LOADING Loading,
                        LOADING_15 Loading15,
                        LOADING_30 Loading30,
                        LOADING_BEFORE LoadingBefore,
                        LOADING_REVERSE LoadingReverse,
                        OPERATION_MODE OperationMode,
                        PRESSURE Pressure,
                        PRESSURE_15 Pressure15,
                        PRESSURE_30 Pressure30,
                        PRESSURE_ADPM_BEGIN PressureADPMBegin,
                        PRESSURE_ADPM_END PressureADPMEnd,
                        PRESSURE_AFTER PressureAfter,
                        PRESSURE_BUFFER PressureBuffer,
                        PRESSURE_CRIMPING PressureCrimping,
                        PRESSURE_ENGINE PressureEngine,
                        PRESSURE_ENGINE_15 PressureEngine15,
                        PRESSURE_ENGINE_30 PressureEngine30,
                        PRESSURE_ENGINE_BEFORE PressureEngineBefore,
                        PRESSURE_ENGINE_AFTER PressureEngineAfter,
                        PRESSURE_ENVIRONMENT PressureEnvironment,
                        PRESSURE_ENVIRONMENT_15 PressureEnvironment15,
                        PRESSURE_ENVIRONMENT_30 PressureEnvironment30,
                        PRESSURE_ENVIRONMENT_BEFORE PressureEnvironmentBefore,
                        PRESSURE_ENVIRONMENT_AFTER PressureEnvironmentAfter,
                        PROBE_VOLUME ProbeVolume,
                        PROTECTION_CORRECTION ProtectionCorrection,
                        PUMP_DEPTH PumpDepth,
                        Q Q,
                        Q_15 Q15,
                        Q_30 Q30,
                        QJ Qj,
                        QJ_LOCKED QjLocked,
                        QJ_MANUAL QjManual,
                        QJ_MANUAL_BEFORE QjManualBefore,
                        QJ_MANUAL_AFTER QjManualAfter,
                        QJ_REDIRECT QjRedirect,
                        QJ_REVERSE QjReverse,
                        ROCK_FREQUENCY RockFrequency,
                        ROTATION_FREQUENCY RotationFrequency,
                        ROTATION_FREQUENCY_15 RotationFrequency15,
                        ROTATION_FREQUENCY_30 RotationFrequency30,
                        ROTATION_FREQUENCY_DELTA RotationFrequencyDelta,
                        TEMPERATURE_ENGINE TemperatureEngine,
                        TEMPERATURE_ENGINE_15 TemperatureEngine15,
                        TEMPERATURE_ENGINE_30 TemperatureEngine30,
                        TEMPERATURE_ENGINE_BEFORE TemperatureEngineBefore,
                        TEMPERATURE_ENGINE_AFTER TemperatureEngineAfter,
                        TEMPERATURE_ENVIRONMENT TemperatureEnvironment,
                        TEMPERATURE_ENVIRONMENT_15 TemperatureEnvironment15,
                        TEMPERATURE_ENVIRONMENT_30 TemperatureEnvironment30,
                        TEMPERATURE_ENVIRONMENT_BEFORE TemperatureEnvironmentBefore,
                        TEMPERATURE_ENVIRONMENT_AFTER TemperatureEnvironmentAfter,
                        TEMPERATURE_INTERVAL TemperatureInterval,
                        VOLTAGE Voltage,
                        VOLTAGE_AFTER VoltageAfter,
                        VOLUME Volume,
                        VOLUME_BEFORE_COLLECTOR VolumeBeforeCollector,
                        VOLUME_BEFORE_LOSS VolumeBeforeLoss,
                        WASH_PRESSURE_BEGIN WashPressureBegin,
                        WASH_PRESSURE_END WashPressureEnd,
                        WASH_VOLUME WashVolume,
                        WEIGHT Weight,
                        WEIGHT DampingWeight,
                        WHEELING Wheeling,

                        BRIGADE Brigade,
                        CYCLES Cycles,
                        MF_POSITION MeasureFacilityPosition,
                        TICKET_TYPE_NUM TicketTypeNum,
                        TICKET_TYPE_CODE TicketTypeCode,

                        TICKET_ID TicketID,
                        DEPARTMENT_ID DepartmentID,

                        CREATED Created,
                        ACCEPTED Accepted,
                        COMPLETED Completed,
                        DECODE (movement_status, 9, movement_created, null) Execution,
                        OBTAIN_TIME ObtainTime,
                        PADDING_TIME PaddingTime,
                        PROBE_TIME ProbeTime,
                        START_TIME StartTime,
                        TREATMENT_TIME TreatmentTime,
                        START_TIME_PLAN StartTimePlan,
                        TIME_COMPLETION TimeCompletion,
                        TREATMENT_DATEBEGIN TreatmentDateBegin,
                        TREATMENT_DATEEND TreatmentDateEnd,
                        e.display_name Contractor,
                        BRIGADE_TYPE BrigadeType,

                        CALLED_AGENTS CalledAgents,
                        DEPARTMENT_DISPLAY_NAME DepartmentName,
                        DETAIL_CLUSTER_CODE DetailClusterCode,
                        DETAIL_FIELD_NAME DetailFieldName,
                        DETAIL_NOTE DetailNote,
                        DETAIL_REQUESTED DetailRequested,
                        DECODE ( detail_sl, NULL, ' ', 'SL' || TO_CHAR( detail_sl+1) ) DetailSL,
                        DETAIL_WELL DetailWellName,
                        DMG_TYPE DMGType,
                        EQUIPMENT_VERIFICATION EquipmentVerification,
                        EVENT_TYPE EventType,
                        DECODE (movement_status, 9, movement_membership_name, null)  ExecutionMembershipName,
                        MOVEMENT_MEMBERSHIP_FIRST_NAME MembershipCreatedName,
                        MOVEMENT_STATUS MovementStatus,
                        PHASE_CHECK PhaseCheck,
                        PULLER Puller,
                        c1.name PullerTypeName,
                        p.title PumpName,
                        REASON Reason,
                        REPAIR_TYPE RepairType,
                        C2.NAME RepairTypeName,
                        REQUEST_REASON RequestReason,
                        REQUEST_TYPE RequestType,
                        ROTATION_CHANGE RotationChange,
                        STOP_REASON StopReason,
                        TICKET_TYPE TicketType,
                        TREATMENT Treatment,
                        UNION_ACTION_TYPE UnionAction,
                        VALVE_VERIFICATION ValveVerification,
                        WELL_AREA WellArea
                    from tt_register r
                        left join tt_classifiers c1 on r.puller_type_id = c1.id and c1.kind ='PT_'
                        left join tt_classifiers c2 on r.repair_type_id = c1.id and c1.kind like'PR%'
                        left join tt_pumps p on r.pump_id = p.id
                        left join tt_enterprises e on r.contractor_id = e.id
                    where created between :dateBegin and :dateEnd
                        and (ticket_id, movement_id) IN (SELECT ticket_id, max(movement_id) FROM tt_register r group by ticket_id)
                        and detail_well_id = :wellID
                    order by  TICKET_ID DESC
                ")
                .AddScalar("Agzu", NHibernateUtil.Boolean)
                .AddScalar("BalanceDelta", NHibernateUtil.Boolean)
                .AddScalar("Bg", NHibernateUtil.Boolean)
                .AddScalar("Carbonate", NHibernateUtil.Boolean)
                .AddScalar("Crimping", NHibernateUtil.Boolean)
                .AddScalar("Dmg", NHibernateUtil.Boolean)
                .AddScalar("DMGBefore", NHibernateUtil.Boolean)
                .AddScalar("Inhibitor", NHibernateUtil.Boolean)
                .AddScalar("Ktpn", NHibernateUtil.Boolean)
                .AddScalar("Kvch", NHibernateUtil.Boolean)
                .AddScalar("LiquidProbe", NHibernateUtil.Boolean)
                .AddScalar("LockAvailable", NHibernateUtil.Boolean)
                .AddScalar("Mixture", NHibernateUtil.Boolean)
                .AddScalar("MultiComponent", NHibernateUtil.Boolean)
                .AddScalar("NeedCrimping", NHibernateUtil.Boolean)
                .AddScalar("PileFieldDemontage", NHibernateUtil.Boolean)
                .AddScalar("PowerOff", NHibernateUtil.Boolean)
                .AddScalar("Preparing", NHibernateUtil.Boolean)
                .AddScalar("PressureAfterDamping", NHibernateUtil.Boolean)
                .AddScalar("PumpCrimping", NHibernateUtil.Boolean)
                .AddScalar("Purge", NHibernateUtil.Boolean)
                .AddScalar("RequestedCNIPR", NHibernateUtil.Boolean)
                .AddScalar("RequestedEPU", NHibernateUtil.Boolean)
                .AddScalar("ServiceArea", NHibernateUtil.Boolean)
                .AddScalar("Sk", NHibernateUtil.Boolean)
                .AddScalar("Ua", NHibernateUtil.Boolean)
                .AddScalar("Watering", NHibernateUtil.Boolean)
                .AddScalar("Worked", NHibernateUtil.Boolean)

                .AddScalar("Amperage", NHibernateUtil.Decimal)
                .AddScalar("Amperage15", NHibernateUtil.Decimal)
                .AddScalar("Amperage30", NHibernateUtil.Decimal)
                .AddScalar("AmperageBefore", NHibernateUtil.Decimal)
                .AddScalar("AmperageAfter", NHibernateUtil.Decimal)
                .AddScalar("AmperageReverse", NHibernateUtil.Decimal)
                .AddScalar("CrimpingBegin", NHibernateUtil.Decimal)
                .AddScalar("CrimpingDuration", NHibernateUtil.Decimal)
                .AddScalar("CrimpingEnd", NHibernateUtil.Decimal)
                .AddScalar("DampingVolume", NHibernateUtil.Decimal)
                .AddScalar("DampingVolumeCycleA", NHibernateUtil.Decimal)
                .AddScalar("DampingVolumeCycleB", NHibernateUtil.Decimal)
                .AddScalar("DampingVolumeCycleC", NHibernateUtil.Decimal)
                .AddScalar("DampingVolumeCycleD", NHibernateUtil.Decimal)
                .AddScalar("DampingWeight", NHibernateUtil.Decimal)
                .AddScalar("DampingWeightCycleA", NHibernateUtil.Decimal)
                .AddScalar("DampingWeightCycleB", NHibernateUtil.Decimal)
                .AddScalar("DampingWeightCycleC", NHibernateUtil.Decimal)
                .AddScalar("DampingWeightCycleD", NHibernateUtil.Decimal)
                .AddScalar("Debit", NHibernateUtil.Decimal)
                .AddScalar("Duration", NHibernateUtil.Decimal)
                .AddScalar("HDynamic", NHibernateUtil.Decimal)
                .AddScalar("HDynamic15", NHibernateUtil.Decimal)
                .AddScalar("HDynamic30", NHibernateUtil.Decimal)
                .AddScalar("HDynamicMinimal", NHibernateUtil.Decimal)
                .AddScalar("HDynamicPressureAfterWash", NHibernateUtil.Decimal)
                .AddScalar("HDynamicPressureDuration", NHibernateUtil.Decimal)
                .AddScalar("HStatic", NHibernateUtil.Decimal)
                .AddScalar("HStaticAfterCrimping", NHibernateUtil.Decimal)
                .AddScalar("HStaticAfterStop", NHibernateUtil.Decimal)
                .AddScalar("Loading", NHibernateUtil.Decimal)
                .AddScalar("Loading15", NHibernateUtil.Decimal)
                .AddScalar("Loading30", NHibernateUtil.Decimal)
                .AddScalar("LoadingBefore", NHibernateUtil.Decimal)
                .AddScalar("LoadingReverse", NHibernateUtil.Decimal)
                .AddScalar("OperationMode", NHibernateUtil.Decimal)
                .AddScalar("Pressure", NHibernateUtil.Decimal)
                .AddScalar("Pressure15", NHibernateUtil.Decimal)
                .AddScalar("Pressure30", NHibernateUtil.Decimal)
                .AddScalar("PressureADPMBegin", NHibernateUtil.Decimal)
                .AddScalar("PressureADPMEnd", NHibernateUtil.Decimal)
                .AddScalar("PressureAfter", NHibernateUtil.Decimal)
                .AddScalar("PressureBuffer", NHibernateUtil.Decimal)
                .AddScalar("PressureCrimping", NHibernateUtil.Decimal)
                .AddScalar("PressureEngine", NHibernateUtil.Decimal)
                .AddScalar("PressureEngine15", NHibernateUtil.Decimal)
                .AddScalar("PressureEngine30", NHibernateUtil.Decimal)
                .AddScalar("PressureEngineBefore", NHibernateUtil.Decimal)
                .AddScalar("PressureEngineAfter", NHibernateUtil.Decimal)
                .AddScalar("PressureEnvironment", NHibernateUtil.Decimal)
                .AddScalar("PressureEnvironment15", NHibernateUtil.Decimal)
                .AddScalar("PressureEnvironment30", NHibernateUtil.Decimal)
                .AddScalar("PressureEnvironmentBefore", NHibernateUtil.Decimal)
                .AddScalar("PressureEnvironmentAfter", NHibernateUtil.Decimal)
                .AddScalar("ProbeVolume", NHibernateUtil.Decimal)
                .AddScalar("ProtectionCorrection", NHibernateUtil.Decimal)
                .AddScalar("PumpDepth", NHibernateUtil.Decimal)
                .AddScalar("Q", NHibernateUtil.Decimal)
                .AddScalar("Q15", NHibernateUtil.Decimal)
                .AddScalar("Q30", NHibernateUtil.Decimal)
                .AddScalar("Qj", NHibernateUtil.Decimal)
                .AddScalar("QjLocked", NHibernateUtil.Decimal)
                .AddScalar("QjManual", NHibernateUtil.Decimal)
                .AddScalar("QjManualBefore", NHibernateUtil.Decimal)
                .AddScalar("QjManualAfter", NHibernateUtil.Decimal)
                .AddScalar("QjRedirect", NHibernateUtil.Decimal)
                .AddScalar("QjReverse", NHibernateUtil.Decimal)
                .AddScalar("RockFrequency", NHibernateUtil.Decimal)
                .AddScalar("RotationFrequency", NHibernateUtil.Decimal)
                .AddScalar("RotationFrequency15", NHibernateUtil.Decimal)
                .AddScalar("RotationFrequency30", NHibernateUtil.Decimal)
                .AddScalar("RotationFrequencyDelta", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEngine", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEngine15", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEngine30", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEngineBefore", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEngineAfter", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEnvironment", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEnvironment15", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEnvironment30", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEnvironmentBefore", NHibernateUtil.Decimal)
                .AddScalar("TemperatureEnvironmentAfter", NHibernateUtil.Decimal)
                .AddScalar("TemperatureInterval", NHibernateUtil.Decimal)
                .AddScalar("Voltage", NHibernateUtil.Decimal)
                .AddScalar("VoltageAfter", NHibernateUtil.Decimal)
                .AddScalar("Volume", NHibernateUtil.Decimal)
                .AddScalar("VolumeBeforeCollector", NHibernateUtil.Decimal)
                .AddScalar("VolumeBeforeLoss", NHibernateUtil.Decimal)
                .AddScalar("WashPressureBegin", NHibernateUtil.Decimal)
                .AddScalar("WashPressureEnd", NHibernateUtil.Decimal)
                .AddScalar("WashVolume", NHibernateUtil.Decimal)
                .AddScalar("Weight", NHibernateUtil.Decimal)
                .AddScalar("Wheeling", NHibernateUtil.Decimal)

                .AddScalar("Brigade", NHibernateUtil.Int32)
                .AddScalar("Cycles", NHibernateUtil.Int32)
                .AddScalar("MeasureFacilityPosition", NHibernateUtil.Int32)
                .AddScalar("TicketTypeNum", NHibernateUtil.Int32)
                .AddScalar("TicketTypeCode", NHibernateUtil.Int32)

                .AddScalar("TicketID", NHibernateUtil.Int64)
                .AddScalar("DepartmentID", NHibernateUtil.Int64)

                .AddScalar("Created", NHibernateUtil.DateTime)
                .AddScalar("Accepted", NHibernateUtil.DateTime)
                .AddScalar("Completed", NHibernateUtil.DateTime)
                .AddScalar("Execution", NHibernateUtil.DateTime)
                .AddScalar("ObtainTime", NHibernateUtil.DateTime)
                .AddScalar("PaddingTime", NHibernateUtil.DateTime)
                .AddScalar("ProbeTime", NHibernateUtil.DateTime)
                .AddScalar("StartTime", NHibernateUtil.DateTime)
                .AddScalar("StartTimePlan", NHibernateUtil.DateTime)
                .AddScalar("TimeCompletion", NHibernateUtil.DateTime)
                .AddScalar("TreatmentDateBegin", NHibernateUtil.DateTime)
                .AddScalar("TreatmentDateEnd", NHibernateUtil.DateTime)
                .AddScalar("TreatmentTime", NHibernateUtil.DateTime)

                .AddScalar("BrigadeType", NHibernateUtil.String)
                .AddScalar("CalledAgents", NHibernateUtil.String)
                .AddScalar("DepartmentName", NHibernateUtil.String)
                .AddScalar("DetailClusterCode", NHibernateUtil.String)
                .AddScalar("DetailFieldName", NHibernateUtil.String)
                .AddScalar("DetailNote", NHibernateUtil.String)
                .AddScalar("DetailRequested", NHibernateUtil.String)
                .AddScalar("DetailSL", NHibernateUtil.String)
                .AddScalar("DetailWellName", NHibernateUtil.String)
                .AddScalar("DMGType", NHibernateUtil.String)
                .AddScalar("EquipmentVerification", NHibernateUtil.String)
                .AddScalar("EventType", NHibernateUtil.String)
                .AddScalar("ExecutionMembershipName", NHibernateUtil.String)
                .AddScalar("MembershipCreatedName", NHibernateUtil.String)
                .AddScalar("MovementStatus", NHibernateUtil.String)
                .AddScalar("PhaseCheck", NHibernateUtil.String)
                .AddScalar("Puller", NHibernateUtil.String)
                .AddScalar("PullerTypeName", NHibernateUtil.String)
                .AddScalar("PumpName", NHibernateUtil.String)
                .AddScalar("Reason", NHibernateUtil.String)
                .AddScalar("RepairType", NHibernateUtil.String)
                .AddScalar("RepairTypeName", NHibernateUtil.String)
                .AddScalar("RequestReason", NHibernateUtil.String)
                .AddScalar("RequestType", NHibernateUtil.String)
                .AddScalar("RotationChange", NHibernateUtil.String)
                .AddScalar("StopReason", NHibernateUtil.String)
                .AddScalar("TicketType", NHibernateUtil.String)
                .AddScalar("Treatment", NHibernateUtil.String)
                .AddScalar("UnionAction", NHibernateUtil.String)
                .AddScalar("ValveVerification", NHibernateUtil.String)
                .AddScalar("WellArea", NHibernateUtil.String);

            sqlQuery
                .SetParameter("wellID", wellID)
                .SetParameter("dateBegin", dateBegin)
                .SetParameter("dateEnd", dateEnd)
                .SetResultTransformer(Transformers.AliasToBean<RegisterRecord>());

            return sqlQuery.List<RegisterRecord>();
        }
    }
}
