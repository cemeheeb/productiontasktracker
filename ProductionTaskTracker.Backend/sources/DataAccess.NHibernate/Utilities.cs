﻿using NHibernate;

using System;
using System.Collections.Generic;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    using Domain.Entities;

    public static class Utilities {
        public static long GetSequenceNextValue(this ISession session, string sequenceName) {
            var dialect = session.GetSessionImplementation().Factory.Dialect;
            var sqlQuery = dialect.GetSequenceNextValString(sequenceName);
            var output = session.CreateSQLQuery(sqlQuery).UniqueResult();
            var outputLong = Convert.ToInt64(output);
            return outputLong;
        }

        public static List<Department> ExtractParents(this Department department) {
            var output = new List<Department>();
            output.Add(department);

            var scanline = department;
            while (scanline != null && scanline != department.Head) {
                scanline = scanline.Parent;
                output.Add(scanline);
            }

            return output;
        }
    }
}
