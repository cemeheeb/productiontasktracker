﻿using System.Diagnostics;
using NHibernate;
using NHibernate.SqlCommand;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    public class QueryLoggerInterceptor : EmptyInterceptor {
        public override SqlString OnPrepareStatement(SqlString sql) {
            Trace.WriteLine(sql);
            return base.OnPrepareStatement(sql);
        }
    }
}
