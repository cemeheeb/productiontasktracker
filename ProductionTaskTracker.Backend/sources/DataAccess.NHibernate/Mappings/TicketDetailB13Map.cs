﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB13Map : SubclassMap<TicketDetailB13> {
        public TicketDetailB13Map() {
            Table("tt_ticket_detail_b17");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.Q, "q");
            Map(x => x.HDynamic, "h_dynamic");
            Map(x => x.Pressure, "pressure"); // new
            Map(x => x.Amperage, "amperage"); // new
            Map(x => x.Voltage, "voltage"); // new
            Map(x => x.Loading, "loading"); // new
            Map(x => x.RotationFrequency, "rotation_frequency"); // new
            Map(x => x.PressureEnvironment, "pressure_environment"); // new
            Map(x => x.PressureEngine, "pressure_engine"); // new
            Map(x => x.TemperatureEnvironment, "temperature_environment"); // new
            Map(x => x.TemperatureEngine, "temperature_engine"); // new
            Map(x => x.ProtectionCorrection, "protection_correction"); // new
            Map(x => x.RotationChange, "rotation_change"); // new
            Map(x => x.QjReverse, "qj_reverse"); // new
            Map(x => x.AmperageReverse, "amperage_reverse"); // new
            Map(x => x.LoadingReverse, "loading_reverse"); // new
        }
    }
}