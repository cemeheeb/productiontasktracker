﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB10Map : SubclassMap<TicketDetailB10> {
        public TicketDetailB10Map() {
            Table("tt_ticket_detail_b10");
            KeyColumn("id");
            References(x => x.Shop, "shop_code").Not.Nullable();
            Map(x => x.MeasureFacilityPosition, "mf_position").Not.Nullable();
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.RequestReason, "reason").Not.Nullable();
        }
    }
}