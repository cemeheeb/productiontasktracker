﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailE3Map : SubclassMap<TicketDetailE3> {
        public TicketDetailE3Map() {
            Table("tt_ticket_detail_e3");
            KeyColumn("id");
            Map(x => x.ProbeTime, "probe_time");
            Map(x => x.Voltage, "voltage");
            Map(x => x.Amperage, "amperage");
            Map(x => x.Loading, "loading");
            Map(x => x.RotationFrequency, "rotation_frequency");
            Map(x => x.PressureEngine, "pressure_engine");
            Map(x => x.PressureEnvironment, "pressure_environment");
            Map(x => x.TemperatureEngine, "temperature_engine");
            Map(x => x.TemperatureEnvironment, "temperature_environment");
            Map(x => x.Duration, "duration");
        }
    }
}