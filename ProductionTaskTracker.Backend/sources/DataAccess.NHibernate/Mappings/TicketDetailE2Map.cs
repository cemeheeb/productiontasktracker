﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailE2Map : SubclassMap<TicketDetailE2> {
        public TicketDetailE2Map() {
            Table("tt_ticket_detail_e2");
            KeyColumn("id");
            Map(x => x.ProbeTime, "probe_time");
        }
    }
}