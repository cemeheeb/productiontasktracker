﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class FieldMap: ClassMap<Field> {
        public FieldMap() {
            Table("tt_fields");
            Id(x => x.Code, "code").Not.Nullable();
            Map(x => x.Name, "name").Not.Nullable();
        }
    }
}
