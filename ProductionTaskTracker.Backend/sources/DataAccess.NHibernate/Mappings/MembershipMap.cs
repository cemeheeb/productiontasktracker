﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class MembershipMap : ClassMap<Membership> {
        public MembershipMap() {
            Table("tt_memberships");
            Id(x => x.Id).GeneratedBy.Sequence("tt_membership_sequence"); ;
            Map(x => x.UserName, "username").Not.Nullable();
            Map(x => x.Password, "password");
            Map(x => x.PasswordSalt, "password_salt");
            Map(x => x.FirstName, "firstname").Not.Nullable();
            Map(x => x.LastName, "lastname").Not.Nullable();
            Map(x => x.Patronym, "patronym");

            HasManyToMany(x => x.Roles)
                .Table("tt_membership_role_link")
                .ParentKeyColumn("membership_id")
                .ChildKeyColumns.Add("membership_role_id")
                .Not.LazyLoad()
                .Fetch.Join();

            Map(x => x.Created, "created");
            Map(x => x.Deleted, "deleted");
        }
    }
}
