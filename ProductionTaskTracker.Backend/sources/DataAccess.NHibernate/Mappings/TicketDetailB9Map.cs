﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB9Map : SubclassMap<TicketDetailB9> {
        public TicketDetailB9Map() {
            Table("tt_ticket_detail_b9");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.StopReason, "reason").Not.Nullable();
            Map(x => x.Worked, "worked");
        }
    }
}