﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class EnterpriseMap : ClassMap<Enterprise> {
        public EnterpriseMap() {
            Table("tt_enterprises");
            Id(x => x.Id, "id").Not.Nullable();
            Map(x => x.Code, "code").Not.Nullable();
            Map(x => x.Name, "name").Not.Nullable();
            Map(x => x.DisplayName, "display_name").Not.Nullable();
        }
    }
}
