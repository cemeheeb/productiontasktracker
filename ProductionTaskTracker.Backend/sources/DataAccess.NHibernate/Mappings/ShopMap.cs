﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class ShopMap: ClassMap<Shop> {
        public ShopMap() {
            Table("tt_shops");
            Id(x => x.Code, "code");
            Map(x => x.Name, "name").Not.Nullable();
        }
    }
}
