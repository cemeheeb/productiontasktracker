﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class MeasureFacilityMap : ClassMap<MeasureFacility> {
        public MeasureFacilityMap() {
            Table("tt_measure_facilities");

            Id(x => x.Id).Not.Nullable();
            References(x => x.Shop, "shop_code").Not.Nullable();
            References(x => x.Field, "field_code").Not.Nullable();
            Map(x => x.Cluster, "cluster_code").Not.Nullable();
            Map(x => x.Position, "position").Not.Nullable();
        }
    }
}
