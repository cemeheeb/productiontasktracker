﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB4Map : SubclassMap<TicketDetailB4> {
        public TicketDetailB4Map() {
            Table("tt_ticket_detail_b4");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.WashVolume, "volume").Not.Nullable();
            Map(x => x.DMGBefore, "dmg_before"); // new
            Map(x => x.TreatmentDateBegin, "treatment_datebegin"); // new
            Map(x => x.TreatmentDateEnd, "treatment_dateend"); // new
            Map(x => x.VolumeBeforeLoss, "qn_volume_before_loss"); // new
            Map(x => x.VolumeBeforeCollector, "qn_volume_before_collector"); // new
            Map(x => x.TemperatureInterval, "temperature_interval"); // new
            Map(x => x.PressureADPMBegin, "pressure_adpm_begin"); // new
            Map(x => x.PressureADPMEnd, "pressure_adpm_end"); // new
            Map(x => x.QjManual, "qj_manual"); // new
        }
    }
}