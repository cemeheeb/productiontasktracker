﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailGMap : SubclassMap<TicketDetailG> {
        public TicketDetailGMap() {
            Table("tt_ticket_detail_g");
            KeyColumn("id");
        }
    }
}