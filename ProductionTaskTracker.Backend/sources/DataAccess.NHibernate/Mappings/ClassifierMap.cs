﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class ClassifierMap : ClassMap<Classifier> {
        public ClassifierMap() {
            Table("tt_classifiers");
            CompositeId()
                .KeyProperty(x => x.Id, "id")
                .KeyProperty(x => x.Kind, "kind");

            Map(x => x.Name, "name").Not.Nullable();
        }
    }
}
