﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class ControlledWellMap : ClassMap<ControlledWell> {
        public ControlledWellMap() {
            Table("tt_controlled_wells");
            Id(x => x.Id, "id").GeneratedBy.Sequence("tt_controlled_well_sequence");
            References(x => x.Well, "well_id").Not.Nullable();
            References(x => x.Department, "department_id").Not.Nullable();
            References(x => x.Executor, "executor_id").Not.Nullable();
            References(x => x.Position, "position_id").Not.Nullable();
            References(x => x.Membership, "membership_id").Not.Nullable();
            Map(x => x.TicketType, "ticket_type").Not.Nullable();
            Map(x => x.RepairType, "repair_type").Not.Nullable();
            Map(x => x.EventType, "event_type").Not.Nullable();
            References(x => x.Pump, "pump_id").Not.Nullable().NotFound.Ignore();
            Map(x => x.PumpDepth, "pump_depth").Not.Nullable();
            Map(x => x.Recent, "recent").Not.Nullable();
            Map(x => x.Deleted, "deleted").Not.Nullable();
        }
    }
}
