﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;

    public class TicketDetailCMap : SubclassMap<TicketDetailC> {
        public TicketDetailCMap() {
            Table("tt_ticket_detail_c1");
            KeyColumn("id");
            Map(x => x.StartTimePlan, "start_time_plan").Not.Nullable();
            Map(x => x.BrigadeType, "brigade_type").Not.Nullable();
            Map(x => x.Brigade, "brigade").Not.Nullable();
            Map(x => x.WellArea, "well_area").Not.Nullable();
            Map(x => x.HStatic, "h_static");
            Map(x => x.Pressure, "pressure"); // new
            Map(x => x.LockAvailable, "lock_available");
            Map(x => x.ServiceArea, "service_area");
            Map(x => x.BalanceDelta, "balance_delta");
        }
    }
}