﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;

    public class TicketDetailE8Map : SubclassMap<TicketDetailE8> {
        public TicketDetailE8Map() {
            Table("tt_ticket_detail_e8");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion");
            Map(x => x.Scraped, "scraped");
            Map(x => x.ScrapProblem, "scrap_problem");
        }
    }
}