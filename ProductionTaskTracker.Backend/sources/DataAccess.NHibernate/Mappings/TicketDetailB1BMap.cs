﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB1BMap : SubclassMap<TicketDetailB1B> {
        public TicketDetailB1BMap() {
            Table("tt_ticket_detail_b1b");
            KeyColumn("id");
            Map(x => x.StartTime, "start_time").Not.Nullable();
            Map(x => x.Treatment, "treatment").Not.Nullable();
            Map(x => x.RepairType, "repair_type").Not.Nullable();
            Map(x => x.PumpTypeSize, "pump_typesize").Not.Nullable();
            Map(x => x.UnionDiameter, "union_diameter").Not.Nullable();
            Map(x => x.PumpDepth, "pump_depth");
            Map(x => x.DampingVolume, "volume");
            Map(x => x.DampingWeight, "weight");
        }
    }
}