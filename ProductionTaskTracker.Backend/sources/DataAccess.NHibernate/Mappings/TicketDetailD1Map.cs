﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailD1Map : SubclassMap<TicketDetailD1> {
        public TicketDetailD1Map() {
            Table("tt_ticket_detail_d1");
            KeyColumn("id");
            Map(x => x.RepairType, "repair_type").Not.Nullable();
            Map(x => x.EventType, "event_type").Not.Nullable();
            References(x => x.Pump, "pump_id").Not.Nullable().NotFound.Ignore();
            Map(x => x.PumpDepth, "pump_depth").Not.Nullable();
            Map(x => x.Qj, "qj");
            Map(x => x.HDynamic, "h_dynamic");
            Map(x => x.Pressure, "pressure");
            Map(x => x.Amperage, "amperage");
            Map(x => x.Loading, "loading");
            Map(x => x.RotationFrequency, "rotation_frequency");
            Map(x => x.PressureEngine, "pressure_engine");
            Map(x => x.PressureEnvironment, "pressure_environment");
            Map(x => x.TemperatureEngine, "temperature_engine");
            Map(x => x.TemperatureEnvironment, "temperature_environment");
            Map(x => x.Duration, "duration");
            Map(x => x.OperationMode, "operation_mode");
        }
    }
}