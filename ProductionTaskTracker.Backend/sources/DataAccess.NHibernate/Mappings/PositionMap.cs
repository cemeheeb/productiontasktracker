﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class PositionMap : ClassMap<Position> {
        public PositionMap() {
            Table("tt_positions");
            Id(x => x.Id).GeneratedBy.Sequence("tt_position_sequence");
            References(x => x.Department, "department_id").Not.Nullable();
            Map(x => x.Name, "name").Not.Nullable();
            Map(x => x.DisplayName, "display_name").Not.Nullable();
            References(x => x.Role, "position_role_id").Not.Nullable();
            Map(x => x.Deleted, "deleted").Not.Nullable();
        }
    }
}
