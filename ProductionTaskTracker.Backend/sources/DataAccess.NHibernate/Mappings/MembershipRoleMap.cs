﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class MembershipRoleMap : ClassMap<MembershipRole> {
        public MembershipRoleMap() {
            Table("tt_membership_roles");
            Id(x => x.Id);
            Map(x => x.Name, "name").Not.Nullable();
            Map(x => x.DisplayName, "display_name").Not.Nullable();
            Map(x => x.Deleted, "deleted");
            BatchSize(25);
        }
    }
}
