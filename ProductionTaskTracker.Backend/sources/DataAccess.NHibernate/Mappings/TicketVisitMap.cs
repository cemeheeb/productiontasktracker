﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class TicketVisitMap : ClassMap<TicketVisit> {
        public TicketVisitMap() {
            Table("tt_ticket_visits");
            Id(x => x.Id, "id").Not.Nullable();
            References(x => x.TicketDetail, "ticket_detail_id").Not.Nullable();
            References(x => x.TicketMovement, "ticket_movement_id").Not.Nullable();
            References(x => x.Membership, "membership_id").Not.Nullable();
            References(x => x.Position, "position_id").Not.Nullable();
            Map(x => x.IsViewed, "is_viewed").Not.Nullable();
            Map(x => x.Created, "created").Not.Nullable();
        }
    }
}
