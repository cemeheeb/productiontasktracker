﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class AutocompleteOptionMap : ClassMap<AutocompleteOption> {
        public AutocompleteOptionMap() {
            Table("tt_autocomplete_options");
            Id(x => x.Id).GeneratedBy.Sequence("hibernate_sequence");
            References(x => x.Membership, "membership_id").Not.Nullable();
            Map(x => x.Parameter, "parameter").Not.Nullable();
            Map(x => x.Text, "text").Not.Nullable();
            Map(x => x.Weight, "weight").Not.Nullable();
        }
    }
}
