﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailE6Map : SubclassMap<TicketDetailE6> {
        public TicketDetailE6Map() {
            Table("tt_ticket_detail_e6");
            KeyColumn("id");
            Map(x => x.PaddingTime, "probe_time");
        }
    }
}