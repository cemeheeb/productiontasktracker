﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB1AMap : SubclassMap<TicketDetailB1A> {
        public TicketDetailB1AMap() {
            Table("tt_ticket_detail_b1a");
            KeyColumn("id");
            Map(x => x.StartTime, "start_time").Not.Nullable();
            Map(x => x.RequestType, "request_type").Not.Nullable();
            Map(x => x.StartTimeFact, "start_time_fact");
            Map(x => x.HStatic, "h_static");
            Map(x => x.Pressure, "pressure");
            Map(x => x.PressureEnvironment, "pressure_environment");
            Map(x => x.PressureEngine, "pressure_engine");
            Map(x => x.HStaticPipe, "h_static_pipe");
            Map(x => x.PressurePipe, "pressure_pipe");
        }
    }
}