﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailE5Map : SubclassMap<TicketDetailE5> {
        public TicketDetailE5Map() {
            Table("tt_ticket_detail_e5");
            KeyColumn("id");
            Map(x => x.ObtainTime, "probe_time");
        }
    }
}