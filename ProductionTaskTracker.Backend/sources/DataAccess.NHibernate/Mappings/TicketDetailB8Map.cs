﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB8Map : SubclassMap<TicketDetailB8> {
        public TicketDetailB8Map() {
            Table("tt_ticket_detail_b8");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.RequestType, "request_type").Not.Nullable();
            Map(x => x.HDynamic, "h_dynamic"); // new
            Map(x => x.Pressure, "pressure"); // new
            Map(x => x.HStatic, "h_static"); // new
            Map(x => x.PressureRedundant, "pressure_redundant"); // new
            Map(x => x.PressureAfter, "pressure_after"); // new
            Map(x => x.PowerOff, "power_off"); // new
            Map(x => x.Purge, "purge"); // new
            Map(x => x.RequestedEPU, "requested_epu");
            Map(x => x.RequestedCNIPR, "requested_cnipr");
        }
    }
}