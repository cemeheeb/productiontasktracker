﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class DepartmentMap : ClassMap<Department> {
        public DepartmentMap() {
            Table("tt_departments");
            Id(x => x.Id).GeneratedBy.Sequence("tt_department_sequence");
            References(x => x.Parent, "parent_id");
            References(x => x.Head, "head_id");
            Map(x => x.FullName, "name").Not.Nullable();
            Map(x => x.DisplayName, "display_name").Not.Nullable();
            References(x => x.Shop, "shop_code");
            HasMany(x => x.Children).KeyColumn("parent_id").Inverse().Fetch.Join().BatchSize(30).ReadOnly();
            Map(x => x.Deleted, "deleted").Not.Nullable();
        }
    }
}
