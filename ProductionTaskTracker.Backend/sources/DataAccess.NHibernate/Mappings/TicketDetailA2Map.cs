﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailA2Map : SubclassMap<TicketDetailA2> {
        public TicketDetailA2Map() {
            Table("tt_ticket_detail_a2");
            KeyColumn("id");
            Map(x => x.UnionAction, "union_action_type");
            Map(x => x.RockFrequency, "rock_frequency");
            Map(x => x.RotationFrequencyDelta, "rotation_frequency_delta");
            Map(x => x.Wheeling, "wheeling");
            Map(x => x.UnionDynamic, "union_dynamic"); // new
            Map(x => x.PlungerLength, "plunger_length"); // new
            Map(x => x.WellPeriodMode, "well_period_mode"); // new
        }
    }
}