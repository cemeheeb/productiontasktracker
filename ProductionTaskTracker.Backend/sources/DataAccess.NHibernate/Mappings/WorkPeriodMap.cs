﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class WorkPeriodMap : ClassMap<WorkPeriod> {
        public WorkPeriodMap() {
            Table("tt_work_periods");
            Id(x => x.Id)
                .GeneratedBy.Sequence("tt_work_period_sequence");

            References(x => x.Membership)
                .Column("membership_id")
                .Index("tt_wp_membership_idx")
                .Not.Nullable()
                .Cascade.All();

            References(x => x.Position)
                .Column("position_id")
                .Index("tt_wp_position_idx")
                .Not.Nullable()
                .Cascade.All();

            Map(x => x.DateBegin, "date_begin")
                .Not.Nullable();

            Map(x => x.DateEnd, "date_end")
                .Nullable();

            Map(x => x.Deleted, "deleted")
                .Not.Nullable();
        }
    }
}
