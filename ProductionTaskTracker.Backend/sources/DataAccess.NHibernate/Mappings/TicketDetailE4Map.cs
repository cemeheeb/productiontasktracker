﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailE4Map : SubclassMap<TicketDetailE4> {
        public TicketDetailE4Map() {
            Table("tt_ticket_detail_e4");
            KeyColumn("id");
            Map(x => x.TreatmentTime, "probe_time");
        }
    }
}