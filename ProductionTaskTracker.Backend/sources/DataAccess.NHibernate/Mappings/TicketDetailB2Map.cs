﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB2Map : SubclassMap<TicketDetailB2> {
        public TicketDetailB2Map() {
            Table("tt_ticket_detail_b2");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.WashVolume, "volume").Not.Nullable();
            Map(x => x.Weight, "weight").Not.Nullable();
            Map(x => x.CalledAgents, "called_agents").Not.Nullable();
            Map(x => x.QjManualBefore, "qj_manual_before");
            Map(x => x.AmperageBefore, "amperage_before");
            Map(x => x.LoadingBefore, "loading_before");
            Map(x => x.RotationFrequency, "rotation_frequency");
            Map(x => x.PressureEnvironmentBefore, "pressure_environment_before");
            Map(x => x.PressureEngineBefore, "pressure_engine_before");
            Map(x => x.TemperatureEnvironmentBefore, "temperature_environment_before");
            Map(x => x.TemperatureEngineBefore, "temperature_engine_before");
            Map(x => x.RotationChange, "rotation_change");
            Map(x => x.PhaseCheck, "phase_check");
            Map(x => x.HStaticAfterStop, "h_static_after_stop");
            Map(x => x.CrimpingBegin, "crimping_begin");
            Map(x => x.CrimpingEnd, "crimping_end");
            Map(x => x.CrimpingDuration, "crimping_duration");
            Map(x => x.HStaticAfterCrimping, "h_static_after_crimping");
            Map(x => x.WashPressureBegin, "wash_pressure_begin");
            Map(x => x.WashPressureEnd, "wash_pressure_end");
            Map(x => x.HDynamicPressureAfterWash, "h_dynamic_pressure_after_wash");
            Map(x => x.HDynamicPressureDuration, "h_dynamic_pressure_duration");
            Map(x => x.QjManualAfter, "qj_manual_after");
            Map(x => x.AmperageAfter, "amperage_after");
            Map(x => x.VoltageAfter, "voltage_after");
            Map(x => x.PressureEnvironmentAfter, "pressure_environment_after");
            Map(x => x.PressureEngineAfter, "pressure_engine_after");
            Map(x => x.TemperatureEnvironmentAfter, "temperature_environment_after");
            Map(x => x.TemperatureEngineAfter, "temperature_engine_after");
            Map(x => x.PumpCrimping, "pump_crimping"); //new
            Map(x => x.PumpWashing, "pump_washing"); //new
        }
    }
}