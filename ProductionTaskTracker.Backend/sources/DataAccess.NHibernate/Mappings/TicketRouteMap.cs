﻿using EAEConsult.ProductionTaskTracker.Backend.Domain.Enumerables;
using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class TicketRouteMap : ClassMap<TicketRoute> {
        public TicketRouteMap() {
            Table("tt_ticket_routes");
            Id(x => x.Id).GeneratedBy.Sequence("tt_ticket_route_sequence");
            Map(x => x.TicketClass, "ticket_class").Not.Nullable();
            Map(x => x.PositionRole, "position_role").Not.Nullable();
            References(x => x.DepartmentFrom, "department_id_from").Not.Nullable();
            Map(x => x.StatusFrom, "status_from").CustomType(typeof(TicketStatus));
            References(x => x.DepartmentTo, "department_id_to").Not.Nullable();
            Map(x => x.StatusTo, "status_to").CustomType(typeof(TicketStatus)).Not.Nullable();
            Map(x => x.DateBegin, "date_begin").Not.Nullable();
            Map(x => x.DateEnd, "date_end");
            References(x => x.Executor, "executor_id").Not.Nullable();
            Map(x => x.Deleted, "deleted").Not.Nullable();
        }
    }
}
