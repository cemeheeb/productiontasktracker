﻿using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Enumerables;
    using Domain.Entities;

    public class TicketMap: ClassMap<Ticket> {
        public TicketMap() {
            Table("tt_tickets");
            Id(x => x.Id).GeneratedBy.Sequence("tt_ticket_sequence");
            Map(x => x.TicketType, "ticket_type").CustomType<EnumStringType<TicketType>>().Not.Nullable();
            Map(x => x.GroupID, "group_id");
            Map(x => x.IsNight, "is_night").Not.Nullable();
            References(x => x.Department, "department_id").Not.Nullable();
            HasMany(x => x.TicketMovements).KeyColumn("ticket_id").Fetch.Join().BatchSize(30).Cascade.AllDeleteOrphan();

            Map(x => x.Created, "created").Not.Nullable();
            Map(x => x.Deleted, "deleted").Not.Nullable();
        }
    }
}
