﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB6Map : SubclassMap<TicketDetailB6> {
        public TicketDetailB6Map() {
            Table("tt_ticket_detail_b6");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.Preparing, "preparing");
            Map(x => x.Mixture, "mixture"); // new
        }
    }
}