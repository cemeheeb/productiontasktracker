﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailA1Map : SubclassMap<TicketDetailA1> {
        public TicketDetailA1Map() {
            Table("tt_ticket_detail_a1");
            KeyColumn("id");
            Map(x => x.Qj, "qj");
            Map(x => x.EquipmentVerification, "equipment_verification");
            Map(x => x.ValveVerification, "valve_verification");
            Map(x => x.QjLocked, "qj_locked");
            Map(x => x.QjRedirect, "qj_redirect");
            Map(x => x.Crimping, "crimping");
            Map(x => x.PressureBuffer, "pressure_buffer");
            Map(x => x.HDynamic, "h_dynamic");
            Map(x => x.Pressure, "pressure");
            Map(x => x.Amperage, "amperage");
            Map(x => x.Loading, "loading");
            Map(x => x.RotationFrequency, "rotation_frequency");
            Map(x => x.PressureEnvironment, "pressure_environment");
            Map(x => x.PressureEngine, "pressure_engine");
            Map(x => x.RevisionSVU, "revision_svu");
            Map(x => x.ReplaceSVU, "replace_svu");
            Map(x => x.TemperatureEnvironment, "temperature_environment");
            Map(x => x.TemperatureEngine, "temperature_engine");
            Map(x => x.GroundEquipmentVerification, "ground_equipment_verification"); // new

        }
    }
}