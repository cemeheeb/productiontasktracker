﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB12Map : SubclassMap<TicketDetailB12> {
        public TicketDetailB12Map() {
            Table("tt_ticket_detail_b12");
            KeyColumn("id");
            References(x => x.Contractor, "contractor_id"); // new
            Map(x => x.Brigade, "brigade").Not.Nullable(); // new
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.RequestReason, "reason").Not.Nullable();
        }
    }
}