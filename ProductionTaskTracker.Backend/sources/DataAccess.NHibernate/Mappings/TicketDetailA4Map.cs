﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailA4Map : SubclassMap<TicketDetailA4> {
        public TicketDetailA4Map() {
            Table("tt_ticket_detail_a4");
            KeyColumn("id");
            Map(x => x.Voltage, "voltage");
            Map(x => x.Amperage, "amperage");
            Map(x => x.Loading, "loading");
            Map(x => x.RotationFrequency, "rotation_frequency");
            Map(x => x.PressureEnvironment, "pressure_environment");
            Map(x => x.PressureEngine, "pressure_engine");
            Map(x => x.TemperatureEnvironment, "temperature_environment");
            Map(x => x.TemperatureEngine, "temperature_engine");
            Map(x => x.DMGType, "dmg_type");
            Map(x => x.Puller, "puller");
            Map(x => x.Gathering, "gathering"); // new
            Map(x => x.SeparateExpense, "separate_expense"); // new
            Map(x => x.SeparateHumidity, "separate_humidity"); // new
            Map(x => x.SeparatePressure, "separate_pressure"); // new
            Map(x => x.SeparateTemperature, "separate_temperature"); // new
        }
    }
}