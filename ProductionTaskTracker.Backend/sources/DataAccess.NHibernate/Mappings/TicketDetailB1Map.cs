﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB1Map : SubclassMap<TicketDetailB1> {
        public TicketDetailB1Map() {
            Table("tt_ticket_detail_b1");
            KeyColumn("id");
            Map(x => x.StartTime, "start_time").Not.Nullable();
            Map(x => x.Q, "q");
            Map(x => x.HStatic, "h_static");
            Map(x => x.Pressure, "pressure");
            Map(x => x.PressureEnvironment, "pressure_environment");
            Map(x => x.PressureEngine, "pressure_engine");
            Map(x => x.TemperatureEnvironment, "temperature_environment");
            Map(x => x.TemperatureEngine, "temperature_engine");
            Map(x => x.Amperage, "amperage");
            Map(x => x.Loading, "loading");
            Map(x => x.RotationFrequency, "rotation_frequency");
            Map(x => x.HDynamic15, "h_dynamic_15");
            Map(x => x.Pressure15, "pressure_15");
            Map(x => x.Q15, "q_15");
            Map(x => x.PressureEnvironment15, "pressure_environment_15");
            Map(x => x.PressureEngine15, "pressure_engine_15");
            Map(x => x.TemperatureEnvironment15, "temperature_environment_15");
            Map(x => x.TemperatureEngine15, "temperature_engine_15");
            Map(x => x.Amperage15, "amperage_15");
            Map(x => x.Loading15, "loading_15");
            Map(x => x.RotationFrequency15, "rotation_frequency_15");
            Map(x => x.HDynamic30, "h_dynamic_30");
            Map(x => x.Pressure30, "pressure_30");
            Map(x => x.Q30, "q_30");
            Map(x => x.PressureEnvironment30, "pressure_environment_30");
            Map(x => x.PressureEngine30, "pressure_engine_30");
            Map(x => x.TemperatureEnvironment30, "temperature_environment_30");
            Map(x => x.TemperatureEngine30, "temperature_engine_30");
            Map(x => x.Amperage30, "amperage_30");
            Map(x => x.Loading30, "loading_30");
            Map(x => x.RotationFrequency30, "rotation_frequency_30");
            Map(x => x.RepairType, "repair_type").Not.Nullable();
            References(x => x.Pump, "pump_id").Not.Nullable().NotFound.Ignore();
            Map(x => x.PumpDepth, "pump_depth");
            Map(x => x.DampingVolume, "volume");
            Map(x => x.DampingWeight, "weight");
            Map(x => x.HDynamicMinimal, "h_dynamic_minimal");
            Map(x => x.WellPurpose, "well_purpose").Not.Nullable();
            Map(x => x.Research, "research").Not.Nullable();
            Map(x => x.PressureEcnMinimal, "pressure_ecn_minimal");
            Map(x => x.ShankLength, "shank_length");
        }
    }
}