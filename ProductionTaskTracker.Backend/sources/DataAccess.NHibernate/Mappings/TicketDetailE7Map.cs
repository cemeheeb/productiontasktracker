﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;

    public class TicketDetailE7Map : SubclassMap<TicketDetailE7> {
        public TicketDetailE7Map() {
            Table("tt_ticket_detail_e7");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion");
            Map(x => x.HDynamic, "h_dynamic");
            Map(x => x.Pressure, "pressure");
        }
    }
}