﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB11Map : SubclassMap<TicketDetailB11> {
        public TicketDetailB11Map() {
            Table("tt_ticket_detail_b11");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.RequestReason, "reason").Not.Nullable();
        }
    }
}