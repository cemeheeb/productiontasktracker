﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailE1Map : SubclassMap<TicketDetailE1> {
        public TicketDetailE1Map() {
            Table("tt_ticket_detail_e1");
            KeyColumn("id");
            Map(x => x.ProbeTime, "probe_time");
        }
    }
}