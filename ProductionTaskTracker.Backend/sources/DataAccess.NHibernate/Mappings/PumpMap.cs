﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class PumpMap: ClassMap<Pump> {
        public PumpMap() {
            Table("tt_pumps");
            Id(x => x.Id, "id").Not.Nullable();
            Map(x => x.Title, "title").Not.Nullable();
            Map(x => x.TypeECN, "type_ecn");
            Map(x => x.Gb, "gb");
            Map(x => x.NominalFlow, "nflow");
            Map(x => x.NominalFeed, "nfeed");
            Map(x => x.Col1, "col1");
            Map(x => x.Col2, "col2");
            Map(x => x.Col3, "col3");
            Map(x => x.Factory, "factory");
            Map(x => x.Country, "country");
        }
    }
}
