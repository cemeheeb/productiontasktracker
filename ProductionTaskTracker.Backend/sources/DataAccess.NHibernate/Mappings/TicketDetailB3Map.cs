﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB3Map : SubclassMap<TicketDetailB3> {
        public TicketDetailB3Map() {
            Table("tt_ticket_detail_b3");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.WashVolume, "volume").Not.Nullable();
            Map(x => x.Weight, "weight").Not.Nullable();
            Map(x => x.NeedCrimping, "need_crimping"); //crimping -> need_crimping
            Map(x => x.QjManualBefore, "qj_manual_before"); // new
            Map(x => x.AmperageBefore, "amperage_before"); // new
            Map(x => x.LoadingBefore, "loading_before"); // new
            Map(x => x.RotationFrequency, "rotation_frequency"); // new
            Map(x => x.PressureEnvironmentBefore, "pressure_environment_before"); // new
            Map(x => x.PressureEngineBefore, "pressure_engine_before"); // new
            Map(x => x.TemperatureEnvironmentBefore, "temperature_environment_before"); // new
            Map(x => x.TemperatureEngineBefore, "temperature_engine_before"); // new
            Map(x => x.HStaticAfterStop, "h_static_after_stop"); // new
            Map(x => x.CrimpingBegin, "crimping_begin"); // new
            Map(x => x.CrimpingEnd, "crimping_end"); // new
            Map(x => x.CrimpingDuration, "crimping_duration"); // new
            Map(x => x.HStaticAfterCrimping, "h_static_after_crimping"); // new
            Map(x => x.WashPressureBegin, "wash_pressure_begin"); // new
            Map(x => x.WashPressureEnd, "wash_pressure_end"); // new
            Map(x => x.HDynamicPressureAfterWash, "h_dynamic_pressure_after_wash"); // new
            Map(x => x.HDynamicPressureDuration, "h_dynamic_pressure_duration"); // new
            Map(x => x.QjManualAfter, "qj_manual_after"); // new
            Map(x => x.AmperageAfter, "amperage_after"); // new
            Map(x => x.VoltageAfter, "voltage_after"); // new
            Map(x => x.PressureEnvironmentAfter, "pressure_environment_after"); // new
            Map(x => x.PressureEngineAfter, "pressure_engine_after"); // new
            Map(x => x.TemperatureEnvironmentAfter, "temperature_environment_after"); // new
            Map(x => x.TemperatureEngineAfter, "temperature_engine_after"); // new
        }
    }
}