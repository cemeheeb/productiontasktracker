﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB5Map : SubclassMap<TicketDetailB5> {
        public TicketDetailB5Map() {
            Table("tt_ticket_detail_b5");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.DampingVolume, "volume").Not.Nullable();
            Map(x => x.Weight, "weight").Not.Nullable();
            Map(x => x.Cycles, "cycles").Not.Nullable();
            Map(x => x.DampingVolumeCycleA, "damping_volume_cycle_1"); // new
            Map(x => x.DampingWeightCycleA, "weight_cycle_1"); // new
            Map(x => x.DampingVolumeCycleB, "damping_volume_cycle_2"); // new
            Map(x => x.DampingWeightCycleB, "weight_cycle_2"); // new
            Map(x => x.DampingVolumeCycleC, "damping_volume_cycle_3"); // new
            Map(x => x.DampingWeightCycleC, "weight_cycle_3"); // new
            Map(x => x.DampingVolumeCycleD, "damping_volume_cycle_4"); // new
            Map(x => x.DampingWeightCycleD, "weight_cycle_4"); // new
            Map(x => x.PressureAfterDamping, "pressure_after_damping"); // new
            Map(x => x.LiquidProbe, "liquid_probe"); // new
        }
    }
}