﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailBaseMap : ClassMap<TicketDetailBase> {
        public TicketDetailBaseMap() {
            Table("tt_ticket_detail_base");
            Id(x => x.Id, "id").GeneratedBy.Sequence("tt_ticket_detail_sequence").Not.Nullable();
            References(x => x.Well, "well_id").Fetch.Join();
            References(x => x.Field, "field_code").Fetch.Join();
            Map(x => x.Cluster, "cluster_code");
            Map(x => x.Expired, "expired");
            Map(x => x.Note, "note");
            Map(x => x.SL, "sl");
            References(x => x.Ticket, "ticket_id").Fetch.Join();
            References(x => x.Membership, "membership_id").Not.Nullable();
            Map(x => x.Requested, "requested");
            Map(x => x.AttachmentID, "attachment_id");
        }
    }
}
