﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities;

    public class WellRegularMap: ClassMap<WellRegular> {
        public WellRegularMap() {
            Table("tt_wells");
            Id(x => x.Id, "id").Not.Nullable().GeneratedBy.Assigned();
            Map(x => x.Code, "code").Not.Nullable();
            References(x => x.Shop, "shop_code").Not.Nullable();
            References(x => x.Field, "field_code").Not.Nullable();
            Map(x => x.EnterpriseCode, "enterprise_code").Nullable();
            Map(x => x.Cluster, "cluster_code").Not.Nullable();
        }
    }
}
