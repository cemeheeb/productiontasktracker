﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Enumerables;
    using Domain.Entities;

    public class TicketMovementMap : ClassMap<TicketMovement> {
        public TicketMovementMap() {
            Table("tt_ticket_movements");
            Id(x => x.Id).GeneratedBy.Sequence("tt_ticket_movement_sequence");
            References(x => x.Ticket, "ticket_id").Not.Nullable().Fetch.Join();
            References(x => x.TicketDetail, "ticket_detail_id").Cascade.All().LazyLoad(Laziness.NoProxy);
            References(x => x.Membership, "membership_id").Not.Nullable().Fetch.Join();
            References(x => x.Position, "position_id").Not.Nullable().Fetch.Join();
            References(x => x.Department, "department_id").Not.Nullable().Fetch.Join();
            Map(x => x.Status, "status").CustomType<TicketStatus>().Not.Nullable();
            Map(x => x.Created, "created").Not.Nullable();
            Map(x => x.Deleted, "deleted").Not.Nullable();
        }
    }
}
