﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailB7Map : SubclassMap<TicketDetailB7> {
        public TicketDetailB7Map() {
            Table("tt_ticket_detail_b7");
            KeyColumn("id");
            Map(x => x.TimeCompletion, "time_completion").Not.Nullable();
            Map(x => x.PressureCrimping, "pressure_crimping").Not.Nullable();
        }
    }
}