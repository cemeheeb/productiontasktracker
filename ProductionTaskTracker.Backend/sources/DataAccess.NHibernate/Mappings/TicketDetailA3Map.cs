﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailA3Map : SubclassMap<TicketDetailA3> {
        public TicketDetailA3Map() {
            Table("tt_ticket_detail_a3");
            KeyColumn("id");
            Map(x => x.Watering, "watering");
            Map(x => x.Kvch, "kvch");
            Map(x => x.MultiComponent, "multi_component");
            Map(x => x.Carbonate, "carbonate");
            Map(x => x.Inhibitor, "inhibitor");
            Map(x => x.ProbeVolume, "volume");
            Map(x => x.OilComponent, "oil_component"); // new
            Map(x => x.Complex, "complex"); // new
        }
    }
}