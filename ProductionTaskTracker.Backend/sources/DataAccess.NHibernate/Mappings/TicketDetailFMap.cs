﻿using FluentNHibernate.Mapping;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate.Mappings {
    using Domain.Entities.TicketDetails;
    public class TicketDetailFMap : SubclassMap<TicketDetailF> {
        public TicketDetailFMap() {
            Table("tt_ticket_detail_f");
            KeyColumn("id");
        }
    }
}