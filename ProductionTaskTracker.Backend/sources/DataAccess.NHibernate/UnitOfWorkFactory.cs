﻿using System.Data;

namespace EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate {
    public class UnitOfWorkFactory : IUnitOfWorkFactory {
        #region IUnitOfWorkFactory Members

        public IUnitOfWork Create(IsolationLevel isolationLevel) {
            var session = NHibernateHelper.SessionFactory.OpenSession();
            return new UnitOfWork(session, isolationLevel);
        }

        public IUnitOfWork Create() {
            return Create(IsolationLevel.ReadCommitted);
        }

        #endregion
    }
}
