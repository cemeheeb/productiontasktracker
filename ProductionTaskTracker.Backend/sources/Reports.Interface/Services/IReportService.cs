﻿using System;
using System.IO;

namespace EAEConsult.ProductionTaskTracker.Backend.Reports.Services {
    public interface IReportService {
        void ReportA(string filename, DateTime dateBegin, DateTime dateEnd, long[] departments, long[] wells, string[] ticketTypes, int? SL, bool showExpired);

        void ReportB(string filename, DateTime dateBegin, DateTime dateEnd, long[] departments, long[] wells, string[] ticketTypes, int? SL, bool showExpired);
    }
}
