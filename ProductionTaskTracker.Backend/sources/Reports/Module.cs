﻿using EAEConsult.ProductionTaskTracker.Backend.Reports.Services;
using Ninject.Modules;

namespace EAEConsult.ProductionTaskTracker.Backend.Reports {
    public class Module : NinjectModule {
        public override void Load() {
            Bind<IReportService>().To<ReportService>().InSingletonScope();
        }
    }
}
