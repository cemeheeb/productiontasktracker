﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Reports.Services {
    using DataAccess.Services;
    using Reports;

    public class ReportService: IReportService {

        #region Fields

        private readonly IReportDataService _reportDataService;

        #endregion

        public ReportService(IReportDataService reportDataService) {
            _reportDataService = reportDataService;
        }

        public void ReportA(string filename, DateTime dateBegin, DateTime dateEnd, long[] departments, long[] wells, string[] ticketTypes, int? SL, bool showExpired) {
            var report = new ReportA(DateTime.Now, _reportDataService.GetReportAData(dateBegin, dateEnd, departments, wells, ticketTypes, SL, showExpired));
            report.Make(filename);
        }

        public void ReportB(string filename, DateTime dateBegin, DateTime dateEnd, long[] departments, long[] wells, string[] ticketTypes, int? SL, bool showExpired) {
            
        }
    }
}
