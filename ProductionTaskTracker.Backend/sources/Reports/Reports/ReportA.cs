﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

using SpreadsheetGear;

namespace EAEConsult.ProductionTaskTracker.Backend.Reports.Reports {
    using Business;
    using Domain.DTO;
    using Domain;
    using Domain.Enumerables;

    public class ReportA : IReport {
        #region Fields

        private readonly DateTime _reportDate;
        private readonly IList<RegisterRecord> _data;

        #endregion

        public ReportA(DateTime reportDate, IEnumerable<RegisterRecord> data) {
            _reportDate = reportDate;
            _data = new List<RegisterRecord>(data);
        }
        
        public void Make(string filename) {
            Factory.GetWorkbookSet(System.Globalization.CultureInfo.CurrentCulture);

            var assembly = Assembly.GetExecutingAssembly();
            var workbook =
                Factory.GetWorkbookSet()
                    .Workbooks
                    .OpenFromStream(assembly.GetManifestResourceStream("EAEConsult.ProductionTaskTracker.Backend.Reports.Templates.Template_type1.xlsx"));

            var worksheet = workbook.Worksheets[0];

            worksheet.Name = "Название";

            // Обработка
            worksheet.Cells[0, 0].Value = $"Отчёт за {_reportDate}";
            
            var row = 5;
            var exclusions = new List<IRange>();
            foreach (var reportATableRecord in _data) {
                var j = 0;
                worksheet.Cells[row, 0].Value = row - 4; // № пп
                worksheet.Cells[row, 1].Value = reportATableRecord.TicketID; //№ заявки
                worksheet.Cells[row, 2].Value = reportATableRecord.DepartmentName; // ЦДНГ
                worksheet.Cells[row, 3].Value = $"{reportATableRecord.Created:dd.MM.yyyy HH:mm}"; // Дата и время подачи заявки
                worksheet.Cells[row, 4].Value = Utilities.GetTicketTypeDescription(reportATableRecord.TicketType); // Тип заявки

                var ticketClassType = Utilities.GetTicketTypeFromEnumerable((TicketType)Enum.Parse(typeof(TicketType), reportATableRecord.TicketType));
                var ticketDetailRequestableProperties = EnumerateRequestableProperties(ticketClassType);

                var type = reportATableRecord.GetType();
                foreach (var property in type.GetProperties()) {
                    bool isRequested = ticketDetailRequestableProperties.Any(x => x.Key == property.Name && reportATableRecord.DetailRequested.IndexOf(x.Value) > -1);
                    if (property.GetValue(reportATableRecord) == null && !isRequested) {
                        continue;
                    }

                    var founded = worksheet.Cells.Find($"{{{property.Name}}}", null, FindLookIn.Values, LookAt.Whole, SearchOrder.ByColumns, SearchDirection.Next, true);
                    if (founded == null) {
                        continue;
                    }

                    if (isRequested) {
                        exclusions.Add(founded);
                        worksheet.Cells[row, founded.Column].Value = "Запрошено";
                    } else {
                        worksheet.Cells[row, founded.Column].Value = Utilities.PropertyToString(property, reportATableRecord);
                    }

                }

                row++;
            }

            // Завести список исключений и проверять помимо значений и колонки из исключений ниже
        

            foreach (var property in typeof(RegisterRecord).GetProperties()) {
                var founded = worksheet.Cells.Find($"{{{property.Name}}}", null, FindLookIn.Values, LookAt.Whole, SearchOrder.ByColumns, SearchDirection.Next, true);
                if (founded == null) {
                    continue;
                }

                switch (property.Name) {
                    case "EquipmentVerification":
                    case "GroundEquipmentVerification":
                    case "ValveVerification":
                    case "UnionAction":
                    case "PullerTypeName":
                    case "Treatment":
                    case "RepairType":
                    case "PumpName":
                    case "CalledAgents":
                    case "Reason":
                    case "WellArea":
                    case "EventType":
                        worksheet.Cells[row, founded.Column].EntireColumn.Hidden = _data.All(x => string.IsNullOrEmpty(property.GetValue(x) as string));
                        break;
                    default:
                        worksheet.Cells[row, founded.Column].EntireColumn.Hidden = _data.All(x => property.GetValue(x) == null);
                        break;
                }
            }

            foreach (var range in exclusions) {
                range.EntireColumn.Hidden = false;
            }

            workbook.SaveAs(filename, FileFormat.Excel8);
        }
        internal List<KeyValuePair<string, string>> EnumerateRequestableProperties(Type ticketDetailType) {
            return ticketDetailType
                .GetProperties()
                .Where(x => Attribute.IsDefined(x, typeof(TicketDetailPropertyRequestableAttribute)))
                .Select(x => new KeyValuePair<string, string>(x.Name, x.GetCustomAttributes(true).Where(y => y is TicketDetailPropertyRequestableAttribute).Select(z => ((TicketDetailPropertyRequestableAttribute)z).Name).First()))
                .ToList();
        }
    }
}