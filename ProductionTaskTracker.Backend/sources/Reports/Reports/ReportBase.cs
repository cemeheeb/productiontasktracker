﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.Reports.Reports {
    public abstract class ReportBase {
        protected DateTime DateBegin;
        protected DateTime DateEnd;

        protected ReportBase(DateTime dateBegin, DateTime dateEnd) {
            DateBegin = dateBegin;
            DateEnd = dateEnd;
        }
    }
}
