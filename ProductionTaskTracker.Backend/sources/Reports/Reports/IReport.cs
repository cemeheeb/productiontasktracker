﻿namespace EAEConsult.ProductionTaskTracker.Backend.Reports.Reports {
    public interface IReport {
        void Make(string filename);
    }
}
