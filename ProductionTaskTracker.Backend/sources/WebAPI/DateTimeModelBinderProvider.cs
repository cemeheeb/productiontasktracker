﻿using System;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI {
    public class DateTimeModelBinderProvider: ModelBinderProvider {
        private readonly DateTimeModelBinder _binder = new DateTimeModelBinder();

        public override IModelBinder GetBinder(HttpConfiguration configuration, Type modelType) {
            return DateTimeModelBinder.CanBindType(modelType) ? _binder : null;
        }
    }
}