﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Providers {
    using Formats;

    public interface IOAuthAuthorizationServerOptionsProvider {
        OAuthAuthorizationServerOptions GetOptions();
    }

    public class AuthorizationServerOptionsProvider : IOAuthAuthorizationServerOptionsProvider {
        #region Fields

        private readonly IOAuthAuthorizationServerProvider _provider;

        #endregion

        public AuthorizationServerOptionsProvider(IOAuthAuthorizationServerProvider provider) {
            _provider = provider;
        }

        public virtual OAuthAuthorizationServerOptions GetOptions() {
            return new OAuthAuthorizationServerOptions {
                AllowInsecureHttp = true,
#if (DEBUG)
                TokenEndpointPath = new PathString("/api/token"),
#else
                TokenEndpointPath = new PathString("/token"),
#endif
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(60*240),
                Provider = _provider,
                ApplicationCanDisplayErrors = true,
                RefreshTokenProvider = new AuthenticationTokenProvider(),
                AccessTokenFormat = new JWTFormat(),
                RefreshTokenFormat = new JWTFormat(),
                AuthenticationMode = AuthenticationMode.Passive
            };
        }
    }
}