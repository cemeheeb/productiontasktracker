﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EAEConsult.ProductionTaskTracker.Backend.Business.Services;
using EAEConsult.ProductionTaskTracker.Backend.DataAccess;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Entities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Providers {
    using Infrastructure.Services;
    using DataAccess.Repositories;

    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider {
        #region Fields

        private readonly IMembershipService _membershipService;
        private readonly ITicketRouteService _ticketRouteService;
        private readonly IDepartmentService _departmentService;
        private readonly IRepairTypeService _repairTypeService;
        private readonly IMembershipRepository _membershipRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public AuthorizationServerProvider(
            IMembershipService membershipService, 
            ITicketRouteService ticketRouteService, 
            IDepartmentService departmentService,
            IRepairTypeService repairTypeService,
            IMembershipRepository membershipRepository, 
            IWorkPeriodRepository workPeriodRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _membershipService = membershipService;
            _ticketRouteService = ticketRouteService;
            _departmentService = departmentService;
            _repairTypeService = repairTypeService;
            _membershipRepository = membershipRepository;
            _workPeriodRepository = workPeriodRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
            using (_unitOfWorkFactory.Create()) {
                var username = context.Parameters.Get("username");
                var password = context.Parameters.Get("password");
                var domain = context.Parameters.Get("domain");

                if (username == null || password == null) {
                    return Task.FromResult<object>(null);
                }

                Membership membership = null;
                if (!string.IsNullOrEmpty(domain)) {
                    membership = _membershipService.ValidateActiveDirectory(username, password, domain);
                }

                if (membership == null) {
                    membership = _membershipService.ValidateLocal(username, password);
                }

                if (membership == null) {
                    return Task.FromResult<object>(null);
                }

                context.Validated();
                return Task.FromResult<object>(null);
            }
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(context.UserName);
                if (membership == null) {
                    context.Rejected();
                    context.SetError("Доступ ограничен", $"Пользователь с именем {context.UserName} не найден");
                    return Task.FromResult<object>(null);
                }

                var identity = new ClaimsIdentity("JWT");
                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaims(membership.Roles.Select(x => new Claim(ClaimTypes.Role, x.Name)));

                var membershipPosition = _workPeriodRepository.GetByMembershipActive(membership).Position;
                var properties = new AuthenticationProperties(
                    new Dictionary<string, string> {
                        {"membership_id", membership.Id.ToString()},
                        {"department_id", membershipPosition.Department.Id.ToString()},
                        {"position_name", membershipPosition.DisplayName},
                        {"membership_rolenames", JsonConvert.SerializeObject(membership.Roles.Select(x => x.Name))},
                        {"position_rolename", membershipPosition.Role.Name},
                        {"memberships", JsonConvert.SerializeObject(_membershipService.GetAll())},
                        {"departments", JsonConvert.SerializeObject(_departmentService.GetAll())},
                        {"repair_types", JsonConvert.SerializeObject(_repairTypeService.Get())},
                        {"routes", JsonConvert.SerializeObject(_ticketRouteService.GetTicketRouteMapJson(membershipPosition))}
                    });
                context.Validated(new AuthenticationTicket(identity, properties));

                return Task.FromResult<object>(null);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context) {
            foreach (var property in context.Properties.Dictionary) {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}