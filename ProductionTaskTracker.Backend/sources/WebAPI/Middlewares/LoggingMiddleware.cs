﻿using System;
using System.IdentityModel.Tokens;
using System.Security.Authentication;
using System.Threading.Tasks;
using Microsoft.Owin;
using Ninject.Extensions.Logging;
using Owin;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Middlewares {
    public class LoggingMiddleware: OwinMiddleware {
        #region Fields

        private readonly ILogger _logger;

        #endregion

        public LoggingMiddleware(OwinMiddleware next, ILogger logger)
            :base(next) {
            _logger = logger;
        }

        public override async Task Invoke(IOwinContext context) {
            var timestamp = DateTime.Now;
            var unique = Guid.NewGuid().ToString("N").Substring(0, 8);

            try {
                _logger.Debug($"{context.Request.Method} {context.Request.Uri} [{unique}]");

                try {
                    await Next.Invoke(context);
                } catch (SecurityTokenExpiredException exception) {
                    _logger.ErrorException($"{context.Request.Method} [{unique}] Исключение [{(DateTime.Now - timestamp).Milliseconds} мс]", exception);
                    throw new AuthenticationException("Токен аутентификации устарел. Пожалуйста выполните повторный вход в систему.");
                }

                if (context.Response.StatusCode == 401)
                    throw new AuthenticationException("Ошибка доступа");

                _logger.Debug($"{context.Request.Method} [{unique}] Успех [{(DateTime.Now - timestamp).Milliseconds} мс]");
            } catch (Exception exception) {
                _logger.ErrorException($"{context.Request.Method} [{unique}] Исключение [{(DateTime.Now - timestamp).Milliseconds} мс]", exception);
                throw;
            }
        }
    }

    internal static class LoggingMiddlewareHandler {
        public static IAppBuilder UseLoggingMiddleware(this IAppBuilder application, ILogger logger) {
            application.Use<LoggingMiddleware>(logger);
            return application;
        }
    }
}