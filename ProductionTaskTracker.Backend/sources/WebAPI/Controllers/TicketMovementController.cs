﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Web.Http;
using System.Collections.Generic;

using AutoMapper;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using Domain.Entities;
    using Business.Models;
    using Domain.Entities.TicketDetails;
    using Domain.Enumerables;
    using Business;
    using DataAccess.Repositories;
    using DataAccess;
    using DataAccess.Services;
    using Domain.DTO;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class TicketMovementController : ApiController {

        #region Fields

        private readonly ILogger _logger;
        private readonly IRegisterService _registerService;
        private readonly IMembershipRepository _membershipRepository;
        private readonly IClassifierRepository _classifierRepository;
        private readonly ITicketMovementRepository _ticketMovementRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly ITicketRouteRepository _ticketRouteRepository;
        private readonly ITicketVisitRepository _ticketVisitRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public TicketMovementController(
            ILoggerFactory loggerFactory,
            IRegisterService registerService,
            IClassifierRepository classifierRepository,
            IMembershipRepository membershipRepository,
            ITicketRepository ticketRepository,
            ITicketMovementRepository ticketMovementRepository,
            ITicketVisitRepository ticketVisitRepository,
            ITicketRouteRepository ticketRouteRepository,
            IWorkPeriodRepository workPeriodRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _logger = loggerFactory.GetLogger("trace");
            _registerService = registerService;
            _classifierRepository = classifierRepository;
            _membershipRepository = membershipRepository;
            _ticketRepository = ticketRepository;
            _ticketMovementRepository = ticketMovementRepository;
            _ticketMovementRepository = ticketMovementRepository;
            _ticketRouteRepository = ticketRouteRepository;
            _ticketVisitRepository = ticketVisitRepository;
            _workPeriodRepository = workPeriodRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        
        [HttpGet]
        [Route("ticket-movements/{ticket_movement_id}/visits")]
        public IHttpActionResult Visits([FromUri(Name = "ticket_movement_id")]long ticketDetailID) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");
            
                return Json(_ticketVisitRepository
                    .GetByTicketDetailID(ticketDetailID)
                    .OrderBy(x => x.Created)
                    .Select(x => new TicketDetailVisitModel(x)));
            }
        }

        [HttpPost]
        [Route("ticket-movements/visits")]
        public IHttpActionResult VisitsPost(long[] ticketMovementIDList) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                var output = new List<long>();
                foreach (var ticketMovementID in ticketMovementIDList) {
                    var ticketMovement = _ticketMovementRepository.Find(ticketMovementID);
                    _ticketVisitRepository.Create(new TicketVisit {
                        TicketDetail = ticketMovement.TicketDetail,
                        TicketMovement = ticketMovement,
                        Membership = membership,
                        Position = workPeriod.Position,
                        Created = DateTime.Now
                    });

                    output.Add(ticketMovementID);
                }

                unitOfWork.Commit();
                return Json(output);
            }
        }

        [HttpGet]
        [Route("ticket-movements/notifications")]
        public IHttpActionResult GetNotifications() {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    return Json("[]");

                return
                    Json(_ticketMovementRepository.GetNotifications(membership, workPeriod.Position.Department)
                        .Select(Mapper.Map<TicketMovementNotification, TicketMovementNotificationModel>));
            }
        }

        [HttpPost]
        [Route("ticket-movements/group")]
        public IHttpActionResult RegisterGroup([FromBody]TicketMovementRegistrationGroupModel model) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                
                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                try {
                    var parameters = model.Parameters?.ToList();
                    if (parameters != null && parameters.Any()) {
                        foreach (var parameter in parameters) {
                            RegisterInternal(parameter, (TicketStatus) model.Status, workPeriod);
                        }
                    } else {
                        foreach (var ticket in _ticketRepository.FindByGroupID(model.Id)) {
                            RegisterInternal(null, ticket, (TicketStatus)model.Status, workPeriod);
                        }
                    }
                } catch (ApiException exception) {
                    return Json($"{{\"error\": {{ \"code\":{exception.Code} \"message\":{exception.Message}}} }}");
                }


                _registerService.CalculateRegister();
                unitOfWork.Commit();

                return Json("");
            }
        }

        [HttpPost]
        [Route("ticket-movements")]
        public IHttpActionResult Register([FromBody]TicketMovementRegistrationModel model) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                try {
                    RegisterInternal(model, (TicketStatus)model.Status, workPeriod);
                } catch (ApiException exception) {
                    return Json($"{{\"error\": {{ \"code\":{exception.Code} \"message\":{exception.Message}}} }}");
                }


                _registerService.CalculateRegister();
                unitOfWork.Commit();

                return Json("");
            }
        }

        private void RegisterInternal(TicketDetailModel parameter, Ticket ticket, TicketStatus status, WorkPeriod workPeriod) {
            var ticketMovements = ticket.TicketMovements.OrderBy(x => x.Created).ThenBy(x => x.Id);
            var ticketMovementFirst = ticketMovements.First();
            var ticketMovementLast = _ticketMovementRepository.GetLatest(ticket);

            if (status == TicketStatus.Cancelled) {
                if (!Equals(workPeriod.Membership, ticketMovementFirst.Membership)) {
                    throw new ApiException(34, "Снять заявку может только автор заявки");
                }
            } else if (!_ticketRouteRepository.Validate(ticket, workPeriod.Position, ticketMovementLast.Status, status)) {
                throw new ApiException(33, "Операция не поддерживается");
            }

            if (status == TicketStatus.Cancelled && ticketMovementLast.Status != TicketStatus.Cancelled) {
                if (Equals(ticketMovementFirst.Membership, workPeriod.Membership)) {
                    var ticketMovement = _ticketMovementRepository.Create(
                        new TicketMovement {
                            Ticket = ticket,
                            Membership = workPeriod.Membership,
                            Position = workPeriod.Position,
                            Department = ticketMovementFirst.Department,
                            Status = status,
                            TicketDetail = ticketMovementLast.TicketDetail
                        }
                        );

                    _registerService.PendingInclude(ticketMovement.Id);
                }

                return;
            }

            var ticketDetailBefore = ticketMovementLast.TicketDetail;
            var ticketType = Utilities.GetTicketTypeFromEnumerable(ticket.TicketType);

            var mapped = Mapper.Map(parameter, typeof (TicketDetailModel), ticketType) as TicketDetailBase;
            mapped.CopyRequestPropertiesFrom(ticketDetailBefore);
            mapped.Membership = workPeriod.Membership;
            mapped.Ticket = ticket;

            if (status == TicketStatus.ProgressRejected && ticketMovementLast.Status == TicketStatus.Completed) {
                mapped.SetExpiredAtNow();
            }

            var ticketMovementNext = _ticketMovementRepository.Create(
                new TicketMovement {
                    Ticket = ticket,
                    Membership = workPeriod.Membership,
                    Position = workPeriod.Position,
                    Department = _ticketRouteRepository.GetAllowedDepartments(ticket, workPeriod.Position, ticketMovementLast.Status, status).First(),
                    Status = status,
                    TicketDetail = mapped
                });

            _registerService.PendingInclude(ticketMovementNext.Id);
        }
        private void RegisterInternal(TicketDetailModel parameter, TicketStatus status, WorkPeriod workPeriod) {
            var ticket = _ticketRepository.Find(parameter.TicketID.Value);
            RegisterInternal(parameter, ticket, status, workPeriod);
        }
    }
}
