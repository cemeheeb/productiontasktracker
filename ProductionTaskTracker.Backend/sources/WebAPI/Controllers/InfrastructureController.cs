﻿using System.Web.Http;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using DataAccess.Repositories;
    using DataAccess;
    using Infrastructure.Models;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class InfrastructureController : ApiController {
#region Fields

        private readonly IMembershipRepository _membershipRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        
#endregion

        public InfrastructureController(IMembershipRepository membershipRepository, IUnitOfWorkFactory unitOfWorkFactory) {
            _membershipRepository = membershipRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("membership")]
        public IHttpActionResult SessionGetMembership() {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                return Json(new MembershipModel(membership));
            }
        }
    }
}