﻿using System;
using System.Web.Http;

using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using Business.Services;
    using DataAccess;
    using DataAccess.Repositories;
    using Domain.Enumerables;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class PumpController: ApiController {

        #region Fields

        private readonly ILogger _logger;
        private readonly ITicketService _ticketService;
        private readonly IMembershipRepository _membershipRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IPumpService _pumpService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public PumpController(
            ILoggerFactory loggerFactory,
            IMembershipRepository membershipRepository,
            IPumpService pumpService,
            IWorkPeriodRepository workPeriodRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _logger = loggerFactory.GetLogger("trace");
            _membershipRepository = membershipRepository;
            _workPeriodRepository = workPeriodRepository;
            _pumpService = pumpService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("pumps")]
        public IHttpActionResult GetPumps(int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_pumpService.Search(null, index, size));
            }
        }

        [HttpGet]
        [Route("pumps")]
        public IHttpActionResult GetPumps(string search, int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_pumpService.Search(search, index, size));
            }
        }



        [HttpGet]
        [Route("pumps/{id}")]
        public IHttpActionResult GetPump(long id) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_pumpService.Find(id));
            }
        }

        [HttpPost]
        [Route("pumps/import")]
        public IHttpActionResult Import() {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                _pumpService.Import();
                unitOfWork.Commit();
            }

            return Json("");
        }
    }
}