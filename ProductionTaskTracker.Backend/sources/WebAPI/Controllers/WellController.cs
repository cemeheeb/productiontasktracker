﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Web.Http;
using EAEConsult.ProductionTaskTracker.Backend.DataAccess;
using EAEConsult.ProductionTaskTracker.Backend.DataAccess.DictionaryRepositories;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Entities;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using DataAccess.Repositories;
    using Domain.Enumerables;
    using Business.Services;
    using Business.Models;


    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class WellController: ApiController {

        #region Fields

        private readonly IMembershipRepository _membershipRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IWellRegularService _wellRegularService;
        private readonly IWellRegularRepository _wellRegularRepository;
        private readonly IControlledWellRepository _controlledWellRepository;
        private readonly IControlledWellService _controlledWellService;
        private readonly IFieldDictionaryRepository _fieldDictionaryRepository;
        private readonly IShopDictionaryRepository _shopDictionaryRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public WellController(
            IMembershipRepository membershipRepository, 
            IWorkPeriodRepository workPeriodRepository,
            IWellRegularRepository wellRegularRepository,
            IWellRegularService wellRegularService,
            IControlledWellRepository controlledWellRepository,
            IControlledWellService controlledWellService,
            IUnitOfWorkFactory unitOfWorkFactory,
            IFieldDictionaryRepository fieldDictionaryRepository,
            IShopDictionaryRepository shopDictionaryRepository,
            IDepartmentRepository departmentRepository
        ) {
            _membershipRepository = membershipRepository;
            _workPeriodRepository = workPeriodRepository;
            _wellRegularRepository = wellRegularRepository;
            _wellRegularService = wellRegularService;
            _controlledWellRepository = controlledWellRepository;
            _controlledWellService = controlledWellService;
            _unitOfWorkFactory = unitOfWorkFactory;
            _fieldDictionaryRepository = fieldDictionaryRepository;
            _shopDictionaryRepository = shopDictionaryRepository;
            _departmentRepository = departmentRepository;
        }

        [HttpGet]
        [Route("controlled-wells")]
        public IHttpActionResult GetControlledWells(ControlledWellFilter filter, int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                var output = _controlledWellService.GetByFilter(filter, index, size);
                return Json(output);
            }
        }
        
        [HttpPost]
        [Route("controlled-wells")]
        public IHttpActionResult CreateControlledWell(ControlledWellCreateModel model) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                var output = _controlledWellService.CreateControlledWell(membership, workPeriod.Position, model);

                unitOfWork.Commit();
                return Json(output);
            }
        }

        [HttpPut]
        [Route("controlled-wells/{id}")]
        public IHttpActionResult UpdateControlledWell(long id, ControlledWellUpdateModel model) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                var output = _controlledWellService.UpdateControlledWell(id, model);

                unitOfWork.Commit();
                return Json(output);
            }
        }

        [HttpDelete]
        [Route("controlled-wells/{id}")]
        public IHttpActionResult DeleteControlledWell(long id) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                _controlledWellRepository.Delete(_controlledWellRepository.Find(id));

                unitOfWork.Commit();
                return Json("");
            }
        }


        [HttpGet]
        [Route("wells")]
        public IHttpActionResult GetWells([FromUri]WellFilterModel wellFilter) {
            using (_unitOfWorkFactory.Create()) {
                return Json(wellFilter.Departments != null && wellFilter.Departments.Length > 0 ?
                    _wellRegularService.Search(wellFilter.Search, _departmentRepository.GetAll().Where(x => wellFilter.Departments.Contains(x.Id)), wellFilter.Index, wellFilter.Size) :
                    _wellRegularService.Search(wellFilter.Search, wellFilter.Index, wellFilter.Size)
                );
            }
        }

        [HttpPost]
        [Route("wells")]
        public IHttpActionResult PostWell([FromBody]WellModel model) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var output = _wellRegularRepository.Create(new WellRegular(model.Id) {
                    Code = model.Code,
                    Field = _fieldDictionaryRepository.Find(model.Field),
                    Shop = _shopDictionaryRepository.Find(model.Shop),
                    Cluster = model.Cluster
                });

                unitOfWork.Commit();
                return Json(new WellModel(output));
            }
        }

        [HttpGet]
        [Route("wells/{id}")]
        public IHttpActionResult GetWell([FromUri]long id) {
            using (_unitOfWorkFactory.Create()) {
                var wellInformationModel = _wellRegularService.GetWellInformation(id);
                if (wellInformationModel == null) {
                    throw new Exception("Скважина не найдена");
                }

                return Json(wellInformationModel);
            }
        }

        [HttpGet]
        [Route("wells/{id}/history")]
        public IHttpActionResult GetWellHistory([FromUri]long id, [FromUri(Name = "date_begin")]DateTime dateBegin, [FromUri(Name = "date_end")]DateTime dateEnd, [FromUri]int index, [FromUri]int size) {
            using (_unitOfWorkFactory.Create()) {
                var wellHistoryModel = _wellRegularService.GetHistory(id, dateBegin, dateEnd, index, size);
                return Json(wellHistoryModel);
            }
        }
    }
}