﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using DataAccess.Repositories;
    using DataAccess.DictionaryRepositories;
    using DataAccess;
    using Business.Models;
    using Business.Services;
    using Domain.Entities;
    using Domain.Enumerables;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class DictionaryController : ApiController {
        #region Fields

        private readonly IFieldService _fieldService;
        private readonly IDepartmentService _departmentService;
        private readonly IEnterpriseService _enterpriseService;
        private readonly IClassifierService _classifierService;
        private readonly IRepairTypeService _repairTypeService;
        private readonly IInformationService _informationService;
        private readonly ITicketRouteService _ticketRouteService;
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IClassifierRepository _classifierRepository;
        private readonly IFieldDictionaryRepository _fieldRepository;
        private readonly IMeasureFacilityRepository _measureFacilityRepository;
        private readonly IMembershipRepository _membershipRepository;
        private readonly IWellRegularRepository _wellRegularRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public DictionaryController(
            IFieldService fieldService,
            IDepartmentService departmentService,
            IEnterpriseService enterpriseService,
            IClassifierService classifierService, 
            IRepairTypeService repairTypeService, 
            IInformationService informationService, 
            ITicketRouteService ticketRouteService,
            IEnterpriseRepository enterpriseRepository,
            IFieldDictionaryRepository fieldRepository,
            IMembershipRepository membershipRepository, 
            IWellRegularRepository wellRegularRepository, 
            IWorkPeriodRepository workPeriodRepository, 
            IMeasureFacilityRepository measureFacilityRepository,
            IClassifierRepository classifierRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _fieldService = fieldService;
            _departmentService = departmentService;
            _enterpriseService = enterpriseService;
            _classifierService = classifierService;
            _repairTypeService = repairTypeService;
            _informationService = informationService;
            _ticketRouteService = ticketRouteService;
            _enterpriseRepository = enterpriseRepository;
            _fieldRepository = fieldRepository;
            _membershipRepository = membershipRepository;
            _wellRegularRepository = wellRegularRepository;
            _workPeriodRepository = workPeriodRepository;
            _measureFacilityRepository = measureFacilityRepository;
            _classifierRepository = classifierRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        
        [HttpGet]
        [Route("fields")]
        public IHttpActionResult GetFields([FromUri(Name = "measure_facility_filter")]bool measureFacilityFilter = false) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var activeWorkPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (activeWorkPeriod == null) {
                    return Json(new[] {new object()});
                }

                var position = activeWorkPeriod.Position;
                var department = position.Department;

                var shopCodes = _ticketRouteService
                    .GetShopsByDepartment(department)
                    .Select(x => x.Code);

                if (measureFacilityFilter) {
                    return Json(_fieldRepository.GetMeasurePositionFields(shopCodes).Select(field => new FieldModel(field)).ToList());
                }

                return Json(_fieldService.GetAll());
            }
        }

        [HttpGet]
        [Route("clusters")]
        public IHttpActionResult GetClusters([FromUri(Name = "field_code")]string fieldCode) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var activeWorkPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                var position = activeWorkPeriod.Position;
                var department = position.Department;
                var shopCodes = _ticketRouteService.GetShopsByDepartment(department).Select(x => x.Code).ToArray();
                return Json(_wellRegularRepository.GetMeasureFacilityClusters(shopCodes, fieldCode).OrderBy(x => x));
            }
        }
        
        [HttpGet]
        [Route("contractors")]
        public IHttpActionResult GetContractors(string search, int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_enterpriseService.Search(search, index, size));
            }
        }

        [HttpPost]
        [Route("contractors")]
        public IHttpActionResult PostContractors(EnterpriseModel model)
        {
            using (_unitOfWorkFactory.Create())
            {
                return Json(_enterpriseRepository.Create(new Enterprise() {
                    Code = model.Code,
                    Name = model.Name,
                    DisplayName = model.DisplayName
                } ));
            }
        }

        [HttpGet]
        [Route("contractors/{id}")]
        public IHttpActionResult GetContractor(long id) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_enterpriseService.Get(id));
            }
        }

        [HttpGet]
        [Route("classifiers/{dictionary}")]
        public IHttpActionResult GetDictionary([FromUri]string dictionary, int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                int sizeTotal;
                var data = _classifierService.Get(dictionary, index, size, out sizeTotal);
                return Json(new DictionaryModel(dictionary, data) { SizeTotal = sizeTotal });
            }
        }

        [HttpPost]
        [Route("classifiers")]
        public IHttpActionResult PostDictionary(ClassifierModel model)
        {
            using (var unit = _unitOfWorkFactory.Create())
            {
                var entity = _classifierRepository.Create(
                    new Classifier(){
                        Kind = model.Kind,
                        Name = model.Name
                    }
                );

                unit.Commit();
                return Json(new ClassifierModel(entity));
            }
        }

        [HttpPut]
        [Route("classifiers")]
        public IHttpActionResult PutDictionary(ClassifierModel model)
        {
            using (var unit = _unitOfWorkFactory.Create())
            {
                var entity = _classifierRepository.Find(model.Kind, model.Id.Value);
                entity.Name = model.Name;

                _classifierRepository.Update(entity);
                unit.Commit();

                return Json(new ClassifierModel(entity));
            }
        }

        [HttpGet]
        [Route("repair-types")]
        public IHttpActionResult GetRepairTypes() {
            using (_unitOfWorkFactory.Create()) {
                return Json(_repairTypeService.Get());
            }
        }

        [HttpGet]
        [Route("fields/{field_code}")]
        public IHttpActionResult GetField(string field_code) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_fieldService.Get(field_code));
            }
        }

        [HttpGet]
        [Route("fields/{field_code}/clusters/{cluster}/measure-facility-positions")]
        public IHttpActionResult GetMeasureFacilityPositions([FromUri(Name = "field_code")]string fieldCode, string cluster) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var activeWorkPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                var position = activeWorkPeriod.Position;
                var department = position.Department;
                return Json(_measureFacilityRepository.GetPositions(_ticketRouteService.GetShopsByDepartment(department).Select(x => x.Code).ToArray(), fieldCode, cluster));
            }
        }
        
        IEnumerable<string> ExtractShopCodes(Department department) {
            var output = new List<string>();

            if (department.Shop != null)
                output.Add(department.Shop.Code);

            foreach (var child in department.Children) {
                output.AddRange(ExtractShopCodes(child));
            }

            return output;
        }
    }
}