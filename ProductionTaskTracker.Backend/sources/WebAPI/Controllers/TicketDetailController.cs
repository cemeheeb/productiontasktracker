﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;

using AutoMapper;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using System.Collections.Generic;
    using System.Security.Authentication;
    using Domain.Enumerables;
    using Domain.Entities;
    using Business.Models;
    using Business.Services;
    using DataAccess;
    using DataAccess.Repositories;
    using DataAccess.Services;
    using Infrastructure.Services;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class TicketDetailController : ApiController {

        #region Fields

        private readonly ILogger _logger;
        private readonly IRegisterService _registerService;
        private readonly IMembershipRepository _membershipRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly ITicketMovementRepository _ticketMovementRepository;
        private readonly ITicketDetailRepository _ticketDetailRepository;
        private readonly ITicketVisitRepository _ticketVisitRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public TicketDetailController(
            ILoggerFactory loggerFactory,
            IRegisterService registerService,
            IMembershipRepository membershipRepository,
            ITicketRepository ticketRepository,
            ITicketMovementRepository ticketMovementRepository,
            ITicketDetailRepository ticketDetailRepository,
            ITicketVisitRepository ticketVisitRepository,
            IWorkPeriodRepository workPeriodRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _logger = loggerFactory.GetLogger("trace");
            _registerService = registerService;
            _membershipRepository = membershipRepository;
            _ticketRepository = ticketRepository;
            _ticketMovementRepository = ticketMovementRepository;
            _ticketDetailRepository = ticketDetailRepository;
            _ticketVisitRepository = ticketVisitRepository;
            _workPeriodRepository = workPeriodRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }


        [HttpGet]
        [Route("ticket-details/{ticket_detail_id}")]
        public IHttpActionResult GetTicketDetail([FromUri(Name = "ticket_detail_id")]long ticketDetailID) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                var ticketDetail = _ticketDetailRepository.Find(ticketDetailID);
                var ticketMovements = ticketDetail.Ticket.TicketMovements.OrderBy(x => x.Id);
                var ticketMovementLast = ticketMovements.Last();

                var ticketDetailModel = (TicketDetailModel)Mapper.Map(ticketDetail, ticketDetail.GetType(), typeof(TicketDetailModel));
                ticketDetailModel.DepartmentID = ticketDetail.Ticket.Department.Id;
                ticketDetailModel.Status = (int)ticketMovementLast.Status;
                ticketDetailModel.IsNight = ticketDetail.Ticket.IsNight;

                if ((workPeriod.Position.Role.Name.EndsWith("cdng") && ticketDetail.Ticket.TicketType == TicketType.G && ticketMovementLast.Status != TicketStatus.Visited) ||
                    (workPeriod.Position.Role.Name.EndsWith("cio") && ticketDetail.Ticket.TicketType == TicketType.F && ticketMovementLast.Status != TicketStatus.Visited)) {
                    var ticketMovement = _ticketMovementRepository.Create(new TicketMovement {
                        Ticket = ticketDetail.Ticket,
                        TicketDetail = ticketDetail,
                        Department = workPeriod.Position.Department,
                        Position = workPeriod.Position,
                        Membership = membership,
                        Status = TicketStatus.Visited,
                        Created = DateTime.Now
                    });
                    _registerService.PendingInclude(ticketMovement.Id);
                }

                _ticketVisitRepository.Create(new TicketVisit {
                    TicketDetail = ticketMovementLast.TicketDetail,
                    TicketMovement = ticketMovementLast,
                    Membership = membership,
                    Position = workPeriod.Position,
                    IsViewed = true,
                    Created = DateTime.Now
                });

                unitOfWork.Commit();
                return Json(ticketDetailModel);
            }
        }

        [HttpGet]
        [Route("ticket-details/groups/{ticket_group_id}")]
        public IHttpActionResult GetTicketDetailGroup([FromUri(Name = "ticket_group_id")]long ticketGroupID) {
            using (var unitOfWork = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                var tickets = _ticketRepository.FindByGroupID(ticketGroupID).ToList();

                var output = new TicketGroupModel {
                    ID = ticketGroupID,
                    Wells = new List<WellModel>(),
                    Parameters = new List<TicketDetailModel>()
                };

                foreach (var ticket in tickets) {
                    var ticketMovements = ticket.TicketMovements.OrderBy(x => x.Id);
                    var ticketMovementLast = ticketMovements.Last();
                    var ticketDetail = ticketMovementLast.TicketDetail;

                    var detailTarget = (TicketDetailModel)Mapper.Map(ticketDetail, ticketDetail.GetType(), typeof(TicketDetailModel));
                    detailTarget.DepartmentID = ticketDetail.Ticket.Department.Id;
                    detailTarget.Status = (int)ticketMovementLast.Status;

                    output.TicketType = ticket.TicketType;
                    output.DepartmentID = ticket.Department.Id;
                    output.Status = (int)ticketMovementLast.Status;
                    output.Wells.Add(new WellModel(ticketMovementLast.TicketDetail.Well));
                    output.Parameters.Add(detailTarget);
                    output.SL = ticketMovementLast.TicketDetail.SL;
                    output.IsNight = ticket.IsNight;

                    if ((workPeriod.Position.Role.Name.EndsWith("cdng") && ticketDetail.Ticket.TicketType == TicketType.G && detailTarget.Status != (int)TicketStatus.Visited) ||
                        (workPeriod.Position.Role.Name.EndsWith("cio") && ticketDetail.Ticket.TicketType == TicketType.F && detailTarget.Status != (int)TicketStatus.Visited)) {
                        var ticketMovement = _ticketMovementRepository.Create(new TicketMovement {
                            Ticket = ticketDetail.Ticket,
                            TicketDetail = ticketDetail,
                            Department = workPeriod.Position.Department,
                            Position = workPeriod.Position,
                            Membership = membership,
                            Status = TicketStatus.Visited,
                            Created = DateTime.Now
                        });
                        _registerService.PendingInclude(ticketMovement.Id);
                    }

                    _ticketVisitRepository.Create(new TicketVisit {
                        TicketDetail = ticketDetail,
                        TicketMovement = ticketMovementLast,
                        Membership = membership,
                        Position = workPeriod.Position,
                        IsViewed = true,
                        Created = DateTime.Now
                    });
                }

                unitOfWork.Commit();
                return Json(output);
            }
        }
    }
}
