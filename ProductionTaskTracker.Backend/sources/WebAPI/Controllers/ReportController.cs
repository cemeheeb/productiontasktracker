﻿using System;
using System.IO;
using System.Web.Http;
using EAEConsult.ProductionTaskTracker.Backend.Business.Models;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using Infrastructure.Services;
    using DataAccess.Repositories;
    using DataAccess;
    using Reports.Services;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class ReportController : ApiController {

        #region Fields

        private readonly IMembershipRepository _membershipRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IReportService _reportService;
        private readonly IFilesystemService _filesystemService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public ReportController(
            IMembershipRepository membershipRepository,
            IWorkPeriodRepository workPeriodRepository,
            IReportService reportService,
            IFilesystemService filesystemService,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _membershipRepository = membershipRepository;
            _workPeriodRepository = workPeriodRepository;
            _reportService = reportService;
            _filesystemService = filesystemService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("reports/reportA")]
        public IHttpActionResult BuildReportA([FromUri]ReportAModel reportA) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    return null;

                const string filename = "Отчет.Тип.1.xls";
                var uid = Guid.NewGuid().ToString("N");

                var outputFilename = Path.Combine(_filesystemService.GetMembershipReportPath(membership, uid), filename);
                _reportService.ReportA(outputFilename, reportA.date_begin, reportA.date_end, reportA.departments, reportA.wells, reportA.ticket_types, reportA.sl, reportA.show_expired??false);

                return Json(new Uri(_filesystemService.GetMembershipReportUri(membership, uid) + "/" + filename));
            }
        }
    }
}