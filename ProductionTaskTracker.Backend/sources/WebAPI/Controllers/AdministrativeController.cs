﻿using System.Web.Http;


namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using DataAccess;
    using Business.Models;
    using Infrastructure.Services;
    using Business.Services;
    using Infrastructure.Models;
    using System.Net.Http;
    using Newtonsoft.Json.Linq;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class AdministrativeController : ApiController {
        #region Fields

        private readonly IDepartmentService _departmentService;
        private readonly IEnterpriseService _enterpriseService;
        private readonly IInformationService _informationService;
        private readonly IMembershipService _membershipService;
        private readonly IPositionService _positionService;
        private readonly IWorkPeriodService _workPeriodService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public AdministrativeController(
            IDepartmentService departmentService,
            IEnterpriseService enterpriseService,
            IInformationService informationService,
            IMembershipService membershipService,
            IPositionService positionService, 
            IWorkPeriodService workPeriodService,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _departmentService = departmentService;
            _enterpriseService = enterpriseService;
            _informationService = informationService;
            _membershipService = membershipService;
            _positionService = positionService;
            _workPeriodService = workPeriodService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("administrative/memberships")]
        public IHttpActionResult GetMemberships([FromUri(Name = "index")]int index, [FromUri(Name = "size")]int size) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_membershipService.Search(index, size));
            }
        }

        [HttpPost]
        [Route("administrative/memberships")]
        public IHttpActionResult PostMemberships([FromBody]MembershipModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var output = _membershipService.Create(model);
                unit.Commit();
                return Json(output);
            }
        }

        [HttpPut]
        [Route("administrative/memberships")]
        public IHttpActionResult PutMemberships([FromBody]MembershipModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var output = _membershipService.Update(model);
                unit.Commit();
                return Json(output);
            }
        }

        [HttpGet]
        [Route("administrative/departments")]
        public IHttpActionResult GetDepartments() {
            using (_unitOfWorkFactory.Create()) {
                return Json(_departmentService.GetAll());
            }
        }

        [HttpGet]
        [Route("administrative/departments/{id}")]
        public IHttpActionResult GetDepartment(long id) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_informationService.GetDepartment(id));
            }
        }

        [HttpPut]
        [Route("administrative/departments/{id}")]
        public IHttpActionResult UpdateDepartment([FromUri]long id, [FromBody]DepartmentModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                _departmentService.Update(model);
                unit.Commit();

                return Json(model);
            }
        }

        [HttpDelete]
        [Route("administrative/departments/{id}")]
        public IHttpActionResult DeleteDepartment(long id) {
            using (var unit = _unitOfWorkFactory.Create()) {
                _departmentService.Remove(id);
                unit.Commit();

                return Ok();
            }
        }

        [HttpPost]
        [Route("administrative/departments")]
        public IHttpActionResult PostDepartment(DepartmentModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var output = _departmentService.Create(model);
                unit.Commit();
                return Json(output);
            }
        }

        [HttpGet]
        [Route("contractors")]
        public IHttpActionResult GetContractors(int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                return Json(_enterpriseService.Search(index, size));
            }
        }

        [HttpGet]
        [Route("administrative/positions")]
        public IHttpActionResult GetPositions() {
            using (_unitOfWorkFactory.Create()) {
                return Json(_positionService.GetAll());
            }
        }

        [HttpPost]
        [Route("administrative/positions")]
        public IHttpActionResult PostPositions(PositionModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var output = _positionService.Create(model);
                unit.Commit();
                return Json(output);
            }
        }

        [HttpPut]
        [Route("administrative/positions/{id}")]
        public IHttpActionResult UpdatePosition([FromUri]long id, [FromBody]PositionModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                _positionService.Update(model);
                unit.Commit();

                return Json(model);
            }
        }

        [HttpDelete]
        [Route("administrative/positions/{id}")]
        public IHttpActionResult RemovePosition([FromUri]long id) {
            using (var unit = _unitOfWorkFactory.Create()) {
                _positionService.Remove(id);
                unit.Commit();
                return Json(new JObject());
            }
        }

        [HttpGet]
        [Route("administrative/work-periods")]
        public IHttpActionResult GetWorkPeriods() {
            using (_unitOfWorkFactory.Create()) {
                var output = _workPeriodService.GetActivePeriods();
                return Json(output);
            }
        }

        [HttpPost]
        [Route("administrative/work-periods")]
        public IHttpActionResult PostWorkPeriod(WorkPeriodBeginModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                _workPeriodService.BeginWorkPeriod(model);
                unit.Commit();
            }

            return Json(new JObject());
        }

        [HttpDelete]
        [Route("administrative/work-periods/{id}")]
        public IHttpActionResult DeleteWorkPeriod([FromUri]long id) {
            using (var unit = _unitOfWorkFactory.Create()) {
                _workPeriodService.EndWorkPeriod(id);
                unit.Commit();
            }

            return Json(new JObject());
        }
    }
}