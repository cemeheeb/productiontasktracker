﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Web.Http;
using Ninject.Extensions.Logging;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web;
    using Domain.Enumerables;

    using Business.Services;
    using Business.Models;

    using DataAccess.Repositories;
    using DataAccess;
    using DataAccess.Services;
    using Infrastructure.Services;
    using Domain.Entities.TicketDetails;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class TicketController : ApiController {

        #region Fields

        private readonly ILogger _logger;
        private readonly IRegisterService _registerService;
        private readonly ITicketService _ticketService;
        private readonly ITicketImportService _ticketImportService;
        private readonly IFilesystemService _filesystemService;
        private readonly IMembershipRepository _membershipRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly ITicketDetailRepository _ticketDetailRepository;
        private readonly ITicketVisitRepository _ticketVisitRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IWellRegularRepository _wellRegularRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public TicketController(
            ILoggerFactory loggerFactory,
            IRegisterService registerService,
            ITicketService ticketService,
            ITicketImportService ticketImportService,
            IFilesystemService filesystemService,
            IMembershipRepository membershipRepository,
            ITicketRepository ticketRepository,
            ITicketDetailRepository ticketDetailRepository,
            ITicketVisitRepository ticketVisitRepository,
            IDepartmentRepository departmentRepository,
            IWellRegularRepository wellRegularRepository,
            IWorkPeriodRepository workPeriodRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _logger = loggerFactory.GetLogger("trace");
            _registerService = registerService;
            _ticketService = ticketService;
            _ticketImportService = ticketImportService;
            _filesystemService = filesystemService;
            _membershipRepository = membershipRepository;
            _ticketRepository = ticketRepository;
            _ticketDetailRepository = ticketDetailRepository;
            _ticketVisitRepository = ticketVisitRepository;
            _departmentRepository = departmentRepository;
            _wellRegularRepository = wellRegularRepository;
            _workPeriodRepository = workPeriodRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("tickets")]
        public IHttpActionResult GetTickets(string kind, string filter, DateTime date_begin, DateTime date_end, int index, int size) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    return null;
                
                switch (kind) {
                    case "incoming":
                        return Json(_ticketService.GetIncoming(HttpUtility.UrlDecode(filter), membership, workPeriod.Position.Department, date_begin, date_end, index, size));
                    case "outcoming":
                        return Json(_ticketService.GetOutcoming(HttpUtility.UrlDecode(filter), membership, workPeriod.Position.Department.Head, date_begin, date_end, index, size));
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        [HttpPost]
        [Route("tickets")]
        public IHttpActionResult RegisterTicket(TicketRegistrationModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                if (model.TicketType == TicketType.B10 &&
                    (string.IsNullOrEmpty(model.FieldCode) || string.IsNullOrEmpty(model.Cluster) ||
                     !model.MeasureFacilityPosition.HasValue))
                    throw new ArgumentNullException(nameof(model));

                var output = _ticketService.RegisterTicket(membership, model);
                unit.Commit();

                return Json(output);
            }
        }

        [HttpPost]
        [Route("tickets/group")]
        public IHttpActionResult RegisterTicketGroup(TicketRegistrationGroupModel model) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var output = _ticketService.RegisterTicketGroup(membership, model);
                unit.Commit();

                return Json(output);
            }
        }

        [HttpGet]
        [Route("tickets/import")]
        public IHttpActionResult TicketImport([FromUri]TicketImportModel model) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    return Json("");

                var uid = Guid.NewGuid().ToString("N");
                var path = _filesystemService.GetMembershipImportPath(membership, uid);
                var filename = "blank.xls";
                var department = _departmentRepository.Find(model.department_id);

                switch (model.ticket_type) {
                    case TicketType.B1B: {
                            var ticketDetails = _ticketDetailRepository.Find(model.ticket_detail_ids).ToList();
                            if (ticketDetails.Count > 0 && ((TicketDetailB1B)ticketDetails[0]).Treatment == TreatmentType.WellRepair) {
                                _ticketImportService.GenerateDirectiveVNRBlank(
                                    Path.Combine(path, filename),
                                    model.ticket_type,
                                    department,
                                    model.sl,
                                    model.is_night,
                                    ticketDetails
                                );
                            } else {
                                _ticketImportService.GenerateUniversalBlank(
                                    Path.Combine(path, filename),
                                    model.ticket_type,
                                    department,
                                    model.sl,
                                    model.is_night,
                                    _ticketDetailRepository.Find(model.ticket_detail_ids)
                                );
                            }
                            break;
                        }
                    case TicketType.B2: {
                            _ticketImportService.GenerateDirective354Blank(
                            Path.Combine(path, filename),
                            model.ticket_type,
                            department,
                            model.sl,
                            model.is_night,
                            _ticketDetailRepository.Find(model.ticket_detail_ids)
                        );
                            break;
                        }
                    default: {
                            _ticketImportService.GenerateUniversalBlank(
                                Path.Combine(path, filename),
                                model.ticket_type,
                                department,
                                model.sl,
                                model.is_night,
                                _ticketDetailRepository.Find(model.ticket_detail_ids)
                            );
                            break;
                        }
                }

                var uri = new Uri(_filesystemService.GetMembershipImportUri(membership, uid), filename);
                return Json(uri);
            }
        }


        [HttpPost]
        [Route("tickets/import")]
        public async Task<ImportFileModel> TicketImport([FromUri(Name = "uid")] string uid) {
            if (!Request.Content.IsMimeMultipartContent()) {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            Guid guid;
            if (Guid.TryParse(uid, out guid)) {
                throw new HttpException("UID не найден");
            }
            var validatedUID = guid.ToString("N");
            var membership = _membershipRepository.FindByUserName(User.Identity.Name);
            var path = _filesystemService.GetMembershipImportPath(membership, validatedUID);
            var provider = new MultipartFormDataStreamProvider(path);
            await Request.Content.ReadAsMultipartAsync(provider);

            // Добавляем новые
            var file = provider.FileData[0];
            var fileInfo = new FileInfo(file.LocalFileName);
            var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
            File.Move(fileInfo.FullName, Path.Combine(path, fileName));
            return new ImportFileModel { FileName = fileName, FileSize = fileInfo.Length, ImportID = validatedUID, Uri = _filesystemService.GetMembershipImportUri(membership, validatedUID) + fileName };
        }

        [HttpGet]
        [Route("tickets/{id}/history")]
        public IHttpActionResult TicketHistory([FromUri(Name="id")]long ticketID) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var ticket = _ticketRepository.Find(ticketID);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                var ticketMovements = ticket
                    .TicketMovements
                    .OrderBy(x => x.Created)
                    .ThenBy(x => x.Id)
                    .ToList();
        
                return Json(
                    ticketMovements
                    .Select(x => new TicketHistoryModel(x,
                        _ticketVisitRepository
                            .GetByTicketDetailID(x.TicketDetail.Id)
                            .Select(y => new TicketDetailVisitModel(y))
                        )
                    ).ToList()
                );
            }
        }


        [HttpGet]
        [Route("ticket-groups/{id}/history")]
        public IHttpActionResult TicketGroupHistory([FromUri(Name = "id")]long ticketGroupID) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    throw new AuthenticationException("Истек срок действия аккаунта");

                var output = new List<TicketHistoryContainerModel>();
                foreach (var ticket in _ticketRepository.FindByGroupID(ticketGroupID)) {
                    var ticketMovements = ticket
                        .TicketMovements
                        .OrderBy(x => x.Created)
                        .ThenBy(x => x.Id)
                        .ToList();

                    output.Add(new TicketHistoryContainerModel {
                        TicketID = ticket.Id,
                        TicketHistoryModels = ticketMovements
                            .Select(x => new TicketHistoryModel(x,
                                _ticketVisitRepository
                                    .GetByTicketDetailID(x.TicketDetail.Id)
                                    .Select(y => new TicketDetailVisitModel(y))
                                )
                            ).ToList()
                    }
                    );
                }

                return Json(output);
            }
        }
    }
}
