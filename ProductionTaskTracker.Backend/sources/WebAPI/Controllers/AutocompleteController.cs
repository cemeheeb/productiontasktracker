﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using DataAccess.Repositories;
    using DataAccess.DictionaryRepositories;
    using DataAccess;
    using Business.Models;
    using Business.Services;
    using Domain.Entities;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class AutocompleteController : ApiController {
        #region Fields

        private readonly IMembershipRepository _membershipRepository;
        private readonly IAutocompleteOptionService _autocompleteOptionService;
        private readonly IAutocompleteOptionRepository _autocompleteOptionRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public AutocompleteController(
            IMembershipRepository membershipRepository,
            IAutocompleteOptionService autocompleteOptionService,
            IAutocompleteOptionRepository autocompleteOptionRepository,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _membershipRepository = membershipRepository;
            _autocompleteOptionService = autocompleteOptionService;
            _autocompleteOptionRepository = autocompleteOptionRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        
        [HttpGet]
        [Route("autocompletes")]
        public IHttpActionResult Filter([FromUri(Name = "membership_id")]long membershipID, [FromUri(Name = "parameter")]string parameter) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                if (membership != null) {
                    return Json(_autocompleteOptionService.Filter(membership, parameter));
                }

                return Json("");
            }
        }

        [HttpDelete]
        [Route("autocompletes")]
        public IHttpActionResult Remove([FromUri(Name = "membership_id")]long membershipID, [FromUri(Name = "parameter")]string parameter, [FromUri(Name = "text")]string text) {
            using (var unit = _unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);
                if (membership != null) {
                    _autocompleteOptionRepository.Remove(membership, parameter, text);
                }

                unit.Commit();
                return Json("");
            }
        }
    }
}