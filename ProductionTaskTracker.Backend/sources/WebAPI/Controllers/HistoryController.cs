﻿using System;
using System.Web.Http;
using EAEConsult.ProductionTaskTracker.Backend.DataAccess;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {
    using Business.Services;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class HistoryController : ApiController {
        #region Fields

        private readonly IHistoryService _historyService;
        private readonly IWellRegularService _wellRegularService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public HistoryController(IHistoryService historyService, IWellRegularService wellRegularService, IUnitOfWorkFactory unitOfWorkFactory) {
            _historyService = historyService;
            _wellRegularService = wellRegularService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("history/repairs")]
        public IHttpActionResult GetHistory(string field, string cluster, string well) {
            field = field.Trim();
            cluster = cluster.Trim();
            well = well.Trim();
            return Json(_historyService.GetHistory(field, cluster, well));
        }

        [HttpGet]
        [Route("history/repairs")]
        public IHttpActionResult GetHistoryBetween(string field, string cluster, string well, DateTime date_start, DateTime date_end) {
            field = field.Trim();
            cluster = cluster.Trim();
            well = well.Trim();
            return Json(_historyService.GetHistoryBetween(field, cluster, well, date_start, date_end));
        }

        [HttpGet]
        [Route("history/repairs")]
        public IHttpActionResult GetHistoryByRemoteWellID(string remote_well_id) {
            return Json(_historyService.GetHistoryByRemoteWellID(remote_well_id));
        }

        [HttpGet]
        [Route("history/repairs")]
        public IHttpActionResult GetHistoryBetweenByRemoteWellID(string remote_well_id, DateTime date_start, DateTime date_end) {
            return Json(_historyService.GetHistoryBetweenByRemoteWellID(remote_well_id, date_start, date_end));
        }
    }
}