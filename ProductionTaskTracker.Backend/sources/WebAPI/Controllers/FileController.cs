﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Results;
using EAEConsult.ProductionTaskTracker.Backend.Business.Models;
using EAEConsult.ProductionTaskTracker.Backend.DataAccess;
using EAEConsult.ProductionTaskTracker.Backend.Domain.Entities.TicketDetails;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;


namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Controllers {

    using Infrastructure.Services;
    using DataAccess.Repositories;

    [Authorize]
#if (DEBUG)
    [RoutePrefix("api")]
#endif
    public class FileController : ApiController {

        #region Fields

        private readonly ILogger _logger;
        private readonly IMembershipRepository _membershipRepository;
        private readonly IWorkPeriodRepository _workPeriodRepository;
        private readonly ITicketDetailRepository _ticketDetailRepository;
        private readonly IFilesystemService _filesystemService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        #endregion

        public FileController(
            ILoggerFactory loggerFactory,
            IMembershipRepository membershipRepository,
            IWorkPeriodRepository workPeriodRepository,
            ITicketDetailRepository ticketDetailRepository,
            IFilesystemService filesystemService,
            IUnitOfWorkFactory unitOfWorkFactory
        ) {
            _logger = loggerFactory.GetLogger("trace");
            _membershipRepository = membershipRepository;
            _workPeriodRepository = workPeriodRepository;
            _ticketDetailRepository = ticketDetailRepository;
            _filesystemService = filesystemService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet]
        [Route("ticket-details/{ticket_detail_id}/attachments")]
        public IHttpActionResult TicketDetailAttachmentGet([FromUri(Name = "ticket_detail_id")]long ticketDetailID) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    return null;

                var ticketDetail = _ticketDetailRepository.Find(ticketDetailID);

                if (string.IsNullOrEmpty(ticketDetail.AttachmentID)) {
                    return Json(new List<AttachmentFileModel>());
                }

                var attachmentPath = Path.Combine(_filesystemService.GetAttachmentPath(), ticketDetail.AttachmentID);
                var output = Directory
                    .EnumerateFiles(attachmentPath)
                    .Select(
                        x =>
                            new AttachmentFileModel {
                                AttachmentID = ticketDetail.AttachmentID,
                                FileName = Path.GetFileName(x),
                                FileSize = new FileInfo(Path.Combine(attachmentPath, x)).Length,
                                Uri = _filesystemService.GetAttachmentUri() + $"{ticketDetail.AttachmentID}/{Path.GetFileName(x)}"
                            });

                return Json(output);
            }
        }

        [HttpPost]
        [Route("ticket-details/{ticket_detail_id}/attachments")]
        public async Task<IList<AttachmentFileModel>> TicketDetailAttachmentPost([FromUri(Name = "ticket_detail_id")]long ticketDetailID) {
            if (!Request.Content.IsMimeMultipartContent()) {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            using (_unitOfWorkFactory.Create()) {
                // Получение файлов прикрепленных к детали
                var ticketDetail = _ticketDetailRepository.Find(ticketDetailID);
                
                // Генерируем новый идентификатор
                var attachmentID = Guid.NewGuid().ToString();
                var attachmentPathAfter = Path.Combine(_filesystemService.GetAttachmentPath(), attachmentID);

                // Создаем для него директорию
                if (!Directory.Exists(attachmentPathAfter)) {
                    Directory.CreateDirectory(attachmentPathAfter);
                }

                var output = new List<AttachmentFileModel>();

                // Копируем ранее прикрепленные документы
                if (ticketDetail.AttachmentID != null) {
                    var attachmentPathBefore = Path.Combine(_filesystemService.GetAttachmentPath(),
                        ticketDetail.AttachmentID);

                    var attachmentsBefore = Directory
                        .EnumerateFiles(attachmentPathBefore);

                    foreach (var attachment in attachmentsBefore) {
                        var fileName = Path.GetFileName(attachment);
                        if (fileName == null) {
                            continue;
                        }

                        File.Copy(attachment, Path.Combine(attachmentPathAfter, fileName));

                        var fileInfo = new FileInfo(attachment);
                        output.Add(new AttachmentFileModel {
                            FileName = fileName,
                            FileSize = fileInfo.Length,
                            AttachmentID = attachmentID,
                            Uri = _filesystemService.GetAttachmentUri() + $"{attachmentID}/{fileName}"
                        });
                    }
                }

                var provider = new MultipartFormDataStreamProvider(attachmentPathAfter);
                await Request.Content.ReadAsMultipartAsync(provider);

                // Добавляем новые
                foreach (var file in provider.FileData) {
                    var fileInfo = new FileInfo(file.LocalFileName);
                    var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    var fileSize = fileInfo.Length;

                    //В случае если файл с таким именем уже был добавлен, то ему присваивается дополнительный индекс
                    var i = 0;
                    var newFileName = fileName;
                    while (output.Exists(x => x.FileName == newFileName)) {
                        newFileName = $"{fileName}_{i++}";
                    }

                    File.Move(fileInfo.FullName, Path.Combine(attachmentPathAfter, newFileName));

                    output.Add(new AttachmentFileModel {
                        FileName = newFileName,
                        FileSize = fileSize,
                        AttachmentID = attachmentID,
                        Uri = _filesystemService.GetAttachmentUri() + $"{attachmentID}/{newFileName}"
                    });
                }

                return output;
            }
        }

        [HttpGet]
        [Route("attachments/{attachment_id}")]
        public IHttpActionResult AttachmentsGet([FromUri(Name = "attachment_id")]string attachmentID) {
            using (_unitOfWorkFactory.Create()) {
                var membership = _membershipRepository.FindByUserName(User.Identity.Name);

                var workPeriod = _workPeriodRepository.GetByMembershipActive(membership);
                if (workPeriod == null)
                    return null;

                if (string.IsNullOrEmpty(attachmentID)) {
                    return Json(new List<AttachmentFileModel>());
                }

                var attachmentPath = Path.Combine(_filesystemService.GetAttachmentPath(), attachmentID);
                if (!Directory.Exists(attachmentPath))
                    return Json(new List<AttachmentFileModel>());

                var output = Directory
                    .EnumerateFiles(attachmentPath)
                    .Select(
                        x =>
                            new AttachmentFileModel {
                                AttachmentID = attachmentID,
                                FileName = Path.GetFileName(x),
                                FileSize = new FileInfo(Path.Combine(attachmentPath, x)).Length,
                                Uri = _filesystemService.GetAttachmentUri() + $"{attachmentID}/{Path.GetFileName(x)}"
                            });

                return Json(output);
            }
        }

        [HttpPost]
        [Route("attachments")]
        public async Task<IList<AttachmentFileModel>> Post() {
            if (!Request.Content.IsMimeMultipartContent()) {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var attachmentID = Guid.NewGuid().ToString();
            var attachmentPath = Path.Combine(_filesystemService.GetAttachmentPath(), attachmentID);
            if (!Directory.Exists(attachmentPath)) {
                Directory.CreateDirectory(attachmentPath);
            }

            var provider = new MultipartFormDataStreamProvider(attachmentPath);
            var parts = await Request.Content.ReadAsMultipartAsync(provider);

            var output = new List<AttachmentFileModel>();
            foreach (var file in provider.FileData) {
                var fileInfo = new FileInfo(file.LocalFileName);
                var fileName = Path.GetFileName(file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty));
                var fileSize = fileInfo.Length;
                File.Move(fileInfo.FullName, Path.Combine(attachmentPath, fileName));
                output.Add(new AttachmentFileModel { FileName = fileName, FileSize = fileSize, AttachmentID = attachmentID, Uri = _filesystemService.GetAttachmentUri() + $"{attachmentID}/{fileName}"});
            }

            return output;
        }
    }
}
