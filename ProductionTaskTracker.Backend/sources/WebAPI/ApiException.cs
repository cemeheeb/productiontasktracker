﻿using System;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI {
    public class ApiException: Exception {
        public ApiException(int code) {
            Code = code;
        }

        public ApiException(int code, string message) : base(message) {
            Code = code;
        }

        public int Code { get; set; }
    }
}