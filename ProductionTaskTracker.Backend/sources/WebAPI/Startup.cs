﻿using System;
using System.Configuration;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Configuration;
using System.Web.Http;
using EAEConsult.ProductionTaskTracker.Backend.DataAccess.NHibernate;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json;
using Ninject;
using Ninject.Extensions.Logging;
using Ninject.Web.Common;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

[assembly: OwinStartup(typeof(EAEConsult.ProductionTaskTracker.Backend.WebAPI.Startup))]

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI {
    using Middlewares;
    using DataAccess;
    using Providers;
    using System.Web;

    public class Startup {
        public void Configuration(IAppBuilder application) {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings =
                new JsonSerializerSettings {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                };

            // Настройка инъекции зависимостей
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            RegisterServices(kernel);

            var loggerFactory = kernel.Get<ILoggerFactory>();

            try {
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                application.UseNinjectMiddleware(() => kernel);

                // Настройка авторизации
                application.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            
                var webApiConfiguration = new HttpConfiguration();
                webApiConfiguration.Filters.Add(new HostAuthenticationFilter("Bearer"));
                webApiConfiguration.Filters.Add(new ExceptionLoggerFilter(loggerFactory));
                webApiConfiguration.MapHttpAttributeRoutes();

                var issuer = WebConfigurationManager.AppSettings["issuer"];
                var secret = TextEncodings.Base64Url.Decode(WebConfigurationManager.AppSettings["secret"]);

                application
                    .UseJwtBearerAuthentication(
                        new JwtBearerAuthenticationOptions {
                            AuthenticationType = "Bearer",
                            AllowedAudiences = new[] { "any" },
                            AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
                            IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[] { new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret) }
                        }
                    )
                    .UseOAuthAuthorizationServer(kernel.Get<IOAuthAuthorizationServerOptionsProvider>().GetOptions())
                    .UseNinjectWebApi(webApiConfiguration);

                // Статические файлы
                var absolutePath = System.Web.Hosting.HostingEnvironment.MapPath($"~/{ConfigurationManager.AppSettings["RootDirectoryPath"]}");
                System.IO.Directory.CreateDirectory(absolutePath);

                var physicalFileSystem = new PhysicalFileSystem($"./{ConfigurationManager.AppSettings["RootDirectoryPath"]}");
                var options = new FileServerOptions
                {
                    EnableDirectoryBrowsing = false,
                    EnableDefaultFiles = true,
                    FileSystem = physicalFileSystem,
                    StaticFileOptions = {
                        FileSystem = physicalFileSystem,
                        RequestPath = PathString.FromUriComponent("/static"),
                        ServeUnknownFileTypes = true,
                    }
                };
                application.UseFileServer(options);

                // Логирование запросов
                application.UseLoggingMiddleware(loggerFactory.GetLogger("file"));
            } catch (Exception exception) {
                kernel.Get<ILoggerFactory>().GetLogger("trace").FatalException("Процедура конфигурации системы вызвала исключение", exception);
                throw;
            }
        }

        private void RegisterServices(IKernel kernel) {
            kernel.Load(new DataAccess.NHibernate.Module());
            kernel.Load(new Infrastructure.Module());
            kernel.Load(new Business.Module());
            kernel.Load(new Reports.Module());

            kernel.Bind<ISessionProvider>().To<SessionProvider>().InSingletonScope();
            kernel.Bind<IUnitOfWorkFactory>().To<UnitOfWorkFactory>().InSingletonScope();
            kernel.Bind<IOAuthAuthorizationServerProvider>().To<AuthorizationServerProvider>().InRequestScope();
            kernel.Bind<IOAuthAuthorizationServerOptionsProvider>().To<AuthorizationServerOptionsProvider>().InRequestScope();
        }
    }
}
