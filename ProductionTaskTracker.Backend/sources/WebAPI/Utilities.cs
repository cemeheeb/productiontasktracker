﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI {
    public static class Utilities {
        public static DateTime BuildDate(string dateString) {
            var regex = new Regex(@"^\d{2}.\d{2}.\d{4}$");
            if (!regex.IsMatch(dateString)) {
                throw new FormatException($"Неверный формат даты {dateString}. Ожидается дата в формате 01.01.1997");
            }
            
            return DateTime.ParseExact(dateString, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
        }
    }
}