﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI {
    public class DateTimeModelBinder: IModelBinder {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext) {
            ValidateBindingContext(bindingContext);

            if (!bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName) ||
                !CanBindType(bindingContext.ModelType)) {
                return false;
            }

            var modelName = bindingContext.ModelName;
            var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
            if (valueProviderResult == null)
                return false;

            var attemptedValue = valueProviderResult.AttemptedValue;

            try {
                bindingContext.Model = DateTime.Parse(attemptedValue);
            } catch (FormatException e) {
                bindingContext.ModelState.AddModelError(modelName, e);
            }

            return true;
        }

        private static void ValidateBindingContext(ModelBindingContext bindingContext) {
            if (bindingContext == null) {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            if (bindingContext.ModelMetadata == null) {
                throw new ArgumentException("ModelMetadata cannot be null", nameof(bindingContext));
            }
        }

        public static bool CanBindType(Type modelType) {
            return modelType == typeof(DateTime) || modelType == typeof(DateTime?);
        }
    }
}