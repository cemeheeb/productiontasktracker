﻿using System;
using System.IdentityModel.Tokens;
using System.Web.Configuration;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Formats {
    public class JWTFormat : ISecureDataFormat<AuthenticationTicket> {
        #region Fields

        private static readonly byte[] Secret = TextEncodings.Base64Url.Decode(WebConfigurationManager.AppSettings["secret"]);
        private readonly string _issuer = WebConfigurationManager.AppSettings["issuer"];

        #endregion

        public string Protect(AuthenticationTicket data) {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            var credentials = new Thinktecture.IdentityModel.Tokens.HmacSigningCredentials(Secret);

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityTokenHandler()
                .WriteToken(new JwtSecurityToken(_issuer, "any", data.Identity.Claims, issued?.UtcDateTime, expires?.UtcDateTime, credentials));

            return token;
        }

        public AuthenticationTicket Unprotect(string protectedText) {
            throw new NotImplementedException();
        }
    }
}