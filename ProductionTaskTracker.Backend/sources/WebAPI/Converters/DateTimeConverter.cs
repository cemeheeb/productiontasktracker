﻿using Newtonsoft.Json.Converters;

namespace EAEConsult.ProductionTaskTracker.Backend.WebAPI.Converters {
    public class DateTimeConverter : IsoDateTimeConverter {
        public DateTimeConverter() {
            DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK";
        }
    }
}